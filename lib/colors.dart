import 'package:flutter/material.dart';

const fontFamilyApp = 'Roboto';

class CustomColors {
  static const Color primary = Color(0xFF6750A4);
  static const Color secondary = Color(0xFF625B71);
  static const Color tertiary = Color(0xFF7D5260);
  static const Color error = Color(0xFFB3261E);
  static const Color background = Color(0xFFFFFBFE);
  static const Color surface = Color(0xFFE7E0EC);
  static const Color tertiary70 = Color(0xFFD29DAC);
  static const Color secondary2 = Color(0xFF4A4458);
  static const Color primary2 = Color(0xFFD0BCFF);
  static const Color surfaceDark = Color(0xFF1C1B1F);
  static const Color primaryDark = Color(0xFF25005A);
  static const Color primaryContainer = Color(0xFF4F378B);
  static const Color tertiaryContainer = Color(0xFF633B48);
  static const Color secondaryContainer = Color(0xFF4A4458);
  static const Color primary70 = Color(0xFFB69DF8);
  static const Color activeSwitch = Color(0xFF6FF7F4);
  static const Color greenEnd = Color(0xFF88BCBA);
  static const Color greenList = Color(0xFF88BCBA);
  static const Color lightContainer = Color(0xFFEADDFF);
  static const Color primary80pink = Color(0xFFD3BBFF);
  static const Color orange2 = Color(0xFFED6746);
  static const Color primary60 = Color(0xFF9F81D9);
  static const Color orange3 = Color(0xFFFFB4A3);
  static const Color primary3 = Color(0xFF523689);
  static const Color greenButton = Color(0xFF4DDAD7);
  static const Color ticketOk = Color(0xFFAFFFFC);
  static const Color orange = Color(0xFFFF8B6F);
  static const Color purplePrograma = Color(0xFF4F3680);
  static const Color purpleDark = Color(0xFF3B1D71);
  static const Color greenDetails = Color(0xFF00504E);
  static const Color purplePsicologo = Color(0xFF150038);
  static const Color purpleProgreso = Color(0xFF34156A);
}
