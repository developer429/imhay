// ignore_for_file: avoid_print, use_rethrow_when_possible, unused_local_variable

import 'dart:convert';

import 'package:http/http.dart' as http;

/*
class DialogflowCXResponse {
  // Variables iniciales, idénticas a las llaves del JSON de la Cloud Function
  int _end; // 1 si es el último mensaje del agente, 0 si no
  Map<String, dynamic> _payload; // Contiene el tipo de elemento y su content para ItemMessage
  List<String> _options; // Opciones de respuesta rápida
  List<String> _response; // El texto de respuesta del bot
  int _statusCode; // El código de estado (200 es que estamos bien)
  Map<String, dynamic> _params; // Los parámetros de la respuesta del agente
  List<Map<String, dynamic>> _habits; // Los habitos ofrecido por el agente

  // Getters
  int get end => _end;
  Map<String, dynamic> get payload => _payload;
  List<String> get options => _options;
  List<String> get response => _response;
  int get statusCode => _statusCode;
  Map<String, dynamic> get params => _params;
  List<Map<String, dynamic>> get habits => _habits;

  // Constructor
  DialogflowCXResponse() {
    _end = 0;
    _payload = {};
    _options = [];
    _response = [];
    _statusCode = 0;
    _params = {};
    _habits = [];
  }

  // Lector del JSON
  DialogflowCXResponse.fromJson(Map<String, dynamic> json) {
    print('json: ' + json.toString());

    _end = json['end'];
    _payload = Map<String, dynamic>.from(json['payload']);
    _options = List<String>.from(json['payload']['options']);
    _response = List<String>.from(json['respuesta_alt']);
    _statusCode = json['statusCode'];
    _params = Map<String, dynamic>.from(json['params']);
    _habits = List<Map<String, dynamic>>.from(json['habitos_disponibles']);

    // print('  <--  _end: ' + _end.toString() + '  -->');
    // print('  <--  _payload: ' + _payload.toString() + '  -->');
    // print('  <--  _options: ' + _options.toString() + '  -->');
    // print('  <--  _response: ' + _response.toString() + '  -->');
    // print('  <--  _statusCode: ' + _statusCode.toString() + '  -->');
    // print('  <--  _params: ' + _params.toString() + '  -->');
    // print('  <--  _habits: ' + _habits.toString() + '  -->');
  }
}
*/
class NotificationService {
  // Link de la Cloud Function desarrollada por Felipe
  static const String sendNots =
      'https://us-central1-maquetadatos-5dee1.cloudfunctions.net/sendNots';
  static const String sendPHQ4 =
      'https://us-central1-maquetadatos-5dee1.cloudfunctions.net/responsePHQ4_termometro';
  static const String sendTermometro =
      'https://us-central1-maquetadatos-5dee1.cloudfunctions.net/responsePHQ4_termometro';
  static const String sendToken =
      'https://us-central1-maquetadatos-5dee1.cloudfunctions.net/addPsiToken';

  // Método para enviar mensajes al bot, detectando intents
  Future<dynamic> sendNotification(String uidPsicologo, String mensajeUsuario,
      String uidUsuario, bool type) async {
    // Se hace un POST a la Cloud Function con el query del usuario y una ID de sesión requerida por el agente
    try {
      final response = await http.post(
        Uri.parse(sendNots),
        body: json.encode({
          'id_receptor': uidPsicologo,
          'msj': mensajeUsuario,
          'id_emisor': uidUsuario,
          'user_type': type
        }),
        headers: {'Content-Type': 'application/json'},
      );

      // Retornamos un DialogflowCXResponse (clase de arriba) si el código nos indica que la conexión fue exitosa
      //print('statusCode: ' + response.statusCode.toString());
      /*if (response.statusCode == 200) {
        final data = Map<String, dynamic>.from(json.decode(response.body));
        return DialogflowCXResponse.fromJson(data);
      }*/
      //throw PlatformException(code: 'bad_request', message: 'Missing params');
    } catch (e) {
      //print('  <--  Error al hacer el POST  -->');
      throw e;
    }
  }

  Future<dynamic> sendAnswersPHQ4(
      String user, int r1, int r2, int r3, int r4, int r5) async {
    // Se hace un POST a la Cloud Function con el query del usuario y una ID de sesión requerida por el agente
    try {
      final response = await http.post(
        Uri.parse(sendPHQ4),
        body: json.encode({
          'usr_id': user,
          "responses": {
            "phq4": [r1, r2, r3, r4, r5], //Lista de ints
          }
        }),
        headers: {'Content-Type': 'application/json'},
      );

      // Retornamos un DialogflowCXResponse (clase de arriba) si el código nos indica que la conexión fue exitosa
      //print('statusCode: ' + response.statusCode.toString());
      /*if (response.statusCode == 200) {
        final data = Map<String, dynamic>.from(json.decode(response.body));
        return DialogflowCXResponse.fromJson(data);
      }*/
      //throw PlatformException(code: 'bad_request', message: 'Missing params');
    } catch (e) {
      //print('  <--  Error al hacer el POST  -->');
      throw e;
    }
  }

  Future<dynamic> sendAnswersTermometro(
      String user, int termometroValue) async {
    // Se hace un POST a la Cloud Function con el query del usuario y una ID de sesión requerida por el agente
    try {
      final response = await http.post(
        Uri.parse(sendTermometro),
        body: json.encode({
          'usr_id': user,
          "responses": {
            "termometro": termometroValue, //Lista de ints
          }
        }),
        headers: {
          'Content-Type': 'application/json',
        },
      );

      // Retornamos un DialogflowCXResponse (clase de arriba) si el código nos indica que la conexión fue exitosa
      //print('statusCode: ' + response.statusCode.toString());
      /*if (response.statusCode == 200) {
        final data = Map<String, dynamic>.from(json.decode(response.body));
        return DialogflowCXResponse.fromJson(data);
      }*/
      //throw PlatformException(code: 'bad_request', message: 'Missing params');
    } catch (e) {
      //print('  <--  Error al hacer el POST  -->');
      throw e;
    }
  }

  Future<dynamic> sendTokenPsicologo(String user, String token) async {
    // Se hace un POST a la Cloud Function con el query del usuario y una ID de sesión requerida por el agente
    try {
      final response = await http.post(
        Uri.parse(sendToken),
        body: json.encode({
          'pwd': user,
          'dev_token': token,
        }),
        headers: {'Content-Type': 'application/json'},
      );

      // Retornamos un DialogflowCXResponse (clase de arriba) si el código nos indica que la conexión fue exitosa
      //print('statusCode: ' + response.statusCode.toString());
      /*if (response.statusCode == 200) {
        final data = Map<String, dynamic>.from(json.decode(response.body));
        return DialogflowCXResponse.fromJson(data);
      }*/
      //throw PlatformException(code: 'bad_request', message: 'Missing params');
    } catch (e) {
      print('  <--  Error al hacer el POST  -->');
      throw e;
    }
  }
}
