// ignore_for_file: file_names, avoid_unnecessary_containers, sized_box_for_whitespace

import 'package:audio_video_progress_bar/audio_video_progress_bar.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:just_audio/just_audio.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:rxdart/rxdart.dart';
import 'package:seguimiento/colors.dart';

class AudioPlayerURL extends StatefulWidget {
  final String url;
  final bool isModule;
  const AudioPlayerURL({Key? key, required this.url, required this.isModule})
      : super(key: key);
  @override
  State<AudioPlayerURL> createState() => _AudioPlayerURLState();
}

class _AudioPlayerURLState extends State<AudioPlayerURL> {
  late AudioPlayer _audioPlayer;
  Stream<PositionData> get _positionDataStream =>
      Rx.combineLatest3<Duration, Duration, Duration?, PositionData>(
        _audioPlayer.positionStream,
        _audioPlayer.bufferedPositionStream,
        _audioPlayer.durationStream,
        (position, buffered, duration) =>
            PositionData(position, buffered, duration ?? Duration.zero),
      );

  @override
  void initState() {
    super.initState();
    _audioPlayer = AudioPlayer()..setUrl(widget.url);
  }

  @override
  void dispose() {
    _audioPlayer.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Controles(
            audioPlayer: _audioPlayer,
            isModule: widget.isModule,
          ),
          Container(
            width: kIsWeb ? 100 : 280,
            child: StreamBuilder<PositionData>(
                stream: _positionDataStream,
                builder: (context, snapshot) {
                  final positionData = snapshot.data;
                  return ProgressBar(
                    thumbColor: widget.isModule
                        ? CustomColors.primary2
                        : CustomColors.primary,
                    progressBarColor: CustomColors.primary,
                    baseBarColor: CustomColors.tertiary,
                    bufferedBarColor: CustomColors.primary60,
                    progress: positionData?.position ?? Duration.zero,
                    buffered: positionData?.buffered ?? Duration.zero,
                    total: positionData?.duration ?? Duration.zero,
                    onSeek: _audioPlayer.seek,
                    timeLabelTextStyle: TextStyle(
                        color: widget.isModule
                            ? CustomColors.primary
                            : Colors.black,
                        fontSize: 12),
                  );
                }),
          ),
        ],
      ),
    );
  }
}

class Controles extends StatelessWidget {
  const Controles({Key? key, required this.audioPlayer, required this.isModule})
      : super(key: key);
  final AudioPlayer audioPlayer;
  final bool isModule;
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<PlayerState>(
        stream: audioPlayer.playerStateStream,
        builder: (context, snapshot) {
          final playerState = snapshot.data;
          final processingState = playerState?.processingState;
          final playing = playerState?.playing;
          if (!(playing ?? false)) {
            return Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100),
                  color:
                      isModule ? CustomColors.primary2 : CustomColors.primary),
              child: IconButton(
                onPressed: audioPlayer.play,
                icon: Icon(
                  MdiIcons.playOutline,
                  color: isModule ? Colors.black : Colors.white,
                ),
              ),
            );
          } else if (processingState != ProcessingState.completed) {
            return Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100),
                  color: CustomColors.primary2),
              child: IconButton(
                onPressed: audioPlayer.pause,
                icon: Icon(MdiIcons.pause),
              ),
            );
          }
          return Icon(MdiIcons.play);
        });
  }
}

class PositionData {
  const PositionData(this.position, this.buffered, this.duration);
  final Duration position;
  final Duration buffered;
  final Duration duration;
}
