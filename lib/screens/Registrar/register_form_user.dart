// ignore_for_file: prefer_typing_uninitialized_variables

import 'package:flutter/material.dart';
import 'package:seguimiento/colors.dart';
import 'package:seguimiento/user_pages/MainPage.dart';

import '../../firebase.dart';

class FormRUT extends StatefulWidget {
  const FormRUT({Key? key, required this.uid}) : super(key: key);
  final uid;
  @override
  State<FormRUT> createState() => _FormRUTState();
}

class _FormRUTState extends State<FormRUT> {
  final _formKey = GlobalKey<FormState>();
  final _rutC = TextEditingController();
  final _nombreC = TextEditingController();
  bool selectedPronombre = true;
  List<String> pronombres =
      <String>['Selecciona un pronombre', 'Él', 'Ella', 'Elle'].toList();
  late String _selection;
  @override
  void initState() {
    _selection = pronombres.first;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      color: CustomColors.primary80pink,
      child: SafeArea(
        child: Scaffold(
          body: Container(
            color: CustomColors.primary80pink,
            child: ListView(
              keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
              children: [
                Container(
                  width: size.width,
                  alignment: Alignment.topLeft,
                  child: IconButton(
                    onPressed: () => Navigator.pop(context),
                    icon: const Icon(
                      Icons.close,
                    ),
                  ),
                ),
                const Image(
                  image: AssetImage('assets/images/logo-inicio.png'),
                ),
                Container(
                  margin: const EdgeInsets.all(10),
                  child: RichText(
                    textScaler: const TextScaler.linear(1.0),
                    textAlign: TextAlign.center,
                    text: const TextSpan(
                      children: <TextSpan>[
                        TextSpan(
                            text: 'Tus datos personales',
                            style: TextStyle(
                                fontSize: 24, color: CustomColors.primaryDark)),
                      ],
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(top: 30),
                  child: Form(
                    key: _formKey,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              margin: const EdgeInsets.all(10),
                              width: size.width / 1.2,
                              child: TextFormField(
                                keyboardType: TextInputType.visiblePassword,
                                validator: (String? value) {
                                  return (value == null || value == '')
                                      ? 'Campo vacío.'
                                      : null;
                                },
                                textAlign: TextAlign.left,
                                autofocus: false,
                                controller: _rutC,
                                decoration: const InputDecoration(
                                    labelText: 'RUT',
                                    floatingLabelBehavior:
                                        FloatingLabelBehavior.always,
                                    hintText: 'Sin puntos ni guión',
                                    border: OutlineInputBorder()),
                              ),
                            ),
                            Container(
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      color: selectedPronombre
                                          ? Colors.black
                                          : CustomColors.error),
                                  borderRadius: BorderRadius.circular(5)),
                              margin: const EdgeInsets.all(10),
                              width: size.width / 1.2,
                              alignment: Alignment.center,
                              height: 70,
                              padding: const EdgeInsets.all(10),
                              child: DropdownButton<String>(
                                underline: Container(
                                  height: 0,
                                  color: Colors.transparent,
                                ),
                                isExpanded: true,
                                value: _selection,
                                elevation: 16,
                                style: const TextStyle(color: Colors.black),
                                isDense: true,
                                onChanged: (newvalue) {
                                  // This is called when the user selects an item.
                                  setState(() {
                                    _selection = newvalue!;
                                  });
                                },
                                items: pronombres.map((String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(
                                      value,
                                      textScaler: const TextScaler.linear(1.0),
                                    ),
                                  );
                                }).toList(),
                              ),
                            ),
                            Visibility(
                              visible: !selectedPronombre,
                              child: Container(
                                width: size.width / 1.25,
                                margin:
                                    const EdgeInsets.only(left: 10, bottom: 20),
                                alignment: Alignment.topLeft,
                                child: const Text(
                                  'Selecciona una opción',
                                  textAlign: TextAlign.start,
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(color: CustomColors.error),
                                ),
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.all(10),
                              width: size.width / 1.2,
                              child: TextFormField(
                                validator: (String? value) {
                                  return (value == null || value == '')
                                      ? 'Campo vacío.'
                                      : null;
                                },
                                textAlign: TextAlign.left,
                                autofocus: false,
                                controller: _nombreC,
                                decoration: const InputDecoration(
                                    labelText: 'Nombre',
                                    floatingLabelBehavior:
                                        FloatingLabelBehavior.always,
                                    hintText: 'Tu nombre completo',
                                    border: OutlineInputBorder()),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(
                      left: 20, right: 20, top: 100, bottom: 10),
                  width: size.width / 1.2,
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor:
                          WidgetStateProperty.all<Color>(CustomColors.primary),
                      shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                      ),
                    ),
                    onPressed: () {
                      if (_formKey.currentState!.validate()) {
                        showDialog<String>(
                          context: context,
                          builder: (BuildContext context) => AlertDialog(
                            title: const Text(
                              '¡Hecho!',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                            ),
                            content: const Text(
                              'Se ha registrado correctamente.\nSerá redirigido a la página de inicio',
                              textScaler: TextScaler.linear(1.0),
                            ),
                            actions: <Widget>[
                              TextButton(
                                onPressed: () {
                                  FirestoreService()
                                      .agregarUsuario(
                                          _rutC.value.text.trim(),
                                          _nombreC.value.text.trim(),
                                          _selection.toString(),
                                          widget.uid)
                                      .then((value) =>
                                          Navigator.pushAndRemoveUntil(
                                              context,
                                              MaterialPageRoute<void>(
                                                  builder:
                                                      (BuildContext context) =>
                                                          const MainPage(
                                                            activePage: 0,
                                                          )),
                                              (route) => false));
                                },
                                child: const Text(
                                  'Comenzar con la experiencia',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      color: CustomColors.primary,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                        );
                      } else {
                        if (_selection == 'Selecciona un pronombre') {
                          setState(() {
                            selectedPronombre = false;
                          });
                        }
                        showDialog<String>(
                          context: context,
                          builder: (BuildContext context) => AlertDialog(
                            title: const Text(
                              'Error',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  color: Colors.red,
                                  fontWeight: FontWeight.bold),
                            ),
                            content: const Text(
                              'Complete los campos faltantes',
                              textScaler: TextScaler.linear(1.0),
                            ),
                            actions: <Widget>[
                              TextButton(
                                onPressed: () => Navigator.pop(
                                  context,
                                ),
                                child: const Text(
                                  'OK',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                        );
                      }
                    },
                    child: const Text(
                      "Finalizar",
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
