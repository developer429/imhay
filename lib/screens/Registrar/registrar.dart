// ignore_for_file: avoid_print, unnecessary_null_comparison

import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:seguimiento/chatlogics/models_and_others/app.dart';
import 'package:seguimiento/colors.dart';
import 'package:seguimiento/screens/Registrar/register_form_user.dart';
import 'package:seguimiento/screens/iniciar_sesion.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';
import '../../firebase.dart';
import '../../user_pages/MainPage.dart';

class Registrar extends StatefulWidget {
  const Registrar({Key? key}) : super(key: key);

  @override
  State<Registrar> createState() => _RegistrarState();
}

class _RegistrarState extends State<Registrar> {
  final FirebaseMessaging firebaseMessaging = FirebaseMessaging.instance;

  void _handleLogin(String provider) async {
    try {
      UserCredential? uc;
      if (provider == 'google') {
        uc = await signInWithGoogle();
        print(uc.user!.uid);
      } else {
        uc = await signInWithApple();
        print(uc.user!.uid);
      }

      if (uc != null) {
        final snapShot = await FirebaseFirestore.instance
            .collection('usuarios')
            .doc(uc.user!.uid) // varuId in your case
            .get();

        if (snapShot == null || !snapShot.exists) {
          // docuement is not exist

          print('id not exist');

          Navigator.push(
            context,
            MaterialPageRoute<void>(
                builder: (BuildContext context) => FormRUT(
                      uid: uc!.user?.uid,
                    )),
          );
        } else {
          print("id is exists");

          firebaseMessaging.requestPermission();

          firebaseMessaging.getToken().then((token) {
            if (token != null) {
              FirestoreService().updateTokenUsuario(token);
            }
          });
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute<void>(
                  builder: (BuildContext context) => const MainPage(
                        activePage: 0,
                      )),
              (route) => false);
        }
      }
    } on FirebaseException catch (e) {
      print(e);
      EasyLoading.showToast(e.message.toString());
    } catch (e) {
      print(e);
      EasyLoading.showToast('Ha ocurrido un error');
    }
  }

  Future<UserCredential> signInWithGoogle() async {
    if (kIsWeb) {
      GoogleAuthProvider googleAuthProvider = GoogleAuthProvider();

      return FirebaseAuth.instance.signInWithPopup(googleAuthProvider);
    }
    final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();

    final GoogleSignInAuthentication googleAuth =
        await googleUser!.authentication;

    final OAuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    return FirebaseAuth.instance.signInWithCredential(credential);
  }

  Future<UserCredential> signInWithApple() async {
    final rawNonce = generateNonce();
    final nonce = sha256ofString(rawNonce);

    // Request credential for the currently signed in Apple account.
    final appleCredential = await SignInWithApple.getAppleIDCredential(
      scopes: [
        AppleIDAuthorizationScopes.email,
        AppleIDAuthorizationScopes.fullName,
      ],
      nonce: nonce,
    );

    final oauthCredential = OAuthProvider("apple.com").credential(
      idToken: appleCredential.identityToken,
      rawNonce: rawNonce,
    );

    return await FirebaseAuth.instance.signInWithCredential(oauthCredential);
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Container(
      color: CustomColors.primary80pink,
      child: SafeArea(
        child: Container(
          color: CustomColors.primary80pink,
          child: Column(
            children: [
              Container(
                width: size.width,
                alignment: Alignment.topLeft,
                child: IconButton(
                  onPressed: () => Navigator.pop(context),
                  icon: const Icon(
                    Icons.close,
                  ),
                ),
              ),
              const Spacer(),
              Container(
                margin: const EdgeInsets.all(10),
                child: const Image(
                  image: AssetImage('assets/images/logo-inicio.png'),
                ),
              ),
              Container(
                margin: const EdgeInsets.all(10),
                width: size.width / 1.1,
                child: RichText(
                  textAlign: TextAlign.center,
                  text: const TextSpan(
                    children: <TextSpan>[
                      TextSpan(
                          text: 'Crea tu cuenta',
                          style: TextStyle(fontSize: 24, color: Colors.black)),
                    ],
                  ),
                ),
              ),
              const Spacer(),
              SizedBox(
                width: size.width / 1.2,
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: WidgetStateProperty.all<Color>(kIsWeb
                        ? CustomColors.primary
                        : Platform.isIOS
                            ? Colors.black
                            : CustomColors.primary),
                    shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                    ),
                  ),
                  onPressed: () => kIsWeb
                      ? _handleLogin('google')
                      : Platform.isIOS
                          ? _handleLogin('apple')
                          : _handleLogin('google'),
                  child: Text(
                    kIsWeb
                        ? 'Registrarte con Google'
                        : Platform.isIOS
                            ? 'Registrarte con Apple ID'
                            : 'Registrarte con Google',
                    textScaler: const TextScaler.linear(1.0),
                    textAlign: TextAlign.center,
                    style: const TextStyle(color: Colors.white),
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(bottom: 20, top: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text(
                      '¿Ya tienes una cuenta?',
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(fontSize: 15, color: Colors.black),
                    ),
                    TextButton(
                      style: ButtonStyle(
                        foregroundColor: WidgetStateProperty.all<Color>(
                            CustomColors.primary),
                      ),
                      onPressed: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: ((context) => const Sesion()),
                        ),
                      ),
                      child: const Text(
                        'Inicia sesión',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            decoration: TextDecoration.underline,
                            fontSize: 15,
                            color: Colors.black,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
