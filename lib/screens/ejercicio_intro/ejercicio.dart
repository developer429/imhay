import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:seguimiento/colors.dart';
import 'package:seguimiento/main.dart';

class Ejercicio extends StatefulWidget {
  const Ejercicio({
    Key? key,
  }) : super(key: key);

  @override
  State<Ejercicio> createState() => _EjercicioState();
}

class _EjercicioState extends State<Ejercicio> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  int activeStep = 0;
  bool activeLast = false;
  bool end = false;
  @override
  Widget build(BuildContext context) {
    switch (activeStep) {
      case 3:
        activeLast = true;
        break;
      case 4:
        end = true;
        break;
      default:
    }
    return Container(
      color: end ? CustomColors.primary80pink : CustomColors.lightContainer,
      child: SafeArea(
        child: Scaffold(
          backgroundColor:
              end ? CustomColors.primary80pink : CustomColors.lightContainer,
          body: Center(
            child: Column(
              children: [
                content(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget topStructure() {
    return Column(
      children: [
        Container(
          alignment: Alignment.topLeft,
          child: IconButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            icon: const Icon(
              Icons.close,
              color: CustomColors.primaryDark,
            ),
          ),
        ),
        header()
      ],
    );
  }

  Widget bottomStructure() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        nextButton(),
      ],
    );
  }

  Widget nextButton() {
    Size size = MediaQuery.of(context).size;

    return Container(
      margin: const EdgeInsets.only(bottom: 10),
      width: size.width / 1.1,
      child: ElevatedButton(
        style: ButtonStyle(
          backgroundColor: WidgetStateProperty.all<Color>(CustomColors.primary),
          shape: WidgetStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(18.0),
            ),
          ),
        ),
        onPressed: () {
          if (activeStep < 4) {
            setState(() {
              activeStep++;
            });
          } else {
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute<void>(
                    builder: (BuildContext context) => const MyHomePage()),
                (route) => false);
          }
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            activeLast
                ? Container()
                : Container(
                    margin: const EdgeInsets.only(right: 10),
                    child: const Icon(
                      Icons.arrow_forward,
                      color: CustomColors.background,
                    )),
            Text(
              buttonText(),
              textScaler: const TextScaler.linear(1.0),
              style: const TextStyle(color: CustomColors.background),
            ),
          ],
        ),
      ),
    );
  }

  Widget header() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      default:
        return Container(
          margin: const EdgeInsets.only(top: 100),
          child: Image(
            width: kIsWeb
                ? 200
                : end
                    ? size.width
                    : size.width / 2,
            height: kIsWeb
                ? 200
                : end
                    ? null
                    : size.width / 2,
            fit: BoxFit.scaleDown,
            image: AssetImage(imageHeader()),
          ),
        );
    }
  }

  String imageHeader() {
    switch (activeStep) {
      case 0:
        return 'assets/images/s-intro.png';
      case 1:
        return 'assets/images/t-intro.png';
      case 2:
        return 'assets/images/o-intro.png';
      case 3:
        return 'assets/images/p-intro.png';
      case 4:
        return 'assets/images/logo-inicio.png';

      default:
        return '';
    }
  }

  String headerText() {
    switch (activeStep) {
      default:
        return '';
    }
  }

  String buttonText() {
    switch (activeStep) {
      case 3:
        return 'Finalizar';
      case 4:
        return 'Volver al inicio';
      default:
        return 'Siguiente';
    }
  }

  Widget content() {
    switch (activeStep) {
      case 0:
        return Expanded(
          child: Column(
            children: [
              topStructure(),
              Container(
                margin: const EdgeInsets.all(20),
                child: RichText(
                  textAlign: TextAlign.center,
                  text: const TextSpan(
                    text: '',
                    style: TextStyle(
                      fontSize: 16,
                      color: CustomColors.primary,
                      height: 1.5,
                    ),
                    children: <TextSpan>[
                      TextSpan(
                          text: 'Suspende ',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          )),
                      TextSpan(
                        text:
                            'todas las actividades que estés realizando en este momento. Tómate una pausa y enfócate en el momento.',
                      ),
                    ],
                  ),
                ),
              ),
              const Spacer(),
              bottomStructure(),
            ],
          ),
        );

      case 1:
        return Expanded(
          child: Column(
            children: [
              topStructure(),
              Container(
                margin: const EdgeInsets.all(20),
                child: RichText(
                  textAlign: TextAlign.center,
                  text: const TextSpan(
                    text: '',
                    style: TextStyle(
                      fontSize: 16,
                      color: CustomColors.primary,
                      height: 1.5,
                    ),
                    children: <TextSpan>[
                      TextSpan(
                          text: 'Toma ',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          )),
                      TextSpan(
                        text:
                            'aire profunda y lentamente, luego exhala despacio. Repite un par de veces, prestando atención a tu respiración. \nLa respiración es un ancla al momento presente.',
                      ),
                    ],
                  ),
                ),
              ),
              const Spacer(),
              bottomStructure(),
            ],
          ),
        );
      case 2:
        return Expanded(
          child: Column(
            children: [
              topStructure(),
              Container(
                margin: const EdgeInsets.all(20),
                child: RichText(
                  textAlign: TextAlign.center,
                  text: const TextSpan(
                    text: '',
                    style: TextStyle(
                      fontSize: 16,
                      color: CustomColors.primary,
                      height: 1.5,
                    ),
                    children: <TextSpan>[
                      TextSpan(
                          text: 'Observa: ',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          )),
                      TextSpan(
                        text:
                            'Observa: date cuenta de lo que te sucede en tu interior y alrededor tuyo. Enfócate en alguna parte de tu cuerpo, ¿cómo la percibes?. Ahora mira a tu entorno, ¿qué es lo que ves?\nTómate el tiempo que necesites para conectarte y luego presiona "Siguiente".',
                      ),
                    ],
                  ),
                ),
              ),
              const Spacer(),
              bottomStructure(),
            ],
          ),
        );
      case 3:
        return Expanded(
          child: Column(
            children: [
              topStructure(),
              Container(
                margin: const EdgeInsets.all(20),
                child: RichText(
                  textAlign: TextAlign.center,
                  text: const TextSpan(
                    text: '',
                    style: TextStyle(
                      fontSize: 16,
                      color: CustomColors.primary,
                      height: 1.5,
                    ),
                    children: <TextSpan>[
                      TextSpan(
                          text: 'Prosigue: ',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          )),
                      TextSpan(
                        text:
                            'Prosigue: llegó el momento de continuar, ¿te sientes diferente? Sigue con lo que estabas haciendo previamente, ahora con más tranquilidad.',
                      ),
                    ],
                  ),
                ),
              ),
              const Spacer(),
              bottomStructure(),
            ],
          ),
        );
      case 4:
        return Expanded(
          child: Column(
            children: [
              topStructure(),
              Container(
                margin: const EdgeInsets.only(top: 50, bottom: 20),
                child: const FittedBox(
                  fit: BoxFit.fitWidth,
                  child: Text(
                    '¡Muy bien!',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(color: CustomColors.primary, fontSize: 24),
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.all(20),
                child: const Text(
                  'Ya has logrado hacer tu primera actividad para mejorar tu salud mental',
                  textScaler: TextScaler.linear(1.0),
                  style: TextStyle(
                    fontSize: 16,
                    height: 1.5,
                    color: CustomColors.primary,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              const Spacer(),
              bottomStructure(),
            ],
          ),
        );

      default:
        return Container();
    }
  }
}
