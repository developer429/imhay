import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:seguimiento/colors.dart';
import 'ejercicio.dart';

class IntroEjercicio extends StatefulWidget {
  const IntroEjercicio({Key? key}) : super(key: key);

  @override
  State<IntroEjercicio> createState() => _IntroEjercicioState();
}

class _IntroEjercicioState extends State<IntroEjercicio> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Container(
      color: CustomColors.lightContainer,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: CustomColors.lightContainer,
          body: Center(
            child: Column(
              children: [
                Container(
                  width: size.width,
                  alignment: Alignment.topLeft,
                  child: IconButton(
                    onPressed: () => Navigator.pop(context),
                    icon: const Icon(
                      Icons.close,
                    ),
                  ),
                ),
                const Spacer(),
                Container(
                  margin: const EdgeInsets.all(10),
                  child: Image(
                    width: kIsWeb ? 200 : size.width / 3,
                    height: kIsWeb ? 200 : size.width / 3,
                    fit: BoxFit.scaleDown,
                    image: const AssetImage('assets/images/stop-ejercicio.png'),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.all(20),
                  child: const Text(
                    'Ejercicio de conexión con el presente',
                    textScaler: TextScaler.linear(1.0),
                    textAlign: TextAlign.center,
                    style: TextStyle(color: CustomColors.primary, fontSize: 24),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.all(20),
                  child: const Text(
                    'Hacer una pausa para conectarnos con el presente nos ayuda a ver en perspectiva y elegir cómo reaccionar frente a lo que estamos viviendo. La sigla STOP es una clave para recordarnos hacer esto. Te invitamos a ponerlo en práctica a continuación.',
                    textScaler: TextScaler.linear(1.0),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: CustomColors.primary, fontSize: 16, height: 1.5),
                  ),
                ),
                const Spacer(),
                Container(
                  margin: const EdgeInsets.only(bottom: 20),
                  width: size.width / 1.2,
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor:
                          WidgetStateProperty.all<Color>(CustomColors.primary),
                      shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                        ),
                      ),
                    ),
                    onPressed: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: ((context) => const Ejercicio()),
                      ),
                    ),
                    child: const Text(
                      'Comenzar',
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(color: CustomColors.background),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
