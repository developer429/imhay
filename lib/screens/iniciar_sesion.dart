// ignore_for_file: avoid_print, avoid_unnecessary_containers, use_rethrow_when_possible, unnecessary_null_comparison
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:seguimiento/chatlogics/models_and_others/app.dart';
import 'package:seguimiento/screens/Registrar/registrar.dart';
import 'package:seguimiento/psicologo_pages/loginpsi.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';

import '../colors.dart';
import '../firebase.dart';
import '../user_pages/MainPage.dart';

class Sesion extends StatefulWidget {
  const Sesion({Key? key}) : super(key: key);

  @override
  State<Sesion> createState() => _SesionState();
}

class _SesionState extends State<Sesion> {
  bool loggedIn = false;
  final FirebaseMessaging firebaseMessaging = FirebaseMessaging.instance;
  Future<bool> checkIfDocExists(String docId) async {
    try {
      // Get reference to Firestore collection
      var collectionRef = FirebaseFirestore.instance.collection('usuarios');

      var doc = await collectionRef.doc(docId).get();
      return doc.exists;
    } catch (e) {
      throw e;
    }
  }

  void _handleLogin(String provider) async {
    try {
      UserCredential? uc;
      if (provider == 'google') {
        uc = await signInWithGoogle();
        print(uc.user!.uid + ': ' + uc.user!.email.toString());
      } else {
        uc = await signInWithApple();
        print('Signed in with Apple ID');
      }

      if (uc != null) {
        loggedIn = true;
        firebaseMessaging.requestPermission();
        firebaseMessaging.getToken().then((token) {
          if (token != null) {
            FirestoreService().updateTokenUsuario(token);
          }
        });
        final snapShot = await FirebaseFirestore.instance
            .collection('usuarios')
            .doc(uc.user!.uid) // varuId in your case
            .get();

        if (snapShot == null || !snapShot.exists) {
          // docuement is not exist

          print('id is not exist');

          Navigator.push(
            context,
            MaterialPageRoute<void>(
                builder: (BuildContext context) => const Registrar()),
          );
        } else {
          print("id is really exist");
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute<void>(
                  builder: (BuildContext context) => const MainPage(
                        activePage: 0,
                      )),
              (route) => false);
        }

        /**/
      }
    } on FirebaseException catch (e) {
      print(e);
      EasyLoading.showToast(e.message.toString());
    } catch (e) {
      print(e);
      EasyLoading.showToast('Ha ocurrido un error');
    }
  }

  Future<UserCredential> signInWithGoogle() async {
    if (kIsWeb) {
      GoogleAuthProvider googleAuthProvider = GoogleAuthProvider();

      return FirebaseAuth.instance.signInWithPopup(googleAuthProvider);
    }
    final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();

    final GoogleSignInAuthentication googleAuth =
        await googleUser!.authentication;

    final OAuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    return FirebaseAuth.instance.signInWithCredential(credential);
  }

  Future<UserCredential> signInWithApple() async {
    final rawNonce = generateNonce();
    final nonce = sha256ofString(rawNonce);

    // Request credential for the currently signed in Apple account.
    final appleCredential = await SignInWithApple.getAppleIDCredential(
      scopes: [
        AppleIDAuthorizationScopes.email,
        AppleIDAuthorizationScopes.fullName,
      ],
      nonce: nonce,
    );

    final oauthCredential = OAuthProvider("apple.com").credential(
      idToken: appleCredential.identityToken,
      rawNonce: rawNonce,
    );

    return await FirebaseAuth.instance.signInWithCredential(oauthCredential);
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Container(
      color: CustomColors.primary80pink,
      child: SafeArea(
        child: Container(
          color: CustomColors.primary80pink,
          child: Column(
            children: [
              Container(
                width: size.width,
                alignment: Alignment.topLeft,
                child: IconButton(
                  onPressed: () => Navigator.pop(context),
                  icon: const Icon(
                    Icons.close,
                  ),
                ),
              ),
              const Spacer(),
              Container(
                margin: const EdgeInsets.all(10),

                // ignore: prefer_const_constructors
                child: Image(
                  image: const AssetImage('assets/images/logo-inicio.png'),
                ),
              ),
              Container(
                margin: const EdgeInsets.all(10),
                child: const Text(
                  'Inicia sesión',
                  textScaler: TextScaler.linear(1.0),
                  style:
                      TextStyle(fontSize: 25, color: CustomColors.primaryDark),
                ),
              ),
              const Spacer(),
              Container(
                margin: const EdgeInsets.all(10),
                width: size.width / 1.1,
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: WidgetStateProperty.all<Color>(kIsWeb
                        ? CustomColors.primary
                        : Platform.isIOS
                            ? Colors.black
                            : CustomColors.primary),
                    shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                      ),
                    ),
                  ),
                  onPressed: () => kIsWeb
                      ? _handleLogin('google')
                      : Platform.isIOS
                          ? _handleLogin('apple')
                          : _handleLogin('google'),
                  child: Text(
                    kIsWeb
                        ? 'Inicia sesión con Google'
                        : Platform.isIOS
                            ? 'Inicia sesión con Apple ID'
                            : 'Inicia sesión con Google',
                    textScaler: const TextScaler.linear(1.0),
                    style: const TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.all(10),
                width: size.width / 1.1,
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        WidgetStateProperty.all<Color>(CustomColors.orange),
                    shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                      ),
                    ),
                  ),
                  onPressed: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const LoginPsicologo()),
                    ),
                  ),
                  child: const Text(
                    'Acceso psicólogo',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(bottom: 20, left: 20, right: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      child: const Text(
                        '¿No tienes una cuenta?',
                        textScaler: TextScaler.linear(1.0),
                      ),
                    ),
                    Container(
                      child: TextButton(
                        style: ButtonStyle(
                          foregroundColor:
                              WidgetStateProperty.all<Color>(Colors.blueGrey),
                        ),
                        onPressed: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: ((context) => const Registrar()),
                          ),
                        ),
                        child: const Text(
                          'Registrate',
                          textScaler: TextScaler.linear(1.0),
                          style: TextStyle(
                              decoration: TextDecoration.underline,
                              fontWeight: FontWeight.bold,
                              color: CustomColors.primaryDark),
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
