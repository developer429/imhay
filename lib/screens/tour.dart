import 'package:flutter/material.dart';
import 'package:seguimiento/colors.dart';
import 'package:seguimiento/screens/Registrar/registrar.dart';

import 'iniciar_sesion.dart';

class Tour extends StatefulWidget {
  const Tour({Key? key}) : super(key: key);

  @override
  _TourState createState() => _TourState();
}

class _TourState extends State<Tour> {
  PageController pagViewController = PageController();
  double currentPageIndex = 0.0;
  bool activeDot1 = true;
  bool activeDot2 = false;
  bool activeDot3 = false;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: CustomColors.primary,
        elevation: 0,
        iconTheme: const IconThemeData(
          color: CustomColors.background,
        ),
      ),
      backgroundColor: CustomColors.primary,
      body: Column(
        children: [
          const Spacer(),
          SingleChildScrollView(
            child: SizedBox(
              height: size.height / 1.5,
              child: PageView(
                controller: pagViewController,
                onPageChanged: (index) {
                  setState(() {
                    currentPageIndex = index + .0;
                    if (currentPageIndex == 0.0) {
                      activeDot1 = true;
                      activeDot2 = false;
                      activeDot3 = false;
                    } else if (currentPageIndex == 1.0) {
                      activeDot1 = false;
                      activeDot2 = true;
                      activeDot3 = false;
                    } else {
                      activeDot1 = false;
                      activeDot2 = false;
                      activeDot3 = true;
                    }
                  });
                },
                children: <Widget>[
                  stepOne(context),
                  stepTwo(context),
                  stepThree(context),
                ],
              ),
            ),
          ),
          const Spacer(),
          SizedBox(
            width: size.width / 1.2,
            child: ElevatedButton(
              style: ButtonStyle(
                backgroundColor:
                    WidgetStateProperty.all<Color>(CustomColors.orange),
                shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                  ),
                ),
              ),
              onPressed: () => Navigator.push(
                context,
                MaterialPageRoute(
                  builder: ((context) => const Registrar()),
                ),
              ),
              child: const Text(
                'Crea tu cuenta para comenzar',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(
                    color: CustomColors.primaryDark,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(bottom: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text(
                  '¿Ya tienes una cuenta?',
                  textScaler: TextScaler.linear(1.0),
                  style: TextStyle(fontSize: 15, color: CustomColors.surface),
                ),
                TextButton(
                  style: ButtonStyle(
                    foregroundColor:
                        WidgetStateProperty.all<Color>(CustomColors.background),
                  ),
                  onPressed: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const Sesion()),
                    ),
                  ),
                  child: const Text(
                    'Inicia sesión',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        decoration: TextDecoration.underline,
                        decorationColor: CustomColors.background,
                        fontSize: 15,
                        color: CustomColors.background,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget stepOne(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Column(
          children: [
            const Image(
              image: AssetImage('assets/images/tour-1.png'),
            ),
            Container(
              margin: const EdgeInsets.all(20),
              child: const Text(
                'Contarás con un programa con actividades secuenciales para realizar en 4 semanas, además de actividades libres.',
                textScaler: TextScaler.linear(1.0),
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 16, color: CustomColors.surface),
              ),
            ),
          ],
        ),
        Container(
          child: dotsNavigation(context),
        )
      ],
    );
  }

  Widget stepTwo(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Column(
          children: [
            const Image(
              image: AssetImage('assets/images/tour-2.png'),
            ),
            Container(
              margin: const EdgeInsets.all(20),
              child: const Text(
                'Podrás pedir ayuda a un profesional de salud mental.',
                textScaler: TextScaler.linear(1.0),
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 16, color: CustomColors.surface),
              ),
            ),
          ],
        ),
        Container(
          child: dotsNavigation(context),
        )
      ],
    );
  }

  Widget stepThree(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Column(
          children: [
            Container(
              margin: const EdgeInsets.only(bottom: 10),
              child: const Image(
                image: AssetImage('assets/images/tour-3.png'),
              ),
            ),
            Container(
              margin: const EdgeInsets.all(20),
              child: const Text(
                'Crea tu cuenta y guarda tu contenido preferido.',
                textScaler: TextScaler.linear(1.0),
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 16, color: CustomColors.surface),
              ),
            ),
          ],
        ),
        Container(
          child: dotsNavigation(context),
        )
      ],
    );
  }

  Widget dotsNavigation(context) {
    Size size = MediaQuery.of(context).size;

    return SizedBox(
      width: size.width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          InkWell(
            onTap: () {
              pagViewController.animateToPage(
                0,
                duration: const Duration(milliseconds: 500),
                curve: Curves.ease,
              );
            },
            child: Container(
              padding: const EdgeInsets.all(5),
              decoration: BoxDecoration(
                border: Border.all(color: CustomColors.surface, width: 2),
                color: activeDot1 ? CustomColors.surface : Colors.transparent,
                shape: BoxShape.circle,
              ),
            ),
          ),
          const SizedBox(
            width: 12,
          ),
          InkWell(
            onTap: () {
              pagViewController.animateToPage(
                1,
                duration: const Duration(milliseconds: 500),
                curve: Curves.ease,
              );
            },
            child: Container(
              padding: const EdgeInsets.all(5),
              decoration: BoxDecoration(
                  border: Border.all(color: CustomColors.surface, width: 2),
                  color: activeDot2 ? CustomColors.surface : Colors.transparent,
                  shape: BoxShape.circle),
            ),
          ),
          const SizedBox(
            width: 12,
          ),
          InkWell(
            onTap: () {
              pagViewController.animateToPage(
                2,
                duration: const Duration(milliseconds: 500),
                curve: Curves.ease,
              );
            },
            child: Container(
              padding: const EdgeInsets.all(5),
              decoration: BoxDecoration(
                  border: Border.all(color: CustomColors.surface, width: 2),
                  color: activeDot3 ? CustomColors.surface : Colors.transparent,
                  shape: BoxShape.circle),
            ),
          ),
        ],
      ),
    );
  }
}
