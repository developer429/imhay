// ignore_for_file: file_names

import 'package:appinio_video_player/appinio_video_player.dart';
import 'package:flutter/material.dart';

class VideoPlayerURL extends StatefulWidget {
  const VideoPlayerURL({Key? key, required this.url}) : super(key: key);
  final String url;

  @override
  State<VideoPlayerURL> createState() => _VideoPlayerURLState();
}

class _VideoPlayerURLState extends State<VideoPlayerURL> {
  late VideoPlayerController videoPlayerController;
  late CustomVideoPlayerController _customVideoPlayerController;
  @override
  void initState() {
    super.initState();
    videoPlayerController =
        VideoPlayerController.networkUrl(Uri.parse(widget.url))
          ..initialize().then((value) => setState(() {}));
    _customVideoPlayerController = CustomVideoPlayerController(
      context: context,
      videoPlayerController: videoPlayerController,
    );
  }

  @override
  void dispose() {
    _customVideoPlayerController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Transform.scale(
      scale: 0.95,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: CustomVideoPlayer(
          customVideoPlayerController: _customVideoPlayerController,
        ),
      ),
    );
  }
}
