import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:intl/intl.dart';

//retrieve all stored user info
class FirestoreService {
  Stream<QuerySnapshot> datos() {
    return FirebaseFirestore.instance.collection('datos').snapshots();
  }

  Stream<QuerySnapshot> favoritos() {
    return FirebaseFirestore.instance.collection('favoritos').snapshots();
  }

  Stream<QuerySnapshot> historialNotificaciones() {
    return FirebaseFirestore.instance
        .collection('historial_noti')
        .orderBy('fecha', descending: true)
        .snapshots();
  }

  Stream<QuerySnapshot> respM2A3() {
    return FirebaseFirestore.instance.collection('M2A3-respuestas').snapshots();
  }

  Stream<QuerySnapshot> respM3A5() {
    return FirebaseFirestore.instance.collection('M3A5-respuestas').snapshots();
  }

  Stream<QuerySnapshot> respM3A6() {
    return FirebaseFirestore.instance.collection('M3A6-respuestas').snapshots();
  }

  Stream<QuerySnapshot> respM3A7() {
    return FirebaseFirestore.instance.collection('M3A7-respuestas').snapshots();
  }

  Stream<QuerySnapshot> respM4A2() {
    return FirebaseFirestore.instance.collection('M4A2-respuestas').snapshots();
  }

  Stream<QuerySnapshot> respM4A3() {
    return FirebaseFirestore.instance.collection('M4A3-respuestas').snapshots();
  }

  Stream<QuerySnapshot> respM4A4() {
    return FirebaseFirestore.instance.collection('M4A4-respuestas').snapshots();
  }

  Stream<QuerySnapshot> respM4A5() {
    return FirebaseFirestore.instance.collection('M4A5-respuestas').snapshots();
  }

//creates user with they're data
  Future agregarUsuario(
      String rut, String nombre, String pronombre, String uid) async {
    String fechaInicio = DateFormat('yyy-MM-dd').format(DateTime.now());
    final FirebaseMessaging firebaseMessaging = FirebaseMessaging.instance;
    FirebaseFirestore.instance.collection('usuarios').doc(uid).set({
      'rut': rut,
      'nombreCompleto': nombre,
      'pronombre': pronombre,
      'psicologo': 'bCWKR2WSgLRlhYcOqNfvaifuqQ92',
      'fechaInicio': fechaInicio,
      'days': [true, true, true, true, true, true, true],
      'time': 12,
      'receiveNots': true,
      'alerta': 'verde',
      'estadoAtencion': 'Ingresado',
      'termometro': 5
    });
    firebaseMessaging.requestPermission();

    firebaseMessaging.getToken().then((token) {
      if (token != null) {
        FirestoreService().updateTokenUsuario(token);
      }
    });

    FirebaseFirestore.instance.collection('datos').doc(uid).set({
      'M1A0': 'No se ha completado',
      'M1A1': 'No se ha completado',
      'M1A2': 'No se ha completado',
      'M1A3': 'No se ha completado',
      'M1A4': 'No se ha completado',
      'M1A5': 'No se ha completado',
      'M1AT': 'No se ha completado',
      'M2A1': 'No se ha completado',
      'M2A2': 'No se ha completado',
      'M2A3': 'No se ha completado',
      'M2A4': 'No se ha completado',
      'M2A5': 'No se ha completado',
      'M2A6': 'No se ha completado',
      'M2AT': 'No se ha completado',
      'M3A1': 'No se ha completado',
      'M3A2': 'No se ha completado',
      'M3A3': 'No se ha completado',
      'M3A4': 'No se ha completado',
      'M3A5': 'No se ha completado',
      'M3A6': 'No se ha completado',
      'M3A7': 'No se ha completado',
      'M3A8': 'No se ha completado',
      'M3AT': 'No se ha completado',
      'M4A1': 'No se ha completado',
      'M4A2': 'No se ha completado',
      'M4A3': 'No se ha completado',
      'M4A4': 'No se ha completado',
      'M4A5': 'No se ha completado',
      'M4A6': 'No se ha completado',
      'M4AT': 'No se ha completado',
    });
    FirebaseFirestore.instance.collection('control').doc(uid).set({
      'terminaM1A0': false,
      'terminaM1A1': false,
      'terminaM1A2': false,
      'terminaM1A3': false,
      'terminaM1A4': false,
      'terminaM1A5': false,
      'terminaM1AT': false,
      'terminaM2A1': false,
      'terminaM2A2': false,
      'terminaM2A3': false,
      'terminaM2A4': false,
      'terminaM2A5': false,
      'terminaM2A6': false,
      'terminaM2AT': false,
      'terminaM3A1': false,
      'terminaM3A2': false,
      'terminaM3A3': false,
      'terminaM3A4': false,
      'terminaM3A5': false,
      'terminaM3A6': false,
      'terminaM3A7': false,
      'terminaM3A8': false,
      'terminaM3AT': false,
      'terminaM4A1': false,
      'terminaM4A2': false,
      'terminaM4A3': false,
      'terminaM4A4': false,
      'terminaM4A5': false,
      'terminaM4A6': false,
      'terminaM4AT': false,
    });
    FirebaseFirestore.instance.collection('page-control').doc(uid).set({
      'M1A0': '1',
      'M1A1': '1',
      'M1A2': '1',
      'M1A3': '1',
      'M1A4': '1',
      'M1A5': '1',
      'M1AT': '1',
      'M2A1': '1',
      'M2A2': '1',
      'M2A3': '1',
      'M2A4': '1',
      'M2A5': '1',
      'M2A6': '1',
      'M2AT': '1',
      'M3A1': '1',
      'M3A2': '1',
      'M3A3': '1',
      'M3A4': '1',
      'M3A5': '1',
      'M3A6': '1',
      'M3A7': '1',
      'M3A8': '1',
      'M3AT': '1',
      'M4A1': '1',
      'M4A2': '1',
      'M4A3': '1',
      'M4A4': '1',
      'M4A5': '1',
      'M4A6': '1',
      'M4AT': '1',
      'A1': '1',
      'A2': '1',
      'A3': '1',
      'A4': '1',
      'A5': '1',
      'A6': '1',
      'A7': '1',
      'A8': '1',
      'A9': '1',
      'A10': '1',
      'A11': '1',
      'A12': '1',
      'A13': '1',
      'A14': '1',
      'A15': '1',
      'A16': '1',
      'A17': '1',
      'A18': '1',
      'A19': '1',
      'A20': '1',
      'A21': '1',
      'A22': '1',
      'A23': '1',
      'A24': '1',
      'A25': '1',
      'A26': '1',
      'A27': '1',
      'A28': '1',
      'A29': '1',
      'A30': '1',
      'A31': '1',
    });
    FirebaseFirestore.instance.collection('favoritos').doc(uid).set({
      'M1A0': false,
      'M1A1': false,
      'M1A2': false,
      'M1A3': false,
      'M1A4': false,
      'M1A5': false,
      'M1AT': false,
      'M2A1': false,
      'M2A2': false,
      'M2A3': false,
      'M2A4': false,
      'M2A5': false,
      'M2A6': false,
      'M2AT': false,
      'M3A1': false,
      'M3A2': false,
      'M3A3': false,
      'M3A4': false,
      'M3A5': false,
      'M3A6': false,
      'M3A7': false,
      'M3A8': false,
      'M3AT': false,
      'M4A1': false,
      'M4A2': false,
      'M4A3': false,
      'M4A4': false,
      'M4A5': false,
      'M4A6': false,
      'M4AT': false,
      'A1': false,
      'A2': false,
      'A3': false,
      'A4': false,
      'A5': false,
      'A6': false,
      'A7': false,
      'A8': false,
      'A9': false,
      'A10': false,
      'A11': false,
      'A12': false,
      'A13': false,
      'A14': false,
      'A15': false,
      'A16': false,
      'A17': false,
      'A18': false,
      'A19': false,
      'A20': false,
      'A21': false,
      'A22': false,
      'A23': false,
      'A24': false,
      'A25': false,
      'A26': false,
      'A27': false,
      'A28': false,
      'A29': false,
      'A30': false,
      'A31': false,
    });
  }

  Future guardarRespuestasM2A3(
    String am1,
    String am2,
    String am3,
    String fam1,
    String fam2,
    String fam3,
    String uni1,
    String uni2,
    String uni3,
    String com1,
    String com2,
    String com3,
  ) async {
    FirebaseFirestore.instance
        .collection('M2A3-respuestas')
        .doc(FirebaseAuth.instance.currentUser!.uid)
        .set({
      'amistad1': am1,
      'amistad2': am2,
      'amistad3': am3,
      'familia1': fam1,
      'familia2': fam2,
      'familia3': fam3,
      'universidad1': uni1,
      'universidad2': uni2,
      'universidad3': uni3,
      'comunidad1': com1,
      'comunidad2': com2,
      'comunidad3': com3,
    });
  }

  Future guardarRespuestasM3A5(
    String involucrados,
    String que,
    String donde,
    String cuando,
    String porque,
    String sentimientos,
    String pensamientos,
    String respuestas,
  ) async {
    FirebaseFirestore.instance
        .collection('M3A5-respuestas')
        .doc(FirebaseAuth.instance.currentUser!.uid)
        .set({
      'involucrados': involucrados,
      'que': que,
      'donde': donde,
      'cuando': cuando,
      'porque': porque,
      'sentimientos': sentimientos,
      'pensamientos': pensamientos,
      'respuestas': respuestas,
    });
  }

  Future updateRespuestasM3A5(String pregunta, String respuesta) async {
    FirebaseFirestore.instance
        .collection('M3A5-respuestas')
        .doc(FirebaseAuth.instance.currentUser!.uid)
        .update({
      pregunta: respuesta,
    });
  }

  Future guardarRespuestasM3A6(
    String solucion1,
    String solucion2,
    String solucion3,
    String solucion4,
    String solucion5,
  ) async {
    FirebaseFirestore.instance
        .collection('M3A6-respuestas')
        .doc(FirebaseAuth.instance.currentUser!.uid)
        .set({
      'solucion1': solucion1,
      'solucion2': solucion2,
      'solucion3': solucion3,
      'solucion4': solucion4,
      'solucion5': solucion5,
    });
  }

  Future guardarRespuestasM3A7(
    String alter1positiva,
    String alter1negativa,
    String alter2positiva,
    String alter2negativa,
  ) async {
    FirebaseFirestore.instance
        .collection('M3A7-respuestas')
        .doc(FirebaseAuth.instance.currentUser!.uid)
        .update({
      'alter1positiva': alter1positiva,
      'alter1negativa': alter1negativa,
      'alter2positiva': alter2positiva,
      'alter2negativa': alter2negativa,
    });
  }

  Future alternativasSeleccionadasM3A7(
    String alter1seleccionada,
    String alter2seleccionada,
  ) async {
    FirebaseFirestore.instance
        .collection('M3A7-respuestas')
        .doc(FirebaseAuth.instance.currentUser!.uid)
        .set({
      'alternativa1': alter1seleccionada,
      'alternativa2': alter2seleccionada,
    });
  }

  Future alternativasFinalSeleccionadasM3A7(
    String alterseleccionada,
  ) async {
    FirebaseFirestore.instance
        .collection('M3A7-respuestas')
        .doc(FirebaseAuth.instance.currentUser!.uid)
        .update({
      'alternativaFinalSeleccionada': alterseleccionada,
    });
  }

  Future guardarRespuestasM4A3(
    String situacion1evitar,
    String situacion1que,
    String situacion2evitar,
    String situacion2que,
    String situacion3evitar,
    String situacion3que,
  ) async {
    FirebaseFirestore.instance
        .collection('M4A3-respuestas')
        .doc(FirebaseAuth.instance.currentUser!.uid)
        .set({
      'situacion1evitar': situacion1evitar,
      'situacion1que': situacion1que,
      'situacion2evitar': situacion2evitar,
      'situacion2que': situacion2que,
      'situacion3evitar': situacion3evitar,
      'situacion3que': situacion3que,
    });
  }

  Future guardarRespuestasM4A4(
    String alternativa1,
    String alternativa2,
    String alternativa3,
  ) async {
    FirebaseFirestore.instance
        .collection('M4A4-respuestas')
        .doc(FirebaseAuth.instance.currentUser!.uid)
        .set({
      'alternativa1': alternativa1,
      'alternativa2': alternativa2,
      'alternativa3': alternativa3,
    });
  }

  Future guardarRespuestasM4A5(
    String situacion1,
    String situacion2,
    String situacion3,
    String situacion4,
    String situacion5,
    String situacion6,
    String situacion7,
  ) async {
    FirebaseFirestore.instance
        .collection('M4A5-respuestas')
        .doc(FirebaseAuth.instance.currentUser!.uid)
        .set({
      'situacion1': situacion1,
      'situacion2': situacion2,
      'situacion3': situacion3,
      'situacion4': situacion4,
      'situacion5': situacion5,
      'situacion6': situacion6,
      'situacion7': situacion7,
    });
  }

  Future guardarRespuestasA3(String situacion, String diario) async {
    FirebaseFirestore.instance
        .collection('A3-diario-procastinacion')
        .doc(FirebaseAuth.instance.currentUser!.uid)
        .set({
      'situacion': situacion,
      'diario': diario,
    });
  }

  Future guardarRespuestasA6(
      String mandar,
      String amenazar,
      String sermonear,
      String lecciones,
      String aconsejar,
      String consolar,
      String aprobar,
      String desaprobar,
      String insultar,
      String interpretar,
      String interrogar,
      String ironizar) async {
    FirebaseFirestore.instance
        .collection('A6-resolviendo-conflictos-respuestas')
        .doc(FirebaseAuth.instance.currentUser!.uid)
        .set({
      'mandar': mandar,
      'amenazar': amenazar,
      'sermonear': sermonear,
      'lecciones': lecciones,
      'aconsejar': aconsejar,
      'consolar': consolar,
      'aprobar': aprobar,
      'desaprobar': desaprobar,
      'insultar': insultar,
      'interpretar': interpretar,
      'interrogar': interrogar,
      'ironizar': ironizar,
    });
  }

  Future guardarRespuestasA8(
      String horario,
      String siestas,
      String bebidas,
      String comer,
      String ejercicio,
      String relajacion,
      String ropa,
      String dormitorio,
      String pantallas,
      String medicamentos) async {
    FirebaseFirestore.instance
        .collection('A8-buen-dormir-respuestas')
        .doc(FirebaseAuth.instance.currentUser!.uid)
        .set({
      'HorarioRegular': horario,
      'SiestasRegulares': siestas,
      'BebidasEstimulantes': bebidas,
      'ComerMucho': comer,
      'Ejercicio': ejercicio,
      'ActividadesRelajantes': relajacion,
      'RopaComoda': ropa,
      'DormitorioAcogedor': dormitorio,
      'Pantallas': pantallas,
      'Medicamentos': medicamentos
    });
  }

  Future guardarRespuestasPlanSeguridad(
      String razones, String calmarme, String contactos, String apoyo) async {
    FirebaseFirestore.instance
        .collection('plan-seguridad-respuestas')
        .doc(FirebaseAuth.instance.currentUser!.uid)
        .set({
      'razones_vivir': razones,
      'que_hacer_calmarme': calmarme,
      'contactos': contactos,
      'apoyo_profesional': apoyo,
    });
  }

  Future updateCurrentPage(String modulo, String page) async {
    FirebaseFirestore.instance
        .collection('page-control')
        .doc(FirebaseAuth.instance.currentUser!.uid)
        .update({
      modulo: page,
    });
  }

  Future guardarRespuestasM4A2(
    String objetivo,
    String igualBeneficios,
    String igualCostos,
    String cambioBeneficios,
    String cambioCostos,
  ) async {
    FirebaseFirestore.instance
        .collection('M4A2-respuestas')
        .doc(FirebaseAuth.instance.currentUser!.uid)
        .set({
      'objetivo': objetivo,
      'igualBeneficios': igualBeneficios,
      'igualCostos': igualCostos,
      'cambioBeneficios': cambioBeneficios,
      'cambioCostos': cambioCostos,
    });
  }

  Future agregarPsicologo(String nombre, String apellido, String? uid) async {
    FirebaseFirestore.instance.collection('psicologos').doc(uid).update({
      'nombre': nombre,
      'apellidos': apellido,
    });
  }

  Future updateTokenUsuario(String tokenDevice) async {
    FirebaseFirestore.instance
        .collection('usuarios')
        .doc(FirebaseAuth.instance.currentUser!.uid)
        .update({
      'pushToken': tokenDevice,
    });
  }

  Future terminaSesion(String modulo, String actividad, String pasos) async {
    String moduloActual = 'M' + modulo;
    String actividadActual = 'A' + actividad;
    String sesion = moduloActual + actividadActual;
    FirebaseFirestore.instance
        .collection('datos')
        .doc(
          FirebaseAuth.instance.currentUser!.uid,
        )
        .update({
      sesion: pasos,
    });
    FirebaseFirestore.instance
        .collection('control')
        .doc(
          FirebaseAuth.instance.currentUser!.uid,
        )
        .update({
      'termina' + sesion: true,
    });
  }

  Future setFavorito(String modulo, bool value) async {
    FirebaseFirestore.instance
        .collection('favoritos')
        .doc(FirebaseAuth.instance.currentUser!.uid)
        .update({
      modulo: value,
    });
  }

  Future setTermometro(
    String modulo,
    int value,
  ) async {
    String time = DateFormat('dd-MM-yyy HH:mm').format(DateTime.now());
    FirebaseFirestore.instance
        .collection('termometros')
        .doc(FirebaseAuth.instance.currentUser!.uid)
        .collection('estados')
        .doc(DateTime.now().toString())
        .set({
      'hora': time,
      'estado': value,
    });
  }

  Future setTime(
      String moduloActividadTiempo, String accion, String diferencia) async {
    FirebaseFirestore.instance
        .collection('tiempos')
        .doc(FirebaseAuth.instance.currentUser!.uid)
        .collection('Historial')
        .doc(moduloActividadTiempo)
        .set({
      'accion': accion,
      'tiempo': diferencia,
    });
  }

  Future updateLoginSemana(int semanaNumero) async {
    var db = await FirebaseFirestore.instance
        .collection('ingresos')
        .doc(FirebaseAuth.instance.currentUser!.uid)
        .collection('Semanas')
        .doc(semanaNumero.toString())
        .get();
    if (!db.exists) {
      FirebaseFirestore.instance
          .collection('ingresos')
          .doc(FirebaseAuth.instance.currentUser!.uid)
          .collection('Semanas')
          .doc(semanaNumero.toString())
          .set({
        'ingresos': 0,
      }).then((value) => null);
      FirebaseFirestore.instance
          .collection('ingresos')
          .doc(FirebaseAuth.instance.currentUser!.uid)
          .collection('Semanas')
          .doc(semanaNumero.toString())
          .update({"ingresos": FieldValue.increment(1)});
    } else {
      FirebaseFirestore.instance
          .collection('ingresos')
          .doc(FirebaseAuth.instance.currentUser!.uid)
          .collection('Semanas')
          .doc(semanaNumero.toString())
          .update({"ingresos": FieldValue.increment(1)});
    }
  }

  Future updateNotificationSettings(
      bool recibir,
      int hora,
      bool lunes,
      bool martes,
      bool miercoles,
      bool jueves,
      bool viernes,
      bool sabado,
      bool domingo) async {
    FirebaseFirestore.instance
        .collection('usuarios')
        .doc(FirebaseAuth.instance.currentUser!.uid)
        .update({
      'receiveNots': recibir,
      'time': hora,
      'days': [lunes, martes, miercoles, jueves, viernes, sabado, domingo],
    });
  }

  Future updateLastTermometroValue(
    String uid,
    int termometroValue,
  ) async {
    FirebaseFirestore.instance
        .collection('usuarios')
        .doc(FirebaseAuth.instance.currentUser!.uid)
        .update({
      'termometro': termometroValue,
    });
  }

  Future updateEstadoNivel(String uid, String estado, String alerta) async {
    FirebaseFirestore.instance.collection('usuarios').doc(uid).update({
      'estadoAtencion': estado,
      'alerta': alerta,
    });
  }

  Future updateEstadoAtencionAutomatico(String uid) async {
    FirebaseFirestore.instance.collection('usuarios').doc(uid).update({
      'estadoAtencion': 'En curso',
    });
  }

  Future setCambiosSeguimientoPsicologo(
      String userID, String cambio, String flag) async {
    String time = DateFormat('dd-MM-yyy').format(DateTime.now());
    switch (flag) {
      case 'E':
        return FirebaseFirestore.instance
            .collection('psicologos')
            .doc(FirebaseAuth.instance.currentUser!.uid)
            .collection('ajustes')
            .doc(DateTime.now().toString())
            .set(
                {'hora': time, 'user': userID, 'cambio': cambio, 'flag': flag});
      case 'N':
        return FirebaseFirestore.instance
            .collection('psicologos')
            .doc(FirebaseAuth.instance.currentUser!.uid)
            .collection('ajustes')
            .doc(DateTime.now().toString())
            .set(
                {'hora': time, 'user': userID, 'cambio': cambio, 'flag': flag});
      default:
        return null;
    }
  }
}
