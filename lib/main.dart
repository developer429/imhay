// ignore_for_file: unused_local_variable, avoid_print

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:seguimiento/chatlogics/models_and_others/chat_provider.dart';
import 'package:seguimiento/chatlogics/models_and_others/chat_home_provider.dart';
import 'package:seguimiento/colors.dart';
import 'package:seguimiento/firebase.dart';
import 'package:seguimiento/notifications_cloud.dart';
import 'package:seguimiento/psicologo_pages/iniciopsi.dart';
import 'package:seguimiento/user_pages/MainPage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:week_of_year/week_of_year.dart';
import 'chatlogics/chat_page.dart';
import 'screens/ejercicio_intro/introejercicio.dart';
import 'screens/iniciar_sesion.dart';
import 'screens/tour.dart';

Future _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  print("Handling a background message: ${message.messageId}");
}

/*Aqui termina lo de esta clase */

void main() async {
  //secures app is initialized
  WidgetsFlutterBinding.ensureInitialized();
  //makes app full screen
  SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  if (kIsWeb) {
    await Firebase.initializeApp(
        options: const FirebaseOptions(
            apiKey: "AIzaSyCLWYGI-5verfLe7BRT3uUCo0cFcMxlz30",
            authDomain: "maquetadatos-5dee1.firebaseapp.com",
            projectId: "maquetadatos-5dee1",
            storageBucket: "maquetadatos-5dee1.appspot.com",
            messagingSenderId: "616126449554",
            appId: "1:616126449554:web:e8ec8297fc8e2362fee34b",
            measurementId: "G-5X969RDC1K"));
  } else {
    await Firebase.initializeApp();
  }

  SharedPreferences prefs = await SharedPreferences.getInstance();
  runApp(MyApp(prefs: prefs));
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key, required this.prefs}) : super(key: key);
  final SharedPreferences prefs;

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final FirebaseFirestore firebaseFirestore = FirebaseFirestore.instance;

  PushNotification? _notificationInfo;
  late final FirebaseMessaging _messaging;

  void registerNotification() async {
    await Firebase.initializeApp();

    _messaging = FirebaseMessaging.instance;

    FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

    NotificationSettings settings = await _messaging.requestPermission(
      alert: true,
      badge: true,
      provisional: false,
      sound: true,
    );

    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      //print('User granted permission');

      FirebaseMessaging.onMessage.listen((RemoteMessage message) {
        PushNotification notification = PushNotification(
          title: message.notification?.title,
          body: message.notification?.body,
          dataId: message.data['idE'],
          dataNombre: message.data['nombre'],
        );

        setState(() {
          _notificationInfo = notification;
        });
      });
    } else {
      //print('User declined permission');
    }
  }

  checkForInitialMessage() async {
    await Firebase.initializeApp();
    RemoteMessage? initialMessage =
        await FirebaseMessaging.instance.getInitialMessage();

    if (initialMessage != null) {
      PushNotification notification = PushNotification(
        title: initialMessage.notification?.title,
        body: initialMessage.notification?.body,
        dataId: initialMessage.data['idE'],
        dataNombre: initialMessage.data['nombre'],
      );
      setState(() {
        _notificationInfo = notification;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    registerNotification();
    checkForInitialMessage();
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      PushNotification notification = PushNotification(
          title: message.notification?.title,
          body: message.notification?.body,
          dataId: message.data['idE'],
          dataNombre: message.data['nombre']);

      setState(() {
        final _notificationInfo = notification;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider<ChatPageProvider>(
          create: (_) => ChatPageProvider(
            firebaseFirestore: firebaseFirestore,
          ),
        ),
        Provider<ChatProvider>(
          create: (_) => ChatProvider(
            prefs: widget.prefs,
            firebaseFirestore: firebaseFirestore,
            //firebaseStorage: firebaseStorage,
          ),
        ),
      ],
      child: MaterialApp(
        title: 'Vamos Juntxs',
        builder: EasyLoading.init(),
        debugShowCheckedModeBanner: false,
        theme: ThemeData(fontFamily: fontFamilyApp),
        home: getLandingPage(_notificationInfo?.dataNombre,
            _notificationInfo?.dataId, _notificationInfo?.userType),
      ),
    );
  }
}

Widget getLandingPage(String? nombreUsuario, String? dataId, String? userType) {
  final FirebaseMessaging firebaseMessaging = FirebaseMessaging.instance;

  if (firebaseAuth.currentUser == null) {
    return const MyHomePage();
  } else {
    var firebaseUser = firebaseAuth.currentUser!;
    String authType = firebaseUser.providerData[0].providerId;
    //Cuando es psicologo
    if (authType == 'password') {
      firebaseMessaging.requestPermission();

      firebaseMessaging.getToken().then((token) {
        if (token != null) {
          NotificationService().sendTokenPsicologo(firebaseUser.uid, token);
        }
      });
      if (nombreUsuario != null) {
        return ChatPage(
          isVistaDetails: false,
          isPsicologo: true,
          arguments: ChatPageArguments(
            peerId: dataId!,
            peerNickname: nombreUsuario,
          ),
        );
      } else {
        return const InicioPsicologo();
      }
    }
    //Cuando es un paciente
    else {
      firebaseMessaging.requestPermission();

      firebaseMessaging.getToken().then((token) {
        if (token != null) {
          FirestoreService().updateTokenUsuario(token);
        }
      });
      if (nombreUsuario != null) {
        return ChatPage(
          isPsicologo: false,
          isVistaDetails: false,
          arguments: ChatPageArguments(
            peerId: dataId!,
            peerNickname: nombreUsuario,
          ),
        );
      } else {
        var date = DateTime.now();
        FirestoreService().updateLoginSemana(date.weekOfYear);
        return const MainPage(
          activePage: 0,
        );
      }
    }
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({
    Key? key,
  }) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  ScrollController scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: CustomColors.greenList,
      body: Center(
        child: SingleChildScrollView(
          controller: scrollController,
          child: Column(
            children: [
              Container(
                margin: const EdgeInsets.all(10),
                child: const Image(
                  image: AssetImage('assets/images/logo-inicio.png'),
                ),
              ),
              Container(
                margin: const EdgeInsets.all(10),
                child: Column(
                  children: <Widget>[
                    ListTile(
                      title: const Text(
                        'Te damos la bienvenida',
                        textScaler: TextScaler.linear(1.0),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.bold,
                            color: CustomColors.primaryDark),
                      ),
                      subtitle: Container(
                        margin: const EdgeInsets.only(top: 15),
                        child: const Text(
                          'Sabemos que la vida universitaria puede ser difícil, por eso en esta app te ofrecemos herramientas para promover tu bienestar y prevenir problemas de salud mental',
                          textAlign: TextAlign.center,
                          textScaler: TextScaler.linear(1.0),
                          style: TextStyle(
                              fontSize: 17,
                              color: CustomColors.purpleDark,
                              height: 1.5),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 20, bottom: 10),
                width: size.width / 1.1,
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        WidgetStateProperty.all<Color>(CustomColors.background),
                    shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                      ),
                    ),
                  ),
                  onPressed: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const IntroEjercicio()),
                    ),
                  ),
                  child: const Text(
                    'Hacer un ejercicio de conexión con el presente',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(color: CustomColors.primary),
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 10),
                width: size.width / 1.1,
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        WidgetStateProperty.all<Color>(CustomColors.primary),
                    shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                      ),
                    ),
                  ),
                  onPressed: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const Tour()),
                    ),
                  ),
                  child: const Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.arrow_forward,
                        color: Colors.white,
                      ),
                      Text(
                        '  Comienza el tour por la app',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(bottom: 20, left: 20, right: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text(
                      '¿Ya tienes una cuenta?',
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(fontSize: 15),
                    ),
                    TextButton(
                      style: ButtonStyle(
                        foregroundColor: WidgetStateProperty.all<Color>(
                            CustomColors.primary),
                      ),
                      onPressed: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: ((context) => const Sesion()),
                        ),
                      ),
                      child: const Text(
                        'Inicia sesión',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            decoration: TextDecoration.underline,
                            fontSize: 15,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
