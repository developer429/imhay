// ignore_for_file: non_constant_identifier_names, avoid_print

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:seguimiento/chatlogics/chat_page.dart';
import 'package:seguimiento/chatlogics/models_and_others/chat_home_provider.dart';
import 'package:seguimiento/chatlogics/models_and_others/firestore_constants.dart';
import 'package:seguimiento/colors.dart';
import 'package:seguimiento/firebase.dart';
import 'package:seguimiento/psicologo_pages/M2A3-respuestas.dart';
import 'package:seguimiento/psicologo_pages/M3A6-respuestas.dart';
import 'package:seguimiento/psicologo_pages/M4A2-respuestas.dart';
import 'package:seguimiento/psicologo_pages/M4A3-respuestas.dart';
import 'package:seguimiento/psicologo_pages/M4A4-respuestas.dart';
import 'package:seguimiento/psicologo_pages/M4A5-respuestas.dart';

import 'M3A5-respuestas.dart';
import 'M3A7-respuestas.dart';

class InfoPaciente extends StatefulWidget {
  const InfoPaciente({
    Key? key,
    required this.userID,
    required this.indexStart,
  }) : super(key: key);

  final String userID;
  final int indexStart;
  @override
  State<InfoPaciente> createState() => _InfoPacienteState();
}

class _InfoPacienteState extends State<InfoPaciente> {
  bool M1A1done = false;
  bool M1A2done = false;
  bool M1A3done = false;
  bool M1A4done = false;
  bool M1A5done = false;
  bool M1A0done = false;
  bool M1ATdone = false;

  bool M2A1done = false;
  bool M2A2done = false;
  bool M2A3done = false;
  bool M2A4done = false;
  bool M2A5done = false;
  bool M2A6done = false;
  bool M2ATdone = false;

  bool M3A1done = false;
  bool M3A2done = false;
  bool M3A3done = false;
  bool M3A4done = false;
  bool M3A5done = false;
  bool M3A6done = false;
  bool M3A7done = false;
  bool M3A8done = false;
  bool M3ATdone = false;

  bool M4A1done = false;
  bool M4A2done = false;
  bool M4A3done = false;
  bool M4A4done = false;
  bool M4A5done = false;
  bool M4A6done = false;
  bool M4ATdone = false;
  String porcentajeCompletado = '0%';
  String porcentajeActual(String modulo) {
    switch (modulo) {
      case 'm1a0':
        return '3%';
      case 'm1a1':
        return '7%';
      case 'm1a2':
        return '11%';
      case 'm1a3':
        return '14%';
      case 'm1a4':
        return '17%';
      case 'm1a5':
        return '21%';
      case 'm1at':
        return '25%';
      case 'm2a1':
        return '29%';
      case 'm2a2':
        return '32%';
      case 'm2a3':
        return '35%';
      case 'm2a4':
        return '39%';
      case 'm2a5':
        return '43%';
      case 'm2a6':
        return '46%';
      case 'm2at':
        return '50%';

      case 'm3a1':
        return '53%';
      case 'm3a2':
        return '56%';
      case 'm3a3':
        return '58%';
      case 'm3a4':
        return '61%';
      case 'm3a5':
        return '64%';
      case 'm3a6':
        return '67%';
      case 'm3a7':
        return '70%';
      case 'm3a8':
        return '73%';
      case 'm3at':
        return '75%';

      case 'm4a1':
        return '78%';
      case 'm4a2':
        return '82%';
      case 'm4a3':
        return '85%';
      case 'm4a4':
        return '89%';
      case 'm4a5':
        return '92%';
      case 'm4a6':
        return '96%';
      case 'm4at':
        return '100%';
      default:
        return '0%';
    }
  }

  String cambioEstado = '';
  String cambioAlerta = '';

  List<String> estadosAtencion = <String>[
    'En seguimiento',
    'En derivación',
    'Mensaje programado',
    'Cerrado/Resuelto',
  ].toList();
  List<String> nivelRiesgo = <String>['Alto', 'Moderado', 'Leve'].toList();

  late String _selectionEstados;
  late String _selectionNiveles;
  late ChatPageProvider chatHomeProvider;

  List<String> horasT = [];
  List<int> estadosT = [];

  bool loadingData = true;
  String alertaUser = '';
  String atencionUser = '';
  String nombreUser = '';
  String rutUser = '';
  String pronombreUser = '';

  CollectionReference userReference =
      FirebaseFirestore.instance.collection('usuarios');
  void getUserData() {
    userReference.doc(widget.userID).get().then((value) {
      var fields = value;
      setState(() {
        nombreUser = fields['nombreCompleto'];
        rutUser = fields['rut'];
        alertaUser = fields['alerta'];
        atencionUser = fields['estadoAtencion'];
        pronombreUser = fields['pronombre'];
        Future.delayed(const Duration(milliseconds: 500), () {
          setState(() {
            loadingData = false;
          });
        });
      });
    });
  }

  CollectionReference termometroReference =
      FirebaseFirestore.instance.collection('termometros');
  void getTermo() {
    termometroReference
        .doc(widget.userID)
        .collection('estados')
        .get()
        .then((value) {
      var fields = value;
      setState(() {
        for (var data in fields.docs) {
          horasT.add(data['hora']);
          estadosT.add(data['estado']);
        }
      });
    });
  }

  List<int> r1Values = [];
  List<int> r2Values = [];
  List<int> r3Values = [];
  List<int> r4Values = [];
  List<int> r5Values = [];
  List<Timestamp> fechaPHQ4 = [];
  List<String> alertaPQH4 = [];
  List<String> bodyPQH4 = [];
  List<String> subBodyPQH4 = [];

  CollectionReference phq4Ref = FirebaseFirestore.instance.collection('phq4');
  void getPHQ4() {
    phq4Ref.doc(widget.userID).collection('respuestas').get().then((value) {
      var fields = value;
      setState(() {
        for (var data in fields.docs) {
          r1Values.add(data['r1']);
          r2Values.add(data['r2']);
          r3Values.add(data['r3']);
          r4Values.add(data['r4']);
          r5Values.add(data['r5']);
          fechaPHQ4.add(data['fecha']);
          alertaPQH4.add(data['alerta']);
          bodyPQH4.add(data['body']);
          subBodyPQH4.add(data['sub_body']);
        }
      });
    });
  }

  List<String> cambios = [];
  List<String> flags = [];
  List<String> horasAjustes = [];
  List<String> users = [];
  CollectionReference ajustesPsiReference =
      FirebaseFirestore.instance.collection('psicologos');
  void getAjustesPsi() {
    ajustesPsiReference
        .doc(FirebaseAuth.instance.currentUser!.uid)
        .collection('ajustes')
        .get()
        .then((value) {
      var fields = value;
      setState(() {
        for (var data in fields.docs) {
          cambios.add(data['cambio']);
          flags.add(data['flag']);
          horasAjustes.add(data['hora']);
          users.add(data['user']);
        }
      });
    });
  }

  @override
  void initState() {
    super.initState();
    getTermo();
    getAjustesPsi();
    getPHQ4();
    getUserData();
    _selectionEstados = estadosAtencion.first;
    _selectionNiveles = nivelRiesgo.first;

    chatHomeProvider = context.read<ChatPageProvider>();
  }

  IconData IconNivelRiesgo(String seleccion) {
    switch (seleccion) {
      case 'Alto':
        return MdiIcons.emoticonSadOutline;

      case 'Moderado':
        return MdiIcons.emoticonNeutralOutline;

      case 'Leve':
        return MdiIcons.emoticonHappyOutline;

      default:
        return MdiIcons.tagFaces;
    }
  }

  Color ColorNivelRiesgo(String seleccion) {
    switch (seleccion) {
      case 'Alto':
        return Colors.red;

      case 'Moderado':
        return Colors.orange;

      case 'Leve':
        return Colors.green;

      default:
        return Colors.black;
    }
  }

  String nivelAlerta(String alerta) {
    switch (alerta) {
      case 'Alto':
        return 'rojo';
      case 'Moderado':
        return 'amarillo';
      case 'Leve':
        return 'verde';
      default:
        return '';
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return loadingData
        ? Container(
            width: size.width,
            height: size.height,
            color: CustomColors.primaryDark,
            child: const Center(
                child: CircularProgressIndicator(
              color: CustomColors.tertiary70,
            )))
        : DefaultTabController(
            initialIndex: widget.indexStart,
            length: 3,
            child: Container(
              color: Colors.black,
              child: SafeArea(
                child: Scaffold(
                  backgroundColor: CustomColors.purplePrograma,
                  appBar: AppBar(
                    toolbarHeight: size.height / 10,
                    backgroundColor: CustomColors.purplePsicologo,
                    elevation: 0,
                    bottom: const TabBar(
                      dividerHeight: 0,
                      indicatorColor: CustomColors.primary80pink,
                      tabs: [
                        Tab(
                          child: Text(
                            'Programa',
                            textScaler: TextScaler.linear(1.0),
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ),
                        Tab(
                          child: Text(
                            '     Chat     ',
                            textScaler: TextScaler.linear(1.0),
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ),
                        Tab(
                          child: Text(
                            'Reportes',
                            textScaler: TextScaler.linear(1.0),
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ],
                    ),
                    title: RichText(
                      text: TextSpan(
                        text: nombreUser + '\n',
                        style: const TextStyle(
                          fontSize: 18,
                          color: CustomColors.primary80pink,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text: atencionUser,
                            style: const TextStyle(
                                fontSize: 12,
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                    iconTheme: const IconThemeData(
                      color: Colors.white, //change your color here
                    ),
                    actions: [
                      IconButton(
                        icon: Icon(MdiIcons.pencilOutline),
                        onPressed: () {
                          showDialog(
                            context: context,
                            builder: (context) => Center(
                              child: SizedBox(
                                height: size.height / 2,
                                child: AlertDialog(
                                  shape: const RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(32.0))),
                                  title: const Text(
                                    'Ajustes',
                                    textScaler: TextScaler.linear(1.0),
                                    style: TextStyle(
                                        color: CustomColors.primaryDark,
                                        fontSize: 18),
                                  ),
                                  contentPadding: const EdgeInsets.all(10),
                                  content: StatefulBuilder(
                                    builder: (BuildContext context, setState) {
                                      return Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Row(
                                            children: [
                                              Container(
                                                  margin: const EdgeInsets.only(
                                                      left: 10, right: 10),
                                                  child: Icon(
                                                    MdiIcons.security,
                                                    color: CustomColors.primary,
                                                  )),
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  const Text(
                                                    'Estado de atención',
                                                    textScaler:
                                                        TextScaler.linear(1.0),
                                                    style: TextStyle(
                                                      color: CustomColors
                                                          .primaryDark,
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width: 200,
                                                    child:
                                                        DropdownButton<String>(
                                                      underline: Container(
                                                        height: 0,
                                                        color:
                                                            Colors.transparent,
                                                      ),
                                                      isExpanded: true,
                                                      value: _selectionEstados,
                                                      elevation: 16,
                                                      style: const TextStyle(
                                                          color: Colors.black),
                                                      isDense: true,
                                                      onChanged: (newvalue) {
                                                        // This is called when the user selects an item.
                                                        setState(() {
                                                          _selectionEstados =
                                                              newvalue!;
                                                        });
                                                      },
                                                      items: estadosAtencion
                                                          .map((String value) {
                                                        return DropdownMenuItem<
                                                            String>(
                                                          value: value,
                                                          child: Text(
                                                            value,
                                                            textScaler:
                                                                const TextScaler
                                                                    .linear(
                                                                    1.0),
                                                          ),
                                                        );
                                                      }).toList(),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                          const Divider(),
                                          Row(
                                            children: [
                                              Container(
                                                  margin: const EdgeInsets.only(
                                                      left: 10, right: 10),
                                                  child: Icon(
                                                      IconNivelRiesgo(
                                                          _selectionNiveles),
                                                      color: ColorNivelRiesgo(
                                                          _selectionNiveles))),
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  const Text(
                                                    'Nivel de riesgo',
                                                    textScaler:
                                                        TextScaler.linear(1.0),
                                                    style: TextStyle(
                                                      color: CustomColors
                                                          .primaryDark,
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width: 200,
                                                    child:
                                                        DropdownButton<String>(
                                                      underline: Container(
                                                        height: 0,
                                                        color:
                                                            Colors.transparent,
                                                      ),
                                                      isExpanded: true,
                                                      value: _selectionNiveles,
                                                      elevation: 16,
                                                      style: const TextStyle(
                                                          color: Colors.black),
                                                      isDense: true,
                                                      onChanged: (newvalue) {
                                                        // This is called when the user selects an item.
                                                        setState(() {
                                                          _selectionNiveles =
                                                              newvalue!;
                                                        });
                                                      },
                                                      items: nivelRiesgo
                                                          .map((String value) {
                                                        return DropdownMenuItem<
                                                            String>(
                                                          value: value,
                                                          child: Text(
                                                            value,
                                                            textScaler:
                                                                const TextScaler
                                                                    .linear(
                                                                    1.0),
                                                          ),
                                                        );
                                                      }).toList(),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                          const Divider(),
                                        ],
                                      );
                                    },
                                  ),
                                  actions: [
                                    TextButton(
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      },
                                      child: const Text(
                                        'Cerrar',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            color: CustomColors.primary),
                                      ),
                                    ),
                                    ElevatedButton(
                                        style: ButtonStyle(
                                          backgroundColor:
                                              WidgetStateProperty.all<Color>(
                                                  CustomColors.primary),
                                        ),
                                        onPressed: () {
                                          if (_selectionEstados !=
                                              atencionUser) {
                                            FirestoreService()
                                                .setCambiosSeguimientoPsicologo(
                                                    widget.userID,
                                                    _selectionEstados,
                                                    'E');
                                          }
                                          if (nivelAlerta(_selectionNiveles) !=
                                              alertaUser) {
                                            FirestoreService()
                                                .setCambiosSeguimientoPsicologo(
                                                    widget.userID,
                                                    nivelAlerta(
                                                        _selectionNiveles),
                                                    'N');
                                          }

                                          FirestoreService().updateEstadoNivel(
                                              widget.userID,
                                              _selectionEstados,
                                              nivelAlerta(_selectionNiveles));
                                          Navigator.of(context).pop();
                                        },
                                        child: const Text(
                                          'Guardar cambios',
                                          textScaler: TextScaler.linear(1.0),
                                          style: TextStyle(color: Colors.white),
                                        ))
                                  ],
                                ),
                              ),
                            ),
                          );
                        },
                      ),
                      IconButton(
                        icon: Icon(MdiIcons.informationOutline),
                        onPressed: () {
                          showDialog(
                            context: context,
                            builder: (context) => Center(
                              child: SizedBox(
                                height: size.height / 1.8,
                                child: AlertDialog(
                                  shape: const RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(32.0))),
                                  title: Text(
                                    nombreUser,
                                    textScaler: const TextScaler.linear(1.0),
                                    style: const TextStyle(
                                        color: CustomColors.primaryDark,
                                        fontSize: 18),
                                  ),
                                  content: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      const Text(
                                        'RUT',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            color: CustomColors.primaryDark),
                                      ),
                                      Text(
                                        rutUser,
                                        textScaler:
                                            const TextScaler.linear(1.0),
                                        style: const TextStyle(
                                            color: CustomColors.primaryDark,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Container(
                                          margin:
                                              const EdgeInsets.only(bottom: 20),
                                          child: const Divider()),
                                      const Text(
                                        'Pronombre',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            color: CustomColors.primaryDark),
                                      ),
                                      Text(
                                        pronombreUser,
                                        textScaler:
                                            const TextScaler.linear(1.0),
                                        style: const TextStyle(
                                            color: CustomColors.primaryDark,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      const Divider(),
                                    ],
                                  ),
                                  actions: [
                                    TextButton(
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      },
                                      child: const Text(
                                        'Cerrar',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            color: CustomColors.primary),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        },
                      ),
                    ],
                  ),
                  body: TabBarView(
                    children: [
                      avancePrograma(),
                      ChatPage(
                        isPsicologo: true,
                        isVistaDetails: true,
                        arguments: ChatPageArguments(
                          peerId: widget.userID,
                          peerNickname: nombreUser,
                        ),
                      ),
                      reporteUser(),
                    ],
                  ),
                ),
              ),
            ),
          );
  }

  Widget reporteUser() {
    String tituloAjustes = 'Ajustado por psicólogo/a';
    String cambioRealizado = 'leve';
    return Container(
      margin: const EdgeInsets.only(left: 5, right: 5),
      child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              margin: const EdgeInsets.only(
                top: 10,
                bottom: 10,
              ),
              child: RichText(
                text: const TextSpan(
                  text: 'Resultados PHQ4',
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: CustomColors.surface,
                  ),
                ),
              ),
            ),
            bodyPQH4.isEmpty
                ? Center(
                    child: Container(
                      margin: const EdgeInsets.only(top: 20, bottom: 20),
                      child: const Text(
                        'Sin datos recolectados',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: CustomColors.orange3,
                            fontSize: 16),
                      ),
                    ),
                  )
                : ListView.separated(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: fechaPHQ4.length,
                    itemBuilder: (BuildContext context, int index) {
                      return ListTile(
                        leading: Image(
                          fit: BoxFit.scaleDown,
                          image: AssetImage(statePHQ4Icon(alertaPQH4[index])),
                        ),
                        title: const Text(
                          'PHQ4',
                          textScaler: TextScaler.linear(1.0),
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: CustomColors.background,
                              fontSize: 12),
                        ),
                        subtitle: RichText(
                          text: TextSpan(
                            text: bodyPQH4[index].toString() + '\n',
                            style: const TextStyle(
                              fontSize: 16,
                              color: CustomColors.surface,
                              height: 1.5,
                            ),
                            children: <TextSpan>[
                              TextSpan(
                                text: subBodyPQH4[index],
                                style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                    fontSize: 17),
                              ),
                            ],
                          ),
                        ),
                        trailing: Text(
                          DateFormat('dd-MM-yyyy')
                              .format(fechaPHQ4[index].toDate()),
                          textScaler: const TextScaler.linear(1.0),
                          style: const TextStyle(
                              color: CustomColors.surface,
                              fontWeight: FontWeight.bold,
                              fontSize: 12),
                        ),
                      );
                    },
                    separatorBuilder: (BuildContext context, int index) =>
                        const Divider(
                      color: CustomColors.primary,
                    ),
                  ),
            Container(
              margin: const EdgeInsets.only(top: 10, bottom: 10),
              child: const Divider(
                color: CustomColors.background,
              ),
            ),
            Container(
              margin: const EdgeInsets.only(
                bottom: 10,
              ),
              child: RichText(
                text: const TextSpan(
                  text: 'Termómetro',
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: CustomColors.surface,
                  ),
                ),
              ),
            ),
            estadosT.isEmpty
                ? Center(
                    child: Container(
                      margin: const EdgeInsets.only(top: 20, bottom: 20),
                      child: const Text(
                        'Sin datos recolectados',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: CustomColors.orange3,
                            fontSize: 16),
                      ),
                    ),
                  )
                : ListView.separated(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: estadosT.length,
                    itemBuilder: (BuildContext context, int index) {
                      return ListTile(
                        leading: Image(
                          fit: BoxFit.scaleDown,
                          image: AssetImage(stateIcon(estadosT[index])),
                        ),
                        title: const Text(
                          'Termómetro del ánimo',
                          textScaler: TextScaler.linear(1.0),
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: CustomColors.background,
                              fontSize: 12),
                        ),
                        subtitle: Text(
                          estadosT[index].toString() +
                              '. ' +
                              stateHint(
                                estadosT[index],
                              ),
                          textScaler: const TextScaler.linear(1.0),
                          style: const TextStyle(
                              color: CustomColors.surface, fontSize: 16),
                        ),
                        trailing: Text(
                          horasT[index].split(' ').first,
                          textScaler: const TextScaler.linear(1.0),
                          style: const TextStyle(
                              color: CustomColors.surface,
                              fontWeight: FontWeight.bold,
                              fontSize: 12),
                        ),
                      );
                    },
                    separatorBuilder: (BuildContext context, int index) =>
                        const Divider(
                      color: CustomColors.primary,
                    ),
                  ),
            Container(
              margin: const EdgeInsets.only(top: 10, bottom: 10),
              child: const Divider(
                color: CustomColors.background,
              ),
            ),
            Container(
              margin: const EdgeInsets.only(bottom: 10),
              child: RichText(
                text: const TextSpan(
                  text: 'Ajustes por psicólogo/a',
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: CustomColors.surface,
                  ),
                ),
              ),
            ),
            Column(
              children: [
                ListView.builder(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: cambios.length,
                  itemBuilder: (BuildContext context, int index) {
                    if (users[index] == widget.userID) {
                      switch (flags[index]) {
                        case 'E':
                          tituloAjustes = 'Estado de atención:';
                          break;
                        default:
                          tituloAjustes = 'Ajustado por psicólogo/a';
                      }
                      switch (cambios[index]) {
                        case 'verde':
                          cambioRealizado = 'Riesgo leve';
                          break;
                        case 'amarillo':
                          cambioRealizado = 'Riesgo moderado';
                          break;
                        case 'rojo':
                          cambioRealizado = 'Riesgo alto';
                          break;
                        default:
                          cambioRealizado = cambios[index];
                      }
                      return ListTile(
                        leading: Image(
                          fit: BoxFit.scaleDown,
                          image: AssetImage(
                              stateAjuste(flags[index], cambios[index])),
                        ),
                        title: Text(
                          tituloAjustes,
                          textScaler: const TextScaler.linear(1.0),
                          style: const TextStyle(
                              fontWeight: FontWeight.bold,
                              color: CustomColors.background,
                              fontSize: 12),
                        ),
                        subtitle: Text(
                          cambioRealizado,
                          textScaler: const TextScaler.linear(1.0),
                          style: const TextStyle(
                              color: CustomColors.surface, fontSize: 16),
                        ),
                        trailing: Text(
                          horasAjustes[index],
                          textScaler: const TextScaler.linear(1.0),
                          style: const TextStyle(
                              color: CustomColors.surface,
                              fontWeight: FontWeight.bold,
                              fontSize: 12),
                        ),
                      );
                    } else {
                      return Container();
                    }
                  },
                ),
                const Divider(
                  color: CustomColors.primary,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget avancePrograma() {
    Size size = MediaQuery.of(context).size;

    return Column(
      children: [
        Expanded(
          child: StreamBuilder<QuerySnapshot>(
            stream: chatHomeProvider.getUsersChat(
              FirestoreConstants.pathControlCollection,
            ),
            builder:
                (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
              print(widget.userID);

              if (snapshot.hasData) {
                if ((snapshot.data?.docs.length ?? 0) > 0) {
                  return ListView.builder(
                    itemCount: snapshot.data?.docs.length,
                    itemBuilder: (context, index) {
                      if (snapshot.hasData) {
                        if (widget.userID == '') {
                          return Container();
                        } else {
                          if (snapshot.data!.docs[index].id == widget.userID) {
                            M1A0done =
                                snapshot.data!.docs[index]['terminaM1A0'];
                            if (M1A0done) {
                              porcentajeCompletado = porcentajeActual('m1a0');
                            }
                            M1A1done =
                                snapshot.data!.docs[index]['terminaM1A1'];
                            if (M1A1done) {
                              porcentajeCompletado = porcentajeActual('m1a1');
                            }
                            M1A2done =
                                snapshot.data!.docs[index]['terminaM1A2'];
                            if (M1A2done) {
                              porcentajeCompletado = porcentajeActual('m1a2');
                            }
                            M1A3done =
                                snapshot.data!.docs[index]['terminaM1A3'];
                            if (M1A3done) {
                              porcentajeCompletado = porcentajeActual('m1a3');
                            }
                            M1A4done =
                                snapshot.data!.docs[index]['terminaM1A4'];
                            if (M1A4done) {
                              porcentajeCompletado = porcentajeActual('m1a4');
                            }
                            M1A5done =
                                snapshot.data!.docs[index]['terminaM1A5'];
                            if (M1A5done) {
                              porcentajeCompletado = porcentajeActual('m1a5');
                            }
                            M1ATdone =
                                snapshot.data!.docs[index]['terminaM1AT'];
                            if (M1ATdone) {
                              porcentajeCompletado = porcentajeActual('m1at');
                            }

                            M2A1done =
                                snapshot.data!.docs[index]['terminaM2A1'];
                            if (M2A1done) {
                              porcentajeCompletado = porcentajeActual('m2a1');
                            }
                            M2A2done =
                                snapshot.data!.docs[index]['terminaM2A2'];
                            if (M2A2done) {
                              porcentajeCompletado = porcentajeActual('m2a2');
                            }
                            M2A3done =
                                snapshot.data!.docs[index]['terminaM2A3'];
                            if (M2A3done) {
                              porcentajeCompletado = porcentajeActual('m2a3');
                            }
                            M2A4done =
                                snapshot.data!.docs[index]['terminaM2A4'];
                            if (M2A4done) {
                              porcentajeCompletado = porcentajeActual('m2a4');
                            }
                            M2A5done =
                                snapshot.data!.docs[index]['terminaM2A5'];
                            if (M2A5done) {
                              porcentajeCompletado = porcentajeActual('m2a5');
                            }
                            M2A6done =
                                snapshot.data!.docs[index]['terminaM2A6'];
                            if (M2A6done) {
                              porcentajeCompletado = porcentajeActual('m2a6');
                            }
                            M2ATdone =
                                snapshot.data!.docs[index]['terminaM2AT'];
                            if (M2ATdone) {
                              porcentajeCompletado = porcentajeActual('m2aT');
                            }
                            M3A1done =
                                snapshot.data!.docs[index]['terminaM3A1'];
                            if (M3A1done) {
                              porcentajeCompletado = porcentajeActual('m3a1');
                            }
                            M3A2done =
                                snapshot.data!.docs[index]['terminaM3A2'];
                            if (M3A2done) {
                              porcentajeCompletado = porcentajeActual('m3a2');
                            }
                            M3A3done =
                                snapshot.data!.docs[index]['terminaM3A3'];
                            if (M3A3done) {
                              porcentajeCompletado = porcentajeActual('m3a3');
                            }
                            M3A4done =
                                snapshot.data!.docs[index]['terminaM3A4'];
                            if (M3A4done) {
                              porcentajeCompletado = porcentajeActual('m3a4');
                            }
                            M3A5done =
                                snapshot.data!.docs[index]['terminaM3A5'];
                            if (M3A5done) {
                              porcentajeCompletado = porcentajeActual('m3a5');
                            }
                            M3A6done =
                                snapshot.data!.docs[index]['terminaM3A6'];
                            if (M3A6done) {
                              porcentajeCompletado = porcentajeActual('m3a6');
                            }
                            M3A7done =
                                snapshot.data!.docs[index]['terminaM3A7'];
                            if (M3A7done) {
                              porcentajeCompletado = porcentajeActual('m3a7');
                            }
                            M3A8done =
                                snapshot.data!.docs[index]['terminaM3A8'];
                            if (M3A8done) {
                              porcentajeCompletado = porcentajeActual('m3a8');
                            }
                            M3ATdone =
                                snapshot.data!.docs[index]['terminaM3AT'];
                            if (M3ATdone) {
                              porcentajeCompletado = porcentajeActual('m3at');
                            }
                            M4A1done =
                                snapshot.data!.docs[index]['terminaM4A1'];
                            if (M4A1done) {
                              porcentajeCompletado = porcentajeActual('m4a1');
                            }
                            M4A2done =
                                snapshot.data!.docs[index]['terminaM4A2'];
                            if (M4A2done) {
                              porcentajeCompletado = porcentajeActual('m4a2');
                            }
                            M4A3done =
                                snapshot.data!.docs[index]['terminaM4A3'];
                            if (M4A3done) {
                              porcentajeCompletado = porcentajeActual('m4a3');
                            }
                            M4A4done =
                                snapshot.data!.docs[index]['terminaM4A4'];
                            if (M4A4done) {
                              porcentajeCompletado = porcentajeActual('m4a4');
                            }
                            M4A5done =
                                snapshot.data!.docs[index]['terminaM4A5'];
                            if (M4A5done) {
                              porcentajeCompletado = porcentajeActual('m4a5');
                            }
                            M4A6done =
                                snapshot.data!.docs[index]['terminaM4A6'];
                            if (M4A6done) {
                              porcentajeCompletado = porcentajeActual('m4a6');
                            }
                            M4ATdone =
                                snapshot.data!.docs[index]['terminaM4AT'];
                            if (M4ATdone) {
                              porcentajeCompletado = porcentajeActual('m4at');
                            }
                            return SizedBox(
                              height: size.height / 1.07,
                              child: ListView(
                                children: [
                                  Container(
                                    width: size.width,
                                    height: size.height / 12,
                                    padding: const EdgeInsets.only(
                                        left: 15, right: 15),
                                    color: CustomColors.purpleProgreso,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Container(
                                              width: size.width / 33,
                                              height: 5,
                                              color: M4ATdone
                                                  ? CustomColors.ticketOk
                                                  : M1A0done
                                                      ? Colors.red
                                                      : CustomColors.surface,
                                              child: const Text(''),
                                            ),
                                            Container(
                                              width: size.width / 33,
                                              height: 5,
                                              color: M4ATdone
                                                  ? CustomColors.ticketOk
                                                  : M1A1done
                                                      ? Colors.red
                                                      : CustomColors.surface,
                                              child: const Text(''),
                                            ),
                                            Container(
                                              width: size.width / 33,
                                              height: 5,
                                              color: M4ATdone
                                                  ? CustomColors.ticketOk
                                                  : M1A2done
                                                      ? Colors.red
                                                      : CustomColors.surface,
                                              child: const Text(''),
                                            ),
                                            Container(
                                              width: size.width / 33,
                                              height: 5,
                                              color: M4ATdone
                                                  ? CustomColors.ticketOk
                                                  : M1A3done
                                                      ? Colors.red
                                                      : CustomColors.surface,
                                              child: const Text(''),
                                            ),
                                            Container(
                                              width: size.width / 33,
                                              height: 5,
                                              color: M4ATdone
                                                  ? CustomColors.ticketOk
                                                  : M1A4done
                                                      ? Colors.red
                                                      : CustomColors.surface,
                                              child: const Text(''),
                                            ),
                                            Container(
                                              width: size.width / 33,
                                              height: 5,
                                              color: M4ATdone
                                                  ? CustomColors.ticketOk
                                                  : M1A5done
                                                      ? Colors.red
                                                      : CustomColors.surface,
                                              child: const Text(''),
                                            ),
                                            Container(
                                              width: size.width / 33,
                                              height: 5,
                                              color: M4ATdone
                                                  ? CustomColors.ticketOk
                                                  : M1ATdone
                                                      ? Colors.red
                                                      : CustomColors.surface,
                                              child: const Text(''),
                                            ),
                                            Container(
                                              width: size.width / 33,
                                              height: 5,
                                              color: M4ATdone
                                                  ? CustomColors.ticketOk
                                                  : M2A1done
                                                      ? Colors.red
                                                      : CustomColors.surface,
                                              child: const Text(''),
                                            ),
                                            Container(
                                              width: size.width / 33,
                                              height: 5,
                                              color: M4ATdone
                                                  ? CustomColors.ticketOk
                                                  : M2A2done
                                                      ? Colors.red
                                                      : CustomColors.surface,
                                              child: const Text(''),
                                            ),
                                            Container(
                                              width: size.width / 33,
                                              height: 5,
                                              color: M4ATdone
                                                  ? CustomColors.ticketOk
                                                  : M2A3done
                                                      ? Colors.red
                                                      : CustomColors.surface,
                                              child: const Text(''),
                                            ),
                                            Container(
                                              width: size.width / 33,
                                              height: 5,
                                              color: M4ATdone
                                                  ? CustomColors.ticketOk
                                                  : M2A4done
                                                      ? Colors.red
                                                      : CustomColors.surface,
                                              child: const Text(''),
                                            ),
                                            Container(
                                              width: size.width / 33,
                                              height: 5,
                                              color: M4ATdone
                                                  ? CustomColors.ticketOk
                                                  : M2A5done
                                                      ? Colors.red
                                                      : CustomColors.surface,
                                              child: const Text(''),
                                            ),
                                            Container(
                                              width: size.width / 33,
                                              height: 5,
                                              color: M4ATdone
                                                  ? CustomColors.ticketOk
                                                  : M2A6done
                                                      ? Colors.red
                                                      : CustomColors.surface,
                                              child: const Text(''),
                                            ),
                                            Container(
                                              width: size.width / 33,
                                              height: 5,
                                              color: M4ATdone
                                                  ? CustomColors.ticketOk
                                                  : M2ATdone
                                                      ? Colors.red
                                                      : CustomColors.surface,
                                              child: const Text(''),
                                            ),
                                            Container(
                                              width: size.width / 33,
                                              height: 5,
                                              color: M4ATdone
                                                  ? CustomColors.ticketOk
                                                  : M3A1done
                                                      ? Colors.red
                                                      : CustomColors.surface,
                                              child: const Text(''),
                                            ),
                                            Container(
                                              width: size.width / 33,
                                              height: 5,
                                              color: M4ATdone
                                                  ? CustomColors.ticketOk
                                                  : M3A3done
                                                      ? Colors.red
                                                      : CustomColors.surface,
                                              child: const Text(''),
                                            ),
                                            Container(
                                              width: size.width / 33,
                                              height: 5,
                                              color: M4ATdone
                                                  ? CustomColors.ticketOk
                                                  : M3A3done
                                                      ? Colors.red
                                                      : CustomColors.surface,
                                              child: const Text(''),
                                            ),
                                            Container(
                                              width: size.width / 33,
                                              height: 5,
                                              color: M4ATdone
                                                  ? CustomColors.ticketOk
                                                  : M3A4done
                                                      ? Colors.red
                                                      : CustomColors.surface,
                                              child: const Text(''),
                                            ),
                                            Container(
                                              width: size.width / 33,
                                              height: 5,
                                              color: M4ATdone
                                                  ? CustomColors.ticketOk
                                                  : M3A5done
                                                      ? Colors.red
                                                      : CustomColors.surface,
                                              child: const Text(''),
                                            ),
                                            Container(
                                              width: size.width / 33,
                                              height: 5,
                                              color: M4ATdone
                                                  ? CustomColors.ticketOk
                                                  : M3A6done
                                                      ? Colors.red
                                                      : CustomColors.surface,
                                              child: const Text(''),
                                            ),
                                            Container(
                                              width: size.width / 33,
                                              height: 5,
                                              color: M4ATdone
                                                  ? CustomColors.ticketOk
                                                  : M3A7done
                                                      ? Colors.red
                                                      : CustomColors.surface,
                                              child: const Text(''),
                                            ),
                                            Container(
                                              width: size.width / 33,
                                              height: 5,
                                              color: M4ATdone
                                                  ? CustomColors.ticketOk
                                                  : M3A8done
                                                      ? Colors.red
                                                      : CustomColors.surface,
                                              child: const Text(''),
                                            ),
                                            Container(
                                              width: size.width / 33,
                                              height: 5,
                                              color: M4ATdone
                                                  ? CustomColors.ticketOk
                                                  : M3ATdone
                                                      ? Colors.red
                                                      : CustomColors.surface,
                                              child: const Text(''),
                                            ),
                                            Container(
                                              width: size.width / 33,
                                              height: 5,
                                              color: M4ATdone
                                                  ? CustomColors.ticketOk
                                                  : M4A1done
                                                      ? Colors.red
                                                      : CustomColors.surface,
                                              child: const Text(''),
                                            ),
                                            Container(
                                              width: size.width / 33,
                                              height: 5,
                                              color: M4ATdone
                                                  ? CustomColors.ticketOk
                                                  : M4A2done
                                                      ? Colors.red
                                                      : CustomColors.surface,
                                              child: const Text(''),
                                            ),
                                            Container(
                                              width: size.width / 33,
                                              height: 5,
                                              color: M4ATdone
                                                  ? CustomColors.ticketOk
                                                  : M4A3done
                                                      ? Colors.red
                                                      : CustomColors.surface,
                                              child: const Text(''),
                                            ),
                                            Container(
                                              width: size.width / 33,
                                              height: 5,
                                              color: M4ATdone
                                                  ? CustomColors.ticketOk
                                                  : M4A4done
                                                      ? Colors.red
                                                      : CustomColors.surface,
                                              child: const Text(''),
                                            ),
                                            Container(
                                              width: size.width / 33,
                                              height: 5,
                                              color: M4ATdone
                                                  ? CustomColors.ticketOk
                                                  : M4A5done
                                                      ? Colors.red
                                                      : CustomColors.surface,
                                              child: const Text(''),
                                            ),
                                            Container(
                                              width: size.width / 33,
                                              height: 5,
                                              color: M4ATdone
                                                  ? CustomColors.ticketOk
                                                  : M4A6done
                                                      ? Colors.red
                                                      : CustomColors.surface,
                                              child: const Text(''),
                                            ),
                                            Container(
                                              width: size.width / 33,
                                              height: 5,
                                              color: M4ATdone
                                                  ? CustomColors.ticketOk
                                                  : CustomColors.surface,
                                              child: const Text(''),
                                            ),
                                          ],
                                        ),
                                        Text(
                                          ' ' +
                                              porcentajeCompletado +
                                              ' de cumplimiento del programa',
                                          textScaler:
                                              const TextScaler.linear(1.0),
                                          style: const TextStyle(
                                            fontSize: 18,
                                            color: CustomColors.background,
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  Container(
                                    margin: const EdgeInsets.only(
                                        top: 15, left: 15, bottom: 5),
                                    child: RichText(
                                      text: const TextSpan(
                                        text: 'Nivel 1 ',
                                        style: TextStyle(
                                          fontSize: 20,
                                          color: CustomColors.surface,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    alignment: Alignment.center,
                                    child: ListTile(
                                      leading: const Image(
                                        image: AssetImage(
                                            'assets/images/m1a0-mpi.png'),
                                      ),
                                      subtitle: const Text(
                                        'Vamos juntxs',
                                        textScaler: TextScaler.linear(1.0),
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 2,
                                        style: TextStyle(
                                            color: CustomColors.surface,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16),
                                      ),
                                      title: Container(
                                        margin: const EdgeInsets.only(top: 5),
                                        child: const Text(
                                          'Lectura',
                                          textScaler: TextScaler.linear(1.0),
                                          style: TextStyle(
                                              color: CustomColors.orange3,
                                              fontSize: 14),
                                        ),
                                      ),
                                      trailing: Icon(
                                        M1A0done
                                            ? Icons.check
                                            : MdiIcons.menuRight,
                                        color: M1A0done
                                            ? CustomColors.ticketOk
                                            : CustomColors.surface,
                                        size: size.height / 24,
                                      ),
                                    ),
                                  ),
                                  const Divider(
                                    color: Colors.white,
                                  ),
                                  Container(
                                    alignment: Alignment.center,
                                    child: ListTile(
                                      leading: const Image(
                                        image: AssetImage(
                                            'assets/images/m1a1-mpi.png'),
                                      ),
                                      subtitle: const Text(
                                        '¿Qué es la depresión?',
                                        textScaler: TextScaler.linear(1.0),
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 2,
                                        style: TextStyle(
                                            color: CustomColors.surface,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16),
                                      ),
                                      title: Container(
                                        margin: const EdgeInsets.only(top: 5),
                                        child: const Text(
                                          'Video',
                                          textScaler: TextScaler.linear(1.0),
                                          style: TextStyle(
                                              color: CustomColors.orange3,
                                              fontSize: 14),
                                        ),
                                      ),
                                      trailing: Icon(
                                        M1A0done
                                            ? M1A1done
                                                ? Icons.check
                                                : MdiIcons.menuRight
                                            : MdiIcons.lock,
                                        color: M1A1done
                                            ? CustomColors.ticketOk
                                            : CustomColors.surface,
                                        size: size.height / 24,
                                      ),
                                    ),
                                  ),
                                  const Divider(
                                    color: Colors.white,
                                  ),
                                  Container(
                                    alignment: Alignment.center,
                                    child: ListTile(
                                      leading: const Image(
                                        image: AssetImage(
                                            'assets/images/m1a2-mpi.png'),
                                      ),
                                      subtitle: const Text(
                                        'Mitos sobre la depresión',
                                        textScaler: TextScaler.linear(1.0),
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 2,
                                        style: TextStyle(
                                            color: CustomColors.surface,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16),
                                      ),
                                      title: Container(
                                        margin: const EdgeInsets.only(top: 5),
                                        child: const Text(
                                          'Ejercicio',
                                          textScaler: TextScaler.linear(1.0),
                                          style: TextStyle(
                                              color: CustomColors.orange3,
                                              fontSize: 14),
                                        ),
                                      ),
                                      trailing: Icon(
                                        M1A1done
                                            ? M1A2done
                                                ? Icons.check
                                                : MdiIcons.menuRight
                                            : MdiIcons.lock,
                                        color: M1A2done
                                            ? CustomColors.ticketOk
                                            : CustomColors.surface,
                                        size: size.height / 24,
                                      ),
                                    ),
                                  ),
                                  const Divider(
                                    color: Colors.white,
                                  ),
                                  Container(
                                    alignment: Alignment.center,
                                    child: ListTile(
                                      leading: const Image(
                                        image: AssetImage(
                                            'assets/images/m1a3-mpi.png'),
                                      ),
                                      subtitle: const Text(
                                        '¿Qué es la ansiedad?',
                                        textScaler: TextScaler.linear(1.0),
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 2,
                                        style: TextStyle(
                                            color: CustomColors.surface,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16),
                                      ),
                                      title: Container(
                                        margin: const EdgeInsets.only(top: 5),
                                        child: const Text(
                                          'Video',
                                          textScaler: TextScaler.linear(1.0),
                                          style: TextStyle(
                                              color: CustomColors.orange3,
                                              fontSize: 14),
                                        ),
                                      ),
                                      trailing: Icon(
                                        M1A2done
                                            ? M1A3done
                                                ? Icons.check
                                                : MdiIcons.menuRight
                                            : MdiIcons.lock,
                                        color: M1A3done
                                            ? CustomColors.ticketOk
                                            : CustomColors.surface,
                                        size: size.height / 24,
                                      ),
                                    ),
                                  ),
                                  const Divider(
                                    color: Colors.white,
                                  ),
                                  Container(
                                    alignment: Alignment.center,
                                    child: ListTile(
                                      leading: const Image(
                                        image: AssetImage(
                                            'assets/images/m1a4-mpi.png'),
                                      ),
                                      subtitle: const Text(
                                        '7 Principios para tu activación conductual',
                                        textScaler: TextScaler.linear(1.0),
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 2,
                                        style: TextStyle(
                                            color: CustomColors.surface,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16),
                                      ),
                                      title: Container(
                                        margin: const EdgeInsets.only(top: 5),
                                        child: const Text(
                                          'Lectura',
                                          textScaler: TextScaler.linear(1.0),
                                          style: TextStyle(
                                              color: CustomColors.orange3,
                                              fontSize: 14),
                                        ),
                                      ),
                                      trailing: Icon(
                                        M1A3done
                                            ? M1A4done
                                                ? Icons.check
                                                : MdiIcons.menuRight
                                            : MdiIcons.lock,
                                        color: M1A4done
                                            ? CustomColors.ticketOk
                                            : CustomColors.surface,
                                        size: size.height / 24,
                                      ),
                                    ),
                                  ),
                                  const Divider(
                                    color: Colors.white,
                                  ),
                                  Container(
                                    alignment: Alignment.center,
                                    child: ListTile(
                                      leading: const Image(
                                        image: AssetImage(
                                            'assets/images/m1a5-mpi.png'),
                                      ),
                                      subtitle: const Text(
                                        'Aplicando la Activación Conductual',
                                        textScaler: TextScaler.linear(1.0),
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 2,
                                        style: TextStyle(
                                            color: CustomColors.surface,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16),
                                      ),
                                      title: Container(
                                        margin: const EdgeInsets.only(top: 5),
                                        child: const Text(
                                          'Ejercicio',
                                          textScaler: TextScaler.linear(1.0),
                                          style: TextStyle(
                                              color: CustomColors.orange3,
                                              fontSize: 14),
                                        ),
                                      ),
                                      trailing: Icon(
                                        M1A4done
                                            ? M1A5done
                                                ? Icons.check
                                                : MdiIcons.menuRight
                                            : MdiIcons.lock,
                                        color: M1A5done
                                            ? CustomColors.ticketOk
                                            : CustomColors.surface,
                                        size: size.height / 24,
                                      ),
                                    ),
                                  ),
                                  const Divider(
                                    color: Colors.white,
                                  ),
                                  Container(
                                    alignment: Alignment.center,
                                    child: ListTile(
                                      leading: const Image(
                                        image: AssetImage(
                                            'assets/images/mat-mpi.png'),
                                      ),
                                      subtitle: const Text(
                                        'Termómetro del Ánimo',
                                        textScaler: TextScaler.linear(1.0),
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 2,
                                        style: TextStyle(
                                            color: CustomColors.surface,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16),
                                      ),
                                      title: Container(
                                        margin: const EdgeInsets.only(top: 5),
                                        child: const Text(
                                          'Ejercicio',
                                          textScaler: TextScaler.linear(1.0),
                                          style: TextStyle(
                                              color: CustomColors.orange3,
                                              fontSize: 14),
                                        ),
                                      ),
                                      trailing: Icon(
                                        M1A5done
                                            ? M1ATdone
                                                ? Icons.check
                                                : MdiIcons.menuRight
                                            : MdiIcons.lock,
                                        color: M1ATdone
                                            ? CustomColors.ticketOk
                                            : CustomColors.surface,
                                        size: size.height / 24,
                                      ),
                                    ),
                                  ),
                                  const Divider(
                                    color: Colors.transparent,
                                  ),
                                  M2(),
                                  M3(),
                                  M4(),
                                ],
                              ),
                            );
                          } else {
                            return Container();
                          }
                        }
                      }
                      return null;
                    },
                  );
                } else {
                  return const Center(
                    child: Text(
                      "ERROR",
                      textScaler: TextScaler.linear(1.0),
                    ),
                  );
                }
              } else {
                return const Center(
                  child: CircularProgressIndicator(
                    color: Colors.red,
                  ),
                );
              }
            },
          ),
        ),
      ],
    );
  }

  Widget M2() {
    Size size = MediaQuery.of(context).size;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.only(top: 15, left: 15, bottom: 5),
          child: const Text(
            'Nivel 2',
            textScaler: TextScaler.linear(1.0),
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            leading: const Image(
              image: AssetImage('assets/images/m2a1-mpi.png'),
            ),
            subtitle: const Text(
              'Redes de apoyo',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(
                  color: CustomColors.surface,
                  fontWeight: FontWeight.bold,
                  fontSize: 16),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Video',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            trailing: Icon(
              M1ATdone
                  ? M2A1done
                      ? Icons.check
                      : MdiIcons.menuRight
                  : MdiIcons.lock,
              color: M2A1done ? CustomColors.ticketOk : CustomColors.surface,
              size: size.height / 24,
            ),
          ),
        ),
        const Divider(
          color: Colors.white,
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            leading: const Image(
              image: AssetImage('assets/images/m2a2-mpi.png'),
            ),
            subtitle: const Text(
              'El Ecomapa',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(
                  color: CustomColors.surface,
                  fontWeight: FontWeight.bold,
                  fontSize: 16),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Lectura',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            trailing: Icon(
              M2A1done
                  ? M2A2done
                      ? Icons.check
                      : MdiIcons.menuRight
                  : MdiIcons.lock,
              color: M2A2done ? CustomColors.ticketOk : CustomColors.surface,
              size: size.height / 24,
            ),
          ),
        ),
        const Divider(
          color: Colors.white,
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            leading: const Image(
              image: AssetImage('assets/images/m2a3-mpi.png'),
            ),
            subtitle: const Text(
              'Tu red de apoyo',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(
                  color: CustomColors.surface,
                  fontWeight: FontWeight.bold,
                  fontSize: 16),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Ejercicio',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            onTap: () {
              if (M2A3done) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: ((context) => M2A3Respuestas(
                          docID: widget.userID,
                          nombreCompleto: nombreUser,
                        )),
                  ),
                );
              } else {
                return;
              }
            },
            trailing: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  M2A2done
                      ? M2A3done
                          ? MdiIcons.textBoxCheckOutline
                          : MdiIcons.menuRight
                      : MdiIcons.lock,
                  color:
                      M2A3done ? CustomColors.ticketOk : CustomColors.surface,
                  size: M2A3done ? 20 : size.height / 24,
                ),
                M2A3done
                    ? Text(
                        'Ver respuestas',
                        textScaler: const TextScaler.linear(1.0),
                        style: TextStyle(
                            color: M2A3done
                                ? CustomColors.ticketOk
                                : Colors.transparent,
                            fontWeight: FontWeight.bold,
                            fontSize: 12),
                      )
                    : const SizedBox.shrink(),
              ],
            ),
          ),
        ),
        const Divider(
          color: Colors.white,
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            leading: const Image(
              image: AssetImage('assets/images/m2a4-mpi.png'),
            ),
            subtitle: const Text(
              'Regulación emocional',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(
                  color: CustomColors.surface,
                  fontWeight: FontWeight.bold,
                  fontSize: 16),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Lectura',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            trailing: Icon(
              M2A3done
                  ? M2A4done
                      ? Icons.check
                      : MdiIcons.menuRight
                  : MdiIcons.lock,
              color: M2A4done ? CustomColors.ticketOk : CustomColors.surface,
              size: size.height / 24,
            ),
          ),
        ),
        const Divider(
          color: Colors.white,
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            leading: const Image(
              image: AssetImage('assets/images/m2a5-mpi.png'),
            ),
            subtitle: const Text(
              '4 estrategias de regulación emocional',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(
                  color: CustomColors.surface,
                  fontWeight: FontWeight.bold,
                  fontSize: 16),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Lectura',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            trailing: Icon(
              M2A4done
                  ? M2A5done
                      ? Icons.check
                      : MdiIcons.menuRight
                  : MdiIcons.lock,
              color: M2A5done ? CustomColors.ticketOk : CustomColors.surface,
              size: size.height / 24,
            ),
          ),
        ),
        const Divider(
          color: Colors.white,
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            leading: const Image(
              image: AssetImage('assets/images/m2a6-mpi.png'),
            ),
            subtitle: const Text(
              'Guía de regulación emocional',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(
                  color: CustomColors.surface,
                  fontWeight: FontWeight.bold,
                  fontSize: 16),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Ejercicio',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            trailing: Icon(
              M2A5done
                  ? M2A6done
                      ? Icons.check
                      : MdiIcons.menuRight
                  : MdiIcons.lock,
              color: M2A6done ? CustomColors.ticketOk : CustomColors.surface,
              size: size.height / 24,
            ),
          ),
        ),
        const Divider(
          color: Colors.white,
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            leading: const Image(
              image: AssetImage('assets/images/mat-mpi.png'),
            ),
            subtitle: const Text(
              'Termómetro del Ánimo',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(
                  color: CustomColors.surface,
                  fontWeight: FontWeight.bold,
                  fontSize: 16),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Ejercicio',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            trailing: Icon(
              M2A6done
                  ? M2ATdone
                      ? Icons.check
                      : MdiIcons.menuRight
                  : MdiIcons.lock,
              color: M2ATdone ? CustomColors.ticketOk : CustomColors.surface,
              size: size.height / 24,
            ),
          ),
        ),
        const Divider(
          color: Colors.transparent,
        ),
      ],
    );
  }

  Widget M3() {
    Size size = MediaQuery.of(context).size;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.only(top: 15, left: 15, bottom: 5),
          child: const Text(
            'Nivel 3',
            textScaler: TextScaler.linear(1.0),
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            leading: const Image(
              image: AssetImage('assets/images/m3a1-mpi.png'),
            ),
            subtitle: const Text(
              '¿Qué son los errores del pensamiento?',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(
                  color: CustomColors.surface,
                  fontWeight: FontWeight.bold,
                  fontSize: 16),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Lectura',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            trailing: Icon(
              M2ATdone
                  ? M3A1done
                      ? Icons.check
                      : MdiIcons.menuRight
                  : MdiIcons.lock,
              color: M3A1done ? CustomColors.ticketOk : CustomColors.surface,
              size: size.height / 24,
            ),
          ),
        ),
        const Divider(
          color: Colors.white,
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            leading: const Image(
              image: AssetImage('assets/images/m3a2-mpi.png'),
            ),
            subtitle: const Text(
              'Identificando errores del pensamiento',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(
                  color: CustomColors.surface,
                  fontWeight: FontWeight.bold,
                  fontSize: 16),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Ejercicio',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            trailing: Icon(
              M3A1done
                  ? M3A2done
                      ? Icons.check
                      : MdiIcons.menuRight
                  : MdiIcons.lock,
              color: M3A2done ? CustomColors.ticketOk : CustomColors.surface,
              size: size.height / 24,
            ),
          ),
        ),
        const Divider(
          color: Colors.white,
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            leading: const Image(
              image: AssetImage('assets/images/m3a3-mpi.png'),
            ),
            subtitle: const Text(
              '5 Pasos para enfrentar los pensamientos negativos',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(
                  color: CustomColors.surface,
                  fontWeight: FontWeight.bold,
                  fontSize: 16),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Lectura',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            trailing: Icon(
              M3A2done
                  ? M3A3done
                      ? Icons.check
                      : MdiIcons.menuRight
                  : MdiIcons.lock,
              color: M3A3done ? CustomColors.ticketOk : CustomColors.surface,
              size: size.height / 24,
            ),
          ),
        ),
        const Divider(
          color: Colors.white,
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            leading: const Image(
              image: AssetImage('assets/images/m3a4-mpi.png'),
            ),
            subtitle: const Text(
              'Resolución de problemas',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(
                  color: CustomColors.surface,
                  fontWeight: FontWeight.bold,
                  fontSize: 16),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Lectura',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            trailing: Icon(
              M3A3done
                  ? M3A4done
                      ? Icons.check
                      : MdiIcons.menuRight
                  : MdiIcons.lock,
              color: M3A4done ? CustomColors.ticketOk : CustomColors.surface,
              size: size.height / 24,
            ),
          ),
        ),
        const Divider(
          color: Colors.white,
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            leading: const Image(
              image: AssetImage('assets/images/m3a5-mpi.png'),
            ),
            subtitle: const Text(
              'Definiendo el problema',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(
                  color: CustomColors.surface,
                  fontWeight: FontWeight.bold,
                  fontSize: 16),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Ejercicio',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            onTap: () {
              if (M3A5done) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: ((context) => M3A5Respuestas(
                          docID: widget.userID,
                          nombreCompleto: nombreUser,
                        )),
                  ),
                );
              } else {
                return;
              }
            },
            trailing: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  M3A4done
                      ? M3A5done
                          ? MdiIcons.textBoxCheckOutline
                          : MdiIcons.menuRight
                      : MdiIcons.lock,
                  color:
                      M3A5done ? CustomColors.ticketOk : CustomColors.surface,
                  size: M3A5done ? 20 : size.height / 24,
                ),
                M3A5done
                    ? Text(
                        'Ver respuestas',
                        textScaler: const TextScaler.linear(1.0),
                        style: TextStyle(
                            color: M3A5done
                                ? CustomColors.ticketOk
                                : Colors.transparent,
                            fontWeight: FontWeight.bold,
                            fontSize: 12),
                      )
                    : const SizedBox.shrink(),
              ],
            ),
          ),
        ),
        const Divider(
          color: Colors.white,
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            leading: const Image(
              image: AssetImage('assets/images/m3a6-mpi.png'),
            ),
            subtitle: const Text(
              'Generando alternativas de solución',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(
                  color: CustomColors.surface,
                  fontWeight: FontWeight.bold,
                  fontSize: 16),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Ejercicio',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            onTap: () {
              if (M3A6done) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: ((context) => M3A6Respuestas(
                          docID: widget.userID,
                          nombreCompleto: nombreUser,
                        )),
                  ),
                );
              } else {
                return;
              }
            },
            trailing: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  M3A5done
                      ? M3A6done
                          ? MdiIcons.textBoxCheckOutline
                          : MdiIcons.menuRight
                      : MdiIcons.lock,
                  color:
                      M3A6done ? CustomColors.ticketOk : CustomColors.surface,
                  size: M3A6done ? 20 : size.height / 24,
                ),
                M3A6done
                    ? Text(
                        'Ver respuestas',
                        textScaler: const TextScaler.linear(1.0),
                        style: TextStyle(
                            color: M3A6done
                                ? CustomColors.ticketOk
                                : Colors.transparent,
                            fontWeight: FontWeight.bold,
                            fontSize: 12),
                      )
                    : const SizedBox.shrink(),
              ],
            ),
          ),
        ),
        const Divider(
          color: Colors.white,
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            leading: const Image(
              image: AssetImage('assets/images/m3a7-mpi.png'),
            ),
            subtitle: const Text(
              'Tomando decisiones',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(
                  color: CustomColors.surface,
                  fontWeight: FontWeight.bold,
                  fontSize: 16),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Ejercicio',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            onTap: () {
              if (M3A7done) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: ((context) => M3A7Respuestas(
                          docID: widget.userID,
                          nombreCompleto: nombreUser,
                        )),
                  ),
                );
              } else {
                return;
              }
            },
            trailing: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  M3A6done
                      ? M3A7done
                          ? MdiIcons.textBoxCheckOutline
                          : MdiIcons.menuRight
                      : MdiIcons.lock,
                  color:
                      M3A7done ? CustomColors.ticketOk : CustomColors.surface,
                  size: M3A7done ? 20 : size.height / 24,
                ),
                M3A7done
                    ? Text(
                        'Ver respuestas',
                        textScaler: const TextScaler.linear(1.0),
                        style: TextStyle(
                            color: M3A7done
                                ? CustomColors.ticketOk
                                : Colors.transparent,
                            fontWeight: FontWeight.bold,
                            fontSize: 12),
                      )
                    : const SizedBox.shrink(),
              ],
            ),
          ),
        ),
        const Divider(
          color: Colors.white,
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            leading: const Image(
              image: AssetImage('assets/images/m3a8-mpi.png'),
            ),
            subtitle: const Text(
              'Implementando la solución y evaluando la solución',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(
                  color: CustomColors.surface,
                  fontWeight: FontWeight.bold,
                  fontSize: 16),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Lectura',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            trailing: Icon(
              M3A7done
                  ? M3A8done
                      ? Icons.check
                      : MdiIcons.menuRight
                  : MdiIcons.lock,
              color: M3A8done ? CustomColors.ticketOk : CustomColors.surface,
              size: size.height / 24,
            ),
          ),
        ),
        const Divider(
          color: Colors.white,
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            leading: const Image(
              image: AssetImage('assets/images/mat-mpi.png'),
            ),
            subtitle: const Text(
              'Termómetro del Ánimo',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(
                  color: CustomColors.surface,
                  fontWeight: FontWeight.bold,
                  fontSize: 16),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Ejercicio',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            trailing: Icon(
              M3A8done
                  ? M3ATdone
                      ? Icons.check
                      : MdiIcons.menuRight
                  : MdiIcons.lock,
              color: M3ATdone ? CustomColors.ticketOk : CustomColors.surface,
              size: size.height / 24,
            ),
          ),
        ),
        const Divider(
          color: Colors.transparent,
        ),
      ],
    );
  }

  Widget M4() {
    Size size = MediaQuery.of(context).size;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.only(top: 15, left: 15, bottom: 5),
          child: const Text(
            'Módulo 4',
            textScaler: TextScaler.linear(1.0),
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            leading: const Image(
              image: AssetImage('assets/images/m4a1-mpi.png'),
            ),
            subtitle: const Text(
              'Exposición y evitación',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(
                  color: CustomColors.surface,
                  fontWeight: FontWeight.bold,
                  fontSize: 16),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Lectura',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            trailing: Icon(
              M3ATdone
                  ? M4A1done
                      ? Icons.check
                      : MdiIcons.menuRight
                  : MdiIcons.lock,
              color: M4A1done ? CustomColors.ticketOk : CustomColors.surface,
              size: size.height / 24,
            ),
          ),
        ),
        const Divider(
          color: Colors.white,
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            leading: const Image(
              image: AssetImage('assets/images/m4a2-mpi.png'),
            ),
            subtitle: const Text(
              'Con los ojos en la meta',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(
                  color: CustomColors.surface,
                  fontWeight: FontWeight.bold,
                  fontSize: 16),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Ejercicio',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            onTap: () {
              if (M4A2done) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: ((context) => M4A2Respuestas(
                          docID: widget.userID,
                          nombreCompleto: nombreUser,
                        )),
                  ),
                );
              } else {
                return;
              }
            },
            trailing: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  M4A1done
                      ? M4A2done
                          ? MdiIcons.textBoxCheckOutline
                          : MdiIcons.menuRight
                      : MdiIcons.lock,
                  color:
                      M4A2done ? CustomColors.ticketOk : CustomColors.surface,
                  size: M4A2done ? 20 : size.height / 24,
                ),
                M4A2done
                    ? Text(
                        'Ver respuestas',
                        textScaler: const TextScaler.linear(1.0),
                        style: TextStyle(
                            color: M4A2done
                                ? CustomColors.ticketOk
                                : Colors.transparent,
                            fontWeight: FontWeight.bold,
                            fontSize: 12),
                      )
                    : const SizedBox.shrink(),
              ],
            ),
          ),
        ),
        const Divider(
          color: Colors.white,
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            leading: const Image(
              image: AssetImage('assets/images/m4a3-mpi.png'),
            ),
            subtitle: const Text(
              '¿Qué es lo que evitas?',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(
                  color: CustomColors.surface,
                  fontWeight: FontWeight.bold,
                  fontSize: 16),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Ejercicio',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            onTap: () {
              if (M4A3done) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: ((context) => M4A3Respuestas(
                          docID: widget.userID,
                          nombreCompleto: nombreUser,
                        )),
                  ),
                );
              } else {
                return;
              }
            },
            trailing: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  M4A2done
                      ? M4A3done
                          ? MdiIcons.textBoxCheckOutline
                          : MdiIcons.menuRight
                      : MdiIcons.lock,
                  color:
                      M4A3done ? CustomColors.ticketOk : CustomColors.surface,
                  size: M4A3done ? 20 : size.height / 24,
                ),
                M4A3done
                    ? Text(
                        'Ver respuestas',
                        textScaler: const TextScaler.linear(1.0),
                        style: TextStyle(
                            color: M4A3done
                                ? CustomColors.ticketOk
                                : Colors.transparent,
                            fontWeight: FontWeight.bold,
                            fontSize: 12),
                      )
                    : const SizedBox.shrink(),
              ],
            ),
          ),
        ),
        const Divider(
          color: Colors.white,
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            leading: const Image(
              image: AssetImage('assets/images/m4a4-mpi.png'),
            ),
            subtitle: const Text(
              'Creando alternativas',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(
                  color: CustomColors.surface,
                  fontWeight: FontWeight.bold,
                  fontSize: 16),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Ejercicio',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            onTap: () {
              if (M4A4done) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: ((context) => M4A4Respuestas(
                          docID: widget.userID,
                          nombreCompleto: nombreUser,
                        )),
                  ),
                );
              } else {
                return;
              }
            },
            trailing: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  M4A3done
                      ? M4A4done
                          ? MdiIcons.textBoxCheckOutline
                          : MdiIcons.menuRight
                      : MdiIcons.lock,
                  color:
                      M4A4done ? CustomColors.ticketOk : CustomColors.surface,
                  size: M4A4done ? 20 : size.height / 24,
                ),
                M4A4done
                    ? Text(
                        'Ver respuestas',
                        textScaler: const TextScaler.linear(1.0),
                        style: TextStyle(
                            color: M4A4done
                                ? CustomColors.ticketOk
                                : Colors.transparent,
                            fontWeight: FontWeight.bold,
                            fontSize: 12),
                      )
                    : const SizedBox.shrink(),
              ],
            ),
          ),
        ),
        const Divider(
          color: Colors.white,
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            leading: const Image(
              image: AssetImage('assets/images/m4a5-mpi.png'),
            ),
            subtitle: const Text(
              'Avanzando paso a paso',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(
                  color: CustomColors.surface,
                  fontWeight: FontWeight.bold,
                  fontSize: 16),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Ejercicio',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            onTap: () {
              if (M4A5done) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: ((context) => M4A5Respuestas(
                          docID: widget.userID,
                          nombreCompleto: nombreUser,
                        )),
                  ),
                );
              } else {
                return;
              }
            },
            trailing: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  M4A4done
                      ? M4A5done
                          ? MdiIcons.textBoxCheckOutline
                          : MdiIcons.menuRight
                      : MdiIcons.lock,
                  color:
                      M4A5done ? CustomColors.ticketOk : CustomColors.surface,
                  size: M4A5done ? 20 : size.height / 24,
                ),
                M4A5done
                    ? Text(
                        'Ver respuestas',
                        textScaler: const TextScaler.linear(1.0),
                        style: TextStyle(
                            color: M4A5done
                                ? CustomColors.ticketOk
                                : Colors.transparent,
                            fontWeight: FontWeight.bold,
                            fontSize: 12),
                      )
                    : const SizedBox.shrink(),
              ],
            ),
          ),
        ),
        const Divider(
          color: Colors.white,
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            leading: const Image(
              image: AssetImage('assets/images/m4a6-mpi.png'),
            ),
            subtitle: const Text(
              'Tu plan de exposición',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(
                  color: CustomColors.surface,
                  fontWeight: FontWeight.bold,
                  fontSize: 16),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Ejercicio',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            trailing: Icon(
              M4A5done
                  ? M4A6done
                      ? Icons.check
                      : MdiIcons.menuRight
                  : MdiIcons.lock,
              color: M4A6done ? CustomColors.ticketOk : CustomColors.surface,
              size: size.height / 24,
            ),
          ),
        ),
        const Divider(
          color: Colors.white,
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            leading: const Image(
              image: AssetImage('assets/images/mat-mpi.png'),
            ),
            subtitle: const Text(
              'Termómetro del Ánimo',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(
                  color: CustomColors.surface,
                  fontWeight: FontWeight.bold,
                  fontSize: 16),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Ejercicio',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            trailing: Icon(
              M4A6done
                  ? M4ATdone
                      ? Icons.check
                      : MdiIcons.menuRight
                  : MdiIcons.lock,
              color: M4ATdone ? CustomColors.ticketOk : CustomColors.surface,
              size: size.height / 24,
            ),
          ),
        ),
      ],
    );
  }

  String statePHQ4Icon(String value) {
    switch (value) {
      case 'verde':
        return 'assets/images/phq4-low.png';

      case 'amarillo':
        return 'assets/images/phq4-mid.png';
      case 'rojo':
        return 'assets/images/phq4-high.png';

      default:
        return 'assets/images/termo-mid.png';
    }
  }

  String stateIcon(int value) {
    switch (value) {
      case 0:
        return 'assets/images/termo-high.png';

      case 1:
        return 'assets/images/termo-high.png';
      case 2:
        return 'assets/images/termo-high.png';
      case 3:
        return 'assets/images/termo-high.png';
      case 4:
        return 'assets/images/termo-high.png';
      case 5:
        return 'assets/images/termo-mid.png';
      case 6:
        return 'assets/images/termo-mid.png';
      case 7:
        return 'assets/images/termo-low.png';
      case 8:
        return 'assets/images/termo-low.png';
      case 9:
        return 'assets/images/termo-low.png';
      case 10:
        return 'assets/images/termo-low.png';
      default:
        return 'assets/images/termo-mid.png';
    }
  }

  String stateAjuste(String flag, String cambio) {
    switch (flag) {
      case 'E':
        return 'assets/images/estado-details.png';

      case 'N':
        return iconEstado(cambio);
      default:
        return 'E';
    }
  }

  String iconEstado(String cambio) {
    switch (cambio) {
      case 'rojo':
        return 'assets/images/state-high.png';
      case 'amarillo':
        return 'assets/images/state-mid.png';

      default:
        return 'assets/images/state-low.png';
    }
  }

  String stateHint(int value) {
    switch (value) {
      case 0:
        return 'No me siento bien';

      case 1:
        return 'No me siento bien';
      case 2:
        return 'No me siento bien';
      case 3:
        return 'No me siento bien';
      case 4:
        return 'No me siento bien';
      case 5:
        return 'Me siento más o menos';
      case 6:
        return 'Me siento más o menos';
      case 7:
        return 'Me siento bien';
      case 8:
        return 'Me siento bien';
      case 9:
        return 'Me siento bien';
      case 10:
        return 'Me siento bien';
      default:
        return 'Me siento más o menos';
    }
  }
}
