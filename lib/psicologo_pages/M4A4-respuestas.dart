// ignore_for_file: non_constant_identifier_names, avoid_unnecessary_containers, sized_box_for_whitespace, file_names

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:seguimiento/colors.dart';
import 'package:seguimiento/firebase.dart';

class M4A4Respuestas extends StatefulWidget {
  const M4A4Respuestas({
    Key? key,
    required this.nombreCompleto,
    required this.docID,
  }) : super(key: key);

  final String nombreCompleto;
  final String docID;

  @override
  State<M4A4Respuestas> createState() => _M4A4RespuestasState();
}

class _M4A4RespuestasState extends State<M4A4Respuestas> {
  String problemaData = '';

  CollectionReference problemaReference =
      FirebaseFirestore.instance.collection('M4A2-respuestas');
  void getProblema() {
    problemaReference.doc(widget.docID).get().then((value) {
      var fields = value;
      setState(() {
        problemaData = fields['objetivo'];
      });
    });
  }

  @override
  void initState() {
    getProblema();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    String alternativa1 = '';
    String alternativa2 = '';
    String alternativa3 = '';

    return Scaffold(
      backgroundColor: CustomColors.purplePrograma,
      appBar: AppBar(
        toolbarHeight: size.height / 10,
        backgroundColor: CustomColors.purplePsicologo,
        title: Text(
          'Estudiante:\n' + widget.nombreCompleto,
          textScaler: const TextScaler.linear(1.0),
          style: const TextStyle(
            fontSize: 18,
            color: Colors.white,
          ),
        ),
        iconTheme: const IconThemeData(
          color: Colors.white, //change your color here
        ),
        elevation: 0,
      ),
      body: Center(
        child: StreamBuilder<QuerySnapshot>(
          stream: FirestoreService().respM4A4(),
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (snapshot.hasData) {
              if ((snapshot.data?.docs.length ?? 0) > 0) {
                return ListView.builder(
                  padding: const EdgeInsets.all(10),
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (context, index) {
                    var idDoc = snapshot.data!.docs[index].id;
                    if (idDoc == widget.docID) {
                      alternativa1 = snapshot.data!.docs[index]['alternativa1'];
                      alternativa2 = snapshot.data!.docs[index]['alternativa2'];
                      alternativa3 = snapshot.data!.docs[index]['alternativa3'];

                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            width: size.width,
                            margin: const EdgeInsets.all(10),
                            padding: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(25),
                              color: CustomColors.greenButton,
                            ),
                            child: const Text(
                              'Respuestas de la actividad\n"Creando alternativas"',
                              textAlign: TextAlign.center,
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 18,
                                  color: CustomColors.purpleDark,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: const Text(
                              'Objetivo personal:',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                color: CustomColors.orange),
                            child: ListTile(
                              title: Text(
                                problemaData,
                                textScaler: const TextScaler.linear(1.0),
                                style: const TextStyle(
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(
                                top: 40, bottom: 10, left: 10, right: 10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white),
                            child: ListTile(
                              title: const Text(
                                'Alternativa de estrategia 1:',
                                textScaler: TextScaler.linear(1.0),
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(
                                alternativa1,
                                textScaler: const TextScaler.linear(1.0),
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white),
                            child: ListTile(
                              title: const Text(
                                'Alternativa de estrategia 2:',
                                textScaler: TextScaler.linear(1.0),
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(
                                alternativa2,
                                textScaler: const TextScaler.linear(1.0),
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white),
                            child: ListTile(
                              title: const Text(
                                'Alternativa de estrategia 3:',
                                textScaler: TextScaler.linear(1.0),
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(
                                alternativa3,
                                textScaler: const TextScaler.linear(1.0),
                              ),
                            ),
                          ),
                        ],
                      );
                    } else {
                      return Container();
                    }
                  },
                );
              } else {
                return const Center(
                  child: Text(
                    "No users",
                    textScaler: TextScaler.linear(1.0),
                  ),
                );
              }
            } else {
              return const Center(
                child: CircularProgressIndicator(
                  color: Colors.red,
                ),
              );
            }
          },
        ),
      ),
    );
  }
}
