// ignore_for_file: file_names, prefer_typing_uninitialized_variables

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:seguimiento/colors.dart';
import 'package:seguimiento/firebase.dart';
import 'package:seguimiento/psicologo_pages/infopaciente.dart';
import '../chatlogics/models_and_others/chat_home_provider.dart';

class NotsPsicologo extends StatefulWidget {
  const NotsPsicologo({Key? key}) : super(key: key);

  @override
  State<NotsPsicologo> createState() => _NotsPsicologoState();
}

class _NotsPsicologoState extends State<NotsPsicologo> {
  late ChatPageProvider chatHomeProvider;

  String estadoAtencion = '';
  String alertaRecibida = '';

  @override
  void initState() {
    super.initState();

    chatHomeProvider = context.read<ChatPageProvider>();
  }

  String iconAlerta(String tipoNotificacion, String nivel) {
    switch (tipoNotificacion) {
      case 'phq4':
        return iconP4(nivel);

      case 'termometro':
        return 'assets/images/termo-high.png';

      case 'chat':
        return 'assets/images/noti-chat.png';

      default:
        return 'assets/images/termo-low.png';
    }
  }

  String iconP4(String nivel) {
    switch (nivel) {
      case 'rojo':
        return 'assets/images/phq4-high.png';
      case 'amarillo':
        return 'assets/images/phq4-mid.png';

      default:
        return 'assets/images/phq4-low.png';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: CustomColors.purpleDark,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: CustomColors.purplePrograma,
          appBar: AppBar(
            title: const Text(
              'Notificaciones',
              textScaler: TextScaler.linear(1.0),
              style: TextStyle(
                fontSize: 20,
                color: Colors.white,
              ),
            ),
            centerTitle: true,
            backgroundColor: CustomColors.purplePsicologo,
            iconTheme: const IconThemeData(
              color: Colors.white, //change your color here
            ),
          ),
          body: Container(
            margin: const EdgeInsets.only(left: 5, right: 5),
            child: StreamBuilder<QuerySnapshot>(
              stream: FirestoreService().historialNotificaciones(),
              builder: (BuildContext context,
                  AsyncSnapshot<QuerySnapshot> snapshot) {
                if (snapshot.hasData) {
                  if ((snapshot.data?.docs.length ?? 0) > 0) {
                    return ListView.builder(
                      shrinkWrap: true,
                      itemCount: snapshot.data!.docs.length,
                      itemBuilder: (context, index) {
                        var notsData = snapshot.data?.docs[index];
                        if (notsData != null) {
                          return Column(
                            children: [
                              ListTile(
                                leading: Image(
                                  fit: BoxFit.scaleDown,
                                  image: AssetImage(iconAlerta(
                                      notsData['tipo'], notsData['alerta'])),
                                ),
                                title: Text(
                                  notsData['nombre'],
                                  style: const TextStyle(
                                      color: CustomColors.primary80pink,
                                      fontSize: 16),
                                ),
                                subtitle: Text(
                                  notsData['body'],
                                  style: const TextStyle(
                                      height: 1.5,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 13,
                                      color: Colors.white),
                                ),
                                trailing: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Text(
                                      DateFormat('dd-MM-yyyy')
                                          .format(notsData['fecha'].toDate()),
                                      style: const TextStyle(
                                          height: 1.7,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 13,
                                          color: Colors.white),
                                    ),
                                    Icon(
                                      MdiIcons.menuRight,
                                      color: Colors.white,
                                    ),
                                  ],
                                ),
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: ((context) => InfoPaciente(
                                              userID: notsData['uid'],
                                              indexStart: 2,
                                            )),
                                      ));
                                },
                              ),
                              const Divider(
                                color: Colors.white,
                                thickness: 0.3,
                              ),
                            ],
                          );
                        } else {
                          return const SizedBox.shrink();
                        }
                      },
                    );
                  } else {
                    return const Center(
                      child: Text(
                        'Sin notificaciones',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(color: Colors.white, fontSize: 22),
                      ),
                    );
                  }
                } else {
                  return const Center(
                    child: CircularProgressIndicator(
                      color: Colors.red,
                    ),
                  );
                }
              },
            ),
          ),
        ),
      ),
    );
  }
}
