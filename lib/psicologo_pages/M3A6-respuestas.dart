// ignore_for_file: non_constant_identifier_names, avoid_unnecessary_containers, sized_box_for_whitespace, file_names

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:seguimiento/colors.dart';
import 'package:seguimiento/firebase.dart';

class M3A6Respuestas extends StatefulWidget {
  const M3A6Respuestas({
    Key? key,
    required this.nombreCompleto,
    required this.docID,
  }) : super(key: key);

  final String nombreCompleto;
  final String docID;

  @override
  State<M3A6Respuestas> createState() => _M3A6RespuestasState();
}

class _M3A6RespuestasState extends State<M3A6Respuestas> {
  String problemaData = '';

  CollectionReference problemaReference =
      FirebaseFirestore.instance.collection('M3A5-respuestas');
  void getProblema() {
    problemaReference.doc(widget.docID).get().then((value) {
      var fields = value;
      setState(() {
        problemaData = fields['descripcionProblema'];
      });
    });
  }

  @override
  void initState() {
    getProblema();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    String solucion1 = '';
    String solucion2 = '';
    String solucion3 = '';
    String solucion4 = '';
    String solucion5 = '';

    return Scaffold(
      backgroundColor: CustomColors.purplePrograma,
      appBar: AppBar(
        toolbarHeight: size.height / 10,
        backgroundColor: CustomColors.purplePsicologo,
        title: Text(
          'Estudiante:\n' + widget.nombreCompleto,
          textScaler: const TextScaler.linear(1.0),
          style: const TextStyle(
            fontSize: 18,
            color: Colors.white,
          ),
        ),
        iconTheme: const IconThemeData(
          color: Colors.white, //change your color here
        ),
        elevation: 0,
      ),
      body: Center(
        child: StreamBuilder<QuerySnapshot>(
          stream: FirestoreService().respM3A6(),
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (snapshot.hasData) {
              if ((snapshot.data?.docs.length ?? 0) > 0) {
                return ListView.builder(
                  padding: const EdgeInsets.all(10),
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (context, index) {
                    var idDoc = snapshot.data!.docs[index].id;
                    if (idDoc == widget.docID) {
                      solucion1 = snapshot.data!.docs[index]['solucion1'];
                      solucion2 = snapshot.data!.docs[index]['solucion2'];
                      solucion3 = snapshot.data!.docs[index]['solucion3'];
                      solucion4 = snapshot.data!.docs[index]['solucion4'];
                      solucion5 = snapshot.data!.docs[index]['solucion5'];

                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            width: size.width,
                            margin: const EdgeInsets.all(10),
                            padding: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              color: CustomColors.greenButton,
                            ),
                            child: const Text(
                              'Respuestas de la actividad\n"Generando alternativas de solución"',
                              textAlign: TextAlign.center,
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 18,
                                  color: CustomColors.purpleDark,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: const Text(
                              'El problema que tiene:',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                color: CustomColors.orange),
                            child: ListTile(
                              title: Text(
                                problemaData,
                                textScaler: const TextScaler.linear(1.0),
                                style: const TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 16),
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: const Text(
                              'Las soluciones que propone:',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white),
                            child: ListTile(
                              title: const Text(
                                'Solución 1:',
                                textScaler: TextScaler.linear(1.0),
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(
                                solucion1,
                                textScaler: const TextScaler.linear(1.0),
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white),
                            child: ListTile(
                              title: const Text(
                                'Solución 2:',
                                textScaler: TextScaler.linear(1.0),
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(
                                solucion2,
                                textScaler: const TextScaler.linear(1.0),
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white),
                            child: ListTile(
                              title: const Text(
                                'Solución 3:',
                                textScaler: TextScaler.linear(1.0),
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(
                                solucion3,
                                textScaler: const TextScaler.linear(1.0),
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white),
                            child: ListTile(
                              title: const Text(
                                'Solución 4:',
                                textScaler: TextScaler.linear(1.0),
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(
                                solucion4,
                                textScaler: const TextScaler.linear(1.0),
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white),
                            child: ListTile(
                              title: const Text(
                                'Solución 5:',
                                textScaler: TextScaler.linear(1.0),
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(
                                solucion5,
                                textScaler: const TextScaler.linear(1.0),
                              ),
                            ),
                          ),
                        ],
                      );
                    } else {
                      return Container();
                    }
                  },
                );
              } else {
                return const Center(
                  child: Text(
                    "No users",
                    textScaler: TextScaler.linear(1.0),
                  ),
                );
              }
            } else {
              return const Center(
                child: CircularProgressIndicator(
                  color: Colors.red,
                ),
              );
            }
          },
        ),
      ),
    );
  }
}
