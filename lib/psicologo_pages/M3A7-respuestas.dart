// ignore_for_file: non_constant_identifier_names, avoid_unnecessary_containers, sized_box_for_whitespace, file_names

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:seguimiento/colors.dart';
import 'package:seguimiento/firebase.dart';

class M3A7Respuestas extends StatefulWidget {
  const M3A7Respuestas({
    Key? key,
    required this.nombreCompleto,
    required this.docID,
  }) : super(key: key);

  final String nombreCompleto;
  final String docID;

  @override
  State<M3A7Respuestas> createState() => _M3A7RespuestasState();
}

class _M3A7RespuestasState extends State<M3A7Respuestas> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    String alter1positiva = '';
    String alter1negativa = '';
    String alter2positiva = '';
    String alter2negativa = '';
    String alternativa1 = '';
    String alternativa2 = '';
    String alternativaFinal = '';

    return Scaffold(
      backgroundColor: CustomColors.purplePrograma,
      appBar: AppBar(
        toolbarHeight: size.height / 10,
        backgroundColor: CustomColors.purplePsicologo,
        title: Text(
          'Estudiante:\n' + widget.nombreCompleto,
          textScaler: const TextScaler.linear(1.0),
          style: const TextStyle(
            fontSize: 18,
            color: Colors.white,
          ),
        ),
        iconTheme: const IconThemeData(
          color: Colors.white, //change your color here
        ),
        elevation: 0,
      ),
      body: Center(
        child: StreamBuilder<QuerySnapshot>(
          stream: FirestoreService().respM3A7(),
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (snapshot.hasData) {
              if ((snapshot.data?.docs.length ?? 0) > 0) {
                return ListView.builder(
                  padding: const EdgeInsets.all(10),
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (context, index) {
                    var idDoc = snapshot.data!.docs[index].id;
                    if (idDoc == widget.docID) {
                      alter1positiva =
                          snapshot.data!.docs[index]['alter1positiva'];
                      alter1negativa =
                          snapshot.data!.docs[index]['alter1negativa'];
                      alter2positiva =
                          snapshot.data!.docs[index]['alter2positiva'];
                      alter2negativa =
                          snapshot.data!.docs[index]['alter2negativa'];
                      alternativa1 = snapshot.data!.docs[index]['alternativa1'];
                      alternativa2 = snapshot.data!.docs[index]['alternativa2'];
                      alternativaFinal = snapshot.data!.docs[index]
                          ['alternativaFinalSeleccionada'];
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            width: size.width,
                            margin: const EdgeInsets.only(
                                top: 10, left: 10, right: 10, bottom: 30),
                            padding: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              color: CustomColors.greenButton,
                            ),
                            child: const Text(
                              'Respuestas de la actividad\n"Tomando decisiones"',
                              textAlign: TextAlign.center,
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 18,
                                  color: CustomColors.purpleDark,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                color: CustomColors.orange),
                            child: ListTile(
                              title: const Text(
                                'Alternativa 1 seleccionada:',
                                textScaler: TextScaler.linear(1.0),
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(
                                alternativa1,
                                textScaler: const TextScaler.linear(1.0),
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white),
                            child: ListTile(
                              title: const Text(
                                'Consecuencia positiva:',
                                textScaler: TextScaler.linear(1.0),
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(
                                alter1positiva,
                                textScaler: const TextScaler.linear(1.0),
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(
                                top: 10, left: 10, right: 10, bottom: 40),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white),
                            child: ListTile(
                              title: const Text(
                                'Consecuencia negativa:',
                                textScaler: TextScaler.linear(1.0),
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(
                                alter1negativa,
                                textScaler: const TextScaler.linear(1.0),
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                color: CustomColors.orange),
                            child: ListTile(
                              title: const Text(
                                'Alternativa 2 seleccionada:',
                                textScaler: TextScaler.linear(1.0),
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(
                                alternativa2,
                                textScaler: const TextScaler.linear(1.0),
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white),
                            child: ListTile(
                              title: const Text(
                                'Consecuencia positiva:',
                                textScaler: TextScaler.linear(1.0),
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(
                                alter2positiva,
                                textScaler: const TextScaler.linear(1.0),
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white),
                            child: ListTile(
                              title: const Text(
                                'Consecuencia negativa:',
                                textScaler: TextScaler.linear(1.0),
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(
                                alter2negativa,
                                textScaler: const TextScaler.linear(1.0),
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(
                                top: 40, left: 10, right: 10, bottom: 10),
                            child: const Text(
                              'Alternativa por la que se decidió:',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                color: CustomColors.primary2),
                            child: ListTile(
                              title: Text(
                                alternativaFinal,
                                textScaler: const TextScaler.linear(1.0),
                                style: const TextStyle(
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ],
                      );
                    } else {
                      return Container();
                    }
                  },
                );
              } else {
                return const Center(
                  child: Text(
                    "No users",
                    textScaler: TextScaler.linear(1.0),
                  ),
                );
              }
            } else {
              return const Center(
                child: CircularProgressIndicator(
                  color: Colors.red,
                ),
              );
            }
          },
        ),
      ),
    );
  }
}
