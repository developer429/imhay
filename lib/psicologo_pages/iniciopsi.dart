// ignore_for_file: unused_local_variable, non_constant_identifier_names, avoid_print

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:seguimiento/colors.dart';
import 'package:seguimiento/notifications_cloud.dart';
import 'package:seguimiento/psicologo_pages/infopaciente.dart';
import 'package:seguimiento/psicologo_pages/nots-psicologo.dart';
import 'package:seguimiento/psicologo_pages/search_pages/search_delegate_custom.dart';
import 'package:seguimiento/user_pages/perfil.dart';
import '../chatlogics/chat_page.dart';
import '../chatlogics/models_and_others/firestore_constants.dart';
import '../chatlogics/models_and_others/chat_home_provider.dart';

class InicioPsicologo extends StatefulWidget {
  const InicioPsicologo({Key? key}) : super(key: key);

  @override
  State<InicioPsicologo> createState() => _InicioPsicologoState();
}

Future _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  print("Handling a background message: ${message.messageId}");
}

/*Agregue esta clase */
class PushNotification {
  PushNotification({
    this.title,
    this.body,
    this.dataId,
    this.dataNombre,
    this.userType,
  });
  String? title;
  String? body;
  String? dataId;
  String? dataNombre;
  String? userType;
}

class _InicioPsicologoState extends State<InicioPsicologo> {
  late ChatPageProvider chatHomeProvider;
  late String currentUserId;
  PushNotification? _notificationInfo;
  late final FirebaseMessaging _messaging;

  void registerNotification() async {
    await Firebase.initializeApp();

    _messaging = FirebaseMessaging.instance;

    FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

    NotificationSettings settings = await _messaging.requestPermission(
      alert: true,
      badge: true,
      provisional: false,
      sound: true,
    );

    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      print('User granted permission');

      FirebaseMessaging.onMessage.listen((RemoteMessage message) {
        PushNotification notification = PushNotification(
          title: message.notification?.title,
          body: message.notification?.body,
          dataId: message.data['idE'],
          userType: message.data['userType'],
          dataNombre: message.data['nombre'],
        );

        setState(() {
          _notificationInfo = notification;
        });
      });
    } else {
      print('User declined permission');
    }
  }

  /* AQUI TERMINA UNA PARTE DE LO QUE AGREGUE */

  checkForInitialMessage() async {
    await Firebase.initializeApp();
    RemoteMessage? initialMessage =
        await FirebaseMessaging.instance.getInitialMessage();

    if (initialMessage != null) {
      PushNotification notification = PushNotification(
        title: initialMessage.notification?.title,
        body: initialMessage.notification?.body,
        dataId: initialMessage.data['idE'],
        userType: initialMessage.data['userType'],
        dataNombre: initialMessage.data['nombre'],
      );
      setState(() {
        _notificationInfo = notification;
        bool user_type = true;
        //El userType de .userType que hacemos extraemos es el user type del emisor.
        if (_notificationInfo!.userType == "1") {
          user_type = false;
        }
      });
    }
  }

  String estadoAtencion = '';
  String alertaRecibida = '';

  @override
  void initState() {
    super.initState();
    getAllUsersData();
    registerNotification();
    checkForInitialMessage();
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      PushNotification notification = PushNotification(
          title: message.notification?.title,
          body: message.notification?.body,
          dataId: message.data['idE'],
          userType: message.data['userType'],
          dataNombre: message.data['nombre']);

      setState(() {
        final _notificationInfo = notification;

        bool user_type = true;
        //El userType de .userType que hacemos extraemos es el user type del emisor.
        if (_notificationInfo.userType == "1") {
          user_type = false;
        }
        if (_notificationInfo.dataNombre != null) {
          print("No es null, user_type es: " + _notificationInfo.userType!);

          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => ChatPage(
                isVistaDetails: false,
                isPsicologo: true,
                arguments: ChatPageArguments(
                  peerId: _notificationInfo.dataId!,
                  peerNickname: _notificationInfo.dataNombre!,
                ),
              ),
            ),
          );
        } else {
          print('Es null');
          print(_notificationInfo.body);
        }
      });
    });

    chatHomeProvider = context.read<ChatPageProvider>();
    currentUserId = FirebaseAuth.instance.currentUser!.uid;
    FirebaseMessaging.instance.getToken().then((token) async {
      if (token != null) {
        NotificationService().sendTokenPsicologo(currentUserId, token);
      }
    });
    /*if (authProvider.getUserFirebaseId()?.isNotEmpty == true) {
      currentUserId = authProvider.getUserFirebaseId()!;
    } else {
      Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => LoginPage()),
        (Route<dynamic> route) => false,
      );
    }*/
    //registerNotification();
    // configLocalNotification();
  }

  String iconAlerta(String tipoNotificacion, String nivel) {
    switch (tipoNotificacion) {
      case 'phq4':
        return iconP4(nivel);

      case 'termometro':
        return 'assets/images/noti-termo.png';

      case 'chat':
        return 'assets/images/noti-chat.png';

      default:
        return 'assets/images/termo-bien.png';
    }
  }

  String iconP4(String nivel) {
    switch (nivel) {
      case 'rojo':
        return 'assets/images/phq4-high.png';
      case 'amarillo':
        return 'assets/images/phq4-mid.png';

      default:
        return 'assets/images/termo-bien.png';
    }
  }

  List<Map<String, dynamic>> usersData = [];
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('usuarios');
  void getAllUsersData() {
    collectionReference.get().then((value) {
      var fields = value;
      setState(() {
        for (var data in fields.docs) {
          usersData.add({
            'id': data.id,
            'alerta': data['alerta'],
            'estadoAtencion': data['estadoAtencion'],
            'nombreCompleto': data['nombreCompleto'],
            'fechaInicio': data['fechaInicio'],
            'pronombre': data['pronombre'],
            'psicologo': data['psicologo'],
            'rut': data['rut'],
          });
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      color: CustomColors.purpleDark,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: CustomColors.purplePrograma,
          appBar: AppBar(
            title: const Text(
              'Información de pacientes',
              textScaler: TextScaler.linear(1.0),
              style: TextStyle(
                fontSize: 20,
                color: Colors.white,
              ),
            ),
            backgroundColor: CustomColors.purplePsicologo,
            actions: [
              IconButton(
                icon: const Icon(Icons.search),
                color: Colors.white,
                onPressed: () => showSearch(
                  context: context,
                  delegate: PostSearchDelegate(posts: usersData),
                ),
              ),
              IconButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const NotsPsicologo()),
                  );
                },
                icon: Icon(MdiIcons.bellOutline),
                color: Colors.white,
              ),
              IconButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const TuPerfil(
                        typeOfUser: 'P',
                      ),
                    ),
                  );
                },
                icon: Icon(MdiIcons.accountCircleOutline),
                color: Colors.white,
              ),
            ],
          ),
          body: Center(
            child: StreamBuilder<QuerySnapshot>(
              stream: chatHomeProvider.getUsersChat(
                FirestoreConstants.pathUserCollection,
              ),
              builder: (BuildContext context,
                  AsyncSnapshot<QuerySnapshot> snapshot) {
                if (snapshot.hasData) {
                  if ((snapshot.data?.docs.length ?? 0) > 0) {
                    return ListView.separated(
                      itemCount: snapshot.data!.docs.length,
                      itemBuilder: (context, index) {
                        var document = snapshot.data?.docs[index];
                        if (document != null) {
                          return ListTile(
                            leading: Container(
                              padding: const EdgeInsets.all(15),
                              decoration: const BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: CustomColors.purpleDark),
                              child: Text(
                                document['nombreCompleto'][0]
                                    .toString()
                                    .toUpperCase(),
                                style: const TextStyle(
                                    color: Colors.white, fontSize: 16),
                              ),
                            ),
                            title: Text(
                              document['nombreCompleto'],
                              style: const TextStyle(
                                  color: CustomColors.primary80pink,
                                  fontSize: 16),
                            ),
                            subtitle: Text(
                              document['estadoAtencion'],
                              style: const TextStyle(
                                  height: 1.7,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 13,
                                  color: Colors.white),
                            ),
                            trailing: Icon(
                              MdiIcons.menuRight,
                              color: Colors.white,
                            ),
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: ((context) => InfoPaciente(
                                          userID: document.id,
                                          indexStart: 1,
                                        )),
                                  ));
                            },
                          );
                        } else {
                          return const SizedBox.shrink();
                        }
                      },
                      separatorBuilder: (BuildContext context, int index) =>
                          const Divider(
                        color: Colors.white,
                        thickness: 0.3,
                      ),
                    );
                  } else {
                    return const Center(
                      child: Text(
                        "No users",
                        textScaler: TextScaler.linear(1.0),
                      ),
                    );
                  }
                } else {
                  return const Center(
                    child: CircularProgressIndicator(
                      color: Colors.red,
                    ),
                  );
                }
              },
            ),
          ),
        ),
      ),
    );
  }
}
