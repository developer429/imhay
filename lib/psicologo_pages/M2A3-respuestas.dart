// ignore_for_file: non_constant_identifier_names, avoid_unnecessary_containers, sized_box_for_whitespace, file_names

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:seguimiento/colors.dart';
import 'package:seguimiento/firebase.dart';

class M2A3Respuestas extends StatefulWidget {
  const M2A3Respuestas({
    Key? key,
    required this.nombreCompleto,
    required this.docID,
  }) : super(key: key);

  final String nombreCompleto;
  final String docID;

  @override
  State<M2A3Respuestas> createState() => _M2A3RespuestasState();
}

class _M2A3RespuestasState extends State<M2A3Respuestas> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    String amistad1 = '';
    String amistad2 = '';
    String amistad3 = '';

    String comunidad1 = '';
    String comunidad2 = '';
    String comunidad3 = '';

    String familia1 = '';
    String familia2 = '';
    String familia3 = '';

    String universidad1 = '';
    String universidad2 = '';
    String universidad3 = '';

    return Scaffold(
      backgroundColor: CustomColors.purplePrograma,
      appBar: AppBar(
        toolbarHeight: size.height / 10,
        backgroundColor: CustomColors.purplePsicologo,
        title: Text(
          'Estudiante:\n' + widget.nombreCompleto,
          textScaler: const TextScaler.linear(1.0),
          style: const TextStyle(
            fontSize: 18,
            color: Colors.white,
          ),
        ),
        iconTheme: const IconThemeData(
          color: Colors.white, //change your color here
        ),
        elevation: 0,
      ),
      body: Center(
        child: StreamBuilder<QuerySnapshot>(
          stream: FirestoreService().respM2A3(),
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (snapshot.hasData) {
              if ((snapshot.data?.docs.length ?? 0) > 0) {
                return ListView.builder(
                  padding: const EdgeInsets.all(10),
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (context, index) {
                    var idDoc = snapshot.data!.docs[index].id;
                    if (idDoc == widget.docID) {
                      amistad1 = snapshot.data!.docs[index]['amistad1'];
                      amistad2 = snapshot.data!.docs[index]['amistad2'];
                      amistad3 = snapshot.data!.docs[index]['amistad3'];

                      familia1 = snapshot.data!.docs[index]['familia1'];
                      familia2 = snapshot.data!.docs[index]['familia2'];
                      familia3 = snapshot.data!.docs[index]['familia3'];

                      universidad1 = snapshot.data!.docs[index]['universidad1'];
                      universidad2 = snapshot.data!.docs[index]['universidad2'];
                      universidad3 = snapshot.data!.docs[index]['universidad3'];

                      comunidad1 = snapshot.data!.docs[index]['comunidad1'];
                      comunidad2 = snapshot.data!.docs[index]['comunidad2'];
                      comunidad3 = snapshot.data!.docs[index]['comunidad3'];
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            width: size.width,
                            margin: const EdgeInsets.all(10),
                            padding: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              color: CustomColors.greenButton,
                            ),
                            child: const Text(
                              'Respuestas de la actividad\n"Tu red de apoyo"',
                              textAlign: TextAlign.center,
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 18,
                                  color: CustomColors.purpleDark,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: const Text(
                              'Amistades',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white),
                            child: ListTile(
                              title: const Text(
                                'Amistad 1:',
                                textScaler: TextScaler.linear(1.0),
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(
                                amistad1,
                                textScaler: const TextScaler.linear(1.0),
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white),
                            child: ListTile(
                              title: const Text(
                                'Amistad 2:',
                                textScaler: TextScaler.linear(1.0),
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(
                                amistad2,
                                textScaler: const TextScaler.linear(1.0),
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white),
                            child: ListTile(
                              title: const Text(
                                'Amistad 3:',
                                textScaler: TextScaler.linear(1.0),
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(
                                amistad3,
                                textScaler: const TextScaler.linear(1.0),
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: const Text(
                              'Familiares',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white),
                            child: ListTile(
                              title: const Text(
                                'Familia 1:',
                                textScaler: TextScaler.linear(1.0),
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(
                                familia1,
                                textScaler: const TextScaler.linear(1.0),
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white),
                            child: ListTile(
                              title: const Text(
                                'Familia 2:',
                                textScaler: TextScaler.linear(1.0),
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(
                                familia2,
                                textScaler: const TextScaler.linear(1.0),
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white),
                            child: ListTile(
                              title: const Text(
                                'Familia 3:',
                                textScaler: TextScaler.linear(1.0),
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(
                                familia3,
                                textScaler: const TextScaler.linear(1.0),
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: const Text(
                              'Universidad',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white),
                            child: ListTile(
                              title: const Text(
                                'Universidad 1:',
                                textScaler: TextScaler.linear(1.0),
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(
                                universidad1,
                                textScaler: const TextScaler.linear(1.0),
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white),
                            child: ListTile(
                              title: const Text(
                                'Universidad 2:',
                                textScaler: TextScaler.linear(1.0),
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(
                                universidad2,
                                textScaler: const TextScaler.linear(1.0),
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white),
                            child: ListTile(
                              title: const Text(
                                'Universidad 3:',
                                textScaler: TextScaler.linear(1.0),
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(
                                universidad3,
                                textScaler: const TextScaler.linear(1.0),
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: const Text(
                              'Comunidades',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white),
                            child: ListTile(
                              title: const Text(
                                'Comunidad 1:',
                                textScaler: TextScaler.linear(1.0),
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(
                                comunidad1,
                                textScaler: const TextScaler.linear(1.0),
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white),
                            child: ListTile(
                              title: const Text(
                                'Comunidad 2:',
                                textScaler: TextScaler.linear(1.0),
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(
                                comunidad2,
                                textScaler: const TextScaler.linear(1.0),
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white),
                            child: ListTile(
                              title: const Text(
                                'Comunidad 3:',
                                textScaler: TextScaler.linear(1.0),
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(
                                comunidad3,
                                textScaler: const TextScaler.linear(1.0),
                              ),
                            ),
                          ),
                        ],
                      );
                    } else {
                      return Container();
                    }
                  },
                );
              } else {
                return const Center(
                  child: Text(
                    "No users",
                    textScaler: TextScaler.linear(1.0),
                  ),
                );
              }
            } else {
              return const Center(
                child: CircularProgressIndicator(
                  color: Colors.red,
                ),
              );
            }
          },
        ),
      ),
    );
  }
}
