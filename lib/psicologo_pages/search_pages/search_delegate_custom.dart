import 'package:flutter/material.dart';
import 'package:seguimiento/colors.dart';
import 'package:seguimiento/psicologo_pages/search_pages/search_results.dart';

class PostSearchDelegate extends SearchDelegate {
  PostSearchDelegate({required this.posts});

  final List<Map<String, dynamic>> posts;
  List<Map<String, dynamic>> results = [];

  @override
  ThemeData appBarTheme(BuildContext context) {
    return ThemeData(
      textTheme: const TextTheme(
        titleLarge: TextStyle(
          color: Colors.white,
        ),
        titleMedium: TextStyle(
          color: Colors.white,
        ),
        titleSmall: TextStyle(
          color: Colors.white,
        ),
      ),
      inputDecorationTheme: const InputDecorationTheme(
        hintStyle: TextStyle(color: Color.fromARGB(255, 218, 207, 207)),
      ),
      appBarTheme: const AppBarTheme(
        color: CustomColors.purplePsicologo,
      ),
    );
  }

  @override
  String get searchFieldLabel => 'Busca por nombre, rut o estado';
  @override
  List<Widget>? buildActions(BuildContext context) => [
        IconButton(
          color: Colors.white,
          icon: const Icon(Icons.clear),
          onPressed: () => query.isEmpty ? close(context, null) : query = '',
        ),
      ];

  @override
  Widget? buildLeading(BuildContext context) => IconButton(
        color: Colors.white,
        icon: const Icon(Icons.arrow_back),
        onPressed: () => close(context, null),
      );

  @override
  @override
  Widget buildResults(BuildContext context) => results.isEmpty
      ? Container(
          color: CustomColors.purplePrograma,
          child: const Center(
            child: Text('No se encontraron resultados',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.white,
                )),
          ),
        )
      : ResultList(posts: results);

  @override
  Widget buildSuggestions(BuildContext context) {
    results = posts.where((post) {
      final String nombre = post['nombreCompleto'].toLowerCase();
      final String rut = post['rut'].toLowerCase();
      final String atencion = post['estadoAtencion'].toLowerCase();

      final String input = query.toLowerCase();

      return nombre.contains(input) ||
          rut.contains(input) ||
          atencion.contains(input);
    }).toList();

    return results.isEmpty
        ? Container(
            color: CustomColors.purplePrograma,
            child: const Center(
              child: Text('No se encontraron resultados',
                  textScaler: TextScaler.linear(1.0),
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.white,
                  )),
            ),
          )
        : ResultList(posts: results);
  }
}
