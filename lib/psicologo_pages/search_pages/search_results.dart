import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:seguimiento/colors.dart';
import 'package:seguimiento/psicologo_pages/infopaciente.dart';

class ResultList extends StatelessWidget {
  const ResultList({
    required this.posts,
    super.key,
  });

  final List<Map<String, dynamic>> posts;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: CustomColors.purplePrograma,
      child: ListView.separated(
        keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
        itemCount: posts.length,
        itemBuilder: (BuildContext context, int index) => ListTile(
          leading: Container(
            padding: const EdgeInsets.all(15),
            decoration: const BoxDecoration(
                shape: BoxShape.circle, color: CustomColors.purpleDark),
            child: Text(
              posts[index]['nombreCompleto'][0].toString().toUpperCase(),
              style: const TextStyle(color: Colors.white, fontSize: 16),
            ),
          ),
          title: Text(
            posts[index]['nombreCompleto'],
            style: const TextStyle(
                color: CustomColors.primary80pink, fontSize: 16),
          ),
          subtitle: Text(
            posts[index]['estadoAtencion'],
            style: const TextStyle(
                height: 1.7,
                fontWeight: FontWeight.bold,
                fontSize: 13,
                color: Colors.white),
          ),
          trailing: Icon(
            MdiIcons.menuRight,
            color: Colors.white,
          ),
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: ((context) => InfoPaciente(
                        userID: posts[index]['id'],
                        indexStart: 1,
                      )),
                ));
          },
        ),
        separatorBuilder: (BuildContext context, int index) => const Divider(
          color: Colors.white,
          thickness: 0.3,
        ),
      ),
    );
  }
}
