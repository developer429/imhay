// ignore_for_file: unused_local_variable

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:seguimiento/firebase.dart';
import 'package:seguimiento/notifications_cloud.dart';
import 'package:seguimiento/psicologo_pages/iniciopsi.dart';

class AuthPsicologo {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  final FirebaseMessaging firebaseMessaging = FirebaseMessaging.instance;

  String? userUID = '';
  String nombrePsi = '';
  String apellidoPsi = '';

  Future<void> iniciaSesionPsicologo(
      BuildContext context, String email, String password) async {
    try {
      UserCredential userCredential = await _firebaseAuth
          .signInWithEmailAndPassword(email: email, password: password);
      userUID = _firebaseAuth.currentUser?.uid;
      switch (userUID) {
        default:
          nombrePsi = 'Apoyo';
          apellidoPsi = 'Vamos Juntxs';
      }
      FirestoreService().agregarPsicologo(nombrePsi, apellidoPsi, userUID);
      firebaseMessaging.requestPermission();

      firebaseMessaging.getToken().then((token) {
        if (token != null) {
          NotificationService().sendTokenPsicologo(userUID!, token);
        }
      });
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute<void>(
              builder: (BuildContext context) => const InicioPsicologo()),
          (route) => false);
    } on FirebaseAuthException catch (ex) {
      switch (ex.code) {
        case 'wrong-password':
          return Future.error('Credenciales incorrectas');

        case 'user-not-found':
          return Future.error('Credenciales incorrectas');
        default:
          return Future.error('Ha ocurrido un error inesperado');
      }
    }
  }

  Future cerrarSesionPsicologo() async {
    return await _firebaseAuth.signOut();
  }

  Future<String> getAuthType() async {
    var firebaseUser = _firebaseAuth.currentUser!;
    String authType = firebaseUser.providerData[1].providerId;
    return authType;
  }
}
