import 'package:flutter/material.dart';
import 'package:seguimiento/colors.dart';
import 'package:seguimiento/psicologo_pages/auth_psicologo.dart';

class LoginPsicologo extends StatefulWidget {
  const LoginPsicologo({Key? key}) : super(key: key);

  @override
  State<LoginPsicologo> createState() => _LoginPsicologoState();
}

class _LoginPsicologoState extends State<LoginPsicologo> {
  final _formKey = GlobalKey<FormState>();
  final _correoC = TextEditingController();
  final _passwordC = TextEditingController();
  String errorLogin = '';
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: CustomColors.primary80pink,
      appBar: AppBar(
        title: const Text(
          'Ingrese sus credenciales',
          textScaler: TextScaler.linear(1.0),
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
      ),
      body: SafeArea(
        child: Form(
          key: _formKey,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Spacer(),
                  Container(
                    margin: const EdgeInsets.all(10),
                    width: size.width / 1.4,
                    child: TextFormField(
                      validator: (String? value) {
                        return (value == null || value == '')
                            ? 'Campo vacío.'
                            : null;
                      },
                      keyboardType: TextInputType.emailAddress,
                      textAlign: TextAlign.center,
                      autofocus: false,
                      controller: _correoC,
                      decoration: const InputDecoration(labelText: 'Correo'),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.all(10),
                    width: size.width / 1.4,
                    child: TextFormField(
                      obscureText: true,
                      validator: (String? value) {
                        return (value == null || value == '')
                            ? 'Campo vacío.'
                            : null;
                      },
                      textAlign: TextAlign.center,
                      autofocus: false,
                      controller: _passwordC,
                      decoration:
                          const InputDecoration(labelText: 'Contraseña'),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.all(10),
                    child: Text(
                      errorLogin,
                      textScaler: const TextScaler.linear(1.0),
                      style: const TextStyle(color: Colors.red),
                    ),
                  ),
                  const Spacer(),
                  Container(
                    margin: const EdgeInsets.all(10),
                    width: size.width / 1.2,
                    child: ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor: WidgetStateProperty.all<Color>(
                            CustomColors.primary),
                        shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0),
                          ),
                        ),
                      ),
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          // If the form is valid, display a snackbar. In the real world,
                          // you'd often call a server or save the information in a database.
                          AuthPsicologo authPsicologo = AuthPsicologo();
                          //FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
                          authPsicologo
                              .iniciaSesionPsicologo(context,
                                  _correoC.text.trim(), _passwordC.text.trim())
                              .catchError((ex) {
                            setState(() {
                              errorLogin = ex.toString();
                            });
                          });

                          /*Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute<void>(
                                  builder: (BuildContext context) =>
                                      const InicioPsicologo()),
                              (route) => false);*/
                          /*FirestoreService()
                              .agregarPsicologo(_correoC.text.trim());
                          print(_firebaseAuth.currentUser?.uid);*/
                        }
                      },
                      child: const Text(
                        "Iniciar Sesión",
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(color: CustomColors.surface),
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
