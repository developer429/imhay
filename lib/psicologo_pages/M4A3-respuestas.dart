// ignore_for_file: non_constant_identifier_names, avoid_unnecessary_containers, sized_box_for_whitespace, file_names

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:seguimiento/colors.dart';
import 'package:seguimiento/firebase.dart';

class M4A3Respuestas extends StatefulWidget {
  const M4A3Respuestas({
    Key? key,
    required this.nombreCompleto,
    required this.docID,
  }) : super(key: key);

  final String nombreCompleto;
  final String docID;

  @override
  State<M4A3Respuestas> createState() => _M4A3RespuestasState();
}

class _M4A3RespuestasState extends State<M4A3Respuestas> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    String situacion1que = '';
    String situacion1evitar = '';
    String situacion2que = '';
    String situacion2evitar = '';
    String situacion3que = '';
    String situacion3evitar = '';

    return Scaffold(
      backgroundColor: CustomColors.purplePrograma,
      appBar: AppBar(
        toolbarHeight: size.height / 10,
        backgroundColor: CustomColors.purplePsicologo,
        title: Text(
          'Estudiante:\n' + widget.nombreCompleto,
          textScaler: const TextScaler.linear(1.0),
          style: const TextStyle(
            fontSize: 18,
            color: Colors.white,
          ),
        ),
        iconTheme: const IconThemeData(
          color: Colors.white, //change your color here
        ),
        elevation: 0,
      ),
      body: Center(
        child: StreamBuilder<QuerySnapshot>(
          stream: FirestoreService().respM4A3(),
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (snapshot.hasData) {
              if ((snapshot.data?.docs.length ?? 0) > 0) {
                return ListView.builder(
                  padding: const EdgeInsets.all(10),
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (context, index) {
                    var idDoc = snapshot.data!.docs[index].id;
                    if (idDoc == widget.docID) {
                      situacion1que =
                          snapshot.data!.docs[index]['situacion1que'];
                      situacion1evitar =
                          snapshot.data!.docs[index]['situacion1evitar'];
                      situacion2que =
                          snapshot.data!.docs[index]['situacion2que'];
                      situacion2evitar =
                          snapshot.data!.docs[index]['situacion2evitar'];
                      situacion3que =
                          snapshot.data!.docs[index]['situacion3que'];
                      situacion3evitar =
                          snapshot.data!.docs[index]['situacion3evitar'];
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            width: size.width,
                            margin: const EdgeInsets.all(10),
                            padding: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(25),
                              color: CustomColors.greenButton,
                            ),
                            child: const Text(
                              'Respuestas de la actividad\n"¿Qué es lo que evitas?"',
                              textAlign: TextAlign.center,
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 18,
                                  color: CustomColors.purpleDark,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: const Text(
                              'Situación 1:',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white),
                            child: ListTile(
                              title: const Text(
                                'Situación que evito: ',
                                textScaler: TextScaler.linear(1.0),
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(
                                situacion1evitar,
                                textScaler: const TextScaler.linear(1.0),
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white),
                            child: ListTile(
                              title: const Text(
                                'Que hago para evitarla',
                                textScaler: TextScaler.linear(1.0),
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(
                                situacion1que,
                                textScaler: const TextScaler.linear(1.0),
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: const Text(
                              'Situación 2:',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white),
                            child: ListTile(
                              title: const Text(
                                'Situación que evito: ',
                                textScaler: TextScaler.linear(1.0),
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(
                                situacion2evitar,
                                textScaler: const TextScaler.linear(1.0),
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white),
                            child: ListTile(
                              title: const Text(
                                'Que hago para evitarla',
                                textScaler: TextScaler.linear(1.0),
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(
                                situacion2que,
                                textScaler: const TextScaler.linear(1.0),
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: const Text(
                              'Situación 3:',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white),
                            child: ListTile(
                              title: const Text(
                                'Situación que evito: ',
                                textScaler: TextScaler.linear(1.0),
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(
                                situacion3evitar,
                                textScaler: const TextScaler.linear(1.0),
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.white),
                            child: ListTile(
                              title: const Text(
                                'Que hago para evitarla',
                                textScaler: TextScaler.linear(1.0),
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(
                                situacion3que,
                                textScaler: const TextScaler.linear(1.0),
                              ),
                            ),
                          ),
                        ],
                      );
                    } else {
                      return Container();
                    }
                  },
                );
              } else {
                return const Center(
                  child: Text(
                    "No users",
                    textScaler: TextScaler.linear(1.0),
                  ),
                );
              }
            } else {
              return const Center(
                child: CircularProgressIndicator(
                  color: Colors.red,
                ),
              );
            }
          },
        ),
      ),
    );
  }
}
