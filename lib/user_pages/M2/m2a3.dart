// ignore_for_file: avoid_print, prefer_typing_uninitialized_variables

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:seguimiento/colors.dart';
import '../../firebase.dart';
import '../MainPage.dart';

class M2A3 extends StatefulWidget {
  const M2A3({
    Key? key,
  }) : super(key: key);

  @override
  State<M2A3> createState() => _M2A3State();
}

class _M2A3State extends State<M2A3> {
  String uid = FirebaseAuth.instance.currentUser!.uid;
  bool loadingData = true;
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('page-control');
  String activeStepData = '';
  void getActivePageStored() {
    collectionReference.doc(uid).get().then((value) {
      var fields = value;
      activeStepData = fields['M2A3'];
      setState(() {
        if (int.parse(activeStepData) < 6) {
          activeStep = int.parse(activeStepData) - 1;
          Future.delayed(const Duration(milliseconds: 500), () {
            setState(() {
              loadingData = false;
            });
          });
        }
      });
    });
  }

  ScrollController scrollController = ScrollController();
  bool favorite = false;
  CollectionReference favoriteData =
      FirebaseFirestore.instance.collection('favoritos');
  void getFavData() {
    favoriteData.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        favorite = fields['M2A3'];
      });
    }).catchError((err) {
      print('Error: $err');
    });
  }

  CollectionReference respuestasReference =
      FirebaseFirestore.instance.collection('M2A3-respuestas');
  void getData() {
    respuestasReference.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        amistad1.text = fields['amistad1'];
        amistad2.text = fields['amistad2'];
        amistad3.text = fields['amistad3'];

        comunidad1.text = fields['comunidad1'];
        comunidad2.text = fields['comunidad2'];
        comunidad3.text = fields['comunidad3'];

        familia1.text = fields['familia1'];
        familia2.text = fields['familia2'];
        familia3.text = fields['familia3'];

        universidad1.text = fields['universidad1'];
        universidad2.text = fields['universidad2'];
        universidad3.text = fields['universidad3'];
      });
    }).catchError((err) {
      print('Error: $err');
    });
  }

  int activeStep = 0;
  final amistad1 = TextEditingController();
  final amistad2 = TextEditingController();
  final amistad3 = TextEditingController();

  final familia1 = TextEditingController();
  final familia2 = TextEditingController();
  final familia3 = TextEditingController();

  final universidad1 = TextEditingController();
  final universidad2 = TextEditingController();
  final universidad3 = TextEditingController();

  final comunidad1 = TextEditingController();
  final comunidad2 = TextEditingController();
  final comunidad3 = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  var timeStart;
  @override
  void initState() {
    super.initState();
    getActivePageStored();
    getData();

    getFavData();

    var tiempo = DateTime.now();
    timeStart = tiempo;
    FirestoreService().setTime(tiempo.toString() + ' - START - M2A3',
        'Comenzó M2A3', '0 minutos (comenzando)');
  }

  @override
  void dispose() {
    super.dispose();
  }

  bool active1 = false;
  bool active2 = false;
  bool active3 = false;
  bool active4 = false;
  bool active5 = false;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        active1 = true;
        active2 = false;
        active3 = false;
        active4 = false;
        active5 = false;

        break;
      case 1:
        active1 = true;
        active2 = true;
        active3 = false;
        active4 = false;
        active5 = false;

        break;
      case 2:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = false;
        active5 = false;

        break;
      case 3:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = false;

        break;
      case 4:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = true;

        break;

      default:
    }
    return PopScope(
      canPop: false,
      onPopInvokedWithResult: (bool didPop, result) {
        if (didPop) {
          return;
        }
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(32.0))),
            title: const Text(
              '¿Estás seguro que deseas salir de la actividad?',
              textScaler: TextScaler.linear(1.0),
              style: TextStyle(color: CustomColors.primaryDark),
            ),
            content: const Text(
              'Tus avances quedarán guardados',
              textScaler: TextScaler.linear(1.0),
              style: TextStyle(color: CustomColors.primaryDark),
            ),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: const Text(
                  'Continuar con la actividad',
                  textScaler: TextScaler.linear(1.0),
                  style: TextStyle(color: CustomColors.primary),
                ),
              ),
              TextButton(
                onPressed: () {
                  FirestoreService()
                      .updateCurrentPage('M2A3', (activeStep + 1).toString());
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute<void>(
                          builder: (BuildContext context) => const MainPage(
                                activePage: 0,
                              )),
                      (route) => false);
                },
                child: const Text(
                  'Salir',
                  textScaler: TextScaler.linear(1.0),
                  style: TextStyle(color: CustomColors.error),
                ),
              ),
            ],
          ),
        );
      },
      child: loadingData
          ? Container(
              width: size.width,
              height: size.height,
              color: CustomColors.primaryDark,
              child: const Center(
                  child: CircularProgressIndicator(
                color: CustomColors.tertiary70,
              )))
          : Container(
              color: Colors.black,
              child: SafeArea(
                child: Scaffold(
                  backgroundColor:
                      active5 ? CustomColors.greenEnd : CustomColors.primary3,
                  body: Center(
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              width: size.width / 5,
                              height: 5,
                              color: active1
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 5,
                              height: 5,
                              color: active2
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 5,
                              height: 5,
                              color: active3
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 5,
                              height: 5,
                              color: active4
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 5,
                              height: 5,
                              color: active5
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                          ],
                        ),
                        content(),
                        Container(
                          margin: const EdgeInsets.only(top: 10, bottom: 10),
                          child: bottomStructure(),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
    );
  }

  Widget topStructure() {
    Size size = MediaQuery.of(context).size;
    return Container(
      width: size.width,
      height: active5 ? null : size.height / 3,
      color: active5 ? CustomColors.greenList : CustomColors.orange2,
      child: Column(
        children: [
          Row(
            children: [
              active5
                  ? Container()
                  : Container(
                      alignment: Alignment.topRight,
                      child: IconButton(
                        onPressed: () {
                          showDialog(
                            context: context,
                            builder: (context) => AlertDialog(
                              shape: const RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(32.0))),
                              title: const Text(
                                '¿Estás seguro que deseas salir de la actividad?',
                                textScaler: TextScaler.linear(1.0),
                                style:
                                    TextStyle(color: CustomColors.primaryDark),
                              ),
                              content: const Text(
                                'Tus avances quedarán guardados',
                                textScaler: TextScaler.linear(1.0),
                                style:
                                    TextStyle(color: CustomColors.primaryDark),
                              ),
                              actions: [
                                TextButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                  child: const Text(
                                    'Continuar con la actividad',
                                    textScaler: TextScaler.linear(1.0),
                                    style:
                                        TextStyle(color: CustomColors.primary),
                                  ),
                                ),
                                TextButton(
                                  onPressed: () {
                                    String tiempo = DateTime.now().toString();
                                    FirestoreService().setTime(
                                        tiempo + ' - CLOSED - M2A3',
                                        'Salió de M2A3 sin terminar',
                                        '0 minutos (sin terminar)');
                                    FirestoreService().updateCurrentPage(
                                        'M2A3', (activeStep + 1).toString());
                                    Navigator.pushAndRemoveUntil(
                                        context,
                                        MaterialPageRoute<void>(
                                            builder: (BuildContext context) =>
                                                const MainPage(
                                                  activePage: 0,
                                                )),
                                        (route) => false);
                                  },
                                  child: const Text(
                                    'Salir',
                                    textScaler: TextScaler.linear(1.0),
                                    style: TextStyle(color: CustomColors.error),
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                        icon: const Icon(
                          Icons.close,
                          color: CustomColors.primaryDark,
                        ),
                      ),
                    ),
              const Spacer(),
              active5 ? Container() : favorito(),
            ],
          ),
          Text(
            headerText(),
            textScaler: const TextScaler.linear(1.0),
            textAlign: TextAlign.center,
            style: const TextStyle(
              color: CustomColors.primaryDark,
              fontSize: 24,
            ),
          ),
          active5 ? Container() : const Spacer(),
          active5
              ? header()
              : Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    headerTitleText(),
                    header(),
                  ],
                ),
        ],
      ),
    );
  }

  Widget favorito() {
    return StreamBuilder<QuerySnapshot>(
        stream: FirestoreService().favoritos(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasData) {
            return SizedBox(
              width: 50,
              height: 50,
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (context, index) {
                    var document = snapshot.data?.docs[index];
                    if (document != null && document.id == uid) {
                      switch (document['M2A3']) {
                        case true:
                          return IconButton(
                              icon: Icon(MdiIcons.star,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('M2A3', false)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Eliminado de favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                        case false:
                          return IconButton(
                              icon: Icon(MdiIcons.starOutline,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('M2A3', true)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Agregado a favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                      }
                    } else {
                      return Container();
                    }
                    return null;
                  }),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(
                color: Colors.red,
              ),
            );
          }
        });
  }

  Widget bottomStructure() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        nextButton(),
      ],
    );
  }

  Widget nextButton() {
    Size size = MediaQuery.of(context).size;
    String empty = '-';
    if (activeStep >= 1 && activeStep < 4) {
      return SizedBox(
        width: size.width / 1,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        WidgetStateProperty.all<Color>(CustomColors.primary3),
                    shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: const BorderSide(color: Colors.white)),
                    ),
                  ),
                  onPressed: () {
                    setState(() {
                      activeStep = activeStep - 1;
                    });
                  },
                  child: const Text(
                    'Anterior',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(color: Colors.white),
                  )),
            ),
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: WidgetStateProperty.all<Color>(active5
                      ? CustomColors.primary
                      : CustomColors.greenButton),
                  shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ),
                  ),
                ),
                onPressed: () {
                  if (activeStep < 4) {
                    if (activeStep == 3) {
                      if (_formKey.currentState!.validate()) {
                        FirestoreService()
                            .guardarRespuestasM2A3(
                                amistad1.text.isEmpty
                                    ? empty
                                    : amistad1.text.trim(),
                                amistad2.text.isEmpty
                                    ? empty
                                    : amistad2.text.trim(),
                                amistad3.text.isEmpty
                                    ? empty
                                    : amistad3.text.trim(),
                                familia1.text.isEmpty
                                    ? empty
                                    : familia1.text.trim(),
                                familia2.text.isEmpty
                                    ? empty
                                    : familia2.text.trim(),
                                familia3.text.isEmpty
                                    ? empty
                                    : familia3.text.trim(),
                                universidad1.text.isEmpty
                                    ? empty
                                    : universidad1.text.trim(),
                                universidad2.text.isEmpty
                                    ? empty
                                    : universidad2.text.trim(),
                                universidad3.text.isEmpty
                                    ? empty
                                    : universidad3.text.trim(),
                                comunidad1.text.isEmpty
                                    ? empty
                                    : comunidad1.text.trim(),
                                comunidad2.text.isEmpty
                                    ? empty
                                    : comunidad2.text.trim(),
                                comunidad3.text.isEmpty
                                    ? empty
                                    : comunidad3.text.trim())
                            .then(
                              (value) => setState(() {
                                activeStep++;
                              }),
                            );
                      } else {
                        showDialog<String>(
                          context: context,
                          builder: (BuildContext context) => AlertDialog(
                            title: const Text(
                              'Error',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  color: Colors.red,
                                  fontWeight: FontWeight.bold),
                            ),
                            content: const Text(
                              'Complete los campos faltantes',
                              textScaler: TextScaler.linear(1.0),
                            ),
                            actions: <Widget>[
                              TextButton(
                                onPressed: () => Navigator.pop(
                                  context,
                                ),
                                child: const Text(
                                  'OK',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      color: CustomColors.primaryDark,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                        );
                      }
                    } else {
                      setState(() {
                        activeStep++;
                      });
                    }
                  } else {
                    FirestoreService().terminaSesion('2', '3',
                        'Leyó la introducción → Completó la construcción de su Ecomapa → Terminó la actividad 9');
                    var tiempo = DateTime.now();
                    var difTiempo = tiempo.difference(timeStart);
                    FirestoreService().setTime(
                        tiempo.toString() + ' - ENDED - M2A3',
                        'Terminó M2A3',
                        difTiempo.inMinutes.toString() + ' minutos');
                    FirestoreService().updateCurrentPage('M2A3', '1');
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute<void>(
                            builder: (BuildContext context) => const MainPage(
                                  activePage: 0,
                                )),
                        (route) => false);
                  }
                },
                child: Text(
                  buttonText(),
                  textScaler: const TextScaler.linear(1.0),
                  style: const TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      return SizedBox(
        width: size.width / 1.2,
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor: WidgetStateProperty.all<Color>(
                active5 ? CustomColors.primary : CustomColors.greenButton),
            shape: WidgetStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              ),
            ),
          ),
          onPressed: () {
            if (activeStep < 4) {
              if (activeStep == 3) {
                if (_formKey.currentState!.validate()) {
                  FirestoreService()
                      .guardarRespuestasM2A3(
                          amistad1.text.isEmpty ? empty : amistad1.text.trim(),
                          amistad2.text.isEmpty ? empty : amistad2.text.trim(),
                          amistad3.text.isEmpty ? empty : amistad3.text.trim(),
                          familia1.text.isEmpty ? empty : familia1.text.trim(),
                          familia2.text.isEmpty ? empty : familia2.text.trim(),
                          familia3.text.isEmpty ? empty : familia3.text.trim(),
                          universidad1.text.isEmpty
                              ? empty
                              : universidad1.text.trim(),
                          universidad2.text.isEmpty
                              ? empty
                              : universidad2.text.trim(),
                          universidad3.text.isEmpty
                              ? empty
                              : universidad3.text.trim(),
                          comunidad1.text.isEmpty
                              ? empty
                              : comunidad1.text.trim(),
                          comunidad2.text.isEmpty
                              ? empty
                              : comunidad2.text.trim(),
                          comunidad3.text.isEmpty
                              ? empty
                              : comunidad3.text.trim())
                      .then(
                        (value) => setState(() {
                          activeStep++;
                        }),
                      );
                } else {
                  showDialog<String>(
                    context: context,
                    builder: (BuildContext context) => AlertDialog(
                      title: const Text(
                        'Error',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            color: Colors.red, fontWeight: FontWeight.bold),
                      ),
                      content: const Text(
                        'Complete los campos faltantes',
                        textScaler: TextScaler.linear(1.0),
                      ),
                      actions: <Widget>[
                        TextButton(
                          onPressed: () => Navigator.pop(
                            context,
                          ),
                          child: const Text(
                            'OK',
                            textScaler: TextScaler.linear(1.0),
                            style: TextStyle(
                                color: CustomColors.primaryDark,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                  );
                }
              } else {
                setState(() {
                  activeStep++;
                });
              }
            } else {
              FirestoreService().terminaSesion('2', '3',
                  'Leyó la introducción → Completó la construcción de su Ecomapa → Terminó la actividad 9');
              var tiempo = DateTime.now();
              var difTiempo = tiempo.difference(timeStart);
              FirestoreService().setTime(tiempo.toString() + ' - ENDED - M2A3',
                  'Terminó M2A3', difTiempo.inMinutes.toString() + ' minutos');
              FirestoreService().updateCurrentPage('M2A3', '1');
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute<void>(
                      builder: (BuildContext context) => const MainPage(
                            activePage: 0,
                          )),
                  (route) => false);
            }
          },
          child: Text(
            buttonText(),
            textScaler: const TextScaler.linear(1.0),
            style: TextStyle(
              color: active5 ? CustomColors.surface : CustomColors.primaryDark,
            ),
          ),
        ),
      );
    }
  }

  Widget header() {
    Size size = MediaQuery.of(context).size;
    switch (activeStep) {
      case 4:
        return Image(
          width: size.width / 8,
          height: size.width / 8,
          fit: BoxFit.scaleDown,
          image: AssetImage(imageHeader()),
        );
      default:
        return Image(
          width: kIsWeb ? 100 : size.width / 3,
          height: kIsWeb ? 100 : size.width / 3,
          fit: BoxFit.scaleDown,
          image: AssetImage(imageHeader()),
        );
    }
  }

  Widget headerTitleText() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        return Container();

      case 4:
        return Container();
      default:
        return SizedBox(
          width: size.width / 2,
          child: Text(
            tituloHeader(),
            textScaler: const TextScaler.linear(1.0),
            style: const TextStyle(
                fontSize: 18,
                color: CustomColors.primaryDark,
                fontWeight: FontWeight.bold),
          ),
        );
    }
  }

  String tituloHeader() {
    switch (activeStep) {
      case 1:
        return '“¿Quiénes forman parte de tus redes?”';
      case 2:
        return '“¿Cómo es tu relación con esas personas?”';
      case 3:
        return '“¡Construye tu Ecomapa!”';

      case 4:
        return '';

      default:
        return '';
    }
  }

  String imageHeader() {
    switch (activeStep) {
      case 0:
        return 'assets/images/m2a3.png';

      case 4:
        return 'assets/images/end.png';

      default:
        return 'assets/images/m2a3.png';
    }
  }

  String headerText() {
    switch (activeStep) {
      case 4:
        return '';
      default:
        return 'Tu red de apoyo';
    }
  }

  String buttonText() {
    switch (activeStep) {
      case 0:
        return 'Comenzar';
      case 3:
        return 'Finalizar';
      case 4:
        return 'Continuar con el programa';
      default:
        return 'Siguiente';
    }
  }

  Widget content() {
    switch (activeStep) {
      case 0:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.all(20),
                    child: RichText(
                      text: const TextSpan(
                        text: '',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                '¡Te queremos invitar a construir tu propio ecomapa! ¿Quieres intentarlo? Por medio de una serie de tres pasos, podrás contar con un mapa de tu red de apoyo que permitirá visualizar de forma gráfica su presencia y estado de la relación.',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );

      case 1:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });
        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.all(20),
                    child: RichText(
                      text: const TextSpan(
                        text: '',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                'Tómate unos minutos para pensar en quiénes son las personas con quienes te relacionas en tu vida cotidiana (ej.: padres, amigos, colegas, compañerxs, etc). Si quieres, puedes escribirlos en una hoja para tenerlos presentes.',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 2:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });
        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.all(20),
                    child: RichText(
                      text: const TextSpan(
                        text: '',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                'Es común que tengamos amistades que se sienten más cercanas, cómodas o confiables, mientras que otras, distantes o incómodas. Ten presente estas características mientras vayas construyendo el ecomapa.',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 3:
        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.all(20),
                    child: RichText(
                      text: const TextSpan(
                        text: '',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                '¡Es hora de la construcción! Escribe el nombre de la persona y ubícalo en un lugar del ecomapa. Recuerda que quienes ubiques en los primeros lugares son aquellas personas con quienes tienes mayor cercanía y confianza; y viceversa.',
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.all(10),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        children: [
                          Container(
                            margin: const EdgeInsets.only(
                                left: 10, right: 10, bottom: 0, top: 10),
                            alignment: Alignment.centerLeft,
                            child: const Text(
                              'Amistades',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 22, color: CustomColors.surface),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: amistad1,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: '1.',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: amistad2,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: '2.',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: amistad3,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: '3.',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(
                                left: 10, right: 10, bottom: 0, top: 30),
                            alignment: Alignment.centerLeft,
                            child: const Text(
                              'Familia',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 22, color: CustomColors.surface),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: familia1,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: '1.',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: familia2,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: '2.',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: familia3,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: '3.',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(
                                left: 10, right: 10, bottom: 0, top: 30),
                            alignment: Alignment.centerLeft,
                            child: const Text(
                              'Universidad',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 22, color: CustomColors.surface),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: universidad1,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: '1.',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: universidad2,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: '2.',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: universidad3,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: '3.',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(
                                left: 10, right: 10, bottom: 0, top: 30),
                            alignment: Alignment.centerLeft,
                            child: const Text(
                              'Comunidad',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 22, color: CustomColors.surface),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: comunidad1,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: '1.',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: comunidad2,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: '2.',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: comunidad3,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: '3.',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 4:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });
        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Container(
                margin: const EdgeInsets.all(15),
                child: Column(
                  children: [
                    topStructure(),
                    Container(
                      margin: const EdgeInsets.all(10),
                      child: const FittedBox(
                        fit: BoxFit.fitWidth,
                        child: Text(
                          '¡Muy bien!',
                          textScaler: TextScaler.linear(1.0),
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 24),
                        ),
                      ),
                    ),
                    const Text(
                      'Tenemos una recompensa para ti:',
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(fontSize: 16),
                      textAlign: TextAlign.center,
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 40, bottom: 10),
                      decoration: BoxDecoration(
                          color: CustomColors.lightContainer,
                          border:
                              Border.all(width: 2, color: CustomColors.surface),
                          borderRadius: BorderRadius.circular(15)),
                      child: Row(
                        children: [
                          const Image(
                            width: 100,
                            height: 120,
                            fit: BoxFit.scaleDown,
                            image: AssetImage('assets/images/m2a3.png'),
                          ),
                          Container(
                            margin: const EdgeInsets.only(left: 10),
                            child: const Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Trabajo por mi bienestar:',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      fontSize: 13,
                                      color: CustomColors.primaryDark),
                                ),
                                Text(
                                  'Construyo mi ecomapa',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16,
                                      color: CustomColors.primaryDark),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 10, bottom: 10),
                      decoration: BoxDecoration(
                          color: CustomColors.lightContainer,
                          border:
                              Border.all(width: 2, color: CustomColors.surface),
                          borderRadius: BorderRadius.circular(15)),
                      child: Row(
                        children: [
                          const Image(
                            width: 100,
                            height: 120,
                            fit: BoxFit.scaleDown,
                            image: AssetImage(
                                'assets/images/hablemos-ciberbullying-icon.png'),
                          ),
                          Container(
                            margin: const EdgeInsets.only(left: 10),
                            child: const Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Actividad complementaria:',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      fontSize: 13,
                                      color: CustomColors.primaryDark),
                                ),
                                Text(
                                  'Video hablemos de\nciberbullying',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16,
                                      color: CustomColors.primaryDark),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      default:
        return Container();
    }
  }
}
