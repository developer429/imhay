// ignore_for_file:   avoid_unnecessary_containers, sized_box_for_whitespace, prefer_typing_uninitialized_variables, file_names, avoid_print, unused_local_variable

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:seguimiento/colors.dart';
import 'package:seguimiento/notifications_cloud.dart';
import '../../firebase.dart';

class SientesPQH4 extends StatefulWidget {
  const SientesPQH4({
    Key? key,
  }) : super(key: key);

  @override
  State<SientesPQH4> createState() => _SientesPQH4State();
}

enum OptionsQuestions { zero, one, two, three }

class _SientesPQH4State extends State<SientesPQH4> {
  String uid = FirebaseAuth.instance.currentUser!.uid;
  bool loadingData = true;
  bool isAvailable = false;
  bool hintIsVisible = true;
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('page-control');
  String activeStepData = '';

  ScrollController scrollController = ScrollController();

  int phq4First = 0;
  int phq4Second = 0;
  String diasRestantes = '';
  String startDate = '';
  CollectionReference dateReference =
      FirebaseFirestore.instance.collection('usuarios');
  void getFechaInicio() {
    dateReference.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        startDate = fields['fechaInicio'];
        String fechaActual = DateFormat('yyy-MM-dd').format(DateTime.now());
        int diferencia = DateTime.parse(fechaActual)
            .difference(DateTime.parse(startDate))
            .inDays;

        phq4First = (14 - diferencia);
        phq4Second = (28 - diferencia);

        if (phq4First < phq4Second && phq4First > 0) {
          diasRestantes = phq4First.toString();
        } else {
          diasRestantes = phq4Second.toString();
        }
        if (phq4Second < 0) {
          diasRestantes = '0';
        }
        if (phq4First == 0 || phq4First == -1) {
          isAvailable = true;
          hintIsVisible = false;
        }
        if (phq4Second == 0 || phq4Second == -1) {
          isAvailable = true;
          hintIsVisible = false;
        }
        if (phq4Second < -1) {
          isAvailable = false;
          hintIsVisible = false;
        }
        Future.delayed(const Duration(milliseconds: 500), () {
          setState(() {
            loadingData = false;
          });
        });
      });
    }).catchError((err) {
      //print('Error: $err');
    });
  }

  List<String> respuestasPHQ4 = [];

  CollectionReference phq1Ref = FirebaseFirestore.instance.collection('phq4');
  void getPHQ4TimesDone() {
    phq1Ref.doc(uid).collection('respuestas').get().then((value) {
      var fields = value;
      setState(() {
        for (var data in fields.docs) {
          respuestasPHQ4.add(data['alerta']);
        }

        print(respuestasPHQ4.isEmpty);
        print(phq4First);
        print(phq4Second);

        if (respuestasPHQ4.isEmpty && phq4First <= 0) {
          isAvailable = true;
          hintIsVisible = false;
        }
        if (respuestasPHQ4.length < 2 && phq4Second <= 0) {
          isAvailable = true;
          hintIsVisible = false;
        }
      });
    }).catchError((err) {
      //print('Error: $err');
    });
  }

  int activeStep = 0;
  int upperBound = 4;
  OptionsQuestions? _question1 = OptionsQuestions.zero;
  OptionsQuestions? _question2 = OptionsQuestions.zero;
  OptionsQuestions? _question3 = OptionsQuestions.zero;
  OptionsQuestions? _question4 = OptionsQuestions.zero;
  OptionsQuestions? _question5 = OptionsQuestions.zero;
  int processData(var result) {
    int valorFinal = 0;
    switch (result) {
      case OptionsQuestions.zero:
        valorFinal = 0;
        break;
      case OptionsQuestions.one:
        valorFinal = 1;
        break;
      case OptionsQuestions.two:
        valorFinal = 2;
        break;
      case OptionsQuestions.three:
        valorFinal = 3;
        break;
      default:
    }

    return valorFinal;
  }

  @override
  void initState() {
    super.initState();
    getFechaInicio();
    //getPHQ4TimesDone();
    //getActivePageStored();

    //getFavData();
  }

  @override
  void dispose() {
    super.dispose();
  }

  bool active1 = false;
  bool active2 = false;
  bool active3 = false;
  bool active4 = false;
  bool active5 = false;
  bool active6 = false;
  bool active7 = false;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        active1 = true;
        active2 = false;
        active3 = false;
        active4 = false;
        active5 = false;
        active6 = false;
        active7 = false;

        break;
      case 1:
        active1 = true;
        active2 = true;
        active3 = false;
        active4 = false;
        active5 = false;
        active6 = false;
        active7 = false;

        break;
      case 2:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = false;
        active5 = false;
        active6 = false;
        active7 = false;

        break;
      case 3:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = false;
        active6 = false;
        active7 = false;

        break;
      case 4:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = true;
        active6 = false;
        active7 = false;

        break;
      case 5:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = true;
        active6 = true;
        active7 = false;

        break;
      case 6:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = true;
        active6 = true;
        active7 = true;

        break;
      default:
    }
    return PopScope(
      canPop: false,
      onPopInvokedWithResult: (bool didPop, result) {
        if (didPop) {
          FirestoreService()
              .updateCurrentPage('SientesPQH4', (activeStep + 1).toString());
          return;
        }
      },
      child: loadingData
          ? Container(
              width: size.width,
              height: size.height,
              color: CustomColors.primaryDark,
              child: const Center(
                  child: CircularProgressIndicator(
                color: CustomColors.tertiary70,
              )))
          : Container(
              color: Colors.black,
              child: SafeArea(
                child: Scaffold(
                  backgroundColor: CustomColors.background,
                  body: Center(
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              width: size.width / 7,
                              height: 5,
                              color: active1
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 7,
                              height: 5,
                              color: active2
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 7,
                              height: 5,
                              color: active3
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 7,
                              height: 5,
                              color: active4
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 7,
                              height: 5,
                              color: active5
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 7,
                              height: 5,
                              color: active6
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 7,
                              height: 5,
                              color: active7
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                          ],
                        ),
                        content(),
                        Container(
                          margin: const EdgeInsets.only(top: 10, bottom: 10),
                          child: bottomStructure(),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
    );
  }

  Widget topStructure() {
    return Container(
      child: Stack(
        children: [
          Column(
            children: [
              Row(
                children: [
                  Container(
                    alignment: Alignment.topRight,
                    child: IconButton(
                      onPressed: () {
                        FirestoreService()
                            .updateCurrentPage(
                                'SientesPQH4', (activeStep + 1).toString())
                            .then((value) => Navigator.of(context).pop());
                      },
                      icon: const Icon(
                        Icons.close,
                        color: CustomColors.primaryDark,
                      ),
                    ),
                  ),
                ],
              ),
              Text(
                headerText(),
                textScaler: const TextScaler.linear(1.0),
                textAlign: TextAlign.center,
                style: const TextStyle(
                  color: CustomColors.primaryDark,
                  fontSize: 24,
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 20, bottom: 20),
                child: header(),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget bottomStructure() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        nextButton(),
      ],
    );
  }

  Widget nextButton() {
    Size size = MediaQuery.of(context).size;
    if (activeStep >= 1 && activeStep < 6) {
      return Container(
        width: size.width / 1,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(
              width: size.width / 2.5,
              child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        WidgetStateProperty.all<Color>(CustomColors.background),
                    shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: const BorderSide(color: CustomColors.primary)),
                    ),
                  ),
                  onPressed: () {
                    setState(() {
                      activeStep = activeStep - 1;
                    });
                  },
                  child: const Text(
                    'Anterior',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(color: CustomColors.primary),
                  )),
            ),
            Container(
              width: size.width / 2.5,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor:
                      WidgetStateProperty.all<Color>(CustomColors.primary),
                  shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ),
                  ),
                ),
                onPressed: () async {
                  if (activeStep < 6) {
                    setState(() {
                      activeStep++;
                    });
                  } else {
                    final response = await NotificationService()
                        .sendAnswersPHQ4(
                          uid,
                          processData(_question1),
                          processData(_question2),
                          processData(_question3),
                          processData(_question4),
                          processData(_question5),
                        )
                        .then((value) => Navigator.of(context).pop())
                        .catchError((err) {
                      setState(() {
                        Navigator.of(context).pop();
                      });
                    });
                  }
                },
                child: Text(
                  buttonText(),
                  textScaler: const TextScaler.linear(1.0),
                  style: const TextStyle(
                      color: CustomColors.background,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      return Container(
        width: size.width / 1.2,
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor: WidgetStateProperty.all<Color>(isAvailable
                ? active7
                    ? CustomColors.greenButton
                    : CustomColors.primary
                : const Color.fromARGB(37, 0, 0, 0)),
            shape: WidgetStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              ),
            ),
          ),
          onPressed: isAvailable
              ? () async {
                  if (activeStep < 6) {
                    setState(() {
                      activeStep++;
                    });
                  } else {
                    final response = await NotificationService()
                        .sendAnswersPHQ4(
                          uid,
                          processData(_question1),
                          processData(_question2),
                          processData(_question3),
                          processData(_question4),
                          processData(_question5),
                        )
                        .then((value) => Navigator.of(context).pop())
                        .catchError((err) {
                      setState(() {
                        Navigator.of(context).pop();
                      });
                    });
                  }
                }
              : null,
          child: Text(
            buttonText(),
            textScaler: const TextScaler.linear(1.0),
            style: TextStyle(
              color:
                  active7 ? CustomColors.primaryDark : CustomColors.background,
            ),
          ),
        ),
      );
    }
  }

  Widget header() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      default:
        return Image(
          width: size.width / 3,
          height: size.width / 3,
          fit: BoxFit.scaleDown,
          image: AssetImage(imageHeader()),
        );
    }
  }

  String imageHeader() {
    switch (activeStep) {
      case 0:
        return 'assets/images/phq4-icon.png';

      default:
        return 'assets/images/phq4-icon.png';
    }
  }

  String headerText() {
    switch (activeStep) {
      case 0:
        return 'Cuéntanos cómo te sientes';

      default:
        return 'Cuéntanos cómo te sientes';
    }
  }

  String buttonText() {
    switch (activeStep) {
      case 0:
        return 'Comenzar';
      case 6:
        return 'Finalizar';

      default:
        return 'Siguiente ';
    }
  }

  Widget content() {
    Size size = MediaQuery.of(context).size;
    switch (activeStep) {
      case 0:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 5, right: 20, left: 20, bottom: 5),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Queremos saber como te sientes, responde 5 preguntas que tenemos para ti.',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[],
                      ),
                    ),
                  ),
                  Visibility(
                    visible: hintIsVisible,
                    child: Container(
                      margin: const EdgeInsets.only(top: 150),
                      padding: const EdgeInsets.all(10),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: CustomColors.primaryDark),
                      child: Text(
                        'Podrás responder en ' + diasRestantes + ' día(s).',
                        textScaler: const TextScaler.linear(1.0),
                        style: const TextStyle(
                            color: CustomColors.background, fontSize: 16),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 1:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 5, right: 20, left: 20, bottom: 5),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Durante las últimas 2 semanas ¿con qué frecuencia le han molestado los siguientes problemas?',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[],
                      ),
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: const EdgeInsets.only(top: 10, left: 20, right: 20),
                    child: const Text(
                      'Se ha sentido nervioso/a, ansioso/a o muy alterado/a',
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.primary,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.only(left: 20, right: 10),
                    child: Column(
                      children: [
                        ListTile(
                          title: const Text(
                            'Nunca',
                            textScaler: TextScaler.linear(1.0),
                          ),
                          leading: Radio<OptionsQuestions>(
                            value: OptionsQuestions.zero,
                            groupValue: _question1,
                            onChanged: (OptionsQuestions? value) {
                              setState(() {
                                _question1 = value;
                                print(_question1);
                              });
                            },
                          ),
                        ),
                        const Divider(),
                        ListTile(
                          title: const Text(
                            'Varios días',
                            textScaler: TextScaler.linear(1.0),
                          ),
                          leading: Radio<OptionsQuestions>(
                            value: OptionsQuestions.one,
                            groupValue: _question1,
                            onChanged: (OptionsQuestions? value) {
                              setState(() {
                                _question1 = value;
                                print(_question1);
                              });
                            },
                          ),
                        ),
                        const Divider(),
                        ListTile(
                          title: const Text(
                            'Más de la mitad de los días',
                            textScaler: TextScaler.linear(1.0),
                          ),
                          leading: Radio<OptionsQuestions>(
                            value: OptionsQuestions.two,
                            groupValue: _question1,
                            onChanged: (OptionsQuestions? value) {
                              setState(() {
                                _question1 = value;
                                print(_question1);
                              });
                            },
                          ),
                        ),
                        const Divider(),
                        ListTile(
                          title: const Text(
                            'Casi todos los días',
                            textScaler: TextScaler.linear(1.0),
                          ),
                          leading: Radio<OptionsQuestions>(
                            value: OptionsQuestions.three,
                            groupValue: _question1,
                            onChanged: (OptionsQuestions? value) {
                              setState(() {
                                _question1 = value;
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 2:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 5, right: 20, left: 20, bottom: 5),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Durante las últimas 2 semanas ¿con qué frecuencia le han molestado los siguientes problemas?',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[],
                      ),
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: const EdgeInsets.only(top: 10, left: 20, right: 20),
                    child: const Text(
                      'No ha podido dejar de preocuparse',
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.primary,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.only(left: 20, right: 10),
                    child: Column(
                      children: [
                        ListTile(
                          title: const Text(
                            'Nunca',
                            textScaler: TextScaler.linear(1.0),
                          ),
                          leading: Radio<OptionsQuestions>(
                            value: OptionsQuestions.zero,
                            groupValue: _question2,
                            onChanged: (OptionsQuestions? value) {
                              setState(() {
                                _question2 = value;
                              });
                            },
                          ),
                        ),
                        const Divider(),
                        ListTile(
                          title: const Text(
                            'Varios días',
                            textScaler: TextScaler.linear(1.0),
                          ),
                          leading: Radio<OptionsQuestions>(
                            value: OptionsQuestions.one,
                            groupValue: _question2,
                            onChanged: (OptionsQuestions? value) {
                              setState(() {
                                _question2 = value;
                              });
                            },
                          ),
                        ),
                        const Divider(),
                        ListTile(
                          title: const Text(
                            'Más de la mitad de los días',
                            textScaler: TextScaler.linear(1.0),
                          ),
                          leading: Radio<OptionsQuestions>(
                            value: OptionsQuestions.two,
                            groupValue: _question2,
                            onChanged: (OptionsQuestions? value) {
                              setState(() {
                                _question2 = value;
                              });
                            },
                          ),
                        ),
                        const Divider(),
                        ListTile(
                          title: const Text(
                            'Casi todos los días',
                            textScaler: TextScaler.linear(1.0),
                          ),
                          leading: Radio<OptionsQuestions>(
                            value: OptionsQuestions.three,
                            groupValue: _question2,
                            onChanged: (OptionsQuestions? value) {
                              setState(() {
                                _question2 = value;
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 3:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 5, right: 20, left: 20, bottom: 5),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Durante las últimas 2 semanas ¿con qué frecuencia le han molestado los siguientes problemas?',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[],
                      ),
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: const EdgeInsets.only(top: 10, left: 20, right: 20),
                    child: const Text(
                      'Sentirse desanimado/a, deprimido/a o sin esperanza',
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.primary,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.only(left: 20, right: 10),
                    child: Column(
                      children: [
                        ListTile(
                          title: const Text(
                            'Nunca',
                            textScaler: TextScaler.linear(1.0),
                          ),
                          leading: Radio<OptionsQuestions>(
                            value: OptionsQuestions.zero,
                            groupValue: _question3,
                            onChanged: (OptionsQuestions? value) {
                              setState(() {
                                _question3 = value;
                              });
                            },
                          ),
                        ),
                        const Divider(),
                        ListTile(
                          title: const Text(
                            'Varios días',
                            textScaler: TextScaler.linear(1.0),
                          ),
                          leading: Radio<OptionsQuestions>(
                            value: OptionsQuestions.one,
                            groupValue: _question3,
                            onChanged: (OptionsQuestions? value) {
                              setState(() {
                                _question3 = value;
                              });
                            },
                          ),
                        ),
                        const Divider(),
                        ListTile(
                          title: const Text(
                            'Más de la mitad de los días',
                            textScaler: TextScaler.linear(1.0),
                          ),
                          leading: Radio<OptionsQuestions>(
                            value: OptionsQuestions.two,
                            groupValue: _question3,
                            onChanged: (OptionsQuestions? value) {
                              setState(() {
                                _question3 = value;
                              });
                            },
                          ),
                        ),
                        const Divider(),
                        ListTile(
                          title: const Text(
                            'Casi todos los días',
                            textScaler: TextScaler.linear(1.0),
                          ),
                          leading: Radio<OptionsQuestions>(
                            value: OptionsQuestions.three,
                            groupValue: _question3,
                            onChanged: (OptionsQuestions? value) {
                              setState(() {
                                _question3 = value;
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 4:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });
        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 5, right: 20, left: 20, bottom: 5),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Durante las últimas 2 semanas ¿con qué frecuencia le han molestado los siguientes problemas?',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[],
                      ),
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: const EdgeInsets.only(top: 10, left: 20, right: 20),
                    child: const Text(
                      'Tener poco interés en hacer las cosas',
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.primary,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.only(left: 20, right: 10),
                    child: Column(
                      children: [
                        ListTile(
                          title: const Text(
                            'Nunca',
                            textScaler: TextScaler.linear(1.0),
                          ),
                          leading: Radio<OptionsQuestions>(
                            value: OptionsQuestions.zero,
                            groupValue: _question4,
                            onChanged: (OptionsQuestions? value) {
                              setState(() {
                                _question4 = value;
                              });
                            },
                          ),
                        ),
                        const Divider(),
                        ListTile(
                          title: const Text(
                            'Varios días',
                            textScaler: TextScaler.linear(1.0),
                          ),
                          leading: Radio<OptionsQuestions>(
                            value: OptionsQuestions.one,
                            groupValue: _question4,
                            onChanged: (OptionsQuestions? value) {
                              setState(() {
                                _question4 = value;
                              });
                            },
                          ),
                        ),
                        const Divider(),
                        ListTile(
                          title: const Text(
                            'Más de la mitad de los días',
                            textScaler: TextScaler.linear(1.0),
                          ),
                          leading: Radio<OptionsQuestions>(
                            value: OptionsQuestions.two,
                            groupValue: _question4,
                            onChanged: (OptionsQuestions? value) {
                              setState(() {
                                _question4 = value;
                              });
                            },
                          ),
                        ),
                        const Divider(),
                        ListTile(
                          title: const Text(
                            'Casi todos los días',
                            textScaler: TextScaler.linear(1.0),
                          ),
                          leading: Radio<OptionsQuestions>(
                            value: OptionsQuestions.three,
                            groupValue: _question4,
                            onChanged: (OptionsQuestions? value) {
                              setState(() {
                                _question4 = value;
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 5:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 5, right: 20, left: 20, bottom: 5),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Durante las últimas 2 semanas ¿con qué frecuencia le han molestado los siguientes problemas?',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[],
                      ),
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: const EdgeInsets.only(top: 10, left: 20, right: 20),
                    child: const Text(
                      'Se le han ocurrido pensamientos de que sería mejor estar muerto o de que se haría daño de alguna manera ',
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.primary,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.only(left: 20, right: 10),
                    child: Column(
                      children: [
                        ListTile(
                          title: const Text(
                            'Nunca',
                            textScaler: TextScaler.linear(1.0),
                          ),
                          leading: Radio<OptionsQuestions>(
                            value: OptionsQuestions.zero,
                            groupValue: _question5,
                            onChanged: (OptionsQuestions? value) {
                              setState(() {
                                _question5 = value;
                              });
                            },
                          ),
                        ),
                        const Divider(),
                        ListTile(
                          title: const Text(
                            'Varios días',
                            textScaler: TextScaler.linear(1.0),
                          ),
                          leading: Radio<OptionsQuestions>(
                            value: OptionsQuestions.one,
                            groupValue: _question5,
                            onChanged: (OptionsQuestions? value) {
                              setState(() {
                                _question5 = value;
                              });
                            },
                          ),
                        ),
                        const Divider(),
                        ListTile(
                          title: const Text(
                            'Más de la mitad de los días',
                            textScaler: TextScaler.linear(1.0),
                          ),
                          leading: Radio<OptionsQuestions>(
                            value: OptionsQuestions.two,
                            groupValue: _question5,
                            onChanged: (OptionsQuestions? value) {
                              setState(() {
                                _question5 = value;
                              });
                            },
                          ),
                        ),
                        const Divider(),
                        ListTile(
                          title: const Text(
                            'Casi todos los días',
                            textScaler: TextScaler.linear(1.0),
                          ),
                          leading: Radio<OptionsQuestions>(
                            value: OptionsQuestions.three,
                            groupValue: _question5,
                            onChanged: (OptionsQuestions? value) {
                              setState(() {
                                _question5 = value;
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 6:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 5, right: 20, left: 20, bottom: 5),
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: const TextSpan(
                        text:
                            'Gracias por contarnos cómo te sientes, así podremos apoyarte de mejor manera',
                        style: TextStyle(
                            fontSize: 22,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      default:
        return Container();
    }
  }
}
