// ignore_for_file: avoid_print, prefer_typing_uninitialized_variables

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:seguimiento/colors.dart';
import '../../firebase.dart';
import '../MainPage.dart';

class M1A0 extends StatefulWidget {
  const M1A0({
    Key? key,
  }) : super(key: key);

  @override
  State<M1A0> createState() => _M1A0State();
}

class _M1A0State extends State<M1A0> {
  String uid = FirebaseAuth.instance.currentUser!.uid;
  bool loadingData = true;
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('page-control');
  String activeStepData = '';
  void getActivePageStored() {
    collectionReference.doc(uid).get().then((value) {
      var fields = value;
      activeStepData = fields['M1A0'];
      setState(() {
        if (int.parse(activeStepData) < 6) {
          activeStep = int.parse(activeStepData) - 1;
          Future.delayed(const Duration(milliseconds: 500), () {
            setState(() {
              loadingData = false;
            });
          });
        }
      });
    });
  }

  ScrollController scrollController = ScrollController();

  bool favorite = false;
  CollectionReference favoriteData =
      FirebaseFirestore.instance.collection('favoritos');
  void getFavData() {
    favoriteData.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        favorite = fields['M1A0'];
      });
    }).catchError((err) {
      print('Error: $err');
    });
  }

  int activeStep = 0;
  var timeStart;

  @override
  void initState() {
    super.initState();
    getActivePageStored();
    getFavData();
    var tiempo = DateTime.now();
    timeStart = tiempo;
    FirestoreService().setTime(tiempo.toString() + ' - START - M1A0',
        'Comenzó M1A0', '0 minutos (comenzando)');
  }

  @override
  void dispose() {
    super.dispose();
  }

  bool active1 = false;
  bool active2 = false;
  bool active3 = false;
  bool active4 = false;
  bool active5 = false;
  bool active6 = false;
  bool active7 = false;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    switch (activeStep) {
      case 0:
        active1 = true;
        active2 = false;
        active3 = false;
        active4 = false;
        active5 = false;
        active6 = false;
        active7 = false;

        break;
      case 1:
        active1 = true;
        active2 = true;
        active3 = false;
        active4 = false;
        active5 = false;
        active6 = false;
        active7 = false;

        break;
      case 2:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = false;
        active5 = false;
        active6 = false;
        active7 = false;
        break;
      case 3:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = false;
        active6 = false;
        active7 = false;
        break;
      case 4:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = true;
        active6 = false;
        active7 = false;
        break;
      case 5:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = true;
        active6 = true;
        active7 = false;
        break;
      case 6:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = true;
        active6 = true;
        active7 = true;
        break;
      default:
    }
    return PopScope(
      canPop: false,
      onPopInvokedWithResult: (bool didPop, result) {
        if (didPop) {
          return;
        }
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(32.0))),
            title: const Text(
              '¿Estás seguro que deseas salir de la actividad?',
              textScaler: TextScaler.linear(1.0),
              style: TextStyle(color: CustomColors.primaryDark),
            ),
            content: const Text(
              'Tus avances quedarán guardados',
              textScaler: TextScaler.linear(1.0),
              style: TextStyle(color: CustomColors.primaryDark),
            ),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: const Text(
                  'Continuar con la actividad',
                  textScaler: TextScaler.linear(1.0),
                  style: TextStyle(color: CustomColors.primary),
                ),
              ),
              TextButton(
                onPressed: () {
                  FirestoreService()
                      .updateCurrentPage('M1A0', (activeStep + 1).toString());
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute<void>(
                          builder: (BuildContext context) => const MainPage(
                                activePage: 0,
                              )),
                      (route) => false);
                },
                child: const Text(
                  'Salir',
                  textScaler: TextScaler.linear(1.0),
                  style: TextStyle(color: CustomColors.error),
                ),
              ),
            ],
          ),
        );
      },
      child: loadingData
          ? Container(
              width: size.width,
              height: size.height,
              color: CustomColors.primaryDark,
              child: const Center(
                  child: CircularProgressIndicator(
                color: CustomColors.tertiary70,
              )))
          : Container(
              color: Colors.black,
              child: SafeArea(
                child: Scaffold(
                  backgroundColor: CustomColors.primary3,
                  body: Center(
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              width: size.width / 7,
                              height: 5,
                              color: active1
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 7,
                              height: 5,
                              color: active2
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 7,
                              height: 5,
                              color: active3
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 7,
                              height: 5,
                              color: active4
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 7,
                              height: 5,
                              color: active5
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 7,
                              height: 5,
                              color: active6
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 7,
                              height: 5,
                              color: active7
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                          ],
                        ),
                        content(),
                        Container(
                          margin: const EdgeInsets.only(top: 10, bottom: 10),
                          child: bottomStructure(),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
    );
  }

  Widget topStructure() {
    Size size = MediaQuery.of(context).size;

    return Container(
      width: size.width,
      height: size.height / 3,
      color: CustomColors.greenList,
      child: Column(
        children: [
          Row(
            children: [
              Container(
                alignment: Alignment.topRight,
                child: IconButton(
                  onPressed: () {
                    showDialog(
                      context: context,
                      builder: (context) => AlertDialog(
                        shape: const RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(32.0))),
                        title: const Text(
                          '¿Estás seguro que deseas salir de la actividad?',
                          textScaler: TextScaler.linear(1.0),
                          style: TextStyle(color: CustomColors.primaryDark),
                        ),
                        content: const Text(
                          'Tus avances quedarán guardados',
                          textScaler: TextScaler.linear(1.0),
                          style: TextStyle(color: CustomColors.primaryDark),
                        ),
                        actions: [
                          TextButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: const Text(
                              'Continuar con la actividad',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(color: CustomColors.primary),
                            ),
                          ),
                          TextButton(
                            onPressed: () {
                              String tiempo = DateTime.now().toString();
                              FirestoreService().setTime(
                                  tiempo + ' - CLOSED - M1A0',
                                  'Salió de M1A0 sin terminar',
                                  '0 minutos (sin terminar)');
                              FirestoreService().updateCurrentPage(
                                  'M1A0', (activeStep + 1).toString());
                              Navigator.pushAndRemoveUntil(
                                  context,
                                  MaterialPageRoute<void>(
                                      builder: (BuildContext context) =>
                                          const MainPage(
                                            activePage: 0,
                                          )),
                                  (route) => false);
                            },
                            child: const Text(
                              'Salir',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(color: CustomColors.error),
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                  icon: const Icon(
                    Icons.close,
                    color: CustomColors.primaryDark,
                  ),
                ),
              ),
              const Spacer(),
              favorito(),
            ],
          ),
          Text(
            headerText(),
            textScaler: const TextScaler.linear(1.0),
            textAlign: TextAlign.center,
            style: const TextStyle(
              color: CustomColors.primaryDark,
              fontSize: 26,
            ),
          ),
          const Spacer(),
          header()
        ],
      ),
    );
  }

  Widget bottomStructure() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        nextButton(),
      ],
    );
  }

  Widget nextButton() {
    Size size = MediaQuery.of(context).size;
    if (activeStep >= 1 && activeStep < 7) {
      return SizedBox(
        width: size.width / 1,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        WidgetStateProperty.all<Color>(CustomColors.primary3),
                    shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: const BorderSide(color: Colors.white)),
                    ),
                  ),
                  onPressed: () {
                    setState(() {
                      activeStep = activeStep - 1;
                    });
                  },
                  child: const Text(
                    'Anterior',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(color: Colors.white),
                  )),
            ),
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor:
                      WidgetStateProperty.all<Color>(CustomColors.greenButton),
                  shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ),
                  ),
                ),
                onPressed: () {
                  if (activeStep < 6) {
                    setState(() {
                      activeStep++;
                    });
                  } else {
                    FirestoreService().terminaSesion('1', '0',
                        'Leyó la bienvenida → Vio la guía de uso de la aplicación → Terminó la actividad 0');
                    var tiempo = DateTime.now();
                    var difTiempo = tiempo.difference(timeStart);
                    FirestoreService().setTime(
                        tiempo.toString() + ' - ENDED - M1A0',
                        'Terminó M1A0',
                        difTiempo.inMinutes.toString() + ' minutos');
                    FirestoreService().updateCurrentPage('M1A0', '1');
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute<void>(
                            builder: (BuildContext context) => const MainPage(
                                  activePage: 0,
                                )),
                        (route) => false);
                  }
                },
                child: Text(
                  buttonText(),
                  textScaler: const TextScaler.linear(1.0),
                  style: const TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      return SizedBox(
        width: size.width / 1.2,
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor:
                WidgetStateProperty.all<Color>(CustomColors.greenButton),
            shape: WidgetStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              ),
            ),
          ),
          onPressed: () {
            if (activeStep < 6) {
              setState(() {
                activeStep++;
              });
            } else {
              FirestoreService().terminaSesion('1', '0',
                  'Leyó la bienvenida → Vio la guía de uso de la aplicación → Terminó la actividad 0');
              var tiempo = DateTime.now();
              var difTiempo = tiempo.difference(timeStart);
              FirestoreService().setTime(tiempo.toString() + ' - ENDED - M1A0',
                  'Terminó M1A0', difTiempo.inMinutes.toString() + ' minutos');
              FirestoreService().updateCurrentPage('M1A0', '1');
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute<void>(
                      builder: (BuildContext context) => const MainPage(
                            activePage: 0,
                          )),
                  (route) => false);
            }
          },
          child: Text(
            buttonText(),
            textScaler: const TextScaler.linear(1.0),
            style: const TextStyle(color: CustomColors.primaryDark),
          ),
        ),
      );
    }
  }

  Widget favorito() {
    return StreamBuilder<QuerySnapshot>(
        stream: FirestoreService().favoritos(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasData) {
            return SizedBox(
              width: 50,
              height: 50,
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (context, index) {
                    var document = snapshot.data?.docs[index];
                    if (document != null && document.id == uid) {
                      switch (document['M1A0']) {
                        case true:
                          return IconButton(
                              icon: Icon(MdiIcons.star,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('M1A0', false)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Eliminado de favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                        case false:
                          return IconButton(
                              icon: Icon(MdiIcons.starOutline,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('M1A0', true)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Agregado a favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                      }
                    } else {
                      return Container();
                    }
                    return null;
                  }),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(
                color: Colors.red,
              ),
            );
          }
        });
  }

  Widget header() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      default:
        return Image(
          width: kIsWeb ? 100 : size.width / 3,
          height: kIsWeb ? 100 : size.width / 3,
          fit: BoxFit.scaleDown,
          image: AssetImage(imageHeader()),
        );
    }
  }

  String imageHeader() {
    switch (activeStep) {
      default:
        return 'assets/images/vamosjuntxs-logo.png';
    }
  }

  String headerText() {
    switch (activeStep) {
      default:
        return 'Vamos juntxs';
    }
  }

  String buttonText() {
    switch (activeStep) {
      case 0:
        return '¿Qué podrás encontrar en la app?';

      case 6:
        return '¡Comencemos!';
      default:
        return 'Siguiente';
    }
  }

  Widget content() {
    switch (activeStep) {
      case 0:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(top: 20, right: 20, left: 20),
                    alignment: Alignment.centerLeft,
                    child: const Text(
                      '¡Bienvenidx: Nos alegra mucho que estés aquí!',
                      textScaler: TextScaler.linear(1.0),
                      style:
                          TextStyle(fontSize: 22, color: CustomColors.surface),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(
                        left: 20, right: 20, bottom: 20, top: 10),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Estudiar en la universidad nos abre nuevas oportunidades y descubrimientos que enriquecen nuestra vida. Sin embargo, sabemos que la vida universitaria puede ser difícil, ya que, a lo largo de nuestros estudios nos encontramos con diversos desafíos, responsabilidades y cambios.',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                ' Estos cambios y desafíos pueden llegar a ser abrumadores y la forma en que los enfrentemos influirá en nuestro bienestar y salud mental.\n\n',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Durante los últimos años, estudios nacionales e internacionales han señalado que',
                          ),
                          TextSpan(
                            text:
                                ' las dificultades para afrontar los cambios y desafíos de los estudios universitarios pueden afectar nuestro bienestar y desencadenar síntomas y problemas de salud mental como depresión, ansiedad y estrés.\n\n',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text: 'Por esto, en',
                          ),
                          TextSpan(
                            text: ' Vamos Juntxs',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' queremos acompañarte durante esta etapa de tu vida y apoyarte en el proceso de tomar acción y adquirir habilidades, conocimientos y estrategias que potencien tu bienestar y salud mental.',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );

      case 1:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(top: 20, right: 20, left: 20),
                    alignment: Alignment.centerLeft,
                    child: const Text(
                      'Tu Programa',
                      textScaler: TextScaler.linear(1.0),
                      style:
                          TextStyle(fontSize: 22, color: CustomColors.surface),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(
                        left: 20, right: 20, bottom: 20, top: 10),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Durante 4 semanas accederás de manera progresiva a contenidos y ejercicios que te entregarán conocimientos y herramientas para cuidar de tu bienestar y ayudarte a prevenir problemas de salud mental. Deberás completar las actividades de cada módulo para poder pasar al siguiente. Tus avances serán recompensandos con insignias, nuevos contenidos y actividades complementarias.',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.all(15),
                    child: const Image(
                      image: AssetImage('assets/images/m1a0-1.png'),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 2:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(top: 20, right: 20, left: 20),
                    alignment: Alignment.centerLeft,
                    child: const Text(
                      'Actividades Complementarias',
                      textScaler: TextScaler.linear(1.0),
                      style:
                          TextStyle(fontSize: 22, color: CustomColors.surface),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(
                        left: 20, right: 20, bottom: 20, top: 10),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Accederás a consejos, contenidos y ejercicios que potenciarán aún más tus conocimientos y las habilidades para cuidar de tu bienestar durante el transcurso de tus estudios universitarios.\n¿Cómo puedes acceder a las actividades?\nAccede a tus actividades presionando el botón que se encuentra en el costado inferior de la pantalla.',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.all(15),
                    child: const Image(
                      image: AssetImage('assets/images/m1a0-2.png'),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 3:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(top: 20, right: 20, left: 20),
                    alignment: Alignment.centerLeft,
                    child: const Text(
                      'Buscar ayuda',
                      textScaler: TextScaler.linear(1.0),
                      style:
                          TextStyle(fontSize: 22, color: CustomColors.surface),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(
                        left: 20, right: 20, bottom: 20, top: 10),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'En Vamxs Juntxs queremos acompañarte cuando lo necesites. Por eso, desde este instante, podrás acceder a distintos recursos de ayuda (chat con psicólogx, números de ayuda, etc.) para apoyarte en momentos en los que sientas malestar o cuando necesites de una guía para poder avanzar en Tu Programa.\n\n',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text: '¿Cómo puedes pedir ayuda?\n',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Pincha en el botón “Buscar Ayuda” que se encuentra en el costado inferior de la pantalla. Ahí se desplegarán las diversas opciones entre las cuales podrás escoger aquellas que más se ajusten a tus necesidades.',
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.all(15),
                    child: const Image(
                      image: AssetImage('assets/images/m1a0-3.png'),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 4:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(top: 20, right: 20, left: 20),
                    alignment: Alignment.centerLeft,
                    child: const Text(
                      'Termómetro',
                      textScaler: TextScaler.linear(1.0),
                      style:
                          TextStyle(fontSize: 22, color: CustomColors.surface),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(
                        left: 20, right: 20, bottom: 20, top: 10),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'El Termómetro del Ánimo es una escala visual por medio de la cual podrás evaluar tu ánimo al asignarle un puntaje de 0 a 10. De esta forma, podrás monitorear tu estado de ánimo de manera rápida y sencilla. De acuerdo a lo que respondas recibirás una reatroalimentación automática con recomendaciones generales acordes a como te sientes.\n\n',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                'Podrás responder el Termómetro las veces que quieras, tus respuestas quedarán registradas en el historial de ánimo que se encuentra en la sección de Tu Perfil. Así, al revisar tu historial, podrás visualizar como ha ido cambiando tu ánimo a lo largo de los días y semanas.',
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.all(15),
                    child: const Image(
                      image: AssetImage('assets/images/m1a0-4.png'),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 5:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(top: 20, right: 20, left: 20),
                    alignment: Alignment.centerLeft,
                    child: const Text(
                      'Chat',
                      textScaler: TextScaler.linear(1.0),
                      style:
                          TextStyle(fontSize: 22, color: CustomColors.surface),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(
                        left: 20, right: 20, bottom: 20, top: 10),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            '¡Estamos aquí para ti! Por medio del Chat de Vamos Juntxs queremos acompañarte durante este periodo. En el chat podrás conversar de manera individual y privada con algunx de los miembros del equipo de Psicólogxs que forman parte de esta iniciativa.\n\n',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text: '¿A qué puedes acceder por medio del chat?\n',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Contención emocional en caso de que sientas malestar. Orientación para resolver dudas respecto al uso de la app y la realización de las actividades  Retroalimentación respecto de tu progreso y estado de ánimo durante el periodo que utilices la app. Orientación general en salud mental, por ejemplo, cuándo, cómo y dónde acceder a servicios de atención de salud mental.\n\n',
                          ),
                          TextSpan(
                            text: '¿En que horario se encuentra disponible?\n',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'El chat se encuentra de disponible de Lunes a Viernes de 9:00 a 18:00 hrs.\n\n',
                          ),
                          TextSpan(
                            text:
                                'Recuerda, no tienes que afrontar siempre las cosas por tu cuenta, nuestro equipo de Psicólogxs cuenta con la experiencia y capacitación para poder acompañarte, apoyarte y ayudarte a resolver dudas.\n\n',
                          ),
                          TextSpan(
                            text:
                                '¿Necesitas conversar? pincha el botón del chat en la app y hablemos.',
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.all(15),
                    child: const Image(
                      image: AssetImage('assets/images/m1a0-5.png'),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 6:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(top: 20, right: 20, left: 20),
                    alignment: Alignment.centerLeft,
                    child: const Text(
                      'Tu perfil',
                      textScaler: TextScaler.linear(1.0),
                      style:
                          TextStyle(fontSize: 22, color: CustomColors.surface),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(
                        left: 20, right: 20, bottom: 20, top: 10),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'En Tu Perfil podrás encontrar las insignias que hayas adquirido a partir de tus avances en el programa. Además, podrás encontrar aquellos contenidos que más te hayan gustado de la app en la sección de “Tus Favoritos”.\n',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                'Pero, ¿cómo puedes guardar contenidos en la sección de favoritos?\n\n',
                          ),
                          TextSpan(
                            text:
                                'Al ingresar a las diversas actividades de la app, podrás encontrar un ícono de estrella al costado superior derecho de tu pantalla. Presiona este ícono cuando quieras guardar una actividad en “Tus Favoritos”.\n\n',
                          ),
                          TextSpan(
                            text: '¿Estás listx?',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.all(15),
                    child: const Image(
                      image: AssetImage('assets/images/m1a0-6.png'),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      default:
        return Container();
    }
  }
}
