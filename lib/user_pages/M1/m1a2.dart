// ignore_for_file: avoid_print, prefer_typing_uninitialized_variables

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:seguimiento/colors.dart';

import '../../firebase.dart';
import '../MainPage.dart';

class M1A2 extends StatefulWidget {
  const M1A2({
    Key? key,
  }) : super(key: key);

  @override
  State<M1A2> createState() => _M1A2State();
}

class _M1A2State extends State<M1A2> {
  String uid = FirebaseAuth.instance.currentUser!.uid;
  bool loadingData = true;
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('page-control');
  String activeStepData = '';
  void getActivePageStored() {
    collectionReference.doc(uid).get().then((value) {
      var fields = value;
      activeStepData = fields['M1A2'];
      setState(() {
        if (int.parse(activeStepData) < 11) {
          activeStep = int.parse(activeStepData) - 1;
          Future.delayed(const Duration(milliseconds: 500), () {
            setState(() {
              loadingData = false;
            });
          });
        }
      });
    });
  }

  ScrollController scrollController = ScrollController();

  bool favorite = false;
  CollectionReference favoriteData =
      FirebaseFirestore.instance.collection('favoritos');
  void getFavData() {
    favoriteData.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        favorite = fields['M1A2'];
      });
    }).catchError((err) {
      print('Error: $err');
    });
  }

  int activeStep = 0;
  int acertado = 0;
  String respuesta = '';
  var timeStart;
  @override
  void initState() {
    super.initState();
    getActivePageStored();
    getFavData();
    var tiempo = DateTime.now();
    timeStart = tiempo;
    FirestoreService().setTime(tiempo.toString() + ' - START - M1A2',
        'Comenzó M1A2', '0 minutos (comenzando)');
  }

  @override
  void dispose() {
    super.dispose();
  }

  bool active1 = false;
  bool active2 = false;
  bool active3 = false;
  bool active4 = false;
  bool active5 = false;
  bool active6 = false;
  bool active7 = false;
  bool active8 = false;
  bool active9 = false;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        active1 = true;
        active2 = false;
        active3 = false;
        active4 = false;
        active5 = false;
        active6 = false;
        active7 = false;
        active8 = false;
        active9 = false;

        break;
      case 1:
        active1 = true;
        active2 = true;
        active3 = false;
        active4 = false;
        active5 = false;
        active6 = false;
        active7 = false;
        active8 = false;
        active9 = false;

        break;
      case 2:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = false;
        active5 = false;
        active6 = false;
        active7 = false;
        active8 = false;
        active9 = false;

        break;
      case 3:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = false;
        active6 = false;
        active7 = false;
        active8 = false;
        active9 = false;

        break;
      case 4:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = true;
        active6 = false;
        active7 = false;
        active8 = false;
        active9 = false;

        break;
      case 5:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = true;
        active6 = true;
        active7 = false;
        active8 = false;
        active9 = false;

        break;
      case 6:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = true;
        active6 = true;
        active7 = true;
        active8 = false;
        active9 = false;

        break;
      case 7:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = true;
        active6 = true;
        active7 = true;
        active8 = true;
        active9 = false;

        break;
      case 8:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = true;
        active6 = true;
        active7 = true;
        active8 = true;
        active9 = true;

        break;

      default:
    }
    return PopScope(
      canPop: false,
      onPopInvokedWithResult: (bool didPop, result) {
        if (didPop) {
          return;
        }
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(32.0))),
            title: const Text(
              '¿Estás seguro que deseas salir de la actividad?',
              textScaler: TextScaler.linear(1.0),
              style: TextStyle(color: CustomColors.primaryDark),
            ),
            content: const Text(
              'Tus avances quedarán guardados',
              textScaler: TextScaler.linear(1.0),
              style: TextStyle(color: CustomColors.primaryDark),
            ),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: const Text(
                  'Continuar con la actividad',
                  textScaler: TextScaler.linear(1.0),
                  style: TextStyle(color: CustomColors.primary),
                ),
              ),
              TextButton(
                onPressed: () {
                  FirestoreService()
                      .updateCurrentPage('M1A2', (activeStep + 1).toString());
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute<void>(
                          builder: (BuildContext context) => const MainPage(
                                activePage: 0,
                              )),
                      (route) => false);
                },
                child: const Text(
                  'Salir',
                  textScaler: TextScaler.linear(1.0),
                  style: TextStyle(color: CustomColors.error),
                ),
              ),
            ],
          ),
        );
      },
      child: loadingData
          ? Container(
              width: size.width,
              height: size.height,
              color: CustomColors.primaryDark,
              child: const Center(
                  child: CircularProgressIndicator(
                color: CustomColors.tertiary70,
              )))
          : Container(
              color: Colors.black,
              child: SafeArea(
                child: Scaffold(
                  backgroundColor:
                      active9 ? CustomColors.greenEnd : CustomColors.primary3,
                  body: Center(
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              width: size.width / 9,
                              height: 5,
                              color: active1
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 9,
                              height: 5,
                              color: active2
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 9,
                              height: 5,
                              color: active3
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 9,
                              height: 5,
                              color: active4
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 9,
                              height: 5,
                              color: active5
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 9,
                              height: 5,
                              color: active6
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 9,
                              height: 5,
                              color: active7
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 9,
                              height: 5,
                              color: active8
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 9,
                              height: 5,
                              color: active9
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                          ],
                        ),
                        content(),
                        Container(
                          margin: const EdgeInsets.only(top: 10, bottom: 10),
                          child: bottomStructure(),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
    );
  }

  Widget topStructure() {
    Size size = MediaQuery.of(context).size;

    return Container(
      width: size.width,
      height: active9 ? null : size.height / 3,
      color: CustomColors.greenList,
      child: Column(
        children: [
          Row(
            children: [
              active9
                  ? Container()
                  : Container(
                      alignment: Alignment.topRight,
                      child: IconButton(
                        onPressed: () {
                          showDialog(
                            context: context,
                            builder: (context) => AlertDialog(
                              shape: const RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(32.0))),
                              title: const Text(
                                '¿Estás seguro que deseas salir de la actividad?',
                                textScaler: TextScaler.linear(1.0),
                                style:
                                    TextStyle(color: CustomColors.primaryDark),
                              ),
                              content: const Text(
                                'Tus avances quedarán guardados',
                                textScaler: TextScaler.linear(1.0),
                                style:
                                    TextStyle(color: CustomColors.primaryDark),
                              ),
                              actions: [
                                TextButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                  child: const Text(
                                    'Continuar con la actividad',
                                    textScaler: TextScaler.linear(1.0),
                                    style:
                                        TextStyle(color: CustomColors.primary),
                                  ),
                                ),
                                TextButton(
                                  onPressed: () {
                                    String tiempo = DateTime.now().toString();
                                    FirestoreService().setTime(
                                        tiempo + ' - CLOSED - M1A2',
                                        'Salió de M1A2 sin terminar',
                                        '0 minutos (sin terminar)');
                                    FirestoreService().updateCurrentPage(
                                        'M1A2', (activeStep + 1).toString());
                                    Navigator.pushAndRemoveUntil(
                                        context,
                                        MaterialPageRoute<void>(
                                            builder: (BuildContext context) =>
                                                const MainPage(
                                                  activePage: 0,
                                                )),
                                        (route) => false);
                                  },
                                  child: const Text(
                                    'Salir',
                                    textScaler: TextScaler.linear(1.0),
                                    style: TextStyle(color: CustomColors.error),
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                        icon: const Icon(
                          Icons.close,
                          color: CustomColors.primaryDark,
                        ),
                      ),
                    ),
              const Spacer(),
              active9 ? Container() : favorito(),
            ],
          ),
          Text(
            headerText(),
            textScaler: const TextScaler.linear(1.0),
            style: const TextStyle(
              color: CustomColors.primaryDark,
              fontSize: 24,
            ),
          ),
          active9 ? Container() : const Spacer(),
          header(),
        ],
      ),
    );
  }

  Widget favorito() {
    return StreamBuilder<QuerySnapshot>(
        stream: FirestoreService().favoritos(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasData) {
            return SizedBox(
              width: 50,
              height: 50,
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (context, index) {
                    var document = snapshot.data?.docs[index];
                    if (document != null && document.id == uid) {
                      switch (document['M1A2']) {
                        case true:
                          return IconButton(
                              icon: Icon(MdiIcons.star,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('M1A2', false)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Eliminado de favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                        case false:
                          return IconButton(
                              icon: Icon(MdiIcons.starOutline,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('M1A2', true)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Agregado a favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                      }
                    } else {
                      return Container();
                    }
                    return null;
                  }),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(
                color: Colors.red,
              ),
            );
          }
        });
  }

  Widget bottomStructure() {
    return Stack(
      alignment: Alignment.center,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            nextButton(),
          ],
        ),
      ],
    );
  }

  Widget header() {
    Size size = MediaQuery.of(context).size;
    switch (activeStep) {
      case 0:
        return Image(
          width: kIsWeb ? 100 : size.width / 3,
          height: kIsWeb ? 100 : size.width / 3,
          fit: BoxFit.scaleDown,
          image: AssetImage(imageHeader()),
        );

      case 8:
        return Image(
          width: size.width / 8,
          height: size.width / 8,
          fit: BoxFit.scaleDown,
          image: AssetImage(imageHeader()),
        );
      default:
        return Container(
          margin: const EdgeInsets.only(left: 20, right: 20, bottom: 50),
          decoration: const BoxDecoration(),
          child: const Text(
            '¿Cuál de las siguientes frases es mito?',
            textAlign: TextAlign.center,
            textScaler: TextScaler.linear(1.0),
            style: TextStyle(
                fontSize: 20,
                color: CustomColors.primaryDark,
                fontWeight: FontWeight.bold),
          ),
        );
    }
  }

  String imageHeader() {
    switch (activeStep) {
      case 0:
        return 'assets/images/m1a2.png';

      case 8:
        return 'assets/images/end.png';
      default:
        return 'assets/images/m1a2.png';
    }
  }

  String headerText() {
    switch (activeStep) {
      case 0:
        return 'Mitos sobre la depresión';

      case 1:
        return 'Mitos sobre la depresión';

      case 8:
        return '';
      default:
        return 'Mitos sobre la depresión';
    }
  }

  String buttonText() {
    switch (activeStep) {
      case 0:
        return 'Comenzar la trivia';

      case 1:
        return 'Siguiente';

      case 8:
        return 'Continuar con el programa';
      default:
        return 'Siguiente';
    }
  }

  void modalRespuesta(response) {
    Size size = MediaQuery.of(context).size;

    switch (response) {
      case 0:
        showModalBottomSheet<void>(
          isDismissible: false,
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(40), topRight: Radius.circular(40)),
          ),
          context: context,
          builder: (BuildContext context) {
            return SizedBox(
              height: 200,
              child: Center(
                child: Column(
                  children: <Widget>[
                    Container(
                      margin: const EdgeInsets.all(10),
                      width: 50,
                      height: 5,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: CustomColors.surface,
                      ),
                      child: const Text(''),
                    ),
                    const Spacer(),
                    Container(
                      margin: const EdgeInsets.all(10),
                      width: size.width / 1.1,
                      child: Row(
                        children: [
                          Container(
                            margin: const EdgeInsets.only(right: 10),
                            child: Icon(
                              MdiIcons.closeCircle,
                              size: 30,
                            ),
                          ),
                          RichText(
                            text: const TextSpan(
                              text: 'Respuesta incorrecta.\n',
                              style: TextStyle(
                                  fontSize: 16,
                                  color: CustomColors.primaryDark,
                                  fontWeight: FontWeight.bold,
                                  height: 1.5),
                              children: <TextSpan>[
                                TextSpan(
                                  text: 'La frase que seleccionaste ',
                                  style: TextStyle(
                                    fontWeight: FontWeight.normal,
                                  ),
                                ),
                                TextSpan(
                                  text: 'NO',
                                ),
                                TextSpan(
                                  text: ' es\nun mito',
                                  style: TextStyle(
                                    fontWeight: FontWeight.normal,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    const Spacer(),
                    Container(
                      margin: const EdgeInsets.all(10),
                      width: size.width / 1.2,
                      child: ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor: WidgetStateProperty.all<Color>(
                              CustomColors.error),
                          shape:
                              WidgetStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18.0),
                            ),
                          ),
                        ),
                        onPressed: () {
                          if (activeStep < 9) {
                            setState(() {
                              activeStep++;
                            });
                            Navigator.pop(context);
                          }
                        },
                        child: Text(
                          buttonText(),
                          textScaler: const TextScaler.linear(1.0),
                          style: const TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        );
        break;
      case 1:
        showModalBottomSheet<void>(
          isDismissible: false,
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(40), topRight: Radius.circular(40)),
          ),
          context: context,
          builder: (BuildContext context) {
            return SizedBox(
              height: 200,
              child: Center(
                child: Column(
                  children: <Widget>[
                    Container(
                      margin: const EdgeInsets.all(10),
                      width: 50,
                      height: 5,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: const Color.fromARGB(255, 218, 218, 218),
                      ),
                      child: const Text(''),
                    ),
                    const Spacer(),
                    Container(
                      margin: const EdgeInsets.all(10),
                      width: size.width / 1.1,
                      child: Row(
                        children: [
                          Container(
                            margin: const EdgeInsets.only(right: 10),
                            child: Icon(
                              MdiIcons.checkCircle,
                              size: 30,
                            ),
                          ),
                          const Text(
                            'Respuesta correcta\nLa frase que seleccionaste es un mito.',
                            textScaler: TextScaler.linear(1.0),
                            style: TextStyle(fontSize: 16, height: 1.5),
                          ),
                        ],
                      ),
                    ),
                    const Spacer(),
                    Container(
                      margin: const EdgeInsets.all(10),
                      width: size.width / 1.2,
                      child: ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor: WidgetStateProperty.all<Color>(
                              CustomColors.primary),
                          shape:
                              WidgetStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18.0),
                            ),
                          ),
                        ),
                        onPressed: () {
                          if (activeStep < 9) {
                            setState(() {
                              activeStep++;
                            });
                            Navigator.pop(context);
                          }
                        },
                        child: Text(
                          buttonText(),
                          textScaler: const TextScaler.linear(1.0),
                          style: const TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        );
        break;
      default:
    }
  }

  Widget content() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });
        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.all(20),
                    child: const Text(
                      '¿Alguna vez has escuchado que la depresión se supera con voluntad o que el que está deprimido es porque quiere? Este y otros mitos existen en nuestra sociedad, pero actualmente son refutados por la ciencia.\n\nPara que aprendas a diferenciar los mitos de la realidad te invitamos a hacer la siguiente trivia.',
                      textScaler: TextScaler.linear(1.0),
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );

      case 1:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });
        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(top: 30),
                    child: InkWell(
                      onTap: () {
                        modalRespuesta(0);
                        respuesta = 'incorrectamente';
                      },
                      child: Container(
                        margin: const EdgeInsets.all(10),
                        padding: const EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: CustomColors.background,
                        ),
                        width: size.width / 1.2,
                        child: const Text(
                          'La depresión es una enfermedad del ánimo con síntomas intensos y duraderos, y si no es tratada a tiempo, puede causar graves consecuencias.',
                          textScaler: TextScaler.linear(1.0),
                          style: TextStyle(
                              fontSize: 16,
                              color: CustomColors.primaryDark,
                              height: 1.5),
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      modalRespuesta(1);
                      respuesta = 'correctamente';
                      acertado++;
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(10),
                      width: size.width / 1.2,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: CustomColors.background,
                      ),
                      child: const Text(
                        'La depresión no es una enfermedad real, es una excusa para no asumir responsabilidades.',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primaryDark,
                            height: 1.5),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );

      case 2:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });
        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(top: 30),
                    child: InkWell(
                      onTap: () {
                        modalRespuesta(1);
                        respuesta = 'correctamente';
                        acertado++;
                      },
                      child: Container(
                        margin: const EdgeInsets.all(10),
                        padding: const EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: CustomColors.background,
                        ),
                        width: size.width / 1.2,
                        child: const Text(
                          'Las personas que se deprimen tienen una debilidad de carácter.',
                          textScaler: TextScaler.linear(1.0),
                          style: TextStyle(
                              fontSize: 16,
                              color: CustomColors.primaryDark,
                              height: 1.5),
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      modalRespuesta(0);
                      respuesta = 'incorrectamente';
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(10),
                      width: size.width / 1.2,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: CustomColors.background,
                      ),
                      child: const Text(
                        'La depresión se produce por una combinación de factores biológicos, psicológicos y sociales.',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primaryDark,
                            height: 1.5),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 3:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(top: 30),
                    child: InkWell(
                      onTap: () {
                        modalRespuesta(0);
                        respuesta = 'incorrectamente';
                      },
                      child: Container(
                        margin: const EdgeInsets.all(10),
                        padding: const EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: CustomColors.background,
                        ),
                        width: size.width / 1.2,
                        child: const Text(
                          'La depresión es una enfermedad que requiere un tratamiento guiado por profesionales de salud mental.',
                          textScaler: TextScaler.linear(1.0),
                          style: TextStyle(
                              fontSize: 16,
                              color: CustomColors.primaryDark,
                              height: 1.5),
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      modalRespuesta(1);
                      respuesta = 'correctamente';
                      acertado++;
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(10),
                      width: size.width / 1.2,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: CustomColors.background,
                      ),
                      child: const Text(
                        'La depresión se supera con fuerza de voluntad, depende sólo de uno salir adelante.',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primaryDark,
                            height: 1.5),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 4:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });
        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(top: 30),
                    child: InkWell(
                      onTap: () {
                        modalRespuesta(1);
                        respuesta = 'correctamente';
                        acertado++;
                      },
                      child: Container(
                        margin: const EdgeInsets.all(10),
                        padding: const EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: CustomColors.background,
                        ),
                        width: size.width / 1.2,
                        child: const Text(
                          'Si tienes depresión es mejor que nadie lo sepa, no te entenderán y tendrán lástima de ti.',
                          textScaler: TextScaler.linear(1.0),
                          style: TextStyle(
                              fontSize: 16,
                              color: CustomColors.primaryDark,
                              height: 1.5),
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      modalRespuesta(0);
                      respuesta = 'incorrectamente';
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(10),
                      width: size.width / 1.2,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: CustomColors.background,
                      ),
                      child: const Text(
                        'Hablar de depresión ayuda a desestigmatizar este problema de salud mental y favorece el acceso a ayuda oportuna.',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primaryDark,
                            height: 1.5),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 5:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });
        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(top: 30),
                    child: InkWell(
                      onTap: () {
                        modalRespuesta(0);
                        respuesta = 'incorrectamente';
                      },
                      child: Container(
                        margin: const EdgeInsets.all(10),
                        padding: const EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: CustomColors.background,
                        ),
                        width: size.width / 1.2,
                        child: const Text(
                          'Existen tratamientos eficaces y científicamente comprobados para tratar la depresión.',
                          textScaler: TextScaler.linear(1.0),
                          style: TextStyle(
                              fontSize: 16,
                              color: CustomColors.primaryDark,
                              height: 1.5),
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      modalRespuesta(1);
                      respuesta = 'correctamente';
                      acertado++;
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(10),
                      width: size.width / 1.2,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: CustomColors.background,
                      ),
                      child: const Text(
                        'La depresión es un problema que dura de por vida.',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primaryDark,
                            height: 1.5),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 6:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });
        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(top: 30),
                    child: InkWell(
                      onTap: () {
                        modalRespuesta(0);
                        respuesta = 'incorrectamente';
                      },
                      child: Container(
                        margin: const EdgeInsets.all(10),
                        padding: const EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: CustomColors.background,
                        ),
                        width: size.width / 1.2,
                        child: const Text(
                          'Las personas con depresión pueden sufrir en silencio y pueden tener momentos en los que la pasan bien.',
                          textScaler: TextScaler.linear(1.0),
                          style: TextStyle(
                              fontSize: 16,
                              color: CustomColors.primaryDark,
                              height: 1.5),
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      modalRespuesta(1);
                      respuesta = 'correctamente';
                      acertado++;
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(10),
                      width: size.width / 1.2,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: CustomColors.background,
                      ),
                      child: const Text(
                        'Las personas con depresión siempre se ven evidentemente tristes.',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primaryDark,
                            height: 1.5),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 7:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });
        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(top: 30),
                    child: InkWell(
                      onTap: () {
                        modalRespuesta(0);
                        respuesta = 'incorrectamente';
                      },
                      child: Container(
                        margin: const EdgeInsets.all(10),
                        padding: const EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: CustomColors.background,
                        ),
                        width: size.width / 1.2,
                        child: const Text(
                          'Lxs psicólogos y psiquiatras son especialistas en salud mental, y están capacitados para ayudar en casos de depresión.',
                          textScaler: TextScaler.linear(1.0),
                          style: TextStyle(
                              fontSize: 16,
                              color: CustomColors.primaryDark,
                              height: 1.5),
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      modalRespuesta(1);
                      respuesta = 'correctamente';
                      acertado++;
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(10),
                      width: size.width / 1.2,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: CustomColors.background,
                      ),
                      child: const Text(
                        'Ir al psicólogo y/o psiquiatra no sirve o son para personas locas.',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primaryDark,
                            height: 1.5),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 8:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });
        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Container(
                margin: const EdgeInsets.all(15),
                child: Column(
                  children: [
                    topStructure(),
                    Container(
                      margin: const EdgeInsets.all(10),
                      child: const FittedBox(
                        fit: BoxFit.fitWidth,
                        child: Text(
                          '¡Muy bien!',
                          textScaler: TextScaler.linear(1.0),
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 24),
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 10),
                      child: Text(
                        'Acertaste a ' +
                            acertado.toString() +
                            ' de las 7 preguntas.\nTenemos una recompensa para ti:',
                        textScaler: const TextScaler.linear(1.0),
                        style: const TextStyle(fontSize: 16),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 40, bottom: 10),
                      decoration: BoxDecoration(
                          color: CustomColors.lightContainer,
                          border:
                              Border.all(width: 2, color: CustomColors.surface),
                          borderRadius: BorderRadius.circular(15)),
                      child: Row(
                        children: [
                          const Image(
                            width: 100,
                            height: 120,
                            fit: BoxFit.scaleDown,
                            image: AssetImage('assets/images/agua-icon.png'),
                          ),
                          Container(
                            margin: const EdgeInsets.only(left: 10),
                            child: const Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Actividad complementaria:',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      fontSize: 13,
                                      color: CustomColors.primaryDark),
                                ),
                                Text(
                                  'Sonidos de la naturaleza:\nAgua de río',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16,
                                      color: CustomColors.primaryDark),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 10, bottom: 10),
                      decoration: BoxDecoration(
                          color: CustomColors.lightContainer,
                          border:
                              Border.all(width: 2, color: CustomColors.surface),
                          borderRadius: BorderRadius.circular(15)),
                      child: Row(
                        children: [
                          const Image(
                            width: 100,
                            height: 120,
                            fit: BoxFit.scaleDown,
                            image: AssetImage('assets/images/bosque-icon.png'),
                          ),
                          Container(
                            margin: const EdgeInsets.only(left: 10),
                            child: const Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Actividad complementaria:',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      fontSize: 13,
                                      color: CustomColors.primaryDark),
                                ),
                                Text(
                                  'Sonidos de la naturaleza:\nBosque lluvioso',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16,
                                      color: CustomColors.primaryDark),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      default:
        return Container();
    }
  }

  Widget nextButton() {
    Size size = MediaQuery.of(context).size;
    switch (activeStep) {
      case 0:
        return SizedBox(
          width: size.width / 1.2,
          child: ElevatedButton(
            style: ButtonStyle(
              backgroundColor: WidgetStateProperty.all<Color>(
                  active9 ? CustomColors.primary : CustomColors.greenButton),
              shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                ),
              ),
            ),
            onPressed: () {
              if (activeStep < 8) {
                setState(() {
                  activeStep++;
                });
              } else {
                FirestoreService().terminaSesion(
                    '1',
                    '2',
                    'Leyó la introducción → Acertó a ' +
                        acertado.toString() +
                        ' de las 7 preguntas → Terminó la actividad 2');
                var tiempo = DateTime.now();
                var difTiempo = tiempo.difference(timeStart);
                FirestoreService().setTime(
                    tiempo.toString() + ' - ENDED - M1A2',
                    'Terminó M1A2',
                    difTiempo.inMinutes.toString() + ' minutos');
                FirestoreService().updateCurrentPage('M1A2', '1');
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute<void>(
                        builder: (BuildContext context) => const MainPage(
                              activePage: 0,
                            )),
                    (route) => false);
              }
            },
            child: Text(
              buttonText(),
              textScaler: const TextScaler.linear(1.0),
              style: const TextStyle(
                  color: CustomColors.primaryDark, fontWeight: FontWeight.bold),
            ),
          ),
        );

      case 8:
        return SizedBox(
          width: size.width / 1.2,
          child: ElevatedButton(
            style: ButtonStyle(
              backgroundColor: WidgetStateProperty.all<Color>(
                  active9 ? CustomColors.primary : CustomColors.greenButton),
              shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                ),
              ),
            ),
            onPressed: () {
              if (activeStep < 2) {
                setState(() {
                  activeStep++;
                });
              } else {
                FirestoreService().terminaSesion(
                    '1',
                    '2',
                    'Leyó la introducción → Acertó a ' +
                        acertado.toString() +
                        ' de las 7 preguntas → Terminó la actividad 2');
                var tiempo = DateTime.now();
                var difTiempo = tiempo.difference(timeStart);
                FirestoreService().setTime(
                    tiempo.toString() + ' - ENDED - M1A2',
                    'Terminó M1A2',
                    difTiempo.inMinutes.toString() + ' minutos');
                FirestoreService().updateCurrentPage('M1A2', '1');
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute<void>(
                        builder: (BuildContext context) => const MainPage(
                              activePage: 0,
                            )),
                    (route) => false);
              }
            },
            child: Text(
              buttonText(),
              textScaler: const TextScaler.linear(1.0),
              style: TextStyle(
                color:
                    active9 ? CustomColors.surface : CustomColors.primaryDark,
              ),
            ),
          ),
        );
      default:
        return Container();
    }
  }
}
