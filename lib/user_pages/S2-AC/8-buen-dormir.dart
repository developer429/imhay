// ignore_for_file: file_names, prefer_typing_uninitialized_variables, avoid_print

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:seguimiento/colors.dart';
import 'package:toggle_switch/toggle_switch.dart';

import '../../firebase.dart';
import '../MainPage.dart';

class A8 extends StatefulWidget {
  const A8({
    Key? key,
  }) : super(key: key);

  @override
  State<A8> createState() => _A8State();
}

class _A8State extends State<A8> {
  String uid = FirebaseAuth.instance.currentUser!.uid;
  bool loadingData = true;
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('page-control');
  String activeStepData = '';
  void getActivePageStored() {
    collectionReference.doc(uid).get().then((value) {
      var fields = value;
      activeStepData = fields['A8'];
      setState(() {
        if (int.parse(activeStepData) < 6) {
          activeStep = int.parse(activeStepData) - 1;
          Future.delayed(const Duration(milliseconds: 500), () {
            setState(() {
              loadingData = false;
            });
          });
        }
      });
    });
  }

  var startHorarioRegular;
  var startSiestasRRegular;
  var startBebidasAntesDormir;
  var startComerMucho;
  var startEjercicioAntesDormir;
  var startActividadesRelajantes;
  var startRopaComoda;
  var startDormitorioAcogedor;
  var startPantallasAntesDormir;
  var startMedicamentos;
  String horarioRegular = 'No';
  String siestasRRegular = 'No';
  String bebidasAntesDormir = 'No';
  String comerMucho = 'No';
  String ejercicioAntesDormir = 'No';
  String actividadesRelajantes = 'No';
  String ropaComoda = 'No';
  String dormitorioAcogedor = 'No';
  String pantallasAntesDormir = 'No';
  String medicamentos = 'No';
  CollectionReference buenDormirReference =
      FirebaseFirestore.instance.collection('A8-buen-dormir-respuestas');
  void getData() {
    buenDormirReference.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        if (fields['HorarioRegular'] == 'Sí') {
          startHorarioRegular = 0;
          horarioRegular = 'Sí';
        } else {
          startHorarioRegular = 1;
          horarioRegular = 'No';
        }
        if (fields['SiestasRegulares'] == 'Sí') {
          startSiestasRRegular = 0;
          siestasRRegular = 'Sí';
        } else {
          startSiestasRRegular = 1;
          siestasRRegular = 'No';
        }
        if (fields['BebidasEstimulantes'] == 'Sí') {
          startBebidasAntesDormir = 0;
          bebidasAntesDormir = 'Sí';
        } else {
          startBebidasAntesDormir = 1;
          bebidasAntesDormir = 'No';
        }
        if (fields['ComerMucho'] == 'Sí') {
          startComerMucho = 0;
          comerMucho = 'Sí';
        } else {
          startComerMucho = 1;
          comerMucho = 'No';
        }
        if (fields['Ejercicio'] == 'Sí') {
          startEjercicioAntesDormir = 0;
          ejercicioAntesDormir = 'Sí';
        } else {
          startEjercicioAntesDormir = 1;
          ejercicioAntesDormir = 'No';
        }
        if (fields['ActividadesRelajantes'] == 'Sí') {
          startActividadesRelajantes = 0;
          actividadesRelajantes = 'Sí';
        } else {
          startActividadesRelajantes = 1;
          actividadesRelajantes = 'No';
        }
        if (fields['RopaComoda'] == 'Sí') {
          startRopaComoda = 0;
          ropaComoda = 'Sí';
        } else {
          startRopaComoda = 1;
          ropaComoda = 'No';
        }
        if (fields['DormitorioAcogedor'] == 'Sí') {
          startDormitorioAcogedor = 0;
          dormitorioAcogedor = 'Sí';
        } else {
          startDormitorioAcogedor = 1;
          dormitorioAcogedor = 'No';
        }
        if (fields['Pantallas'] == 'Sí') {
          startPantallasAntesDormir = 0;
          pantallasAntesDormir = 'Sí';
        } else {
          startPantallasAntesDormir = 1;
          pantallasAntesDormir = 'No';
        }
        if (fields['Medicamentos'] == 'Sí') {
          startMedicamentos = 0;
          medicamentos = 'Sí';
        } else {
          startMedicamentos = 1;
          medicamentos = 'No';
        }
      });
    }).catchError((err) {
      print('Error: $err');
      startHorarioRegular = 1;
      startSiestasRRegular = 1;
      startBebidasAntesDormir = 1;
      startComerMucho = 1;
      startEjercicioAntesDormir = 1;
      startActividadesRelajantes = 1;
      startRopaComoda = 1;
      startDormitorioAcogedor = 1;
      startPantallasAntesDormir = 1;
      startMedicamentos = 1;
    });
  }

  ScrollController scrollController = ScrollController();

  bool favorite = false;
  CollectionReference favoriteData =
      FirebaseFirestore.instance.collection('favoritos');
  void getFavData() {
    favoriteData.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        favorite = fields['A8'];
      });
    }).catchError((err) {
      print('Error: $err');
    });
  }

  int activeStep = 0;
  @override
  void initState() {
    super.initState();

    getActivePageStored();
    getData();
    getFavData();
  }

  @override
  void dispose() {
    super.dispose();
  }

  bool active1 = false;
  bool active2 = false;
  bool active3 = false;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        active1 = true;
        active2 = false;
        active3 = false;

        break;
      case 1:
        active1 = true;
        active2 = true;
        active3 = false;

        break;
      case 2:
        active1 = true;
        active2 = true;
        active3 = true;

        break;
      default:
    }
    return PopScope(
      canPop: false,
      onPopInvokedWithResult: (bool didPop, result) async {
        if (didPop) {
          return;
        }
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(32.0))),
            title: const Text(
              '¿Estás seguro que deseas salir de la actividad?',
              textScaler: TextScaler.linear(1.0),
              style: TextStyle(color: CustomColors.primaryDark),
            ),
            content: const Text(
              'Tus respuestas no serán guardadas',
              textScaler: TextScaler.linear(1.0),
              style: TextStyle(color: CustomColors.primaryDark),
            ),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: const Text(
                  'Continuar con la actividad',
                  textScaler: TextScaler.linear(1.0),
                  style: TextStyle(color: CustomColors.primary),
                ),
              ),
              TextButton(
                onPressed: () {
                  FirestoreService()
                      .updateCurrentPage('A8', (activeStep + 1).toString());
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute<void>(
                          builder: (BuildContext context) => const MainPage(
                                activePage: 1,
                              )),
                      (route) => false);
                },
                child: const Text(
                  'Salir',
                  textScaler: TextScaler.linear(1.0),
                  style: TextStyle(color: CustomColors.error),
                ),
              ),
            ],
          ),
        );
      },
      child: loadingData
          ? Container(
              width: size.width,
              height: size.height,
              color: CustomColors.primaryDark,
              child: const Center(
                  child: CircularProgressIndicator(
                color: CustomColors.tertiary70,
              )))
          : Container(
              color: Colors.black,
              child: SafeArea(
                child: Scaffold(
                  backgroundColor: CustomColors.background,
                  body: Center(
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              width: size.width / 3,
                              height: 5,
                              color: active1
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 3,
                              height: 5,
                              color: active2
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 3,
                              height: 5,
                              color: active3
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                          ],
                        ),
                        content(),
                        Container(
                          margin: const EdgeInsets.only(top: 10, bottom: 10),
                          child: bottomStructure(),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
    );
  }

  Widget topStructure() {
    return Column(
      children: [
        Row(
          children: [
            Container(
              alignment: Alignment.topRight,
              child: IconButton(
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: (context) => AlertDialog(
                            shape: const RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(32.0))),
                            title: const Text(
                              '¿Estás seguro que deseas salir de la actividad?',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(color: CustomColors.primaryDark),
                            ),
                            content: const Text(
                              'Tus respuestas no serán guardadas',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(color: CustomColors.primaryDark),
                            ),
                            actions: [
                              TextButton(
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                child: const Text(
                                  'Continuar con la actividad',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(color: CustomColors.primary),
                                ),
                              ),
                              TextButton(
                                onPressed: () {
                                  FirestoreService().updateCurrentPage(
                                      'A8', (activeStep + 1).toString());
                                  Navigator.pushAndRemoveUntil(
                                      context,
                                      MaterialPageRoute<void>(
                                          builder: (BuildContext context) =>
                                              const MainPage(
                                                activePage: 1,
                                              )),
                                      (route) => false);
                                },
                                child: const Text(
                                  'Salir',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(color: CustomColors.error),
                                ),
                              ),
                            ],
                          ));
                },
                icon: const Icon(
                  Icons.close,
                  color: CustomColors.primaryDark,
                ),
              ),
            ),
            const Spacer(),
            favorito(),
          ],
        ),
        Text(
          headerText(),
          textScaler: const TextScaler.linear(1.0),
          textAlign: TextAlign.center,
          style: const TextStyle(
            color: CustomColors.primaryDark,
            fontSize: 24,
          ),
        ),
        Container(
          margin: const EdgeInsets.only(top: 20, bottom: 20),
          child: header(),
        ),
      ],
    );
  }

  Widget favorito() {
    return StreamBuilder<QuerySnapshot>(
        stream: FirestoreService().favoritos(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasData) {
            return SizedBox(
              width: 50,
              height: 50,
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (context, index) {
                    var document = snapshot.data?.docs[index];
                    if (document != null && document.id == uid) {
                      switch (document['A8']) {
                        case true:
                          return IconButton(
                              icon: Icon(MdiIcons.star,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('A8', false)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Eliminado de favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                        case false:
                          return IconButton(
                              icon: Icon(MdiIcons.starOutline,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService().setFavorito('A8', true).then(
                                    (value) => Fluttertoast.showToast(
                                        msg: "Agregado a favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                      }
                    } else {
                      return Container();
                    }
                    return null;
                  }),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(
                color: Colors.red,
              ),
            );
          }
        });
  }

  Widget bottomStructure() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        nextButton(),
      ],
    );
  }

  Widget nextButton() {
    Size size = MediaQuery.of(context).size;
    if (activeStep >= 1 && activeStep < 3) {
      return SizedBox(
        width: size.width / 1,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        WidgetStateProperty.all<Color>(CustomColors.background),
                    shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: const BorderSide(color: CustomColors.primary)),
                    ),
                  ),
                  onPressed: () {
                    setState(() {
                      activeStep = activeStep - 1;
                    });
                  },
                  child: const Text(
                    'Anterior',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(color: CustomColors.primary),
                  )),
            ),
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor:
                      WidgetStateProperty.all<Color>(CustomColors.primary),
                  shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ),
                  ),
                ),
                onPressed: () {
                  if (activeStep < 2) {
                    setState(() {
                      activeStep++;
                    });
                  } else {
                    FirestoreService().updateCurrentPage('A8', '1');
                    FirestoreService().guardarRespuestasA8(
                        horarioRegular,
                        siestasRRegular,
                        bebidasAntesDormir,
                        comerMucho,
                        ejercicioAntesDormir,
                        actividadesRelajantes,
                        ropaComoda,
                        dormitorioAcogedor,
                        pantallasAntesDormir,
                        medicamentos);

                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute<void>(
                            builder: (BuildContext context) => const MainPage(
                                  activePage: 1,
                                )),
                        (route) => false);
                  }
                },
                child: Text(
                  buttonText(),
                  textScaler: const TextScaler.linear(1.0),
                  style: const TextStyle(
                      color: CustomColors.background,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      return SizedBox(
        width: size.width / 1.2,
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor:
                WidgetStateProperty.all<Color>(CustomColors.primary),
            shape: WidgetStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              ),
            ),
          ),
          onPressed: () {
            if (activeStep < 2) {
              setState(() {
                activeStep++;
              });
            } else {
              FirestoreService().updateCurrentPage('A8', '1');
              FirestoreService().guardarRespuestasA8(
                  horarioRegular,
                  siestasRRegular,
                  bebidasAntesDormir,
                  comerMucho,
                  ejercicioAntesDormir,
                  actividadesRelajantes,
                  ropaComoda,
                  dormitorioAcogedor,
                  pantallasAntesDormir,
                  medicamentos);
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute<void>(
                      builder: (BuildContext context) => const MainPage(
                            activePage: 1,
                          )),
                  (route) => false);
            }
          },
          child: Text(
            buttonText(),
            textScaler: const TextScaler.linear(1.0),
            style: const TextStyle(color: CustomColors.background),
          ),
        ),
      );
    }
  }

  Widget header() {
    Size size = MediaQuery.of(context).size;
    switch (activeStep) {
      default:
        return Image(
          width: size.width / 3,
          height: size.width / 3,
          fit: BoxFit.scaleDown,
          image: AssetImage(imageHeader()),
        );
    }
  }

  String imageHeader() {
    switch (activeStep) {
      case 0:
        return 'assets/images/buen-dormir-icon.png';

      default:
        return 'assets/images/buen-dormir-icon.png';
    }
  }

  String headerText() {
    switch (activeStep) {
      case 0:
        return 'Buen dormir';

      default:
        return 'Buen dormir';
    }
  }

  String buttonText() {
    switch (activeStep) {
      case 2:
        return 'Finalizar';

      default:
        return 'Siguiente ';
    }
  }

  Widget content() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 5, right: 20, left: 20, bottom: 30),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Para tener un buen descanso y que nuestro cuerpo y mente funcionen de mejor manera el día siguiente, no solo es importante la cantidad de horas que dormimos, sino que la calidad del sueño que tengamos.\n\n',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                'Descansar bien mejora tu estado de ánimo y rendimiento en general, mantiene tu mente alerta, te ayuda a concentrarte, y fortalece tu sistema inmune. Por el contrario, cuando no duermes lo suficiente, tu concentración sufre, puedes irritarte con mayor facilidad y aumenta la probabilidad que presentes problemas de salud física o mental.\n\n',
                          ),
                          TextSpan(
                            text:
                                'Dormir es importante para tener una buena salud mental.\n\n',
                          ),
                          TextSpan(
                            text:
                                'Aquí te entregamos algunas recomendaciones para que tengas un buen dormir:\n\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Mantén un horario regular para acostarte y levantarte.\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'En la medida de lo posible, intenta dormir entre 8 y 10 horas de sueño.\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text: 'Evita tomar siestas durante el día.\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Presta atención a tu consumo de cafeína, el consumo excesivo aumenta los niveles de ansiedad y afecta la calidad del sueño.\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Si consumes café, energéticas, té u otro estimulante, procura que no sea después de las 18:00 hrs.\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Evita comer en exceso y comidas condimentadas o azucaradas antes de acostarte.\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Realiza ejercicio regularmente, pero no justo antes de ir a dormir.\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Antes de acostarte realiza alguna actividad relajante (ej.: leer un libro, escuchar un podcast o música, meditar, tomar un baño de agua cálida).\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text: 'Usa ropa y pijamas cómodos.\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Haz de tu dormitorio un espacio acogedor, silencioso, oscuro y de temperatura agradable.\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Evita ver televisión, usar el celular y el computador en la cama antes de dormir. Puedes colocar una alarma en tu teléfono para saber cuándo es el momento de tomar distancia de las pantallas.\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Minimiza las distracciones (ej.: carga tu teléfono en otra habitación; silencia las notificaciones; utiliza tapones de oído o ruido blanco en caso que haya mucho ruido).\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'No te automediques. Si estás teniendo problemas para conciliar el sueño, solicita ayuda profesional.\n',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 1:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 5, right: 20, left: 20, bottom: 30),
                    child: RichText(
                      text: const TextSpan(
                        text: '',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            fontWeight: FontWeight.bold,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                'Te invitamos a revisar la presencia/ausencia de estos hábitos en tu vida cotidiana por medio de una breve actividad. A continuación, encontrarás un checklist donde puedes visualizar tus hábitos de sueño ¿Hay aspectos que te gustaría mejorar? Tener claridad de ello puede ayudarte a realizar cambiar en éstos para favorecer tu bienestar.',
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                      //height: 1000,
                      margin: const EdgeInsets.all(15),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15)),
                      child: ListView(
                        physics: const NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        children: [
                          ListTile(
                            title: const Text(
                              '¿Tengo un horario regular para dormir?',
                              textScaler: TextScaler.linear(1.0),
                            ),
                            trailing: ToggleSwitch(
                              minWidth: 50.0,
                              cornerRadius: 20.0,
                              activeBgColors: const [
                                [CustomColors.activeSwitch],
                                [CustomColors.activeSwitch]
                              ],
                              activeFgColor: CustomColors.primaryDark,
                              inactiveBgColor: CustomColors.background,
                              inactiveFgColor: CustomColors.primaryDark,
                              borderWidth: 1,
                              borderColor: const [CustomColors.primaryDark],
                              initialLabelIndex: startHorarioRegular,
                              totalSwitches: 2,
                              labels: const ['Sí', 'No'],
                              customTextStyles: const [
                                TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 14)
                              ],
                              onToggle: (index) {
                                if (index == 0) {
                                  horarioRegular = 'Sí';
                                } else {
                                  horarioRegular = 'No';
                                }
                              },
                            ),
                          ),
                          const Divider(
                            thickness: 1,
                          ),
                          ListTile(
                            title: const Text(
                              '¿Tomo siestas regularmente?',
                              textScaler: TextScaler.linear(1.0),
                            ),
                            trailing: ToggleSwitch(
                              minWidth: 50.0,
                              cornerRadius: 20.0,
                              activeBgColors: const [
                                [CustomColors.activeSwitch],
                                [CustomColors.activeSwitch]
                              ],
                              activeFgColor: CustomColors.primaryDark,
                              inactiveBgColor: CustomColors.background,
                              inactiveFgColor: CustomColors.primaryDark,
                              borderWidth: 1,
                              borderColor: const [CustomColors.primaryDark],
                              initialLabelIndex: startSiestasRRegular,
                              totalSwitches: 2,
                              labels: const ['Sí', 'No'],
                              customTextStyles: const [
                                TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 14)
                              ],
                              onToggle: (index) {
                                if (index == 0) {
                                  siestasRRegular = 'Sí';
                                } else {
                                  siestasRRegular = 'No';
                                }
                              },
                            ),
                          ),
                          const Divider(
                            thickness: 1,
                          ),
                          ListTile(
                            title: const Text(
                              '¿Suelo beber bebidas estimulantes antes de dormir?',
                              textScaler: TextScaler.linear(1.0),
                            ),
                            trailing: ToggleSwitch(
                              minWidth: 50.0,
                              cornerRadius: 20.0,
                              activeBgColors: const [
                                [CustomColors.activeSwitch],
                                [CustomColors.activeSwitch]
                              ],
                              activeFgColor: CustomColors.primaryDark,
                              inactiveBgColor: CustomColors.background,
                              inactiveFgColor: CustomColors.primaryDark,
                              borderWidth: 1,
                              borderColor: const [CustomColors.primaryDark],
                              initialLabelIndex: startBebidasAntesDormir,
                              totalSwitches: 2,
                              labels: const ['Sí', 'No'],
                              customTextStyles: const [
                                TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 14)
                              ],
                              onToggle: (index) {
                                if (index == 0) {
                                  bebidasAntesDormir = 'Sí';
                                } else {
                                  bebidasAntesDormir = 'No';
                                }
                              },
                            ),
                          ),
                          const Divider(
                            thickness: 1,
                          ),
                          ListTile(
                            title: const Text(
                              '¿Como mucho antes de dormir?',
                              textScaler: TextScaler.linear(1.0),
                            ),
                            trailing: ToggleSwitch(
                              minWidth: 50.0,
                              cornerRadius: 20.0,
                              activeBgColors: const [
                                [CustomColors.activeSwitch],
                                [CustomColors.activeSwitch]
                              ],
                              activeFgColor: CustomColors.primaryDark,
                              inactiveBgColor: CustomColors.background,
                              inactiveFgColor: CustomColors.primaryDark,
                              borderWidth: 1,
                              borderColor: const [CustomColors.primaryDark],
                              initialLabelIndex: startComerMucho,
                              totalSwitches: 2,
                              labels: const ['Sí', 'No'],
                              customTextStyles: const [
                                TextStyle(fontWeight: FontWeight.bold)
                              ],
                              onToggle: (index) {
                                if (index == 0) {
                                  comerMucho = 'Sí';
                                } else {
                                  comerMucho = 'No';
                                }
                              },
                            ),
                          ),
                          const Divider(
                            thickness: 1,
                          ),
                          ListTile(
                            title: const Text(
                              '¿Hago ejercicio antes de dormir?',
                              textScaler: TextScaler.linear(1.0),
                            ),
                            trailing: ToggleSwitch(
                              minWidth: 50.0,
                              cornerRadius: 20.0,
                              activeBgColors: const [
                                [CustomColors.activeSwitch],
                                [CustomColors.activeSwitch]
                              ],
                              activeFgColor: CustomColors.primaryDark,
                              inactiveBgColor: CustomColors.background,
                              inactiveFgColor: CustomColors.primaryDark,
                              borderWidth: 1,
                              borderColor: const [CustomColors.primaryDark],
                              initialLabelIndex: startEjercicioAntesDormir,
                              totalSwitches: 2,
                              labels: const ['Sí', 'No'],
                              customTextStyles: const [
                                TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 14)
                              ],
                              onToggle: (index) {
                                if (index == 0) {
                                  ejercicioAntesDormir = 'Sí';
                                } else {
                                  ejercicioAntesDormir = 'No';
                                }
                              },
                            ),
                          ),
                          const Divider(
                            thickness: 1,
                          ),
                          ListTile(
                            title: const Text(
                              '¿Hago actividades para relajarme en la noche?',
                              textScaler: TextScaler.linear(1.0),
                            ),
                            trailing: ToggleSwitch(
                              minWidth: 50.0,
                              cornerRadius: 20.0,
                              activeBgColors: const [
                                [CustomColors.activeSwitch],
                                [CustomColors.activeSwitch]
                              ],
                              activeFgColor: CustomColors.primaryDark,
                              inactiveBgColor: CustomColors.background,
                              inactiveFgColor: CustomColors.primaryDark,
                              borderWidth: 1,
                              borderColor: const [CustomColors.primaryDark],
                              initialLabelIndex: startActividadesRelajantes,
                              totalSwitches: 2,
                              labels: const ['Sí', 'No'],
                              customTextStyles: const [
                                TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 14)
                              ],
                              onToggle: (index) {
                                if (index == 0) {
                                  actividadesRelajantes = 'Sí';
                                } else {
                                  actividadesRelajantes = 'No';
                                }
                              },
                            ),
                          ),
                          const Divider(
                            thickness: 1,
                          ),
                          ListTile(
                            title: const Text(
                              '¿Duermo con ropa cómoda?',
                              textScaler: TextScaler.linear(1.0),
                            ),
                            trailing: ToggleSwitch(
                              minWidth: 50.0,
                              cornerRadius: 20.0,
                              activeBgColors: const [
                                [CustomColors.activeSwitch],
                                [CustomColors.activeSwitch]
                              ],
                              activeFgColor: CustomColors.primaryDark,
                              inactiveBgColor: CustomColors.background,
                              inactiveFgColor: CustomColors.primaryDark,
                              borderWidth: 1,
                              borderColor: const [CustomColors.primaryDark],
                              initialLabelIndex: startRopaComoda,
                              totalSwitches: 2,
                              labels: const ['Sí', 'No'],
                              customTextStyles: const [
                                TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 14)
                              ],
                              onToggle: (index) {
                                if (index == 0) {
                                  ropaComoda = 'Sí';
                                } else {
                                  ropaComoda = 'No';
                                }
                              },
                            ),
                          ),
                          const Divider(
                            thickness: 1,
                          ),
                          ListTile(
                            title: const Text(
                              '¿Mi dormitorio es acogedor para dormir?',
                              textScaler: TextScaler.linear(1.0),
                            ),
                            trailing: ToggleSwitch(
                              minWidth: 50.0,
                              cornerRadius: 20.0,
                              activeBgColors: const [
                                [CustomColors.activeSwitch],
                                [CustomColors.activeSwitch]
                              ],
                              activeFgColor: CustomColors.primaryDark,
                              inactiveBgColor: CustomColors.background,
                              inactiveFgColor: CustomColors.primaryDark,
                              borderWidth: 1,
                              borderColor: const [CustomColors.primaryDark],
                              initialLabelIndex: startDormitorioAcogedor,
                              totalSwitches: 2,
                              labels: const ['Sí', 'No'],
                              customTextStyles: const [
                                TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 14)
                              ],
                              onToggle: (index) {
                                if (index == 0) {
                                  dormitorioAcogedor = 'Sí';
                                } else {
                                  dormitorioAcogedor = 'No';
                                }
                              },
                            ),
                          ),
                          const Divider(
                            thickness: 1,
                          ),
                          ListTile(
                            title: const Text(
                              '¿Veo pantallas (ej. celular, computador) antes de dormir?',
                              textScaler: TextScaler.linear(1.0),
                            ),
                            trailing: ToggleSwitch(
                              minWidth: 50.0,
                              cornerRadius: 20.0,
                              activeBgColors: const [
                                [CustomColors.activeSwitch],
                                [CustomColors.activeSwitch]
                              ],
                              activeFgColor: CustomColors.primaryDark,
                              inactiveBgColor: CustomColors.background,
                              inactiveFgColor: CustomColors.primaryDark,
                              borderWidth: 1,
                              borderColor: const [CustomColors.primaryDark],
                              initialLabelIndex: startPantallasAntesDormir,
                              totalSwitches: 2,
                              labels: const ['Sí', 'No'],
                              customTextStyles: const [
                                TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 14)
                              ],
                              onToggle: (index) {
                                if (index == 0) {
                                  pantallasAntesDormir = 'Sí';
                                } else {
                                  pantallasAntesDormir = 'No';
                                }
                              },
                            ),
                          ),
                          const Divider(
                            thickness: 1,
                          ),
                          ListTile(
                            title: const Text(
                              '¿Tomo medicamentos que puedas afectar mi calidad de sueño?',
                              textScaler: TextScaler.linear(1.0),
                            ),
                            trailing: ToggleSwitch(
                              minWidth: 50.0,
                              cornerRadius: 20.0,
                              activeBgColors: const [
                                [CustomColors.activeSwitch],
                                [CustomColors.activeSwitch]
                              ],
                              activeFgColor: CustomColors.primaryDark,
                              inactiveBgColor: CustomColors.background,
                              inactiveFgColor: CustomColors.primaryDark,
                              borderWidth: 1,
                              borderColor: const [CustomColors.primaryDark],
                              initialLabelIndex: startMedicamentos,
                              totalSwitches: 2,
                              labels: const ['Sí', 'No'],
                              customTextStyles: const [
                                TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 14)
                              ],
                              onToggle: (index) {
                                if (index == 0) {
                                  medicamentos = 'Sí';
                                } else {
                                  medicamentos = 'No';
                                }
                              },
                            ),
                          ),
                        ],
                      )),
                ],
              ),
            ),
          ),
        );
      case 2:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                      width: size.width,
                      margin: const EdgeInsets.only(
                          top: 15, left: 20, bottom: 5, right: 10),
                      child: const Text(
                        '¿Qué hacer cuando es la hora de dormir y no consigo hacerlo? ',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                          fontSize: 22,
                          color: CustomColors.primary,
                        ),
                      )),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 5, right: 20, left: 20, bottom: 30),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Si bien las recomendaciones anteriores te pueden ayudar a tener una mejor calidad de sueño, es posible que haya situaciones donde aún siguiéndolas te encuentres con dificultades para conciliar o mantener el sueño. Te compartimos aquí algunas ideas que pueden serte útiles para esos momentos:\n\n',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text: '1. Ajusta tu ambiente:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' ¿hay mucho ruido? Intenta con tapones para los oídos o escucha ruido blanco. ¿Hay mucha luz? Apaga las luces que puedas, utiliza un antifaz para dormir, o evalúa si es posible conseguir cortinas black-out. ¿Muy caliente o muy frío? Abre o cierra un ventana, o añade o remueve una frazada.\n\n',
                          ),
                          TextSpan(
                            text: '2. Cambia el foco:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' a veces a nuestros cerebros les gusta volverse activos tan pronto como reducimos el ritmo (ej.: repasando eventos estresantes en nuestra mente). Mantener la mente ocupada con un juego puede ser útil en estos casos, como el ',
                          ),
                          TextSpan(
                            text: ' juego del alfabeto,',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' para llevar a la mente a un estado más neutral. Elige una categoría que guste (ej.: flores, canciones, bandas de música, películas, autos, etc), y luego piensa en algún ítem para esa categoría que comience con cada letra del alfabeto. Si no sabes qué responder en una letra, solo sáltala y continúa con la siguiente.\n\n',
                          ),
                          TextSpan(
                            text: '3. Sal de la cama:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' si pasan 20 minutos y no logras dormirte, levántate y has algo relajante (con luz baja) hasta que sientas el sueño suficiente para volver a dormir. Te recomendamos que evites mirar el reloj, pues te puede producir mayor estrés.\n\n',
                          ),
                          TextSpan(
                            text: '4. Mantén la perspectiva:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' una noche de mal sueño no te destruirá. A la mañana siguiente, la luz del día te ayudará a sentirte alerta (ej.: puedes abrir las cortinas o salir un momento afuera). Un poco de agua fría en la cara o salir al aire libre te pueden ayudar a sentirte más despiertx. No dejes que tu mente añada que no estás durmiendo como parte de tus preocupaciones, la próxima noche no está tan lejos.\n\n',
                          ),
                          TextSpan(
                            text: '5. Planifica con anticipación:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' si te despiertas frecuentemente por alguna razón específica (ej.: sentir mucho frío), tómate algunos minutos antes de ir a dormir para solucionarlos o para tener una solución rápida cerca (ej.: frazada extra a los pies de la cama). Quizás no soluciones completamente el despertar por las noches, pero puedes minimizar el tiempo que te toma atender a aquello que te despierta por las noches.\n\n',
                          ),
                          TextSpan(
                            text:
                                'Busca ayuda profesional si esto no es suficiente: si regularmente tienes problemas para conciliar o mantener el sueño, puede que hayan factores que estén contribuyendo a ello (ej.: tomar mucha cafeína; siestas largas y prolongadas; o  usar dispositivos tecnológicos cerca del horario de sueño). Si descartaste estos factores y los problemas de sueño persisten, consulta con un médico general y/o un especialista en trastornos del sueño. El insomnio se considera crónico cuando ocurre tres o cuatro veces por semana por al menos tres meses.',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      default:
        return Container();
    }
  }
}
