// ignore_for_file: file_names, avoid_print

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:seguimiento/colors.dart';
import 'package:seguimiento/user_pages/MainPage.dart';
import '../../firebase.dart';

class A12 extends StatefulWidget {
  const A12({
    Key? key,
  }) : super(key: key);

  @override
  State<A12> createState() => _A12State();
}

class _A12State extends State<A12> {
  String uid = FirebaseAuth.instance.currentUser!.uid;
  bool loadingData = true;
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('page-control');
  String activeStepData = '';
  void getActivePageStored() {
    collectionReference.doc(uid).get().then((value) {
      var fields = value;
      activeStepData = fields['A12'];
      setState(() {
        if (int.parse(activeStepData) < 6) {
          activeStep = int.parse(activeStepData) - 1;
          Future.delayed(const Duration(milliseconds: 500), () {
            setState(() {
              loadingData = false;
            });
          });
        }
      });
    });
  }

  ScrollController scrollController = ScrollController();

  bool favorite = false;
  CollectionReference favoriteData =
      FirebaseFirestore.instance.collection('favoritos');
  void getFavData() {
    favoriteData.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        favorite = fields['A12'];
      });
    }).catchError((err) {
      print('Error: $err');
    });
  }

  int activeStep = 0;
  @override
  void initState() {
    super.initState();
    getActivePageStored();
    getFavData();
  }

  @override
  void dispose() {
    super.dispose();
  }

  bool active1 = false;
  bool active2 = false;
  bool active3 = false;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        active1 = true;
        active2 = false;
        active3 = false;

        break;
      case 1:
        active1 = true;
        active2 = true;
        active3 = false;
        break;
      case 2:
        active1 = true;
        active2 = true;
        active3 = true;
        break;

      default:
    }
    return PopScope(
      canPop: false,
      onPopInvokedWithResult: (bool didPop, result) {
        if (didPop) {
          FirestoreService()
              .updateCurrentPage('A12', (activeStep + 1).toString());
          return;
        }
      },
      child: loadingData
          ? Container(
              width: size.width,
              height: size.height,
              color: CustomColors.primaryDark,
              child: const Center(
                  child: CircularProgressIndicator(
                color: CustomColors.tertiary70,
              )))
          : Container(
              color: Colors.black,
              child: SafeArea(
                child: Scaffold(
                  backgroundColor: CustomColors.background,
                  body: Center(
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              width: size.width / 3,
                              height: 5,
                              color: active1
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 3,
                              height: 5,
                              color: active2
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 3,
                              height: 5,
                              color: active3
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                          ],
                        ),
                        content(),
                        Container(
                          margin: const EdgeInsets.only(top: 10, bottom: 10),
                          child: bottomStructure(),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
    );
  }

  Widget topStructure() {
    return Column(
      children: [
        Row(
          children: [
            Container(
              alignment: Alignment.topRight,
              child: IconButton(
                onPressed: () {
                  FirestoreService()
                      .updateCurrentPage('A12', (activeStep + 1).toString())
                      .then((value) => Navigator.of(context).pop());
                },
                icon: const Icon(
                  Icons.close,
                  color: CustomColors.primaryDark,
                ),
              ),
            ),
            const Spacer(),
            favorito(),
          ],
        ),
        Container(
          margin: const EdgeInsets.only(left: 10, right: 10),
          child: Text(
            headerText(),
            textScaler: const TextScaler.linear(1.0),
            textAlign: TextAlign.center,
            style: const TextStyle(
              color: CustomColors.primaryDark,
              fontSize: 24,
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(top: 20, bottom: 20),
          child: header(),
        ),
      ],
    );
  }

  Widget favorito() {
    return StreamBuilder<QuerySnapshot>(
        stream: FirestoreService().favoritos(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasData) {
            return SizedBox(
              width: 50,
              height: 50,
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (context, index) {
                    var document = snapshot.data?.docs[index];
                    if (document != null && document.id == uid) {
                      switch (document['A12']) {
                        case true:
                          return IconButton(
                              icon: Icon(MdiIcons.star,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('A12', false)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Eliminado de favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                        case false:
                          return IconButton(
                              icon: Icon(MdiIcons.starOutline,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('A12', true)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Agregado a favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                      }
                    } else {
                      return Container();
                    }
                    return null;
                  }),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(
                color: Colors.red,
              ),
            );
          }
        });
  }

  Widget bottomStructure() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        nextButton(),
      ],
    );
  }

  Widget nextButton() {
    Size size = MediaQuery.of(context).size;
    if (activeStep >= 1 && activeStep < 3) {
      return SizedBox(
        width: size.width / 1,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        WidgetStateProperty.all<Color>(CustomColors.background),
                    shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: const BorderSide(color: CustomColors.primary)),
                    ),
                  ),
                  onPressed: () {
                    setState(() {
                      activeStep = activeStep - 1;
                    });
                  },
                  child: const Text(
                    'Anterior',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(color: CustomColors.primary),
                  )),
            ),
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor:
                      WidgetStateProperty.all<Color>(CustomColors.primary),
                  shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ),
                  ),
                ),
                onPressed: () {
                  if (activeStep < 2) {
                    setState(() {
                      activeStep++;
                    });
                  } else {
                    FirestoreService().updateCurrentPage('A12', '1');
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute<void>(
                            builder: (BuildContext context) => const MainPage(
                                  activePage: 1,
                                )),
                        (route) => false);
                  }
                },
                child: Text(
                  buttonText(),
                  textScaler: const TextScaler.linear(1.0),
                  style: const TextStyle(
                      color: CustomColors.background,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      return SizedBox(
        width: size.width / 1.2,
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor:
                WidgetStateProperty.all<Color>(CustomColors.primary),
            shape: WidgetStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              ),
            ),
          ),
          onPressed: () {
            if (activeStep < 2) {
              setState(() {
                activeStep++;
              });
            } else {
              FirestoreService().updateCurrentPage('A12', '1');
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute<void>(
                      builder: (BuildContext context) => const MainPage(
                            activePage: 1,
                          )),
                  (route) => false);
            }
          },
          child: Text(
            buttonText(),
            textScaler: const TextScaler.linear(1.0),
            style: const TextStyle(color: CustomColors.background),
          ),
        ),
      );
    }
  }

  Widget header() {
    Size size = MediaQuery.of(context).size;
    switch (activeStep) {
      default:
        return Image(
          width: size.width / 3,
          height: size.width / 3,
          fit: BoxFit.scaleDown,
          image: AssetImage(imageHeader()),
        );
    }
  }

  String imageHeader() {
    switch (activeStep) {
      case 0:
        return 'assets/images/cambiando-forma-icon.png';

      default:
        return 'assets/images/cambiando-forma-icon.png';
    }
  }

  String headerText() {
    switch (activeStep) {
      case 0:
        return 'Cambiando la forma en la que nos hablamos';

      default:
        return 'Cambiando la forma en la que nos hablamos';
    }
  }

  String buttonText() {
    switch (activeStep) {
      case 2:
        return 'Finalizar';

      default:
        return 'Siguiente ';
    }
  }

  Widget content() {
    switch (activeStep) {
      case 0:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 20, right: 20, left: 20, bottom: 30),
                    child: RichText(
                      text: const TextSpan(
                        text: '',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                '¿Alguna vez has notado la forma en la que te refieres a ti mismx frente a situaciones difíciles? ¿te has encontrado diciéndote cosas negativas?\n\n',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          TextSpan(
                            text:
                                'En ocasiones frente a situaciones difíciles o cuando nos estamos enfrentando a un problema nos hablamos de forma negativa con frases tales como “nadie me entiende” o “soy insuficiente”.',
                          ),
                          TextSpan(
                            text:
                                ' Este diálogo interno negativo refleja pensamientos, creencias y actitudes que nos llevan a reaccionar de forma negativa y hace que sea más difícil poder afrontar los problemas ',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          TextSpan(
                            text:
                                '(recordemos lo aprendido acerca de los errores de pensamiento y regulación emocional).\n\n',
                          ),
                          TextSpan(
                            text:
                                'Por esto, es importante que podamos identificar cuando nos hablamos de forma negativa y cambiar estas frases por otras más constructivas y realistas.',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 1:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: const EdgeInsets.only(
                        top: 5, right: 20, left: 20, bottom: 5),
                    child: RichText(
                      text: const TextSpan(
                        text: '',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                '¿Cuáles son las señales de que te estás hablando de forma negativa?\n\n',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text: '1. ',
                          ),
                          TextSpan(
                            text:
                                'Usas palabras o frases muy críticas tales como “debo/debería” o “tengo que”. Ejemplo: “debería hacerlo siempre bien”.\n',
                          ),
                          TextSpan(
                            text: '2. ',
                          ),
                          TextSpan(
                            text:
                                'Usas palabras o frases catastróficas para situaciones que no lo ameritan. Ejemplo: “que me haya equivocado es lo peor que podía pasar”.\n',
                          ),
                          TextSpan(
                            text: '3. ',
                          ),
                          TextSpan(
                            text:
                                'Sobregeneralizas las situaciones. Ejemplo: “siempre me saldrá mal” o “nunca podré lograrlo”.\n',
                          ),
                          TextSpan(
                            text: '4. ',
                          ),
                          TextSpan(
                            text:
                                'Usas frases hostiles para referirte a ti mismx. Ejemplo: “soy tontx”.',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 2:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: const EdgeInsets.only(
                        top: 5, right: 20, left: 20, bottom: 20),
                    child: RichText(
                      text: const TextSpan(
                        text: '',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                '¿Qué puedes hacer para cambiar la forma en la que te hablas?\n\n',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text: '1. ',
                          ),
                          TextSpan(
                            text: 'Argumenta el diálogo negativo con lógica.\n',
                          ),
                          TextSpan(
                            text: '2. ',
                          ),
                          TextSpan(
                            text:
                                'Cambia los “tengo que” y “debo/debería” por “¿Por qué debería?”, “Me gustaría que”.\n',
                          ),
                          TextSpan(
                            text: '3. ',
                          ),
                          TextSpan(
                            text:
                                'Cuestiona tus ideas catastróficas y evalúa la situación de forma más realista.\n',
                          ),
                          TextSpan(
                            text: '4. ',
                          ),
                          TextSpan(
                            text:
                                'Cuestiona las generalizaciones excesivas cambiando los “siempre” y “nunca” por palabras más precisas como “en este momento”.\n',
                          ),
                          TextSpan(
                            text: '5. ',
                          ),
                          TextSpan(
                            text: 'Usar autoafirmaciones positivas como:\n',
                          ),
                          TextSpan(
                            text: '      ·  ',
                          ),
                          TextSpan(
                            text:
                                '“si lo intento puedo resolver este problema”\n',
                          ),
                          TextSpan(
                            text: '      ·  ',
                          ),
                          TextSpan(
                            text: '“no tengo que complacer a todos”\n',
                          ),
                          TextSpan(
                            text: '      ·  ',
                          ),
                          TextSpan(
                            text: '“este mal momento va a terminar”\n',
                          ),
                          TextSpan(
                            text: '      ·  ',
                          ),
                          TextSpan(
                            text: '“puedo pedir ayuda si lo necesito”\n',
                          ),
                          TextSpan(
                            text: '      ·  ',
                          ),
                          TextSpan(
                            text: '“estoy orgullosx de mí”\n\n',
                          ),
                          TextSpan(
                            text:
                                'Recuerda que la forma en la que te hablas a ti mismx tiene un gran impacto en tu bienestar emocional, por lo que es importante ser consciente de tus pensamientos y cambiarlos por frases más constructivas y realistas.',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );

      default:
        return Container();
    }
  }
}
