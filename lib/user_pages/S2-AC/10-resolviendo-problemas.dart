// ignore_for_file: file_names, avoid_print

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:seguimiento/colors.dart';
import 'package:seguimiento/user_pages/MainPage.dart';
import '../../firebase.dart';

class A10 extends StatefulWidget {
  const A10({
    Key? key,
  }) : super(key: key);

  @override
  State<A10> createState() => _A10State();
}

class _A10State extends State<A10> {
  String uid = FirebaseAuth.instance.currentUser!.uid;
  bool loadingData = true;
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('page-control');
  String activeStepData = '';
  void getActivePageStored() {
    collectionReference.doc(uid).get().then((value) {
      var fields = value;
      activeStepData = fields['A10'];
      setState(() {
        if (int.parse(activeStepData) < 6) {
          activeStep = int.parse(activeStepData) - 1;
          Future.delayed(const Duration(milliseconds: 500), () {
            setState(() {
              loadingData = false;
            });
          });
        }
      });
    });
  }

  bool favorite = false;
  CollectionReference favoriteData =
      FirebaseFirestore.instance.collection('favoritos');
  void getFavData() {
    favoriteData.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        favorite = fields['A10'];
      });
    }).catchError((err) {
      print('Error: $err');
    });
  }

  ScrollController scrollController = ScrollController();

  int activeStep = 0;
  @override
  void initState() {
    super.initState();

    getActivePageStored();
    getFavData();
  }

  @override
  void dispose() {
    super.dispose();
  }

  bool active1 = false;
  bool active2 = false;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        active1 = true;
        active2 = false;

        break;
      case 1:
        active1 = true;
        active2 = true;

        break;

      default:
    }
    return PopScope(
      canPop: false,
      onPopInvokedWithResult: (bool didPop, result) {
        if (didPop) {
          FirestoreService()
              .updateCurrentPage('A10', (activeStep + 1).toString());
          return;
        }
      },
      child: loadingData
          ? Container(
              width: size.width,
              height: size.height,
              color: CustomColors.primaryDark,
              child: const Center(
                  child: CircularProgressIndicator(
                color: CustomColors.tertiary70,
              )))
          : Container(
              color: Colors.black,
              child: SafeArea(
                child: Scaffold(
                  backgroundColor: CustomColors.background,
                  body: Center(
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              width: size.width / 2,
                              height: 5,
                              color: active1
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 2,
                              height: 5,
                              color: active2
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                          ],
                        ),
                        content(),
                        Container(
                          margin: const EdgeInsets.only(top: 10, bottom: 10),
                          child: bottomStructure(),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
    );
  }

  Widget topStructure() {
    return Column(
      children: [
        Row(
          children: [
            Container(
              alignment: Alignment.topRight,
              child: IconButton(
                onPressed: () {
                  FirestoreService()
                      .updateCurrentPage('A10', (activeStep + 1).toString())
                      .then((value) => Navigator.of(context).pop());
                },
                icon: const Icon(
                  Icons.close,
                  color: CustomColors.primaryDark,
                ),
              ),
            ),
            const Spacer(),
            favorito(),
          ],
        ),
        Text(
          headerText(),
          textScaler: const TextScaler.linear(1.0),
          textAlign: TextAlign.center,
          style: const TextStyle(
            color: CustomColors.primaryDark,
            fontSize: 24,
          ),
        ),
        Container(
          margin: const EdgeInsets.only(top: 20, bottom: 20),
          child: header(),
        ),
      ],
    );
  }

  Widget favorito() {
    return StreamBuilder<QuerySnapshot>(
        stream: FirestoreService().favoritos(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasData) {
            return SizedBox(
              width: 50,
              height: 50,
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (context, index) {
                    var document = snapshot.data?.docs[index];
                    if (document != null && document.id == uid) {
                      switch (document['A10']) {
                        case true:
                          return IconButton(
                              icon: Icon(MdiIcons.star,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('A10', false)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Eliminado de favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                        case false:
                          return IconButton(
                              icon: Icon(MdiIcons.starOutline,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('A10', true)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Agregado a favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                      }
                    } else {
                      return Container();
                    }
                    return null;
                  }),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(
                color: Colors.red,
              ),
            );
          }
        });
  }

  Widget bottomStructure() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        nextButton(),
      ],
    );
  }

  Widget nextButton() {
    Size size = MediaQuery.of(context).size;
    if (activeStep >= 1 && activeStep < 2) {
      return SizedBox(
        width: size.width / 1,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        WidgetStateProperty.all<Color>(CustomColors.background),
                    shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: const BorderSide(color: CustomColors.primary)),
                    ),
                  ),
                  onPressed: () {
                    setState(() {
                      activeStep = activeStep - 1;
                    });
                  },
                  child: const Text(
                    'Anterior',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(color: CustomColors.primary),
                  )),
            ),
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor:
                      WidgetStateProperty.all<Color>(CustomColors.primary),
                  shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ),
                  ),
                ),
                onPressed: () {
                  if (activeStep < 1) {
                    setState(() {
                      activeStep++;
                    });
                  } else {
                    FirestoreService().updateCurrentPage('A10', '1');
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute<void>(
                            builder: (BuildContext context) => const MainPage(
                                  activePage: 1,
                                )),
                        (route) => false);
                  }
                },
                child: Text(
                  buttonText(),
                  textScaler: const TextScaler.linear(1.0),
                  style: const TextStyle(
                      color: CustomColors.background,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      return SizedBox(
        width: size.width / 1.2,
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor:
                WidgetStateProperty.all<Color>(CustomColors.primary),
            shape: WidgetStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              ),
            ),
          ),
          onPressed: () {
            if (activeStep < 1) {
              setState(() {
                activeStep++;
              });
            } else {
              FirestoreService().updateCurrentPage('A10', '1');
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute<void>(
                      builder: (BuildContext context) => const MainPage(
                            activePage: 1,
                          )),
                  (route) => false);
            }
          },
          child: Text(
            buttonText(),
            textScaler: const TextScaler.linear(1.0),
            style: const TextStyle(color: CustomColors.background),
          ),
        ),
      );
    }
  }

  Widget header() {
    Size size = MediaQuery.of(context).size;
    switch (activeStep) {
      default:
        return Image(
          width: size.width / 3,
          height: size.width / 3,
          fit: BoxFit.scaleDown,
          image: AssetImage(imageHeader()),
        );
    }
  }

  String imageHeader() {
    switch (activeStep) {
      case 0:
        return 'assets/images/resolviendo-problemas-icon.png';

      default:
        return 'assets/images/resolviendo-problemas-icon.png';
    }
  }

  String headerText() {
    switch (activeStep) {
      case 0:
        return '¿Cómo estás resolviendo tus problemas?';

      default:
        return '¿Cómo estás resolviendo tus problemas?';
    }
  }

  String buttonText() {
    switch (activeStep) {
      case 1:
        return 'Finalizar';

      default:
        return 'Siguiente ';
    }
  }

  Widget content() {
    switch (activeStep) {
      case 0:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 20, right: 20, left: 20, bottom: 30),
                    child: RichText(
                      text: const TextSpan(
                        text: '',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                'Como ya has aprendido, la orientación (positiva o negativa) que las personas tenemos al afrontar un problema influye en nuestra capacidad de resolverlo.\n\n',
                          ),
                          TextSpan(
                            text:
                                'Pero, sabías que ¿además de la orientación existen estilos para resolver problemas? Aquí te los presentamos:\n\n',
                          ),
                          TextSpan(
                            text: '1. Estilo impulsivo o descuidado\n',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Este estilo refiere a aquellas situaciones en las cuales consideramos pocas alternativas de solución, pensamos de forma rápida las posibles consecuencias y no evaluamos adecuadamente los resultados de la solución. Así nuestros intentos por solucionar los problemas se vuelven limitados, apresurados e incompletos.\n\n',
                          ),
                          TextSpan(
                            text: '2. Estilo evitativo\n',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Este estilo refiere a las situaciones en las cuales preferimos evitar los problemas en lugar de enfrentarlos, posponiendo la resolución por tiempo indefinido esperando que estos se solucionen solos o intentando que otros resuelvan los problemas por nosotros. De esta forma este estilo se caracteriza por la procrastinación, pasividad y dependencia a otros.\n\n',
                          ),
                          TextSpan(
                            text:
                                '3. Estilo planificado de solución de problemas\n',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Este estilo, a diferencia de los anteriores, es constructivo en la solución de problemas ya que se caracteriza por la implementación reflexiva y sistemática de los pasos para resolver problemas de manera planificada.',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 1:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: const EdgeInsets.only(
                        top: 5, right: 20, left: 20, bottom: 30),
                    child: RichText(
                      text: const TextSpan(
                        text: '',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                'Ahora te invitamos a reflexionar sobre qué orientaciones y estilos coinciden con tus creencias, acciones pasadas y cómo esto varía de acuerdo con las situaciones. Esto te ayudará a identificar con qué recursos y estrategias cuentas para afrontar problemas estresantes.\n\n',
                          ),
                          TextSpan(
                              text:
                                  'Para esto es importante que tengas en cuenta que:\n',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(
                              text: '   ·  ',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(
                            text:
                                'Identificar tu orientación y estilo de afrontamiento te ayudará a identificar tus fortalezas.\n',
                          ),
                          TextSpan(
                              text: '   ·  ',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(
                            text:
                                'Identificar tu orientación y estilo de afrontamiento te ayudará a identificar en que áreas necesitas trabajar.\n',
                          ),
                          TextSpan(
                              text: '   ·  ',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(
                            text:
                                'La orientación y estilo de afrontamiento puede variar de acuerdo con el tipo de problema, situación, etc.\n',
                          ),
                          TextSpan(
                              text: '   ·  ',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(
                            text:
                                'Tener dificultades para resolver un problema no significa que siempre será así, podrás aprender estrategias y habilidades que te permitirán sobrellevar las dificultades.\n',
                          ),
                          TextSpan(
                              text: '   ·  ',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(
                            text:
                                'La orientación y el estilo no es un rasgo de personalidad, si no que son tendencias y patrones que pueden ser modificados y mejorados.\n\n',
                          ),
                          TextSpan(
                            text:
                                'Identificar tu orientación y estilo de afrontamiento es una ayuda para el desarrollo de habilidades efectivas de resolución de problemas y para mejorar tu bienestar emocional. Recuerda que la orientación y el estilo no son rasgos fijos de personalidad, sino tendencias y patrones que se pueden modificar y ajustar a los contextos.',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );

      default:
        return Container();
    }
  }
}
