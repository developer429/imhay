// ignore_for_file: prefer_typing_uninitialized_variables, avoid_print

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:seguimiento/colors.dart';
import '../../firebase.dart';
import '../MainPage.dart';

class M3A5 extends StatefulWidget {
  const M3A5({
    Key? key,
  }) : super(key: key);

  @override
  State<M3A5> createState() => _M3A5State();
}

class _M3A5State extends State<M3A5> {
  String uid = FirebaseAuth.instance.currentUser!.uid;
  bool loadingData = true;
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('page-control');
  String activeStepData = '';
  void getActivePageStored() {
    collectionReference.doc(uid).get().then((value) {
      var fields = value;
      activeStepData = fields['M3A5'];
      setState(() {
        if (int.parse(activeStepData) < 7) {
          activeStep = int.parse(activeStepData) - 1;
          Future.delayed(const Duration(milliseconds: 500), () {
            setState(() {
              loadingData = false;
            });
          });
        }
      });
    });
  }

  CollectionReference dataReference =
      FirebaseFirestore.instance.collection('M3A5-respuestas');
  void getData() {
    dataReference.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        cuando.text = fields['cuando'];
        descripcionProblema.text = fields['descripcionProblema'];
        donde.text = fields['donde'];
        involucrados.text = fields['involucrados'];
        meta1.text = fields['meta1'];
        meta2.text = fields['meta2'];
        meta3.text = fields['meta3'];
        obstaculo1.text = fields['obstaculo1'];
        obstaculo2.text = fields['obstaculo2'];
        obstaculo3.text = fields['obstaculo3'];
        pensamientos.text = fields['pensamientos'];
        porque.text = fields['porque'];
        que.text = fields['que'];
        respuesta.text = fields['respuestas'];
        sentimientos.text = fields['sentimientos'];
      });
    }).catchError((err) {
      //print('Error: $err');
    });
  }

  ScrollController scrollController = ScrollController();

  bool favorite = false;
  CollectionReference favoriteData =
      FirebaseFirestore.instance.collection('favoritos');
  void getFavData() {
    favoriteData.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        favorite = fields['M3A5'];
      });
    }).catchError((err) {
      //print('Error: $err');
    });
  }

  int activeStep = 0;
  final involucrados = TextEditingController();
  final que = TextEditingController();
  final donde = TextEditingController();

  final cuando = TextEditingController();
  final porque = TextEditingController();
  final sentimientos = TextEditingController();

  final pensamientos = TextEditingController();
  final respuesta = TextEditingController();

  final descripcionProblema = TextEditingController();

  final meta1 = TextEditingController();
  final meta2 = TextEditingController();
  final meta3 = TextEditingController();

  final obstaculo1 = TextEditingController();
  final obstaculo2 = TextEditingController();
  final obstaculo3 = TextEditingController();
  final _form1Key = GlobalKey<FormState>();
  final _form2Key = GlobalKey<FormState>();
  final _form3Key = GlobalKey<FormState>();
  final _form4Key = GlobalKey<FormState>();

  var timeStart;
  @override
  void initState() {
    super.initState();
    getActivePageStored();
    getData();

    getFavData();

    var tiempo = DateTime.now();
    timeStart = tiempo;
    FirestoreService().setTime(tiempo.toString() + ' - START - M3A5',
        'Comenzó M3A5', '0 minutos (comenzando)');
  }

  @override
  void dispose() {
    super.dispose();
  }

  bool active1 = false;
  bool active2 = false;
  bool active3 = false;
  bool active4 = false;
  bool active5 = false;
  bool active6 = false;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        active1 = true;
        active2 = false;
        active3 = false;
        active4 = false;
        active5 = false;
        active6 = false;

        break;
      case 1:
        active1 = true;
        active2 = true;
        active3 = false;
        active4 = false;
        active5 = false;
        active6 = false;

        break;
      case 2:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = false;
        active5 = false;
        active6 = false;

        break;
      case 3:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = false;
        active6 = false;

        break;
      case 4:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = true;
        active6 = false;

        break;
      case 5:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = true;
        active6 = true;

        break;
      default:
    }
    return PopScope(
      canPop: false,
      onPopInvokedWithResult: (bool didPop, result) {
        if (didPop) {
          return;
        }
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(32.0))),
            title: const Text(
              '¿Estás seguro que deseas salir de la actividad?',
              textScaler: TextScaler.linear(1.0),
              style: TextStyle(color: CustomColors.primaryDark),
            ),
            content: const Text(
              'Tus avances quedarán guardados',
              textScaler: TextScaler.linear(1.0),
              style: TextStyle(color: CustomColors.primaryDark),
            ),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: const Text(
                  'Continuar con la actividad',
                  textScaler: TextScaler.linear(1.0),
                  style: TextStyle(color: CustomColors.primary),
                ),
              ),
              TextButton(
                onPressed: () {
                  FirestoreService()
                      .updateCurrentPage('M3A5', (activeStep + 1).toString());
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute<void>(
                          builder: (BuildContext context) => const MainPage(
                                activePage: 0,
                              )),
                      (route) => false);
                },
                child: const Text(
                  'Salir',
                  textScaler: TextScaler.linear(1.0),
                  style: TextStyle(color: CustomColors.error),
                ),
              ),
            ],
          ),
        );
      },
      child: loadingData
          ? Container(
              width: size.width,
              height: size.height,
              color: CustomColors.primaryDark,
              child: const Center(
                  child: CircularProgressIndicator(
                color: CustomColors.tertiary70,
              )))
          : Container(
              color: Colors.black,
              child: SafeArea(
                child: Scaffold(
                  backgroundColor:
                      active6 ? CustomColors.greenEnd : CustomColors.primary3,
                  body: Center(
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              width: size.width / 6,
                              height: 5,
                              color: active1
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 6,
                              height: 5,
                              color: active2
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 6,
                              height: 5,
                              color: active3
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 6,
                              height: 5,
                              color: active4
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 6,
                              height: 5,
                              color: active5
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 6,
                              height: 5,
                              color: active6
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                          ],
                        ),
                        content(),
                        Container(
                          margin: const EdgeInsets.only(top: 10, bottom: 10),
                          child: bottomStructure(),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
    );
  }

  Widget topStructure() {
    Size size = MediaQuery.of(context).size;
    return Container(
      width: size.width,
      height: active6 ? null : size.height / 3,
      color: active6 ? CustomColors.greenList : CustomColors.primary60,
      child: Column(
        children: [
          Row(
            children: [
              active6
                  ? Container()
                  : Container(
                      alignment: Alignment.topRight,
                      child: IconButton(
                        onPressed: () {
                          showDialog(
                            context: context,
                            builder: (context) => AlertDialog(
                              shape: const RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(32.0))),
                              title: const Text(
                                '¿Estás seguro que deseas salir de la actividad?',
                                textScaler: TextScaler.linear(1.0),
                                style:
                                    TextStyle(color: CustomColors.primaryDark),
                              ),
                              content: const Text(
                                'Tus avances quedarán guardados',
                                textScaler: TextScaler.linear(1.0),
                                style:
                                    TextStyle(color: CustomColors.primaryDark),
                              ),
                              actions: [
                                TextButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                  child: const Text(
                                    'Continuar con la actividad',
                                    textScaler: TextScaler.linear(1.0),
                                    style:
                                        TextStyle(color: CustomColors.primary),
                                  ),
                                ),
                                TextButton(
                                  onPressed: () {
                                    String tiempo = DateTime.now().toString();
                                    FirestoreService().setTime(
                                        tiempo + ' - CLOSED - M3A5',
                                        'Salió de M3A5 sin terminar',
                                        '0 minutos (sin terminar)');
                                    FirestoreService().updateCurrentPage(
                                        'M3A5', (activeStep + 1).toString());
                                    Navigator.pushAndRemoveUntil(
                                        context,
                                        MaterialPageRoute<void>(
                                            builder: (BuildContext context) =>
                                                const MainPage(
                                                  activePage: 0,
                                                )),
                                        (route) => false);
                                  },
                                  child: const Text(
                                    'Salir',
                                    textScaler: TextScaler.linear(1.0),
                                    style: TextStyle(color: CustomColors.error),
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                        icon: const Icon(
                          Icons.close,
                          color: CustomColors.primaryDark,
                        ),
                      ),
                    ),
              const Spacer(),
              active6 ? Container() : favorito(),
            ],
          ),
          Text(
            headerText(),
            textScaler: const TextScaler.linear(1.0),
            textAlign: TextAlign.center,
            style: const TextStyle(
              color: CustomColors.primaryDark,
              fontSize: 24,
            ),
          ),
          active6 ? Container() : const Spacer(),
          active6
              ? header()
              : Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    headerTitleText(),
                    header(),
                  ],
                ),
        ],
      ),
    );
  }

  Widget favorito() {
    return StreamBuilder<QuerySnapshot>(
        stream: FirestoreService().favoritos(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasData) {
            return SizedBox(
              width: 50,
              height: 50,
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (context, index) {
                    var document = snapshot.data?.docs[index];
                    if (document != null && document.id == uid) {
                      switch (document['M3A5']) {
                        case true:
                          return IconButton(
                              icon: Icon(MdiIcons.star,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('M3A5', false)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Eliminado de favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                        case false:
                          return IconButton(
                              icon: Icon(MdiIcons.starOutline,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('M3A5', true)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Agregado a favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                      }
                    } else {
                      return Container();
                    }
                    return null;
                  }),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(
                color: Colors.red,
              ),
            );
          }
        });
  }

  Widget bottomStructure() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        nextButton(),
      ],
    );
  }

  Widget nextButton() {
    Size size = MediaQuery.of(context).size;
    String empty = '-';
    if (activeStep >= 1 && activeStep < 5) {
      return SizedBox(
        width: size.width / 1,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        WidgetStateProperty.all<Color>(CustomColors.primary3),
                    shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: const BorderSide(color: Colors.white)),
                    ),
                  ),
                  onPressed: () {
                    setState(() {
                      activeStep = activeStep - 1;
                    });
                  },
                  child: const Text(
                    'Anterior',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(color: Colors.white),
                  )),
            ),
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: WidgetStateProperty.all<Color>(active6
                      ? CustomColors.primary
                      : CustomColors.greenButton),
                  shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ),
                  ),
                ),
                onPressed: () {
                  if (activeStep < 5) {
                    if (activeStep == 0) {
                      setState(() {
                        activeStep++;
                      });
                    }
                    if (activeStep == 1) {
                      if (_form1Key.currentState!.validate()) {
                        FirestoreService()
                            .guardarRespuestasM3A5(
                              involucrados.text.trim(),
                              que.text.trim(),
                              donde.text.trim(),
                              cuando.text.trim(),
                              porque.text.trim(),
                              sentimientos.text.trim(),
                              pensamientos.text.trim(),
                              respuesta.text.trim(),
                            )
                            .then(
                              (value) => setState(() {
                                activeStep++;
                              }),
                            );
                      } else {
                        showDialog<String>(
                          context: context,
                          builder: (BuildContext context) => AlertDialog(
                            title: const Text(
                              'Error',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  color: Colors.red,
                                  fontWeight: FontWeight.bold),
                            ),
                            content: const Text(
                              'Complete los campos faltantes',
                              textScaler: TextScaler.linear(1.0),
                            ),
                            actions: <Widget>[
                              TextButton(
                                onPressed: () => Navigator.pop(
                                  context,
                                ),
                                child: const Text(
                                  'OK',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      color: CustomColors.primaryDark,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                        );
                      }
                    }
                    if (activeStep == 2) {
                      if (_form2Key.currentState!.validate()) {
                        FirestoreService()
                            .updateRespuestasM3A5('descripcionProblema',
                                descripcionProblema.text.trim())
                            .then(
                              (value) => setState(() {
                                activeStep++;
                              }),
                            );
                      } else {
                        showDialog<String>(
                          context: context,
                          builder: (BuildContext context) => AlertDialog(
                            title: const Text(
                              'Error',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  color: Colors.red,
                                  fontWeight: FontWeight.bold),
                            ),
                            content: const Text(
                              'Complete los campos faltantes',
                              textScaler: TextScaler.linear(1.0),
                            ),
                            actions: <Widget>[
                              TextButton(
                                onPressed: () => Navigator.pop(
                                  context,
                                ),
                                child: const Text(
                                  'OK',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      color: CustomColors.primaryDark,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                        );
                      }
                    }
                    if (activeStep == 3) {
                      if (_form3Key.currentState!.validate()) {
                        FirestoreService()
                            .updateRespuestasM3A5('meta1', meta1.text.trim());
                        FirestoreService()
                            .updateRespuestasM3A5('meta2', meta2.text.trim());
                        FirestoreService()
                            .updateRespuestasM3A5('meta3',
                                meta3.text.isEmpty ? empty : meta3.text.trim())
                            .then(
                              (value) => setState(() {
                                activeStep++;
                              }),
                            );
                      } else {
                        showDialog<String>(
                          context: context,
                          builder: (BuildContext context) => AlertDialog(
                            title: const Text(
                              'Error',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  color: Colors.red,
                                  fontWeight: FontWeight.bold),
                            ),
                            content: const Text(
                              'Complete los campos faltantes',
                              textScaler: TextScaler.linear(1.0),
                            ),
                            actions: <Widget>[
                              TextButton(
                                onPressed: () => Navigator.pop(
                                  context,
                                ),
                                child: const Text(
                                  'OK',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      color: CustomColors.primaryDark,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                        );
                      }
                    }
                    if (activeStep == 4) {
                      if (_form4Key.currentState!.validate()) {
                        FirestoreService().updateRespuestasM3A5(
                            'obstaculo1', obstaculo1.text.trim());
                        FirestoreService().updateRespuestasM3A5(
                            'obstaculo2', obstaculo2.text.trim());
                        FirestoreService()
                            .updateRespuestasM3A5(
                                'obstaculo3',
                                obstaculo3.text.isEmpty
                                    ? empty
                                    : obstaculo3.text.trim())
                            .then(
                              (value) => setState(() {
                                activeStep++;
                              }),
                            );
                      } else {
                        showDialog<String>(
                          context: context,
                          builder: (BuildContext context) => AlertDialog(
                            title: const Text(
                              'Error',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  color: Colors.red,
                                  fontWeight: FontWeight.bold),
                            ),
                            content: const Text(
                              'Complete los campos faltantes',
                              textScaler: TextScaler.linear(1.0),
                            ),
                            actions: <Widget>[
                              TextButton(
                                onPressed: () => Navigator.pop(
                                  context,
                                ),
                                child: const Text(
                                  'OK',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      color: CustomColors.primaryDark,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                        );
                      }
                    }
                  } else {
                    FirestoreService().terminaSesion('3', '5',
                        'Leyó la introducción → Buscó los hechos → Describió su problema → Definió sus metas → Identificó los obstáculos → Terminó la actividad 18');
                    var tiempo = DateTime.now();
                    var difTiempo = tiempo.difference(timeStart);
                    FirestoreService().setTime(
                        tiempo.toString() + ' - ENDED - M3A5',
                        'Terminó M3A5',
                        difTiempo.inMinutes.toString() + ' minutos');
                    FirestoreService().updateCurrentPage('M3A5', '1');
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute<void>(
                            builder: (BuildContext context) => const MainPage(
                                  activePage: 0,
                                )),
                        (route) => false);
                  }
                },
                child: Text(
                  buttonText(),
                  textScaler: const TextScaler.linear(1.0),
                  style: const TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      return SizedBox(
        width: size.width / 1.2,
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor: WidgetStateProperty.all<Color>(
                active6 ? CustomColors.primary : CustomColors.greenButton),
            shape: WidgetStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              ),
            ),
          ),
          onPressed: () {
            if (activeStep < 5) {
              if (activeStep == 0) {
                setState(() {
                  activeStep++;
                });
              }
              if (activeStep == 1) {
                try {
                  if (_form1Key.currentState!.validate()) {
                    FirestoreService()
                        .guardarRespuestasM3A5(
                          involucrados.text.trim(),
                          que.text.trim(),
                          donde.text.trim(),
                          cuando.text.trim(),
                          porque.text.trim(),
                          sentimientos.text.trim(),
                          pensamientos.text.trim(),
                          respuesta.text.trim(),
                        )
                        .then(
                          (value) => setState(() {
                            activeStep++;
                          }),
                        );
                  } else {
                    showDialog<String>(
                      context: context,
                      builder: (BuildContext context) => AlertDialog(
                        title: const Text(
                          'Error',
                          textScaler: TextScaler.linear(1.0),
                          style: TextStyle(
                              color: Colors.red, fontWeight: FontWeight.bold),
                        ),
                        content: const Text(
                          'Complete los campos faltantes',
                          textScaler: TextScaler.linear(1.0),
                        ),
                        actions: <Widget>[
                          TextButton(
                            onPressed: () => Navigator.pop(
                              context,
                            ),
                            child: const Text(
                              'OK',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  color: CustomColors.primaryDark,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                    );
                  }
                } catch (e) {
                  print(e);
                }
              }
              if (activeStep == 2) {
                if (_form2Key.currentState!.validate()) {
                  FirestoreService()
                      .updateRespuestasM3A5('descripcionProblema',
                          descripcionProblema.text.trim())
                      .then(
                        (value) => setState(() {
                          activeStep++;
                        }),
                      );
                } else {
                  showDialog<String>(
                    context: context,
                    builder: (BuildContext context) => AlertDialog(
                      title: const Text(
                        'Error',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            color: Colors.red, fontWeight: FontWeight.bold),
                      ),
                      content: const Text(
                        'Complete los campos faltantes',
                        textScaler: TextScaler.linear(1.0),
                      ),
                      actions: <Widget>[
                        TextButton(
                          onPressed: () => Navigator.pop(
                            context,
                          ),
                          child: const Text(
                            'OK',
                            textScaler: TextScaler.linear(1.0),
                            style: TextStyle(
                                color: CustomColors.primaryDark,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                  );
                }
              }
              if (activeStep == 3) {
                if (_form3Key.currentState!.validate()) {
                  FirestoreService()
                      .updateRespuestasM3A5('meta1', meta1.text.trim());
                  FirestoreService()
                      .updateRespuestasM3A5('meta2', meta2.text.trim());
                  FirestoreService()
                      .updateRespuestasM3A5('meta3',
                          meta3.text.isEmpty ? empty : meta3.text.trim())
                      .then(
                        (value) => setState(() {
                          activeStep++;
                        }),
                      );
                } else {
                  showDialog<String>(
                    context: context,
                    builder: (BuildContext context) => AlertDialog(
                      title: const Text(
                        'Error',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            color: Colors.red, fontWeight: FontWeight.bold),
                      ),
                      content: const Text(
                        'Complete los campos faltantes',
                        textScaler: TextScaler.linear(1.0),
                      ),
                      actions: <Widget>[
                        TextButton(
                          onPressed: () => Navigator.pop(
                            context,
                          ),
                          child: const Text(
                            'OK',
                            textScaler: TextScaler.linear(1.0),
                            style: TextStyle(
                                color: CustomColors.primaryDark,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                  );
                }
              }
              if (activeStep == 4) {
                if (_form4Key.currentState!.validate()) {
                  FirestoreService().updateRespuestasM3A5(
                      'obstaculo1', obstaculo1.text.trim());
                  FirestoreService().updateRespuestasM3A5(
                      'obstaculo2', obstaculo2.text.trim());
                  FirestoreService()
                      .updateRespuestasM3A5(
                          'obstaculo3',
                          obstaculo3.text.isEmpty
                              ? empty
                              : obstaculo3.text.trim())
                      .then(
                        (value) => setState(() {
                          activeStep++;
                        }),
                      );
                } else {
                  showDialog<String>(
                    context: context,
                    builder: (BuildContext context) => AlertDialog(
                      title: const Text(
                        'Error',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            color: Colors.red, fontWeight: FontWeight.bold),
                      ),
                      content: const Text(
                        'Complete los campos faltantes',
                        textScaler: TextScaler.linear(1.0),
                      ),
                      actions: <Widget>[
                        TextButton(
                          onPressed: () => Navigator.pop(
                            context,
                          ),
                          child: const Text(
                            'OK',
                            textScaler: TextScaler.linear(1.0),
                            style: TextStyle(
                                color: CustomColors.primaryDark,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                  );
                }
              }
            } else {
              FirestoreService().terminaSesion('3', '5',
                  'Leyó la introducción → Buscó los hechos → Describió su problema → Definió sus metas → Identificó los obstáculos → Terminó la actividad 18');
              var tiempo = DateTime.now();
              var difTiempo = tiempo.difference(timeStart);
              FirestoreService().setTime(tiempo.toString() + ' - ENDED - M3A5',
                  'Terminó M3A5', difTiempo.inMinutes.toString() + ' minutos');
              FirestoreService().updateCurrentPage('M3A5', '1');
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute<void>(
                      builder: (BuildContext context) => const MainPage(
                            activePage: 0,
                          )),
                  (route) => false);
            }
          },
          child: Text(
            buttonText(),
            textScaler: const TextScaler.linear(1.0),
            style: TextStyle(
              color: active6 ? CustomColors.surface : CustomColors.primaryDark,
            ),
          ),
        ),
      );
    }
  }

  Widget header() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 5:
        return Image(
          width: size.width / 8,
          height: size.width / 8,
          fit: BoxFit.scaleDown,
          image: AssetImage(imageHeader()),
        );
      default:
        return Image(
          width: kIsWeb ? 100 : size.width / 3,
          height: kIsWeb ? 100 : size.width / 3,
          fit: BoxFit.scaleDown,
          image: AssetImage(imageHeader()),
        );
    }
  }

  Widget headerTitleText() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        return Container();

      case 5:
        return Container();
      default:
        return SizedBox(
          width: size.width / 2,
          child: Text(
            tituloHeader(),
            textScaler: const TextScaler.linear(1.0),
            style: const TextStyle(
                fontSize: 18,
                color: CustomColors.primaryDark,
                fontWeight: FontWeight.bold),
          ),
        );
    }
  }

  String tituloHeader() {
    switch (activeStep) {
      case 1:
        return 'Paso 1:\nBusca los "hechos"';
      case 2:
        return 'Paso 2:\nReflexiona sobre tus respuestas';
      case 3:
        return 'Paso 3:\nDefine tus metas';
      case 4:
        return 'Paso 4:\nIdentifica los obstáculos';
      case 5:
        return '';

      default:
        return '';
    }
  }

  String imageHeader() {
    switch (activeStep) {
      case 0:
        return 'assets/images/m3a5.png';

      case 5:
        return 'assets/images/end.png';

      default:
        return 'assets/images/m3a5.png';
    }
  }

  String headerText() {
    switch (activeStep) {
      case 5:
        return '';
      default:
        return 'Definiendo el problema';
    }
  }

  String buttonText() {
    switch (activeStep) {
      case 0:
        return 'Comenzar';
      case 4:
        return 'Finalizar';
      case 5:
        return 'Continuar con el programa';
      default:
        return 'Siguiente';
    }
  }

  Widget content() {
    switch (activeStep) {
      case 0:
        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.all(20),
                    child: RichText(
                      text: const TextSpan(
                        text: '',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                'Ver el problema nos puede ayudar a encontrar su solución, sin embargo, y aunque pueda sonar sorprendente, definir el problema puede ser la parte más difícil en la resolución de problemas, y sin una adecuada definición del problema este puede ser más difícil de resolver.\n\nEn esta actividad te ayudaremos a definir un problema por el que estés pasando en la actualidad para luego poder generar alternativas de solución a este.',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );

      case 1:
        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.all(20),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'En primer lugar, es importante buscar los “hechos” que se encuentran disponibles. Muchas veces podemos intentar resolver un problema sin contar con toda la información posible, y al no saber realmente cual es el problema podemos encontrarnos en situaciones en las cuales no estemos trabajando en el problema adecuado. Por esto es importante que busquemos la información que no necesariamente es evidente. Aquí te dejamos algunas preguntas que puedes responder para buscar dicha información:',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.all(10),
                    child: Form(
                      key: _form1Key,
                      child: Column(
                        children: [
                          Container(
                            margin: const EdgeInsets.only(
                                left: 10, right: 10, bottom: 0, top: 10),
                            alignment: Alignment.centerLeft,
                            child: const Text(
                              '¿Quién(es) están involucrados?',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 18, color: CustomColors.surface),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              validator: (String? value) {
                                return (value == null || value == '')
                                    ? 'Campo vacío.'
                                    : null;
                              },
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: involucrados,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: 'Escribe tu respuesta...',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(
                                left: 10, right: 10, bottom: 0, top: 10),
                            alignment: Alignment.centerLeft,
                            child: const Text(
                              '¿Qué fue lo que pasó (o qué no pasó) que te generó malestar?',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 18, color: CustomColors.surface),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              validator: (String? value) {
                                return (value == null || value == '')
                                    ? 'Campo vacío.'
                                    : null;
                              },
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: que,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: 'Escribe tu respuesta...',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(
                                left: 10, right: 10, bottom: 0, top: 10),
                            alignment: Alignment.centerLeft,
                            child: const Text(
                              '¿Dónde pasó?',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 18, color: CustomColors.surface),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              validator: (String? value) {
                                return (value == null || value == '')
                                    ? 'Campo vacío.'
                                    : null;
                              },
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: donde,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: 'Escribe tu respuesta...',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(
                                left: 10, right: 10, bottom: 0, top: 10),
                            alignment: Alignment.centerLeft,
                            child: const Text(
                              '¿Cuándo pasó?',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 18, color: CustomColors.surface),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              validator: (String? value) {
                                return (value == null || value == '')
                                    ? 'Campo vacío.'
                                    : null;
                              },
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: cuando,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: 'Escribe tu respuesta...',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(
                                left: 10, right: 10, bottom: 0, top: 10),
                            alignment: Alignment.centerLeft,
                            child: const Text(
                              '¿Por qué sucedió?',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 18, color: CustomColors.surface),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              validator: (String? value) {
                                return (value == null || value == '')
                                    ? 'Campo vacío.'
                                    : null;
                              },
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: porque,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: 'Escribe tu respuesta...',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(
                                left: 10, right: 10, bottom: 0, top: 10),
                            alignment: Alignment.centerLeft,
                            child: const Text(
                              '¿Cómo te sentiste con la situación?',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 18, color: CustomColors.surface),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              validator: (String? value) {
                                return (value == null || value == '')
                                    ? 'Campo vacío.'
                                    : null;
                              },
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: sentimientos,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: 'Escribe tu respuesta...',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(
                                left: 10, right: 10, bottom: 0, top: 10),
                            alignment: Alignment.centerLeft,
                            child: const Text(
                              '¿Qué pasaba por tu mente en ese momento?',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 18, color: CustomColors.surface),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              validator: (String? value) {
                                return (value == null || value == '')
                                    ? 'Campo vacío.'
                                    : null;
                              },
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: pensamientos,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: 'Escribe tu respuesta...',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(
                                left: 10, right: 10, bottom: 0, top: 10),
                            alignment: Alignment.centerLeft,
                            child: const Text(
                              '¿Cómo respondiste a la situación? (que fue lo que hiciste)',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 18, color: CustomColors.surface),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              validator: (String? value) {
                                return (value == null || value == '')
                                    ? 'Campo vacío.'
                                    : null;
                              },
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: respuesta,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: 'Escribe tu respuesta...',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 2:
        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  InkWell(
                    onTap: () {
                      showDialog<String>(
                        context: context,
                        builder: (BuildContext context) => AlertDialog(
                          shape: const RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(32.0))),
                          title: const Text(
                            'Errores del pensamiento\n',
                            textScaler: TextScaler.linear(1.0),
                            style: TextStyle(
                                fontSize: 20, color: CustomColors.primaryDark),
                          ),
                          content: RichText(
                            text: const TextSpan(
                              text:
                                  'Recordemos que los errores del pensamiento son aquellos que:\n',
                              style: TextStyle(
                                fontSize: 16,
                                color: CustomColors.primaryDark,
                                height: 1.5,
                              ),
                              children: <TextSpan>[
                                TextSpan(
                                  text: '· ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                TextSpan(text: ' Distorsionan la realidad.\n'),
                                TextSpan(
                                  text: '· ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                TextSpan(
                                    text:
                                        ' Nos hacen sentir mal (ej. Angustiadxs, bajoneadxs o enojadxs) de forma innecesaria.\n'),
                                TextSpan(
                                  text: '· ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                TextSpan(
                                    text:
                                        ' Nos conducen a conductas infructuosas o dañinas.\n'),
                                TextSpan(
                                  text: '· ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                TextSpan(
                                    text:
                                        ' Nos impiden hacer nuestras actividades cotidianas y/o avanzar hacia nuestras metas.'),
                              ],
                            ),
                          ),
                          actions: <Widget>[
                            TextButton(
                              onPressed: () => Navigator.pop(context, 'Cerrar'),
                              child: const Text(
                                'Cerrar',
                                textScaler: TextScaler.linear(1.0),
                                style: TextStyle(color: CustomColors.primary),
                              ),
                            ),
                          ],
                        ),
                      );
                    },
                    child: Container(
                      margin: const EdgeInsets.all(20),
                      child: RichText(
                        text: const TextSpan(
                          text:
                              'Después de responder las preguntas, tómate un momento para pensar en tus respuestas y describir el problema con claridad. Esto es importante porque cuando estás estresado/a, puedes usar un lenguaje emocionalmente cargado y poco claro, lo que puede llevar a exageraciones o malentendidos. Asegúrate de separar los hechos de las suposiciones, ya que estas últimas pueden ser consideradas verdades antes de que se compruebe su veracidad (¿recuerdas los',
                          style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.surface,
                            height: 1.5,
                          ),
                          children: [
                            TextSpan(
                              text: ' errores del pensamiento',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                decoration: TextDecoration.underline,
                              ),
                            ),
                            TextSpan(
                              text:
                                  '?). Si actuamos en función de suposiciones, será más difícil comprender el problema y resolverlo con éxito.\n\n',
                            ),
                            TextSpan(
                              text:
                                  'Luego de este tiempo de reflexión te dejamos un espacio para que puedas describir el problema que estás teniendo en la actualidad de la forma más breve, precisa y clara que puedas:',
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.all(10),
                    child: Form(
                      key: _form2Key,
                      child: Column(
                        children: [
                          Container(
                            margin: const EdgeInsets.only(
                                left: 10, right: 10, bottom: 0, top: 10),
                            alignment: Alignment.centerLeft,
                            child: const Text(
                              'Descripción del problema (escribe tu respuesta para continuar con la actividad):',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 18, color: CustomColors.surface),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              validator: (String? value) {
                                return (value == null || value == '')
                                    ? 'Campo vacío.'
                                    : null;
                              },
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: descripcionProblema,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: 'Escribe tu respuesta...\n\n\n\n\n',
                                  hintMaxLines: 5,
                                  border: OutlineInputBorder()),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 3:
        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.all(20),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Ahora que cuentas con una descripción clara del problema lo siguiente es poner metas realistas y alcanzables de solución. Si el problema es muy grande o la meta de solución es poco realista es poco probable que logres lo que esperas y eso puede generarte mayor malestar. De esta forma, si el problema que estás enfrentando te parece demasiado grande puedes dividirlo (y simplificarlo) en partes más pequeñas y manejables que te puedan acercar al resultado final deseado. Las metas que te pongas en este punto pueden ser tanto para cambiar la situación en sí para que esta ya no sea un problema y/o pueden enfocarse en cambiar tu forma de reaccionar frente a la situación.\n\n',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                'Deja tus metas aquí (escribir al menos dos metas para poder continuar):',
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.all(10),
                    child: Form(
                      key: _form3Key,
                      child: Column(
                        children: [
                          Container(
                            margin: const EdgeInsets.only(
                                left: 10, right: 10, bottom: 0, top: 10),
                            alignment: Alignment.centerLeft,
                            child: const Text(
                              'Meta 1',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 18, color: CustomColors.surface),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              validator: (String? value) {
                                return (value == null || value == '')
                                    ? 'Campo vacío.'
                                    : null;
                              },
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: meta1,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: 'Escribe tu respuesta...',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(
                                left: 10, right: 10, bottom: 0, top: 10),
                            alignment: Alignment.centerLeft,
                            child: const Text(
                              'Meta 2',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 18, color: CustomColors.surface),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              validator: (String? value) {
                                return (value == null || value == '')
                                    ? 'Campo vacío.'
                                    : null;
                              },
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: meta2,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: 'Escribe tu respuesta...',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(
                                left: 10, right: 10, bottom: 0, top: 10),
                            alignment: Alignment.centerLeft,
                            child: const Text(
                              'Meta 3',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 18, color: CustomColors.surface),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: meta3,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: 'Escribe tu respuesta...',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 4:
        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.all(20),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Ahora que definiste tus metas, el siguiente paso es identificar los obstáculos que pueden estar impidiendo que alcances dichas metas en la actualidad. Identificar estos obstáculos es de gran ayuda para poder generar mejores alternativas de solución. Para esto puedes responder la pregunta de “¿Qué hace que esta situación sea un problema?”. La respuesta a esta pregunta puede variar ampliamente donde, por ejemplo una situación puede ser un problema por falta de recursos (materiales, habilidades, etc.), porque la situación es desconocida o poco familiar, porque la situación es muy compleja, porque nuestra reacción emocional a la situación es difícil de afrontar, etc. Toma un momento para pensar en esta pregunta y escribe los obstáculos que identificas a continuación (escribe al menos 2 obstáculos para poder finalizar la actividad):',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.all(10),
                    child: Form(
                      key: _form4Key,
                      child: Column(
                        children: [
                          Container(
                            margin: const EdgeInsets.only(
                                left: 10, right: 10, bottom: 0, top: 10),
                            alignment: Alignment.centerLeft,
                            child: const Text(
                              'Obstáculo 1',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 18, color: CustomColors.surface),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              validator: (String? value) {
                                return (value == null || value == '')
                                    ? 'Campo vacío.'
                                    : null;
                              },
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: obstaculo1,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: 'Escribe tu respuesta...',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(
                                left: 10, right: 10, bottom: 0, top: 10),
                            alignment: Alignment.centerLeft,
                            child: const Text(
                              'Obstáculo 2',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 18, color: CustomColors.surface),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              validator: (String? value) {
                                return (value == null || value == '')
                                    ? 'Campo vacío.'
                                    : null;
                              },
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: obstaculo2,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: 'Escribe tu respuesta...',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(
                                left: 10, right: 10, bottom: 0, top: 10),
                            alignment: Alignment.centerLeft,
                            child: const Text(
                              'Obstáculo 3',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 18, color: CustomColors.surface),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: obstaculo3,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: 'Escribe tu respuesta...',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 5:
        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Container(
                margin: const EdgeInsets.all(15),
                child: Column(
                  children: [
                    topStructure(),
                    Container(
                      margin: const EdgeInsets.all(10),
                      child: const FittedBox(
                        fit: BoxFit.fitWidth,
                        child: Text(
                          '¡Muy bien!',
                          textScaler: TextScaler.linear(1.0),
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              // color: CustomColors.primaryDark,
                              fontSize: 24),
                        ),
                      ),
                    ),
                    const Text(
                      'Tus respuetas han quedado guardadas y las podrás ver en tu perfil. Además, puedes volver a contestar las veces que quieras este ejercicio.\nTenemos una recompensa para ti:',
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(fontSize: 16),
                      textAlign: TextAlign.center,
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 40, bottom: 10),
                      decoration: BoxDecoration(
                          color: CustomColors.lightContainer,
                          border:
                              Border.all(width: 2, color: CustomColors.surface),
                          borderRadius: BorderRadius.circular(15)),
                      child: Row(
                        children: [
                          const Image(
                            width: 100,
                            height: 120,
                            fit: BoxFit.scaleDown,
                            image: AssetImage(
                                'assets/images/superando-sobrecarga-icon.png'),
                          ),
                          Container(
                            margin: const EdgeInsets.only(left: 10),
                            child: const Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Actividad complementaria:',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      fontSize: 13,
                                      color: CustomColors.primaryDark),
                                ),
                                Text(
                                  'Superando la sobrecarga\nmental',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16,
                                      color: CustomColors.primaryDark),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 10, bottom: 10),
                      decoration: BoxDecoration(
                          color: CustomColors.lightContainer,
                          border:
                              Border.all(width: 2, color: CustomColors.surface),
                          borderRadius: BorderRadius.circular(15)),
                      child: Row(
                        children: [
                          const Image(
                            width: 100,
                            height: 120,
                            fit: BoxFit.scaleDown,
                            image: AssetImage('assets/images/m3a5.png'),
                          ),
                          Container(
                            margin: const EdgeInsets.only(left: 10),
                            child: const Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Trabajo por mi bienestar:',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      fontSize: 13,
                                      color: CustomColors.primaryDark),
                                ),
                                Text(
                                  'Defino bien mis problemas',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16,
                                      color: CustomColors.primaryDark),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );

      default:
        return Container();
    }
  }
}
