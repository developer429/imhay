// ignore_for_file: prefer_typing_uninitialized_variables

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:seguimiento/colors.dart';
import '../../firebase.dart';
import '../MainPage.dart';

class M3A3 extends StatefulWidget {
  const M3A3({
    Key? key,
  }) : super(key: key);

  @override
  State<M3A3> createState() => _M3A3State();
}

class _M3A3State extends State<M3A3> {
  String uid = FirebaseAuth.instance.currentUser!.uid;
  bool loadingData = true;
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('page-control');
  String activeStepData = '';
  void getActivePageStored() {
    collectionReference.doc(uid).get().then((value) {
      var fields = value;
      activeStepData = fields['M3A3'];
      setState(() {
        if (int.parse(activeStepData) < 9) {
          activeStep = int.parse(activeStepData) - 1;
          Future.delayed(const Duration(milliseconds: 500), () {
            setState(() {
              loadingData = false;
            });
          });
        }
      });
    });
  }

  ScrollController scrollController = ScrollController();

  bool favorite = false;
  CollectionReference favoriteData =
      FirebaseFirestore.instance.collection('favoritos');
  void getFavData() {
    favoriteData.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        favorite = fields['M3A3'];
      });
    }).catchError((err) {
      //print('Error: $err');
    });
  }

  int activeStep = 0;
  var timeStart;
  @override
  void initState() {
    super.initState();
    getActivePageStored();

    getFavData();
    var tiempo = DateTime.now();
    timeStart = tiempo;
    FirestoreService().setTime(tiempo.toString() + ' - START - M3A3',
        'Comenzó M3A3', '0 minutos (comenzando)');
  }

  @override
  void dispose() {
    super.dispose();
  }

  bool active1 = false;
  bool active2 = false;
  bool active3 = false;
  bool active4 = false;
  bool active5 = false;
  bool active6 = false;
  bool active7 = false;
  bool active8 = false;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        active1 = true;
        active2 = false;
        active3 = false;
        active4 = false;
        active5 = false;
        active6 = false;
        active7 = false;
        active8 = false;
        break;
      case 1:
        active1 = true;
        active2 = true;
        active3 = false;
        active4 = false;
        active5 = false;
        active6 = false;
        active7 = false;
        active8 = false;

        break;
      case 2:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = false;
        active5 = false;
        active6 = false;
        active7 = false;
        active8 = false;

        break;
      case 3:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = false;
        active6 = false;
        active7 = false;
        active8 = false;

        break;
      case 4:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = true;
        active6 = false;
        active7 = false;
        active8 = false;

        break;
      case 5:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = true;
        active6 = true;
        active7 = false;
        active8 = false;

        break;
      case 6:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = true;
        active6 = true;
        active7 = true;
        active8 = false;

        break;
      case 7:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = true;
        active6 = true;
        active7 = true;
        active8 = true;

        break;
      default:
    }
    return PopScope(
      canPop: false,
      onPopInvokedWithResult: (bool didPop, result) {
        if (didPop) {
          return;
        }
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(32.0))),
            title: const Text(
              '¿Estás seguro que deseas salir de la actividad?',
              textScaler: TextScaler.linear(1.0),
              style: TextStyle(color: CustomColors.primaryDark),
            ),
            content: const Text(
              'Tus avances quedarán guardados',
              textScaler: TextScaler.linear(1.0),
              style: TextStyle(color: CustomColors.primaryDark),
            ),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: const Text(
                  'Continuar con la actividad',
                  textScaler: TextScaler.linear(1.0),
                  style: TextStyle(color: CustomColors.primary),
                ),
              ),
              TextButton(
                onPressed: () {
                  FirestoreService()
                      .updateCurrentPage('M3A3', (activeStep + 1).toString());
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute<void>(
                          builder: (BuildContext context) => const MainPage(
                                activePage: 0,
                              )),
                      (route) => false);
                },
                child: const Text(
                  'Salir',
                  textScaler: TextScaler.linear(1.0),
                  style: TextStyle(color: CustomColors.error),
                ),
              ),
            ],
          ),
        );
      },
      child: loadingData
          ? Container(
              width: size.width,
              height: size.height,
              color: CustomColors.primaryDark,
              child: const Center(
                  child: CircularProgressIndicator(
                color: CustomColors.tertiary70,
              )))
          : Container(
              color: Colors.black,
              child: SafeArea(
                child: Scaffold(
                  backgroundColor:
                      active8 ? CustomColors.greenEnd : CustomColors.primary3,
                  body: Center(
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              width: size.width / 8,
                              height: 5,
                              color: active1
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 8,
                              height: 5,
                              color: active2
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 8,
                              height: 5,
                              color: active3
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 8,
                              height: 5,
                              color: active4
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 8,
                              height: 5,
                              color: active5
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 8,
                              height: 5,
                              color: active6
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 8,
                              height: 5,
                              color: active7
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 8,
                              height: 5,
                              color: active8
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                          ],
                        ),
                        content(),
                        Container(
                          margin: const EdgeInsets.only(top: 10, bottom: 10),
                          child: bottomStructure(),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
    );
  }

  Widget topStructure() {
    Size size = MediaQuery.of(context).size;

    return Container(
      width: size.width,
      height: active8 ? null : size.height / 3,
      color: active8 ? CustomColors.greenList : CustomColors.primary60,
      child: Column(
        children: [
          Row(
            children: [
              active8
                  ? Container()
                  : Container(
                      alignment: Alignment.topRight,
                      child: IconButton(
                        onPressed: () {
                          showDialog(
                            context: context,
                            builder: (context) => AlertDialog(
                              shape: const RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(32.0))),
                              title: const Text(
                                '¿Estás seguro que deseas salir de la actividad?',
                                textScaler: TextScaler.linear(1.0),
                                style:
                                    TextStyle(color: CustomColors.primaryDark),
                              ),
                              content: const Text(
                                'Tus avances quedarán guardados',
                                textScaler: TextScaler.linear(1.0),
                                style:
                                    TextStyle(color: CustomColors.primaryDark),
                              ),
                              actions: [
                                TextButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                  child: const Text(
                                    'Continuar con la actividad',
                                    textScaler: TextScaler.linear(1.0),
                                    style:
                                        TextStyle(color: CustomColors.primary),
                                  ),
                                ),
                                TextButton(
                                  onPressed: () {
                                    String tiempo = DateTime.now().toString();
                                    FirestoreService().setTime(
                                        tiempo + ' - CLOSED - M3A3',
                                        'Salió de M3A3 sin terminar',
                                        '0 minutos (sin terminar)');
                                    FirestoreService().updateCurrentPage(
                                        'M3A3', (activeStep + 1).toString());
                                    Navigator.pushAndRemoveUntil(
                                        context,
                                        MaterialPageRoute<void>(
                                            builder: (BuildContext context) =>
                                                const MainPage(
                                                  activePage: 0,
                                                )),
                                        (route) => false);
                                  },
                                  child: const Text(
                                    'Salir',
                                    textScaler: TextScaler.linear(1.0),
                                    style: TextStyle(color: CustomColors.error),
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                        icon: const Icon(
                          Icons.close,
                          color: CustomColors.primaryDark,
                        ),
                      ),
                    ),
              const Spacer(),
              active8 ? Container() : favorito(),
            ],
          ),
          Text(
            headerText(),
            textScaler: const TextScaler.linear(1.0),
            textAlign: TextAlign.center,
            style: const TextStyle(
              color: CustomColors.primaryDark,
              fontSize: 24,
            ),
          ),
          active8 ? Container() : const Spacer(),
          active8
              ? header()
              : Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    headerTitleText(),
                    header(),
                  ],
                ),
        ],
      ),
    );
  }

  Widget favorito() {
    return StreamBuilder<QuerySnapshot>(
        stream: FirestoreService().favoritos(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasData) {
            return SizedBox(
              width: 50,
              height: 50,
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (context, index) {
                    var document = snapshot.data?.docs[index];
                    if (document != null && document.id == uid) {
                      switch (document['M3A3']) {
                        case true:
                          return IconButton(
                              icon: Icon(MdiIcons.star,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('M3A3', false)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Eliminado de favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                        case false:
                          return IconButton(
                              icon: Icon(MdiIcons.starOutline,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('M3A3', true)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Agregado a favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                      }
                    } else {
                      return Container();
                    }
                    return null;
                  }),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(
                color: Colors.red,
              ),
            );
          }
        });
  }

  Widget bottomStructure() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        nextButton(),
      ],
    );
  }

  Widget nextButton() {
    Size size = MediaQuery.of(context).size;
    if (activeStep >= 1 && activeStep < 7) {
      return SizedBox(
        width: size.width / 1,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        WidgetStateProperty.all<Color>(CustomColors.primary3),
                    shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: const BorderSide(color: Colors.white)),
                    ),
                  ),
                  onPressed: () {
                    setState(() {
                      activeStep = activeStep - 1;
                    });
                  },
                  child: const Text(
                    'Anterior',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(color: Colors.white),
                  )),
            ),
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: WidgetStateProperty.all<Color>(active8
                      ? CustomColors.primary
                      : CustomColors.greenButton),
                  shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ),
                  ),
                ),
                onPressed: () {
                  if (activeStep < 7) {
                    setState(() {
                      activeStep++;
                    });
                  } else {
                    FirestoreService().terminaSesion('3', '3',
                        'Leyó la introducción → Aprendió sobre los 5 pasos para enfrentar pensamientos negativos → Terminó la actividad 16');
                    var tiempo = DateTime.now();
                    var difTiempo = tiempo.difference(timeStart);
                    FirestoreService().setTime(
                        tiempo.toString() + ' - ENDED - M3A3',
                        'Terminó M3A3',
                        difTiempo.inMinutes.toString() + ' minutos');
                    FirestoreService().updateCurrentPage('M3A3', '1');
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute<void>(
                            builder: (BuildContext context) => const MainPage(
                                  activePage: 0,
                                )),
                        (route) => false);
                  }
                },
                child: Text(
                  buttonText(),
                  textScaler: const TextScaler.linear(1.0),
                  style: const TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      return SizedBox(
        width: size.width / 1.2,
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor: WidgetStateProperty.all<Color>(
                active8 ? CustomColors.primary : CustomColors.greenButton),
            shape: WidgetStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              ),
            ),
          ),
          onPressed: () {
            if (activeStep < 7) {
              setState(() {
                activeStep++;
              });
            } else {
              FirestoreService().terminaSesion('3', '3',
                  'Leyó la introducción → Aprendió sobre los 5 pasos para enfrentar pensamientos negativos → Terminó la actividad 16');
              var tiempo = DateTime.now();
              var difTiempo = tiempo.difference(timeStart);
              FirestoreService().setTime(tiempo.toString() + ' - ENDED - M3A3',
                  'Terminó M3A3', difTiempo.inMinutes.toString() + ' minutos');
              FirestoreService().updateCurrentPage('M3A3', '1');
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute<void>(
                      builder: (BuildContext context) => const MainPage(
                            activePage: 0,
                          )),
                  (route) => false);
            }
          },
          child: Text(
            buttonText(),
            textScaler: const TextScaler.linear(1.0),
            style: TextStyle(
              color: active8 ? CustomColors.surface : CustomColors.primaryDark,
            ),
          ),
        ),
      );
    }
  }

  Widget header() {
    Size size = MediaQuery.of(context).size;
    switch (activeStep) {
      case 8:
        return Image(
          width: size.width / 8,
          height: size.width / 8,
          fit: BoxFit.scaleDown,
          image: AssetImage(imageHeader()),
        );
      default:
        return Image(
          width: kIsWeb ? 100 : size.width / 3,
          height: kIsWeb ? 100 : size.width / 3,
          fit: BoxFit.scaleDown,
          image: AssetImage(imageHeader()),
        );
    }
  }

  Widget headerTitleText() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        return Container();
      case 6:
        return Container();
      case 7:
        return Container();
      default:
        return Container(
          margin: const EdgeInsets.only(bottom: 20),
          width: size.width / 2,
          child: Text(
            tituloHeader(),
            textScaler: const TextScaler.linear(1.0),
            style: const TextStyle(
                fontSize: 18,
                color: CustomColors.primaryDark,
                fontWeight: FontWeight.bold),
          ),
        );
    }
  }

  String tituloHeader() {
    switch (activeStep) {
      case 1:
        return 'Paso 1: Identifica';
      case 2:
        return 'Paso 2: Sospecha';
      case 3:
        return 'Paso 3: Busca evidencia';
      case 4:
        return 'Paso 4: Concluye';
      case 5:
        return 'Paso 5: Superar el pensamiento negativo';
      default:
        return '';
    }
  }

  String imageHeader() {
    switch (activeStep) {
      case 0:
        return 'assets/images/m3a3.png';

      case 7:
        return 'assets/images/end.png';

      default:
        return 'assets/images/m3a3.png';
    }
  }

  String headerText() {
    switch (activeStep) {
      case 7:
        return '';
      default:
        return '5 pasos para enfrentar los pensamientos negativos';
    }
  }

  String buttonText() {
    switch (activeStep) {
      case 0:
        return 'Comenzar';
      case 6:
        return 'Finalizar';
      case 7:
        return 'Continuar con el programa';
      default:
        return 'Siguiente';
    }
  }

  Widget content() {
    switch (activeStep) {
      case 0:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.all(20),
                    child: RichText(
                      text: const TextSpan(
                        text: '',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                'Ahora que ya sabes cómo identificar los errores del pensamiento, te invitamos a seguir la siguiente estrategia de 5 pasos con la que podrás enfrentar de mejor manera los pensamientos negativos.',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );

      case 1:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.all(20),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Para poder identificar posibles pensamientos negativos puedes hacerte preguntas tales como ',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                              text:
                                  '“¿Qué ideas se vienen a mi cabeza?” o “¿Qué es lo que estoy pensando?”.\n\n',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(
                            text:
                                'En ocasiones es posible que te sea difícil reconocer cuáles eran tus pensamientos, por lo que puede resultarte más sencillo identificar cómo te estabas sintiendo en ese momento. Para eso puedes usar preguntas como por ejemplo ',
                          ),
                          TextSpan(
                              text:
                                  '“¿Cómo me siento con lo que sucedió?” y “¿es posible que me sienta así por lo que estoy pensando?”.',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 2:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.all(20),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Recuerda o vuelve a revisar cuáles son los errores de pensamiento más comunes y sospecha si puedes estar teniendo alguno de ellos. Para esto puedes preguntarte ',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                              text:
                                  '“¿no será que estoy teniendo un error de pensamiento?” y “¿Cuál (o cuáles) puede ser?”.',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 3:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.all(20),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Debes buscar evidencia a favor y en contra de lo que estás pensando por medio de preguntas tales como ',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                              text:
                                  '“¿Qué pistas tengo a favor y en contra de lo que pensé?”, “¿Hay otra forma de explicar lo que me está pasando?”, “¿Cómo me ha ido en otros momentos?”, “¿He podido superar otras situaciones difíciles?”, etc.\n',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(
                            text:
                                'También te puede ser útil preguntarle su opinión a alguien de tu confianza o preguntarte qué le dirías a un amigx que estuviera pensando algo similar.',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 4:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.all(20),
                    child: RichText(
                      text: const TextSpan(
                        text: '',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                'Luego de responder a las preguntas anteriores, y contando con la evidencia suficiente para aquello, debes concluir si estás teniendo o no un error del pensamiento.',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 5:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.all(20),
                    child: RichText(
                      text: const TextSpan(
                        text: '',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                'Si tu conclusión es que estás teniendo un error del pensamiento debes reemplazar ese pensamiento por una alternativa más realista.',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 6:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(top: 20, left: 20, bottom: 5),
                    alignment: Alignment.centerLeft,
                    child: const Text(
                      'Por ejemplo...',
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(fontSize: 22, color: Colors.white),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 5, left: 20, bottom: 20, right: 10),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Si piensas “me voy a echar el ramo” luego de obtener una mala nota en la primera evaluación de un ramo (predecir el futuro), debes reemplazar este pensamiento por una alternativa tal como “no me fue bien en la primera evaluación, me preparé mejor para la próxima”.\nSin embargo, en ocasiones puede que tus pensamientos sean ciertos y que no se estén cometiendo errores del pensamiento.',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 20, left: 20, bottom: 5),
                    alignment: Alignment.centerLeft,
                    child: const Text(
                      'Por ejemplo...',
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(fontSize: 22, color: Colors.white),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 5, left: 20, bottom: 20, right: 10),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Si piensas “me voy a echar el ramo” al final del semestre luego de tener malos resultados en las evaluaciones, es probable que tu pensamiento sea correcto. Para hacer frente a estas situaciones puedes tomar una o más de las siguientes opciones:\n\n',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text: '   ·  Enfocarte en resolver el problema: ',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                          TextSpan(
                              text:
                                  'si la situación es algo que podemos resolver (al menos hasta cierto punto) debemos evaluar las posibilidades y tomar las alternativas que nos permitan resolver el problema. Por ejemplo, si aún tienes posibilidades de obtener una mejor nota porque aún quedan evaluaciones o porque puedes prepararte para el examen final, puedes enfocarte en buscar alternativas para poder estudiar de la mejor manera posible.\n\n'),
                          TextSpan(
                            text: '   ·  Bajarle el perfil: ',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                          TextSpan(
                              text:
                                  'incluso si nos encontramos en un mal escenario, muchas veces nuestros miedos o preocupaciones respecto de la situación son extremos o poco realistas, por lo que es importante que pensemos en resultados más realistas y que le quitemos el carácter catastrófico a las situaciones. Por ejemplo, si al final del curso no hay posibilidades de pasar un ramo, debemos pensar que eso no implica que no puedas hacerlo mejor en el futuro y que tendrás otras oportunidades.\n\n'),
                          TextSpan(
                            text: '   ·  Aceptar y pasar la página: ',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                          TextSpan(
                              text:
                                  'cuando no podemos resolver un problema (o cuando el problema no tiene solución) debemos avanzar hacia aceptar esta realidad. Para poder hacerlo debemos aprender a focalizarnos en nuestros valores, propósitos y acciones valiosas, enfocándonos en las partes más satisfactorias de nuestras vidas y enriqueciendo nuestras experiencias de nuevas formas.'),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 7:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Container(
                margin: const EdgeInsets.all(15),
                child: Column(
                  children: [
                    topStructure(),
                    Container(
                      margin: const EdgeInsets.all(10),
                      child: const FittedBox(
                        fit: BoxFit.fitWidth,
                        child: Text(
                          '¡Muy bien!',
                          textScaler: TextScaler.linear(1.0),
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              // color: CustomColors.primaryDark,
                              fontSize: 24),
                        ),
                      ),
                    ),
                    const Text(
                      'Tenemos una recompensa para ti:',
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(fontSize: 16),
                      textAlign: TextAlign.center,
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 40, bottom: 10),
                      decoration: BoxDecoration(
                          color: CustomColors.lightContainer,
                          border:
                              Border.all(width: 2, color: CustomColors.surface),
                          borderRadius: BorderRadius.circular(15)),
                      child: Row(
                        children: [
                          const Image(
                            width: 100,
                            height: 120,
                            fit: BoxFit.scaleDown,
                            image: AssetImage('assets/images/m3a3.png'),
                          ),
                          Container(
                            margin: const EdgeInsets.only(left: 10),
                            child: const Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Trabajo por mi bienestar:',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      fontSize: 13,
                                      color: CustomColors.primaryDark),
                                ),
                                Text(
                                  'Enfrento los pensamientos\nnegativos',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16,
                                      color: CustomColors.primaryDark),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      default:
        return Container();
    }
  }
}
