// ignore_for_file: prefer_typing_uninitialized_variables

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:seguimiento/colors.dart';
import '../../firebase.dart';
import '../MainPage.dart';

class M3A1 extends StatefulWidget {
  const M3A1({
    Key? key,
  }) : super(key: key);

  @override
  State<M3A1> createState() => _M3A1State();
}

class _M3A1State extends State<M3A1> {
  String uid = FirebaseAuth.instance.currentUser!.uid;
  bool loadingData = true;
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('page-control');
  String activeStepData = '';
  void getActivePageStored() {
    collectionReference.doc(uid).get().then((value) {
      var fields = value;
      activeStepData = fields['M3A1'];
      setState(() {
        if (int.parse(activeStepData) < 13) {
          activeStep = int.parse(activeStepData) - 1;
          Future.delayed(const Duration(milliseconds: 500), () {
            setState(() {
              loadingData = false;
            });
          });
        }
      });
    });
  }

  ScrollController scrollController = ScrollController();
  bool favorite = false;
  CollectionReference favoriteData =
      FirebaseFirestore.instance.collection('favoritos');
  void getFavData() {
    favoriteData.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        favorite = fields['M3A1'];
      });
    }).catchError((err) {
      //print('Error: $err');
    });
  }

  int activeStep = 0;
  var timeStart;
  @override
  void initState() {
    super.initState();
    getActivePageStored();

    getFavData();
    var tiempo = DateTime.now();
    timeStart = tiempo;
    FirestoreService().setTime(tiempo.toString() + ' - START - M3A1',
        'Comenzó M3A1', '0 minutos (comenzando)');
  }

  @override
  void dispose() {
    super.dispose();
  }

  bool active1 = false;
  bool active2 = false;
  bool active3 = false;
  bool active4 = false;
  bool active5 = false;
  bool active6 = false;
  bool active7 = false;
  bool active8 = false;
  bool active9 = false;
  bool active10 = false;
  bool active11 = false;
  bool active12 = false;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        active1 = true;
        active2 = false;
        active3 = false;
        active4 = false;
        active5 = false;
        active6 = false;
        active7 = false;
        active8 = false;
        active9 = false;
        active10 = false;
        active11 = false;
        active12 = false;
        break;
      case 1:
        active1 = true;
        active2 = true;
        active3 = false;
        active4 = false;
        active5 = false;
        active6 = false;
        active7 = false;
        active8 = false;
        active9 = false;
        active10 = false;
        active11 = false;
        active12 = false;

        break;
      case 2:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = false;
        active5 = false;
        active6 = false;
        active7 = false;
        active8 = false;
        active9 = false;
        active10 = false;
        active11 = false;
        active12 = false;

        break;
      case 3:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = false;
        active6 = false;
        active7 = false;
        active8 = false;
        active9 = false;
        active10 = false;
        active11 = false;
        active12 = false;

        break;
      case 4:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = true;
        active6 = false;
        active7 = false;
        active8 = false;
        active9 = false;
        active10 = false;
        active11 = false;
        active12 = false;

        break;
      case 5:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = true;
        active6 = true;
        active7 = false;
        active8 = false;
        active9 = false;
        active10 = false;
        active11 = false;
        active12 = false;

        break;
      case 6:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = true;
        active6 = true;
        active7 = true;
        active8 = false;
        active9 = false;
        active10 = false;
        active11 = false;
        active12 = false;

        break;
      case 7:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = true;
        active6 = true;
        active7 = true;
        active8 = true;
        active9 = false;
        active10 = false;
        active11 = false;
        active12 = false;

        break;
      case 8:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = true;
        active6 = true;
        active7 = true;
        active8 = true;
        active9 = true;
        active10 = false;
        active11 = false;
        active12 = false;

        break;
      case 9:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = true;
        active6 = true;
        active7 = true;
        active8 = true;
        active9 = true;
        active10 = true;
        active11 = false;
        active12 = false;

        break;
      case 10:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = true;
        active6 = true;
        active7 = true;
        active8 = true;
        active9 = true;
        active10 = true;
        active11 = true;
        active12 = false;

        break;
      case 11:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = true;
        active6 = true;
        active7 = true;
        active8 = true;
        active9 = true;
        active10 = true;
        active11 = true;
        active12 = true;

        break;
      default:
    }
    return PopScope(
      canPop: false,
      onPopInvokedWithResult: (bool didPop, result) {
        if (didPop) {
          return;
        }
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(32.0))),
            title: const Text(
              '¿Estás seguro que deseas salir de la actividad?',
              textScaler: TextScaler.linear(1.0),
              style: TextStyle(color: CustomColors.primaryDark),
            ),
            content: const Text(
              'Tus avances quedarán guardados',
              textScaler: TextScaler.linear(1.0),
              style: TextStyle(color: CustomColors.primaryDark),
            ),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: const Text(
                  'Continuar con la actividad',
                  textScaler: TextScaler.linear(1.0),
                  style: TextStyle(color: CustomColors.primary),
                ),
              ),
              TextButton(
                onPressed: () {
                  FirestoreService()
                      .updateCurrentPage('M3A1', (activeStep + 1).toString());
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute<void>(
                          builder: (BuildContext context) => const MainPage(
                                activePage: 0,
                              )),
                      (route) => false);
                },
                child: const Text(
                  'Salir',
                  textScaler: TextScaler.linear(1.0),
                  style: TextStyle(color: CustomColors.error),
                ),
              ),
            ],
          ),
        );
      },
      child: loadingData
          ? Container(
              width: size.width,
              height: size.height,
              color: CustomColors.primaryDark,
              child: const Center(
                  child: CircularProgressIndicator(
                color: CustomColors.tertiary70,
              )))
          : Container(
              color: Colors.black,
              child: SafeArea(
                child: Scaffold(
                  backgroundColor:
                      active12 ? CustomColors.greenEnd : CustomColors.primary3,
                  body: Center(
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              width: size.width / 12,
                              height: 5,
                              color: active1
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 12,
                              height: 5,
                              color: active2
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 12,
                              height: 5,
                              color: active3
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 12,
                              height: 5,
                              color: active4
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 12,
                              height: 5,
                              color: active5
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 12,
                              height: 5,
                              color: active6
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 12,
                              height: 5,
                              color: active7
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 12,
                              height: 5,
                              color: active8
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 12,
                              height: 5,
                              color: active9
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 12,
                              height: 5,
                              color: active10
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 12,
                              height: 5,
                              color: active11
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 12,
                              height: 5,
                              color: active12
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                          ],
                        ),
                        content(),
                        Container(
                          margin: const EdgeInsets.only(top: 10, bottom: 10),
                          child: bottomStructure(),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
    );
  }

  Widget topStructure() {
    Size size = MediaQuery.of(context).size;

    return Container(
      width: size.width,
      height: active12 ? null : size.height / 3,
      color: active12 ? CustomColors.greenList : CustomColors.primary60,
      child: Column(
        children: [
          Row(
            children: [
              active12
                  ? Container()
                  : Container(
                      alignment: Alignment.topRight,
                      child: IconButton(
                        onPressed: () {
                          showDialog(
                            context: context,
                            builder: (context) => AlertDialog(
                              shape: const RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(32.0))),
                              title: const Text(
                                '¿Estás seguro que deseas salir de la actividad?',
                                textScaler: TextScaler.linear(1.0),
                                style:
                                    TextStyle(color: CustomColors.primaryDark),
                              ),
                              content: const Text(
                                'Tus avances quedarán guardados',
                                textScaler: TextScaler.linear(1.0),
                                style:
                                    TextStyle(color: CustomColors.primaryDark),
                              ),
                              actions: [
                                TextButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                  child: const Text(
                                    'Continuar con la actividad',
                                    textScaler: TextScaler.linear(1.0),
                                    style:
                                        TextStyle(color: CustomColors.primary),
                                  ),
                                ),
                                TextButton(
                                  onPressed: () {
                                    String tiempo = DateTime.now().toString();
                                    FirestoreService().setTime(
                                        tiempo + ' - CLOSED - M3A1',
                                        'Salió de M3A1 sin terminar',
                                        '0 minutos (sin terminar)');
                                    FirestoreService().updateCurrentPage(
                                        'M3A1', (activeStep + 1).toString());
                                    Navigator.pushAndRemoveUntil(
                                        context,
                                        MaterialPageRoute<void>(
                                            builder: (BuildContext context) =>
                                                const MainPage(
                                                  activePage: 0,
                                                )),
                                        (route) => false);
                                  },
                                  child: const Text(
                                    'Salir',
                                    textScaler: TextScaler.linear(1.0),
                                    style: TextStyle(color: CustomColors.error),
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                        icon: const Icon(
                          Icons.close,
                          color: CustomColors.primaryDark,
                        ),
                      ),
                    ),
              const Spacer(),
              active12 ? Container() : favorito(),
            ],
          ),
          Text(
            headerText(),
            textScaler: const TextScaler.linear(1.0),
            textAlign: TextAlign.center,
            style: const TextStyle(
              color: CustomColors.primaryDark,
              fontSize: 24,
            ),
          ),
          active12 ? Container() : const Spacer(),
          active12
              ? header()
              : Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    headerTitleText(),
                    header(),
                  ],
                ),
        ],
      ),
    );
  }

  Widget favorito() {
    return StreamBuilder<QuerySnapshot>(
        stream: FirestoreService().favoritos(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasData) {
            return SizedBox(
              width: 50,
              height: 50,
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (context, index) {
                    var document = snapshot.data?.docs[index];
                    if (document != null && document.id == uid) {
                      switch (document['M3A1']) {
                        case true:
                          return IconButton(
                              icon: Icon(MdiIcons.star,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('M3A1', false)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Eliminado de favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                        case false:
                          return IconButton(
                              icon: Icon(MdiIcons.starOutline,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('M3A1', true)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Agregado a favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                      }
                    } else {
                      return Container();
                    }
                    return null;
                  }),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(
                color: Colors.red,
              ),
            );
          }
        });
  }

  Widget bottomStructure() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        nextButton(),
      ],
    );
  }

  Widget nextButton() {
    Size size = MediaQuery.of(context).size;
    if (activeStep >= 1 && activeStep < 11) {
      return SizedBox(
        width: size.width / 1,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        WidgetStateProperty.all<Color>(CustomColors.primary3),
                    shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: const BorderSide(color: Colors.white)),
                    ),
                  ),
                  onPressed: () {
                    setState(() {
                      activeStep = activeStep - 1;
                    });
                  },
                  child: const Text(
                    'Anterior',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(color: Colors.white),
                  )),
            ),
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: WidgetStateProperty.all<Color>(active12
                      ? CustomColors.primary
                      : CustomColors.greenButton),
                  shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ),
                  ),
                ),
                onPressed: () {
                  if (activeStep < 11) {
                    setState(() {
                      activeStep++;
                    });
                  } else {
                    FirestoreService().terminaSesion('3', '1',
                        'Leyó la introducción → Aprendió sobre los 10 errores del pensamiento → Terminó la actividad 14');
                    var tiempo = DateTime.now();
                    var difTiempo = tiempo.difference(timeStart);
                    FirestoreService().setTime(
                        tiempo.toString() + ' - ENDED - M3A1',
                        'Terminó M3A1',
                        difTiempo.inMinutes.toString() + ' minutos');
                    FirestoreService().updateCurrentPage('M3A1', '1');
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute<void>(
                            builder: (BuildContext context) => const MainPage(
                                  activePage: 0,
                                )),
                        (route) => false);
                  }
                },
                child: Text(
                  buttonText(),
                  textScaler: const TextScaler.linear(1.0),
                  style: const TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      return SizedBox(
        width: size.width / 1.2,
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor: WidgetStateProperty.all<Color>(
                active12 ? CustomColors.primary : CustomColors.greenButton),
            shape: WidgetStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              ),
            ),
          ),
          onPressed: () {
            if (activeStep < 11) {
              setState(() {
                activeStep++;
              });
            } else {
              FirestoreService().terminaSesion('3', '1',
                  'Leyó la introducción → Aprendió sobre los 10 errores del pensamiento → Terminó la actividad 14');
              var tiempo = DateTime.now();
              var difTiempo = tiempo.difference(timeStart);
              FirestoreService().setTime(tiempo.toString() + ' - ENDED - M3A1',
                  'Terminó M3A1', difTiempo.inMinutes.toString() + ' minutos');
              FirestoreService().updateCurrentPage('M3A1', '1');
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute<void>(
                      builder: (BuildContext context) => const MainPage(
                            activePage: 0,
                          )),
                  (route) => false);
            }
          },
          child: Text(
            buttonText(),
            textScaler: const TextScaler.linear(1.0),
            style: TextStyle(
              color: active12 ? CustomColors.surface : CustomColors.primaryDark,
            ),
          ),
        ),
      );
    }
  }

  Widget header() {
    Size size = MediaQuery.of(context).size;
    switch (activeStep) {
      case 11:
        return Image(
          width: size.width / 8,
          height: size.width / 8,
          fit: BoxFit.scaleDown,
          image: AssetImage(imageHeader()),
        );
      default:
        return Image(
          width: kIsWeb ? 100 : size.width / 3,
          height: kIsWeb ? 100 : size.width / 3,
          fit: BoxFit.scaleDown,
          image: AssetImage(imageHeader()),
        );
    }
  }

  Widget headerTitleText() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        return Container();

      case 11:
        return Container();
      default:
        return SizedBox(
          width: size.width / 2,
          child: Text(
            tituloHeader(),
            style: const TextStyle(
                fontSize: 16,
                color: CustomColors.primaryDark,
                fontWeight: FontWeight.bold),
          ),
        );
    }
  }

  String tituloHeader() {
    switch (activeStep) {
      case 1:
        return 'Error 1:\n"Predecir el futuro"';
      case 2:
        return 'Error 2:\n"Todo o nada"';
      case 3:
        return 'Error 3:\n"Lectura de mentes"';
      case 4:
        return 'Error 4:\n"Tomarlo de forma personal"';
      case 5:
        return 'Error 5:\n"Descontar lo positivo"';
      case 6:
        return 'Error 6:\n"Pensamiento emocional"';
      case 7:
        return 'Error 7:\n"Debería o tengo que…"';
      case 8:
        return 'Error 8:\n"Poner etiquetas"';
      case 9:
        return 'Error 9:\n"Sobregeneralizar"';
      case 10:
        return 'Error 10:\n"Filtro mental"';

      default:
        return '';
    }
  }

  String imageHeader() {
    switch (activeStep) {
      case 0:
        return 'assets/images/m3a1.png';

      case 11:
        return 'assets/images/end.png';

      default:
        return 'assets/images/m3a1.png';
    }
  }

  String headerText() {
    switch (activeStep) {
      case 11:
        return '';
      default:
        return '¿Qué son los errores del pensamiento?';
    }
  }

  String buttonText() {
    switch (activeStep) {
      case 0:
        return 'Conoce los 10 errores más comunes';
      case 10:
        return 'Finalizar';
      case 11:
        return 'Continuar con el programa';
      default:
        return 'Siguiente';
    }
  }

  Widget content() {
    switch (activeStep) {
      case 0:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.all(20),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'En esta actividad te invitamos a que conozcas los errores del pensamiento, cómo estos impactan en tu estado de ánimo y en las cosas que haces.\n\n',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                '¿Sabías que la interpretación que hacemos de las situaciones (más que la situación en sí) influye en cómo nos sentimos, comportamos y en nuestras reacciones fisiológicas?\n\n',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                          TextSpan(
                              text:
                                  'Existen ciertas situaciones que son casi “universalmente” perturbadoras (ej. cuando nos rechaza la persona que nos gusta, cuando tenemos una pelea con alguien a quien queremos, etc.). Sin embargo, hay veces en las cuales podemos ver las situaciones de forma mucho más negativa de lo que realmente son, incluso pudiendo malinterpretar situaciones neutrales o positivas. Así, resulta esencial que identifiques tus errores de pensamiento para poder enfrentarlos de forma adecuada y cuidar de tu bienestar.\n\n'),
                          TextSpan(
                              text:
                                  'Los errores del pensamiento son aquellos que:\n'),
                          TextSpan(
                            text: '   ·  Distorsionan la realidad.\n',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                          TextSpan(
                            text:
                                '   ·  Nos hacen sentir mal (ej. Angustiadxs, bajoneadxs o enojadxs) de forma innecesaria.\n',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                          TextSpan(
                            text:
                                '   ·  Nos conducen a conductas infructuosas o dañinas.\n',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                          TextSpan(
                            text:
                                '   ·  Nos impiden hacer nuestras actividades cotidianas y/o avanzar hacia nuestras metas.\n',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    margin:
                        const EdgeInsets.only(top: 30, left: 20, bottom: 10),
                    alignment: Alignment.centerLeft,
                    child: const Text(
                      'Conoce los 10 errores de\npensamiento más comunes',
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(fontSize: 22, color: Colors.white),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.all(20),
                    child: RichText(
                      text: const TextSpan(
                        text: '',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                'Darte cuenta de que estás pensando de manera equivocada puede ser el primer paso para mejorar tu estado de ánimo, sentirte bien contigo mismx y con quienes te rodean. Además, conocer los errores que puedes cometer al pensar también te ayudará a reconocerlos en otras personas y entender cómo se sienten.',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );

      case 1:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.all(20),
                    alignment: Alignment.centerLeft,
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Exagerar el significado de un evento, pensando de forma pesimista acerca del futuro esperando que las cosas estén cada vez peor, terminan mal o sean un fracaso.\n\n',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text: 'Ejemplo:\n',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                          TextSpan(
                              text:
                                  '"Me fue mal en la primera prueba, me voy a echar el ramo"'),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 2:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.all(20),
                    alignment: Alignment.centerLeft,
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Pensar en extremo (siempre/nunca; todos/nadie), viendo las cosas en blanco o negro, sin que existan términos medios.\n\n',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text: 'Ejemplo:\n',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                          TextSpan(
                              text:
                                  '"Si no logro todo lo que espero entonces soy un fracaso"'),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 3:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.all(20),
                    alignment: Alignment.centerLeft,
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Creer que podemos adivinar lo que otros están pensando o sintiendo, interpretándolo de forma negativa y sin tener la información suficiente para hacerlo.\n\n',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text: 'Ejemplo:\n',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                          TextSpan(
                              text:
                                  '"Mi compañerx está pensando que no puedo aportar nada al trabajo que estamos haciendo"'),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 4:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.all(20),
                    alignment: Alignment.centerLeft,
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Culparnos por las cosas que salen mal o creer que los eventos externos se relacionan con nosotros, sin que haya evidencia de eso.\n\n',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text: 'Ejemplo:\n',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                          TextSpan(
                              text:
                                  '"Mi amigx no me ha escrito hoy, debe estar enojadx conmigo"'),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 5:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.all(20),
                    alignment: Alignment.centerLeft,
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Fijarse solo en negativo de las situaciones, sin considerar lo positivo, viendo el vaso medio vacío.\n\n',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text: 'Ejemplo:\n',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                          TextSpan(
                              text:
                                  '“Tuve una buena nota en la presentación, pero eso no significa que lo hice bien solo tuve suerte”'),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 6:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.all(20),
                    alignment: Alignment.centerLeft,
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Pensar que algo debe ser verdad si es que así lo “sentimos” (“si lo siento debe ser así”), ignorando o descartando información que nos contradice.\n\n',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text: 'Ejemplo:\n',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                          TextSpan(
                              text:
                                  '"Mis amigxs siempre han estado ahí, pero aún así siento que no les importo"'),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 7:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.all(20),
                    alignment: Alignment.centerLeft,
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Autoexigirse demasiado, poniéndonos metas demasiado altas sin permitirnos cometer errores y exagerando el significado negativo si las expectativas no se cumplen.\n\n',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text: 'Ejemplo:\n',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                          TextSpan(
                              text:
                                  '"No me puedo equivocar nunca, siempre tengo que hacer las cosas bien"'),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 8:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.all(20),
                    alignment: Alignment.centerLeft,
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Nos ponemos etiquetas rígidas y globales acerca de nosotros mismos o de otros, sin considerar la evidencia que nos pueda llevar a una conclusión más razonable y/o menos extrema.\n\n',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text: 'Ejemplo:\n',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                          TextSpan(text: '"Soy tontx"'),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 9:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });
        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.all(20),
                    alignment: Alignment.centerLeft,
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Llegar a conclusiones negativas y radicales que van mucho más allá de la situación actual, viendo eventos negativos aislados como un patrón global de nunca acabar.\n\n',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text: 'Ejemplo:\n',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                          TextSpan(
                              text:
                                  '"Me sentí incómodx en la fiesta, no voy a poder hacer nuevxs amigxs"'),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 10:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.all(20),
                    alignment: Alignment.centerLeft,
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Prestar atención indebida a un detalle negativo, en lugar de ver la imagen completa.\n\n',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text: 'Ejemplo:\n',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                          TextSpan(
                              text:
                                  '"Como me fue mal en esta prueba significa que no soy buenx en esto"'),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 11:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Container(
                margin: const EdgeInsets.all(15),
                child: Column(
                  children: [
                    topStructure(),
                    Container(
                      margin: const EdgeInsets.all(10),
                      child: const FittedBox(
                        fit: BoxFit.fitWidth,
                        child: Text(
                          '¡Muy bien!',
                          textScaler: TextScaler.linear(1.0),
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              // color: CustomColors.primaryDark,
                              fontSize: 24),
                        ),
                      ),
                    ),
                    const Text(
                      'Tenemos una recompensa para ti:',
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(fontSize: 16),
                      textAlign: TextAlign.center,
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 40, bottom: 10),
                      decoration: BoxDecoration(
                          color: CustomColors.lightContainer,
                          border:
                              Border.all(width: 2, color: CustomColors.surface),
                          borderRadius: BorderRadius.circular(15)),
                      child: Row(
                        children: [
                          const Image(
                            width: 100,
                            height: 120,
                            fit: BoxFit.scaleDown,
                            image: AssetImage('assets/images/m3a1.png'),
                          ),
                          Container(
                            margin: const EdgeInsets.only(left: 10),
                            child: const Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Conocimiento en salud mental:',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      fontSize: 13,
                                      color: CustomColors.primaryDark),
                                ),
                                Text(
                                  '¿Qué son los errores\ndel pensamiento?',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16,
                                      color: CustomColors.primaryDark),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      default:
        return Container();
    }
  }
}
