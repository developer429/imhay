// ignore_for_file: avoid_print, prefer_typing_uninitialized_variables

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:seguimiento/colors.dart';
import '../../firebase.dart';
import '../MainPage.dart';

class M3A7 extends StatefulWidget {
  const M3A7({
    Key? key,
  }) : super(key: key);

  @override
  State<M3A7> createState() => _M3A7State();
}

class _M3A7State extends State<M3A7> {
  String uid = FirebaseAuth.instance.currentUser!.uid;
  bool loadingData = true;
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('page-control');
  String activeStepData = '';
  void getActivePageStored() {
    collectionReference.doc(uid).get().then((value) {
      var fields = value;
      activeStepData = fields['M3A7'];
      setState(() {
        if (int.parse(activeStepData) < 7) {
          activeStep = int.parse(activeStepData) - 1;
          Future.delayed(const Duration(milliseconds: 500), () {
            setState(() {
              loadingData = false;
            });
          });
        }
      });
    });
  }

  CollectionReference dataReference =
      FirebaseFirestore.instance.collection('M3A7-respuestas');
  void getData() {
    dataReference.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        alter1negativa.text = fields['alter1negativa'];
        alter1positiva.text = fields['alter1positiva'];
        alter2negativa.text = fields['alter2negativa'];
        alter2positiva.text = fields['alter2positiva'];
      });
    }).catchError((err) {
      // print('Error: $err');
    });
  }

  ScrollController scrollController = ScrollController();

  bool favorite = false;
  CollectionReference favoriteData =
      FirebaseFirestore.instance.collection('favoritos');
  void getFavData() {
    favoriteData.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        favorite = fields['M3A7'];
      });
    }).catchError((err) {
      //print('Error: $err');
    });
  }

  final _formKey = GlobalKey<FormState>();
  final alter1positiva = TextEditingController();
  final alter1negativa = TextEditingController();
  final alter2positiva = TextEditingController();
  final alter2negativa = TextEditingController();

  String alternativa1 = '';
  String alternativa2 = '';
  String alternativa3 = '';
  String alternativa4 = '';
  String alternativa5 = '';

  bool alter3 = false;
  bool alter4 = false;
  bool alter5 = false;

  bool alter1check = false;
  bool alter2check = false;
  bool alter3check = false;
  bool alter4check = false;
  bool alter5check = false;

  String selectedOption1 = '';
  String selectedOption2 = '';
  String selectedOption3 = '';
  String selectedOption4 = '';
  String selectedOption5 = '';
  String seleccionada1 = '';
  String seleccionada2 = '';

  int checkedCount = 0;
  bool selectOne = false;
  bool selectTwo = false;
  bool selectNone = false;

  CollectionReference alternativasReference =
      FirebaseFirestore.instance.collection('M3A6-respuestas');
  void getAlternativas() {
    alternativasReference.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        alternativa1 = fields['solucion1'];
        alternativa2 = fields['solucion2'];
        alternativa3 = fields['solucion3'];
        alternativa4 = fields['solucion4'];
        alternativa5 = fields['solucion5'];
        if (alternativa3 != '-') {
          alter3 = true;
        }
        if (alternativa4 != '-') {
          alter4 = true;
        }
        if (alternativa5 != '-') {
          alter5 = true;
        }
      });
    });
  }

  CollectionReference seleccionadasReference =
      FirebaseFirestore.instance.collection('M3A7-respuestas');
  void getSeleccionadas() {
    seleccionadasReference.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        if (selectedAlternatives.isEmpty) {
          selectedAlternatives.add(fields['alternativa1']);
          selectedAlternatives.add(fields['alternativa2']);
        }
      });
    }).catchError((err) {
      print('Error: $err');
    });
  }

  int activeStep = 0;
  var timeStart;
  @override
  void initState() {
    super.initState();
    getActivePageStored();
    getAlternativas();
    getData();
    getSeleccionadas();
    getFavData();
    var tiempo = DateTime.now();
    timeStart = tiempo;
    FirestoreService().setTime(tiempo.toString() + ' - START - M3A7',
        'Comenzó M3A7', '0 minutos (comenzando)');
  }

  @override
  void dispose() {
    super.dispose();
  }

  List<String> selectedAlternatives = [];

  bool active1 = false;
  bool active2 = false;
  bool active3 = false;
  bool active4 = false;
  bool active5 = false;
  String finalSeleccionada = '';
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        active1 = true;
        active2 = false;
        active3 = false;
        active4 = false;
        active5 = false;
        break;
      case 1:
        active1 = true;
        active2 = true;
        active3 = false;
        active4 = false;
        active5 = false;

        break;
      case 2:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = false;
        active5 = false;

        break;
      case 3:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = false;

        break;
      case 4:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = true;

        break;

      default:
    }
    return PopScope(
      canPop: false,
      onPopInvokedWithResult: (bool didPop, result) {
        if (didPop) {
          return;
        }
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(32.0))),
            title: const Text(
              '¿Estás seguro que deseas salir de la actividad?',
              textScaler: TextScaler.linear(1.0),
              style: TextStyle(color: CustomColors.primaryDark),
            ),
            content: const Text(
              'Tus avances quedarán guardados',
              textScaler: TextScaler.linear(1.0),
              style: TextStyle(color: CustomColors.primaryDark),
            ),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: const Text(
                  'Continuar con la actividad',
                  textScaler: TextScaler.linear(1.0),
                  style: TextStyle(color: CustomColors.primary),
                ),
              ),
              TextButton(
                onPressed: () {
                  FirestoreService()
                      .updateCurrentPage('M3A7', (activeStep + 1).toString());
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute<void>(
                          builder: (BuildContext context) => const MainPage(
                                activePage: 0,
                              )),
                      (route) => false);
                },
                child: const Text(
                  'Salir',
                  textScaler: TextScaler.linear(1.0),
                  style: TextStyle(color: CustomColors.error),
                ),
              ),
            ],
          ),
        );
      },
      child: loadingData
          ? Container(
              width: size.width,
              height: size.height,
              color: CustomColors.primaryDark,
              child: const Center(
                  child: CircularProgressIndicator(
                color: CustomColors.tertiary70,
              )))
          : Container(
              color: Colors.black,
              child: SafeArea(
                child: Scaffold(
                  backgroundColor:
                      active5 ? CustomColors.greenEnd : CustomColors.primary3,
                  body: Center(
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              width: size.width / 5,
                              height: 5,
                              color: active1
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 5,
                              height: 5,
                              color: active2
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 5,
                              height: 5,
                              color: active3
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 5,
                              height: 5,
                              color: active4
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 5,
                              height: 5,
                              color: active5
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                          ],
                        ),
                        content(),
                        Container(
                          margin: const EdgeInsets.only(top: 10, bottom: 10),
                          child: bottomStructure(),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
    );
  }

  Widget topStructure() {
    Size size = MediaQuery.of(context).size;
    return Container(
      width: size.width,
      height: active5 ? null : size.height / 3,
      color: active5 ? CustomColors.greenList : CustomColors.primary60,
      child: Column(
        children: [
          Row(
            children: [
              active5
                  ? Container()
                  : Container(
                      alignment: Alignment.topRight,
                      child: IconButton(
                        onPressed: () {
                          showDialog(
                            context: context,
                            builder: (context) => AlertDialog(
                              shape: const RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(32.0))),
                              title: const Text(
                                '¿Estás seguro que deseas salir de la actividad?',
                                textScaler: TextScaler.linear(1.0),
                                style:
                                    TextStyle(color: CustomColors.primaryDark),
                              ),
                              content: const Text(
                                'Tus avances quedarán guardados',
                                textScaler: TextScaler.linear(1.0),
                                style:
                                    TextStyle(color: CustomColors.primaryDark),
                              ),
                              actions: [
                                TextButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                  child: const Text(
                                    'Continuar con la actividad',
                                    textScaler: TextScaler.linear(1.0),
                                    style:
                                        TextStyle(color: CustomColors.primary),
                                  ),
                                ),
                                TextButton(
                                  onPressed: () {
                                    FirestoreService().updateCurrentPage(
                                        'M3A7', (activeStep + 1).toString());
                                    Navigator.pushAndRemoveUntil(
                                        context,
                                        MaterialPageRoute<void>(
                                            builder: (BuildContext context) =>
                                                const MainPage(
                                                  activePage: 0,
                                                )),
                                        (route) => false);
                                  },
                                  child: const Text(
                                    'Salir',
                                    textScaler: TextScaler.linear(1.0),
                                    style: TextStyle(color: CustomColors.error),
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                        icon: const Icon(
                          Icons.close,
                          color: CustomColors.primaryDark,
                        ),
                      ),
                    ),
              const Spacer(),
              active5 ? Container() : favorito(),
            ],
          ),
          Text(
            headerText(),
            textScaler: const TextScaler.linear(1.0),
            textAlign: TextAlign.center,
            style: const TextStyle(
              color: CustomColors.primaryDark,
              fontSize: 24,
            ),
          ),
          active5 ? Container() : const Spacer(),
          active5
              ? header()
              : Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    headerTitleText(),
                    header(),
                  ],
                ),
        ],
      ),
    );
  }

  Widget favorito() {
    return StreamBuilder<QuerySnapshot>(
        stream: FirestoreService().favoritos(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasData) {
            return SizedBox(
              width: 50,
              height: 50,
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (context, index) {
                    var document = snapshot.data?.docs[index];
                    if (document != null && document.id == uid) {
                      switch (document['M3A7']) {
                        case true:
                          return IconButton(
                              icon: Icon(MdiIcons.star,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('M3A7', false)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Eliminado de favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                        case false:
                          return IconButton(
                              icon: Icon(MdiIcons.starOutline,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('M3A7', true)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Agregado a favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                      }
                    } else {
                      return Container();
                    }
                    return null;
                  }),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(
                color: Colors.red,
              ),
            );
          }
        });
  }

  Widget bottomStructure() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        nextButton(),
      ],
    );
  }

  Widget nextButton() {
    Size size = MediaQuery.of(context).size;
    if (activeStep >= 1 && activeStep < 4) {
      return SizedBox(
        width: size.width / 1,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        WidgetStateProperty.all<Color>(CustomColors.primary3),
                    shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: const BorderSide(color: Colors.white)),
                    ),
                  ),
                  onPressed: () {
                    setState(() {
                      activeStep = activeStep - 1;
                    });
                  },
                  child: const Text(
                    'Anterior',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(color: Colors.white),
                  )),
            ),
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: WidgetStateProperty.all<Color>(active5
                      ? CustomColors.primary
                      : CustomColors.greenButton),
                  shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ),
                  ),
                ),
                onPressed: () {
                  if (activeStep < 4) {
                    if (activeStep == 0) {
                      setState(() {
                        activeStep++;
                      });
                    }
                    if (activeStep == 1) {
                      if (checkedCount == 2) {
                        setState(() {
                          FirestoreService()
                              .alternativasSeleccionadasM3A7(
                                selectedAlternatives[0],
                                selectedAlternatives[1],
                              )
                              .then(
                                (value) => setState(() {
                                  activeStep++;
                                }),
                              );
                        });
                      } else {
                        showDialog<String>(
                          context: context,
                          builder: (BuildContext context) => AlertDialog(
                            shape: const RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(32.0))),
                            title: const Text(
                              'Aviso',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 25,
                                  color: CustomColors.primaryDark),
                            ),
                            content: const Text(
                              'Selecciona 2 alternativas',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(color: CustomColors.primaryDark),
                            ),
                            actions: <Widget>[
                              TextButton(
                                onPressed: () =>
                                    Navigator.pop(context, 'Cerrar'),
                                child: const Text(
                                  'Cerrar',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(color: CustomColors.primary),
                                ),
                              ),
                            ],
                          ),
                        );
                      }
                    }
                    if (activeStep == 2) {
                      if (_formKey.currentState!.validate()) {
                        FirestoreService()
                            .guardarRespuestasM3A7(
                                alter1positiva.text.trim(),
                                alter1negativa.text.trim(),
                                alter2positiva.text.trim(),
                                alter2negativa.text.trim())
                            .then(
                              (value) => setState(() {
                                activeStep++;
                              }),
                            );
                      } else {
                        showDialog<String>(
                          context: context,
                          builder: (BuildContext context) => AlertDialog(
                            title: const Text(
                              'Error',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  color: Colors.red,
                                  fontWeight: FontWeight.bold),
                            ),
                            content: const Text(
                              'Complete los campos faltantes',
                              textScaler: TextScaler.linear(1.0),
                            ),
                            actions: <Widget>[
                              TextButton(
                                onPressed: () => Navigator.pop(
                                  context,
                                ),
                                child: const Text(
                                  'OK',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      color: CustomColors.primaryDark,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                        );
                      }
                    }
                    if (activeStep == 3) {
                      if (finalSeleccionada != '') {
                        setState(() {
                          FirestoreService()
                              .alternativasFinalSeleccionadasM3A7(
                                finalSeleccionada,
                              )
                              .then(
                                (value) => setState(() {
                                  activeStep++;
                                }),
                              );
                        });
                      } else {
                        showDialog<String>(
                          context: context,
                          builder: (BuildContext context) => AlertDialog(
                            shape: const RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(32.0))),
                            title: const Text(
                              'Aviso',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 25,
                                  color: CustomColors.primaryDark),
                            ),
                            content: const Text(
                              'Selecciona una opción',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(color: CustomColors.primaryDark),
                            ),
                            actions: <Widget>[
                              TextButton(
                                onPressed: () =>
                                    Navigator.pop(context, 'Cerrar'),
                                child: const Text(
                                  'Cerrar',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(color: CustomColors.primary),
                                ),
                              ),
                            ],
                          ),
                        );
                      }
                    }
                  } else {
                    FirestoreService().terminaSesion('3', '7',
                        'Leyó la introducción → Seleccionó dos de sus alternativas de solución → Definió consecuencias positivas y negativas para las alternativas que seleccionó → Se decidió por una alternativa → Terminó la actividad 20');
                    var tiempo = DateTime.now();
                    var difTiempo = tiempo.difference(timeStart);
                    FirestoreService().setTime(
                        tiempo.toString() + ' - ENDED - M3A7',
                        'Terminó M3A7',
                        difTiempo.inMinutes.toString() + ' minutos');
                    FirestoreService().updateCurrentPage('M3A7', '1');
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute<void>(
                            builder: (BuildContext context) => const MainPage(
                                  activePage: 0,
                                )),
                        (route) => false);
                  }
                },
                child: Text(
                  buttonText(),
                  textScaler: const TextScaler.linear(1.0),
                  style: const TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      return SizedBox(
        width: size.width / 1.2,
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor: WidgetStateProperty.all<Color>(
                active5 ? CustomColors.primary : CustomColors.greenButton),
            shape: WidgetStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              ),
            ),
          ),
          onPressed: () {
            if (activeStep < 4) {
              if (activeStep == 0 || activeStep == 3) {
                setState(() {
                  activeStep++;
                });
              }
              if (activeStep == 1) {
                if (checkedCount == 2) {
                  setState(() {
                    FirestoreService()
                        .alternativasSeleccionadasM3A7(
                          selectedAlternatives[0],
                          selectedAlternatives[1],
                        )
                        .then(
                          (value) => setState(() {
                            activeStep++;
                          }),
                        );
                  });
                } else {
                  showDialog<String>(
                    context: context,
                    builder: (BuildContext context) => AlertDialog(
                      shape: const RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(32.0))),
                      title: const Text(
                        'Aviso',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontSize: 25, color: CustomColors.primaryDark),
                      ),
                      content: const Text(
                        'Selecciona 2 alternativas',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(color: CustomColors.primaryDark),
                      ),
                      actions: <Widget>[
                        TextButton(
                          onPressed: () => Navigator.pop(context, 'Cerrar'),
                          child: const Text(
                            'Cerrar',
                            textScaler: TextScaler.linear(1.0),
                            style: TextStyle(color: CustomColors.primary),
                          ),
                        ),
                      ],
                    ),
                  );
                }
              }
              if (activeStep == 2) {
                if (_formKey.currentState!.validate()) {
                  setState(() {
                    activeStep++;
                  });
                } else {
                  showDialog<String>(
                    context: context,
                    builder: (BuildContext context) => AlertDialog(
                      title: const Text(
                        'Error',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            color: Colors.red, fontWeight: FontWeight.bold),
                      ),
                      content: const Text(
                        'Complete los campos faltantes',
                        textScaler: TextScaler.linear(1.0),
                      ),
                      actions: <Widget>[
                        TextButton(
                          onPressed: () => Navigator.pop(
                            context,
                          ),
                          child: const Text(
                            'OK',
                            textScaler: TextScaler.linear(1.0),
                            style: TextStyle(
                                color: CustomColors.primaryDark,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                  );
                }
              }
            } else {
              FirestoreService().terminaSesion('3', '7',
                  'Leyó la introducción → Seleccionó dos de sus alternativas de solución → Definió consecuencias positivas y negativas para las alternativas que seleccionó → Se decidió por una alternativa → Terminó la actividad 20');
              var tiempo = DateTime.now();
              var difTiempo = tiempo.difference(timeStart);
              FirestoreService().setTime(tiempo.toString() + ' - ENDED - M3A7',
                  'Terminó M3A7', difTiempo.inMinutes.toString() + ' minutos');
              FirestoreService().updateCurrentPage('M3A7', '1');
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute<void>(
                      builder: (BuildContext context) => const MainPage(
                            activePage: 0,
                          )),
                  (route) => false);
            }
          },
          child: Text(
            buttonText(),
            textScaler: const TextScaler.linear(1.0),
            style: TextStyle(
              color: active5 ? CustomColors.surface : CustomColors.primaryDark,
            ),
          ),
        ),
      );
    }
  }

  Widget header() {
    Size size = MediaQuery.of(context).size;
    switch (activeStep) {
      case 4:
        return Image(
          width: size.width / 8,
          height: size.width / 8,
          fit: BoxFit.scaleDown,
          image: AssetImage(imageHeader()),
        );
      default:
        return Image(
          width: kIsWeb ? 100 : size.width / 3,
          height: kIsWeb ? 100 : size.width / 3,
          fit: BoxFit.scaleDown,
          image: AssetImage(imageHeader()),
        );
    }
  }

  Widget headerTitleText() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        return Container();

      case 4:
        return Container();
      default:
        return SizedBox(
          width: size.width / 2,
          child: Text(
            tituloHeader(),
            textScaler: const TextScaler.linear(1.0),
            style: const TextStyle(
                fontSize: 17,
                color: CustomColors.primaryDark,
                fontWeight: FontWeight.bold),
          ),
        );
    }
  }

  String tituloHeader() {
    switch (activeStep) {
      case 1:
        return 'Paso 1:\nIdentifica y descarta las soluciones que serán inefectivas';
      case 2:
        return 'Paso 2:\nPredice las posibles consecuencias y resultados de las otras alternativas';
      case 3:
        return 'Paso 3:\nIdentifica la solución efectiva';

      default:
        return '';
    }
  }

  String imageHeader() {
    switch (activeStep) {
      case 0:
        return 'assets/images/m3a7.png';

      case 4:
        return 'assets/images/end.png';

      default:
        return 'assets/images/m3a7.png';
    }
  }

  String headerText() {
    switch (activeStep) {
      case 4:
        return '';
      default:
        return 'Tomando decisiones';
    }
  }

  String buttonText() {
    switch (activeStep) {
      case 0:
        return 'Comenzar';
      case 3:
        return 'Finalizar';
      case 4:
        return 'Continuar con el programa';
      default:
        return 'Siguiente';
    }
  }

  Widget content() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.all(20),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            '¡Buen trabajo! Ya has realizado pasos importantes para poder solucionar el problema al cual te enfrentas hoy en día, y ahora que ya cuentas con diversas ideas y estrategias posibles de solución llegó el momento de tomar decisiones. Para esto en primer lugar debes evaluar los pros y contras de las ideas que resultaron de tu trabajo anterior, de modo tal que puedas maximizar los efectos positivos y minimizar las consecuencias negativas de tu elección. En este punto es importante que tengas cuidado de las soluciones rápidas que puedan entregar alivio inmediato a parte del problema, pero que a largo plazo puedan ser perjudiciales tanto para ti como para quienes te rodean. \n\n',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                'Sabemos que tomar decisiones puede ser difícil, por lo cual queremos acompañarte en este proceso por medio de los siguientes pasos:',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );

      case 1:
        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.all(20),
                    alignment: Alignment.centerLeft,
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Luego de un análisis inicial rápido elimina de tu lista de soluciones aquellas que son evidentemente de “menor calidad” ya sea porque generan riesgos o consecuencias negativas inaceptables o porque no son posibles de ser implementadas debido a falta de habilidades, recursos u otros obstáculos significativos.\n\n',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                              text:
                                  'Ahora que ya realizaste tu análisis inicial, te invitamos a escoger dos alternativas de solución. Estas alternativas pueden ser algunas de las alternativas anteriores que no hayas descartado, o también pueden ser nuevas alternativas que hayan surgido en este paso. Escoge tus alternativas de solución de la siguiente lista o vuelve al paso anterior para escribir nuevas alternativas de solución.'),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.all(20),
                    child: Column(
                      children: <Widget>[
                        CheckboxListTile(
                          controlAffinity: ListTileControlAffinity.leading,
                          side: const BorderSide(color: CustomColors.surface),
                          activeColor: CustomColors.primary70,
                          checkColor: CustomColors.primaryDark,
                          title: Text(
                            alternativa1,
                            textScaler: const TextScaler.linear(1.0),
                            style: const TextStyle(
                                fontSize: 18, color: CustomColors.surface),
                          ),
                          value: alter1check,
                          onChanged: (bool? value) {
                            setState(() {
                              alter1check = value!;
                              if (value == true) {
                                checkedCount = checkedCount + 1;
                                selectedAlternatives.add(alternativa1);
                              } else {
                                checkedCount = checkedCount - 1;
                                selectedAlternatives.remove(alternativa1);
                              }
                            });
                          },
                        ),
                        const Divider(
                          color: CustomColors.background,
                        ),
                        CheckboxListTile(
                          controlAffinity: ListTileControlAffinity.leading,
                          side: const BorderSide(color: CustomColors.surface),
                          activeColor: CustomColors.primary70,
                          checkColor: CustomColors.primaryDark,
                          title: Text(
                            alternativa2,
                            textScaler: const TextScaler.linear(1.0),
                            style: const TextStyle(
                                fontSize: 18, color: CustomColors.surface),
                          ),
                          value: alter2check,
                          onChanged: (bool? value) {
                            setState(() {
                              alter2check = value!;
                              if (value == true) {
                                checkedCount = checkedCount + 1;
                                selectedAlternatives.add(alternativa2);
                              } else {
                                checkedCount = checkedCount - 1;
                                selectedAlternatives.remove(alternativa2);
                              }
                            });
                          },
                        ),
                        const Divider(
                          color: CustomColors.background,
                        ),
                        Visibility(
                          visible: alter3,
                          child: CheckboxListTile(
                            controlAffinity: ListTileControlAffinity.leading,
                            side: const BorderSide(color: CustomColors.surface),
                            activeColor: CustomColors.primary70,
                            checkColor: CustomColors.primaryDark,
                            title: Text(
                              alternativa3,
                              textScaler: const TextScaler.linear(1.0),
                              style: const TextStyle(
                                  fontSize: 18, color: CustomColors.surface),
                            ),
                            value: alter3check,
                            onChanged: (bool? value) {
                              setState(() {
                                alter3check = value!;
                                if (value == true) {
                                  checkedCount = checkedCount + 1;
                                  selectedAlternatives.add(alternativa3);
                                } else {
                                  checkedCount = checkedCount - 1;
                                  selectedAlternatives.remove(alternativa3);
                                }
                              });
                            },
                          ),
                        ),
                        Visibility(
                          visible: alter3,
                          child: const Divider(
                            color: CustomColors.background,
                          ),
                        ),
                        Visibility(
                          visible: alter4,
                          child: CheckboxListTile(
                            controlAffinity: ListTileControlAffinity.leading,
                            side: const BorderSide(color: CustomColors.surface),
                            activeColor: CustomColors.primary70,
                            checkColor: CustomColors.primaryDark,
                            title: Text(
                              alternativa4,
                              textScaler: const TextScaler.linear(1.0),
                              style: const TextStyle(
                                  fontSize: 18, color: CustomColors.surface),
                            ),
                            value: alter4check,
                            onChanged: (bool? value) {
                              setState(() {
                                alter4check = value!;
                                if (value == true) {
                                  checkedCount = checkedCount + 1;
                                  selectedAlternatives.add(alternativa4);
                                } else {
                                  checkedCount = checkedCount - 1;
                                  selectedAlternatives.remove(alternativa4);
                                }
                              });
                            },
                          ),
                        ),
                        Visibility(
                          visible: alter4,
                          child: const Divider(
                            color: CustomColors.background,
                          ),
                        ),
                        Visibility(
                          visible: alter5,
                          child: CheckboxListTile(
                            controlAffinity: ListTileControlAffinity.leading,
                            side: const BorderSide(color: CustomColors.surface),
                            activeColor: CustomColors.primary70,
                            checkColor: CustomColors.primaryDark,
                            title: Text(
                              alternativa5,
                              textScaler: const TextScaler.linear(1.0),
                              style: const TextStyle(
                                  fontSize: 18, color: CustomColors.surface),
                            ),
                            value: alter5check,
                            onChanged: (bool? value) {
                              setState(() {
                                alter5check = value!;
                                if (value == true) {
                                  checkedCount = checkedCount + 1;
                                  selectedAlternatives.add(alternativa5);
                                } else {
                                  checkedCount = checkedCount - 1;
                                  selectedAlternatives.remove(alternativa5);
                                }
                              });
                            },
                          ),
                        ),
                        Visibility(
                          visible: alter5,
                          child: const Divider(
                            color: CustomColors.background,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );

      case 2:
        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.all(20),
                    alignment: Alignment.centerLeft,
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Ahora que eliminaste algunas de las opciones tomate un tiempo para analizar las alternativas restantes y considerar sus consecuencias tanto positivas como negativas. Para ayudarte a esto te invitamos a tener en mente las siguientes preguntas para las alternativas restantes:\n\n',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                          TextSpan(
                              text:
                                  '¿Esta alternativa resuelve el problema? (me permite alcanzar las metas)\n'),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                          TextSpan(
                              text:
                                  '¿Puedo llevar a cabo esta alternativa? (tengo los recursos, habilidades, etc. para poder realizarla)\n'),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                          TextSpan(
                              text:
                                  '¿Qué efectos tendrá en mí esta alternativa tanto a corto como largo plazo? (identifico las posibles consecuencias positivas y negativas para mí)\n'),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                          TextSpan(
                              text:
                                  '¿Qué efectos tendrá en otrxs tanto a corto como a largo plazo? (identifico las posibles consecuencias positivas y negativas para quienes me rodean)\n\n'),
                          TextSpan(
                              text:
                                  'Las diferentes opciones tienen varias consecuencias, por lo que puede ser abrumador evaluar todas las posibilidades. Es esencial establecer las consecuencias más significativas o importantes para simplificar el proceso de evaluación.\n\n'),
                          TextSpan(
                              text:
                                  'Para ayudarte a ejercitar en este proceso te dejamos el siguiente espacio para que puedas escribir las consecuencias positivas y negativas de dos de tus alternativas de solución.'),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.all(10),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            margin: const EdgeInsets.only(left: 10),
                            child: const Text(
                              'Alternativa 1',
                              textAlign: TextAlign.start,
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 22, color: CustomColors.surface),
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              showDialog<String>(
                                context: context,
                                builder: (BuildContext context) => AlertDialog(
                                  shape: const RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(32.0))),
                                  title: const Text(
                                    'Alternativas de solución',
                                    textScaler: TextScaler.linear(1.0),
                                    style: TextStyle(
                                        fontSize: 20,
                                        color: CustomColors.primaryDark),
                                  ),
                                  content: const Text(
                                    'Los completaste en la actividad anterior: Generando alternativas de solución.\nPara esta actividad, te mostramos las dos primeras alternativas que escribiste en dicha actividad',
                                    textScaler: TextScaler.linear(1.0),
                                    style: TextStyle(
                                        color: CustomColors.primaryDark),
                                  ),
                                  actions: <Widget>[
                                    TextButton(
                                      onPressed: () =>
                                          Navigator.pop(context, 'Cerrar'),
                                      child: const Text(
                                        'Cerrar',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            color: CustomColors.primary),
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            },
                            child: Container(
                              width: size.width,
                              margin: const EdgeInsets.all(10),
                              padding: const EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  color: CustomColors.surface.withAlpha(30)),
                              child: Text(
                                selectedAlternatives[0],
                                textScaler: const TextScaler.linear(1.0),
                                style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18,
                                    color: CustomColors.surface),
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(
                                left: 10, right: 10, bottom: 0, top: 10),
                            alignment: Alignment.centerLeft,
                            child: const Text(
                              'Consecuencia positiva',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 18, color: CustomColors.surface),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              validator: (String? value) {
                                return (value == null || value == '')
                                    ? 'Campo vacío.'
                                    : null;
                              },
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: alter1positiva,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: 'Escribe tu respuesta...',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(
                                left: 10, right: 10, bottom: 0, top: 10),
                            alignment: Alignment.centerLeft,
                            child: const Text(
                              'Consecuencia negativa',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 18, color: CustomColors.surface),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              validator: (String? value) {
                                return (value == null || value == '')
                                    ? 'Campo vacío.'
                                    : null;
                              },
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: alter1negativa,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: 'Escribe tu respuesta...',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(left: 10, top: 50),
                            child: const Text(
                              'Alternativa 2',
                              textAlign: TextAlign.start,
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 22, color: CustomColors.surface),
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              showDialog<String>(
                                context: context,
                                builder: (BuildContext context) => AlertDialog(
                                  shape: const RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(32.0))),
                                  title: const Text(
                                    'Alternativas de solución',
                                    textScaler: TextScaler.linear(1.0),
                                    style: TextStyle(
                                        fontSize: 20,
                                        color: CustomColors.primaryDark),
                                  ),
                                  content: const Text(
                                    'Los completaste en la actividad anterior: Generando alternativas de solución.\nPara esta actividad, te mostramos las dos primeras alternativas que escribiste en dicha actividad',
                                    textScaler: TextScaler.linear(1.0),
                                    style: TextStyle(
                                        color: CustomColors.primaryDark),
                                  ),
                                  actions: <Widget>[
                                    TextButton(
                                      onPressed: () =>
                                          Navigator.pop(context, 'Cerrar'),
                                      child: const Text(
                                        'Cerrar',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            color: CustomColors.primary),
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            },
                            child: Container(
                              width: size.width,
                              margin: const EdgeInsets.all(10),
                              padding: const EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  color: CustomColors.surface.withAlpha(30)),
                              child: Text(
                                selectedAlternatives[1],
                                textScaler: const TextScaler.linear(1.0),
                                style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18,
                                    color: CustomColors.surface),
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(
                                left: 10, right: 10, bottom: 0, top: 10),
                            alignment: Alignment.centerLeft,
                            child: const Text(
                              'Consecuencia positiva',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 18, color: CustomColors.surface),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              validator: (String? value) {
                                return (value == null || value == '')
                                    ? 'Campo vacío.'
                                    : null;
                              },
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: alter2positiva,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: 'Escribe tu respuesta...',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(
                                left: 10, right: 10, bottom: 0, top: 10),
                            alignment: Alignment.centerLeft,
                            child: const Text(
                              'Consecuencia negativa',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 18, color: CustomColors.surface),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              validator: (String? value) {
                                return (value == null || value == '')
                                    ? 'Campo vacío.'
                                    : null;
                              },
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: alter2negativa,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: 'Escribe tu respuesta...',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 3:
        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.all(20),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'En este punto, y antes de optar por alguna de las alternativas te invitamos a tomarte un momento de reflexión y pensar en las siguientes preguntas:\n¿El problema efectivamente tiene solución?\n¿Existe una solución satisfactoria?\n¿Necesito más información antes de elegir una o más alternativas?\n¿Qué alternativa(s) debo elegir?\n\n',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                '¡Llegó el momento de tomar una decisión! A continuación te dejamos el siguiente espacio para que puedas contarnos si optaste por alguna de las siguientes alternativas:',
                          ),
                        ],
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        if (selectTwo == false || selectNone == false) {
                          selectOne = true;
                          selectTwo = false;
                          selectNone = false;
                          finalSeleccionada = selectedAlternatives[0];
                        }
                      });
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(10),
                      width: size.width / 1.1,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: selectOne
                            ? CustomColors.primary70
                            : CustomColors.surface,
                      ),
                      child: Text(
                        'Alternativa 1:\n' + selectedAlternatives[0],
                        textScaler: const TextScaler.linear(1.0),
                        style: const TextStyle(
                            fontSize: 16,
                            color: CustomColors.primaryDark,
                            height: 1.5),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        if (selectOne == false || selectNone == false) {
                          selectOne = false;
                          selectTwo = true;
                          selectNone = false;
                          finalSeleccionada = selectedAlternatives[1];
                        }
                      });
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(10),
                      width: size.width / 1.1,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: selectTwo
                            ? CustomColors.primary70
                            : CustomColors.surface,
                      ),
                      child: Text(
                        'Alternativa 2:\n' + selectedAlternatives[1],
                        textScaler: const TextScaler.linear(1.0),
                        style: const TextStyle(
                            fontSize: 16,
                            color: CustomColors.primaryDark,
                            height: 1.5),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        if (selectTwo == false || selectOne == false) {
                          selectOne = false;
                          selectTwo = false;
                          selectNone = true;
                          finalSeleccionada =
                              'No elegí ninguna de las dos alternativas anteriores.';
                        }
                      });
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(10),
                      width: size.width / 1.1,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: selectNone
                            ? CustomColors.primary70
                            : CustomColors.surface,
                      ),
                      child: const Text(
                        'No elegí ninguna de las dos alternativas anteriores.',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primaryDark,
                            height: 1.5),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );

      case 4:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Container(
                margin: const EdgeInsets.all(15),
                child: Column(
                  children: [
                    topStructure(),
                    Container(
                      margin: const EdgeInsets.all(10),
                      child: const FittedBox(
                        fit: BoxFit.fitWidth,
                        child: Text(
                          '¡Muy bien!',
                          textScaler: TextScaler.linear(1.0),
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              // color: CustomColors.primaryDark,
                              fontSize: 24),
                        ),
                      ),
                    ),
                    const Text(
                      'Tenemos una recompensa para ti:',
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(fontSize: 16),
                      textAlign: TextAlign.center,
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 40, bottom: 10),
                      decoration: BoxDecoration(
                          color: CustomColors.lightContainer,
                          border:
                              Border.all(width: 2, color: CustomColors.surface),
                          borderRadius: BorderRadius.circular(15)),
                      child: Row(
                        children: [
                          const Image(
                            width: 100,
                            height: 120,
                            fit: BoxFit.scaleDown,
                            image: AssetImage(
                                'assets/images/cambiando-forma-icon.png'),
                          ),
                          Container(
                            margin: const EdgeInsets.only(left: 10),
                            child: const Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Actividad complementaria:',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      fontSize: 13,
                                      color: CustomColors.primaryDark),
                                ),
                                Text(
                                  'Cambiando la forma en\nla que nos hablamos',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16,
                                      color: CustomColors.primaryDark),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 10, bottom: 10),
                      decoration: BoxDecoration(
                          color: CustomColors.lightContainer,
                          border:
                              Border.all(width: 2, color: CustomColors.surface),
                          borderRadius: BorderRadius.circular(15)),
                      child: Row(
                        children: [
                          const Image(
                            width: 100,
                            height: 120,
                            fit: BoxFit.scaleDown,
                            image: AssetImage('assets/images/m3a7.png'),
                          ),
                          Container(
                            margin: const EdgeInsets.only(left: 10),
                            child: const Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Trabajo por mi bienestar:',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      fontSize: 13,
                                      color: CustomColors.primaryDark),
                                ),
                                Text(
                                  'Puedo tomar decisiones',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16,
                                      color: CustomColors.primaryDark),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );

      default:
        return Container();
    }
  }
}
