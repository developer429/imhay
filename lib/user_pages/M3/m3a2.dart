// ignore_for_file: prefer_typing_uninitialized_variables

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:seguimiento/colors.dart';
import '../../firebase.dart';
import '../MainPage.dart';

class M3A2 extends StatefulWidget {
  const M3A2({
    Key? key,
  }) : super(key: key);

  @override
  State<M3A2> createState() => _M3A2State();
}

class _M3A2State extends State<M3A2> {
  String uid = FirebaseAuth.instance.currentUser!.uid;
  bool loadingData = true;
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('page-control');
  String activeStepData = '';
  void getActivePageStored() {
    collectionReference.doc(uid).get().then((value) {
      var fields = value;
      activeStepData = fields['M3A2'];
      setState(() {
        if (int.parse(activeStepData) < 13) {
          activeStep = int.parse(activeStepData) - 1;
          Future.delayed(const Duration(milliseconds: 500), () {
            setState(() {
              loadingData = false;
            });
          });
        }
      });
    });
  }

  ScrollController scrollController = ScrollController();
  bool favorite = false;
  CollectionReference favoriteData =
      FirebaseFirestore.instance.collection('favoritos');
  void getFavData() {
    favoriteData.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        favorite = fields['M3A2'];
      });
    }).catchError((err) {
      //print('Error: $err');
    });
  }

  int activeStep = 0;
  int acertado = 0;
  var timeStart;
  @override
  void initState() {
    super.initState();
    getActivePageStored();

    getFavData();
    var tiempo = DateTime.now();
    timeStart = tiempo;
    FirestoreService().setTime(tiempo.toString() + ' - START - M3A2',
        'Comenzó M3A2', '0 minutos (comenzando)');
  }

  @override
  void dispose() {
    super.dispose();
  }

  bool correct = false;
  bool false1 = false;
  bool false2 = false;
  bool active1 = false;
  bool active2 = false;
  bool active3 = false;
  bool active4 = false;
  bool active5 = false;
  bool active6 = false;
  bool active7 = false;
  bool active8 = false;
  bool active9 = false;
  bool active10 = false;
  bool active11 = false;
  bool active12 = false;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        active1 = true;
        active2 = false;
        active3 = false;
        active4 = false;
        active5 = false;
        active6 = false;
        active7 = false;
        active8 = false;
        active9 = false;
        active10 = false;
        active11 = false;
        active12 = false;
        break;
      case 1:
        active1 = true;
        active2 = true;
        active3 = false;
        active4 = false;
        active5 = false;
        active6 = false;
        active7 = false;
        active8 = false;
        active9 = false;
        active10 = false;
        active11 = false;
        active12 = false;

        break;
      case 2:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = false;
        active5 = false;
        active6 = false;
        active7 = false;
        active8 = false;
        active9 = false;
        active10 = false;
        active11 = false;
        active12 = false;

        break;
      case 3:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = false;
        active6 = false;
        active7 = false;
        active8 = false;
        active9 = false;
        active10 = false;
        active11 = false;
        active12 = false;

        break;
      case 4:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = true;
        active6 = false;
        active7 = false;
        active8 = false;
        active9 = false;
        active10 = false;
        active11 = false;
        active12 = false;

        break;
      case 5:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = true;
        active6 = true;
        active7 = false;
        active8 = false;
        active9 = false;
        active10 = false;
        active11 = false;
        active12 = false;

        break;
      case 6:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = true;
        active6 = true;
        active7 = true;
        active8 = false;
        active9 = false;
        active10 = false;
        active11 = false;
        active12 = false;

        break;
      case 7:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = true;
        active6 = true;
        active7 = true;
        active8 = true;
        active9 = false;
        active10 = false;
        active11 = false;
        active12 = false;

        break;
      case 8:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = true;
        active6 = true;
        active7 = true;
        active8 = true;
        active9 = true;
        active10 = false;
        active11 = false;
        active12 = false;

        break;
      case 9:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = true;
        active6 = true;
        active7 = true;
        active8 = true;
        active9 = true;
        active10 = true;
        active11 = false;
        active12 = false;

        break;
      case 10:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = true;
        active6 = true;
        active7 = true;
        active8 = true;
        active9 = true;
        active10 = true;
        active11 = true;
        active12 = false;

        break;
      case 11:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = true;
        active6 = true;
        active7 = true;
        active8 = true;
        active9 = true;
        active10 = true;
        active11 = true;
        active12 = true;

        break;
      default:
    }
    return PopScope(
      canPop: false,
      onPopInvokedWithResult: (bool didPop, result) {
        if (didPop) {
          return;
        }
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(32.0))),
            title: const Text(
              '¿Estás seguro que deseas salir de la actividad?',
              textScaler: TextScaler.linear(1.0),
              style: TextStyle(color: CustomColors.primaryDark),
            ),
            content: const Text(
              'Tus avances quedarán guardados',
              textScaler: TextScaler.linear(1.0),
              style: TextStyle(color: CustomColors.primaryDark),
            ),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: const Text(
                  'Continuar con la actividad',
                  textScaler: TextScaler.linear(1.0),
                  style: TextStyle(color: CustomColors.primary),
                ),
              ),
              TextButton(
                onPressed: () {
                  FirestoreService()
                      .updateCurrentPage('M3A2', (activeStep + 1).toString());
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute<void>(
                          builder: (BuildContext context) => const MainPage(
                                activePage: 0,
                              )),
                      (route) => false);
                },
                child: const Text(
                  'Salir',
                  textScaler: TextScaler.linear(1.0),
                  style: TextStyle(color: CustomColors.error),
                ),
              ),
            ],
          ),
        );
      },
      child: loadingData
          ? Container(
              width: size.width,
              height: size.height,
              color: CustomColors.primaryDark,
              child: const Center(
                  child: CircularProgressIndicator(
                color: CustomColors.tertiary70,
              )))
          : Container(
              color: Colors.black,
              child: SafeArea(
                child: Scaffold(
                  backgroundColor:
                      active12 ? CustomColors.greenEnd : CustomColors.primary3,
                  body: Center(
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              width: size.width / 12,
                              height: 5,
                              color: active1
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 12,
                              height: 5,
                              color: active2
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 12,
                              height: 5,
                              color: active3
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 12,
                              height: 5,
                              color: active4
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 12,
                              height: 5,
                              color: active5
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 12,
                              height: 5,
                              color: active6
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 12,
                              height: 5,
                              color: active7
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 12,
                              height: 5,
                              color: active8
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 12,
                              height: 5,
                              color: active9
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 12,
                              height: 5,
                              color: active10
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 12,
                              height: 5,
                              color: active11
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 12,
                              height: 5,
                              color: active12
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                          ],
                        ),
                        content(),
                        Container(
                          margin: const EdgeInsets.only(top: 10, bottom: 10),
                          child: bottomStructure(),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
    );
  }

  Widget topStructure() {
    Size size = MediaQuery.of(context).size;
    return Container(
      width: size.width,
      height: active12 ? null : size.height / 3,
      color: active12 ? CustomColors.greenList : CustomColors.primary60,
      child: Column(
        children: [
          Row(
            children: [
              active12
                  ? Container()
                  : Container(
                      alignment: Alignment.topRight,
                      child: IconButton(
                        onPressed: () {
                          showDialog(
                            context: context,
                            builder: (context) => AlertDialog(
                              shape: const RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(32.0))),
                              title: const Text(
                                '¿Estás seguro que deseas salir de la actividad?',
                                textScaler: TextScaler.linear(1.0),
                                style:
                                    TextStyle(color: CustomColors.primaryDark),
                              ),
                              content: const Text(
                                'Tus avances quedarán guardados',
                                textScaler: TextScaler.linear(1.0),
                                style:
                                    TextStyle(color: CustomColors.primaryDark),
                              ),
                              actions: [
                                TextButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                  child: const Text(
                                    'Continuar con la actividad',
                                    textScaler: TextScaler.linear(1.0),
                                    style:
                                        TextStyle(color: CustomColors.primary),
                                  ),
                                ),
                                TextButton(
                                  onPressed: () {
                                    String tiempo = DateTime.now().toString();
                                    FirestoreService().setTime(
                                        tiempo + ' - CLOSED - M3A2',
                                        'Salió de M3A2 sin terminar',
                                        '0 minutos (sin terminar)');
                                    FirestoreService().updateCurrentPage(
                                        'M3A2', (activeStep + 1).toString());
                                    Navigator.pushAndRemoveUntil(
                                        context,
                                        MaterialPageRoute<void>(
                                            builder: (BuildContext context) =>
                                                const MainPage(
                                                  activePage: 0,
                                                )),
                                        (route) => false);
                                  },
                                  child: const Text(
                                    'Salir',
                                    textScaler: TextScaler.linear(1.0),
                                    style: TextStyle(color: CustomColors.error),
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                        icon: const Icon(
                          Icons.close,
                          color: CustomColors.primaryDark,
                        ),
                      ),
                    ),
              const Spacer(),
              active12 ? Container() : favorito(),
            ],
          ),
          Text(
            headerText(),
            textScaler: const TextScaler.linear(1.0),
            textAlign: TextAlign.center,
            style: const TextStyle(
              color: CustomColors.primaryDark,
              fontSize: 24,
            ),
          ),
          active12 ? Container() : const Spacer(),
          active12
              ? header()
              : Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    active1 ? header() : Container(),
                    headerTitleText(),
                  ],
                ),
        ],
      ),
    );
  }

  Widget favorito() {
    return StreamBuilder<QuerySnapshot>(
        stream: FirestoreService().favoritos(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasData) {
            return SizedBox(
              width: 50,
              height: 50,
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (context, index) {
                    var document = snapshot.data?.docs[index];
                    if (document != null && document.id == uid) {
                      switch (document['M3A2']) {
                        case true:
                          return IconButton(
                              icon: Icon(MdiIcons.star,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('M3A2', false)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Eliminado de favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                        case false:
                          return IconButton(
                              icon: Icon(MdiIcons.starOutline,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('M3A2', true)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Agregado a favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                      }
                    } else {
                      return Container();
                    }
                    return null;
                  }),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(
                color: Colors.red,
              ),
            );
          }
        });
  }

  Widget bottomStructure() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        nextButton(),
      ],
    );
  }

  Widget nextButton() {
    Size size = MediaQuery.of(context).size;
    switch (activeStep) {
      case 0:
        return SizedBox(
          width: size.width / 1.2,
          child: ElevatedButton(
            style: ButtonStyle(
              backgroundColor: WidgetStateProperty.all<Color>(
                  active12 ? CustomColors.primary : CustomColors.greenButton),
              shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                ),
              ),
            ),
            onPressed: () {
              if (activeStep < 11) {
                setState(() {
                  correct = false;
                  false1 = false;
                  false2 = false;
                  activeStep++;
                });
              } else {
                FirestoreService().terminaSesion(
                    '3',
                    '2',
                    'Leyó la introducción → Acertó a ' +
                        acertado.toString() +
                        ' de las 10 preguntas → Terminó la actividad 15');
                var tiempo = DateTime.now();
                var difTiempo = tiempo.difference(timeStart);
                FirestoreService().setTime(
                    tiempo.toString() + ' - ENDED - M3A2',
                    'Terminó M3A2',
                    difTiempo.inMinutes.toString() + ' minutos');
                FirestoreService().updateCurrentPage('M3A2', '1');
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute<void>(
                        builder: (BuildContext context) => const MainPage(
                              activePage: 0,
                            )),
                    (route) => false);
              }
            },
            child: Text(
              buttonText(),
              textScaler: const TextScaler.linear(1.0),
              style: TextStyle(
                color:
                    active12 ? CustomColors.surface : CustomColors.primaryDark,
              ),
            ),
          ),
        );

      case 11:
        return SizedBox(
          width: size.width / 1.2,
          child: ElevatedButton(
            style: ButtonStyle(
              backgroundColor: WidgetStateProperty.all<Color>(
                  active12 ? CustomColors.primary : CustomColors.greenButton),
              shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                ),
              ),
            ),
            onPressed: () {
              if (activeStep < 11) {
                setState(() {
                  correct = false;
                  false1 = false;
                  false2 = false;
                  activeStep++;
                });
              } else {
                FirestoreService().terminaSesion(
                    '3',
                    '2',
                    'Leyó la introducción → Acertó a ' +
                        acertado.toString() +
                        ' de las 10 preguntas → Terminó la actividad 15');
                var tiempo = DateTime.now();
                var difTiempo = tiempo.difference(timeStart);
                FirestoreService().setTime(
                    tiempo.toString() + ' - ENDED - M3A2',
                    'Terminó M3A2',
                    difTiempo.inMinutes.toString() + ' minutos');
                FirestoreService().updateCurrentPage('M3A2', '1');
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute<void>(
                        builder: (BuildContext context) => const MainPage(
                              activePage: 0,
                            )),
                    (route) => false);
              }
            },
            child: Text(
              buttonText(),
              textScaler: const TextScaler.linear(1.0),
              style: TextStyle(
                color:
                    active12 ? CustomColors.surface : CustomColors.primaryDark,
              ),
            ),
          ),
        );

      default:
        return Container();
    }
  }

  Widget header() {
    Size size = MediaQuery.of(context).size;
    switch (activeStep) {
      case 0:
        return Image(
          width: kIsWeb ? 100 : size.width / 3,
          height: kIsWeb ? 100 : size.width / 3,
          fit: BoxFit.scaleDown,
          image: AssetImage(imageHeader()),
        );
      case 11:
        return Image(
          width: size.width / 8,
          height: size.width / 8,
          fit: BoxFit.scaleDown,
          image: AssetImage(imageHeader()),
        );
      default:
        return Container();
    }
  }

  Widget headerTitleText() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        return Container();

      case 11:
        return Container();
      default:
        return Container(
          margin: const EdgeInsets.only(bottom: 20),
          width: size.width / 1.2,
          child: Text(
            tituloHeader(),
            textScaler: const TextScaler.linear(1.0),
            textAlign: TextAlign.center,
            style: const TextStyle(
                fontSize: 18,
                color: CustomColors.primaryDark,
                fontWeight: FontWeight.bold),
          ),
        );
    }
  }

  String respuestaCorrecta() {
    switch (activeStep) {
      case 1:
        return '"Todo o nada"';
      case 2:
        return '"Poner etiquetas"';
      case 3:
        return '"Lectura de mentes"';
      case 4:
        return '"Tomarlo de forma personal"';
      case 5:
        return '"Descontar lo positivo"';
      case 6:
        return '"Debería o tengo que..."';
      case 7:
        return '"Filtro mental"';
      case 8:
        return '"Sobregeneralización"';
      case 9:
        return '"Pensamiento emocional"';
      case 10:
        return '"Predecir el futuro"';

      default:
        return '';
    }
  }

  void modalRespuesta(response) {
    Size size = MediaQuery.of(context).size;

    switch (response) {
      case 0:
        showModalBottomSheet<void>(
          isDismissible: false,
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(40), topRight: Radius.circular(40)),
          ),
          context: context,
          builder: (BuildContext context) {
            return SizedBox(
              height: 200,
              child: Center(
                child: Column(
                  children: <Widget>[
                    Container(
                      margin: const EdgeInsets.all(10),
                      width: 50,
                      height: 5,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: CustomColors.surface,
                      ),
                      child: const Text(''),
                    ),
                    Container(
                      margin: const EdgeInsets.all(10),
                      width: size.width / 1.1,
                      child: Row(
                        children: [
                          Container(
                            margin: const EdgeInsets.only(right: 10),
                            child: Icon(
                              MdiIcons.closeCircle,
                              size: 30,
                            ),
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              RichText(
                                text: const TextSpan(
                                  text: 'Respuesta incorrecta.\n',
                                  style: TextStyle(
                                      fontSize: 16,
                                      height: 1.5,
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold),
                                  children: <TextSpan>[
                                    TextSpan(
                                        text: 'La respuesta correcta es:',
                                        style: TextStyle(
                                            fontWeight: FontWeight.normal)),
                                  ],
                                ),
                              ),
                              Text(
                                respuestaCorrecta(),
                                textScaler: const TextScaler.linear(1.0),
                                style: const TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.all(10),
                      width: size.width / 1.2,
                      child: ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor: WidgetStateProperty.all<Color>(
                              CustomColors.error),
                          shape:
                              WidgetStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18.0),
                            ),
                          ),
                        ),
                        onPressed: () {
                          if (activeStep < 11) {
                            setState(() {
                              activeStep++;
                            });
                            Navigator.pop(context);
                          }
                        },
                        child: Text(
                          buttonText(),
                          textScaler: const TextScaler.linear(1.0),
                          style: const TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        );
        break;
      case 1:
        showModalBottomSheet<void>(
          isDismissible: false,
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(40), topRight: Radius.circular(40)),
          ),
          context: context,
          builder: (BuildContext context) {
            return SizedBox(
              height: 200,
              child: Center(
                child: Column(
                  children: <Widget>[
                    Container(
                      margin: const EdgeInsets.all(10),
                      width: 50,
                      height: 5,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: const Color.fromARGB(255, 218, 218, 218),
                      ),
                      child: const Text(''),
                    ),
                    const Spacer(),
                    Container(
                      margin: const EdgeInsets.all(10),
                      width: size.width / 1.1,
                      child: Row(
                        children: [
                          Container(
                            margin: const EdgeInsets.only(right: 10),
                            child: Icon(
                              MdiIcons.checkCircle,
                              size: 30,
                            ),
                          ),
                          const Text(
                            'Respuesta correcta',
                            textScaler: TextScaler.linear(1.0),
                            style: TextStyle(fontSize: 16),
                          ),
                        ],
                      ),
                    ),
                    const Spacer(),
                    Container(
                      margin: const EdgeInsets.all(10),
                      width: size.width / 1.2,
                      child: ElevatedButton(
                        style: ButtonStyle(
                          backgroundColor: WidgetStateProperty.all<Color>(
                              CustomColors.primary),
                          shape:
                              WidgetStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18.0),
                            ),
                          ),
                        ),
                        onPressed: () {
                          if (activeStep < 11) {
                            setState(() {
                              activeStep++;
                            });
                            Navigator.pop(context);
                          }
                        },
                        child: Text(
                          buttonText(),
                          textScaler: const TextScaler.linear(1.0),
                          style: const TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        );
        break;
      default:
    }
  }

  String tituloHeader() {
    switch (activeStep) {
      case 1:
        return '“Si no les caigo bien a todxs mis compañerxs entonces soy una persona desagradable”';
      case 2:
        return '“No importa cómo me haya ido antes, como me saqué una mala nota en la prueba significa que soy tontx”';
      case 3:
        return '“Le dije a mis papás que no me fue bien en la presentación, están pensando que soy flojx y que no me importan mis estudios”';
      case 4:
        return '“Mi amiga estaba callada durante el almuerzo, debo haber hecho algo que le molestó”';
      case 5:
        return '“Si me fue bien en la presentación fue porque tuve suerte y la profe andaba buena onda”';
      case 6:
        return '“Tengo que tener buenas notas siempre, de lo contrario no sirvo para esta carrera”';
      case 7:
        return '“Mi amigx se demoró mucho en responder el mensaje que le envié, eso significa que no le importo”';
      case 8:
        return '“Como les dije a mis amigxs que no podría ir a la fiesta ahora no me van a invitar nunca más”';
      case 9:
        return '“Aunque me está yendo bien en el semestre, siento que soy un fracaso”';
      case 10:
        return '“Tuve una discusión con mi pololx, la relación terminará de forma terrible”';

      default:
        return '';
    }
  }

  String imageHeader() {
    switch (activeStep) {
      case 0:
        return 'assets/images/m3a2.png';

      case 11:
        return 'assets/images/end.png';

      default:
        return 'assets/images/m3a2.png';
    }
  }

  String headerText() {
    switch (activeStep) {
      case 11:
        return '';
      default:
        return 'Identificando errores del pensamiento';
    }
  }

  String buttonText() {
    switch (activeStep) {
      case 0:
        return 'Comencemos';
      case 10:
        return 'Finalizar';
      case 11:
        return 'Continuar con el programa';
      default:
        return 'Siguiente';
    }
  }

  Widget content() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });
        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.all(20),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Ahora que ya conoces qué son los errores del pensamiento y cuáles son los más comunes, te invitamos a realizar el siguiente ejercicio.\n\n',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                              text:
                                  '¿En qué consiste? Deberás identificar el error de pensamiento que se presenta en cada frase.'),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );

      case 1:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.only(bottom: 50),
                    child: topStructure(),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        modalRespuesta(0);
                      });
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(10),
                      width: size.width / 1.2,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: CustomColors.background,
                      ),
                      child: const Text(
                        'Pensamiento emocional',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primaryDark,
                            height: 1.5),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        modalRespuesta(1);
                        acertado++;
                      });
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: CustomColors.background,
                      ),
                      width: size.width / 1.2,
                      child: const Text(
                        'Todo o nada',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primaryDark,
                            height: 1.5),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        modalRespuesta(0);
                      });
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(10),
                      width: size.width / 1.2,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: CustomColors.background,
                      ),
                      child: const Text(
                        'Lectura de mentes',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primaryDark,
                            height: 1.5),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 2:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.only(bottom: 50),
                    child: topStructure(),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        modalRespuesta(0);
                      });
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(10),
                      width: size.width / 1.2,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: CustomColors.background,
                      ),
                      child: const Text(
                        'Tomarlo de forma personal',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primaryDark,
                            height: 1.5),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        modalRespuesta(0);
                      });
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(10),
                      width: size.width / 1.2,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: CustomColors.background,
                      ),
                      child: const Text(
                        'Debería o tengo que',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primaryDark,
                            height: 1.5),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        modalRespuesta(1);
                        acertado++;
                      });
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: CustomColors.background,
                      ),
                      width: size.width / 1.2,
                      child: const Text(
                        'Poner etiquetas',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primaryDark,
                            height: 1.5),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 3:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.only(bottom: 50),
                    child: topStructure(),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        modalRespuesta(1);
                        acertado++;
                      });
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: CustomColors.background,
                      ),
                      width: size.width / 1.2,
                      child: const Text(
                        'Lectura de mentes',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primaryDark,
                            height: 1.5),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        modalRespuesta(0);
                      });
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(10),
                      width: size.width / 1.2,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: CustomColors.background,
                      ),
                      child: const Text(
                        'Tomarlo de forma personal',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primaryDark,
                            height: 1.5),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        modalRespuesta(0);
                      });
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(10),
                      width: size.width / 1.2,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: CustomColors.background,
                      ),
                      child: const Text(
                        'Pensamiento emocional',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primaryDark,
                            height: 1.5),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 4:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.only(bottom: 50),
                    child: topStructure(),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        modalRespuesta(1);
                        acertado++;
                      });
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: CustomColors.background,
                      ),
                      width: size.width / 1.2,
                      child: const Text(
                        'Tomarlo de forma personal',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primaryDark,
                            height: 1.5),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        modalRespuesta(0);
                      });
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(10),
                      width: size.width / 1.2,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: CustomColors.background,
                      ),
                      child: const Text(
                        'Lectura de mentes',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primaryDark,
                            height: 1.5),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        modalRespuesta(0);
                      });
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(10),
                      width: size.width / 1.2,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: CustomColors.background,
                      ),
                      child: const Text(
                        'Pensamiento emocional',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primaryDark,
                            height: 1.5),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 5:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.only(bottom: 50),
                    child: topStructure(),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        modalRespuesta(0);
                      });
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(10),
                      width: size.width / 1.2,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: CustomColors.background,
                      ),
                      child: const Text(
                        'Filtro mental',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primaryDark,
                            height: 1.5),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        modalRespuesta(0);
                      });
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(10),
                      width: size.width / 1.2,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: CustomColors.background,
                      ),
                      child: const Text(
                        'Sobregeneralizar',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primaryDark,
                            height: 1.5),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        modalRespuesta(1);
                        acertado++;
                      });
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: CustomColors.background,
                      ),
                      width: size.width / 1.2,
                      child: const Text(
                        'Descontar lo positivo',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primaryDark,
                            height: 1.5),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 6:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.only(bottom: 50),
                    child: topStructure(),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        modalRespuesta(0);
                      });
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(10),
                      width: size.width / 1.2,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: CustomColors.background,
                      ),
                      child: const Text(
                        'Sobregeneralizar',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primaryDark,
                            height: 1.5),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        modalRespuesta(0);
                      });
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(10),
                      width: size.width / 1.2,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: CustomColors.background,
                      ),
                      child: const Text(
                        'Descontar lo positivo',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primaryDark,
                            height: 1.5),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        modalRespuesta(1);
                        acertado++;
                      });
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: CustomColors.background,
                      ),
                      width: size.width / 1.2,
                      child: const Text(
                        'Debería o tengo que...',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primaryDark,
                            height: 1.5),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 7:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.only(bottom: 50),
                    child: topStructure(),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        modalRespuesta(0);
                      });
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(10),
                      width: size.width / 1.2,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: CustomColors.background,
                      ),
                      child: const Text(
                        'Descontar lo positivo',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primaryDark,
                            height: 1.5),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        modalRespuesta(1);
                        acertado++;
                      });
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: CustomColors.background,
                      ),
                      width: size.width / 1.2,
                      child: const Text(
                        'Filtro mental',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primaryDark,
                            height: 1.5),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        modalRespuesta(0);
                      });
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(10),
                      width: size.width / 1.2,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: CustomColors.background,
                      ),
                      child: const Text(
                        'Todo o nada',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primaryDark,
                            height: 1.5),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 8:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.only(bottom: 50),
                    child: topStructure(),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        modalRespuesta(0);
                      });
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(10),
                      width: size.width / 1.2,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: CustomColors.background,
                      ),
                      child: const Text(
                        'Filtro mental',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primaryDark,
                            height: 1.5),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        modalRespuesta(0);
                      });
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(10),
                      width: size.width / 1.2,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: CustomColors.background,
                      ),
                      child: const Text(
                        'Descontar lo positivo',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primaryDark,
                            height: 1.5),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        modalRespuesta(1);
                        acertado++;
                      });
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: CustomColors.background,
                      ),
                      width: size.width / 1.2,
                      child: const Text(
                        'Sobregeneralización',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primaryDark,
                            height: 1.5),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 9:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.only(bottom: 50),
                    child: topStructure(),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        modalRespuesta(1);
                        acertado++;
                      });
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: CustomColors.background,
                      ),
                      width: size.width / 1.2,
                      child: const Text(
                        'Pensamiento emocional',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primaryDark,
                            height: 1.5),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        modalRespuesta(0);
                      });
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(10),
                      width: size.width / 1.2,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: CustomColors.background,
                      ),
                      child: const Text(
                        'Poner etiquetas',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primaryDark,
                            height: 1.5),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        modalRespuesta(0);
                      });
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(10),
                      width: size.width / 1.2,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: CustomColors.background,
                      ),
                      child: const Text(
                        'Debería o tengo que',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primaryDark,
                            height: 1.5),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 10:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.only(bottom: 50),
                    child: topStructure(),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        modalRespuesta(0);
                      });
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(10),
                      width: size.width / 1.2,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: CustomColors.background,
                      ),
                      child: const Text(
                        'Filtro emocional',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primaryDark,
                            height: 1.5),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        modalRespuesta(1);
                        acertado++;
                      });
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: CustomColors.background,
                      ),
                      width: size.width / 1.2,
                      child: const Text(
                        'Predecir el futuro',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primaryDark,
                            height: 1.5),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        modalRespuesta(0);
                      });
                    },
                    child: Container(
                      margin: const EdgeInsets.all(10),
                      padding: const EdgeInsets.all(10),
                      width: size.width / 1.2,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: CustomColors.background,
                      ),
                      child: const Text(
                        'Pensamiento emocional',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primaryDark,
                            height: 1.5),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 11:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Container(
                margin: const EdgeInsets.all(15),
                child: Column(
                  children: [
                    topStructure(),
                    Container(
                      margin: const EdgeInsets.all(10),
                      child: const FittedBox(
                        fit: BoxFit.fitWidth,
                        child: Text(
                          '¡Muy bien!',
                          textScaler: TextScaler.linear(1.0),
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              // color: CustomColors.primaryDark,
                              fontSize: 24),
                        ),
                      ),
                    ),
                    const Text(
                      'Tenemos una recompensa para ti:',
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(fontSize: 16),
                      textAlign: TextAlign.center,
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 40, bottom: 10),
                      decoration: BoxDecoration(
                          color: CustomColors.lightContainer,
                          border:
                              Border.all(width: 2, color: CustomColors.surface),
                          borderRadius: BorderRadius.circular(15)),
                      child: Row(
                        children: [
                          const Image(
                            width: 100,
                            height: 120,
                            fit: BoxFit.scaleDown,
                            image: AssetImage('assets/images/selva-icon.png'),
                          ),
                          Container(
                            margin: const EdgeInsets.only(left: 10),
                            child: const Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Actividad complementaria:',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      fontSize: 13,
                                      color: CustomColors.primaryDark),
                                ),
                                Text(
                                  'Sonidos de la naturaleza:\nSelva tropical',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16,
                                      color: CustomColors.primaryDark),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 10, bottom: 10),
                      decoration: BoxDecoration(
                          color: CustomColors.lightContainer,
                          border:
                              Border.all(width: 2, color: CustomColors.surface),
                          borderRadius: BorderRadius.circular(15)),
                      child: Row(
                        children: [
                          const Image(
                            width: 100,
                            height: 120,
                            fit: BoxFit.scaleDown,
                            image: AssetImage('assets/images/viento-icon.png'),
                          ),
                          Container(
                            margin: const EdgeInsets.only(left: 10),
                            child: const Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Actividad complementaria:',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      fontSize: 13,
                                      color: CustomColors.primaryDark),
                                ),
                                Text(
                                  'Sonidos de la naturaleza:\nViento',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16,
                                      color: CustomColors.primaryDark),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      default:
        return Container();
    }
  }
}
