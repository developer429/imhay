// ignore_for_file:   avoid_unnecessary_containers, sized_box_for_whitespace, prefer_typing_uninitialized_variables

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:seguimiento/colors.dart';
import 'package:seguimiento/user_pages/MainPage.dart';
import '../chatlogics/chat_page.dart';

class ChateaPsicologo extends StatefulWidget {
  const ChateaPsicologo({
    Key? key,
  }) : super(key: key);

  @override
  State<ChateaPsicologo> createState() => _ChateaPsicologoState();
}

class _ChateaPsicologoState extends State<ChateaPsicologo> {
  String uid = FirebaseAuth.instance.currentUser!.uid;
  String psicologoAsignado = '';
  String nombrePsicologo = '';
  bool loadingData = true;
  bool activeChat = true;
  Color activeButton = CustomColors.primary;
  DateTime now = DateTime.now();

  CollectionReference userReference =
      FirebaseFirestore.instance.collection('usuarios');
  CollectionReference psiReference =
      FirebaseFirestore.instance.collection('psicologos');
  void getPsicologo() {
    userReference.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        psicologoAsignado = fields['psicologo'];
        psiReference.doc(fields['psicologo']).get().then((value) {
          var fields = value;
          setState(() {
            nombrePsicologo = fields['nombre'] + ' ' + fields['apellidos'];
            Future.delayed(const Duration(milliseconds: 1000), () {
              setState(() {
                loadingData = false;
              });
            });
          });
        });
      });
    });
  }

  ScrollController scrollController = ScrollController();
  int activeStep = 0;
  int upperBound = 1;
  String pasos = '';
  var timeStart;
  @override
  void initState() {
    super.initState();
    getPsicologo();
    if (now.hour > 17 || now.hour < 9) {
      activeChat = false;
      activeButton = CustomColors.surface.withAlpha(50);
    } else {
      activeChat = true;
      activeButton = CustomColors.primary;
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  bool active1 = false;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        active1 = true;
        break;

      default:
    }
    return loadingData
        ? Container(
            width: size.width,
            height: size.height,
            color: CustomColors.primaryDark,
            child: const Center(
                child: CircularProgressIndicator(
              color: CustomColors.surface,
            )))
        : Container(
            color: CustomColors.background,
            child: SafeArea(
              child: Scaffold(
                backgroundColor: CustomColors.background,
                body: Center(
                  child: Column(
                    children: [
                      content(),
                      Container(
                          margin: const EdgeInsets.only(top: 10, bottom: 10),
                          child: bottomStructure()),
                    ],
                  ),
                ),
              ),
            ),
          );
  }

  Widget topStructure() {
    return Container(
      child: Stack(
        children: [
          Column(
            children: [
              Row(
                children: [
                  Container(
                    alignment: Alignment.topRight,
                    child: IconButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      icon: const Icon(
                        Icons.arrow_back,
                        color: CustomColors.primaryDark,
                      ),
                    ),
                  ),
                ],
              ),
              Text(
                headerText(),
                textScaler: const TextScaler.linear(1.0),
                textAlign: TextAlign.center,
                style: const TextStyle(
                  color: CustomColors.primaryDark,
                  fontSize: 24,
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 20, bottom: 20),
                child: header(),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget bottomStructure() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        nextButton(),
      ],
    );
  }

  Widget nextButton() {
    Size size = MediaQuery.of(context).size;

    return Container(
      width: size.width / 1.2,
      child: ElevatedButton(
        style: ButtonStyle(
          backgroundColor: WidgetStateProperty.all<Color>(activeButton),
          shape: WidgetStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(18.0),
            ),
          ),
        ),
        onPressed: () async {
          activeChat
              ? Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ChatPage(
                      isVistaDetails: false,
                      isPsicologo: false,
                      arguments: ChatPageArguments(
                        peerId: psicologoAsignado,
                        peerNickname: nombrePsicologo,
                      ),
                    ),
                  ),
                )
              : showDialog(
                  context: context,
                  builder: (context) => AlertDialog(
                    shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(32.0))),
                    title: const Text(
                      'Fuera de horario',
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(color: CustomColors.primaryDark),
                    ),
                    content: const Text(
                      '¡Hola! en este momento el chat se encuentra fuera de horario. Vuelve a intentarlo de 9.00 a 18.00 hrs',
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(color: CustomColors.primaryDark),
                    ),
                    actions: [
                      TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: const Text(
                          'Volver',
                          textScaler: TextScaler.linear(1.0),
                          style: TextStyle(color: CustomColors.primary),
                        ),
                      ),
                    ],
                  ),
                );
        },
        child: Text(
          buttonText(),
          textScaler: const TextScaler.linear(1.0),
          style: const TextStyle(color: CustomColors.surface),
        ),
      ),
    );
  }

  Widget header() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      default:
        return Image(
          width: size.width / 3,
          height: size.width / 3,
          fit: BoxFit.scaleDown,
          image: AssetImage(imageHeader()),
        );
    }
  }

  String imageHeader() {
    switch (activeStep) {
      case 0:
        return 'assets/images/chatear.png';

      default:
        return ' ';
    }
  }

  String headerText() {
    switch (activeStep) {
      case 0:
        return 'Chatea con un psicólogx';

      default:
        return ' ';
    }
  }

  String buttonText() {
    switch (activeStep) {
      case 0:
        return 'Chatea Ahora';

      default:
        return ' ';
    }
  }

  Widget content() {
    switch (activeStep) {
      case 0:
        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 20, right: 20, left: 20, bottom: 5),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'El chat se encuentra disponible de lunes a viernes entre las 9:00 y las 18:00 hrs. En el chat podrás acceder a:\n',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                              text: '   ·  ',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(
                            text:
                                'Contención emocional en casos de que sientas un malestar intenso.\n',
                          ),
                          TextSpan(
                              text: '   ·  ',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(
                            text:
                                'Orientación para resolver dudas respecto al uso y realización de actividades en la app.\n',
                          ),
                          TextSpan(
                              text: '   ·  ',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(
                            text:
                                'Retroalimentación acerca de tu progreso en los módulos de Tu Programa.\n\n',
                          ),
                          TextSpan(
                            text:
                                'En caso de que te encuentres fuera de horario para chatear y requieras de ayuda urgente, recuerda que por medio de la app puedes acceder a otros recursos de ayuda.',
                          ),
                        ],
                      ),
                    ),
                  ),
                  Visibility(
                    visible: !activeChat,
                    child: Container(
                      margin: const EdgeInsets.only(
                          left: 20, right: 20, top: 30, bottom: 30),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          color: CustomColors.primary2),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Container(
                            margin: const EdgeInsets.only(
                                left: 10, right: 10, top: 10),
                            child: const Text(
                              'El chat no se encuentra disponible en este horario. Vuelve a intentarlo más tarde.',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(color: Colors.black),
                            ),
                          ),
                          TextButton(
                            onPressed: () {
                              Navigator.pushAndRemoveUntil(
                                  context,
                                  MaterialPageRoute<void>(
                                      builder: (BuildContext context) =>
                                          const MainPage(
                                            activePage: 2,
                                          )),
                                  (route) => false);
                            },
                            child: const Text(
                              'Otras opciones de ayuda',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(color: CustomColors.primary3),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );

      default:
        return Container();
    }
  }
}
