// ignore_for_file: file_names, avoid_print

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:seguimiento/colors.dart';
import 'package:seguimiento/user_pages/MainPage.dart';
import '../../firebase.dart';

class A3 extends StatefulWidget {
  const A3({
    Key? key,
  }) : super(key: key);

  @override
  State<A3> createState() => _A3State();
}

class _A3State extends State<A3> {
  String uid = FirebaseAuth.instance.currentUser!.uid;
  bool loadingData = true;
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('page-control');
  String activeStepData = '';
  void getActivePageStored() {
    collectionReference.doc(uid).get().then((value) {
      var fields = value;
      activeStepData = fields['A3'];
      setState(() {
        if (int.parse(activeStepData) < 6) {
          activeStep = int.parse(activeStepData) - 1;
          Future.delayed(const Duration(milliseconds: 500), () {
            setState(() {
              loadingData = false;
            });
          });
        }
      });
    });
  }

  ScrollController scrollController = ScrollController();

  CollectionReference diarioReference =
      FirebaseFirestore.instance.collection('A3-diario-procastinacion');
  void getData() {
    diarioReference.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        situacion.text = fields['situacion'];
        diario.text = fields['diario'];
      });
    }).catchError((err) {
      print('Error: $err');
    });
  }

  bool favorite = false;
  CollectionReference favoriteData =
      FirebaseFirestore.instance.collection('favoritos');
  void getFavData() {
    favoriteData.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        favorite = fields['A3'];
      });
    }).catchError((err) {
      print('Error: $err');
    });
  }

  final situacion = TextEditingController();
  final diario = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  int activeStep = 0;
  @override
  void initState() {
    super.initState();
    getActivePageStored();

    getData();

    getFavData();
  }

  @override
  void dispose() {
    super.dispose();
  }

  bool active1 = false;
  bool active2 = false;
  bool active3 = false;
  bool active4 = false;
  bool active5 = false;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    switch (activeStep) {
      case 0:
        active1 = true;
        active2 = false;
        active3 = false;
        active4 = false;
        active5 = false;
        break;
      case 1:
        active1 = true;
        active2 = true;
        active3 = false;
        active4 = false;
        active5 = false;

        break;
      case 2:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = false;
        active5 = false;

        break;
      case 3:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = false;

        break;
      case 4:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = true;

        break;

      default:
    }
    return PopScope(
      canPop: false,
      onPopInvokedWithResult: (bool didPop, result) {
        if (didPop) {
          FirestoreService()
              .updateCurrentPage('A3', (activeStep + 1).toString());
          return;
        }
      },
      child: loadingData
          ? Container(
              width: size.width,
              height: size.height,
              color: CustomColors.primaryDark,
              child: const Center(
                  child: CircularProgressIndicator(
                color: CustomColors.tertiary70,
              )))
          : Container(
              color: Colors.black,
              child: SafeArea(
                child: Scaffold(
                  backgroundColor: CustomColors.background,
                  body: Center(
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              width: size.width / 5,
                              height: 5,
                              color: active1
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 5,
                              height: 5,
                              color: active2
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 5,
                              height: 5,
                              color: active3
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 5,
                              height: 5,
                              color: active4
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 5,
                              height: 5,
                              color: active5
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                          ],
                        ),
                        content(),
                        Container(
                          margin: const EdgeInsets.only(top: 10, bottom: 10),
                          child: bottomStructure(),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
    );
  }

  Widget topStructure() {
    return Column(
      children: [
        Row(
          children: [
            Container(
              alignment: Alignment.topRight,
              child: IconButton(
                onPressed: () {
                  FirestoreService()
                      .updateCurrentPage('A3', (activeStep + 1).toString())
                      .then((value) => Navigator.of(context).pop());
                },
                icon: const Icon(
                  Icons.close,
                  color: CustomColors.primaryDark,
                ),
              ),
            ),
            const Spacer(),
            favorito(),
          ],
        ),
        Text(
          headerText(),
          textScaler: const TextScaler.linear(1.0),
          style: const TextStyle(
            color: CustomColors.primaryDark,
            fontSize: 24,
          ),
        ),
        Container(
          margin: const EdgeInsets.only(top: 20, bottom: 20),
          child: header(),
        ),
      ],
    );
  }

  Widget favorito() {
    return StreamBuilder<QuerySnapshot>(
        stream: FirestoreService().favoritos(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasData) {
            return SizedBox(
              width: 50,
              height: 50,
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (context, index) {
                    var document = snapshot.data?.docs[index];
                    if (document != null && document.id == uid) {
                      switch (document['A3']) {
                        case true:
                          return IconButton(
                              icon: Icon(MdiIcons.star,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('A3', false)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Eliminado de favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                        case false:
                          return IconButton(
                              icon: Icon(MdiIcons.starOutline,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService().setFavorito('A3', true).then(
                                    (value) => Fluttertoast.showToast(
                                        msg: "Agregado a favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                      }
                    } else {
                      return Container();
                    }
                    return null;
                  }),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(
                color: Colors.red,
              ),
            );
          }
        });
  }

  Widget bottomStructure() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        nextButton(),
      ],
    );
  }

  Widget nextButton() {
    Size size = MediaQuery.of(context).size;
    String empty = '-';
    if (activeStep >= 1 && activeStep < 5) {
      return SizedBox(
        width: size.width / 1,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        WidgetStateProperty.all<Color>(CustomColors.background),
                    shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: const BorderSide(color: CustomColors.primary)),
                    ),
                  ),
                  onPressed: () {
                    setState(() {
                      activeStep = activeStep - 1;
                    });
                  },
                  child: const Text(
                    'Anterior',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(color: CustomColors.primary),
                  )),
            ),
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor:
                      WidgetStateProperty.all<Color>(CustomColors.primary),
                  shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ),
                  ),
                ),
                onPressed: () {
                  if (activeStep < 4) {
                    if (activeStep == 3) {
                      if (_formKey.currentState!.validate()) {
                        FirestoreService()
                            .guardarRespuestasA3(
                              situacion.text.isEmpty
                                  ? empty
                                  : situacion.text.trim(),
                              diario.text.isEmpty ? empty : diario.text.trim(),
                            )
                            .then(
                              (value) => setState(() {
                                activeStep++;
                              }),
                            );
                      } else {
                        showDialog<String>(
                          context: context,
                          builder: (BuildContext context) => AlertDialog(
                            title: const Text(
                              'Error',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  color: Colors.red,
                                  fontWeight: FontWeight.bold),
                            ),
                            content: const Text(
                              'Complete los campos faltantes',
                              textScaler: TextScaler.linear(1.0),
                            ),
                            actions: <Widget>[
                              TextButton(
                                onPressed: () => Navigator.pop(
                                  context,
                                ),
                                child: const Text(
                                  'OK',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      color: CustomColors.primaryDark,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                        );
                      }
                    } else {
                      setState(() {
                        activeStep++;
                      });
                    }
                  } else {
                    FirestoreService().updateCurrentPage('A3', '1');

                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute<void>(
                            builder: (BuildContext context) => const MainPage(
                                  activePage: 1,
                                )),
                        (route) => false);
                  }
                },
                child: Text(
                  buttonText(),
                  textScaler: const TextScaler.linear(1.0),
                  style: const TextStyle(
                      color: CustomColors.background,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      return SizedBox(
        width: size.width / 1.2,
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor:
                WidgetStateProperty.all<Color>(CustomColors.primary),
            shape: WidgetStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              ),
            ),
          ),
          onPressed: () {
            if (activeStep < 4) {
              if (activeStep == 3) {
                if (_formKey.currentState!.validate()) {
                  FirestoreService()
                      .guardarRespuestasA3(
                        situacion.text.isEmpty ? empty : situacion.text.trim(),
                        diario.text.isEmpty ? empty : diario.text.trim(),
                      )
                      .then(
                        (value) => setState(() {
                          activeStep++;
                        }),
                      );
                } else {
                  showDialog<String>(
                    context: context,
                    builder: (BuildContext context) => AlertDialog(
                      title: const Text(
                        'Error',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            color: Colors.red, fontWeight: FontWeight.bold),
                      ),
                      content: const Text(
                        'Complete los campos faltantes',
                        textScaler: TextScaler.linear(1.0),
                      ),
                      actions: <Widget>[
                        TextButton(
                          onPressed: () => Navigator.pop(
                            context,
                          ),
                          child: const Text(
                            'OK',
                            textScaler: TextScaler.linear(1.0),
                            style: TextStyle(
                                color: CustomColors.primaryDark,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                  );
                }
              } else {
                setState(() {
                  activeStep++;
                });
              }
            } else {
              FirestoreService().updateCurrentPage('A3', '1');

              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute<void>(
                      builder: (BuildContext context) => const MainPage(
                            activePage: 1,
                          )),
                  (route) => false);
            }
            /*
            */
          },
          child: Text(
            buttonText(),
            textScaler: const TextScaler.linear(1.0),
            style: const TextStyle(color: CustomColors.background),
          ),
        ),
      );
    }
  }

  Widget header() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      default:
        return Image(
          width: size.width / 3,
          height: size.width / 3,
          fit: BoxFit.scaleDown,
          image: AssetImage(imageHeader()),
        );
    }
  }

  String imageHeader() {
    switch (activeStep) {
      case 0:
        return 'assets/images/superar-procastinacion-icon.png';

      default:
        return 'assets/images/superar-procastinacion-icon.png';
    }
  }

  String headerText() {
    switch (activeStep) {
      case 0:
        return 'Superando la procastinación';

      default:
        return 'Superando la procastinación';
    }
  }

  String buttonText() {
    switch (activeStep) {
      case 4:
        return 'Finalizar';

      default:
        return 'Siguiente ';
    }
  }

  Widget content() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 20, right: 20, left: 20, bottom: 20),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            '¿Te has encontrado posponiendo tareas que sabes que debes realizar?\n',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            fontWeight: FontWeight.bold,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                'De ser así, es posible que ya conozcas la procrastinación. La procrastinación es una conducta orientada por las emociones, la cual tiene principalmente dos objetivos. Por una parte, procrastinar permite alejarse de situaciones que resultan desagradables, aburridas o amenazantes. Por otra parte, la procrastinación nos acerca a cosas o situaciones que resultan más placenteras, estimulantes, menos desagradables, etc. Estos objetivos a su vez interactúan entre sí, dando pie a diversas situaciones o razones que aumentan la probabilidad de que pospongamos la realización de alguna actividad relevante. Entre estas razones se encuentran:\n\n',
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'No saber calcular los costos y beneficios de nuestras actividades a largo plazo, tomando decisiones que favorecen la gratificación inmediata o el beneficio a corto plazo.\n',
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Temor a equivocarnos o tomar malas decisiones, posponiendo las elecciones y quedándonos estancados.\n',
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Temor a no cumplir con los estándares (excesivamente) elevados que hemos establecido para nuestro desempeño académico.\n',
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Dejar de lado actividades relevantes porque nos parecen aburridas o poco interesantes o porque nos generan malestar emocional.\n\n',
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          TextSpan(
                            text:
                                'De esta forma, tendemos a procrastinar cuando:\n\n',
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          TextSpan(
                            text: '   1.  ',
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          TextSpan(
                            text:
                                'tenemos pocas expectativas de logro y/o recompensa en nuestras actividades o tareas;\n',
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          TextSpan(
                            text: '   2.  ',
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          TextSpan(
                            text:
                                'cuando cumplir con una tarea o responsabilidad no parece tener un beneficio a corto plazo; y/o\n',
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          TextSpan(
                            text: '   3.  ',
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          TextSpan(
                            text:
                                'cuando estamos expuestos a situaciones o estímulos que captan más fácilmente nuestra atención (ej. redes sociales, ver una serie, etc.).\n\n',
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          TextSpan(
                            text: 'Por otra parte, ',
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          TextSpan(
                            text:
                                'mientras más lejos se encuentre el plazo límite para nuestras actividades, más alta es la probabilidad de procrastinación. ',
                          ),
                          TextSpan(
                            text:
                                'Lo anterior debido a que frente a plazos más extensos, resulta más fácil autoconvencernos de que todavía tenemos tiempo suficiente y/o  que “trabajamos mejor bajo presión”. Estos factores, que muchas veces se suman a una mala organización del tiempo, pueden resultar en niveles elevados de estrés y un peor  desempeño académico (ej. entregas de trabajos de una menor calidad) e incluso pueden provocar problemas interpersonales (ej. discusiones con grupos de trabajo).',
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 1:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                      width: size.width,
                      margin: const EdgeInsets.only(
                          top: 15, left: 20, bottom: 5, right: 10),
                      child: const Text(
                        '¿Cómo disminuir la procrastinación estudiando en la universidad?',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                          fontSize: 22,
                          color: CustomColors.primary,
                        ),
                      )),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: const EdgeInsets.only(
                        top: 5, right: 20, left: 20, bottom: 5),
                    child: RichText(
                      text: const TextSpan(
                        text: '',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                'Dado que nuestra probabilidad de procrastinar depende de diversos factores (y de cómo estos interactúan entre sí), lo primero que debemos hacer es identificar qué factores tienen mayor influencia en nosotros. Esto es especialmente relevante cuando estamos procrastinando actividades sin una razón tan clara o evidente. Para lograr este objetivo te invitamos a construir un diario personal con el cual podrás contar con mayor autoconocimiento (reconocer cuándo y cómo procrastinas), y por lo tanto con mejores estrategias, para hacerle frente a tu procrastinación.\n\n',
                          ),
                          TextSpan(
                            text:
                                'En la construcción de este diario personal, te acompañarán los apuntes de Camila, una estudiante de tercer año de psicología quien desarrolló este diario. Sus respuestas son una orientación a lo que podrías escribir:',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 2:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                      width: size.width,
                      margin:
                          const EdgeInsets.only(top: 15, left: 20, bottom: 5),
                      child: const Text(
                        'La situación de Camila',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                          fontSize: 22,
                          color: CustomColors.primary,
                        ),
                      )),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: const EdgeInsets.only(
                        top: 5, right: 20, left: 20, bottom: 5),
                    child: RichText(
                      text: const TextSpan(
                        text: '',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text: 'Un examen de psicología del desarrollo.',
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    width: size.width,
                    margin:
                        const EdgeInsets.only(top: 25, left: 20, bottom: 25),
                    child: const Text(
                      'Diario personal de Camila',
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(
                        fontSize: 22,
                        color: CustomColors.primary,
                      ),
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: const EdgeInsets.only(
                        top: 5, right: 20, left: 20, bottom: 25),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Después de clases, llamé a la Vero. Hablamos sobre la asamblea de carrera de ayer, y que nos deberíamos juntar.\n\n',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                'Pensé que estaba muy cansada para concentrarme.\n\n',
                          ),
                          TextSpan(
                            text:
                                'Le escribí por WhatsApp al Nico para preguntarle cómo le había ido acompañando a su papá al super. Hablamos por casi una hora.\n\n',
                          ),
                          TextSpan(
                            text:
                                'Me dije que estudiaría después de tomar once. Para motivarme a estudiar puse la tele de fondo. Me puse a arreglar el espacio para hacerlo más cómodo para estudiar.\n\n',
                          ),
                          TextSpan(
                            text:
                                'Me empecé a preocupar porque no encontraba mis apuntes, pensé que se los había prestado a alguien y no me los devolvió. Los encontré en una mochila.\n\n',
                          ),
                          TextSpan(
                            text:
                                'Me sentía media nerviosa para estudiar, además que se había hecho tarde. Puse la alarma a las 5 am para levantarme a estudiar.\n\n',
                          ),
                          TextSpan(
                            text:
                                'Cuando la alarma sonó, la apagué. Empecé a estudiar una hora después de lo programado. Me sentía en un apuro mientras estudiaba mis apuntes y textos.\n\n',
                          ),
                          TextSpan(
                            text: 'Me fue peor de lo que necesitaba.\n\n',
                          ),
                          TextSpan(
                            text:
                                'Pensé que si hubiera tenido más tiempo para estudiar, me habría ido mejor. Me prepararé con anticipación y tranquila cuando estudie para la próxima prueba.',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 3:
        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    width: size.width,
                    margin:
                        const EdgeInsets.only(top: 25, left: 20, bottom: 25),
                    child: const Text(
                      'Ahora es tu turno...',
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(
                        fontSize: 22,
                        color: CustomColors.primary,
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.all(10),
                    height: size.height / 1.5,
                    child: Form(
                      key: _formKey,
                      child: Column(
                        children: [
                          Container(
                            margin: const EdgeInsets.only(
                                left: 10, right: 10, bottom: 0, top: 10),
                            alignment: Alignment.centerLeft,
                            child: const Text(
                              'Tu situación:',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                fontSize: 18,
                                color: CustomColors.primary,
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: situacion,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: 'Escribe tu respuesta...',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(
                                left: 10, right: 10, bottom: 0, top: 20),
                            alignment: Alignment.centerLeft,
                            child: const Text(
                              'Tu diario personal:',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                fontSize: 18,
                                color: CustomColors.primary,
                              ),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: diario,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText:
                                      'Puede ser más breve o largo que el de Camila\n\n\n\n\n\n\n\n\n\n',
                                  hintMaxLines: 10,
                                  border: OutlineInputBorder()),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );

      case 4:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 20, right: 20, left: 20, bottom: 5),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Ahora que has terminado de completar tu diario, te invitamos a utilizarlo para crear un plan de acción al cual puedas recurrir cuando observes que te puedes encontrar procrastinando. Para construir este plan, se puedes seguir tres pasos:\n\n',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text: '1. Revisar tus hallazgos\n',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          TextSpan(
                            text:
                                'Al revisar las respuestas de tu diario personal ¿Cuáles son tus hallazgos? (ej.: para Camila fue darse cuenta que continuamente no cumplía su promesa de estudiar con mayor rigurosidad).\n\n',
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          TextSpan(
                            text:
                                '2. Potencia tu autocontrol al sustituir hábitos de procastinación por acciones productivas\n',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          TextSpan(
                            text:
                                'Anota estrategias que puedas utilizar cuando te sientas procrastinando. Recuerda que no hay una estrategia que le sirva a todos de la misma manera, por lo que debes probar y evaluar cuál te funciona de mejor a ti. Aquí te dejamos algunas opciones:\n\n',
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          TextSpan(
                            text: '       a. Crea tus propios plazos:',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          TextSpan(
                            text:
                                ' como las tareas, trabajos y evaluaciones de la universidad suelen tener plazos lejanos el tiempo, establecer tus propios plazos favorecerá tu organización y progreso.\n\n',
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          TextSpan(
                            text:
                                '       b. Cambia el entorno en el que estudias:',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          TextSpan(
                            text:
                                ' si te distraes con mayor facilidad, organiza tu entorno de manera tal que puedas disminuir distracciones (ej. estudiar en un lugar tranquilo, desactivar las notificaciones de redes sociales, etc.).\n\n',
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          TextSpan(
                            text: '       c. Avanza paso a paso:',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          TextSpan(
                            text:
                                ' divide las tareas y actividades en pasos más pequeños, avanzando de manera progresiva. Esta estrategia favorece que puedas mantener la atención en lo que estás haciendo y disminuye el nivel de ansiedad o malestar emocional que la actividad pueda producir. Con cada paso que des aumentará tu sentido de autoeficacia y se potenciará tu motivación.\n\n',
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          TextSpan(
                            text:
                                '       d. Recompensa tus esfuerzos y avances:',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          TextSpan(
                            text:
                                ' puedes cambiar el valor de las actividades por medio de agregar recompensas al cumplir un objetivo (ej. ver un capítulo de una serie luego de estudiar para una prueba). Sin embargo, es importante que la recompensa sea proporcional al esfuerzo realizado, de lo contrario podrías impactar negativamente en tu motivación. Así, si la recompensa es muy pequeña no será lo suficientemente motivante y si la recompensa es demasiado grande (ej. ver series toda la tarde luego de haber estudiado media hora) será más fácil que procrastines lo que tienes que hacer.\n\n',
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          TextSpan(
                            text: '       e. Establece metas realistas:',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          TextSpan(
                            text:
                                ' tener metas realistas y alcanzables permite mantener expectativas saludables respecto a nuestros progresos y ayuda a promover nuestro sentido de autoeficacia y competencia. Por el contrario, las metas poco realistas impiden que evaluemos de manera adecuada nuestro progreso, aumentan el estrés, la ansiedad y las probabilidades de procrastinación.\n\n',
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          TextSpan(
                            text: '       f. Practica la autocompasión:',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          TextSpan(
                            text:
                                ' sé amable en la forma en la que te tratas y no te castigues (ej. usando un lenguaje negativo hacia ti mismx) por no estar avanzando como lo esperabas. La amabilidad y la comprensión de tus dificultades reducirá la probabilidad de que procrastines y promoverá tu bienestar y salud mental.\n\n',
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          TextSpan(
                            text: '       g. Reconecta con tus propósitos:',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          TextSpan(
                            text:
                                ' la falta de motivación es un desafío común a la hora de enfrentarnos a la procrastinación. Estudiar en educación superior se relaciona con metas y un plan de vida a largo plazo. Cuando sientas que tu motivación disminuye, hazte preguntas tales como ¿qué quiero lograr? ¿cómo mis estudios me ayudarán a lograrlo?, etc., que te permitan reflexionar y reconectar con tus metas y el sentido que tiene estudiar para ti.\n\n',
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          TextSpan(
                            text: '3. Evalúa el resultado de tu plan\n',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          TextSpan(
                            text:
                                'Describe qué sucedió al ejecutar tu plan: ¿qué cosas salieron bien?, ¿qué cosas podrías mejorar para una próxima ocasión? ¿A qué obstáculos o dificultades te enfrentaste?\n\n',
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      default:
        return Container();
    }
  }
}
