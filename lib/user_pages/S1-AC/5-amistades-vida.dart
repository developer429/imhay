// ignore_for_file: file_names, avoid_print

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:seguimiento/colors.dart';
import 'package:seguimiento/user_pages/MainPage.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../firebase.dart';

class A5 extends StatefulWidget {
  const A5({
    Key? key,
  }) : super(key: key);

  @override
  State<A5> createState() => _A5State();
}

class _A5State extends State<A5> {
  String uid = FirebaseAuth.instance.currentUser!.uid;
  bool loadingData = true;
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('page-control');
  String activeStepData = '';
  void getActivePageStored() {
    collectionReference.doc(uid).get().then((value) {
      var fields = value;
      activeStepData = fields['A5'];
      setState(() {
        if (int.parse(activeStepData) < 6) {
          activeStep = int.parse(activeStepData) - 1;
          Future.delayed(const Duration(milliseconds: 500), () {
            setState(() {
              loadingData = false;
            });
          });
        }
      });
    });
  }

  final Uri _url = Uri.parse(
      'https://www.upb.edu.co/es/central-blogs/vida-universitaria/como-hacer-amigos');

  ScrollController scrollController = ScrollController();

  bool favorite = false;
  CollectionReference favoriteData =
      FirebaseFirestore.instance.collection('favoritos');
  void getFavData() {
    favoriteData.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        favorite = fields['A5'];
      });
    }).catchError((err) {
      print('Error: $err');
    });
  }

  int activeStep = 0;
  @override
  void initState() {
    super.initState();
    getActivePageStored();

    getFavData();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> _launchUrl() async {
    if (!await launchUrl(_url)) {
      throw Exception('Could not launch $_url');
    }
  }

  bool active1 = false;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    switch (activeStep) {
      case 0:
        active1 = true;
        break;

      default:
    }
    return PopScope(
      canPop: false,
      onPopInvokedWithResult: (bool didPop, result) {
        if (didPop) {
          FirestoreService()
              .updateCurrentPage('A5', (activeStep + 1).toString());
          return;
        }
      },
      child: loadingData
          ? Container(
              width: size.width,
              height: size.height,
              color: CustomColors.primaryDark,
              child: const Center(
                  child: CircularProgressIndicator(
                color: CustomColors.tertiary70,
              )))
          : Container(
              color: Colors.black,
              child: SafeArea(
                child: Scaffold(
                  backgroundColor: CustomColors.background,
                  body: Center(
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              width: size.width,
                              height: 5,
                              color: active1
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                          ],
                        ),
                        content(),
                        Container(
                          margin: const EdgeInsets.only(top: 10, bottom: 10),
                          child: bottomStructure(),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
    );
  }

  Widget topStructure() {
    return Column(
      children: [
        Row(
          children: [
            Container(
              alignment: Alignment.topRight,
              child: IconButton(
                onPressed: () {
                  FirestoreService()
                      .updateCurrentPage('A5', (activeStep + 1).toString())
                      .then((value) => Navigator.of(context).pop());
                },
                icon: const Icon(
                  Icons.close,
                  color: CustomColors.primaryDark,
                ),
              ),
            ),
            const Spacer(),
            favorito(),
          ],
        ),
        Text(
          headerText(),
          textScaler: const TextScaler.linear(1.0),
          style: const TextStyle(
            color: CustomColors.primaryDark,
            fontSize: 24,
          ),
        ),
        Container(
          margin: const EdgeInsets.only(top: 20, bottom: 20),
          child: header(),
        ),
      ],
    );
  }

  Widget favorito() {
    return StreamBuilder<QuerySnapshot>(
        stream: FirestoreService().favoritos(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasData) {
            return SizedBox(
              width: 50,
              height: 50,
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (context, index) {
                    var document = snapshot.data?.docs[index];
                    if (document != null && document.id == uid) {
                      switch (document['A5']) {
                        case true:
                          return IconButton(
                              icon: Icon(MdiIcons.star,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('A5', false)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Eliminado de favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                        case false:
                          return IconButton(
                              icon: Icon(MdiIcons.starOutline,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService().setFavorito('A5', true).then(
                                    (value) => Fluttertoast.showToast(
                                        msg: "Agregado a favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                      }
                    } else {
                      return Container();
                    }
                    return null;
                  }),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(
                color: Colors.red,
              ),
            );
          }
        });
  }

  Widget bottomStructure() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        nextButton(),
      ],
    );
  }

  Widget nextButton() {
    Size size = MediaQuery.of(context).size;
    return SizedBox(
      width: size.width / 1.2,
      child: ElevatedButton(
        style: ButtonStyle(
          backgroundColor: WidgetStateProperty.all<Color>(CustomColors.primary),
          shape: WidgetStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(18.0),
            ),
          ),
        ),
        onPressed: () {
          FirestoreService().updateCurrentPage('A5', '1');

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute<void>(
                  builder: (BuildContext context) => const MainPage(
                        activePage: 1,
                      )),
              (route) => false);
        },
        child: Text(
          buttonText(),
          textScaler: const TextScaler.linear(1.0),
          style: const TextStyle(
              color: CustomColors.background, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }

  Widget header() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      default:
        return Image(
          width: size.width / 3,
          height: size.width / 3,
          fit: BoxFit.scaleDown,
          image: AssetImage(imageHeader()),
        );
    }
  }

  String imageHeader() {
    switch (activeStep) {
      case 0:
        return 'assets/images/amistades-social-icon.png';

      default:
        return 'assets/images/amistades-social-icon.png';
    }
  }

  String headerText() {
    switch (activeStep) {
      case 0:
        return 'Amistades y vida social';

      default:
        return 'Amistades y vida social';
    }
  }

  String buttonText() {
    switch (activeStep) {
      default:
        return 'Finalizar ';
    }
  }

  Widget content() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 20, right: 20, left: 20, bottom: 5),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'La vida universitaria trae consigo cambios en los entornos, vínculos y relaciones que sostenemos con nuestras amistades. Al compartir las experiencias y desafíos de la vida universitaria, las amistades que puedas hacer durante este periodo pueden tener un impacto positivo y ayudarte a disfrutar de esta etapa. Sin embargo, estos vínculos no se generan de forma automática y pueden tomar tiempo. Así, es posible que frente a situaciones tales como entrar a estudiar o cambiarte de carrera no sepas cómo relacionarte con tus compañerxs. Esto puede sentirse abrumador, pero también es una oportunidad para abrirse, conocer personas con intereses similares, y construir amistades valiosas.\n\n',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                'Aquí te compartimos algunos tips que pueden ayudarte a generar nuevas relaciones con tus compañerxs en la universidad:\n\n',
                          ),
                          TextSpan(
                            text:
                                '   ·  Enfrenta el miedo de conocer a alguien nuevo:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' empezar desde cero tiene sus ventajas, siéntete en la libertad de ser tú mismx y aventurarte a relacionarte con las personas con las que vas a compartir durante tu pregrado.\n\n',
                          ),
                          TextSpan(
                            text: '   ·  Habla con confianza:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' un miedo recurrente que tienen lxs estudiantes a la hora de hacer amigos, es el tener que decir siempre algo interesante o inteligente por temor a ser juzgadx. Sin embargo, esto es una expectativa poco realista y autoexigente. Así que atrévete y no tengas miedo a cometer errores, esto ayuda a generar espacios seguros para dialogar sobre todo tipo de temas.\n\n',
                          ),
                          TextSpan(
                            text: '   ·  Inicia conversaciones:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' decirlo es más fácil que hacerlo, pero luego de dejar el miedo, no tienes que esperar a que los otros den el primer paso, acércate e inicia la conversación, así podrás conocer a tus compañerxs.\n\n',
                          ),
                          TextSpan(
                            text: '   ·  Escucha lo que otros dicen:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' esto te ayudará a establecer relaciones recíprocas y de confianza, además podrás diferenciar con quiénes tienes gustos en común y aprenderás sobre temas que desconocías y te pueden interesar.\n\n',
                          ),
                          TextSpan(
                            text:
                                '   ·  Busca actividades extracurriculares de tu agrado:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' allí puedes conectar con personas con las que compartes intereses, que pueden pertenecer a otros pregrados y otros semestres, y te pueden guiar en tu vida universitaria.\n\n',
                          ),
                          TextSpan(
                            text: '   ·  Ten apertura:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' reconocer que unx no se las sabe todas es importante, si tienes apertura, cada día y cada persona llegará con algo nuevo para enseñarte, y donde cada quién podrá aportar desde su experiencia.\n\n',
                          ),
                          TextSpan(
                            text: '   ·  Conoce tus límites:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' reflexiona acerca de lo que significa para ti una amistad y cómo puedes relacionarte cuidando de tu bienestar, esto te permitirá poner límites y evitar relaciones que resulten perjudiciales o que lleven a situaciones que tengan un impacto negativo en tu experiencia universitaria.\n\n',
                          ),
                          TextSpan(
                            text: '   ·  Considera que las amistades cambian: ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'si bien hay amistades que logran sobreponerse ante cualquier adversidad, hay otras que se acaban y eso está bien. Las personas cambian, los intereses varían, y puede haber momentos en los que dos personas ya no tienen un punto de encuentro en común y toman caminos diferentes.',
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    width: size.width,
                    margin: const EdgeInsets.only(top: 50, left: 20, bottom: 5),
                    child: const Text(
                      'Dato útil:',
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(
                        fontSize: 22,
                        color: CustomColors.primary,
                      ),
                    ),
                  ),
                  Container(
                    width: size.width,
                    margin: const EdgeInsets.only(left: 20, bottom: 5),
                    child: const Text(
                      'Estos cinco tips te ayudarán a saber cómo hacer amigos en la U.',
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(
                        fontSize: 16,
                        color: CustomColors.primary,
                      ),
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: const EdgeInsets.only(top: 5, bottom: 20, left: 20),
                    child: ElevatedButton.icon(
                      style: ButtonStyle(
                        backgroundColor: WidgetStateProperty.all<Color>(
                            CustomColors.primary80pink),
                        shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                          ),
                        ),
                      ),
                      onPressed: () {
                        _launchUrl();
                      },
                      icon: Icon(MdiIcons.openInNew,
                          color: CustomColors.primaryDark),
                      label: const Text(
                        'Ir al artículo',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            color: CustomColors.primaryDark,
                            decoration: TextDecoration.underline),
                      ), // <-- Text
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      default:
        return Container();
    }
  }
}
