// ignore_for_file: file_names, avoid_print, prefer_typing_uninitialized_variables

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:seguimiento/colors.dart';
import 'package:seguimiento/user_pages/MainPage.dart';
import 'package:toggle_switch/toggle_switch.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../firebase.dart';

class A6 extends StatefulWidget {
  const A6({
    Key? key,
  }) : super(key: key);

  @override
  State<A6> createState() => _A6State();
}

class _A6State extends State<A6> {
  String uid = FirebaseAuth.instance.currentUser!.uid;
  bool loadingData = true;
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('page-control');
  String activeStepData = '';
  void getActivePageStored() {
    collectionReference.doc(uid).get().then((value) {
      var fields = value;
      activeStepData = fields['A6'];
      setState(() {
        if (int.parse(activeStepData) < 6) {
          activeStep = int.parse(activeStepData) - 1;
          Future.delayed(const Duration(milliseconds: 500), () {
            setState(() {
              loadingData = false;
            });
          });
        }
      });
    });
  }

  var startMandar;
  var startAmenazar;
  var startSermonear;
  var startLecciones;
  var startAconsejar;
  var startConsolar;
  var startAprobar;
  var startDesaprobar;
  var startInsultar;
  var startInterpretar;
  var startInterrogar;
  var startIronizar;

  String mandar = 'No';
  String amenazar = 'No';
  String sermonear = 'No';
  String lecciones = 'No';
  String aconsejar = 'No';
  String consolar = 'No';
  String aprobar = 'No';
  String desaprobar = 'No';
  String insultar = 'No';
  String interpretar = 'No';
  String interrogar = 'No';
  String ironizar = 'No';

  CollectionReference resolverConflictosReferense = FirebaseFirestore.instance
      .collection('A6-resolviendo-conflictos-respuestas');
  void getData() {
    resolverConflictosReferense.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        if (fields['mandar'] == 'Sí') {
          startMandar = 0;
          mandar = 'Sí';
        } else {
          startMandar = 1;
          mandar = 'No';
        }
        if (fields['amenazar'] == 'Sí') {
          startAmenazar = 0;
          amenazar = 'Sí';
        } else {
          startAmenazar = 1;
          amenazar = 'No';
        }
        if (fields['sermonear'] == 'Sí') {
          startSermonear = 0;
          sermonear = 'Sí';
        } else {
          startSermonear = 1;
          sermonear = 'No';
        }
        if (fields['lecciones'] == 'Sí') {
          startLecciones = 0;
          lecciones = 'Sí';
        } else {
          startLecciones = 1;
          lecciones = 'No';
        }
        if (fields['aconsejar'] == 'Sí') {
          startAconsejar = 0;
          aconsejar = 'Sí';
        } else {
          startAconsejar = 1;
          aconsejar = 'No';
        }
        if (fields['consolar'] == 'Sí') {
          startConsolar = 0;
          consolar = 'Sí';
        } else {
          startConsolar = 1;
          consolar = 'No';
        }
        if (fields['aprobar'] == 'Sí') {
          startAprobar = 0;
          aprobar = 'Sí';
        } else {
          startAprobar = 1;
          aprobar = 'No';
        }
        if (fields['desaprobar'] == 'Sí') {
          startDesaprobar = 0;
          desaprobar = 'Sí';
        } else {
          startDesaprobar = 1;
          desaprobar = 'No';
        }
        if (fields['insultar'] == 'Sí') {
          startInsultar = 0;
          insultar = 'Sí';
        } else {
          startInsultar = 1;
          insultar = 'No';
        }
        if (fields['interpretar'] == 'Sí') {
          startInterpretar = 0;
          interpretar = 'Sí';
        } else {
          startInterpretar = 1;
          interpretar = 'No';
        }
        if (fields['interrogar'] == 'Sí') {
          startInterrogar = 0;
          interrogar = 'Sí';
        } else {
          startInterrogar = 1;
          interrogar = 'No';
        }
        if (fields['ironizar'] == 'Sí') {
          startIronizar = 0;
          ironizar = 'Sí';
        } else {
          startIronizar = 1;
          ironizar = 'No';
        }
      });
    }).catchError((err) {
      print('Error: $err');
      startMandar = 1;
      startAmenazar = 1;
      startSermonear = 1;
      startLecciones = 1;
      startAconsejar = 1;
      startConsolar = 1;
      startAprobar = 1;
      startDesaprobar = 1;
      startInsultar = 1;
      startInterpretar = 1;
      startInterrogar = 1;
      startIronizar = 1;
    });
  }

  final Uri _url = Uri.parse(
      'https://bibliotecadigital.mineduc.cl/bitstream/handle/20.500.12365/437/MONO-365.pdf?sequence=1&isAllowed=y');

  ScrollController scrollController = ScrollController();

  bool favorite = false;
  CollectionReference favoriteData =
      FirebaseFirestore.instance.collection('favoritos');
  void getFavData() {
    favoriteData.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        favorite = fields['A6'];
      });
    }).catchError((err) {
      print('Error: $err');
    });
  }

  int activeStep = 0;
  @override
  void initState() {
    super.initState();
    getActivePageStored();
    getData();
    getFavData();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> _launchUrl() async {
    if (!await launchUrl(_url)) {
      throw Exception('Could not launch $_url');
    }
  }

  bool active1 = false;
  bool active2 = false;
  bool active3 = false;
  bool active4 = false;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    switch (activeStep) {
      case 0:
        active1 = true;
        active2 = false;
        active3 = false;
        active4 = false;
        break;
      case 1:
        active1 = true;
        active2 = true;
        active3 = false;
        active4 = false;

        break;
      case 2:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = false;

        break;
      case 3:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;

        break;

      default:
    }
    return PopScope(
      canPop: false,
      onPopInvokedWithResult: (bool didPop, result) {
        if (didPop) {
          FirestoreService()
              .updateCurrentPage('A6', (activeStep + 1).toString());
          return;
        }
      },
      child: loadingData
          ? Container(
              width: size.width,
              height: size.height,
              color: CustomColors.primaryDark,
              child: const Center(
                  child: CircularProgressIndicator(
                color: CustomColors.tertiary70,
              )))
          : Container(
              color: Colors.black,
              child: SafeArea(
                child: Scaffold(
                  backgroundColor: CustomColors.background,
                  body: Center(
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              width: size.width / 4,
                              height: 5,
                              color: active1
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 4,
                              height: 5,
                              color: active2
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 4,
                              height: 5,
                              color: active3
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 4,
                              height: 5,
                              color: active4
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                          ],
                        ),
                        content(),
                        Container(
                          margin: const EdgeInsets.only(top: 10, bottom: 10),
                          child: bottomStructure(),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
    );
  }

  Widget topStructure() {
    return Column(
      children: [
        Row(
          children: [
            Container(
              alignment: Alignment.topRight,
              child: IconButton(
                onPressed: () {
                  FirestoreService()
                      .updateCurrentPage('A6', (activeStep + 1).toString())
                      .then((value) => Navigator.of(context).pop());
                },
                icon: const Icon(
                  Icons.close,
                  color: CustomColors.primaryDark,
                ),
              ),
            ),
            const Spacer(),
            favorito(),
          ],
        ),
        Text(
          headerText(),
          textScaler: const TextScaler.linear(1.0),
          style: const TextStyle(
            color: CustomColors.primaryDark,
            fontSize: 24,
          ),
        ),
        Container(
          margin: const EdgeInsets.only(top: 20, bottom: 20),
          child: header(),
        ),
      ],
    );
  }

  Widget favorito() {
    return StreamBuilder<QuerySnapshot>(
        stream: FirestoreService().favoritos(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasData) {
            return SizedBox(
              width: 50,
              height: 50,
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (context, index) {
                    var document = snapshot.data?.docs[index];
                    if (document != null && document.id == uid) {
                      switch (document['A6']) {
                        case true:
                          return IconButton(
                              icon: Icon(MdiIcons.star,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('A6', false)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Eliminado de favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                        case false:
                          return IconButton(
                              icon: Icon(MdiIcons.starOutline,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService().setFavorito('A6', true).then(
                                    (value) => Fluttertoast.showToast(
                                        msg: "Agregado a favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                      }
                    } else {
                      return Container();
                    }
                    return null;
                  }),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(
                color: Colors.red,
              ),
            );
          }
        });
  }

  Widget bottomStructure() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        nextButton(),
      ],
    );
  }

  Widget nextButton() {
    Size size = MediaQuery.of(context).size;
    if (activeStep >= 1 && activeStep < 4) {
      return SizedBox(
        width: size.width / 1,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        WidgetStateProperty.all<Color>(CustomColors.background),
                    shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: const BorderSide(color: CustomColors.primary)),
                    ),
                  ),
                  onPressed: () {
                    setState(() {
                      activeStep = activeStep - 1;
                    });
                  },
                  child: const Text(
                    'Anterior',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(color: CustomColors.primary),
                  )),
            ),
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor:
                      WidgetStateProperty.all<Color>(CustomColors.primary),
                  shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ),
                  ),
                ),
                onPressed: () {
                  if (activeStep < 3) {
                    setState(() {
                      activeStep++;
                    });
                  } else {
                    FirestoreService().updateCurrentPage('A6', '1');
                    FirestoreService().guardarRespuestasA6(
                        mandar,
                        amenazar,
                        sermonear,
                        lecciones,
                        aconsejar,
                        consolar,
                        aprobar,
                        desaprobar,
                        insultar,
                        interpretar,
                        interrogar,
                        ironizar);
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute<void>(
                            builder: (BuildContext context) => const MainPage(
                                  activePage: 1,
                                )),
                        (route) => false);
                  }
                },
                child: Text(
                  buttonText(),
                  textScaler: const TextScaler.linear(1.0),
                  style: const TextStyle(
                      color: CustomColors.background,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      return SizedBox(
        width: size.width / 1.2,
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor:
                WidgetStateProperty.all<Color>(CustomColors.primary),
            shape: WidgetStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              ),
            ),
          ),
          onPressed: () {
            if (activeStep < 3) {
              setState(() {
                activeStep++;
              });
            } else {
              FirestoreService().updateCurrentPage('A6', '1');
              FirestoreService().guardarRespuestasA6(
                  mandar,
                  amenazar,
                  sermonear,
                  lecciones,
                  aconsejar,
                  consolar,
                  aprobar,
                  desaprobar,
                  insultar,
                  interpretar,
                  interrogar,
                  ironizar);
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute<void>(
                      builder: (BuildContext context) => const MainPage(
                            activePage: 1,
                          )),
                  (route) => false);
            }
            /*
            */
          },
          child: Text(
            buttonText(),
            textScaler: const TextScaler.linear(1.0),
            style: const TextStyle(color: CustomColors.background),
          ),
        ),
      );
    }
  }

  Widget header() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      default:
        return Image(
          width: size.width / 3,
          height: size.width / 3,
          fit: BoxFit.scaleDown,
          image: AssetImage(imageHeader()),
        );
    }
  }

  String imageHeader() {
    switch (activeStep) {
      case 0:
        return 'assets/images/resolver-conflictos-icon.png';

      default:
        return 'assets/images/resolver-conflictos-icon.png';
    }
  }

  String headerText() {
    switch (activeStep) {
      case 0:
        return 'Resolviendo conflictos';

      default:
        return 'Resolviendo conflictos';
    }
  }

  String buttonText() {
    switch (activeStep) {
      case 3:
        return 'Finalizar';

      default:
        return 'Siguiente ';
    }
  }

  Widget content() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 20, right: 20, left: 20, bottom: 20),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Seguramente en tu vida te has encontrado en situaciones donde has experimentado una diferencia de opinión y/o perspectiva con una persona o grupo (ej.: amistades, familia, compañerxs, etc.).\n\n',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                'La universidad es un espacio donde confluyen estudiantes con distintas trayectorias de vida e intereses. De esta forma, es normal que pensemos y hagamos cosas de manera distinta a la gente que tenemos a nuestro alrededor. Esto puede ser una oportunidad para expandir nuestras perspectivas, sin embargo, a veces estas diferencias se traducen en conflictos que afectan negativamente la experiencia de socialización y aprendizaje (ej.: una persona malinterpreta lo que otra ha dicho y reacciona defendiéndose ante lo que ha considerado una ofensa).\n\n',
                          ),
                          TextSpan(
                            text:
                                'Así, los conflictos se producen cuando dos o más personas presentan posiciones, opiniones, expectativas y/o intereses que son incompatibles. Dichas situaciones pueden tener un impacto significativo en la relación entre las partes, con algunas relaciones siendo robustecidas mientras otras, deterioradas de acuerdo a la oportunidad y manera en la que se haya decidido abordarlas  En este escenario, la comunicación puede funcionar como un obstáculo para superar estas diferencias. Sin embargo, también puede convertirse en una gran aliada para afrontarlos de forma positiva.\n',
                          ),
                          TextSpan(
                            text:
                                'En esta actividad, queremos compartirte algunas ideas y herramientas que podrán ayudarte a afrontar los conflictos de manera constructiva y pacífica por medio de la comunicación.',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 1:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                      width: size.width,
                      margin: const EdgeInsets.only(
                          top: 15, left: 20, bottom: 5, right: 10),
                      child: const Text(
                        '¿Cómo puedo mejorar mi comunicación para la resolución de conflictos?',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                          fontSize: 22,
                          color: CustomColors.primary,
                        ),
                      )),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: const EdgeInsets.only(
                        top: 5, right: 20, left: 20, bottom: 5),
                    child: RichText(
                      text: const TextSpan(
                        text: '',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Reconoce cuál es el problema del conflicto: implica distinguir y despejar lo que está en disputa en la situación, cuál es tu rol en el conflicto y por qué es importante para ti. Te puedes preguntar ¿Qué ha sucedido?, ¿en qué no estamos coincidiendo?, ¿cómo estoy contribuyendo en este conflicto?\n\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Interésate por lo que la otra persona comunica: intenta ponerte en el lugar de la otra persona, entendiendo su perspectiva del conflicto. Te puedes preguntar ¿Qué me quiso decir con esto? También puedes pedir una clarificación del mensaje ¿lo que me quisiste decir fue esto … o más bien era otra cosa? También es importante valorar el esfuerzo de la otra persona por comunicarse Gracias por compartir tus ideas y cómo te sientes, Me interesa conocer tu opinión.\n\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Escucha activamente: desmenuza el mensaje en función de lo que quieres decir y comunicar, y no en función de lo que personalmente quieres escuchar. Te puedes preguntar ¿Qué quiero conseguir con esta conversación?, ¿qué necesito para sentirme satisfecho/a?, ¿qué espero de la otra persona?\n\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Reconoce tus emociones y las de la otra persona: las emociones pueden contribuir u obstaculizar la resolución de conflictos. Te puedes preguntar ¿Qué siento?, ¿qué siente la otra persona?. Emociones difíciles de administrar (ej.: dolor, rabia, miedo, entre otras) pueden verse favorecidas al tomar distancia de la situación (ej.: contar hasta diez antes de tomar decir algo).\n\n',
                          ),
                          TextSpan(
                            text:
                                'Emite el mensaje en primera persona: mensajes como yo creo, yo siento, yo quiero, yo vi, entre otros, pueden facilitar reconocer el problema para cada una de las partes involucradas.',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 2:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: const EdgeInsets.only(
                        top: 5, right: 20, left: 20, bottom: 5),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Ahora te invitamos a reflexionar y reconocer algunas actitudes que dificultan la resolución pacífica de conflictos.',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text: ' Selecciona aquellas que reconozcas en ti:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    //height: 1000,
                    margin: const EdgeInsets.all(15),
                    decoration:
                        BoxDecoration(borderRadius: BorderRadius.circular(15)),
                    child: ListView(
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      children: [
                        ListTile(
                          title: const Text(
                            'Mandar, dirigir',
                            textScaler: TextScaler.linear(1.0),
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 16),
                          ),
                          subtitle: RichText(
                            text: const TextSpan(
                              text: '',
                              style: TextStyle(
                                  fontSize: 14,
                                  color: CustomColors.primaryDark,
                                  height: 1.5),
                              children: <TextSpan>[
                                TextSpan(
                                  text:
                                      'Decir a la otra persona lo que debe hacer.\n',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                TextSpan(text: 'Por ejemplo:\n'),
                                TextSpan(
                                  text: '   ·  ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                TextSpan(
                                  text: '“Tienes que…”\n',
                                ),
                                TextSpan(
                                  text: '   ·  ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                TextSpan(
                                  text: '“Deber hacer…”',
                                ),
                              ],
                            ),
                          ),
                          trailing: ToggleSwitch(
                            minWidth: 50.0,
                            cornerRadius: 20.0,
                            activeBgColors: const [
                              [CustomColors.activeSwitch],
                              [CustomColors.orange3]
                            ],
                            activeFgColor: CustomColors.primaryDark,
                            inactiveBgColor: CustomColors.background,
                            inactiveFgColor: CustomColors.primaryDark,
                            borderWidth: 1,
                            borderColor: const [CustomColors.primaryDark],
                            initialLabelIndex: startMandar,
                            totalSwitches: 2,
                            labels: const ['Sí', 'No'],
                            customTextStyles: const [
                              TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 14)
                            ],
                            onToggle: (index) {
                              if (index == 0) {
                                mandar = 'Sí';
                              } else {
                                mandar = 'No';
                              }
                            },
                          ),
                        ),
                        const Divider(
                          thickness: 1,
                        ),
                        ListTile(
                          title: const Text(
                            'Amenazar',
                            textScaler: TextScaler.linear(1.0),
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 16),
                          ),
                          subtitle: RichText(
                            text: const TextSpan(
                              text: '',
                              style: TextStyle(
                                  fontSize: 14,
                                  color: CustomColors.primaryDark,
                                  height: 1.5),
                              children: <TextSpan>[
                                TextSpan(
                                  text:
                                      'Decir a la otra persona lo que le puede pasar si no hace lo que se le pide.\n',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                TextSpan(text: 'Por ejemplo:\n'),
                                TextSpan(
                                  text: '   ·  ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                TextSpan(
                                  text: '“Haz esto, de lo contrario…”\n',
                                ),
                                TextSpan(
                                  text: '   ·  ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                TextSpan(
                                  text:
                                      '“Más vale que hagas lo que te pido, sino…”',
                                ),
                              ],
                            ),
                          ),
                          trailing: ToggleSwitch(
                            minWidth: 50.0,
                            cornerRadius: 20.0,
                            activeBgColors: const [
                              [CustomColors.activeSwitch],
                              [CustomColors.orange3]
                            ],
                            activeFgColor: CustomColors.primaryDark,
                            inactiveBgColor: CustomColors.background,
                            inactiveFgColor: CustomColors.primaryDark,
                            borderWidth: 1,
                            borderColor: const [CustomColors.primaryDark],
                            initialLabelIndex: startAmenazar,
                            totalSwitches: 2,
                            labels: const ['Sí', 'No'],
                            customTextStyles: const [
                              TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 14)
                            ],
                            onToggle: (index) {
                              if (index == 0) {
                                amenazar = 'Sí';
                              } else {
                                amenazar = 'No';
                              }
                            },
                          ),
                        ),
                        const Divider(
                          thickness: 1,
                        ),
                        ListTile(
                          title: const Text(
                            'Sermonear',
                            textScaler: TextScaler.linear(1.0),
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 16),
                          ),
                          subtitle: RichText(
                            text: const TextSpan(
                              text: '',
                              style: TextStyle(
                                  fontSize: 14,
                                  color: CustomColors.primaryDark,
                                  height: 1.5),
                              children: <TextSpan>[
                                TextSpan(
                                  text:
                                      'Aludir a una norma externa para decir lo que debe hacer el otro.\n',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                TextSpan(text: 'Por ejemplo:\n'),
                                TextSpan(
                                  text: '   ·  ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                TextSpan(
                                  text: '“Debes ser responsable”',
                                ),
                              ],
                            ),
                          ),
                          trailing: ToggleSwitch(
                            minWidth: 50.0,
                            cornerRadius: 20.0,
                            activeBgColors: const [
                              [CustomColors.activeSwitch],
                              [CustomColors.orange3]
                            ],
                            activeFgColor: CustomColors.primaryDark,
                            inactiveBgColor: CustomColors.background,
                            inactiveFgColor: CustomColors.primaryDark,
                            borderWidth: 1,
                            borderColor: const [CustomColors.primaryDark],
                            initialLabelIndex: startSermonear,
                            totalSwitches: 2,
                            labels: const ['Sí', 'No'],
                            customTextStyles: const [
                              TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 14)
                            ],
                            onToggle: (index) {
                              if (index == 0) {
                                sermonear = 'Sí';
                              } else {
                                sermonear = 'No';
                              }
                            },
                          ),
                        ),
                        const Divider(
                          thickness: 1,
                        ),
                        ListTile(
                          title: const Text(
                            'Dar lecciones',
                            textScaler: TextScaler.linear(1.0),
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 16),
                          ),
                          subtitle: RichText(
                            text: const TextSpan(
                              text: '',
                              style: TextStyle(
                                  fontSize: 14,
                                  color: CustomColors.primaryDark,
                                  height: 1.5),
                              children: <TextSpan>[
                                TextSpan(
                                  text:
                                      'Aludir a la experiencia para decir lo que es bueno o malo para la otra persona.\n',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                TextSpan(text: 'Por ejemplo:\n'),
                                TextSpan(
                                  text: '   ·  ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                TextSpan(
                                  text:
                                      '“Antes los hombres eran más caballeros”\n',
                                ),
                                TextSpan(
                                  text: '   ·  ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                TextSpan(
                                  text:
                                      '“Antes las mujeres eran más señoritas”',
                                ),
                              ],
                            ),
                          ),
                          trailing: ToggleSwitch(
                            minWidth: 50.0,
                            cornerRadius: 20.0,
                            activeBgColors: const [
                              [CustomColors.activeSwitch],
                              [CustomColors.orange3]
                            ],
                            activeFgColor: CustomColors.primaryDark,
                            inactiveBgColor: CustomColors.background,
                            inactiveFgColor: CustomColors.primaryDark,
                            borderWidth: 1,
                            borderColor: const [CustomColors.primaryDark],
                            initialLabelIndex: startLecciones,
                            totalSwitches: 2,
                            labels: const ['Sí', 'No'],
                            customTextStyles: const [
                              TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 14)
                            ],
                            onToggle: (index) {
                              if (index == 0) {
                                lecciones = 'Sí';
                              } else {
                                lecciones = 'No';
                              }
                            },
                          ),
                        ),
                        const Divider(
                          thickness: 1,
                        ),
                        ListTile(
                          title: const Text(
                            'Aconsejar',
                            textScaler: TextScaler.linear(1.0),
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 16),
                          ),
                          subtitle: RichText(
                            text: const TextSpan(
                              text: '',
                              style: TextStyle(
                                  fontSize: 14,
                                  color: CustomColors.primaryDark,
                                  height: 1.5),
                              children: <TextSpan>[
                                TextSpan(
                                  text:
                                      'Decir a la otra persona lo que es mejor para él o ella.\n',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                TextSpan(text: 'Por ejemplo:\n'),
                                TextSpan(
                                  text: '   ·  ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                TextSpan(
                                  text: '“Lo mejor que puedes hacer es…”\n',
                                ),
                                TextSpan(
                                  text: '   ·  ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                TextSpan(
                                  text: '“Deja ese grupo, es lo mejor para ti”',
                                ),
                              ],
                            ),
                          ),
                          trailing: ToggleSwitch(
                            minWidth: 50.0,
                            cornerRadius: 20.0,
                            activeBgColors: const [
                              [CustomColors.activeSwitch],
                              [CustomColors.orange3]
                            ],
                            activeFgColor: CustomColors.primaryDark,
                            inactiveBgColor: CustomColors.background,
                            inactiveFgColor: CustomColors.primaryDark,
                            borderWidth: 1,
                            borderColor: const [CustomColors.primaryDark],
                            initialLabelIndex: startAconsejar,
                            totalSwitches: 2,
                            labels: const ['Sí', 'No'],
                            customTextStyles: const [
                              TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 14)
                            ],
                            onToggle: (index) {
                              if (index == 0) {
                                aconsejar = 'Sí';
                              } else {
                                aconsejar = 'No';
                              }
                            },
                          ),
                        ),
                        const Divider(
                          thickness: 1,
                        ),
                        ListTile(
                          title: const Text(
                            'Consolar',
                            textScaler: TextScaler.linear(1.0),
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 16),
                          ),
                          subtitle: RichText(
                            text: const TextSpan(
                              text: '',
                              style: TextStyle(
                                  fontSize: 14,
                                  color: CustomColors.primaryDark,
                                  height: 1.5),
                              children: <TextSpan>[
                                TextSpan(
                                  text:
                                      'Minimizar lo que la otra persona siente o quiere.\n',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                TextSpan(text: 'Por ejemplo:\n'),
                                TextSpan(
                                  text: '   ·  ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                TextSpan(
                                  text: '“Ya se te pasará…”\n',
                                ),
                                TextSpan(
                                  text: '   ·  ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                TextSpan(
                                  text: '“No te preocupes…”',
                                ),
                              ],
                            ),
                          ),
                          trailing: ToggleSwitch(
                            minWidth: 50.0,
                            cornerRadius: 20.0,
                            activeBgColors: const [
                              [CustomColors.activeSwitch],
                              [CustomColors.orange3]
                            ],
                            activeFgColor: CustomColors.primaryDark,
                            inactiveBgColor: CustomColors.background,
                            inactiveFgColor: CustomColors.primaryDark,
                            borderWidth: 1,
                            borderColor: const [CustomColors.primaryDark],
                            initialLabelIndex: startConsolar,
                            totalSwitches: 2,
                            labels: const ['Sí', 'No'],
                            customTextStyles: const [
                              TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 14)
                            ],
                            onToggle: (index) {
                              if (index == 0) {
                                consolar = 'Sí';
                              } else {
                                consolar = 'No';
                              }
                            },
                          ),
                        ),
                        const Divider(
                          thickness: 1,
                        ),
                        ListTile(
                          title: const Text(
                            'Aprobar',
                            textScaler: TextScaler.linear(1.0),
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 16),
                          ),
                          subtitle: RichText(
                            text: const TextSpan(
                              text: '',
                              style: TextStyle(
                                  fontSize: 14,
                                  color: CustomColors.primaryDark,
                                  height: 1.5),
                              children: <TextSpan>[
                                TextSpan(
                                  text: 'Dar la razón a la otra persona.\n',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                TextSpan(text: 'Por ejemplo:\n'),
                                TextSpan(
                                  text: '   ·  ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                TextSpan(
                                  text:
                                      '“Estoy de acuerdo contigo, lo mejor es que…”',
                                ),
                              ],
                            ),
                          ),
                          trailing: ToggleSwitch(
                            minWidth: 50.0,
                            cornerRadius: 20.0,
                            activeBgColors: const [
                              [CustomColors.activeSwitch],
                              [CustomColors.orange3]
                            ],
                            activeFgColor: CustomColors.primaryDark,
                            inactiveBgColor: CustomColors.background,
                            inactiveFgColor: CustomColors.primaryDark,
                            borderWidth: 1,
                            borderColor: const [CustomColors.primaryDark],
                            initialLabelIndex: startAprobar,
                            totalSwitches: 2,
                            labels: const ['Sí', 'No'],
                            customTextStyles: const [
                              TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 14)
                            ],
                            onToggle: (index) {
                              if (index == 0) {
                                aprobar = 'Sí';
                              } else {
                                aprobar = 'No';
                              }
                            },
                          ),
                        ),
                        const Divider(
                          thickness: 1,
                        ),
                        ListTile(
                          title: const Text(
                            'Desaprobar',
                            textScaler: TextScaler.linear(1.0),
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 16),
                          ),
                          subtitle: RichText(
                            text: const TextSpan(
                              text: '',
                              style: TextStyle(
                                  fontSize: 14,
                                  color: CustomColors.primaryDark,
                                  height: 1.5),
                              children: <TextSpan>[
                                TextSpan(
                                  text: 'Quitar la razón a la otra persona.\n',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                TextSpan(text: 'Por ejemplo:\n'),
                                TextSpan(
                                  text: '   ·  ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                TextSpan(
                                  text:
                                      '“Lo que estás diciendo es una tontería…”\n',
                                ),
                                TextSpan(
                                  text: '   ·  ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                TextSpan(
                                  text:
                                      '“Lo que estás diciendo no tiene sentido”',
                                ),
                              ],
                            ),
                          ),
                          trailing: ToggleSwitch(
                            minWidth: 50.0,
                            cornerRadius: 20.0,
                            activeBgColors: const [
                              [CustomColors.activeSwitch],
                              [CustomColors.orange3]
                            ],
                            activeFgColor: CustomColors.primaryDark,
                            inactiveBgColor: CustomColors.background,
                            inactiveFgColor: CustomColors.primaryDark,
                            borderWidth: 1,
                            borderColor: const [CustomColors.primaryDark],
                            initialLabelIndex: startDesaprobar,
                            totalSwitches: 2,
                            labels: const ['Sí', 'No'],
                            customTextStyles: const [
                              TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 14)
                            ],
                            onToggle: (index) {
                              if (index == 0) {
                                desaprobar = 'Sí';
                              } else {
                                desaprobar = 'No';
                              }
                            },
                          ),
                        ),
                        const Divider(
                          thickness: 1,
                        ),
                        ListTile(
                          title: const Text(
                            'Insultar',
                            textScaler: TextScaler.linear(1.0),
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 16),
                          ),
                          subtitle: RichText(
                            text: const TextSpan(
                              text: '',
                              style: TextStyle(
                                  fontSize: 14,
                                  color: CustomColors.primaryDark,
                                  height: 1.5),
                              children: <TextSpan>[
                                TextSpan(
                                  text:
                                      'Despreciar a la otra persona por lo que dice o hace.\n',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                TextSpan(text: 'Por ejemplo:\n'),
                                TextSpan(
                                  text: '   ·  ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                TextSpan(
                                  text: '“Eso te pasa por tonto”\n',
                                ),
                                TextSpan(
                                  text: '   ·  ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                TextSpan(
                                  text: '“Qué estupidez lo que escucho”\n',
                                ),
                                TextSpan(
                                  text: '   ·  ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                TextSpan(
                                  text: '“No eres capaz de nada mejor”',
                                ),
                              ],
                            ),
                          ),
                          trailing: ToggleSwitch(
                            minWidth: 50.0,
                            cornerRadius: 20.0,
                            activeBgColors: const [
                              [CustomColors.activeSwitch],
                              [CustomColors.orange3]
                            ],
                            activeFgColor: CustomColors.primaryDark,
                            inactiveBgColor: CustomColors.background,
                            inactiveFgColor: CustomColors.primaryDark,
                            borderWidth: 1,
                            borderColor: const [CustomColors.primaryDark],
                            initialLabelIndex: startInsultar,
                            totalSwitches: 2,
                            labels: const ['Sí', 'No'],
                            customTextStyles: const [
                              TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 14)
                            ],
                            onToggle: (index) {
                              if (index == 0) {
                                insultar = 'Sí';
                              } else {
                                insultar = 'No';
                              }
                            },
                          ),
                        ),
                        const Divider(
                          thickness: 1,
                        ),
                        ListTile(
                          title: const Text(
                            'Interpretar',
                            textScaler: TextScaler.linear(1.0),
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 16),
                          ),
                          subtitle: RichText(
                            text: const TextSpan(
                              text: '',
                              style: TextStyle(
                                  fontSize: 14,
                                  color: CustomColors.primaryDark,
                                  height: 1.5),
                              children: <TextSpan>[
                                TextSpan(
                                  text:
                                      'Decir a la otra persona el motivo oculto de su actitud.\n',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                TextSpan(text: 'Por ejemplo:\n'),
                                TextSpan(
                                  text: '   ·  ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                TextSpan(
                                  text:
                                      '“En el fondo quieres llamar la atención”\n',
                                ),
                                TextSpan(
                                  text: '   ·  ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                TextSpan(
                                  text: '“En realidad tú quieres otra cosa”',
                                ),
                              ],
                            ),
                          ),
                          trailing: ToggleSwitch(
                            minWidth: 50.0,
                            cornerRadius: 20.0,
                            activeBgColors: const [
                              [CustomColors.activeSwitch],
                              [CustomColors.orange3]
                            ],
                            activeFgColor: CustomColors.primaryDark,
                            inactiveBgColor: CustomColors.background,
                            inactiveFgColor: CustomColors.primaryDark,
                            borderWidth: 1,
                            borderColor: const [CustomColors.primaryDark],
                            initialLabelIndex: startInterpretar,
                            totalSwitches: 2,
                            labels: const ['Sí', 'No'],
                            customTextStyles: const [
                              TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 14)
                            ],
                            onToggle: (index) {
                              if (index == 0) {
                                interpretar = 'Sí';
                              } else {
                                interpretar = 'No';
                              }
                            },
                          ),
                        ),
                        const Divider(
                          thickness: 1,
                        ),
                        ListTile(
                          title: const Text(
                            'Interrogar',
                            textScaler: TextScaler.linear(1.0),
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 16),
                          ),
                          subtitle: RichText(
                            text: const TextSpan(
                              text: '',
                              style: TextStyle(
                                  fontSize: 14,
                                  color: CustomColors.primaryDark,
                                  height: 1.5),
                              children: <TextSpan>[
                                TextSpan(
                                  text:
                                      'Sacar información a la otra persona.\n',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                TextSpan(text: 'Por ejemplo:\n'),
                                TextSpan(
                                  text: '   ·  ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                TextSpan(
                                  text: '“¿Cuándo, dónde, por qué?”\n',
                                ),
                                TextSpan(
                                  text: '   ·  ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                TextSpan(
                                  text: '“¿Qué dijo de esta persona?”',
                                ),
                              ],
                            ),
                          ),
                          trailing: ToggleSwitch(
                            minWidth: 50.0,
                            cornerRadius: 20.0,
                            activeBgColors: const [
                              [CustomColors.activeSwitch],
                              [CustomColors.orange3]
                            ],
                            activeFgColor: CustomColors.primaryDark,
                            inactiveBgColor: CustomColors.background,
                            inactiveFgColor: CustomColors.primaryDark,
                            borderWidth: 1,
                            borderColor: const [CustomColors.primaryDark],
                            initialLabelIndex: startInterrogar,
                            totalSwitches: 2,
                            labels: const ['Sí', 'No'],
                            customTextStyles: const [
                              TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 14)
                            ],
                            onToggle: (index) {
                              if (index == 0) {
                                interrogar = 'Sí';
                              } else {
                                interrogar = 'No';
                              }
                            },
                          ),
                        ),
                        const Divider(
                          thickness: 1,
                        ),
                        ListTile(
                          title: const Text(
                            'Ironizar',
                            textScaler: TextScaler.linear(1.0),
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 16),
                          ),
                          subtitle: RichText(
                            text: const TextSpan(
                              text: '',
                              style: TextStyle(
                                  fontSize: 14,
                                  color: CustomColors.primaryDark,
                                  height: 1.5),
                              children: <TextSpan>[
                                TextSpan(
                                  text: 'Reírse de la otra persona.\n',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                TextSpan(text: 'Por ejemplo:\n'),
                                TextSpan(
                                  text: '   ·  ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                TextSpan(
                                  text:
                                      '“¡Claro! ¡Deja el trabajo tirado y ándate a carretear!”\n',
                                ),
                                TextSpan(
                                  text: '   ·  ',
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                TextSpan(
                                  text: '“¿Así actúan las mujeres?...”',
                                ),
                              ],
                            ),
                          ),
                          trailing: ToggleSwitch(
                            minWidth: 50.0,
                            cornerRadius: 20.0,
                            activeBgColors: const [
                              [CustomColors.activeSwitch],
                              [CustomColors.orange3]
                            ],
                            activeFgColor: CustomColors.primaryDark,
                            inactiveBgColor: CustomColors.background,
                            inactiveFgColor: CustomColors.primaryDark,
                            borderWidth: 1,
                            borderColor: const [CustomColors.primaryDark],
                            initialLabelIndex: startIronizar,
                            totalSwitches: 2,
                            labels: const ['Sí', 'No'],
                            customTextStyles: const [
                              TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 14)
                            ],
                            onToggle: (index) {
                              if (index == 0) {
                                ironizar = 'Sí';
                              } else {
                                ironizar = 'No';
                              }
                            },
                          ),
                        ),
                        const Divider(
                          thickness: 1,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );

      case 3:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 20, right: 20, left: 20, bottom: 5),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            '¿Qué actitudes marcaste? Un punto común de estas actitudes es que están centradas en el punto de vista personal, y no el de colaboración. Puedes volver a este ejercicio para revisar tus respuestas o modificarlas cuando desees. Te invitamos a evaluar estas actitudes en función de las recomendaciones previas.',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[],
                      ),
                    ),
                  ),
                  Container(
                    width: size.width,
                    margin: const EdgeInsets.only(top: 50, left: 20, bottom: 5),
                    child: const Text(
                      'Fuente:',
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(
                        fontSize: 22,
                        color: CustomColors.primary,
                      ),
                    ),
                  ),
                  Container(
                    width: size.width,
                    margin: const EdgeInsets.only(left: 20, bottom: 5),
                    child: const Text(
                      'Conceptos clave para la resolución pacífica de conflictos en el ámbito escolar.',
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(
                        fontSize: 16,
                        color: CustomColors.primary,
                      ),
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: const EdgeInsets.only(top: 5, bottom: 20, left: 20),
                    child: ElevatedButton.icon(
                      style: ButtonStyle(
                        backgroundColor: WidgetStateProperty.all<Color>(
                            CustomColors.primary80pink),
                        shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                          ),
                        ),
                      ),
                      onPressed: () {
                        _launchUrl();
                      },
                      icon: Icon(MdiIcons.openInNew,
                          color: CustomColors.primaryDark),
                      label: const Text(
                        'Ir al artículo',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            color: CustomColors.primaryDark,
                            decoration: TextDecoration.underline),
                      ), // <-- Text
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      default:
        return Container();
    }
  }
}
