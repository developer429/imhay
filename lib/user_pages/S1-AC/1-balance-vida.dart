// ignore_for_file: file_names, avoid_print

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:seguimiento/colors.dart';
import 'package:seguimiento/user_pages/MainPage.dart';
import '../../firebase.dart';

class A1 extends StatefulWidget {
  const A1({
    Key? key,
  }) : super(key: key);

  @override
  State<A1> createState() => _A1State();
}

class _A1State extends State<A1> {
  String uid = FirebaseAuth.instance.currentUser!.uid;
  bool loadingData = true;
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('page-control');
  String activeStepData = '';
  void getActivePageStored() {
    collectionReference.doc(uid).get().then((value) {
      var fields = value;
      activeStepData = fields['A1'];
      setState(() {
        if (int.parse(activeStepData) < 7) {
          activeStep = int.parse(activeStepData) - 1;
          Future.delayed(const Duration(milliseconds: 500), () {
            setState(() {
              loadingData = false;
            });
          });
        }
      });
    });
  }

  bool favorite = false;
  CollectionReference favoriteData =
      FirebaseFirestore.instance.collection('favoritos');
  void getFavData() {
    favoriteData.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        favorite = fields['A1'];
      });
    }).catchError((err) {
      print('Error: $err');
    });
  }

  ScrollController scrollController = ScrollController();

  int activeStep = 0;
  @override
  void initState() {
    super.initState();
    getActivePageStored();

    getFavData();
  }

  @override
  void dispose() {
    super.dispose();
  }

  bool active1 = false;
  bool active2 = false;
  bool active3 = false;
  bool active4 = false;
  bool active5 = false;
  bool active6 = false;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        active1 = true;
        active2 = false;
        active3 = false;
        active4 = false;
        active5 = false;
        active6 = false;

        break;
      case 1:
        active1 = true;
        active2 = true;
        active3 = false;
        active4 = false;
        active5 = false;
        active6 = false;
        break;
      case 2:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = false;
        active5 = false;
        active6 = false;
        break;
      case 3:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = false;
        active6 = false;
        break;
      case 4:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = true;
        active6 = false;
        break;
      case 5:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = true;
        active6 = true;
        break;
      default:
    }
    return PopScope(
      canPop: false,
      onPopInvokedWithResult: (bool didPop, result) {
        if (didPop) {
          FirestoreService()
              .updateCurrentPage('A1', (activeStep + 1).toString());
          return;
        }
      },
      child: loadingData
          ? Container(
              width: size.width,
              height: size.height,
              color: CustomColors.primaryDark,
              child: const Center(
                  child: CircularProgressIndicator(
                color: CustomColors.tertiary70,
              )))
          : Container(
              color: Colors.black,
              child: SafeArea(
                child: Scaffold(
                  backgroundColor: CustomColors.background,
                  body: Center(
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              width: size.width / 6,
                              height: 5,
                              color: active1
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 6,
                              height: 5,
                              color: active2
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 6,
                              height: 5,
                              color: active3
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 6,
                              height: 5,
                              color: active4
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 6,
                              height: 5,
                              color: active5
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 6,
                              height: 5,
                              color: active6
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                          ],
                        ),
                        content(),
                        Container(
                          margin: const EdgeInsets.only(top: 10, bottom: 10),
                          child: bottomStructure(),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
    );
  }

  Widget topStructure() {
    return Column(
      children: [
        Row(
          children: [
            Container(
              alignment: Alignment.topRight,
              child: IconButton(
                onPressed: () {
                  FirestoreService()
                      .updateCurrentPage('A1', (activeStep + 1).toString())
                      .then((value) => Navigator.of(context).pop());
                },
                icon: const Icon(
                  Icons.close,
                  color: CustomColors.primaryDark,
                ),
              ),
            ),
            const Spacer(),
            favorito(),
          ],
        ),
        Text(
          headerText(),
          textScaler: const TextScaler.linear(1.0),
          textAlign: TextAlign.center,
          style: const TextStyle(
            color: CustomColors.primaryDark,
            fontSize: 24,
          ),
        ),
        Container(
          margin: const EdgeInsets.only(top: 20, bottom: 20),
          child: header(),
        ),
      ],
    );
  }

  Widget favorito() {
    return StreamBuilder<QuerySnapshot>(
        stream: FirestoreService().favoritos(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasData) {
            return SizedBox(
              width: 50,
              height: 50,
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (context, index) {
                    var document = snapshot.data?.docs[index];
                    if (document != null && document.id == uid) {
                      switch (document['A1']) {
                        case true:
                          return IconButton(
                              icon: Icon(MdiIcons.star,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('A1', false)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Eliminado de favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                        case false:
                          return IconButton(
                              icon: Icon(MdiIcons.starOutline,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService().setFavorito('A1', true).then(
                                    (value) => Fluttertoast.showToast(
                                        msg: "Agregado a favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                      }
                    } else {
                      return Container();
                    }
                    return null;
                  }),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(
                color: Colors.red,
              ),
            );
          }
        });
  }

  Widget bottomStructure() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        nextButton(),
      ],
    );
  }

  Widget nextButton() {
    Size size = MediaQuery.of(context).size;
    if (activeStep >= 1 && activeStep < 6) {
      return SizedBox(
        width: size.width / 1,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        WidgetStateProperty.all<Color>(CustomColors.background),
                    shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: const BorderSide(color: CustomColors.primary)),
                    ),
                  ),
                  onPressed: () {
                    setState(() {
                      activeStep = activeStep - 1;
                    });
                  },
                  child: const Text(
                    'Anterior',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(color: CustomColors.primary),
                  )),
            ),
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor:
                      WidgetStateProperty.all<Color>(CustomColors.primary),
                  shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ),
                  ),
                ),
                onPressed: () {
                  if (activeStep < 5) {
                    setState(() {
                      activeStep++;
                    });
                  } else {
                    FirestoreService().updateCurrentPage('A1', '1');

                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute<void>(
                            builder: (BuildContext context) => const MainPage(
                                  activePage: 1,
                                )),
                        (route) => false);
                  }
                },
                child: Text(
                  buttonText(),
                  textScaler: const TextScaler.linear(1.0),
                  style: const TextStyle(
                      color: CustomColors.background,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      return SizedBox(
        width: size.width / 1.2,
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor:
                WidgetStateProperty.all<Color>(CustomColors.primary),
            shape: WidgetStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              ),
            ),
          ),
          onPressed: () async {
            if (activeStep < 5) {
              setState(() {
                activeStep++;
              });
            } else {
              FirestoreService().updateCurrentPage('A1', '1');

              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute<void>(
                      builder: (BuildContext context) => const MainPage(
                            activePage: 1,
                          )),
                  (route) => false);
            }
          },
          child: Text(
            buttonText(),
            textScaler: const TextScaler.linear(1.0),
            style: const TextStyle(color: CustomColors.background),
          ),
        ),
      );
    }
  }

  Widget header() {
    Size size = MediaQuery.of(context).size;
    switch (activeStep) {
      default:
        return Image(
          width: size.width / 3,
          height: size.width / 3,
          fit: BoxFit.scaleDown,
          image: AssetImage(imageHeader()),
        );
    }
  }

  String imageHeader() {
    switch (activeStep) {
      case 0:
        return 'assets/images/balance-vida-icon.png';

      default:
        return 'assets/images/balance-vida-icon.png';
    }
  }

  String headerText() {
    switch (activeStep) {
      case 0:
        return 'Balance de vida personal y universidad';

      default:
        return 'Balance de vida personal y universidad';
    }
  }

  String buttonText() {
    switch (activeStep) {
      case 5:
        return 'Finalizar';

      default:
        return 'Siguiente ';
    }
  }

  Widget content() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 20, right: 20, left: 20, bottom: 30),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            '¿Te ha pasado sentir que te falta tiempo para realizar tus deberes para la universidad? ¿Has sentido que no logras compartir con tus amistades, familia o cercanos como quisieras? La educación superior trae consigo una serie de desafíos y cambios en la vida cotidiana: nuevas experiencias, lugares, amistades, aprendizajes, responsabilidades, etc. Estos cambios pueden ser excitantes, pero a la vez pueden ser difíciles y estresantes. Así, puede que algunos cambios y/o desafíos no generen un gran impacto, pero quizás otros sí. Por esto es importante que seamos conscientes de los diferentes desafíos a los que nos enfrentamos, de modo tal que no descuidemos los distintos ámbitos relevantes de nuestra vida ',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                '(recuerda: ¡no eres solo un estudiante!)\n\n',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          TextSpan(
                            text:
                                'A continuación, te compartimos algunos consejos que podrán ayudarte a equilibrar de mejor forma tu vida universitaria con tu vida personal:',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 1:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                      width: size.width,
                      margin:
                          const EdgeInsets.only(top: 15, left: 20, bottom: 5),
                      child: const Text(
                        'Pon tu bienestar en primer lugar',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                          fontSize: 22,
                          color: CustomColors.primary,
                        ),
                      )),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: const EdgeInsets.only(
                        top: 5, right: 20, left: 20, bottom: 5),
                    child: RichText(
                      text: const TextSpan(
                        text: '',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Más allá de qué carrera estás estudiando y cuál sea el nivel de carga académica que esta conlleva, ',
                          ),
                          TextSpan(
                            text:
                                'no podrás estudiar de manera efectiva si no cuidas de tu bienestar. ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Ninguna tarea, trabajo, prueba, etc., es más importante que tu bienestar. Es por esto por lo cual es importante que a lo largo del semestre prestes atención a tus necesidades físicas, emocionales y mentales. De esta forma, podrás cuidar de tu bienestar y mantener un balance saludable entre tus distintas actividades, responsabilidades e intereses.\n\n',
                          ),
                          TextSpan(
                            text:
                                '   ·  Este balance es aún más importante durante periodos de estrés, ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                '(ej. semanas de evaluaciones, cierres de semestre, etc.). Es sabido que durante dichos períodos, los niveles de estrés y malestar de lxs estudiantes pueden aumentar hasta el punto de afectar negativamente su salud y bienestar. Así que, más allá de las demandas que debas enfrentar durante esos periodos, recuerda que tu bienestar es lo primero.\n\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Algunas señales que te pueden estar indicando que es necesario trabajar en un balance más saludable son: sentir cansancio excesivo, enfermarse con mayor frecuencia, faltar continuamente a clases, disminuir de manera drástica tu desempeño académico, etc.',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 2:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                      width: size.width,
                      margin: const EdgeInsets.only(
                          top: 15, left: 20, bottom: 5, right: 10),
                      child: const Text(
                        'Sé realista',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                          fontSize: 22,
                          color: CustomColors.primary,
                        ),
                      )),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: const EdgeInsets.only(
                        top: 5, right: 20, left: 20, bottom: 5),
                    child: RichText(
                      text: const TextSpan(
                        text: '',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Estudiar en la educación superior requiere adaptarse a constantes cambios, por lo cual enfrentarte a desafíos y dificultades al intentar mantener una rutina balanceada es esperable. ',
                          ),
                          TextSpan(
                            text: 'Recuerda que no estás solx, ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'tus compañerxs se enfrentan a los mismos desafíos y ',
                          ),
                          TextSpan(
                            text:
                                'nadie mantiene un balance perfecto y de manera sencilla.\n\n',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Al momento de buscar ese balance procura ser realista sin esperar “tenerlo todo de una vez” y',
                          ),
                          TextSpan(
                            text:
                                ' tomándote el tiempo que necesites para encontrar el mejor ritmo para ti y tu bienestar. ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Por otra parte, ser realista también implica mantener cierto grado de',
                          ),
                          TextSpan(
                            text:
                                ' flexibilidad para poder responder a los cambios inesperados. ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Así, es importante que comprendas que en ciertos momentos la balanza se inclinará más hacia los estudios, otras veces se inclinará hacia la vida social, etc.',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );

      case 3:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                      width: size.width,
                      margin:
                          const EdgeInsets.only(top: 15, left: 20, bottom: 5),
                      child: const Text(
                        'Toma el control de tu tiempo y establece metas y prioridades',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                          fontSize: 22,
                          color: CustomColors.primary,
                        ),
                      )),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: const EdgeInsets.only(
                        top: 5, right: 20, left: 20, bottom: 5),
                    child: RichText(
                      text: const TextSpan(
                        text: '',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                '   ·  Mantener un balance en gran medida implica planificar con tiempo y tener claridad respecto de cuáles son tus prioridades. ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Para lograr esto necesitas establecer cuáles serán tus metas (personales, académicas, etc.) a principio de cada semestre o periodo. Esto te ayudará a mantener la motivación, organizarte mejor e invertir tiempo en aquellas cosas que consideras más importantes.\n\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Para organizar tu tiempo y actividades puedes emplear diversas estrategias y recursos (ej. libretas, planificadores semanales, etc.), pero ten presente que cada persona es diferente y por lo tanto debes emplear aquellas estrategias y/o recursos que más se ajusten a tus preferencias y necesidades.',
                          ),
                          TextSpan(
                            text:
                                ' Encontrar la estrategia que más se ajuste a ti también te ayudará a monitorear tus actividades a lo largo del periodo académico, pudiendo identificar cuando estas se encuentran desbalanceadas.',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' Esto te permitirá realizar ajustes de forma oportuna, de manera tal que tu vida personal no impacte de manera negativa en tu desempeño académico y que tus actividades académicas no impacten de manera negativa en tu bienestar y salud mental.',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 4:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                      width: size.width,
                      margin: const EdgeInsets.only(
                        top: 15,
                        left: 20,
                        bottom: 5,
                        right: 10,
                      ),
                      child: const Text(
                        'No dejes de lado tus relaciones interpersonales',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                          fontSize: 22,
                          color: CustomColors.primary,
                        ),
                      )),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: const EdgeInsets.only(
                        top: 5, right: 20, left: 20, bottom: 5),
                    child: RichText(
                      text: const TextSpan(
                        text: '',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'En ciertos periodos del semestre o año puede ser fácil sentir la necesidad de pasar semanas completas dedicándote exclusivamente a tus estudios. Sin embargo, esto puede afectar tus relaciones personales (familiares, amistades y/o de pareja) y tener un impacto negativo en tu salud mental. De esta forma,',
                          ),
                          TextSpan(
                            text:
                                ' para mantener un balance saludable es importante que aprendas a poner límites, es decir, aprender a cuándo decir que no y cuándo decir que si.\n\n',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Decir que no a un plan o actividad puede ser difícil, pero es un aspecto fundamental para no comprometerte en demasiadas actividades y terminar agotándote al intentar manejar más de lo que es posible. Así, es importante que aprendas a ',
                          ),
                          TextSpan(
                            text:
                                'reconocer cuándo lo mejor para ti es decir que no,',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' ya sea porque tienes que estudiar para una prueba, terminar un trabajo o porque necesitas tiempo para ti y para descansar. Por otra parte, también es importante ',
                          ),
                          TextSpan(
                            text:
                                'reconocer cuándo lo mejor para ti es decir que sí ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'a un plan o actividad. Pasar tiempo con tus seres queridos puede ser una excelente forma de relajarte y una oportunidad de hablar de aquello que te preocupa con personas en las que confías.\n\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Para aprender a reconocer y establecer límites que te ayuden a cuidar de tu bienestar y salud mental ',
                          ),
                          TextSpan(
                            text:
                                'debes ser clarx (contigo mismx y con quienes te rodean) acerca de tus necesidades y prioridades.',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' Una estrategia que puede ayudarte en este proceso es compartir información acerca de tus horarios, rutina y carga académica a tus seres queridos. Esto permitirá que tus seres queridos puedan tener expectativas claras de tus tiempos, seguir siendo parte de tu vida cotidiana y apoyarte en los momentos de mayor estrés.',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 5:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                      width: size.width,
                      margin: const EdgeInsets.only(
                        top: 15,
                        left: 20,
                        bottom: 5,
                        right: 10,
                      ),
                      child: const Text(
                        'Busca apoyo',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                          fontSize: 22,
                          color: CustomColors.primary,
                        ),
                      )),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 20, right: 20, left: 20, bottom: 30),
                    child: RichText(
                      text: const TextSpan(
                        text: '',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Uno de los aspectos más fundamentales para mantener un balance saludable y cuidar de tu bienestar y salud mental,',
                          ),
                          TextSpan(
                            text:
                                ' es aprender a reconocer cuándo necesitas apoyo y buscar ayuda si la necesitas.\n\n',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'En estudiantes de educación superior es común escuchar frases tales como “mis problemas no son tan relevantes”, “no quiero ser una carga para otrxs”, “prefiero solucionar las cosas por mi propia cuenta”, etc. ',
                          ),
                          TextSpan(
                            text:
                                'Este tipo de creencias impide que las personas reciban el apoyo que necesitan cuándo lo necesitan, ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'lo que puede llevar a que los problemas y/o el malestar empeoren con el tiempo. ',
                          ),
                          TextSpan(
                            text:
                                'Todos merecemos apoyo y no existen reglas que determinen cuándo un problema es “lo suficientemente importante” para recibirlo.',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' De esta forma, en ocasiones puede que necesitemos apoyo para enfrentar situaciones de vida más complejas, pero en otros momentos necesitaremos apoyo para enfrentar el estrés en situaciones cotidianas. ',
                          ),
                          TextSpan(
                            text:
                                'Por eso es importante que busques ayuda si te sientes estresadx, abrumadx o no estás logrando mantener un balance que aporte a tu bienestar.\n\n',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                '   ·  Recuerda que hay gente que quiere y puede ayudarte. ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Puedes hablar con tus compañerxs, amigxs, familiares, pareja, etc. acerca de tus preocupaciones y/o dificultades. También puedes acercarte a profesores, personal de apoyo estudiantil, etc., ya que ellxs tienen experiencia apoyando a estudiantes en situaciones difíciles. Además, también puedes buscar apoyo en profesionales de salud mental ',
                          ),
                          TextSpan(
                            text:
                                '(explora la sección Buscar Ayuda para que veas los recursos de Vamos Juntxs tiene para ti). ',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              decoration: TextDecoration.underline,
                            ),
                          ),
                          TextSpan(
                            text:
                                'Mientras más pronto reconozcas que requieres apoyo y busques ayuda, más pronto obtendrás el apoyo que necesitas para cuidar de tu bienestar y salud mental.',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      default:
        return Container();
    }
  }
}
