// ignore_for_file: file_names, avoid_print

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:seguimiento/colors.dart';
import 'package:seguimiento/user_pages/MainPage.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../firebase.dart';

class A7 extends StatefulWidget {
  const A7({
    Key? key,
  }) : super(key: key);

  @override
  State<A7> createState() => _A7State();
}

class _A7State extends State<A7> {
  String uid = FirebaseAuth.instance.currentUser!.uid;
  bool loadingData = true;
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('page-control');
  String activeStepData = '';
  void getActivePageStored() {
    collectionReference.doc(uid).get().then((value) {
      var fields = value;
      activeStepData = fields['A7'];
      setState(() {
        if (int.parse(activeStepData) < 6) {
          activeStep = int.parse(activeStepData) - 1;
          Future.delayed(const Duration(milliseconds: 500), () {
            setState(() {
              loadingData = false;
            });
          });
        }
      });
    });
  }

  final Uri _url = Uri.parse(
      'https://www.ehu.eus/documents/1904000/1916168/19+Gu%C3%ADa+Trabajo+Equipos.pdf');

  ScrollController scrollController = ScrollController();

  bool favorite = false;
  CollectionReference favoriteData =
      FirebaseFirestore.instance.collection('favoritos');
  void getFavData() {
    favoriteData.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        favorite = fields['A7'];
      });
    }).catchError((err) {
      print('Error: $err');
    });
  }

  int activeStep = 0;
  @override
  void initState() {
    super.initState();
    getActivePageStored();

    getFavData();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> _launchUrl() async {
    if (!await launchUrl(_url)) {
      throw Exception('Could not launch $_url');
    }
  }

  bool active1 = false;
  bool active2 = false;
  bool active3 = false;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    switch (activeStep) {
      case 0:
        active1 = true;
        active2 = false;
        active3 = false;
        break;
      case 1:
        active1 = true;
        active2 = true;
        active3 = false;

        break;
      case 2:
        active1 = true;
        active2 = true;
        active3 = true;

        break;

      default:
    }
    return PopScope(
      canPop: false,
      onPopInvokedWithResult: (bool didPop, result) {
        if (didPop) {
          FirestoreService()
              .updateCurrentPage('A7', (activeStep + 1).toString());
          return;
        }
      },
      child: loadingData
          ? Container(
              width: size.width,
              height: size.height,
              color: CustomColors.primaryDark,
              child: const Center(
                  child: CircularProgressIndicator(
                color: CustomColors.tertiary70,
              )))
          : Container(
              color: Colors.black,
              child: SafeArea(
                child: Scaffold(
                  backgroundColor: CustomColors.background,
                  body: Center(
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              width: size.width / 3,
                              height: 5,
                              color: active1
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 3,
                              height: 5,
                              color: active2
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 3,
                              height: 5,
                              color: active3
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                          ],
                        ),
                        content(),
                        Container(
                          margin: const EdgeInsets.only(top: 10, bottom: 10),
                          child: bottomStructure(),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
    );
  }

  Widget topStructure() {
    return Column(
      children: [
        Row(
          children: [
            Container(
              alignment: Alignment.topRight,
              child: IconButton(
                onPressed: () {
                  FirestoreService()
                      .updateCurrentPage('A7', (activeStep + 1).toString())
                      .then((value) => Navigator.of(context).pop());
                },
                icon: const Icon(
                  Icons.close,
                  color: CustomColors.primaryDark,
                ),
              ),
            ),
            const Spacer(),
            favorito(),
          ],
        ),
        Text(
          headerText(),
          textScaler: const TextScaler.linear(1.0),
          textAlign: TextAlign.center,
          style: const TextStyle(
            color: CustomColors.primaryDark,
            fontSize: 24,
          ),
        ),
        Container(
          margin: const EdgeInsets.only(top: 20, bottom: 20),
          child: header(),
        ),
      ],
    );
  }

  Widget favorito() {
    return StreamBuilder<QuerySnapshot>(
        stream: FirestoreService().favoritos(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasData) {
            return SizedBox(
              width: 50,
              height: 50,
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (context, index) {
                    var document = snapshot.data?.docs[index];
                    if (document != null && document.id == uid) {
                      switch (document['A7']) {
                        case true:
                          return IconButton(
                              icon: Icon(MdiIcons.star,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('A7', false)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Eliminado de favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                        case false:
                          return IconButton(
                              icon: Icon(MdiIcons.starOutline,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService().setFavorito('A7', true).then(
                                    (value) => Fluttertoast.showToast(
                                        msg: "Agregado a favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                      }
                    } else {
                      return Container();
                    }
                    return null;
                  }),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(
                color: Colors.red,
              ),
            );
          }
        });
  }

  Widget bottomStructure() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        nextButton(),
      ],
    );
  }

  Widget nextButton() {
    Size size = MediaQuery.of(context).size;
    if (activeStep >= 1 && activeStep < 3) {
      return SizedBox(
        width: size.width / 1,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        WidgetStateProperty.all<Color>(CustomColors.background),
                    shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: const BorderSide(color: CustomColors.primary)),
                    ),
                  ),
                  onPressed: () {
                    setState(() {
                      activeStep = activeStep - 1;
                    });
                  },
                  child: const Text(
                    'Anterior',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(color: CustomColors.primary),
                  )),
            ),
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor:
                      WidgetStateProperty.all<Color>(CustomColors.primary),
                  shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ),
                  ),
                ),
                onPressed: () {
                  if (activeStep < 2) {
                    setState(() {
                      activeStep++;
                    });
                  } else {
                    FirestoreService().updateCurrentPage('A7', '1');

                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute<void>(
                            builder: (BuildContext context) => const MainPage(
                                  activePage: 1,
                                )),
                        (route) => false);
                  }
                },
                child: Text(
                  buttonText(),
                  textScaler: const TextScaler.linear(1.0),
                  style: const TextStyle(
                      color: CustomColors.background,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      return SizedBox(
        width: size.width / 1.2,
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor:
                WidgetStateProperty.all<Color>(CustomColors.primary),
            shape: WidgetStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              ),
            ),
          ),
          onPressed: () {
            if (activeStep < 2) {
              setState(() {
                activeStep++;
              });
            } else {
              FirestoreService().updateCurrentPage('A7', '1');

              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute<void>(
                      builder: (BuildContext context) => const MainPage(
                            activePage: 1,
                          )),
                  (route) => false);
            }
            /*
            */
          },
          child: Text(
            buttonText(),
            textScaler: const TextScaler.linear(1.0),
            style: const TextStyle(color: CustomColors.background),
          ),
        ),
      );
    }
  }

  Widget header() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      default:
        return Image(
          width: size.width / 3,
          height: size.width / 3,
          fit: BoxFit.scaleDown,
          image: AssetImage(imageHeader()),
        );
    }
  }

  String imageHeader() {
    switch (activeStep) {
      case 0:
        return 'assets/images/trabajos-grupo-icon.png';

      default:
        return 'assets/images/trabajos-grupo-icon.png';
    }
  }

  String headerText() {
    switch (activeStep) {
      case 0:
        return 'Trabajando en grupos: desafíos y oportunidades';

      default:
        return 'Trabajando en grupos: desafíos y oportunidades';
    }
  }

  String buttonText() {
    switch (activeStep) {
      case 2:
        return 'Finalizar';

      default:
        return 'Siguiente ';
    }
  }

  Widget content() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 20, right: 20, left: 20, bottom: 20),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Es común que en las asignaturas que curses durante tu etapa universitaria tengas que trabajar en grupo para desarrollar alguna actividad (ej.: entregar un informe o una presentación oral sobre algún tema que se aborde en clases, etc.). Las dinámicas de aprendizaje cooperativo tienen diversos beneficios tales como potenciar la adquisición y retención de conocimiento, así como también promover el desarrollo de habilidades de comunicación y solución de problemas. De esta forma, trabajar en grupo puede ser una valiosa oportunidad para adquirir herramientas que aporten a tu desarrollo personal y profesional. Sin embargo, trabajar en grupo también supone enfrentar desafíos que pueden traducirse en conflictos que pueden impactar negativamente en el aprendizaje y desempeño académico. Algunos de estos desafíos son:\n\n',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Lidiar con diferencias de personalidad, intereses, estilos de aprendizaje, fortalezas y debilidades de entre lxs integrantes del grupo.\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Establecer roles, responsabilidades y reglas claras para el funcionamiento del grupo.\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Determinar tiempos, plazos y lugares en los cuales se llevará a cabo la actividad grupal.\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Promover una contribución equitativa entre integrantes, evitando percepciones de desigualdad respecto del esfuerzo invertido por cada integrante.\n\n',
                          ),
                          TextSpan(
                            text:
                                'Frente a estas situaciones, sostener una actitud cooperativa puede ser una herramienta que facilite superar estas diferencias y prevenir que estas escalen a conflictos o problemas. La cooperación puede entenderse como ',
                          ),
                          TextSpan(
                              text:
                                  'acciones de reconocimiento y valoración de las diferencias sobre los modos de actuar y pensar, y por consiguiente, de los distintos aportes y esfuerzos de cada uno de sus miembros.',
                              style: TextStyle(fontStyle: FontStyle.italic)),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 1:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: const EdgeInsets.only(
                        top: 5, right: 20, left: 20, bottom: 5),
                    child: RichText(
                      text: const TextSpan(
                        text: '',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                'Algunos consejos que te ayudarán a mantener la actitud cooperativa en tus grupos de trabajo son:\n\n',
                          ),
                          TextSpan(
                            text: '   ·  Respetar las ideas de otros:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' respetar las ideas de otrxs, ser honestx y permitir que todos expresen su opinión ayudará a negociar y llegar a acuerdos respecto a cuáles serán las metas comunes.\n\n',
                          ),
                          TextSpan(
                            text: '   ·  Contribuir de forma equitativa:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' compartir la responsabilidad para alcanzar las metas y poner límites permite promover el trabajo colaborativo y previene la generación de situaciones en las cuales lxs integrantes no invierten el mismo esfuerzo en la tarea.\n\n',
                          ),
                          TextSpan(
                            text: '   ·  Mostrar apertura:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' no te cierres a las diferencias (de personalidad, estilos de aprendizaje, etc.) que tengas con lxs integrantes, toma en cuenta sus perspectivas y ayuda a que los desacuerdos se resuelvan de forma colaborativa (ej. votando, elaborando consensos).\n\n',
                          ),
                          TextSpan(
                            text: '   ·  Comunícate de manera efectiva:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' expresa las ideas y/o inquietudes que tengas respecto a la realización de la actividad, no interrumpas y escucha las ideas y/o inquietudes de lxs integrantes y entrega retroalimentaciones constructivas.\n\n',
                          ),
                          TextSpan(
                            text: '   ·  Maneja tu tiempo:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' sé puntual para llegar a reuniones (de organización y/o trabajo) del grupo, sé flexible para acordar horarios y establece límites que consideren el resto de tus actividades.',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );

      case 2:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 20, right: 20, left: 20, bottom: 5),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Sumado a lo anterior, todos los miembros son responsables del reconocimiento que se ofrece a los demás miembros del equipo. El reconocimiento mutuo es un valor que expresa la satisfacción y el sentimiento de agradecimiento que tienen los miembros del equipo.\n\n',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                'Algunas frases de reconocimiento y valoración que puedes utilizar son:\n\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text: 'Gracias por compartir tu punto de vista.\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text: 'No había considerado eso que dices.\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text: 'Tu trabajo es importante para nosotros.\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Me importa lo que puedas pensar sobre este tema.\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text: 'Qué valioso eso que dices.\n\n',
                          ),
                          TextSpan(
                            text:
                                'Ten en cuenta que cuando se trata de evaluar el aporte que realiza cada miembro al rendimiento del equipo, el único criterio objetivo es el tiempo dedicado por cada miembro al trabajo. Sin embargo, debe quedar muy claro que el tiempo individual dedicado a las tareas del equipo no es una medida exacta del esfuerzo subjetivo realizado por cada individuo, ni del valor de las aportaciones de cada miembro al resultado final del equipo.',
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    width: size.width,
                    margin: const EdgeInsets.only(top: 50, left: 20, bottom: 5),
                    child: const Text(
                      'Fuente:',
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(
                        fontSize: 22,
                        color: CustomColors.primary,
                      ),
                    ),
                  ),
                  Container(
                    width: size.width,
                    margin: const EdgeInsets.only(left: 20, bottom: 5),
                    child: const Text(
                      'Guía para el trabajo en equipo',
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(
                        fontSize: 16,
                        color: CustomColors.primary,
                      ),
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: const EdgeInsets.only(top: 5, bottom: 20, left: 20),
                    child: ElevatedButton.icon(
                      style: ButtonStyle(
                        backgroundColor: WidgetStateProperty.all<Color>(
                            CustomColors.primary80pink),
                        shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                          ),
                        ),
                      ),
                      onPressed: () {
                        _launchUrl();
                      },
                      icon: Icon(MdiIcons.openInNew,
                          color: CustomColors.primaryDark),
                      label: const Text(
                        'Ir al artículo',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            color: CustomColors.primaryDark,
                            decoration: TextDecoration.underline),
                      ), // <-- Text
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      default:
        return Container();
    }
  }
}
