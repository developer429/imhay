// ignore_for_file: file_names, avoid_print

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:seguimiento/colors.dart';
import 'package:seguimiento/user_pages/MainPage.dart';
import 'package:seguimiento/video-player.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../firebase.dart';

class A4 extends StatefulWidget {
  const A4({
    Key? key,
  }) : super(key: key);

  @override
  State<A4> createState() => _A4State();
}

class _A4State extends State<A4> {
  String uid = FirebaseAuth.instance.currentUser!.uid;
  bool loadingData = true;
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('page-control');
  String activeStepData = '';
  void getActivePageStored() {
    collectionReference.doc(uid).get().then((value) {
      var fields = value;
      activeStepData = fields['A4'];
      setState(() {
        if (int.parse(activeStepData) < 6) {
          activeStep = int.parse(activeStepData) - 1;
          Future.delayed(const Duration(milliseconds: 500), () {
            setState(() {
              loadingData = false;
            });
          });
        }
      });
    });
  }

  String videoA3 =
      'https://firebasestorage.googleapis.com/v0/b/maquetadatos-5dee1.appspot.com/o/videos%2Fpoder-autoinstrucciones.mp4?alt=media&token=0579b70e-b20d-403c-9382-18b1f081f180';
  final Uri _url1 =
      Uri.parse('https://youtu.be/BC5g6vO2SvI?si=QVVbQseUkTfPIPIB');
  final Uri _url2 = Uri.parse(
      'https://concienciasaludable.uchile.cl/2023/10/22/que-es-y-como-manejar-la-ansiedad-academica/');

  ScrollController scrollController = ScrollController();

  bool favorite = false;
  CollectionReference favoriteData =
      FirebaseFirestore.instance.collection('favoritos');
  void getFavData() {
    favoriteData.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        favorite = fields['A4'];
      });
    }).catchError((err) {
      print('Error: $err');
    });
  }

  int activeStep = 0;
  @override
  void initState() {
    super.initState();
    getActivePageStored();

    getFavData();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> _launchUrl(_url) async {
    if (!await launchUrl(_url)) {
      throw Exception('Could not launch $_url');
    }
  }

  bool active1 = false;
  bool active2 = false;
  bool active3 = false;
  bool active4 = false;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    switch (activeStep) {
      case 0:
        active1 = true;
        active2 = false;
        active3 = false;
        active4 = false;
        break;
      case 1:
        active1 = true;
        active2 = true;
        active3 = false;
        active4 = false;

        break;
      case 2:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = false;

        break;
      case 3:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;

        break;

      default:
    }
    return PopScope(
      canPop: false,
      onPopInvokedWithResult: (bool didPop, result) {
        if (didPop) {
          FirestoreService()
              .updateCurrentPage('A4', (activeStep + 1).toString());
          return;
        }
      },
      child: loadingData
          ? Container(
              width: size.width,
              height: size.height,
              color: CustomColors.primaryDark,
              child: const Center(
                  child: CircularProgressIndicator(
                color: CustomColors.tertiary70,
              )))
          : Container(
              color: Colors.black,
              child: SafeArea(
                child: Scaffold(
                  backgroundColor: CustomColors.background,
                  body: Center(
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              width: size.width / 4,
                              height: 5,
                              color: active1
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 4,
                              height: 5,
                              color: active2
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 4,
                              height: 5,
                              color: active3
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 4,
                              height: 5,
                              color: active4
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                          ],
                        ),
                        content(),
                        Container(
                          margin: const EdgeInsets.only(top: 10, bottom: 10),
                          child: bottomStructure(),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
    );
  }

  Widget topStructure() {
    return Column(
      children: [
        Row(
          children: [
            Container(
              alignment: Alignment.topRight,
              child: IconButton(
                onPressed: () {
                  FirestoreService()
                      .updateCurrentPage('A4', (activeStep + 1).toString())
                      .then((value) => Navigator.of(context).pop());
                },
                icon: const Icon(
                  Icons.close,
                  color: CustomColors.primaryDark,
                ),
              ),
            ),
            const Spacer(),
            favorito(),
          ],
        ),
        Text(
          headerText(),
          textScaler: const TextScaler.linear(1.0),
          style: const TextStyle(
            color: CustomColors.primaryDark,
            fontSize: 24,
          ),
        ),
        Container(
          margin: const EdgeInsets.only(top: 20, bottom: 20),
          child: header(),
        ),
      ],
    );
  }

  Widget favorito() {
    return StreamBuilder<QuerySnapshot>(
        stream: FirestoreService().favoritos(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasData) {
            return SizedBox(
              width: 50,
              height: 50,
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (context, index) {
                    var document = snapshot.data?.docs[index];
                    if (document != null && document.id == uid) {
                      switch (document['A4']) {
                        case true:
                          return IconButton(
                              icon: Icon(MdiIcons.star,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('A4', false)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Eliminado de favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                        case false:
                          return IconButton(
                              icon: Icon(MdiIcons.starOutline,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService().setFavorito('A4', true).then(
                                    (value) => Fluttertoast.showToast(
                                        msg: "Agregado a favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                      }
                    } else {
                      return Container();
                    }
                    return null;
                  }),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(
                color: Colors.red,
              ),
            );
          }
        });
  }

  Widget bottomStructure() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        nextButton(),
      ],
    );
  }

  Widget nextButton() {
    Size size = MediaQuery.of(context).size;
    if (activeStep >= 1 && activeStep < 4) {
      return SizedBox(
        width: size.width / 1,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        WidgetStateProperty.all<Color>(CustomColors.background),
                    shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: const BorderSide(color: CustomColors.primary)),
                    ),
                  ),
                  onPressed: () {
                    setState(() {
                      activeStep = activeStep - 1;
                    });
                  },
                  child: const Text(
                    'Anterior',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(color: CustomColors.primary),
                  )),
            ),
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor:
                      WidgetStateProperty.all<Color>(CustomColors.primary),
                  shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ),
                  ),
                ),
                onPressed: () {
                  if (activeStep < 3) {
                    setState(() {
                      activeStep++;
                    });
                  } else {
                    FirestoreService().updateCurrentPage('A4', '1');

                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute<void>(
                            builder: (BuildContext context) => const MainPage(
                                  activePage: 1,
                                )),
                        (route) => false);
                  }
                },
                child: Text(
                  buttonText(),
                  textScaler: const TextScaler.linear(1.0),
                  style: const TextStyle(
                      color: CustomColors.background,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      return SizedBox(
        width: size.width / 1.2,
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor:
                WidgetStateProperty.all<Color>(CustomColors.primary),
            shape: WidgetStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              ),
            ),
          ),
          onPressed: () {
            if (activeStep < 3) {
              setState(() {
                activeStep++;
              });
            } else {
              FirestoreService().updateCurrentPage('A4', '1');

              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute<void>(
                      builder: (BuildContext context) => const MainPage(
                            activePage: 1,
                          )),
                  (route) => false);
            }
            /*
            */
          },
          child: Text(
            buttonText(),
            textScaler: const TextScaler.linear(1.0),
            style: const TextStyle(color: CustomColors.background),
          ),
        ),
      );
    }
  }

  Widget header() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      default:
        return Image(
          width: size.width / 3,
          height: size.width / 3,
          fit: BoxFit.scaleDown,
          image: AssetImage(imageHeader()),
        );
    }
  }

  String imageHeader() {
    switch (activeStep) {
      case 0:
        return 'assets/images/afrontar-estres-icon.png';

      default:
        return 'assets/images/afrontar-estres-icon.png';
    }
  }

  String headerText() {
    switch (activeStep) {
      case 0:
        return 'Afrontando el estrés académico';

      default:
        return 'Afrontando el estrés académico';
    }
  }

  String buttonText() {
    switch (activeStep) {
      case 3:
        return 'Finalizar';

      default:
        return 'Siguiente ';
    }
  }

  Widget content() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 20, right: 20, left: 20, bottom: 20),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Uno de los desafíos más comunes que lxs estudiantes de educación superior deben enfrentar corresponde a poder sobrellevar el estrés académico. La educación superior trae consigo cambios en la relación con los procesos de aprendizaje, requiriendo de ',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                'mayor organización y un aumento en el tiempo de estudio,',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text: ' y con ello, la necesidad de formar ',
                          ),
                          TextSpan(
                            text: 'nuevas estrategias de aprendizaje.\n\n',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text: 'Frente a estos desafíos,',
                          ),
                          TextSpan(
                            text:
                                ' cierta cantidad de estrés es esperable e incluso puede ayudarte a mantener tu motivación e impulsarte a enfrentar los desafíos académicos',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' que se te presentan. Sin embargo, dado que sentir estrés (especialmente en los momentos con mayor carga académica) es esperable, muchas veces pasamos por alto cuando este está llegando a',
                          ),
                          TextSpan(
                            text: ' niveles problemáticos. ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text: 'De esta forma,',
                          ),
                          TextSpan(
                            text:
                                ' niveles muy elevados de ansiedad y estrés académico',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' pueden afectar la percepción sobre las propias habilidades e imposibilitar enfrentar la tarea de manera óptima.\n\n',
                          ),
                          TextSpan(
                            text:
                                'Algunas de las circunstancias que pueden aumentar los niveles de estrés académico corresponden a:\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Expectativas y metas poco realistas respecto al desempeño académico.\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Temor a decepcionar a la familia (ej. estudiantes de primera generación).\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Preocupaciones financieras respecto a los costos de estudiar (ej. créditos universitarios, carga financiera a nivel familiar, etc.).\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Estar recibiendo una beca o beneficio que se encuentre condicionado a notas o cantidad de créditos aprobados.\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Tener una carga académica excesiva (ej. haber inscrito muchos cursos).\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Tener dificultades respecto a la organización y priorización de las actividades académicas.\n\n',
                          ),
                          TextSpan(
                            text:
                                'Experimentar niveles elevados de estrés académico además puede traducirse en una reducción en la motivación por los estudios, un impacto negativo en el desempeño académico y una mayor probabilidad de deserción. Además, los niveles elevados de estrés se asocian a problemas de sueño, depresión, ansiedad, consumo problemático de sustancias, etc.\n\n',
                          ),
                          TextSpan(
                            text:
                                'A partir de lo anterior, resulta importante que, además de intentar manejar una rutina balanceada, puedas aprender a manejar el estrés académico de la mejor forma posible y reconocer cuando este está llegando a niveles que ponen en riesgo tu bienestar y salud.',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 1:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                      width: size.width,
                      margin: const EdgeInsets.only(
                          top: 15, left: 20, bottom: 5, right: 10),
                      child: const Text(
                        '¿Cómo manejar la ansiedad académica en el estudio?',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                          fontSize: 22,
                          color: CustomColors.primary,
                        ),
                      )),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: const EdgeInsets.only(
                        top: 5, right: 20, left: 20, bottom: 5),
                    child: RichText(
                      text: const TextSpan(
                        text: '',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                'Te compartimos algunos consejos que te pueden ser de utilidad para manejar tu ansiedad durante el estudio:\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Agenda tus evaluaciones y planifica el estudio con anticipación, considerando espacios de descanso y recreación.\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Identifica los horarios en que rindes mejor y el tiempo que logras mantener la concentración en el estudio.\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Activa tus conocimientos previos sobre la temática y conéctalos con los nuevos contenidos.\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Identifica y cuestiona los pensamientos negativos (ej.: “no lo voy a lograr”, “esto es muy difícil”, “siempre me ha costado esta materia”, “no lograré terminar la carrera”).\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Respeta tu agotamiento. Si requieres una pausa es mejor realizarla a tiempo y luego continuar con el estudio. Tomar descansos con regularidad disminuye el estrés y ayuda potenciar la atención y concentración, permitiéndote estudiar de manera más efectiva.\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Enfócate en tu aprendizaje. Aunque pueda ser difícil olvidarse de las notas dada la cultura de la educación superior, se ha observado que mantener el foco en el aprendizaje potencia la motivación y el desempeño académico.\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Cuida de tu bienestar por medio de una rutina saludable de alimentación, ejercicio y sueño. Esto potenciará tu nivel de energía y concentración en tus actividades académicas cotidianas.\n\n',
                          ),
                          TextSpan(
                            text:
                                'En la siguiente vista, te compartimos un video con una estrategia para enfrentar la ansiedad que podría serte útil al enfrentar los estudios en la universidad:',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 2:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    width: size.width / 1.2,
                    height: size.height / 4,
                    margin: const EdgeInsets.only(top: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: CustomColors.primary80pink,
                    ),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        const Center(
                          child: CircularProgressIndicator(
                            color: CustomColors.orange,
                          ),
                        ),
                        VideoPlayerURL(url: videoA3),
                      ],
                    ),
                  ),
                  Container(
                    width: size.width,
                    margin: const EdgeInsets.only(top: 50, left: 20, bottom: 5),
                    child: const Text(
                      'Fuente (YouTube):',
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(
                        fontSize: 22,
                        color: CustomColors.primary,
                      ),
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: const EdgeInsets.only(top: 5, bottom: 20, left: 20),
                    child: ElevatedButton.icon(
                      style: ButtonStyle(
                        backgroundColor: WidgetStateProperty.all<Color>(
                            CustomColors.primary80pink),
                        shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                          ),
                        ),
                      ),
                      onPressed: () {
                        _launchUrl(_url1);
                      },
                      icon: Icon(MdiIcons.openInNew,
                          color: CustomColors.primaryDark),
                      label: const Text(
                        'El poder de las autoinstrucciones',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            color: CustomColors.primaryDark,
                            decoration: TextDecoration.underline),
                      ), // <-- Text
                    ),
                  ),
                ],
              ),
            ),
          ),
        );

      case 3:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 20, right: 20, left: 20, bottom: 5),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Enfrentar evaluaciones (pruebas, trabajos, presentaciones, etc.) es uno de los momentos que pueden aumentar los niveles de estrés académico. A partir de lo anterior, a continuación te compartimos algunos consejos que te ayudarán a manejar la ansiedad ',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                'antes, durante y posterior al rendimiento de una evaluación. ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Ten en cuenta que si bien las estrategias presentadas pueden ser de mucha utilidad para manejar la ansiedad académica en las evaluaciones, es importante mencionar que, ',
                          ),
                          TextSpan(
                            text:
                                'para lograr efectos a largo plazo, se deben realizar acciones sostenidas a lo largo del tiempo.\n\n',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                '¿Cómo manejar la ansiedad académica antes de una evaluación?\n\n',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Si tuviste un buen estudio, evita repasar hasta último minuto, pues el nerviosismo podría hacer que no recuerdes de manera inmediata lo aprendido e incrementes tus niveles de ansiedad.\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Intenta distraerte antes de la evaluación (ej.: pon atención en situaciones que no tengan relación con esta instancia).\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'No realices comparaciones con lo aprendido por otras personas, puedes sentir temor de que estén mejor preparadas/os que tú.\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Monitorea tus niveles de ansiedad, utiliza técnicas de respiración y relajación antes de entrar.\n\n',
                          ),
                          TextSpan(
                            text:
                                '¿Cómo manejar la ansiedad académica durante una evaluación?\n\n',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Lee las preguntas con calma y asegúrate de entender lo que se está preguntando en cada apartado.\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Recuerda que las preguntas de la evaluación contienen conceptos claves que pueden ayudarte a recordar lo aprendido.\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Date tiempo para activar tus conocimientos y elaborar tus respuestas.\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Si temes olvidar algún concepto o idea, haz un borrador o punteo frente a la pregunta en cuestión.\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Comienza respondiendo aquello que sabes, eso disminuirá tu ansiedad y facilitará que recuerdes lo estudiado.\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Lleva el control del tiempo. Puedes utilizar reloj en caso que el celular sea un distractor.\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Siempre pon la evaluación en perspectiva. Puede haber otras instancias que te permitan mejorar tu calificación final del ramo.\n\n',
                          ),
                          TextSpan(
                            text:
                                '¿Cómo manejar la ansiedad académica posterior a una evaluación?\n\n',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'No compares las respuestas con tus compañeras/os, podrías quedar con una sensación errada sobre tu desempeño.\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Evita juzgar severamente lo que lograste rendir estando bajo presión.',
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    width: size.width,
                    margin: const EdgeInsets.only(top: 50, left: 20, bottom: 5),
                    child: const Text(
                      'Fuente:',
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(
                        fontSize: 22,
                        color: CustomColors.primary,
                      ),
                    ),
                  ),
                  Container(
                    width: size.width,
                    margin: const EdgeInsets.only(left: 20, bottom: 5),
                    child: const Text(
                      'Qué es y cómo manejar la ansiedad Académica.',
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(
                        fontSize: 16,
                        color: CustomColors.primary,
                      ),
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: const EdgeInsets.only(top: 5, bottom: 20, left: 20),
                    child: ElevatedButton.icon(
                      style: ButtonStyle(
                        backgroundColor: WidgetStateProperty.all<Color>(
                            CustomColors.primary80pink),
                        shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                          ),
                        ),
                      ),
                      onPressed: () {
                        _launchUrl(_url2);
                      },
                      icon: Icon(MdiIcons.openInNew,
                          color: CustomColors.primaryDark),
                      label: const Text(
                        'Ir al artículo',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            color: CustomColors.primaryDark,
                            decoration: TextDecoration.underline),
                      ), // <-- Text
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      default:
        return Container();
    }
  }
}
