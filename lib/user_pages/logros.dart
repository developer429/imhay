// ignore_for_file: non_constant_identifier_names

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:seguimiento/colors.dart';
import 'package:url_launcher/url_launcher.dart';

class Logros extends StatefulWidget {
  const Logros({
    Key? key,
  }) : super(key: key);

  @override
  State<Logros> createState() => _LogrosState();
}

class _LogrosState extends State<Logros> {
  String uid = FirebaseAuth.instance.currentUser!.uid;
  bool M1A1L = false;
  bool M1A3L = false;
  bool M1A4L = false;
  bool M1A5L = false;

  bool M2A1L = false;
  bool M2A3L = false;
  bool M2A4L = false;
  bool M2A6L = false;

  bool M3A1L = false;
  bool M3A3L = false;
  bool M3A4L = false;
  bool M3A5L = false;
  bool M3A7L = false;

  bool M4A1L = false;
  bool M4A3L = false;
  bool M4A6L = false;

  ScrollController scrollController = ScrollController();

  CollectionReference doneReference =
      FirebaseFirestore.instance.collection('control');
  void getLogros() {
    doneReference.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        M1A1L = fields['terminaM1A1'];
        M1A3L = fields['terminaM1A3'];
        M1A4L = fields['terminaM1A4'];
        M1A5L = fields['terminaM1A5'];

        M2A1L = fields['terminaM2A1'];
        M2A3L = fields['terminaM2A3'];
        M2A4L = fields['terminaM2A4'];
        M2A6L = fields['terminaM2A6'];

        M3A1L = fields['terminaM3A1'];
        M3A3L = fields['terminaM3A3'];
        M3A4L = fields['terminaM3A4'];
        M3A5L = fields['terminaM3A5'];
        M3A7L = fields['terminaM3A7'];

        M4A1L = fields['terminaM4A1'];
        M4A3L = fields['terminaM4A3'];
        M4A6L = fields['terminaM4A6'];
      });
    });
  }

  final Uri pagina2 = Uri.parse('https://hablemosdetodo.injuv.gob.cl');

  int activeStep = 0;
  @override
  void initState() {
    super.initState();
    getLogros();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> _launchUrl(url) async {
    if (!await launchUrl(url)) {
      throw Exception('Could not launch $url');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: CustomColors.background,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: CustomColors.background,
          body: Center(
            child: Column(
              children: [
                content(),
                Container(
                  margin: const EdgeInsets.only(top: 10, bottom: 10),
                  child: bottomStructure(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget topStructure() {
    return Stack(
      children: [
        Column(
          children: [
            Row(
              children: [
                Container(
                  alignment: Alignment.topRight,
                  child: IconButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    icon: const Icon(
                      Icons.arrow_back,
                      color: CustomColors.primaryDark,
                    ),
                  ),
                ),
              ],
            ),
            Text(
              headerText(),
              textScaler: const TextScaler.linear(1.0),
              textAlign: TextAlign.center,
              style: const TextStyle(
                color: CustomColors.primaryDark,
                fontSize: 24,
              ),
            ),
            header(),
          ],
        ),
      ],
    );
  }

  Widget bottomStructure() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        nextButton(),
      ],
    );
  }

  Widget nextButton() {
    Size size = MediaQuery.of(context).size;

    return SizedBox(
      width: size.width / 1.2,
      child: ElevatedButton(
        style: ButtonStyle(
          backgroundColor: WidgetStateProperty.all<Color>(CustomColors.primary),
          shape: WidgetStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(18.0),
            ),
          ),
        ),
        onPressed: () {
          Navigator.of(context).pop();
        },
        child: Text(
          buttonText(),
          textScaler: const TextScaler.linear(1.0),
          style: const TextStyle(color: CustomColors.surface),
        ),
      ),
    );
  }

  Widget header() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      default:
        return Container(
          margin: const EdgeInsets.all(15),
          decoration: const BoxDecoration(),
          child: Image(
            width: size.width / 3,
            height: size.width / 3,
            fit: BoxFit.scaleDown,
            image: AssetImage(imageHeader()),
          ),
        );
    }
  }

  String imageHeader() {
    switch (activeStep) {
      case 0:
        return 'assets/images/logros.png';

      default:
        return ' ';
    }
  }

  String headerText() {
    switch (activeStep) {
      case 0:
        return 'Tus logros';

      default:
        return ' ';
    }
  }

  String buttonText() {
    switch (activeStep) {
      case 0:
        return 'Volver a mi perfil';

      default:
        return ' ';
    }
  }

  Widget content() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.all(20),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'En esta sección, podrás ver las insignias que has obteniendo en la medida que avances en las actividades de Tu Programa.',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                      ),
                    ),
                  ),
                  Visibility(
                    visible: !M1A1L,
                    child: Container(
                      height: size.height / 3,
                      alignment: Alignment.center,
                      child: const Text(
                        'No hay datos',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            color: CustomColors.primary,
                            fontWeight: FontWeight.bold,
                            fontSize: 24),
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.all(20),
                    child: Column(
                      children: [
                        Visibility(
                          visible: M4A6L,
                          child: Container(
                            margin: const EdgeInsets.only(top: 10, bottom: 10),
                            decoration: BoxDecoration(
                                color: CustomColors.lightContainer,
                                border: Border.all(
                                    width: 2, color: CustomColors.surface),
                                borderRadius: BorderRadius.circular(15)),
                            child: Row(
                              children: [
                                const Image(
                                  width: 100,
                                  height: 120,
                                  fit: BoxFit.scaleDown,
                                  image: AssetImage('assets/images/m4a6.png'),
                                ),
                                Container(
                                  margin: const EdgeInsets.only(left: 10),
                                  child: const Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Trabajo por mi bienestar:',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            fontSize: 13,
                                            color: CustomColors.primaryDark),
                                      ),
                                      Text(
                                        'Me expongo para superar\nmis miedos',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16,
                                            color: CustomColors.primaryDark),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        Visibility(
                          visible: M4A3L,
                          child: Container(
                            margin: const EdgeInsets.only(top: 10, bottom: 10),
                            decoration: BoxDecoration(
                                color: CustomColors.lightContainer,
                                border: Border.all(
                                    width: 2, color: CustomColors.surface),
                                borderRadius: BorderRadius.circular(15)),
                            child: Row(
                              children: [
                                const Image(
                                  width: 100,
                                  height: 120,
                                  fit: BoxFit.scaleDown,
                                  image: AssetImage('assets/images/m4a3.png'),
                                ),
                                Container(
                                  margin: const EdgeInsets.only(left: 10),
                                  child: const Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Trabajo por mi bienestar:',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            fontSize: 13,
                                            color: CustomColors.primaryDark),
                                      ),
                                      Text(
                                        'Identifico lo que suelo\nevitar',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16,
                                            color: CustomColors.primaryDark),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        Visibility(
                          visible: M4A1L,
                          child: Container(
                            margin: const EdgeInsets.only(top: 10, bottom: 10),
                            decoration: BoxDecoration(
                                color: CustomColors.lightContainer,
                                border: Border.all(
                                    width: 2, color: CustomColors.surface),
                                borderRadius: BorderRadius.circular(15)),
                            child: Row(
                              children: [
                                const Image(
                                  width: 100,
                                  height: 120,
                                  fit: BoxFit.scaleDown,
                                  image: AssetImage('assets/images/m4a1.png'),
                                ),
                                Container(
                                  margin: const EdgeInsets.only(left: 10),
                                  child: const Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Conocimiento en salud mental:',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            fontSize: 13,
                                            color: CustomColors.primaryDark),
                                      ),
                                      Text(
                                        'Evitación y exposición',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16,
                                            color: CustomColors.primaryDark),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        Visibility(
                          visible: M3A7L,
                          child: Container(
                            margin: const EdgeInsets.only(top: 10, bottom: 10),
                            decoration: BoxDecoration(
                                color: CustomColors.lightContainer,
                                border: Border.all(
                                    width: 2, color: CustomColors.surface),
                                borderRadius: BorderRadius.circular(15)),
                            child: Row(
                              children: [
                                const Image(
                                  width: 100,
                                  height: 120,
                                  fit: BoxFit.scaleDown,
                                  image: AssetImage('assets/images/m3a7.png'),
                                ),
                                Container(
                                  margin: const EdgeInsets.only(left: 10),
                                  child: const Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Trabajo por mi bienestar:',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            fontSize: 13,
                                            color: CustomColors.primaryDark),
                                      ),
                                      Text(
                                        'Puedo tomar decisiones',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16,
                                            color: CustomColors.primaryDark),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        Visibility(
                          visible: M3A5L,
                          child: Container(
                            margin: const EdgeInsets.only(top: 10, bottom: 10),
                            decoration: BoxDecoration(
                                color: CustomColors.lightContainer,
                                border: Border.all(
                                    width: 2, color: CustomColors.surface),
                                borderRadius: BorderRadius.circular(15)),
                            child: Row(
                              children: [
                                const Image(
                                  width: 100,
                                  height: 120,
                                  fit: BoxFit.scaleDown,
                                  image: AssetImage('assets/images/m3a5.png'),
                                ),
                                Container(
                                  margin: const EdgeInsets.only(left: 10),
                                  child: const Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Trabajo por mi bienestar:',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            fontSize: 13,
                                            color: CustomColors.primaryDark),
                                      ),
                                      Text(
                                        'Defino bien mis problemas',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16,
                                            color: CustomColors.primaryDark),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        Visibility(
                          visible: M3A4L,
                          child: Container(
                            margin: const EdgeInsets.only(top: 10, bottom: 10),
                            decoration: BoxDecoration(
                                color: CustomColors.lightContainer,
                                border: Border.all(
                                    width: 2, color: CustomColors.surface),
                                borderRadius: BorderRadius.circular(15)),
                            child: Row(
                              children: [
                                const Image(
                                  width: 100,
                                  height: 120,
                                  fit: BoxFit.scaleDown,
                                  image: AssetImage('assets/images/m3a4.png'),
                                ),
                                Container(
                                  margin: const EdgeInsets.only(left: 10),
                                  child: const Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Conocimiento en salud mental:',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            fontSize: 13,
                                            color: CustomColors.primaryDark),
                                      ),
                                      Text(
                                        'Resolución de problemas',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16,
                                            color: CustomColors.primaryDark),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        Visibility(
                          visible: M3A3L,
                          child: Container(
                            margin: const EdgeInsets.only(top: 10, bottom: 10),
                            decoration: BoxDecoration(
                                color: CustomColors.lightContainer,
                                border: Border.all(
                                    width: 2, color: CustomColors.surface),
                                borderRadius: BorderRadius.circular(15)),
                            child: Row(
                              children: [
                                const Image(
                                  width: 100,
                                  height: 120,
                                  fit: BoxFit.scaleDown,
                                  image: AssetImage('assets/images/m3a3.png'),
                                ),
                                Container(
                                  margin: const EdgeInsets.only(left: 10),
                                  child: const Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Trabajo por mi bienestar:',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            fontSize: 13,
                                            color: CustomColors.primaryDark),
                                      ),
                                      Text(
                                        'Enfrento los pensamientos\nnegativos',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16,
                                            color: CustomColors.primaryDark),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        Visibility(
                          visible: M3A1L,
                          child: Container(
                            margin: const EdgeInsets.only(top: 10, bottom: 10),
                            decoration: BoxDecoration(
                                color: CustomColors.lightContainer,
                                border: Border.all(
                                    width: 2, color: CustomColors.surface),
                                borderRadius: BorderRadius.circular(15)),
                            child: Row(
                              children: [
                                const Image(
                                  width: 100,
                                  height: 120,
                                  fit: BoxFit.scaleDown,
                                  image: AssetImage('assets/images/m3a1.png'),
                                ),
                                Container(
                                  margin: const EdgeInsets.only(left: 10),
                                  child: const Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Conocimiento en salud mental:',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            fontSize: 13,
                                            color: CustomColors.primaryDark),
                                      ),
                                      Text(
                                        '¿Qué son los errores\ndel pensamiento?',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16,
                                            color: CustomColors.primaryDark),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        Visibility(
                          visible: M2A6L,
                          child: Container(
                            margin: const EdgeInsets.only(top: 10, bottom: 10),
                            decoration: BoxDecoration(
                                color: CustomColors.lightContainer,
                                border: Border.all(
                                    width: 2, color: CustomColors.surface),
                                borderRadius: BorderRadius.circular(15)),
                            child: Row(
                              children: [
                                const Image(
                                  width: 100,
                                  height: 120,
                                  fit: BoxFit.scaleDown,
                                  image: AssetImage('assets/images/m2a6.png'),
                                ),
                                Container(
                                  margin: const EdgeInsets.only(left: 10),
                                  child: const Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Trabajo por mi bienestar',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            fontSize: 13,
                                            color: CustomColors.primaryDark),
                                      ),
                                      Text(
                                        'Uso de la técnica de\nrelajación progresiva',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16,
                                            color: CustomColors.primaryDark),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        Visibility(
                          visible: M2A4L,
                          child: Container(
                            margin: const EdgeInsets.only(top: 10, bottom: 10),
                            decoration: BoxDecoration(
                                color: CustomColors.lightContainer,
                                border: Border.all(
                                    width: 2, color: CustomColors.surface),
                                borderRadius: BorderRadius.circular(15)),
                            child: Row(
                              children: [
                                const Image(
                                  width: 100,
                                  height: 120,
                                  fit: BoxFit.scaleDown,
                                  image: AssetImage('assets/images/m2a4.png'),
                                ),
                                Container(
                                  margin: const EdgeInsets.only(left: 10),
                                  child: const Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Conocimiento en salud mental',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            fontSize: 13,
                                            color: CustomColors.primaryDark),
                                      ),
                                      Text(
                                        'Regulación emocional',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16,
                                            color: CustomColors.primaryDark),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        Visibility(
                          visible: M2A3L,
                          child: Container(
                            margin: const EdgeInsets.only(top: 10, bottom: 10),
                            decoration: BoxDecoration(
                                color: CustomColors.lightContainer,
                                border: Border.all(
                                    width: 2, color: CustomColors.surface),
                                borderRadius: BorderRadius.circular(15)),
                            child: Row(
                              children: [
                                const Image(
                                  width: 100,
                                  height: 120,
                                  fit: BoxFit.scaleDown,
                                  image: AssetImage('assets/images/m2a3.png'),
                                ),
                                Container(
                                  margin: const EdgeInsets.only(left: 10),
                                  child: const Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Trabajo por mi bienestar:',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            fontSize: 13,
                                            color: CustomColors.primaryDark),
                                      ),
                                      Text(
                                        'Construyo mi ecomapa',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16,
                                            color: CustomColors.primaryDark),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        Visibility(
                          visible: M2A1L,
                          child: Container(
                            margin: const EdgeInsets.only(top: 10, bottom: 10),
                            decoration: BoxDecoration(
                                color: CustomColors.lightContainer,
                                border: Border.all(
                                    width: 2, color: CustomColors.surface),
                                borderRadius: BorderRadius.circular(15)),
                            child: Row(
                              children: [
                                const Image(
                                  width: 100,
                                  height: 120,
                                  fit: BoxFit.scaleDown,
                                  image: AssetImage('assets/images/m2a1.png'),
                                ),
                                Container(
                                  margin: const EdgeInsets.only(left: 10),
                                  child: const Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Conocimiento en salud mental:',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            fontSize: 13,
                                            color: CustomColors.primaryDark),
                                      ),
                                      Text(
                                        'Redes de apoyo',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16,
                                            color: CustomColors.primaryDark),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        Visibility(
                          visible: M1A5L,
                          child: Container(
                            margin: const EdgeInsets.only(top: 10, bottom: 10),
                            decoration: BoxDecoration(
                                color: CustomColors.lightContainer,
                                border: Border.all(
                                    width: 2, color: CustomColors.surface),
                                borderRadius: BorderRadius.circular(15)),
                            child: Row(
                              children: [
                                const Image(
                                  width: 100,
                                  height: 120,
                                  fit: BoxFit.scaleDown,
                                  image: AssetImage('assets/images/m1a5.png'),
                                ),
                                Container(
                                  margin: const EdgeInsets.only(left: 10),
                                  child: const Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Trabajo por mi bienestar:',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            fontSize: 13,
                                            color: CustomColors.primaryDark),
                                      ),
                                      Text(
                                        'Yo me activo',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16,
                                            color: CustomColors.primaryDark),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        Visibility(
                          visible: M1A4L,
                          child: Container(
                            margin: const EdgeInsets.only(top: 10, bottom: 10),
                            decoration: BoxDecoration(
                                color: CustomColors.lightContainer,
                                border: Border.all(
                                    width: 2, color: CustomColors.surface),
                                borderRadius: BorderRadius.circular(15)),
                            child: Row(
                              children: [
                                const Image(
                                  width: 100,
                                  height: 120,
                                  fit: BoxFit.scaleDown,
                                  image: AssetImage('assets/images/m1a4.png'),
                                ),
                                Container(
                                  margin: const EdgeInsets.only(left: 10),
                                  child: const Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Conocimiento en salud mental:',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            fontSize: 13,
                                            color: CustomColors.primaryDark),
                                      ),
                                      Text(
                                        '7 Principios de la\nactivación conductual',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16,
                                            color: CustomColors.primaryDark),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        Visibility(
                          visible: M1A3L,
                          child: Container(
                            margin: const EdgeInsets.only(top: 10, bottom: 10),
                            decoration: BoxDecoration(
                                color: CustomColors.lightContainer,
                                border: Border.all(
                                    width: 2, color: CustomColors.surface),
                                borderRadius: BorderRadius.circular(15)),
                            child: Row(
                              children: [
                                const Image(
                                  width: 100,
                                  height: 120,
                                  fit: BoxFit.scaleDown,
                                  image: AssetImage('assets/images/m1a3.png'),
                                ),
                                Container(
                                  margin: const EdgeInsets.only(left: 10),
                                  child: const Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Conocimiento en salud mental:',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            fontSize: 13,
                                            color: CustomColors.primaryDark),
                                      ),
                                      Text(
                                        '¿Qué es la ansiedad?',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16,
                                            color: CustomColors.primaryDark),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        Visibility(
                          visible: M1A1L,
                          child: Container(
                            margin: const EdgeInsets.only(top: 10, bottom: 10),
                            decoration: BoxDecoration(
                                color: CustomColors.lightContainer,
                                border: Border.all(
                                    width: 2, color: CustomColors.surface),
                                borderRadius: BorderRadius.circular(15)),
                            child: Row(
                              children: [
                                Container(
                                  margin: const EdgeInsets.only(left: 10),
                                  child: const Image(
                                    width: 100,
                                    height: 120,
                                    fit: BoxFit.scaleDown,
                                    image:
                                        AssetImage('assets/images/logros.png'),
                                  ),
                                ),
                                Container(
                                  margin: const EdgeInsets.only(left: 10),
                                  child: const Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Dato útil',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            fontSize: 13,
                                            color: CustomColors.primaryDark),
                                      ),
                                      Text(
                                        'Sitio web\nHablemos de todo',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: CustomColors.primaryDark,
                                          fontSize: 16,
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                const Spacer(),
                                Container(
                                  margin: const EdgeInsets.only(right: 10),
                                  child: IconButton(
                                      onPressed: () {
                                        _launchUrl(pagina2);
                                      },
                                      icon: Icon(
                                        MdiIcons.openInNew,
                                        color: CustomColors.primaryDark,
                                      )),
                                )
                              ],
                            ),
                          ),
                        ),
                        Visibility(
                          visible: M1A1L,
                          child: Container(
                            margin: const EdgeInsets.only(top: 10, bottom: 10),
                            decoration: BoxDecoration(
                                color: CustomColors.lightContainer,
                                border: Border.all(
                                    width: 2, color: CustomColors.surface),
                                borderRadius: BorderRadius.circular(15)),
                            child: Row(
                              children: [
                                const Image(
                                  width: 100,
                                  height: 120,
                                  fit: BoxFit.scaleDown,
                                  image: AssetImage('assets/images/m1a1.png'),
                                ),
                                Container(
                                  margin: const EdgeInsets.only(left: 10),
                                  child: const Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Conocimiento en salud mental:',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            fontSize: 13,
                                            color: CustomColors.primaryDark),
                                      ),
                                      Text(
                                        '¿Qué es la depresión?',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: CustomColors.primaryDark,
                                            fontSize: 16),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );

      default:
        return Container();
    }
  }
}
