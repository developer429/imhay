// ignore_for_file: unused_field, file_names

import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:seguimiento/user_pages/perfil.dart';
import 'package:seguimiento/user_pages/plan-seguridad.dart';
import 'package:seguimiento/user_pages/sientes-mal.dart';
import 'package:url_launcher/url_launcher.dart';

import '../colors.dart';
import 'chatear.dart';

class PideAyuda extends StatefulWidget {
  const PideAyuda({Key? key}) : super(key: key);

  @override
  State<PideAyuda> createState() => _PideAyudaState();
}

class _PideAyudaState extends State<PideAyuda> {
  final Uri _url =
      Uri.parse('https://www.imhay.org/donde-pedir-ayuda-en-salud-mental/');
  Future<void> _launchUrl() async {
    if (!await launchUrl(_url)) {
      throw Exception('Could not launch $_url');
    }
  }

  Future<void>? _launched;
  final String _phone = '*4141';

  Future<void> _makePhoneCall(String phoneNumber) async {
    final Uri launchUri = Uri(
      scheme: 'tel',
      path: phoneNumber,
    );
    await launchUrl(launchUri);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.purplePrograma,
      extendBodyBehindAppBar: true,
      extendBody: true,
      body: Center(
        child: pideAyuda(),
      ),
    );
  }

  Widget pideAyuda() {
    return Column(
      children: [
        Expanded(
          child: ListView(
            children: [
              Container(
                alignment: Alignment.topCenter,
                child: Stack(
                  alignment: Alignment.topRight,
                  children: [
                    Container(
                      margin: const EdgeInsets.only(bottom: 30),
                      child: const Image(
                        image: AssetImage('assets/images/ayuda-banner.png'),
                      ),
                    ),
                    IconButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => const TuPerfil(
                              typeOfUser: 'U',
                            ),
                          ),
                        );
                      },
                      icon: Icon(
                        MdiIcons.accountCircleOutline,
                        color: Colors.black,
                      ),
                    ),
                  ],
                ),
              ),
              ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const SientesMal()),
                    ),
                  );
                },
                leading: Icon(
                  MdiIcons.playOutline,
                  color: Colors.white70,
                ),
                title: const Text(
                  '¿Te sientes mal en este momento?',
                  overflow: TextOverflow.ellipsis,
                  textScaler: TextScaler.linear(1.0),
                  maxLines: 2,
                  style: TextStyle(color: Colors.white70, fontSize: 17),
                ),
                subtitle: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Realiza este breve ejercicio que te ayudará a retomar la calma',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(color: Colors.white70, fontSize: 16),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: Colors.white70,
                ),
              ),
              Container(
                margin: const EdgeInsets.only(left: 10, right: 10),
                child: const Divider(
                  color: Colors.white,
                  thickness: 0.5,
                ),
              ),
              ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const ChateaPsicologo()),
                    ),
                  );
                },
                leading: Icon(
                  MdiIcons.forumOutline,
                  color: Colors.white70,
                ),
                title: const Text(
                  'Chatea con un psicólogx',
                  overflow: TextOverflow.ellipsis,
                  textScaler: TextScaler.linear(1.0),
                  maxLines: 2,
                  style: TextStyle(color: Colors.white70, fontSize: 17),
                ),
                subtitle: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Conversa ahora para acceder a contención emocional u orientación con las actividades de la app',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(color: Colors.white70, fontSize: 16),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: Colors.white70,
                ),
              ),
              Container(
                margin: const EdgeInsets.only(left: 10, right: 10),
                child: const Divider(
                  color: Colors.white,
                  thickness: 0.5,
                ),
              ),
              ListTile(
                onTap: () {
                  setState(() {
                    _launched = _makePhoneCall(_phone);
                  });
                },
                leading: Icon(
                  MdiIcons.phoneOutline,
                  color: Colors.white70,
                ),
                title: const Text(
                  '¿Necesitas ayuda urgente?',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(color: Colors.white70, fontSize: 17),
                ),
                subtitle: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Llama ahora al *4141, en donde encontrarás contención, orientación y apoyo especializado en situaciones de crisis de salud mental. Línea disponible 24/7 desde celulares.',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(color: Colors.white70, fontSize: 16),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: Colors.white70,
                ),
              ),
              Container(
                margin: const EdgeInsets.only(left: 10, right: 10),
                child: const Divider(
                  color: Colors.white,
                  thickness: 0.5,
                ),
              ),
              ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const PlanSeguridad()),
                    ),
                  );
                },
                leading: Icon(
                  MdiIcons.pencilOutline,
                  color: Colors.white70,
                ),
                title: const Text(
                  'Plan de seguridad',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(color: Colors.white70, fontSize: 17),
                ),
                subtitle: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Crea un plan al cual puedes recurrir en casos en lo que tu malestar emocional es insoportable y corres el riesgo de llegar a hacerte daño.',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(color: Colors.white70, fontSize: 16),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: Colors.white70,
                ),
              ),
              Container(
                margin: const EdgeInsets.only(left: 10, right: 10),
                child: const Divider(
                  color: Colors.white,
                  thickness: 0.5,
                ),
              ),
              ListTile(
                onTap: () {
                  _launchUrl();
                },
                leading: Icon(
                  MdiIcons.web,
                  color: Colors.white70,
                ),
                title: const Text(
                  'Otros recursos de ayuda',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(color: Colors.white70, fontSize: 17),
                ),
                subtitle: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Aquí encontrarás diversas opciones de ayuda y orientación en salud mental',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(color: Colors.white70, fontSize: 16),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.openInNew,
                  color: Colors.white70,
                ),
              ),
              Container(
                margin: const EdgeInsets.only(left: 10, right: 10),
                child: const Divider(
                  color: Colors.white,
                  thickness: 0.5,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
