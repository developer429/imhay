// ignore_for_file: file_names

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:seguimiento/audio-player.dart';
import 'package:seguimiento/colors.dart';

class SientesMal extends StatefulWidget {
  const SientesMal({
    Key? key,
  }) : super(key: key);

  @override
  State<SientesMal> createState() => _SientesMalState();
}

class _SientesMalState extends State<SientesMal> {
  int activeStep = 0;
  String pasos = '';

  String audioSientesMal_1 =
      'https://firebasestorage.googleapis.com/v0/b/maquetadatos-5dee1.appspot.com/o/audios%2FPide%20Ayuda_te%20sientes%20mal%20en%20este%20momento%20.mp3?alt=media&token=d9ff2ecc-16ab-4475-9355-a2a0d9c08ced';
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  bool active1 = false;
  bool active2 = false;
  ScrollController scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        active1 = true;
        active2 = false;

        break;
      case 1:
        active1 = false;
        active2 = true;

        break;

      default:
    }
    return Container(
      color: Colors.black,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: CustomColors.background,
          body: Center(
            child: Column(
              children: [
                Row(
                  children: [
                    Container(
                      width: size.width / 2,
                      height: 5,
                      color: active1 ? CustomColors.primary2 : Colors.black,
                      child: const Text(''),
                    ),
                    Container(
                      width: size.width / 2,
                      height: 5,
                      color: active2 ? CustomColors.primary2 : Colors.black,
                      child: const Text(''),
                    ),
                  ],
                ),
                content(),
                Container(
                  margin: const EdgeInsets.only(top: 10, bottom: 10),
                  child: bottomStructure(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget topStructure() {
    return Stack(
      children: [
        Column(
          children: [
            Row(
              children: [
                Container(
                  alignment: Alignment.topRight,
                  child: IconButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    icon: const Icon(
                      Icons.arrow_back,
                      color: CustomColors.primaryDark,
                    ),
                  ),
                ),
              ],
            ),
            Container(
              margin: const EdgeInsets.all(10),
              child: Text(
                headerText(),
                textScaler: const TextScaler.linear(1.0),
                textAlign: TextAlign.center,
                style: const TextStyle(
                  color: CustomColors.primaryDark,
                  fontSize: 24,
                ),
              ),
            ),
            header(),
          ],
        ),
      ],
    );
  }

  Widget bottomStructure() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        nextButton(),
      ],
    );
  }

  Widget nextButton() {
    Size size = MediaQuery.of(context).size;
    if (activeStep >= 1 && activeStep < 2) {
      return SizedBox(
        width: size.width / 1,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        WidgetStateProperty.all<Color>(CustomColors.background),
                    shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: const BorderSide(color: CustomColors.primary)),
                    ),
                  ),
                  onPressed: () {
                    setState(() {
                      activeStep = activeStep - 1;
                    });
                  },
                  child: const Text(
                    'Anterior',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(color: CustomColors.primary),
                  )),
            ),
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor:
                      WidgetStateProperty.all<Color>(CustomColors.primary),
                  shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ),
                  ),
                ),
                onPressed: () {
                  if (activeStep < 1) {
                    setState(() {
                      activeStep++;
                    });
                  } else {
                    Navigator.of(context).pop();
                  }
                },
                child: Text(
                  buttonText(),
                  textScaler: const TextScaler.linear(1.0),
                  style: const TextStyle(
                      color: CustomColors.background,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      return SizedBox(
        width: size.width / 1.2,
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor:
                WidgetStateProperty.all<Color>(CustomColors.primary),
            shape: WidgetStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              ),
            ),
          ),
          onPressed: () {
            if (activeStep < 1) {
              setState(() {
                activeStep++;
              });
            } else {
              Navigator.of(context).pop();
            }
            /*
            */
          },
          child: Text(
            buttonText(),
            textScaler: const TextScaler.linear(1.0),
            style: const TextStyle(color: CustomColors.background),
          ),
        ),
      );
    }
  }

  Widget header() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      default:
        return Container(
          margin: const EdgeInsets.all(15),
          decoration: const BoxDecoration(),
          child: Image(
            width: size.width / 2,
            height: size.width / 2,
            fit: BoxFit.scaleDown,
            image: AssetImage(imageHeader()),
          ),
        );
    }
  }

  String imageHeader() {
    switch (activeStep) {
      default:
        return 'assets/images/sientes-mal.png';
    }
  }

  String headerText() {
    switch (activeStep) {
      default:
        return '¿Te sientes mal en este momento?';
    }
  }

  String buttonText() {
    switch (activeStep) {
      case 1:
        return 'Finalizar';

      default:
        return 'Siguiente ';
    }
  }

  Widget content() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });
        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    width: size.width,
                    margin: const EdgeInsets.only(top: 15, left: 20),
                    child: const Text(
                      'Ejercicio guiado',
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.primary,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    width: kIsWeb ? size.width / 2 : size.width,
                    height: size.height / 12,
                    alignment: Alignment.center,
                    margin: const EdgeInsets.all(15),
                    decoration: BoxDecoration(
                      color: CustomColors.primary70,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: AudioPlayerURL(
                      url: audioSientesMal_1,
                      isModule: false,
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 20, right: 20, left: 20, bottom: 30),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Has llegado aquí porque en este momento no te sientes bien, ¿te parece si hacemos un ejercicio de respiración? esto podrá servirte para que lentamente retomes la calma y puedas sentirte un poco mejor.\n\n',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                              text:
                                  '¿Por qué nos sirve respirar lenta y tranquilamente cuando nuestras emociones nos sobrepasan?\n\n',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(
                            text:
                                'Cuando experimentamos emociones intensas sentimos que necesitamos más aire, lo que nos lleva a respirar más rápido e hiperventilarnos. Sin embargo, respirar de forma acelerada no tiene un efecto calmante, si no que intensifica lo que estamos sintiendo. Esto sucede debido a que el hiperventilar le indica a nuestro cuerpo que debemos prepararnos para “luchar o huir”. Por esto, para disminuir la intensidad de nuestras emociones y nuestro malestar es importante que bajemos el ritmo de nuestra respiración.\n\nA continuación te dejamos dos simples estrategias que puedes emplear, en cualquier momento o lugar, cuando sientas que tu malestar o tus emociones son muy intensas.',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );

      case 1:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });
        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 20, right: 20, left: 20, bottom: 5),
                    child: RichText(
                      text: const TextSpan(
                        text: '',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text: '1·  ',
                          ),
                          TextSpan(
                            text: 'Usa el frío a tu favor\n',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          TextSpan(
                            text:
                                'Busca algo muy frío, como una bolsa de hielo y ponlo en tu rostro por algunos segundos. También puedes llenar un lavamanos con agua muy fría y sumergir tu cara unos 30 segundos.\n\n',
                          ),
                          TextSpan(
                            text: '2·  ',
                          ),
                          TextSpan(
                            text: 'Recuéstate y estírate\n',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          TextSpan(
                            text:
                                'Busca un lugar donde puedas recostarte. Estira lo más que puedas (de forma progresiva) tus musculos de la cabeza a los pies.\n\n',
                          ),
                          TextSpan(
                            text:
                                'Recuerda que en la app también puedes acceder a otros recursos de ayuda, tales como el chat con psicólogx, números de ayuda, etc. para momentos en que sientas que tu malestar es muy intenso.',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      default:
        return Container();
    }
  }
}
