import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:seguimiento/colors.dart';
import 'package:seguimiento/main.dart';
import 'package:seguimiento/psicologo_pages/auth_psicologo.dart';
import 'package:seguimiento/user_pages/como-te-sientes.dart';
import 'package:seguimiento/user_pages/favoritos.dart';
import 'package:seguimiento/user_pages/historial-animo.dart';
import 'package:seguimiento/user_pages/logros.dart';
import 'package:seguimiento/user_pages/notificaciones-configurables.dart';

class TuPerfil extends StatefulWidget {
  const TuPerfil({
    Key? key,
    required this.typeOfUser,
  }) : super(key: key);
  final String typeOfUser;

  @override
  State<TuPerfil> createState() => _TuPerfilState();
}

class _TuPerfilState extends State<TuPerfil> {
  String uid = FirebaseAuth.instance.currentUser!.uid;

  Future<void> _signOut() async {
    uid = '';
    var logOut = FirebaseAuth.instance.signOut();
    await logOut.then((value) => Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => const MyHomePage()),
        (route) => false));
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    switch (widget.typeOfUser) {
      case 'P':
        return Container(
          color: Colors.black,
          child: SafeArea(
            child: Scaffold(
              backgroundColor: CustomColors.purplePrograma,
              body: Column(
                children: [
                  Expanded(
                    child: ListView(
                      children: [
                        Container(
                          alignment: Alignment.topCenter,
                          child: Stack(
                            alignment: Alignment.topLeft,
                            children: [
                              Container(
                                margin: const EdgeInsets.only(bottom: 10),
                                child: const Image(
                                  image: AssetImage(
                                      'assets/images/perfil-banner.png'),
                                ),
                              ),
                              IconButton(
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                icon: const Icon(
                                  Icons.arrow_back,
                                  color: Colors.black,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          alignment: Alignment.center,
                          child: ListTile(
                            onTap: () {
                              AuthPsicologo authPsicologo = AuthPsicologo();
                              authPsicologo.cerrarSesionPsicologo().then(
                                  (value) => Navigator.pushAndRemoveUntil(
                                      context,
                                      MaterialPageRoute<void>(
                                          builder: (BuildContext context) =>
                                              const MyHomePage()),
                                      (route) => false));
                            },
                            title: const Text(
                              'Cerrar sesión',
                              overflow: TextOverflow.ellipsis,
                              textScaler: TextScaler.linear(1.0),
                              maxLines: 2,
                              style: TextStyle(
                                  color: Colors.white70,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18),
                            ),
                            leading: Icon(
                              MdiIcons.logout,
                              color: Colors.white70,
                            ),
                            trailing: Icon(
                              MdiIcons.menuRight,
                              color: Colors.white70,
                              size: size.height / 24,
                            ),
                          ),
                        ),
                        const Divider(
                          color: Colors.white,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );

      default:
        return Container(
          color: Colors.black,
          child: SafeArea(
            child: Scaffold(
              backgroundColor: CustomColors.purplePrograma,
              body: Column(
                children: [
                  Expanded(
                    child: ListView(
                      children: [
                        Container(
                          alignment: Alignment.topCenter,
                          child: Stack(
                            alignment: Alignment.topLeft,
                            children: [
                              Container(
                                margin: const EdgeInsets.only(bottom: 10),
                                child: const Image(
                                  image: AssetImage(
                                      'assets/images/perfil-banner.png'),
                                ),
                              ),
                              IconButton(
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                icon: const Icon(
                                  Icons.arrow_back,
                                  color: Colors.black,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          alignment: Alignment.center,
                          child: ListTile(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: ((context) => const Logros()),
                                ),
                              );
                            },
                            title: const Text(
                              'Tus logros',
                              overflow: TextOverflow.ellipsis,
                              textScaler: TextScaler.linear(1.0),
                              maxLines: 2,
                              style: TextStyle(
                                  color: Colors.white70,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18),
                            ),
                            leading: Icon(
                              MdiIcons.bookmarkOutline,
                              color: Colors.white70,
                            ),
                            trailing: Icon(
                              MdiIcons.menuRight,
                              color: Colors.white70,
                              size: size.height / 24,
                            ),
                          ),
                        ),
                        const Divider(
                          color: Colors.white,
                        ),
                        Container(
                          alignment: Alignment.center,
                          child: ListTile(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: ((context) => const Favoritos()),
                                ),
                              );
                            },
                            title: const Text(
                              'Tus favoritos',
                              overflow: TextOverflow.ellipsis,
                              textScaler: TextScaler.linear(1.0),
                              maxLines: 2,
                              style: TextStyle(
                                  color: Colors.white70,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18),
                            ),
                            leading: Icon(
                              MdiIcons.starOutline,
                              color: Colors.white70,
                            ),
                            trailing: Icon(
                              MdiIcons.menuRight,
                              color: Colors.white70,
                              size: size.height / 24,
                            ),
                          ),
                        ),
                        const Divider(
                          color: Colors.white,
                        ),
                        Container(
                          alignment: Alignment.center,
                          child: ListTile(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: ((context) =>
                                      const HistorialAnimo()),
                                ),
                              );
                            },
                            title: const Text(
                              'Tu historial del ánimo',
                              overflow: TextOverflow.ellipsis,
                              textScaler: TextScaler.linear(1.0),
                              maxLines: 2,
                              style: TextStyle(
                                  color: Colors.white70,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18),
                            ),
                            leading: Icon(
                              MdiIcons.thermometerHigh,
                              color: Colors.white70,
                            ),
                            trailing: Icon(
                              MdiIcons.menuRight,
                              color: Colors.white70,
                              size: size.height / 24,
                            ),
                          ),
                        ),
                        const Divider(
                          color: Colors.white,
                        ),
                        Container(
                          alignment: Alignment.center,
                          child: ListTile(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: ((context) => const SientesPQH4()),
                                ),
                              );
                            },
                            title: const Text(
                              'Cuéntanos cómo te sientes',
                              overflow: TextOverflow.ellipsis,
                              textScaler: TextScaler.linear(1.0),
                              maxLines: 2,
                              style: TextStyle(
                                  color: Colors.white70,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18),
                            ),
                            leading: Icon(
                              MdiIcons.tagFaces,
                              color: Colors.white70,
                            ),
                            trailing: Icon(
                              MdiIcons.menuRight,
                              color: Colors.white70,
                              size: size.height / 24,
                            ),
                          ),
                        ),
                        const Divider(
                          color: Colors.white,
                        ),
                        Container(
                          alignment: Alignment.center,
                          child: ListTile(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: ((context) =>
                                      const ConfigurarNotificaciones()),
                                ),
                              );
                            },
                            title: const Text(
                              'Configura las notificaciones',
                              overflow: TextOverflow.ellipsis,
                              textScaler: TextScaler.linear(1.0),
                              maxLines: 2,
                              style: TextStyle(
                                  color: Colors.white70,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18),
                            ),
                            leading: Icon(
                              MdiIcons.bellRingOutline,
                              color: Colors.white70,
                            ),
                            trailing: Icon(
                              MdiIcons.menuRight,
                              color: Colors.white70,
                              size: size.height / 24,
                            ),
                          ),
                        ),
                        const Divider(
                          color: Colors.white,
                        ),
                        Container(
                          alignment: Alignment.center,
                          child: ListTile(
                            onTap: () {
                              _signOut();
                            },
                            title: const Text(
                              'Cerrar sesión',
                              overflow: TextOverflow.ellipsis,
                              textScaler: TextScaler.linear(1.0),
                              maxLines: 2,
                              style: TextStyle(
                                  color: Colors.white70,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18),
                            ),
                            leading: Icon(
                              MdiIcons.logout,
                              color: Colors.white70,
                            ),
                            trailing: Icon(
                              MdiIcons.menuRight,
                              color: Colors.white70,
                              size: size.height / 24,
                            ),
                          ),
                        ),
                        const Divider(
                          color: Colors.white,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
    }
  }
}
