// ignore_for_file: prefer_typing_uninitialized_variables

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:seguimiento/colors.dart';
import '../../firebase.dart';
import '../MainPage.dart';

class M4A1 extends StatefulWidget {
  const M4A1({
    Key? key,
  }) : super(key: key);

  @override
  State<M4A1> createState() => _M4A1State();
}

class _M4A1State extends State<M4A1> {
  String uid = FirebaseAuth.instance.currentUser!.uid;
  bool loadingData = true;
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('page-control');
  String activeStepData = '';
  void getActivePageStored() {
    collectionReference.doc(uid).get().then((value) {
      var fields = value;
      activeStepData = fields['M4A1'];
      setState(() {
        if (int.parse(activeStepData) < 4) {
          activeStep = int.parse(activeStepData) - 1;
          Future.delayed(const Duration(milliseconds: 500), () {
            setState(() {
              loadingData = false;
            });
          });
        }
      });
    });
  }

  ScrollController scrollController = ScrollController();

  bool favorite = false;
  CollectionReference favoriteData =
      FirebaseFirestore.instance.collection('favoritos');
  void getFavData() {
    favoriteData.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        favorite = fields['M4A1'];
      });
    }).catchError((err) {
      // print('Error: $err');
    });
  }

  int activeStep = 0;
  var timeStart;
  @override
  void initState() {
    super.initState();
    getActivePageStored();

    getFavData();
    var tiempo = DateTime.now();
    timeStart = tiempo;
    FirestoreService().setTime(tiempo.toString() + ' - START - M4A1',
        'Comenzó M4A1', '0 minutos (comenzando)');
  }

  @override
  void dispose() {
    super.dispose();
  }

  bool active1 = false;
  bool active2 = false;
  bool active3 = false;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        active1 = true;
        active2 = false;
        active3 = false;
        break;
      case 1:
        active1 = true;
        active2 = true;
        active3 = false;
        break;
      case 2:
        active1 = true;
        active2 = true;
        active3 = true;
        break;
      default:
    }
    return PopScope(
      canPop: false,
      onPopInvokedWithResult: (bool didPop, result) {
        if (didPop) {
          return;
        }
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(32.0))),
            title: const Text(
              '¿Estás seguro que deseas salir de la actividad?',
              textScaler: TextScaler.linear(1.0),
              style: TextStyle(color: CustomColors.primaryDark),
            ),
            content: const Text(
              'Tus avances quedarán guardados',
              textScaler: TextScaler.linear(1.0),
              style: TextStyle(color: CustomColors.primaryDark),
            ),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: const Text(
                  'Continuar con la actividad',
                  textScaler: TextScaler.linear(1.0),
                  style: TextStyle(color: CustomColors.primary),
                ),
              ),
              TextButton(
                onPressed: () {
                  FirestoreService()
                      .updateCurrentPage('M4A1', (activeStep + 1).toString());
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute<void>(
                          builder: (BuildContext context) => const MainPage(
                                activePage: 0,
                              )),
                      (route) => false);
                },
                child: const Text(
                  'Salir',
                  textScaler: TextScaler.linear(1.0),
                  style: TextStyle(color: CustomColors.error),
                ),
              ),
            ],
          ),
        );
      },
      child: loadingData
          ? Container(
              width: size.width,
              height: size.height,
              color: CustomColors.primaryDark,
              child: const Center(
                  child: CircularProgressIndicator(
                color: CustomColors.tertiary70,
              )))
          : Container(
              color: Colors.black,
              child: SafeArea(
                child: Scaffold(
                  backgroundColor:
                      active3 ? CustomColors.greenEnd : CustomColors.primary3,
                  body: Center(
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              width: size.width / 3,
                              height: 5,
                              color: active1
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 3,
                              height: 5,
                              color: active2
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 3,
                              height: 5,
                              color: active3
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                          ],
                        ),
                        content(),
                        Container(
                          margin: const EdgeInsets.only(top: 10, bottom: 10),
                          child: bottomStructure(),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
    );
  }

  Widget topStructure() {
    Size size = MediaQuery.of(context).size;

    return Container(
      width: size.width,
      height: active3 ? null : size.height / 3,
      color: active3 ? CustomColors.greenList : CustomColors.orange3,
      child: Column(
        children: [
          Row(
            children: [
              active3
                  ? Container()
                  : Container(
                      alignment: Alignment.topRight,
                      child: IconButton(
                        onPressed: () {
                          showDialog(
                            context: context,
                            builder: (context) => AlertDialog(
                              shape: const RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(32.0))),
                              title: const Text(
                                '¿Estás seguro que deseas salir de la actividad?',
                                textScaler: TextScaler.linear(1.0),
                                style:
                                    TextStyle(color: CustomColors.primaryDark),
                              ),
                              content: const Text(
                                'Tus avances quedarán guardados',
                                textScaler: TextScaler.linear(1.0),
                                style:
                                    TextStyle(color: CustomColors.primaryDark),
                              ),
                              actions: [
                                TextButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                  child: const Text(
                                    'Continuar con la actividad',
                                    textScaler: TextScaler.linear(1.0),
                                    style:
                                        TextStyle(color: CustomColors.primary),
                                  ),
                                ),
                                TextButton(
                                  onPressed: () {
                                    String tiempo = DateTime.now().toString();
                                    FirestoreService().setTime(
                                        tiempo + ' - CLOSED - M4A1',
                                        'Salió de M4A1 sin terminar',
                                        '0 minutos (sin terminar)');
                                    FirestoreService().updateCurrentPage(
                                        'M4A1', (activeStep + 1).toString());
                                    Navigator.pushAndRemoveUntil(
                                        context,
                                        MaterialPageRoute<void>(
                                            builder: (BuildContext context) =>
                                                const MainPage(
                                                  activePage: 0,
                                                )),
                                        (route) => false);
                                  },
                                  child: const Text(
                                    'Salir',
                                    textScaler: TextScaler.linear(1.0),
                                    style: TextStyle(color: CustomColors.error),
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                        icon: const Icon(
                          Icons.close,
                          color: CustomColors.primaryDark,
                        ),
                      ),
                    ),
              const Spacer(),
              active3 ? Container() : favorito(),
            ],
          ),
          Text(
            headerText(),
            textScaler: const TextScaler.linear(1.0),
            style: const TextStyle(
              color: CustomColors.primaryDark,
              fontSize: 24,
            ),
          ),
          active2 ? Container() : const Spacer(),
          header(),
        ],
      ),
    );
  }

  Widget favorito() {
    return StreamBuilder<QuerySnapshot>(
        stream: FirestoreService().favoritos(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasData) {
            return SizedBox(
              width: 50,
              height: 50,
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (context, index) {
                    var document = snapshot.data?.docs[index];
                    if (document != null && document.id == uid) {
                      switch (document['M4A1']) {
                        case true:
                          return IconButton(
                              icon: Icon(MdiIcons.star,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('M4A1', false)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Eliminado de favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                        case false:
                          return IconButton(
                              icon: Icon(MdiIcons.starOutline,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('M4A1', true)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Agregado a favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                      }
                    } else {
                      return Container();
                    }
                    return null;
                  }),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(
                color: Colors.red,
              ),
            );
          }
        });
  }

  Widget bottomStructure() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        nextButton(),
      ],
    );
  }

  Widget nextButton() {
    Size size = MediaQuery.of(context).size;
    if (activeStep == 1) {
      return SizedBox(
        width: size.width / 1,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        WidgetStateProperty.all<Color>(CustomColors.primary3),
                    shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: const BorderSide(color: Colors.white)),
                    ),
                  ),
                  onPressed: () {
                    setState(() {
                      activeStep = activeStep - 1;
                    });
                  },
                  child: const Text(
                    'Anterior',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(color: Colors.white),
                  )),
            ),
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: WidgetStateProperty.all<Color>(active3
                      ? CustomColors.primary
                      : CustomColors.greenButton),
                  shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ),
                  ),
                ),
                onPressed: () {
                  if (activeStep < 2) {
                    setState(() {
                      activeStep++;
                    });
                  } else {
                    FirestoreService().terminaSesion('4', '1',
                        'Leyó la introducción → Aprendió sobre la evitación y exposición de situaciones que causan ansiedad y/o miedo → Terminó la actividad 23');
                    var tiempo = DateTime.now();
                    var difTiempo = tiempo.difference(timeStart);
                    FirestoreService().setTime(
                        tiempo.toString() + ' - ENDED - M4A1',
                        'Terminó M4A1',
                        difTiempo.inMinutes.toString() + ' minutos');
                    FirestoreService().updateCurrentPage('M4A1', '1');
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute<void>(
                            builder: (BuildContext context) => const MainPage(
                                  activePage: 0,
                                )),
                        (route) => false);
                  }
                },
                child: Text(
                  buttonText(),
                  textScaler: const TextScaler.linear(1.0),
                  style: const TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      return SizedBox(
        width: size.width / 1.2,
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor: WidgetStateProperty.all<Color>(
                active3 ? CustomColors.primary : CustomColors.greenButton),
            shape: WidgetStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              ),
            ),
          ),
          onPressed: () {
            if (activeStep < 2) {
              setState(() {
                activeStep++;
              });
            } else {
              FirestoreService().terminaSesion('4', '1',
                  'Leyó la introducción → Aprendió sobre la evitación y exposición de situaciones que causan ansiedad y/o miedo → Terminó la actividad 23');
              var tiempo = DateTime.now();
              var difTiempo = tiempo.difference(timeStart);
              FirestoreService().setTime(tiempo.toString() + ' - ENDED - M4A1',
                  'Terminó M4A1', difTiempo.inMinutes.toString() + ' minutos');
              FirestoreService().updateCurrentPage('M4A1', '1');
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute<void>(
                      builder: (BuildContext context) => const MainPage(
                            activePage: 0,
                          )),
                  (route) => false);
            }
          },
          child: Text(
            buttonText(),
            textScaler: const TextScaler.linear(1.0),
            style: TextStyle(
              color: active3 ? CustomColors.surface : CustomColors.primaryDark,
            ),
          ),
        ),
      );
    }
  }

  Widget header() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 2:
        return Image(
          width: size.width / 8,
          height: size.width / 8,
          fit: BoxFit.scaleDown,
          image: AssetImage(imageHeader()),
        );
      default:
        return Image(
          width: kIsWeb ? 100 : size.width / 3,
          height: kIsWeb ? 100 : size.width / 3,
          fit: BoxFit.scaleDown,
          image: AssetImage(imageHeader()),
        );
    }
  }

  String imageHeader() {
    switch (activeStep) {
      case 0:
        return 'assets/images/m4a1.png';

      case 1:
        return 'assets/images/m4a1.png';

      case 2:
        return 'assets/images/end.png';
      default:
        return ' ';
    }
  }

  String headerText() {
    switch (activeStep) {
      case 0:
        return 'Exposición y evitación';

      case 1:
        return 'Exposición y evitación';

      case 2:
        return '';
      default:
        return ' ';
    }
  }

  String buttonText() {
    switch (activeStep) {
      case 0:
        return 'Siguiente';

      case 1:
        return 'Finalizar';

      case 2:
        return 'Continuar con el programa';
      default:
        return ' ';
    }
  }

  Widget content() {
    switch (activeStep) {
      case 0:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
                controller: scrollController,
                child: Column(
                  children: [
                    topStructure(),
                    Container(
                      margin: const EdgeInsets.all(20),
                      child: RichText(
                        text: const TextSpan(
                          text:
                              'Muchas veces utilizamos estrategias para evadir problemas, situaciones y/o emociones que nos resultan desagradables o difíciles de manejar. Por ejemplo, muchas veces evadimos situaciones que nos generan miedo o ansiedad tales como:\n\n',
                          style: TextStyle(
                              fontSize: 16,
                              color: CustomColors.surface,
                              height: 1.5),
                          children: <TextSpan>[
                            TextSpan(
                              text: '   ·  ',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            ),
                            TextSpan(
                                text:
                                    'Faltar a una exposición oral en la universidad porque te incomoda hablar en público.\n'),
                            TextSpan(
                              text: '   ·  ',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            ),
                            TextSpan(
                                text:
                                    'No ir a una fiesta a la que quieres ir porque asistirán personas que no conoces.\n'),
                            TextSpan(
                              text: '   ·  ',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            ),
                            TextSpan(
                                text:
                                    'No usar el metro porque te dan miedo los espacios cerrados.\n'),
                            TextSpan(
                              text: '   ·  ',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            ),
                            TextSpan(
                                text:
                                    'Evitar ir al gimnasio en ciertos horarios en los que hay más personas porque te preocupa lo que puedan pensar de ti.\n'),
                            TextSpan(
                              text: '   ·  ',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            ),
                            TextSpan(
                                text:
                                    'No intentar hacer cosas nuevas (deporte, pasatiempo, etc.) que te gustaría probar porque piensas que no lo harás bien.'),
                          ],
                        ),
                      ),
                    ),
                  ],
                )),
          ),
        );

      case 1:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.all(20),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Si bien evitar nos puede traer alivio inmediato a corto plazo, a largo plazo la evitación hace que los problemas se mantengan, se intensifiquen o incluso generan nuevos problemas. Por ejemplo, si no asistimos a las evaluaciones orales que tengamos en un curso de la universidad, podríamos terminar reprobando el curso.\n',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                'Así, para poder responder mejor frente a estas experiencias y generar un cambio positivo en nuestras vidas, es importante que nos enfrentemos (y no evitemos) aquellas situaciones, para así poder entrenar habilidades que mejoren nuestra capacidad de hacerles frente.',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );

      case 2:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Container(
                margin: const EdgeInsets.all(15),
                child: Column(
                  children: [
                    topStructure(),
                    Container(
                      margin: const EdgeInsets.all(10),
                      child: const FittedBox(
                        fit: BoxFit.fitWidth,
                        child: Text(
                          '¡Muy bien!',
                          textScaler: TextScaler.linear(1.0),
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              // color: CustomColors.primaryDark,
                              fontSize: 24),
                        ),
                      ),
                    ),
                    const Text(
                      'Tenemos una recompensa para ti:',
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(fontSize: 16),
                      textAlign: TextAlign.center,
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 40, bottom: 10),
                      decoration: BoxDecoration(
                          color: CustomColors.lightContainer,
                          border:
                              Border.all(width: 2, color: CustomColors.surface),
                          borderRadius: BorderRadius.circular(15)),
                      child: Row(
                        children: [
                          const Image(
                            width: 100,
                            height: 120,
                            fit: BoxFit.scaleDown,
                            image: AssetImage('assets/images/m4a1.png'),
                          ),
                          Container(
                            margin: const EdgeInsets.only(left: 10),
                            child: const Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Conocimiento en salud mental:',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      fontSize: 13,
                                      color: CustomColors.primaryDark),
                                ),
                                Text(
                                  'Evitación y exposición',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16,
                                      color: CustomColors.primaryDark),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );

      default:
        return Container();
    }
  }
}
