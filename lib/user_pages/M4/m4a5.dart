// ignore_for_file: prefer_typing_uninitialized_variables

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:seguimiento/colors.dart';
import '../../firebase.dart';
import '../MainPage.dart';

class M4A5 extends StatefulWidget {
  const M4A5({
    Key? key,
  }) : super(key: key);

  @override
  State<M4A5> createState() => _M4A5State();
}

class _M4A5State extends State<M4A5> {
  String uid = FirebaseAuth.instance.currentUser!.uid;
  bool loadingData = true;
  CollectionReference controlReference =
      FirebaseFirestore.instance.collection('page-control');
  String activeStepData = '';
  void getActivePageStored() {
    controlReference.doc(uid).get().then((value) {
      var fields = value;
      activeStepData = fields['M4A5'];
      setState(() {
        if (int.parse(activeStepData) < 4) {
          activeStep = int.parse(activeStepData) - 1;
          Future.delayed(const Duration(milliseconds: 500), () {
            setState(() {
              loadingData = false;
            });
          });
        }
      });
    });
  }

  ScrollController scrollController = ScrollController();

  bool favorite = false;
  CollectionReference favoriteData =
      FirebaseFirestore.instance.collection('favoritos');
  void getFavData() {
    favoriteData.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        favorite = fields['M4A5'];
      });
    }).catchError((err) {
      //print('Error: $err');
    });
  }

  String alternativa1 = '';
  String alternativa2 = '';
  String alternativa3 = '';

  bool alter2 = false;
  bool alter3 = false;
  CollectionReference alternativasReference =
      FirebaseFirestore.instance.collection('M4A4-respuestas');
  void getAlternativas() {
    alternativasReference.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        alternativa1 = fields['alternativa1'];
        alternativa2 = fields['alternativa2'];
        alternativa3 = fields['alternativa3'];
        if (alternativa2 != '-') {
          alter2 = true;
        }
        if (alternativa3 != '-') {
          alter3 = true;
        }
      });
    });
  }

  int activeStep = 0;
  final situacion1 = TextEditingController();
  final situacion2 = TextEditingController();
  final situacion3 = TextEditingController();
  final situacion4 = TextEditingController();
  final situacion5 = TextEditingController();
  final situacion6 = TextEditingController();
  final situacion7 = TextEditingController();

  final _form1Key = GlobalKey<FormState>();

  String problemaData = '';

  CollectionReference problemaReference =
      FirebaseFirestore.instance.collection('M3A5-respuestas');
  void getProblema() {
    problemaReference.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        problemaData = fields['descripcionProblema'];
      });
    });
  }

  CollectionReference dataReference =
      FirebaseFirestore.instance.collection('M4A5-respuestas');
  void getData() {
    dataReference.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        situacion1.text = fields['situacion1'];
        situacion2.text = fields['situacion2'];
        situacion3.text = fields['situacion3'];
        situacion4.text = fields['situacion4'];
        situacion5.text = fields['situacion5'];
        situacion6.text = fields['situacion6'];
        situacion7.text = fields['situacion7'];
      });
    }).catchError((err) {
      //print('Error: $err');
    });
  }

  var timeStart;
  @override
  void initState() {
    super.initState();
    getActivePageStored();
    getAlternativas();
    getData();

    getFavData();
    var tiempo = DateTime.now();
    timeStart = tiempo;
    FirestoreService().setTime(tiempo.toString() + ' - START - M4A5',
        'Comenzó M4A5', '0 minutos (comenzando)');
  }

  @override
  void dispose() {
    super.dispose();
  }

  bool active1 = false;
  bool active2 = false;
  bool active3 = false;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    switch (activeStep) {
      case 0:
        active1 = true;
        active2 = false;
        active3 = false;

        break;
      case 1:
        active1 = true;
        active2 = true;
        active3 = false;

        break;
      case 2:
        active1 = true;
        active2 = true;
        active3 = true;

        break;

      default:
    }
    return PopScope(
      canPop: false,
      onPopInvokedWithResult: (bool didPop, result) {
        if (didPop) {
          return;
        }
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(32.0))),
            title: const Text(
              '¿Estás seguro que deseas salir de la actividad?',
              textScaler: TextScaler.linear(1.0),
              style: TextStyle(color: CustomColors.primaryDark),
            ),
            content: const Text(
              'Tus avances quedarán guardados',
              textScaler: TextScaler.linear(1.0),
              style: TextStyle(color: CustomColors.primaryDark),
            ),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: const Text(
                  'Continuar con la actividad',
                  textScaler: TextScaler.linear(1.0),
                  style: TextStyle(color: CustomColors.primary),
                ),
              ),
              TextButton(
                onPressed: () {
                  FirestoreService()
                      .updateCurrentPage('M4A5', (activeStep + 1).toString());
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute<void>(
                          builder: (BuildContext context) => const MainPage(
                                activePage: 0,
                              )),
                      (route) => false);
                },
                child: const Text(
                  'Salir',
                  textScaler: TextScaler.linear(1.0),
                  style: TextStyle(color: CustomColors.error),
                ),
              ),
            ],
          ),
        );
      },
      child: loadingData
          ? Container(
              width: size.width,
              height: size.height,
              color: CustomColors.primaryDark,
              child: const Center(
                  child: CircularProgressIndicator(
                color: CustomColors.tertiary70,
              )))
          : Container(
              color: Colors.black,
              child: SafeArea(
                child: Scaffold(
                  backgroundColor:
                      active3 ? CustomColors.greenEnd : CustomColors.primary3,
                  body: Center(
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              width: size.width / 3,
                              height: 5,
                              color: active1
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 3,
                              height: 5,
                              color: active2
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 3,
                              height: 5,
                              color: active3
                                  ? CustomColors.primary3
                                  : Colors.white,
                              child: const Text(''),
                            ),
                          ],
                        ),
                        content(),
                        Container(
                          margin: const EdgeInsets.only(top: 10, bottom: 10),
                          child: bottomStructure(),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
    );
  }

  Widget topStructure() {
    Size size = MediaQuery.of(context).size;
    return Container(
      width: size.width,
      height: active3 ? null : size.height / 3,
      color: active3 ? CustomColors.greenList : CustomColors.orange3,
      child: Column(
        children: [
          Row(
            children: [
              active3
                  ? Container()
                  : Container(
                      alignment: Alignment.topRight,
                      child: IconButton(
                        onPressed: () {
                          showDialog(
                            context: context,
                            builder: (context) => AlertDialog(
                              shape: const RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(32.0))),
                              title: const Text(
                                '¿Estás seguro que deseas salir de la actividad?',
                                textScaler: TextScaler.linear(1.0),
                                style:
                                    TextStyle(color: CustomColors.primaryDark),
                              ),
                              content: const Text(
                                'Tus avances quedarán guardados',
                                textScaler: TextScaler.linear(1.0),
                                style:
                                    TextStyle(color: CustomColors.primaryDark),
                              ),
                              actions: [
                                TextButton(
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                  child: const Text(
                                    'Continuar con la actividad',
                                    textScaler: TextScaler.linear(1.0),
                                    style:
                                        TextStyle(color: CustomColors.primary),
                                  ),
                                ),
                                TextButton(
                                  onPressed: () {
                                    String tiempo = DateTime.now().toString();
                                    FirestoreService().setTime(
                                        tiempo + ' - CLOSED - M4A5',
                                        'Salió de M4A5 sin terminar',
                                        '0 minutos (sin terminar)');
                                    FirestoreService().updateCurrentPage(
                                        'M4A5', (activeStep + 1).toString());
                                    Navigator.pushAndRemoveUntil(
                                        context,
                                        MaterialPageRoute<void>(
                                            builder: (BuildContext context) =>
                                                const MainPage(
                                                  activePage: 0,
                                                )),
                                        (route) => false);
                                  },
                                  child: const Text(
                                    'Salir',
                                    textScaler: TextScaler.linear(1.0),
                                    style: TextStyle(color: CustomColors.error),
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                        icon: const Icon(
                          Icons.close,
                          color: CustomColors.primaryDark,
                        ),
                      ),
                    ),
              const Spacer(),
              active3 ? Container() : favorito(),
            ],
          ),
          Text(
            headerText(),
            textScaler: const TextScaler.linear(1.0),
            textAlign: TextAlign.center,
            style: const TextStyle(
              color: CustomColors.primaryDark,
              fontSize: 24,
            ),
          ),
          active3 ? Container() : const Spacer(),
          header()
        ],
      ),
    );
  }

  Widget favorito() {
    return StreamBuilder<QuerySnapshot>(
        stream: FirestoreService().favoritos(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasData) {
            return SizedBox(
              width: 50,
              height: 50,
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (context, index) {
                    var document = snapshot.data?.docs[index];
                    if (document != null && document.id == uid) {
                      switch (document['M4A5']) {
                        case true:
                          return IconButton(
                              icon: Icon(MdiIcons.star,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('M4A5', false)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Eliminado de favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                        case false:
                          return IconButton(
                              icon: Icon(MdiIcons.starOutline,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('M4A5', true)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Agregado a favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                      }
                    } else {
                      return Container();
                    }
                    return null;
                  }),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(
                color: Colors.red,
              ),
            );
          }
        });
  }

  Widget bottomStructure() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        nextButton(),
      ],
    );
  }

  Widget nextButton() {
    Size size = MediaQuery.of(context).size;
    String empty = '-';

    if (activeStep == 1) {
      return SizedBox(
        width: size.width / 1,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        WidgetStateProperty.all<Color>(CustomColors.primary3),
                    shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: const BorderSide(color: Colors.white)),
                    ),
                  ),
                  onPressed: () {
                    setState(() {
                      activeStep = activeStep - 1;
                    });
                  },
                  child: const Text(
                    'Anterior',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(color: Colors.white),
                  )),
            ),
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: WidgetStateProperty.all<Color>(active3
                      ? CustomColors.primary
                      : CustomColors.greenButton),
                  shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ),
                  ),
                ),
                onPressed: () {
                  if (activeStep < 2) {
                    if (activeStep == 1) {
                      if (_form1Key.currentState!.validate()) {
                        FirestoreService()
                            .guardarRespuestasM4A5(
                              situacion1.text.trim(),
                              situacion2.text.trim(),
                              situacion3.text.trim(),
                              situacion4.text.isEmpty
                                  ? empty
                                  : situacion4.text.trim(),
                              situacion5.text.isEmpty
                                  ? empty
                                  : situacion4.text.trim(),
                              situacion6.text.isEmpty
                                  ? empty
                                  : situacion4.text.trim(),
                              situacion7.text.isEmpty
                                  ? empty
                                  : situacion4.text.trim(),
                            )
                            .then(
                              (value) => setState(() {
                                activeStep++;
                              }),
                            );
                      } else {
                        showDialog<String>(
                          context: context,
                          builder: (BuildContext context) => AlertDialog(
                            title: const Text(
                              'Error',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  color: Colors.red,
                                  fontWeight: FontWeight.bold),
                            ),
                            content: const Text(
                              'Complete los campos faltantes',
                              textScaler: TextScaler.linear(1.0),
                            ),
                            actions: <Widget>[
                              TextButton(
                                onPressed: () => Navigator.pop(
                                  context,
                                ),
                                child: const Text(
                                  'OK',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      color: CustomColors.primaryDark,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                        );
                      }
                    } else {
                      setState(() {
                        activeStep++;
                      });
                    }
                  } else {
                    FirestoreService().terminaSesion('4', '5',
                        'Leyó la introducción → Ordenó las situaciones que evita de menos difícil a más difícil → Terminó la actividad 27');
                    var tiempo = DateTime.now();
                    var difTiempo = tiempo.difference(timeStart);
                    FirestoreService().setTime(
                        tiempo.toString() + ' - ENDED - M4A5',
                        'Terminó M4A5',
                        difTiempo.inMinutes.toString() + ' minutos');
                    FirestoreService().updateCurrentPage('M4A5', '1');
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute<void>(
                            builder: (BuildContext context) => const MainPage(
                                  activePage: 0,
                                )),
                        (route) => false);
                  }
                },
                child: Text(
                  buttonText(),
                  textScaler: const TextScaler.linear(1.0),
                  style: const TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      return SizedBox(
        width: size.width / 1.2,
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor: WidgetStateProperty.all<Color>(
                active3 ? CustomColors.primary : CustomColors.greenButton),
            shape: WidgetStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              ),
            ),
          ),
          onPressed: () {
            if (activeStep < 2) {
              if (activeStep == 1) {
                if (_form1Key.currentState!.validate()) {
                  FirestoreService()
                      .guardarRespuestasM4A5(
                        situacion1.text.trim(),
                        situacion2.text.trim(),
                        situacion3.text.trim(),
                        situacion4.text.isEmpty
                            ? empty
                            : situacion4.text.trim(),
                        situacion5.text.isEmpty
                            ? empty
                            : situacion4.text.trim(),
                        situacion6.text.isEmpty
                            ? empty
                            : situacion4.text.trim(),
                        situacion7.text.isEmpty
                            ? empty
                            : situacion4.text.trim(),
                      )
                      .then(
                        (value) => setState(() {
                          activeStep++;
                        }),
                      );
                } else {
                  showDialog<String>(
                    context: context,
                    builder: (BuildContext context) => AlertDialog(
                      title: const Text(
                        'Error',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            color: Colors.red, fontWeight: FontWeight.bold),
                      ),
                      content: const Text(
                        'Complete los campos faltantes',
                        textScaler: TextScaler.linear(1.0),
                      ),
                      actions: <Widget>[
                        TextButton(
                          onPressed: () => Navigator.pop(
                            context,
                          ),
                          child: const Text(
                            'OK',
                            textScaler: TextScaler.linear(1.0),
                            style: TextStyle(
                                color: CustomColors.primaryDark,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                  );
                }
              } else {
                setState(() {
                  activeStep++;
                });
              }
            } else {
              FirestoreService().terminaSesion('4', '5',
                  'Leyó la introducción → Ordenó las situaciones que evita de menos difícil a más difícil → Terminó la actividad 27');
              var tiempo = DateTime.now();
              var difTiempo = tiempo.difference(timeStart);
              FirestoreService().setTime(tiempo.toString() + ' - ENDED - M4A5',
                  'Terminó M4A5', difTiempo.inMinutes.toString() + ' minutos');
              FirestoreService().updateCurrentPage('M4A5', '1');
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute<void>(
                      builder: (BuildContext context) => const MainPage(
                            activePage: 0,
                          )),
                  (route) => false);
            }
          },
          child: Text(
            buttonText(),
            textScaler: const TextScaler.linear(1.0),
            style: TextStyle(
              color: active3 ? CustomColors.surface : CustomColors.primaryDark,
            ),
          ),
        ),
      );
    }
  }

  Widget header() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 2:
        return Image(
          width: size.width / 8,
          height: size.width / 8,
          fit: BoxFit.scaleDown,
          image: AssetImage(imageHeader()),
        );
      default:
        return Image(
          width: kIsWeb ? 100 : size.width / 3,
          height: kIsWeb ? 100 : size.width / 3,
          fit: BoxFit.scaleDown,
          image: AssetImage(imageHeader()),
        );
    }
  }

  String imageHeader() {
    switch (activeStep) {
      case 0:
        return 'assets/images/m4a5.png';

      case 2:
        return 'assets/images/end.png';

      default:
        return 'assets/images/m4a5.png';
    }
  }

  String headerText() {
    switch (activeStep) {
      case 2:
        return '';
      default:
        return 'Avanzando paso a paso';
    }
  }

  String buttonText() {
    switch (activeStep) {
      case 0:
        return 'Comencemos';
      case 1:
        return 'Finalizar';
      case 2:
        return 'Continuar con el programa';

      default:
        return 'Siguiente';
    }
  }

  Widget content() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.all(20),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Para poder generar un cambio real en tu vida, es necesario que pongas en práctica ',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                'las estrategias que pensaste en el ejercicio anterior.\n',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                              text:
                                  'Así tendrás éxito en enfrentar, aceptar y manejar de mejor forma las situaciones difíciles o incómodas. Para esto es importante avanzar paso a paso y de forma progresiva.'),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.all(10),
                    alignment: Alignment.topLeft,
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              margin: const EdgeInsets.only(left: 10),
                              child: const Text(
                                'Tus alternativas de estrategias',
                                textScaler: TextScaler.linear(1.0),
                                style: TextStyle(
                                    fontSize: 20, color: CustomColors.surface),
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.only(left: 10),
                              child: Icon(
                                MdiIcons.informationOutline,
                                color: CustomColors.surface,
                              ),
                            ),
                          ],
                        ),
                        InkWell(
                          onTap: () {
                            showDialog<String>(
                              context: context,
                              builder: (BuildContext context) => AlertDialog(
                                shape: const RoundedRectangleBorder(
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(32.0))),
                                title: const Text(
                                  'Tus alternativas de estrategia',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      fontSize: 20,
                                      color: CustomColors.primaryDark),
                                ),
                                content: const Text(
                                  'Esta es la respuesta que diste en la actividad anterior "Creando alternativas"',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      color: CustomColors.primaryDark),
                                ),
                                actions: <Widget>[
                                  TextButton(
                                    onPressed: () =>
                                        Navigator.pop(context, 'Cerrar'),
                                    child: const Text(
                                      'Cerrar',
                                      textScaler: TextScaler.linear(1.0),
                                      style: TextStyle(
                                          color: CustomColors.primary),
                                    ),
                                  ),
                                ],
                              ),
                            );
                          },
                          child: Container(
                            width: size.width,
                            margin: const EdgeInsets.all(10),
                            padding: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                color: CustomColors.surface.withAlpha(30)),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  margin:
                                      const EdgeInsets.only(top: 5, bottom: 5),
                                  child: const Text(
                                    'Alternativa 1:',
                                    textScaler: TextScaler.linear(1.0),
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                        color: CustomColors.surface),
                                  ),
                                ),
                                Container(
                                  margin:
                                      const EdgeInsets.only(top: 5, bottom: 5),
                                  child: Text(
                                    alternativa1,
                                    textScaler: const TextScaler.linear(1.0),
                                    style: const TextStyle(
                                        fontSize: 16,
                                        color: CustomColors.surface),
                                  ),
                                ),
                                Visibility(
                                  visible: alter2,
                                  child: Container(
                                    margin: const EdgeInsets.only(
                                        top: 30, bottom: 5),
                                    child: const Text(
                                      'Alternativa 2:',
                                      textScaler: TextScaler.linear(1.0),
                                      style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold,
                                          color: CustomColors.surface),
                                    ),
                                  ),
                                ),
                                Visibility(
                                  visible: alter2,
                                  child: Container(
                                    margin: const EdgeInsets.only(
                                        top: 5, bottom: 5),
                                    child: Text(
                                      alternativa2,
                                      textScaler: const TextScaler.linear(1.0),
                                      style: const TextStyle(
                                          fontSize: 16,
                                          color: CustomColors.surface),
                                    ),
                                  ),
                                ),
                                Visibility(
                                  visible: alter3,
                                  child: Container(
                                    margin: const EdgeInsets.only(
                                        top: 30, bottom: 5),
                                    child: const Text(
                                      'Alternativa 3:',
                                      textScaler: TextScaler.linear(1.0),
                                      style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold,
                                          color: CustomColors.surface),
                                    ),
                                  ),
                                ),
                                Visibility(
                                  visible: alter3,
                                  child: Container(
                                    margin: const EdgeInsets.only(
                                        top: 5, bottom: 5),
                                    child: Text(
                                      alternativa3,
                                      textScaler: const TextScaler.linear(1.0),
                                      style: const TextStyle(
                                          fontSize: 16,
                                          color: CustomColors.surface),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.all(20),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'En esta actividad queremos apoyarte en este proceso por medio del establecimiento de una jerarquía en la que puedas ordenar las situaciones que evitas y que quieres enfrentar de acuerdo al grado de malestar que cada una de estas situaciones te generan.',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );

      case 1:
        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.all(20),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Lo primero que debes hacer es determinar cuál es la situación que te resulta más difícil y que más evitas y luego avanzar en la lista hasta que llegues a la situación que te genera menos angustia y que menos evitas. Por ejemplo, si la situación que evitas es hablar en público, la situación más difícil podría ser hablar frente a una audiencia, mientras que la situación menos difícil podría ser hablar con un amigx. Con esto en mente te invitamos a que te tomes tu tiempo para crear tu jerarquía la cual puedes dejar en el siguiente espacio:',
                        style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.surface,
                          height: 1.5,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.all(10),
                    child: Form(
                      key: _form1Key,
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Container(
                                margin:
                                    const EdgeInsets.only(left: 10, right: 10),
                                child: Icon(
                                  MdiIcons.arrowUp,
                                  color: CustomColors.surface,
                                ),
                              ),
                              Container(
                                alignment: Alignment.centerLeft,
                                child: const Text(
                                  'Situación menos dificil',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      fontSize: 20, color: Colors.white),
                                ),
                              ),
                            ],
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              validator: (String? value) {
                                return (value == null || value == '')
                                    ? 'Campo vacío.'
                                    : null;
                              },
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: situacion1,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: '...',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              validator: (String? value) {
                                return (value == null || value == '')
                                    ? 'Campo vacío.'
                                    : null;
                              },
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: situacion2,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: '...',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              validator: (String? value) {
                                return (value == null || value == '')
                                    ? 'Campo vacío.'
                                    : null;
                              },
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: situacion3,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: '...',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: situacion4,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: '...',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: situacion5,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: '...',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: situacion6,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: '...',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: situacion7,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: '...',
                                  border: OutlineInputBorder()),
                            ),
                          ),
                          Row(
                            children: [
                              Container(
                                margin:
                                    const EdgeInsets.only(left: 10, right: 10),
                                child: Icon(
                                  MdiIcons.arrowDown,
                                  color: CustomColors.surface,
                                ),
                              ),
                              Container(
                                alignment: Alignment.centerLeft,
                                child: const Text(
                                  'Situación más dificil',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      fontSize: 20, color: Colors.white),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );

      case 2:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });
        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Container(
                margin: const EdgeInsets.all(15),
                child: Column(
                  children: [
                    topStructure(),
                    Container(
                      margin: const EdgeInsets.all(10),
                      child: const FittedBox(
                        fit: BoxFit.fitWidth,
                        child: Text(
                          '¡Muy bien!',
                          textScaler: TextScaler.linear(1.0),
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              // color: CustomColors.primaryDark,
                              fontSize: 24),
                        ),
                      ),
                    ),
                    const Text(
                      'Tenemos una recompensa para ti:',
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(fontSize: 16),
                      textAlign: TextAlign.center,
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 40, bottom: 10),
                      decoration: BoxDecoration(
                          color: CustomColors.lightContainer,
                          border:
                              Border.all(width: 2, color: CustomColors.surface),
                          borderRadius: BorderRadius.circular(15)),
                      child: Row(
                        children: [
                          const Image(
                            width: 100,
                            height: 120,
                            fit: BoxFit.scaleDown,
                            image: AssetImage(
                                'assets/images/respiracion-diafragma-icon.png'),
                          ),
                          Container(
                            margin: const EdgeInsets.only(left: 10),
                            child: const Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Actividad complementaria:',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      fontSize: 13,
                                      color: CustomColors.primaryDark),
                                ),
                                Text(
                                  'Respiración diafragmática',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16,
                                      color: CustomColors.primaryDark),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );

      default:
        return Container();
    }
  }
}
