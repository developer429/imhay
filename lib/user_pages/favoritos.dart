// ignore_for_file: non_constant_identifier_names

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:seguimiento/colors.dart';
import 'package:seguimiento/user_pages/S1-AC/1-balance-vida.dart';
import 'package:seguimiento/user_pages/S1-AC/2-org-tiempo-estudio.dart';
import 'package:seguimiento/user_pages/S1-AC/3-superar-procastinacion.dart';
import 'package:seguimiento/user_pages/S1-AC/4-afrontar-estres.dart';
import 'package:seguimiento/user_pages/S1-AC/5-amistades-vida.dart';
import 'package:seguimiento/user_pages/S1-AC/6-resolviendo-conflictos.dart';
import 'package:seguimiento/user_pages/S1-AC/7-trabajos-grupo.dart';
import 'package:seguimiento/user_pages/S2-AC/10-resolviendo-problemas.dart';
import 'package:seguimiento/user_pages/S2-AC/11-superando-sobrecarga.dart';
import 'package:seguimiento/user_pages/S2-AC/12-cambiando-forma.dart';
import 'package:seguimiento/user_pages/S2-AC/8-buen-dormir.dart';
import 'package:seguimiento/user_pages/S2-AC/9-habitos-saludables.dart';
import 'package:seguimiento/user_pages/S3-AC/13-agua-rio.dart';
import 'package:seguimiento/user_pages/S3-AC/14-bosque-lluvioso.dart';
import 'package:seguimiento/user_pages/S3-AC/15-respiracion-dormir.dart';
import 'package:seguimiento/user_pages/S3-AC/16-relajacion-respiracion.dart';
import 'package:seguimiento/user_pages/S3-AC/17-selva-tropical.dart';
import 'package:seguimiento/user_pages/S3-AC/18-viento.dart';
import 'package:seguimiento/user_pages/S3-AC/19-noche-verano.dart';
import 'package:seguimiento/user_pages/S3-AC/20-olas-suaves.dart';
import 'package:seguimiento/user_pages/S3-AC/21-lluvia-tranquila.dart';
import 'package:seguimiento/user_pages/S3-AC/22-respiracion-diafragma.dart';
import 'package:seguimiento/user_pages/S4-AC/23-consejos-mindful.dart';
import 'package:seguimiento/user_pages/S4-AC/24-practica-mindful.dart';
import 'package:seguimiento/user_pages/S4-AC/25-practica-aware.dart';
import 'package:seguimiento/user_pages/S5-AC/26-busqueda-ayuda.dart';
import 'package:seguimiento/user_pages/S5-AC/27-prevencion-suicidio.dart';
import 'package:seguimiento/user_pages/S5-AC/28-hablemos-bullying.dart';
import 'package:seguimiento/user_pages/S5-AC/29-hablemos-ciberbullying.dart';
import 'package:seguimiento/user_pages/S5-AC/30-manejo-crisis.dart';
import 'package:seguimiento/user_pages/S5-AC/31-consumo-universitario.dart';
import 'package:seguimiento/user_pages/termometro.dart';
import 'M1/m1a0.dart';
import 'M1/m1a1.dart';
import 'M1/m1a2.dart';
import 'M1/m1a3.dart';
import 'M1/m1a4.dart';
import 'M1/m1a5.dart';
import 'M2/m2a1.dart';
import 'M2/m2a2.dart';
import 'M2/m2a3.dart';
import 'M2/m2a4.dart';
import 'M2/m2a5.dart';
import 'M2/m2a6.dart';
import 'M3/m3a1.dart';
import 'M3/m3a2.dart';
import 'M3/m3a3.dart';
import 'M3/m3a4.dart';
import 'M3/m3a5.dart';
import 'M3/m3a6.dart';
import 'M3/m3a7.dart';
import 'M3/m3a8.dart';
import 'M4/m4a1.dart';
import 'M4/m4a2.dart';
import 'M4/m4a3.dart';
import 'M4/m4a4.dart';
import 'M4/m4a5.dart';
import 'M4/m4a6.dart';

class Favoritos extends StatefulWidget {
  const Favoritos({
    Key? key,
  }) : super(key: key);

  @override
  State<Favoritos> createState() => _FavoritosState();
}

class _FavoritosState extends State<Favoritos> {
  String uid = FirebaseAuth.instance.currentUser!.uid;
  bool M1A0F = false;
  bool M1A1F = false;
  bool M1A2F = false;
  bool M1A3F = false;
  bool M1A4F = false;
  bool M1A5F = false;
  bool M1ATF = false;

  bool M2A1F = false;
  bool M2A2F = false;
  bool M2A3F = false;
  bool M2A4F = false;
  bool M2A5F = false;
  bool M2A6F = false;
  bool M2ATF = false;

  bool M3A1F = false;
  bool M3A2F = false;
  bool M3A3F = false;
  bool M3A4F = false;
  bool M3A5F = false;
  bool M3A6F = false;
  bool M3A7F = false;
  bool M3A8F = false;
  bool M3ATF = false;

  bool M4A1F = false;
  bool M4A2F = false;
  bool M4A3F = false;
  bool M4A4F = false;
  bool M4A5F = false;
  bool M4A6F = false;
  bool M4ATF = false;

  bool A1F = false;
  bool A2F = false;
  bool A3F = false;
  bool A4F = false;
  bool A5F = false;
  bool A6F = false;
  bool A7F = false;
  bool A8F = false;
  bool A9F = false;
  bool A10F = false;
  bool A11F = false;
  bool A12F = false;
  bool A13F = false;
  bool A14F = false;
  bool A15F = false;
  bool A16F = false;
  bool A17F = false;
  bool A18F = false;
  bool A19F = false;
  bool A20F = false;
  bool A21F = false;
  bool A22F = false;
  bool A23F = false;
  bool A24F = false;
  bool A25F = false;
  bool A26F = false;
  bool A27F = false;
  bool A28F = false;
  bool A29F = false;
  bool A30F = false;
  bool A31F = false;
  List<bool> evaluateFavs = [];
  CollectionReference favoritosReference =
      FirebaseFirestore.instance.collection('favoritos');
  void getFavourites() {
    favoritosReference.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        M1A0F = fields['M1A0'];
        evaluateFavs.add(M1A0F);
        M1A1F = fields['M1A1'];
        evaluateFavs.add(M1A1F);
        M1A2F = fields['M1A2'];
        evaluateFavs.add(M1A2F);
        M1A3F = fields['M1A3'];
        evaluateFavs.add(M1A3F);
        M1A4F = fields['M1A4'];
        evaluateFavs.add(M1A4F);
        M1A5F = fields['M1A5'];
        evaluateFavs.add(M1A5F);
        M1ATF = fields['M1AT'];
        evaluateFavs.add(M1ATF);

        M2A1F = fields['M2A1'];
        evaluateFavs.add(M2A1F);
        M2A2F = fields['M2A2'];
        evaluateFavs.add(M2A2F);
        M2A3F = fields['M2A3'];
        evaluateFavs.add(M2A3F);
        M2A4F = fields['M2A4'];
        evaluateFavs.add(M2A4F);
        M2A5F = fields['M2A5'];
        evaluateFavs.add(M2A5F);
        M2A6F = fields['M2A6'];
        evaluateFavs.add(M2A6F);
        M2ATF = fields['M2AT'];
        evaluateFavs.add(M2ATF);

        M3A1F = fields['M3A1'];
        evaluateFavs.add(M3A1F);

        M3A2F = fields['M3A2'];
        evaluateFavs.add(M3A2F);
        M3A3F = fields['M3A3'];
        evaluateFavs.add(M3A3F);
        M3A4F = fields['M3A4'];
        evaluateFavs.add(M3A4F);
        M3A5F = fields['M3A5'];
        evaluateFavs.add(M3A5F);
        M3A6F = fields['M3A6'];
        evaluateFavs.add(M3A6F);
        M3A7F = fields['M3A7'];
        evaluateFavs.add(M3A7F);
        M3A8F = fields['M3A8'];
        evaluateFavs.add(M3A8F);
        M3ATF = fields['M3AT'];
        evaluateFavs.add(M3ATF);

        M4A1F = fields['M4A1'];
        evaluateFavs.add(M4A1F);
        M4A2F = fields['M4A2'];
        evaluateFavs.add(M4A2F);
        M4A3F = fields['M4A3'];
        evaluateFavs.add(M4A3F);
        M4A4F = fields['M4A4'];
        evaluateFavs.add(M4A4F);
        M4A5F = fields['M4A5'];
        evaluateFavs.add(M4A5F);
        M4A6F = fields['M4A6'];
        evaluateFavs.add(M4A6F);
        M4ATF = fields['M4AT'];
        evaluateFavs.add(M4ATF);

        A1F = fields['A1'];
        evaluateFavs.add(A1F);
        A2F = fields['A2'];
        evaluateFavs.add(A2F);
        A3F = fields['A3'];
        evaluateFavs.add(A3F);
        A4F = fields['A4'];
        evaluateFavs.add(A4F);
        A5F = fields['A5'];
        evaluateFavs.add(A5F);
        A6F = fields['A6'];
        evaluateFavs.add(A6F);
        A7F = fields['A7'];
        evaluateFavs.add(A7F);
        A8F = fields['A8'];
        evaluateFavs.add(A8F);
        A9F = fields['A9'];
        evaluateFavs.add(A9F);
        A10F = fields['A10'];
        evaluateFavs.add(A10F);
        A11F = fields['A11'];
        evaluateFavs.add(A11F);
        A12F = fields['A12'];
        evaluateFavs.add(A12F);
        A13F = fields['A13'];
        evaluateFavs.add(A13F);
        A14F = fields['A14'];
        evaluateFavs.add(A14F);
        A15F = fields['A15'];
        evaluateFavs.add(A15F);
        A16F = fields['A16'];
        evaluateFavs.add(A16F);
        A17F = fields['A17'];
        evaluateFavs.add(A17F);
        A18F = fields['A18'];
        evaluateFavs.add(A18F);
        A19F = fields['A19'];
        evaluateFavs.add(A19F);
        A20F = fields['A20'];
        evaluateFavs.add(A20F);
        A21F = fields['A21'];
        evaluateFavs.add(A21F);
        A22F = fields['A22'];
        evaluateFavs.add(A22F);
        A23F = fields['A23'];
        evaluateFavs.add(A23F);
        A24F = fields['A24'];
        evaluateFavs.add(A24F);
        A25F = fields['A25'];
        evaluateFavs.add(A25F);
        A26F = fields['A26'];
        evaluateFavs.add(A26F);
        A27F = fields['A27'];
        evaluateFavs.add(A27F);
        A28F = fields['A28'];
        evaluateFavs.add(A28F);
        A29F = fields['A29'];
        evaluateFavs.add(A29F);
        A30F = fields['A30'];
        evaluateFavs.add(A30F);
        A31F = fields['A31'];
        evaluateFavs.add(A31F);
      });
    });
  }

  ScrollController scrollController = ScrollController();
  int activeStep = 0;
  @override
  void initState() {
    super.initState();
    getFavourites();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: CustomColors.background,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: CustomColors.background,
          body: Center(
            child: Column(
              children: [
                content(),
                Container(
                  margin: const EdgeInsets.only(top: 10, bottom: 10),
                  child: bottomStructure(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget topStructure() {
    return Stack(
      children: [
        Column(
          children: [
            Row(
              children: [
                Container(
                  alignment: Alignment.topRight,
                  child: IconButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    icon: const Icon(
                      Icons.arrow_back,
                      color: CustomColors.primaryDark,
                    ),
                  ),
                ),
              ],
            ),
            Text(
              headerText(),
              textScaler: const TextScaler.linear(1.0),
              textAlign: TextAlign.center,
              style: const TextStyle(
                color: CustomColors.primaryDark,
                fontSize: 24,
              ),
            ),
            header(),
          ],
        ),
      ],
    );
  }

  Widget bottomStructure() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        nextButton(),
      ],
    );
  }

  Widget nextButton() {
    Size size = MediaQuery.of(context).size;

    return SizedBox(
      width: size.width / 1.2,
      child: ElevatedButton(
        style: ButtonStyle(
          backgroundColor: WidgetStateProperty.all<Color>(CustomColors.primary),
          shape: WidgetStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(18.0),
            ),
          ),
        ),
        onPressed: () {
          Navigator.of(context).pop();
        },
        child: Text(
          buttonText(),
          textScaler: const TextScaler.linear(1.0),
          style: const TextStyle(color: CustomColors.surface),
        ),
      ),
    );
  }

  Widget header() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      default:
        return Container(
          margin: const EdgeInsets.all(15),
          decoration: const BoxDecoration(),
          child: Image(
            width: size.width / 3,
            height: size.width / 3,
            fit: BoxFit.scaleDown,
            image: AssetImage(imageHeader()),
          ),
        );
    }
  }

  String imageHeader() {
    switch (activeStep) {
      case 0:
        return 'assets/images/favoritos.png';

      default:
        return ' ';
    }
  }

  String headerText() {
    switch (activeStep) {
      case 0:
        return 'Tus favoritos';

      default:
        return ' ';
    }
  }

  String buttonText() {
    switch (activeStep) {
      case 0:
        return 'Volver a mi perfil';

      default:
        return ' ';
    }
  }

  Widget content() {
    switch (activeStep) {
      case 0:
        return Expanded(
          child: SingleChildScrollView(
            controller: scrollController,
            child: Column(
              children: [
                favoritos(),
              ],
            ),
          ),
        );

      default:
        return Container();
    }
  }

  Widget favoritos() {
    Size size = MediaQuery.of(context).size;

    return Container(
      margin: const EdgeInsets.only(bottom: 50, left: 5, right: 5),
      //height: size.height,
      child: ListView(
        shrinkWrap: true,
        controller: scrollController,
        children: [
          topStructure(),
          Container(
            margin:
                const EdgeInsets.only(top: 5, right: 20, left: 20, bottom: 5),
            child: RichText(
              text: const TextSpan(
                text:
                    'Aquí puedes encontrar los contenidos que hayas guardado como tus favoritos. Esto te permitirá acceder rápidamente a aquellos contenidos que más te hayan gustado todas las veces que quieras.',
                style: TextStyle(
                    fontSize: 16, color: CustomColors.primary, height: 1.5),
                children: <TextSpan>[],
              ),
            ),
          ),
          Visibility(
            visible: !evaluateFavs.contains(true),
            child: Container(
              height: size.height / 3,
              alignment: Alignment.center,
              child: const Text(
                'No hay datos',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(
                    color: CustomColors.primary,
                    fontWeight: FontWeight.bold,
                    fontSize: 24),
              ),
            ),
          ),
          Visibility(
            visible: M1A0F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const M1A0()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/m1a0-mpi.png'),
                ),
                subtitle: const Text(
                  'Vamos juntxs',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Lectura',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: M1A0F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: M1A1F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const M1A1()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/m1a1-mpi.png'),
                ),
                subtitle: const Text(
                  '¿Qué es la depresión?',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Video',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: M1A1F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: M1A2F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const M1A2()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/m1a2-mpi.png'),
                ),
                subtitle: const Text(
                  'Mitos sobre la depresión',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Ejercicio',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: M1A2F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: M1A3F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const M1A3()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/m1a3-mpi.png'),
                ),
                subtitle: const Text(
                  '¿Qué es la ansiedad?',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Video',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: M1A3F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: M1A4F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const M1A4()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/m1a4-mpi.png'),
                ),
                subtitle: const Text(
                  '7 Principios para tu activación conductual',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Lectura',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: M1A4F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: M1A5F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const M1A5()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/m1a5-mpi.png'),
                ),
                subtitle: const Text(
                  'Aplicando la Activación Conductual',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Ejercicio',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: M1A5F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: M1ATF,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const Termometro(
                            modulo: '1',
                          )),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/mat-mpi.png'),
                ),
                subtitle: const Text(
                  'Termómetro del Ánimo',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Ejercicio',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: M1ATF,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: M2A1F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const M2A1()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/m2a1-mpi.png'),
                ),
                subtitle: const Text(
                  'Redes de apoyo',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Video',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: M2A1F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: M2A2F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const M2A2()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/m2a2-mpi.png'),
                ),
                subtitle: const Text(
                  'El Ecomapa',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Lectura',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: M2A2F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: M2A3F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const M2A3()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/m2a3-mpi.png'),
                ),
                subtitle: const Text(
                  'Tu red de apoyo',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Ejercicio',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: M2A3F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: M2A4F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const M2A4()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/m2a4-mpi.png'),
                ),
                subtitle: const Text(
                  'Regulación emocional',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Lectura',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: M2A4F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: M2A5F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const M2A5()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/m2a5-mpi.png'),
                ),
                subtitle: const Text(
                  '4 estrategias de regulación emocional',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Lectura',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: M2A5F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: M2A6F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const M2A6()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/m2a6-mpi.png'),
                ),
                subtitle: const Text(
                  'Guía de regulación emocional',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Ejercicio',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: M2A6F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: M2ATF,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const Termometro(
                            modulo: '2',
                          )),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/mat-mpi.png'),
                ),
                subtitle: const Text(
                  'Termómetro del Ánimo',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Ejercicio',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: M2ATF,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: M3A1F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const M3A1()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/m3a1-mpi.png'),
                ),
                subtitle: const Text(
                  '¿Qué son los errores del pensamiento?',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Lectura',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: M3A1F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: M3A2F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const M3A2()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/m3a2-mpi.png'),
                ),
                subtitle: const Text(
                  'Identificando errores del pensamiento',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Ejercicio',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: M3A2F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: M3A3F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const M3A3()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/m3a3-mpi.png'),
                ),
                subtitle: const Text(
                  '5 Pasos para enfrentar los pensamientos negativos',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Lectura',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: M3A3F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: M3A4F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const M3A4()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/m3a4-mpi.png'),
                ),
                subtitle: const Text(
                  'Resolución de problemas',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Lectura',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: M3A4F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: M3A5F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const M3A5()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/m3a5-mpi.png'),
                ),
                subtitle: const Text(
                  'Definiendo el problema',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Ejercicio',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: M3A5F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: M3A6F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const M3A6()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/m3a6-mpi.png'),
                ),
                subtitle: const Text(
                  'Generando alternativas de solución',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Ejercicio',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: M3A6F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: M3A7F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const M3A7()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/m3a7-mpi.png'),
                ),
                subtitle: const Text(
                  'Tomando decisiones',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Ejercicio',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: M3A7F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: M3A8F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const M3A8()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/m3a8-mpi.png'),
                ),
                subtitle: const Text(
                  'Implementando la solución y evaluando la solución',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Lectura',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: M3A8F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: M3ATF,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const Termometro(
                            modulo: '3',
                          )),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/mat-mpi.png'),
                ),
                subtitle: const Text(
                  'Termómetro del Ánimo',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Ejercicio',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: M3ATF,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: M4A1F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const M4A1()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/m4a1-mpi.png'),
                ),
                subtitle: const Text(
                  'Exposición y evitación',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Lectura',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: M4A1F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: M4A2F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const M4A2()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/m4a3-mpi.png'),
                ),
                subtitle: const Text(
                  'Con los ojos en la meta',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Ejercicio',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: M4A2F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: M4A3F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const M4A3()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/m4a3-mpi.png'),
                ),
                subtitle: const Text(
                  '¿Qué es lo que evitas?',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Ejercicio',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: M4A3F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: M4A4F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const M4A4()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/m4a4-mpi.png'),
                ),
                subtitle: const Text(
                  'Creando alternativas',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Ejercicio',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: M4A4F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: M4A5F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const M4A5()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/m4a5-mpi.png'),
                ),
                subtitle: const Text(
                  'Avanzando paso a paso',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Ejercicio',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: M4A5F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: M4A6F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const M4A6()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/m4a6-mpi.png'),
                ),
                subtitle: const Text(
                  'Tu plan de exposición',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Ejercicio',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: M4A6F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: M4ATF,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const Termometro(
                            modulo: '4',
                          )),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/mat-mpi.png'),
                ),
                subtitle: const Text(
                  'Termómetro del Ánimo',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Ejercicio',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: M4ATF,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: A1F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const A1()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/balance-vida-icon.png'),
                ),
                subtitle: const Text(
                  'Balance de vida personal y universidad',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Actividad complementaria',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: A1F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: A2F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const A2()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/org-tiempo-icon.png'),
                ),
                subtitle: const Text(
                  'Organización del tiempo y estudio',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Actividad complementaria',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: A2F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: A3F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const A3()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage(
                      'assets/images/superar-procastinacion-icon.png'),
                ),
                subtitle: const Text(
                  'Superando la procastinación',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Actividad complementaria',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: A3F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: A4F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const A4()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/afrontar-estres-icon.png'),
                ),
                subtitle: const Text(
                  'Afrontando el estrés académico',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Actividad complementaria',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: A4F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: A5F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const A5()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/amistades-social-icon.png'),
                ),
                subtitle: const Text(
                  'Amistades y vida social',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Actividad complementaria',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: A5F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: A6F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const A6()),
                    ),
                  );
                },
                leading: const Image(
                  image:
                      AssetImage('assets/images/resolver-conflictos-icon.png'),
                ),
                subtitle: const Text(
                  'Resolviendo conflictos',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Actividad complementaria',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: A6F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: A7F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const A7()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/trabajos-grupo-icon.png'),
                ),
                subtitle: const Text(
                  'Trabajos en grupo: desafíos y oportunidades',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Actividad complementaria',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: A7F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: A8F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const A8()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/buen-dormir-icon.png'),
                ),
                subtitle: const Text(
                  'Buen dormir',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Actividad complementaria',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: A8F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: A9F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const A9()),
                    ),
                  );
                },
                leading: const Image(
                  image:
                      AssetImage('assets/images/habitos-saludables-icon.png'),
                ),
                subtitle: const Text(
                  'Hábitos saludables',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Actividad complementaria',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: A9F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: A10F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const A10()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage(
                      'assets/images/resolviendo-problemas-icon.png'),
                ),
                subtitle: const Text(
                  '¿Cómo estás resolviendo problemas?',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Actividad complementaria',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: A10F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: A11F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const A11()),
                    ),
                  );
                },
                leading: const Image(
                  image:
                      AssetImage('assets/images/superando-sobrecarga-icon.png'),
                ),
                subtitle: const Text(
                  'Superando la sobrecarga mental',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Actividad complementaria',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: A11F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: A12F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const A12()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/cambiando-forma-icon.png'),
                ),
                subtitle: const Text(
                  'Cambiando la forma en la que nos hablamos',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Actividad complementaria',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: A12F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: A13F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const A13()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/agua-icon.png'),
                ),
                subtitle: const Text(
                  'Agua de río',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Actividad complementaria',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: A13F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: A14F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const A14()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/bosque-icon.png'),
                ),
                subtitle: const Text(
                  'Bosque lluvioso',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Actividad complementaria',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: A14F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: A15F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const A15()),
                    ),
                  );
                },
                leading: const Image(
                  image:
                      AssetImage('assets/images/respiracion-dormir-icon.png'),
                ),
                subtitle: const Text(
                  'Respiración para antes de dormir',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Actividad complementaria',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: A15F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: A16F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const A16()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage(
                      'assets/images/relajacion-respiracion-icon.png'),
                ),
                subtitle: const Text(
                  'Relajación por medio de la respiración',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Actividad complementaria',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: A16F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: A17F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const A17()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/selva-icon.png'),
                ),
                subtitle: const Text(
                  'Selva tropical',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Actividad complementaria',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: A17F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: A18F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const A18()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/viento-icon.png'),
                ),
                subtitle: const Text(
                  'Viento',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Actividad complementaria',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: A18F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: A19F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const A19()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/noche-verano-icon.png'),
                ),
                subtitle: const Text(
                  'Noche de verano',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Actividad complementaria',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: A19F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: A20F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const A20()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/olas-suaves-icon.png'),
                ),
                subtitle: const Text(
                  'Olas suaves',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Actividad complementaria',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: A20F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: A21F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const A21()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/lluvia-tranquila-icon.png'),
                ),
                subtitle: const Text(
                  'Lluvia tranquila',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Actividad complementaria',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: A21F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: A22F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const A22()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage(
                      'assets/images/respiracion-diafragma-icon.png'),
                ),
                subtitle: const Text(
                  'Respiración Diafragmática',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Actividad complementaria',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: A22F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: A23F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const A23()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/consejo-mindful-icon.png'),
                ),
                subtitle: const Text(
                  'Consejos de mindfulness',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Actividad complementaria',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: A23F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: A24F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const A24()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/practica-mindful-icon.png'),
                ),
                subtitle: const Text(
                  'Práctica de mindfulness',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Actividad complementaria',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: A24F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: A25F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const A25()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/practica-aware-icon.png'),
                ),
                subtitle: const Text(
                  'Práctica Aware',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Actividad complementaria',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: A25F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: A26F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const A26()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/busqueda-ayuda-icon.png'),
                ),
                subtitle: const Text(
                  'Búsqueda de ayuda: Primera consulta en salud mental',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Actividad complementaria',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: A26F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: A27F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const A27()),
                    ),
                  );
                },
                leading: const Image(
                  image:
                      AssetImage('assets/images/prevencion-suicidio-icon.png'),
                ),
                subtitle: const Text(
                  'Video Hablemos de prevención del suicidio',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Actividad complementaria',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: A27F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: A28F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const A28()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/hablemos-bullying-icon.png'),
                ),
                subtitle: const Text(
                  'Video Hablemos de bullying',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Actividad complementaria',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: A28F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: A29F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const A29()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage(
                      'assets/images/hablemos-ciberbullying-icon.png'),
                ),
                subtitle: const Text(
                  'Video Hablemos de cyberbullying',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Actividad complementaria',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: A29F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: A30F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const A30()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage('assets/images/manejo-crisis-icon.png'),
                ),
                subtitle: const Text(
                  'Video Manejo de crisis en salud mental',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Actividad complementaria',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: A30F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
          Visibility(
            visible: A31F,
            child: Container(
              alignment: Alignment.center,
              height: size.height / 8,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const A31()),
                    ),
                  );
                },
                leading: const Image(
                  image: AssetImage(
                      'assets/images/consumo-universitario-icon.png'),
                ),
                subtitle: const Text(
                  'Consumo de alcohol y drogas en el contexto universitario',
                  textScaler: TextScaler.linear(1.0),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: TextStyle(
                      color: CustomColors.primaryDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                title: Container(
                  margin: const EdgeInsets.only(top: 5),
                  child: const Text(
                    'Actividad complementaria',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(
                        color: CustomColors.primaryDark, fontSize: 14),
                  ),
                ),
                trailing: Icon(
                  MdiIcons.menuRight,
                  color: CustomColors.primaryDark,
                  size: size.height / 24,
                ),
              ),
            ),
          ),
          Visibility(
            visible: A31F,
            child: const Divider(
              color: Colors.black,
            ),
          ),
        ],
      ),
    );
  }
}
