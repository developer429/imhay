// ignore_for_file: file_names

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:seguimiento/colors.dart';

class HistorialAnimo extends StatefulWidget {
  const HistorialAnimo({
    Key? key,
  }) : super(key: key);

  @override
  State<HistorialAnimo> createState() => _HistorialAnimoState();
}

class _HistorialAnimoState extends State<HistorialAnimo> {
  String uid = FirebaseAuth.instance.currentUser!.uid;
  List<String> horasT = [];
  List<int> estadosT = [];
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('termometros');
  void getTermo() {
    collectionReference.doc(uid).collection('estados').get().then((value) {
      var fields = value;
      setState(() {
        for (var data in fields.docs) {
          horasT.add(data['hora']);
          estadosT.add(data['estado']);
        }
      });
    });
  }

  ScrollController scrollController = ScrollController();

  int activeStep = 0;
  @override
  void initState() {
    super.initState();
    getTermo();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: CustomColors.background,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: CustomColors.background,
          body: Center(
            child: Column(
              children: [
                content(),
                Container(
                  margin: const EdgeInsets.only(top: 10, bottom: 10),
                  child: bottomStructure(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget topStructure() {
    return Stack(
      children: [
        Column(
          children: [
            Row(
              children: [
                Container(
                  alignment: Alignment.topRight,
                  child: IconButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    icon: const Icon(
                      Icons.arrow_back,
                      color: CustomColors.primaryDark,
                    ),
                  ),
                ),
              ],
            ),
            Text(
              headerText(),
              textScaler: const TextScaler.linear(1.0),
              textAlign: TextAlign.center,
              style: const TextStyle(
                color: CustomColors.primaryDark,
                fontSize: 24,
              ),
            ),
            header(),
          ],
        ),
      ],
    );
  }

  Widget bottomStructure() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        nextButton(),
      ],
    );
  }

  Widget nextButton() {
    Size size = MediaQuery.of(context).size;

    return SizedBox(
      width: size.width / 1.2,
      child: ElevatedButton(
        style: ButtonStyle(
          backgroundColor: WidgetStateProperty.all<Color>(CustomColors.primary),
          shape: WidgetStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(18.0),
            ),
          ),
        ),
        onPressed: () {
          Navigator.of(context).pop();
        },
        child: Text(
          buttonText(),
          textScaler: const TextScaler.linear(1.0),
          style: const TextStyle(color: CustomColors.surface),
        ),
      ),
    );
  }

  Widget header() {
    Size size = MediaQuery.of(context).size;
    switch (activeStep) {
      default:
        return Container(
          margin: const EdgeInsets.all(15),
          decoration: const BoxDecoration(),
          child: Image(
            width: size.width / 3,
            height: size.width / 3,
            fit: BoxFit.scaleDown,
            image: AssetImage(imageHeader()),
          ),
        );
    }
  }

  String imageHeader() {
    switch (activeStep) {
      case 0:
        return 'assets/images/termometro.png';

      default:
        return ' ';
    }
  }

  String headerText() {
    switch (activeStep) {
      case 0:
        return 'Tu Historial del ánimo';

      default:
        return ' ';
    }
  }

  String buttonText() {
    switch (activeStep) {
      case 0:
        return 'Volver';

      default:
        return ' ';
    }
  }

  String stateText(int value) {
    switch (value) {
      case 0:
        return 'Estás viviendo un mal momento. Esto no va a durar para siempre. Conversa con alguien cercano.';

      case 1:
        return 'Estás viviendo un mal momento. Esto no va a durar para siempre. Conversa con alguien cercano.';
      case 2:
        return 'Estás viviendo un mal momento. Esto no va a durar para siempre. Conversa con alguien cercano.';
      case 3:
        return 'Estás viviendo un mal momento. Esto no va a durar para siempre. Conversa con alguien cercano.';
      case 4:
        return 'Estás viviendo un mal momento. Esto no va a durar para siempre. Conversa con alguien cercano.';
      case 5:
        return 'Mmmh…tienes uno de esos días no tan buenos. Recuerda a los aliados para una buena salud mental: actividad física regular, alimentación saludable, higiene del sueño, organizar rutinas, pausas para distracción, socializar, etc.';
      case 6:
        return 'Mmmh…tienes uno de esos días no tan buenos. Recuerda a los aliados para una buena salud mental: actividad física regular, alimentación saludable, higiene del sueño, organizar rutinas, pausas para distracción, socializar, etc.';
      case 7:
        return '¡La energía está en ti! Recuerda darte pequeños descansos en medio de tus actividades.';
      case 8:
        return '¡La energía está en ti! Recuerda darte pequeños descansos en medio de tus actividades.';
      case 9:
        return '¡La energía está en ti! Recuerda darte pequeños descansos en medio de tus actividades.';
      case 10:
        return '¡La energía está en ti! Recuerda darte pequeños descansos en medio de tus actividades.';
      default:
        return 'Mmmh…tienes uno de esos días no tan buenos. Recuerda a los aliados para una buena salud mental: actividad física regular, alimentación saludable, higiene del sueño, organizar rutinas, pausas para distracción, socializar, etc.';
    }
  }

  String stateIcon(int value) {
    switch (value) {
      case 0:
        return 'assets/images/termo-high.png';

      case 1:
        return 'assets/images/termo-high.png';
      case 2:
        return 'assets/images/termo-high.png';
      case 3:
        return 'assets/images/termo-high.png';
      case 4:
        return 'assets/images/termo-high.png';
      case 5:
        return 'assets/images/termo-mid.png';
      case 6:
        return 'assets/images/termo-mid.png';
      case 7:
        return 'assets/images/termo-low.png';
      case 8:
        return 'assets/images/termo-low.png';
      case 9:
        return 'assets/images/termo-low.png';
      case 10:
        return 'assets/images/termo-low.png';
      default:
        return 'assets/images/termo-mid.png';
    }
  }

  String stateHint(int value) {
    switch (value) {
      case 0:
        return 'No me siento bien';

      case 1:
        return 'No me siento bien';
      case 2:
        return 'No me siento bien';
      case 3:
        return 'No me siento bien';
      case 4:
        return 'No me siento bien';
      case 5:
        return 'Me siento más o menos';
      case 6:
        return 'Me siento más o menos';
      case 7:
        return 'Me siento bien';
      case 8:
        return 'Me siento bien';
      case 9:
        return 'Me siento bien';
      case 10:
        return 'Me siento bien';
      default:
        return 'Me siento más o menos';
    }
  }

  Widget content() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.all(20),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'En esta sección podrás ver tus respuestas al termómetro del ánimo.',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                      ),
                    ),
                  ),
                  Visibility(
                    visible: estadosT.isEmpty,
                    child: Container(
                      height: size.height / 3,
                      alignment: Alignment.center,
                      child: const Text(
                        'No hay datos',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            color: CustomColors.primary,
                            fontWeight: FontWeight.bold,
                            fontSize: 25),
                      ),
                    ),
                  ),
                  Visibility(
                    visible: estadosT.isNotEmpty,
                    child: Container(
                        margin: const EdgeInsets.all(10), child: estados()),
                  ),
                ],
              ),
            ),
          ),
        );

      default:
        return Container();
    }
  }

  Widget estados() {
    Size size = MediaQuery.of(context).size;

    return ListView.separated(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: estadosT.length,
      itemBuilder: (BuildContext context, int index) {
        return Container(
          margin: const EdgeInsets.all(10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: const EdgeInsets.only(right: 20),
                child: Image(
                  width: 60,
                  height: 60,
                  fit: BoxFit.scaleDown,
                  image: AssetImage(stateIcon(estadosT[index])),
                ),
              ),
              Column(
                children: [
                  SizedBox(
                    width: size.width / 1.5,
                    child: Text(
                      horasT[index],
                      textScaler: const TextScaler.linear(1.0),
                      style: const TextStyle(
                          color: CustomColors.primaryDark,
                          fontWeight: FontWeight.bold,
                          fontSize: 15),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(bottom: 5, top: 5),
                    width: size.width / 1.5,
                    child: Text(
                      estadosT[index].toString() +
                          '. ' +
                          stateHint(
                            estadosT[index],
                          ),
                      textScaler: const TextScaler.linear(1.0),
                      style: const TextStyle(
                          color: CustomColors.primaryDark, fontSize: 20),
                    ),
                  ),
                  SizedBox(
                    width: size.width / 1.5,
                    child: Text(
                      stateText(estadosT[index]),
                      textScaler: const TextScaler.linear(1.0),
                      style: const TextStyle(
                          color: CustomColors.primaryDark, fontSize: 15),
                    ),
                  ),
                ],
              ),
            ],
          ),
        );
      },
      separatorBuilder: (BuildContext context, int index) => const Divider(
        color: CustomColors.primary,
      ),
    );
  }
}
