// ignore_for_file: file_names, avoid_print

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:seguimiento/audio-player.dart';
import 'package:seguimiento/colors.dart';
import 'package:seguimiento/user_pages/MainPage.dart';
import '../../firebase.dart';

class A24 extends StatefulWidget {
  const A24({
    Key? key,
  }) : super(key: key);

  @override
  State<A24> createState() => _A24State();
}

class _A24State extends State<A24> {
  String uid = FirebaseAuth.instance.currentUser!.uid;
  bool loadingData = true;
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('page-control');
  String activeStepData = '';
  void getActivePageStored() {
    collectionReference.doc(uid).get().then((value) {
      var fields = value;
      activeStepData = fields['A24'];
      setState(() {
        if (int.parse(activeStepData) < 6) {
          activeStep = int.parse(activeStepData) - 1;
          Future.delayed(const Duration(milliseconds: 500), () {
            setState(() {
              loadingData = false;
            });
          });
        }
      });
    });
  }

  ScrollController scrollController = ScrollController();
  bool favorite = false;
  CollectionReference favoriteData =
      FirebaseFirestore.instance.collection('favoritos');
  void getFavData() {
    favoriteData.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        favorite = fields['A24'];
      });
    }).catchError((err) {
      print('Error: $err');
    });
  }

  int activeStep = 0;
  String audioA24_1 =
      'https://firebasestorage.googleapis.com/v0/b/maquetadatos-5dee1.appspot.com/o/audios%2FAL_Pr%C3%A1ctica%20de%20mindfulness.m4a?alt=media&token=e54ab0a0-c10d-40e0-8214-d0bdd9e9e612';
  @override
  void initState() {
    super.initState();
    getActivePageStored();
    getFavData();
  }

  @override
  void dispose() {
    super.dispose();
  }

  bool active1 = false;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        active1 = true;
        break;

      default:
    }
    return PopScope(
      canPop: false,
      onPopInvokedWithResult: (bool didPop, result) {
        if (didPop) {
          FirestoreService()
              .updateCurrentPage('A24', (activeStep + 1).toString());
          return;
        }
      },
      child: loadingData
          ? Container(
              width: size.width,
              height: size.height,
              color: CustomColors.primaryDark,
              child: const Center(
                  child: CircularProgressIndicator(
                color: CustomColors.tertiary70,
              )))
          : Container(
              color: Colors.black,
              child: SafeArea(
                child: Scaffold(
                  backgroundColor: CustomColors.background,
                  body: Center(
                    child: Column(
                      children: [
                        content(),
                        Container(
                          margin: const EdgeInsets.only(top: 10, bottom: 10),
                          child: bottomStructure(),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
    );
  }

  Widget topStructure() {
    return Column(
      children: [
        Row(
          children: [
            Container(
              alignment: Alignment.topRight,
              child: IconButton(
                onPressed: () {
                  FirestoreService()
                      .updateCurrentPage('A24', (activeStep + 1).toString())
                      .then((value) => Navigator.of(context).pop());
                },
                icon: const Icon(
                  Icons.close,
                  color: CustomColors.primaryDark,
                ),
              ),
            ),
            const Spacer(),
            favorito(),
          ],
        ),
        Text(
          headerText(),
          textScaler: const TextScaler.linear(1.0),
          textAlign: TextAlign.center,
          style: const TextStyle(
            color: CustomColors.primaryDark,
            fontSize: 24,
          ),
        ),
        Container(
          margin: const EdgeInsets.only(top: 20, bottom: 20),
          child: header(),
        ),
      ],
    );
  }

  Widget favorito() {
    return StreamBuilder<QuerySnapshot>(
        stream: FirestoreService().favoritos(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasData) {
            return SizedBox(
              width: 50,
              height: 50,
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (context, index) {
                    var document = snapshot.data?.docs[index];
                    if (document != null && document.id == uid) {
                      switch (document['A24']) {
                        case true:
                          return IconButton(
                              icon: Icon(MdiIcons.star,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('A24', false)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Eliminado de favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                        case false:
                          return IconButton(
                              icon: Icon(MdiIcons.starOutline,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('A24', true)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Agregado a favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                      }
                    } else {
                      return Container();
                    }
                    return null;
                  }),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(
                color: Colors.red,
              ),
            );
          }
        });
  }

  Widget bottomStructure() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        nextButton(),
      ],
    );
  }

  Widget nextButton() {
    Size size = MediaQuery.of(context).size;

    return SizedBox(
      width: size.width / 1.2,
      child: ElevatedButton(
        style: ButtonStyle(
          backgroundColor: WidgetStateProperty.all<Color>(CustomColors.primary),
          shape: WidgetStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(18.0),
            ),
          ),
        ),
        onPressed: () {
          FirestoreService().updateCurrentPage('A24', '1');

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute<void>(
                  builder: (BuildContext context) => const MainPage(
                        activePage: 1,
                      )),
              (route) => false);
        },
        child: Text(
          buttonText(),
          textScaler: const TextScaler.linear(1.0),
          style: const TextStyle(color: CustomColors.surface),
        ),
      ),
    );
  }

  Widget header() {
    Size size = MediaQuery.of(context).size;
    switch (activeStep) {
      default:
        return Image(
          width: size.width / 3,
          height: size.width / 3,
          fit: BoxFit.scaleDown,
          image: AssetImage(imageHeader()),
        );
    }
  }

  String imageHeader() {
    switch (activeStep) {
      case 0:
        return 'assets/images/practica-mindful-icon.png';

      default:
        return ' ';
    }
  }

  String headerText() {
    switch (activeStep) {
      case 0:
        return 'Práctica de mindfulness';

      default:
        return ' ';
    }
  }

  String buttonText() {
    switch (activeStep) {
      case 0:
        return 'Finalizar';

      default:
        return ' ';
    }
  }

  Widget content() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    width: kIsWeb ? size.width / 2 : size.width,
                    height: size.height / 12,
                    alignment: Alignment.center,
                    margin: const EdgeInsets.all(15),
                    decoration: BoxDecoration(
                      color: CustomColors.primary70,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: AudioPlayerURL(
                      url: audioA24_1,
                      isModule: false,
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 5, right: 20, left: 20, bottom: 5),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Con esta práctica podrás guiar tu atención a distintas partes de tu cuerpo, tomado conciencia del momento presente y distanciándote de pensamientos o emociones que pueden estar afectando tu bienestar.\n\n',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text: 'La práctica se divide en los siguientes:\n',
                          ),
                          TextSpan(
                            text:
                                '1. Siéntate en un lugar cómodo, apoyando tus pies en el suelo y dejando tus manos sobre tus piernas. Tu espalda debe permanecer recta pero no rígida y tus extremidades permanecerán relajadas. Si lo deseas puedes cerrar tus ojos.\n\n',
                          ),
                          TextSpan(
                            text:
                                '2. Presta atención a tus pies, sintiendo cualquier sensación entre la planta del pie y el suelo. Presta atención a todas las partes que están en contacto con el suelo y explora cualquier sensación de cosquilleo, de calor o de frío. Expande tu atención de forma progresiva a tu talón, tus pantorrillas y tu canillas ¿cómo se sienten?.\n\n',
                          ),
                          TextSpan(
                            text:
                                '3. Ahora lleva tu atención tus rodillas ¿Cómo se sienten las rodillas desde adentro?, ¿Cómo tocan la ropa?, ¿Hay diferencia entre la rodilla izquierda y la derecha? Trata de prestar atención a cualquier sensación que surja.\n\n',
                          ),
                          TextSpan(
                            text:
                                '4. Presta atención al peso de tu cuerpo sobre la silla. Siente la presión que ejerce tu cuerpo ¿Puedes decir con claridad dónde empieza y termina el contacto de tu cuerpo con la silla?.\n\n',
                          ),
                          TextSpan(
                            text:
                                '5. Ahora recorre tu espalda, luego tu estómago y tu pecho. ¿Cuáles son las sensaciones que tienes al respirar? Siente la expansión y contracción de tu pecho, estómago y costillas mientras el aire entra y sale.\n\n',
                          ),
                          TextSpan(
                            text:
                                '6. Por último, presta atención a tu nariz, observa cómo entra el aire, su temperatura ¿Puedes sentir la diferencia en cómo entra y cómo sale el aire de tu cuerpo? Al finalizar, puedes abrir los ojos.\n\n',
                          ),
                          TextSpan(
                            text:
                                'Te invitamos a que practiques estar en contacto con tu respiración, a prestar atención a todas las sensaciones que surjan en tu cuerpo, sin juicios.\n',
                          ),
                          TextSpan(
                              text:
                                  'Recuerda que no hay práctica buena o práctica mala, simplemente presta atención a tu respiración y si pierdes la atención vuelve a intentarlo con calma.',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );

      default:
        return Container();
    }
  }
}
