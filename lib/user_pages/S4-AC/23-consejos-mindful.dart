// ignore_for_file: file_names, avoid_print

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:seguimiento/colors.dart';
import 'package:seguimiento/user_pages/MainPage.dart';
import '../../firebase.dart';

class A23 extends StatefulWidget {
  const A23({
    Key? key,
  }) : super(key: key);

  @override
  State<A23> createState() => _A23State();
}

class _A23State extends State<A23> {
  String uid = FirebaseAuth.instance.currentUser!.uid;
  bool loadingData = true;
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('page-control');
  String activeStepData = '';
  void getActivePageStored() {
    collectionReference.doc(uid).get().then((value) {
      var fields = value;
      activeStepData = fields['A23'];
      setState(() {
        if (int.parse(activeStepData) < 6) {
          activeStep = int.parse(activeStepData) - 1;
          Future.delayed(const Duration(milliseconds: 500), () {
            setState(() {
              loadingData = false;
            });
          });
        }
      });
    });
  }

  bool favorite = false;
  CollectionReference favoriteData =
      FirebaseFirestore.instance.collection('favoritos');
  void getFavData() {
    favoriteData.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        favorite = fields['A23'];
      });
    }).catchError((err) {
      print('Error: $err');
    });
  }

  ScrollController scrollController = ScrollController();

  int activeStep = 0;
  @override
  void initState() {
    super.initState();

    getActivePageStored();

    getFavData();
  }

  @override
  void dispose() {
    super.dispose();
  }

  bool active1 = false;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        active1 = true;

        break;

      default:
    }
    return PopScope(
      canPop: false,
      onPopInvokedWithResult: (bool didPop, result) {
        if (didPop) {
          FirestoreService()
              .updateCurrentPage('A23', (activeStep + 1).toString());
          return;
        }
      },
      child: loadingData
          ? Container(
              width: size.width,
              height: size.height,
              color: CustomColors.primaryDark,
              child: const Center(
                  child: CircularProgressIndicator(
                color: CustomColors.tertiary70,
              )))
          : Container(
              color: Colors.black,
              child: SafeArea(
                child: Scaffold(
                  backgroundColor: CustomColors.background,
                  body: Center(
                    child: Column(
                      children: [
                        content(),
                        Container(
                          margin: const EdgeInsets.only(top: 10, bottom: 10),
                          child: bottomStructure(),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
    );
  }

  Widget topStructure() {
    return Column(
      children: [
        Row(
          children: [
            Container(
              alignment: Alignment.topRight,
              child: IconButton(
                onPressed: () {
                  FirestoreService()
                      .updateCurrentPage('A23', (activeStep + 1).toString())
                      .then((value) => Navigator.of(context).pop());
                },
                icon: const Icon(
                  Icons.close,
                  color: CustomColors.primaryDark,
                ),
              ),
            ),
            const Spacer(),
            favorito(),
          ],
        ),
        Text(
          headerText(),
          textScaler: const TextScaler.linear(1.0),
          textAlign: TextAlign.center,
          style: const TextStyle(
            color: CustomColors.primaryDark,
            fontSize: 24,
          ),
        ),
        Container(
          margin: const EdgeInsets.only(top: 20, bottom: 20),
          child: header(),
        ),
      ],
    );
  }

  Widget favorito() {
    return StreamBuilder<QuerySnapshot>(
        stream: FirestoreService().favoritos(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasData) {
            return SizedBox(
              width: 50,
              height: 50,
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (context, index) {
                    var document = snapshot.data?.docs[index];
                    if (document != null && document.id == uid) {
                      switch (document['A23']) {
                        case true:
                          return IconButton(
                              icon: Icon(MdiIcons.star,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('A23', false)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Eliminado de favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                        case false:
                          return IconButton(
                              icon: Icon(MdiIcons.starOutline,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('A23', true)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Agregado a favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                      }
                    } else {
                      return Container();
                    }
                    return null;
                  }),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(
                color: Colors.red,
              ),
            );
          }
        });
  }

  Widget bottomStructure() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        nextButton(),
      ],
    );
  }

  Widget nextButton() {
    Size size = MediaQuery.of(context).size;

    return SizedBox(
      width: size.width / 1.2,
      child: ElevatedButton(
        style: ButtonStyle(
          backgroundColor: WidgetStateProperty.all<Color>(CustomColors.primary),
          shape: WidgetStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(18.0),
            ),
          ),
        ),
        onPressed: () {
          FirestoreService().updateCurrentPage('A23', '1');

          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute<void>(
                  builder: (BuildContext context) => const MainPage(
                        activePage: 1,
                      )),
              (route) => false);
        },
        child: Text(
          buttonText(),
          textScaler: const TextScaler.linear(1.0),
          style: const TextStyle(color: CustomColors.surface),
        ),
      ),
    );
  }

  Widget header() {
    Size size = MediaQuery.of(context).size;
    switch (activeStep) {
      default:
        return Image(
          width: size.width / 3,
          height: size.width / 3,
          fit: BoxFit.scaleDown,
          image: AssetImage(imageHeader()),
        );
    }
  }

  String imageHeader() {
    switch (activeStep) {
      case 0:
        return 'assets/images/consejo-mindful-icon.png';

      default:
        return 'assets/images/consejo-mindful-icon.png';
    }
  }

  String headerText() {
    switch (activeStep) {
      case 0:
        return 'Consejos de mindfulness';

      default:
        return 'Consejos de mindfulness';
    }
  }

  String buttonText() {
    switch (activeStep) {
      case 1:
        return 'Finalizar';

      default:
        return 'Finalizar';
    }
  }

  Widget content() {
    switch (activeStep) {
      case 0:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 20, right: 20, left: 20, bottom: 30),
                    child: RichText(
                      text: const TextSpan(
                        text: '',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                'Mindfulness se define como una forma de conciencia no evaluativa, centrada en el presente, ',
                          ),
                          TextSpan(
                            text:
                                'en la que cada pensamiento, sentimiento o sensación que surge en el campo atencional es reconocida y aceptada tal como es, sin juicios de valor.\n\n',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Actualmente, la evidencia muestra que el mindfulness es una práctica que puede ayudarnos a promover nuestro bienestar y afrontar de mejor manera la ansiedad.\n\n',
                          ),
                          TextSpan(
                            text:
                                'Para realizar una práctica de meditación mindfulness, te recomendamos seguir los siguientes consejos:\n\n',
                          ),
                          TextSpan(
                              text: '   ·  ',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(
                            text:
                                'Define un momento tranquilo del día en que quieras practicar.\n',
                          ),
                          TextSpan(
                              text: '   ·  ',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(
                            text:
                                'Busca un lugar silencioso en tu casa -que dentro de lo posible- esté libre de distracciones.\n',
                          ),
                          TextSpan(
                              text: '   ·  ',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(
                            text:
                                'Usa ropa cómoda, siéntate en una silla o en el suelo, mantén la espalda recta y el cuerpo relajado.\n',
                          ),
                          TextSpan(
                              text: '   ·  ',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(
                            text:
                                'Una vez que sientas comodidad, cierra los ojos y céntrate en lo que sucede con tu cuerpo en el momento presente.\n',
                          ),
                          TextSpan(
                              text: '   ·  ',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(
                            text:
                                'Lleva tu atención a la respiración: inspira y exhala de forma sueva por la nariz.\n',
                          ),
                          TextSpan(
                              text: '   ·  ',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(
                            text:
                                'Si te distraes, no te preocupes, simplemente vuelve a traer tu atención a la respiración.\n',
                          ),
                          TextSpan(
                              text: '   ·  ',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(
                            text:
                                'Acepta las emociones y pensamientos que se te vengan a la mente. No les juzgues ni clasifiques como bueno o malo, positivo o negativo.\n',
                          ),
                          TextSpan(
                              text: '   ·  ',
                              style: TextStyle(fontWeight: FontWeight.bold)),
                          TextSpan(
                            text:
                                'Trata de practicar diariamente entre 5 y 15 minutos cada vez.',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );

      default:
        return Container();
    }
  }
}
