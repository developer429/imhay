// ignore_for_file: file_names, avoid_print, non_constant_identifier_names, unused_local_variable, avoid_returning_null_for_void

import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:seguimiento/colors.dart';
import 'package:seguimiento/user_pages/S1-AC/2-org-tiempo-estudio.dart';
import 'package:seguimiento/user_pages/M1/m1a1.dart';
import 'package:seguimiento/user_pages/M1/m1a2.dart';
import 'package:seguimiento/user_pages/M1/m1a3.dart';
import 'package:seguimiento/user_pages/M1/m1a4.dart';
import 'package:seguimiento/user_pages/M2/m2a1.dart';
import 'package:seguimiento/user_pages/M2/m2a2.dart';
import 'package:seguimiento/user_pages/M2/m2a3.dart';
import 'package:seguimiento/user_pages/M2/m2a4.dart';
import 'package:seguimiento/user_pages/M2/m2a5.dart';
import 'package:seguimiento/user_pages/M2/m2a6.dart';
import 'package:seguimiento/user_pages/M3/m3a1.dart';
import 'package:seguimiento/user_pages/M3/m3a2.dart';
import 'package:seguimiento/user_pages/M3/m3a3.dart';
import 'package:seguimiento/user_pages/M3/m3a4.dart';
import 'package:seguimiento/user_pages/M3/m3a5.dart';
import 'package:seguimiento/user_pages/M3/m3a6.dart';
import 'package:seguimiento/user_pages/M3/m3a8.dart';
import 'package:seguimiento/user_pages/M4/m4a1.dart';
import 'package:seguimiento/user_pages/M4/m4a2.dart';
import 'package:seguimiento/user_pages/M4/m4a3.dart';
import 'package:seguimiento/user_pages/M4/m4a4.dart';
import 'package:seguimiento/user_pages/M4/m4a6.dart';
import 'package:seguimiento/user_pages/S1-AC/3-superar-procastinacion.dart';
import 'package:seguimiento/user_pages/S1-AC/5-amistades-vida.dart';
import 'package:seguimiento/user_pages/S1-AC/6-resolviendo-conflictos.dart';
import 'package:seguimiento/user_pages/S1-AC/7-trabajos-grupo.dart';
import 'package:seguimiento/user_pages/S2-AC/10-resolviendo-problemas.dart';
import 'package:seguimiento/user_pages/S2-AC/11-superando-sobrecarga.dart';
import 'package:seguimiento/user_pages/S2-AC/12-cambiando-forma.dart';
import 'package:seguimiento/user_pages/S2-AC/8-buen-dormir.dart';
import 'package:seguimiento/user_pages/S2-AC/9-habitos-saludables.dart';
import 'package:seguimiento/user_pages/S3-AC/13-agua-rio.dart';
import 'package:seguimiento/user_pages/S3-AC/14-bosque-lluvioso.dart';
import 'package:seguimiento/user_pages/S3-AC/15-respiracion-dormir.dart';
import 'package:seguimiento/user_pages/S3-AC/16-relajacion-respiracion.dart';
import 'package:seguimiento/user_pages/S3-AC/17-selva-tropical.dart';
import 'package:seguimiento/user_pages/S3-AC/18-viento.dart';
import 'package:seguimiento/user_pages/S3-AC/19-noche-verano.dart';
import 'package:seguimiento/user_pages/S3-AC/20-olas-suaves.dart';
import 'package:seguimiento/user_pages/S3-AC/21-lluvia-tranquila.dart';
import 'package:seguimiento/user_pages/S3-AC/22-respiracion-diafragma.dart';
import 'package:seguimiento/user_pages/S4-AC/23-consejos-mindful.dart';
import 'package:seguimiento/user_pages/S4-AC/24-practica-mindful.dart';
import 'package:seguimiento/user_pages/S4-AC/25-practica-aware.dart';
import 'package:seguimiento/user_pages/S5-AC/26-busqueda-ayuda.dart';
import 'package:seguimiento/user_pages/S5-AC/27-prevencion-suicidio.dart';
import 'package:seguimiento/user_pages/S5-AC/28-hablemos-bullying.dart';
import 'package:seguimiento/user_pages/S5-AC/29-hablemos-ciberbullying.dart';
import 'package:seguimiento/user_pages/S5-AC/30-manejo-crisis.dart';
import 'package:seguimiento/user_pages/S5-AC/31-consumo-universitario.dart';
import 'package:seguimiento/user_pages/chatear.dart';
import 'package:seguimiento/user_pages/perfil.dart';
import 'package:seguimiento/user_pages/busca-ayuda.dart';
import 'package:seguimiento/user_pages/programa-final.dart';
import 'package:seguimiento/user_pages/termometro.dart';
import '../chatlogics/chat_page.dart';
import '../chatlogics/models_and_others/firestore_constants.dart';
import '../chatlogics/models_and_others/chat_home_provider.dart';
import '../psicologo_pages/iniciopsi.dart';
import 'S1-AC/1-balance-vida.dart';
import 'M1/m1a0.dart';
import 'M1/m1a5.dart';
import 'M3/m3a7.dart';
import 'M4/m4a5.dart';
import 'S1-AC/4-afrontar-estres.dart';

FirebaseAuth firebaseAuth = FirebaseAuth.instance;
Future _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  print("Handling a background message: ${message.messageId}");
}

class MainPage extends StatefulWidget {
  const MainPage({
    Key? key,
    required this.activePage,
  }) : super(key: key);
  final int activePage;

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  late ChatPageProvider chatHomeProvider;
  int _selectedIndex = 0;
  PushNotification? _notificationInfo;
  late final FirebaseMessaging _messaging;

  void registerNotification() async {
    await Firebase.initializeApp();

    _messaging = FirebaseMessaging.instance;

    FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

    NotificationSettings settings = await _messaging.requestPermission(
      alert: true,
      badge: true,
      provisional: false,
      sound: true,
    );

    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      // print('User granted permission');

      FirebaseMessaging.onMessage.listen((RemoteMessage message) {
        PushNotification notification = PushNotification(
          title: message.notification?.title,
          body: message.notification?.body,
          dataId: message.data['idE'],
          userType: message.data['userType'],
          dataNombre: message.data['nombre'],
        );

        setState(() {
          _notificationInfo = notification;
        });
      });
    } else {
      //print('User declined permission');
    }
  }

  checkForInitialMessage() async {
    await Firebase.initializeApp();
    RemoteMessage? initialMessage =
        await FirebaseMessaging.instance.getInitialMessage();

    if (initialMessage != null) {
      PushNotification notification = PushNotification(
        title: initialMessage.notification?.title,
        body: initialMessage.notification?.body,
        dataId: initialMessage.data['idE'],
        userType: initialMessage.data['userType'],
        dataNombre: initialMessage.data['nombre'],
      );
      setState(() {
        _notificationInfo = notification;
        bool userType = true;
        //El userType de .userType que hacemos extraemos es el user type del emisor.
        if (_notificationInfo!.userType == "1") {
          userType = false;
        }
      });
    }
  }

  String title = 'Tu programa';

  bool programaActivo = true;
  bool actividadesActivo = false;
  bool ayudaActivo = false;
  bool termoActivo = false;
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
      if (_selectedIndex == 0) {
        title = 'Tu programa';
        isVisible = true;
        labelVisible = true;
        programaActivo = true;
        actividadesActivo = false;
        ayudaActivo = false;
        termoActivo = false;
      }

      if (_selectedIndex == 1) {
        title = 'Actividades';
        isVisible = true;
        labelVisible = true;
        programaActivo = false;
        actividadesActivo = true;
        ayudaActivo = false;
        termoActivo = false;
      }
      if (_selectedIndex == 2) {
        title = 'Busca ayuda';
        isVisible = false;
        labelVisible = false;
        programaActivo = false;
        actividadesActivo = false;
        ayudaActivo = true;
        termoActivo = false;
      }
      if (_selectedIndex == 3) {
        title = 'Termómetro';
        isVisible = false;
        labelVisible = false;
        programaActivo = false;
        actividadesActivo = false;
        ayudaActivo = false;
        termoActivo = true;
      }
    });
  }

  bool desbloqueaM2 = false;
  bool desbloqueaM3 = false;
  bool desbloqueaM4 = false;
  String M2Restante = '';
  String M3Restante = '';
  String M4Restante = '';

  String startDate = '';
  CollectionReference dateReference =
      FirebaseFirestore.instance.collection('usuarios');
  void getFechaInicio() {
    dateReference.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        startDate = fields['fechaInicio'];
        String fechaActual = DateFormat('yyy-MM-dd').format(DateTime.now());
        int diferencia = DateTime.parse(fechaActual)
            .difference(DateTime.parse(startDate))
            .inDays;

        M2Restante = (7 - diferencia).toString();
        M3Restante = (14 - diferencia).toString();
        M4Restante = (21 - diferencia).toString();

        if (diferencia >= 7) {
          desbloqueaM2 = true;
        }
        if (diferencia >= 14) {
          desbloqueaM3 = true;
        }
        if (diferencia >= 21) {
          desbloqueaM4 = true;
        }
      });
    }).catchError((err) {
      //print('Error: $err');
    });
  }

  bool isVisible = true;
  bool labelVisible = true;
  @override
  void initState() {
    super.initState();
    /*
    controller.addListener(() {
      if (controller.position.userScrollDirection == ScrollDirection.reverse) {
        setState(() {
          isVisible = false;
        });
      }
      if (controller.position.userScrollDirection == ScrollDirection.forward) {
        setState(() {
          isVisible = false;
        });
      }
      if (controller.position.atEdge) {
        setState(() {
          isVisible = true;
        });
      }
    });
    */
    getFechaInicio();
    registerNotification();
    checkForInitialMessage();

    _selectedIndex = widget.activePage;
    if (_selectedIndex == 0) {
      title = 'Tu programa';
      isVisible = true;
      labelVisible = true;
      programaActivo = true;
      actividadesActivo = false;
      ayudaActivo = false;
      termoActivo = false;
    }

    if (_selectedIndex == 1) {
      title = 'Actividades';
      isVisible = true;
      labelVisible = true;
      programaActivo = false;
      actividadesActivo = true;
      ayudaActivo = false;
      termoActivo = false;
    }
    if (_selectedIndex == 2) {
      title = 'Busca ayuda';
      isVisible = false;
      labelVisible = false;
      programaActivo = false;
      actividadesActivo = false;
      ayudaActivo = true;
      termoActivo = false;
    }
    if (_selectedIndex == 3) {
      title = 'Termómetro';
      isVisible = false;
      labelVisible = false;
      programaActivo = false;
      actividadesActivo = false;
      ayudaActivo = false;
      termoActivo = true;
    }

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      PushNotification notification = PushNotification(
          title: message.notification?.title,
          body: message.notification?.body,
          dataId: message.data['idE'],
          userType: message.data['userType'],
          dataNombre: message.data['nombre']);

      setState(() {
        final _notificationInfo = notification;

        bool userType = true;
        //El userType de .userType que hacemos extraemos es el user type del emisor.
        if (_notificationInfo.userType == "1") {
          userType = false;
        }
        if (_notificationInfo.dataNombre != null) {
          // print("No es null, userType es: " + _notificationInfo.userType!);

          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => ChatPage(
                isVistaDetails: false,
                isPsicologo: false,
                arguments: ChatPageArguments(
                  peerId: _notificationInfo.dataId!,
                  peerNickname: _notificationInfo.dataNombre!,
                ),
              ),
            ),
          );
        } else {
          //print('Es null');
          // print(_notificationInfo.body);
        }
      });
    });

    chatHomeProvider = context.read<ChatPageProvider>();
    //getActivitiesData();
  }

  @override
  void dispose() {
    //controller.removeListener(() {});
    super.dispose();
  }

  String uid = FirebaseAuth.instance.currentUser!.uid;

  ScrollController controllerPrograma =
      ScrollController(initialScrollOffset: 0.0, keepScrollOffset: false);
  ScrollController controllerActividades =
      ScrollController(initialScrollOffset: 0.0, keepScrollOffset: false);

  bool M1A1done = false;
  bool M1A2done = false;
  bool M1A3done = false;
  bool M1A4done = false;
  bool M1A5done = false;
  bool M1A0done = false;
  bool M1ATdone = false;

  bool M2A1done = false;
  bool M2A2done = false;
  bool M2A3done = false;
  bool M2A4done = false;
  bool M2A5done = false;
  bool M2A6done = false;
  bool M2ATdone = false;

  bool M3A1done = false;
  bool M3A2done = false;
  bool M3A3done = false;
  bool M3A4done = false;
  bool M3A5done = false;
  bool M3A6done = false;
  bool M3A7done = false;
  bool M3A8done = false;
  bool M3ATdone = false;

  bool M4A1done = false;
  bool M4A2done = false;
  bool M4A3done = false;
  bool M4A4done = false;
  bool M4A5done = false;
  bool M4A6done = false;
  bool M4ATdone = false;

  @override
  Widget build(BuildContext context) {
    List<Widget> _widgetOptions = <Widget>[
      tuPrograma(),
      actividades(),
      buscaAyuda(),
      termometro(),
    ];
    Color activePageBG() {
      switch (_selectedIndex) {
        case 0:
          return CustomColors.purplePrograma;
        case 1:
          return CustomColors.purplePrograma;

        default:
          return CustomColors.purplePrograma;
      }
    }

    return Container(
      color: Colors.black,
      child: SafeArea(
        child: Scaffold(
          floatingActionButton: Visibility(
            visible: isVisible,
            child: FloatingActionButton.extended(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const ChateaPsicologo(),
                  ),
                );
              },
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              backgroundColor: CustomColors.background,
              label: Visibility(
                visible: labelVisible,
                child: const Text(
                  'Chat',
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: CustomColors.primary),
                ),
              ),
              icon: Container(
                margin: const EdgeInsets.only(left: 10),
                child: Icon(
                  MdiIcons.messageOutline,
                  color: CustomColors.primary,
                ),
              ),
            ),
          ),
          bottomNavigationBar: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            showUnselectedLabels: true,
            backgroundColor: CustomColors.surface,
            elevation: 1,
            items: <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Container(
                  padding: const EdgeInsets.all(2),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(25),
                      color: programaActivo
                          ? CustomColors.primary2.withAlpha(200)
                          : Colors.transparent),
                  child: Icon(MdiIcons.formatListBulletedType),
                ),
                label: 'Tu programa',
              ),
              BottomNavigationBarItem(
                icon: Container(
                  padding: const EdgeInsets.all(2),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(25),
                      color: actividadesActivo
                          ? CustomColors.primary2.withAlpha(200)
                          : Colors.transparent),
                  child: Icon(MdiIcons.vectorSquare),
                ),
                label: 'Actividades',
              ),
              BottomNavigationBarItem(
                icon: Container(
                  padding: const EdgeInsets.all(2),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(25),
                      color: ayudaActivo
                          ? CustomColors.primary2.withAlpha(200)
                          : Colors.transparent),
                  child: Icon(MdiIcons.alertCircleOutline),
                ),
                label: 'Busca ayuda',
              ),
              BottomNavigationBarItem(
                icon: Container(
                  padding: const EdgeInsets.all(2),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(25),
                      color: termoActivo
                          ? CustomColors.primary2.withAlpha(200)
                          : Colors.transparent),
                  child: Icon(MdiIcons.thermometerLow),
                ),
                label: 'Termómetro',
              ),
            ],
            currentIndex: _selectedIndex,
            unselectedItemColor: const Color.fromARGB(255, 135, 109, 205),
            selectedItemColor: CustomColors.primaryDark,
            onTap: _onItemTapped,
          ),
          backgroundColor: activePageBG(),
          body: Center(child: _widgetOptions.elementAt(_selectedIndex)),
        ),
      ),
    );
  }

  Widget tuPrograma() {
    Size size = MediaQuery.of(context).size;

    return Column(
      children: [
        Expanded(
          key: const ValueKey('programa'),
          child: StreamBuilder<QuerySnapshot>(
            stream: chatHomeProvider.getUsersChat(
              FirestoreConstants.pathControlCollection,
            ),
            builder:
                (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
              if (snapshot.hasData) {
                if ((snapshot.data?.docs.length ?? 0) > 0) {
                  return ListView.builder(
                    itemCount: snapshot.data?.docs.length,
                    itemBuilder: (context, index) {
                      if (snapshot.hasData) {
                        if (uid == '') {
                          return Container();
                        } else {
                          if (snapshot.data!.docs[index].id == uid) {
                            M1A0done =
                                snapshot.data!.docs[index]['terminaM1A0'];
                            M1A1done =
                                snapshot.data!.docs[index]['terminaM1A1'];
                            M1A2done =
                                snapshot.data!.docs[index]['terminaM1A2'];
                            M1A3done =
                                snapshot.data!.docs[index]['terminaM1A3'];
                            M1A4done =
                                snapshot.data!.docs[index]['terminaM1A4'];
                            M1A5done =
                                snapshot.data!.docs[index]['terminaM1A5'];
                            M1ATdone =
                                snapshot.data!.docs[index]['terminaM1AT'];

                            M2A1done =
                                snapshot.data!.docs[index]['terminaM2A1'];
                            M2A2done =
                                snapshot.data!.docs[index]['terminaM2A2'];
                            M2A3done =
                                snapshot.data!.docs[index]['terminaM2A3'];
                            M2A4done =
                                snapshot.data!.docs[index]['terminaM2A4'];
                            M2A5done =
                                snapshot.data!.docs[index]['terminaM2A5'];
                            M2A6done =
                                snapshot.data!.docs[index]['terminaM2A6'];
                            M2ATdone =
                                snapshot.data!.docs[index]['terminaM2AT'];
                            M3A1done =
                                snapshot.data!.docs[index]['terminaM3A1'];
                            M3A2done =
                                snapshot.data!.docs[index]['terminaM3A2'];
                            M3A3done =
                                snapshot.data!.docs[index]['terminaM3A3'];
                            M3A4done =
                                snapshot.data!.docs[index]['terminaM3A4'];
                            M3A5done =
                                snapshot.data!.docs[index]['terminaM3A5'];
                            M3A6done =
                                snapshot.data!.docs[index]['terminaM3A6'];
                            M3A7done =
                                snapshot.data!.docs[index]['terminaM3A7'];
                            M3A8done =
                                snapshot.data!.docs[index]['terminaM3A8'];
                            M3ATdone =
                                snapshot.data!.docs[index]['terminaM3AT'];
                            M4A1done =
                                snapshot.data!.docs[index]['terminaM4A1'];
                            M4A2done =
                                snapshot.data!.docs[index]['terminaM4A2'];
                            M4A3done =
                                snapshot.data!.docs[index]['terminaM4A3'];
                            M4A4done =
                                snapshot.data!.docs[index]['terminaM4A4'];
                            M4A5done =
                                snapshot.data!.docs[index]['terminaM4A5'];
                            M4A6done =
                                snapshot.data!.docs[index]['terminaM4A6'];
                            M4ATdone =
                                snapshot.data!.docs[index]['terminaM4AT'];
                            return Container(
                              margin: const EdgeInsets.only(bottom: 50),
                              child: ListView(
                                shrinkWrap: true,
                                controller: controllerPrograma,
                                children: [
                                  Container(
                                    alignment: Alignment.topCenter,
                                    child: Stack(
                                      alignment: Alignment.topRight,
                                      children: [
                                        const Image(
                                          image: AssetImage(
                                              'assets/images/programa-banner.png'),
                                        ),
                                        IconButton(
                                          onPressed: () {
                                            Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                builder: (context) =>
                                                    const TuPerfil(
                                                  typeOfUser: 'U',
                                                ),
                                              ),
                                            );
                                          },
                                          icon: Icon(
                                            MdiIcons.accountCircleOutline,
                                            color: Colors.black,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    margin: const EdgeInsets.only(
                                        top: 15, left: 15, bottom: 5),
                                    child: RichText(
                                      text: const TextSpan(
                                        text: 'Nivel 1',
                                        style: TextStyle(
                                          fontSize: 22,
                                          color: CustomColors.surface,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    alignment: Alignment.center,
                                    child: ListTile(
                                      onTap: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: ((context) =>
                                                const M1A0()),
                                          ),
                                        );
                                      },
                                      leading: const Image(
                                        image: AssetImage(
                                            'assets/images/m1a0-mpi.png'),
                                      ),
                                      subtitle: const Text(
                                        'Vamos juntxs',
                                        textScaler: TextScaler.linear(1.0),
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 2,
                                        style: TextStyle(
                                            color: CustomColors.surface,
                                            fontSize: 18),
                                      ),
                                      title: Container(
                                        margin: const EdgeInsets.only(top: 5),
                                        child: const Text(
                                          'Lectura',
                                          textScaler: TextScaler.linear(1.0),
                                          style: TextStyle(
                                              color: CustomColors.orange3,
                                              fontSize: 14),
                                        ),
                                      ),
                                      trailing: Icon(
                                        M1A0done
                                            ? Icons.check
                                            : MdiIcons.menuRight,
                                        color: M1A0done
                                            ? CustomColors.ticketOk
                                            : CustomColors.surface,
                                        size: size.height / 24,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: const EdgeInsets.only(
                                        left: 10, right: 10),
                                    child: const Divider(
                                      color: Colors.white,
                                      thickness: 0.5,
                                    ),
                                  ),
                                  Container(
                                    alignment: Alignment.center,
                                    child: ListTile(
                                      onTap: () {
                                        if (M1A0done) {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: ((context) =>
                                                  const M1A1()),
                                            ),
                                          );
                                        } else {
                                          return null;
                                        }
                                      },
                                      leading: const Image(
                                        image: AssetImage(
                                            'assets/images/m1a1-mpi.png'),
                                      ),
                                      subtitle: const Text(
                                        '¿Qué es la depresión?',
                                        textScaler: TextScaler.linear(1.0),
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 2,
                                        style: TextStyle(
                                            color: CustomColors.surface,
                                            fontSize: 18),
                                      ),
                                      title: Container(
                                        margin: const EdgeInsets.only(top: 5),
                                        child: const Text(
                                          'Video',
                                          textScaler: TextScaler.linear(1.0),
                                          style: TextStyle(
                                              color: CustomColors.orange3,
                                              fontSize: 14),
                                        ),
                                      ),
                                      trailing: Icon(
                                        M1A0done
                                            ? M1A1done
                                                ? Icons.check
                                                : MdiIcons.menuRight
                                            : MdiIcons.lock,
                                        color: M1A1done
                                            ? CustomColors.ticketOk
                                            : CustomColors.surface,
                                        size: size.height / 24,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: const EdgeInsets.only(
                                        left: 10, right: 10),
                                    child: const Divider(
                                      color: Colors.white,
                                      thickness: 0.5,
                                    ),
                                  ),
                                  Container(
                                    alignment: Alignment.center,
                                    child: ListTile(
                                      onTap: () {
                                        if (M1A1done) {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: ((context) =>
                                                  const M1A2()),
                                            ),
                                          );
                                        } else {
                                          return null;
                                        }
                                      },
                                      leading: const Image(
                                        image: AssetImage(
                                            'assets/images/m1a2-mpi.png'),
                                      ),
                                      subtitle: const Text(
                                        'Mitos sobre la depresión',
                                        textScaler: TextScaler.linear(1.0),
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 2,
                                        style: TextStyle(
                                            color: CustomColors.surface,
                                            fontSize: 18),
                                      ),
                                      title: Container(
                                        margin: const EdgeInsets.only(top: 5),
                                        child: const Text(
                                          'Ejercicio',
                                          textScaler: TextScaler.linear(1.0),
                                          style: TextStyle(
                                              color: CustomColors.orange3,
                                              fontSize: 14),
                                        ),
                                      ),
                                      trailing: Icon(
                                        M1A1done
                                            ? M1A2done
                                                ? Icons.check
                                                : MdiIcons.menuRight
                                            : MdiIcons.lock,
                                        color: M1A2done
                                            ? CustomColors.ticketOk
                                            : CustomColors.surface,
                                        size: size.height / 24,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: const EdgeInsets.only(
                                        left: 10, right: 10),
                                    child: const Divider(
                                      color: Colors.white,
                                      thickness: 0.5,
                                    ),
                                  ),
                                  Container(
                                    alignment: Alignment.center,
                                    child: ListTile(
                                      onTap: () {
                                        if (M1A2done) {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: ((context) =>
                                                  const M1A3()),
                                            ),
                                          );
                                        } else {
                                          return null;
                                        }
                                      },
                                      leading: const Image(
                                        image: AssetImage(
                                            'assets/images/m1a3-mpi.png'),
                                      ),
                                      subtitle: const Text(
                                        '¿Qué es la ansiedad?',
                                        textScaler: TextScaler.linear(1.0),
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 2,
                                        style: TextStyle(
                                            color: CustomColors.surface,
                                            fontSize: 18),
                                      ),
                                      title: Container(
                                        margin: const EdgeInsets.only(top: 5),
                                        child: const Text(
                                          'Video',
                                          textScaler: TextScaler.linear(1.0),
                                          style: TextStyle(
                                              color: CustomColors.orange3,
                                              fontSize: 14),
                                        ),
                                      ),
                                      trailing: Icon(
                                        M1A2done
                                            ? M1A3done
                                                ? Icons.check
                                                : MdiIcons.menuRight
                                            : MdiIcons.lock,
                                        color: M1A3done
                                            ? CustomColors.ticketOk
                                            : CustomColors.surface,
                                        size: size.height / 24,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: const EdgeInsets.only(
                                        left: 10, right: 10),
                                    child: const Divider(
                                      color: Colors.white,
                                      thickness: 0.5,
                                    ),
                                  ),
                                  Container(
                                    alignment: Alignment.center,
                                    child: ListTile(
                                      onTap: () {
                                        if (M1A3done) {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: ((context) =>
                                                  const M1A4()),
                                            ),
                                          );
                                        } else {
                                          return null;
                                        }
                                      },
                                      leading: const Image(
                                        image: AssetImage(
                                            'assets/images/m1a4-mpi.png'),
                                      ),
                                      subtitle: const Text(
                                        '7 Principios para tu activación conductual',
                                        textScaler: TextScaler.linear(1.0),
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 2,
                                        style: TextStyle(
                                            color: CustomColors.surface,
                                            fontSize: 18),
                                      ),
                                      title: Container(
                                        margin: const EdgeInsets.only(top: 5),
                                        child: const Text(
                                          'Lectura',
                                          textScaler: TextScaler.linear(1.0),
                                          style: TextStyle(
                                              color: CustomColors.orange3,
                                              fontSize: 14),
                                        ),
                                      ),
                                      trailing: Icon(
                                        M1A3done
                                            ? M1A4done
                                                ? Icons.check
                                                : MdiIcons.menuRight
                                            : MdiIcons.lock,
                                        color: M1A4done
                                            ? CustomColors.ticketOk
                                            : CustomColors.surface,
                                        size: size.height / 24,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: const EdgeInsets.only(
                                        left: 10, right: 10),
                                    child: const Divider(
                                      color: Colors.white,
                                      thickness: 0.5,
                                    ),
                                  ),
                                  Container(
                                    alignment: Alignment.center,
                                    child: ListTile(
                                      onTap: () {
                                        if (M1A4done) {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: ((context) =>
                                                  const M1A5()),
                                            ),
                                          );
                                        } else {
                                          return null;
                                        }
                                      },
                                      leading: const Image(
                                        image: AssetImage(
                                            'assets/images/m1a5-mpi.png'),
                                      ),
                                      subtitle: const Text(
                                        'Aplicando la Activación Conductual',
                                        textScaler: TextScaler.linear(1.0),
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 2,
                                        style: TextStyle(
                                            color: CustomColors.surface,
                                            fontSize: 18),
                                      ),
                                      title: Container(
                                        margin: const EdgeInsets.only(top: 5),
                                        child: const Text(
                                          'Ejercicio',
                                          textScaler: TextScaler.linear(1.0),
                                          style: TextStyle(
                                              color: CustomColors.orange3,
                                              fontSize: 14),
                                        ),
                                      ),
                                      trailing: Icon(
                                        M1A4done
                                            ? M1A5done
                                                ? Icons.check
                                                : MdiIcons.menuRight
                                            : MdiIcons.lock,
                                        color: M1A5done
                                            ? CustomColors.ticketOk
                                            : CustomColors.surface,
                                        size: size.height / 24,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: const EdgeInsets.only(
                                        left: 10, right: 10),
                                    child: const Divider(
                                      color: Colors.white,
                                      thickness: 0.5,
                                    ),
                                  ),
                                  Container(
                                    alignment: Alignment.center,
                                    child: ListTile(
                                      onTap: () {
                                        if (M1A5done) {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: ((context) =>
                                                  const Termometro(
                                                    modulo: '1',
                                                  )),
                                            ),
                                          );
                                        } else {
                                          return null;
                                        }
                                      },
                                      leading: const Image(
                                        image: AssetImage(
                                            'assets/images/mat-mpi.png'),
                                      ),
                                      subtitle: const Text(
                                        'Termómetro del Ánimo',
                                        textScaler: TextScaler.linear(1.0),
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 2,
                                        style: TextStyle(
                                            color: CustomColors.surface,
                                            fontSize: 18),
                                      ),
                                      title: Container(
                                        margin: const EdgeInsets.only(top: 5),
                                        child: const Text(
                                          'Ejercicio',
                                          textScaler: TextScaler.linear(1.0),
                                          style: TextStyle(
                                              color: CustomColors.orange3,
                                              fontSize: 14),
                                        ),
                                      ),
                                      trailing: Icon(
                                        M1A5done
                                            ? M1ATdone
                                                ? Icons.check
                                                : MdiIcons.menuRight
                                            : MdiIcons.lock,
                                        color: M1ATdone
                                            ? CustomColors.ticketOk
                                            : CustomColors.surface,
                                        size: size.height / 24,
                                      ),
                                    ),
                                  ),
                                  const Divider(
                                    color: Colors.transparent,
                                  ),
                                  desbloqueaM2
                                      ? M2()
                                      : InkWell(
                                          onTap: () {
                                            showDialog(
                                              context: context,
                                              builder: (context) => AlertDialog(
                                                shape:
                                                    const RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius.all(
                                                                Radius.circular(
                                                                    32.0))),
                                                title: const Text(
                                                  '¡Bloqueado! :(',
                                                  textScaler:
                                                      TextScaler.linear(1.0),
                                                  style: TextStyle(
                                                      color: CustomColors
                                                          .primaryDark),
                                                ),
                                                content: Text(
                                                  'El contenido será desbloqueado dentro de ' +
                                                      M2Restante +
                                                      ' días',
                                                  textScaler:
                                                      const TextScaler.linear(
                                                          1.0),
                                                  style: const TextStyle(
                                                      color: CustomColors
                                                          .primaryDark),
                                                ),
                                                actions: [
                                                  TextButton(
                                                    onPressed: () {
                                                      Navigator.of(context)
                                                          .pop();
                                                    },
                                                    child: const Text(
                                                      'Ok',
                                                      textScaler:
                                                          TextScaler.linear(
                                                              1.0),
                                                      style: TextStyle(
                                                          color: CustomColors
                                                              .primary),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            );
                                          },
                                          child: const Image(
                                            image: AssetImage(
                                                'assets/images/lvl2-locked-actividades.png'),
                                          ),
                                        ),
                                  desbloqueaM3
                                      ? M3()
                                      : InkWell(
                                          onTap: () {
                                            showDialog(
                                              context: context,
                                              builder: (context) => AlertDialog(
                                                shape:
                                                    const RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius.all(
                                                                Radius.circular(
                                                                    32.0))),
                                                title: const Text(
                                                  '¡Bloqueado! :(',
                                                  textScaler:
                                                      TextScaler.linear(1.0),
                                                  style: TextStyle(
                                                      color: CustomColors
                                                          .primaryDark),
                                                ),
                                                content: Text(
                                                  'El contenido será desbloqueado dentro de ' +
                                                      M3Restante +
                                                      ' días',
                                                  textScaler:
                                                      const TextScaler.linear(
                                                          1.0),
                                                  style: const TextStyle(
                                                      color: CustomColors
                                                          .primaryDark),
                                                ),
                                                actions: [
                                                  TextButton(
                                                    onPressed: () {
                                                      Navigator.of(context)
                                                          .pop();
                                                    },
                                                    child: const Text(
                                                      'Ok',
                                                      textScaler:
                                                          TextScaler.linear(
                                                              1.0),
                                                      style: TextStyle(
                                                          color: CustomColors
                                                              .primary),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            );
                                          },
                                          child: const Image(
                                            image: AssetImage(
                                                'assets/images/lvl3-locked-actividades.png'),
                                          ),
                                        ),
                                  desbloqueaM4
                                      ? M4()
                                      : InkWell(
                                          onTap: () {
                                            showDialog(
                                              context: context,
                                              builder: (context) => AlertDialog(
                                                shape:
                                                    const RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius.all(
                                                                Radius.circular(
                                                                    32.0))),
                                                title: const Text(
                                                  '¡Bloqueado! :(',
                                                  textScaler:
                                                      TextScaler.linear(1.0),
                                                  style: TextStyle(
                                                      color: CustomColors
                                                          .primaryDark),
                                                ),
                                                content: Text(
                                                  'El contenido será desbloqueado dentro de ' +
                                                      M4Restante +
                                                      ' días',
                                                  textScaler:
                                                      const TextScaler.linear(
                                                          1.0),
                                                  style: const TextStyle(
                                                      color: CustomColors
                                                          .primaryDark),
                                                ),
                                                actions: [
                                                  TextButton(
                                                    onPressed: () {
                                                      Navigator.of(context)
                                                          .pop();
                                                    },
                                                    child: const Text(
                                                      'Ok',
                                                      textScaler:
                                                          TextScaler.linear(
                                                              1.0),
                                                      style: TextStyle(
                                                          color: CustomColors
                                                              .primary),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            );
                                          },
                                          child: const Image(
                                            image: AssetImage(
                                                'assets/images/lvl4-locked-actividades.png'),
                                          ),
                                        ),
                                ],
                              ),
                            );
                          } else {
                            return Container();
                          }
                        }
                      }
                      return null;
                    },
                  );
                } else {
                  return const Center(
                    child: Text(
                      "ERROR",
                      textScaler: TextScaler.linear(1.0),
                    ),
                  );
                }
              } else {
                return const Center(
                  child: CircularProgressIndicator(
                    color: Colors.red,
                  ),
                );
              }
            },
          ),
        ),
      ],
    );
  }

  Widget M2() {
    Size size = MediaQuery.of(context).size;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.only(top: 15, left: 15, bottom: 5),
          child: const Text(
            'Nivel 2',
            textScaler: TextScaler.linear(1.0),
            style: TextStyle(color: Colors.white, fontSize: 22),
          ),
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            onTap: () {
              if (M1ATdone) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: ((context) => const M2A1()),
                  ),
                );
              } else {
                return null;
              }
            },
            leading: const Image(
              image: AssetImage('assets/images/m2a1-mpi.png'),
            ),
            subtitle: const Text(
              'Redes de apoyo',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(color: CustomColors.surface, fontSize: 18),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Video',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            trailing: Icon(
              M1ATdone
                  ? M2A1done
                      ? Icons.check
                      : MdiIcons.menuRight
                  : MdiIcons.lock,
              color: M2A1done ? CustomColors.ticketOk : CustomColors.surface,
              size: size.height / 24,
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(left: 10, right: 10),
          child: const Divider(
            color: Colors.white,
            thickness: 0.5,
          ),
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            onTap: () {
              if (M2A1done) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: ((context) => const M2A2()),
                  ),
                );
              } else {
                return null;
              }
            },
            leading: const Image(
              image: AssetImage('assets/images/m2a2-mpi.png'),
            ),
            subtitle: const Text(
              'El Ecomapa',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(color: CustomColors.surface, fontSize: 18),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Lectura',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            trailing: Icon(
              M2A1done
                  ? M2A2done
                      ? Icons.check
                      : MdiIcons.menuRight
                  : MdiIcons.lock,
              color: M2A2done ? CustomColors.ticketOk : CustomColors.surface,
              size: size.height / 24,
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(left: 10, right: 10),
          child: const Divider(
            color: Colors.white,
            thickness: 0.5,
          ),
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            onTap: () {
              if (M2A2done) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: ((context) => const M2A3()),
                  ),
                );
              } else {
                return null;
              }
            },
            leading: const Image(
              image: AssetImage('assets/images/m2a3-mpi.png'),
            ),
            subtitle: const Text(
              'Tu red de apoyo',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(color: CustomColors.surface, fontSize: 18),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Ejercicio',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            trailing: Icon(
              M2A2done
                  ? M2A3done
                      ? Icons.check
                      : MdiIcons.menuRight
                  : MdiIcons.lock,
              color: M2A3done ? CustomColors.ticketOk : CustomColors.surface,
              size: size.height / 24,
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(left: 10, right: 10),
          child: const Divider(
            color: Colors.white,
            thickness: 0.5,
          ),
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            onTap: () {
              if (M2A3done) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: ((context) => const M2A4()),
                  ),
                );
              } else {
                return null;
              }
            },
            leading: const Image(
              image: AssetImage('assets/images/m2a4-mpi.png'),
            ),
            subtitle: const Text(
              'Regulación emocional',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(color: CustomColors.surface, fontSize: 18),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Lectura',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            trailing: Icon(
              M2A3done
                  ? M2A4done
                      ? Icons.check
                      : MdiIcons.menuRight
                  : MdiIcons.lock,
              color: M2A4done ? CustomColors.ticketOk : CustomColors.surface,
              size: size.height / 24,
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(left: 10, right: 10),
          child: const Divider(
            color: Colors.white,
            thickness: 0.5,
          ),
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            onTap: () {
              if (M2A4done) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: ((context) => const M2A5()),
                  ),
                );
              } else {
                return null;
              }
            },
            leading: const Image(
              image: AssetImage('assets/images/m2a5-mpi.png'),
            ),
            subtitle: const Text(
              '4 estrategias de regulación emocional',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(color: CustomColors.surface, fontSize: 18),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Lectura',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            trailing: Icon(
              M2A4done
                  ? M2A5done
                      ? Icons.check
                      : MdiIcons.menuRight
                  : MdiIcons.lock,
              color: M2A5done ? CustomColors.ticketOk : CustomColors.surface,
              size: size.height / 24,
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(left: 10, right: 10),
          child: const Divider(
            color: Colors.white,
            thickness: 0.5,
          ),
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            onTap: () {
              if (M2A5done) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: ((context) => const M2A6()),
                  ),
                );
              } else {
                return null;
              }
            },
            leading: const Image(
              image: AssetImage('assets/images/m2a6-mpi.png'),
            ),
            subtitle: const Text(
              'Guía de regulación emocional',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(color: CustomColors.surface, fontSize: 18),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Ejercicio',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            trailing: Icon(
              M2A5done
                  ? M2A6done
                      ? Icons.check
                      : MdiIcons.menuRight
                  : MdiIcons.lock,
              color: M2A6done ? CustomColors.ticketOk : CustomColors.surface,
              size: size.height / 24,
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(left: 10, right: 10),
          child: const Divider(
            color: Colors.white,
            thickness: 0.5,
          ),
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            onTap: () {
              if (M2A6done) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: ((context) => const Termometro(
                          modulo: '2',
                        )),
                  ),
                );
              } else {
                return null;
              }
            },
            leading: const Image(
              image: AssetImage('assets/images/mat-mpi.png'),
            ),
            subtitle: const Text(
              'Termómetro del Ánimo',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(color: CustomColors.surface, fontSize: 18),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Ejercicio',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            trailing: Icon(
              M2A6done
                  ? M2ATdone
                      ? Icons.check
                      : MdiIcons.menuRight
                  : MdiIcons.lock,
              color: M2ATdone ? CustomColors.ticketOk : CustomColors.surface,
              size: size.height / 24,
            ),
          ),
        ),
        const Divider(
          color: Colors.transparent,
        ),
      ],
    );
  }

  Widget M3() {
    Size size = MediaQuery.of(context).size;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.only(top: 15, left: 15, bottom: 5),
          child: const Text(
            'Nivel 3',
            textScaler: TextScaler.linear(1.0),
            style: TextStyle(color: Colors.white, fontSize: 22),
          ),
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            onTap: () {
              if (M2ATdone) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: ((context) => const M3A1()),
                  ),
                );
              } else {
                return null;
              }
            },
            leading: const Image(
              image: AssetImage('assets/images/m3a1-mpi.png'),
            ),
            subtitle: const Text(
              '¿Qué son los errores del pensamiento?',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(color: CustomColors.surface, fontSize: 18),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Lectura',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            trailing: Icon(
              M2ATdone
                  ? M3A1done
                      ? Icons.check
                      : MdiIcons.menuRight
                  : MdiIcons.lock,
              color: M3A1done ? CustomColors.ticketOk : CustomColors.surface,
              size: size.height / 24,
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(left: 10, right: 10),
          child: const Divider(
            color: Colors.white,
            thickness: 0.5,
          ),
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            onTap: () {
              if (M3A1done) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: ((context) => const M3A2()),
                  ),
                );
              } else {
                return null;
              }
            },
            leading: const Image(
              image: AssetImage('assets/images/m3a2-mpi.png'),
            ),
            subtitle: const Text(
              'Identificando errores del pensamiento',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(color: CustomColors.surface, fontSize: 18),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Ejercicio',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            trailing: Icon(
              M3A1done
                  ? M3A2done
                      ? Icons.check
                      : MdiIcons.menuRight
                  : MdiIcons.lock,
              color: M3A2done ? CustomColors.ticketOk : CustomColors.surface,
              size: size.height / 24,
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(left: 10, right: 10),
          child: const Divider(
            color: Colors.white,
            thickness: 0.5,
          ),
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            onTap: () {
              if (M3A2done) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: ((context) => const M3A3()),
                  ),
                );
              } else {
                return null;
              }
            },
            leading: const Image(
              image: AssetImage('assets/images/m3a3-mpi.png'),
            ),
            subtitle: const Text(
              '5 Pasos para enfrentar los pensamientos negativos',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(color: CustomColors.surface, fontSize: 18),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Lectura',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            trailing: Icon(
              M3A2done
                  ? M3A3done
                      ? Icons.check
                      : MdiIcons.menuRight
                  : MdiIcons.lock,
              color: M3A3done ? CustomColors.ticketOk : CustomColors.surface,
              size: size.height / 24,
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(left: 10, right: 10),
          child: const Divider(
            color: Colors.white,
            thickness: 0.5,
          ),
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            onTap: () {
              if (M3A3done) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: ((context) => const M3A4()),
                  ),
                );
              } else {
                return null;
              }
            },
            leading: const Image(
              image: AssetImage('assets/images/m3a4-mpi.png'),
            ),
            subtitle: const Text(
              'Resolución de problemas',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(color: CustomColors.surface, fontSize: 18),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Lectura',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            trailing: Icon(
              M3A3done
                  ? M3A4done
                      ? Icons.check
                      : MdiIcons.menuRight
                  : MdiIcons.lock,
              color: M3A4done ? CustomColors.ticketOk : CustomColors.surface,
              size: size.height / 24,
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(left: 10, right: 10),
          child: const Divider(
            color: Colors.white,
            thickness: 0.5,
          ),
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            onTap: () {
              if (M3A4done) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: ((context) => const M3A5()),
                  ),
                );
              } else {
                return null;
              }
            },
            leading: const Image(
              image: AssetImage('assets/images/m3a5-mpi.png'),
            ),
            subtitle: const Text(
              'Definiendo el problema',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(color: CustomColors.surface, fontSize: 18),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Ejercicio',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            trailing: Icon(
              M3A4done
                  ? M3A5done
                      ? Icons.check
                      : MdiIcons.menuRight
                  : MdiIcons.lock,
              color: M3A5done ? CustomColors.ticketOk : CustomColors.surface,
              size: size.height / 24,
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(left: 10, right: 10),
          child: const Divider(
            color: Colors.white,
            thickness: 0.5,
          ),
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            onTap: () {
              if (M3A5done) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: ((context) => const M3A6()),
                  ),
                );
              } else {
                return null;
              }
            },
            leading: const Image(
              image: AssetImage('assets/images/m3a6-mpi.png'),
            ),
            subtitle: const Text(
              'Generando alternativas de solución',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(color: CustomColors.surface, fontSize: 18),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Ejercicio',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            trailing: Icon(
              M3A5done
                  ? M3A6done
                      ? Icons.check
                      : MdiIcons.menuRight
                  : MdiIcons.lock,
              color: M3A6done ? CustomColors.ticketOk : CustomColors.surface,
              size: size.height / 24,
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(left: 10, right: 10),
          child: const Divider(
            color: Colors.white,
            thickness: 0.5,
          ),
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            onTap: () {
              if (M3A6done) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: ((context) => const M3A7()),
                  ),
                );
              } else {
                return null;
              }
            },
            leading: const Image(
              image: AssetImage('assets/images/m3a7-mpi.png'),
            ),
            subtitle: const Text(
              'Tomando decisiones',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(color: CustomColors.surface, fontSize: 18),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Ejercicio',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            trailing: Icon(
              M3A6done
                  ? M3A7done
                      ? Icons.check
                      : MdiIcons.menuRight
                  : MdiIcons.lock,
              color: M3A7done ? CustomColors.ticketOk : CustomColors.surface,
              size: size.height / 24,
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(left: 10, right: 10),
          child: const Divider(
            color: Colors.white,
            thickness: 0.5,
          ),
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            onTap: () {
              if (M3A7done) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: ((context) => const M3A8()),
                  ),
                );
              } else {
                return null;
              }
            },
            leading: const Image(
              image: AssetImage('assets/images/m3a8-mpi.png'),
            ),
            subtitle: const Text(
              'Implementando la solución y evaluando la solución',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(color: CustomColors.surface, fontSize: 18),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Lectura',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            trailing: Icon(
              M3A7done
                  ? M3A8done
                      ? Icons.check
                      : MdiIcons.menuRight
                  : MdiIcons.lock,
              color: M3A8done ? CustomColors.ticketOk : CustomColors.surface,
              size: size.height / 24,
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(left: 10, right: 10),
          child: const Divider(
            color: Colors.white,
            thickness: 0.5,
          ),
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            onTap: () {
              if (M3A8done) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: ((context) => const Termometro(
                          modulo: '3',
                        )),
                  ),
                );
              } else {
                return null;
              }
            },
            leading: const Image(
              image: AssetImage('assets/images/mat-mpi.png'),
            ),
            subtitle: const Text(
              'Termómetro del Ánimo',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(color: CustomColors.surface, fontSize: 18),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Ejercicio',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            trailing: Icon(
              M3A8done
                  ? M3ATdone
                      ? Icons.check
                      : MdiIcons.menuRight
                  : MdiIcons.lock,
              color: M3ATdone ? CustomColors.ticketOk : CustomColors.surface,
              size: size.height / 24,
            ),
          ),
        ),
        const Divider(
          color: Colors.transparent,
        ),
      ],
    );
  }

  Widget M4() {
    Size size = MediaQuery.of(context).size;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: const EdgeInsets.only(top: 15, left: 15, bottom: 5),
          child: const Text(
            'Módulo 4',
            textScaler: TextScaler.linear(1.0),
            style: TextStyle(color: Colors.white, fontSize: 22),
          ),
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            onTap: () {
              if (M3ATdone) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: ((context) => const M4A1()),
                  ),
                );
              } else {
                return null;
              }
            },
            leading: const Image(
              image: AssetImage('assets/images/m4a1-mpi.png'),
            ),
            subtitle: const Text(
              'Exposición y evitación',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(color: CustomColors.surface, fontSize: 18),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Lectura',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            trailing: Icon(
              M3ATdone
                  ? M4A1done
                      ? Icons.check
                      : MdiIcons.menuRight
                  : MdiIcons.lock,
              color: M4A1done ? CustomColors.ticketOk : CustomColors.surface,
              size: size.height / 24,
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(left: 10, right: 10),
          child: const Divider(
            color: Colors.white,
            thickness: 0.5,
          ),
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            onTap: () {
              if (M4A1done) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: ((context) => const M4A2()),
                  ),
                );
              } else {
                return null;
              }
            },
            leading: const Image(
              image: AssetImage('assets/images/m4a2-mpi.png'),
            ),
            subtitle: const Text(
              'Con los ojos en la meta',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(color: CustomColors.surface, fontSize: 18),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Ejercicio',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            trailing: Icon(
              M4A1done
                  ? M4A2done
                      ? Icons.check
                      : MdiIcons.menuRight
                  : MdiIcons.lock,
              color: M4A2done ? CustomColors.ticketOk : CustomColors.surface,
              size: size.height / 24,
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(left: 10, right: 10),
          child: const Divider(
            color: Colors.white,
            thickness: 0.5,
          ),
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            onTap: () {
              if (M4A2done) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: ((context) => const M4A3()),
                  ),
                );
              } else {
                return null;
              }
            },
            leading: const Image(
              image: AssetImage('assets/images/m4a3-mpi.png'),
            ),
            subtitle: const Text(
              '¿Qué es lo que evitas?',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(color: CustomColors.surface, fontSize: 18),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Ejercicio',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            trailing: Icon(
              M4A2done
                  ? M4A3done
                      ? Icons.check
                      : MdiIcons.menuRight
                  : MdiIcons.lock,
              color: M4A3done ? CustomColors.ticketOk : CustomColors.surface,
              size: size.height / 24,
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(left: 10, right: 10),
          child: const Divider(
            color: Colors.white,
            thickness: 0.5,
          ),
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            onTap: () {
              if (M4A3done) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: ((context) => const M4A4()),
                  ),
                );
              } else {
                return null;
              }
            },
            leading: const Image(
              image: AssetImage('assets/images/m4a4-mpi.png'),
            ),
            subtitle: const Text(
              'Creando alternativas',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(color: CustomColors.surface, fontSize: 18),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Ejercicio',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            trailing: Icon(
              M4A3done
                  ? M4A4done
                      ? Icons.check
                      : MdiIcons.menuRight
                  : MdiIcons.lock,
              color: M4A4done ? CustomColors.ticketOk : CustomColors.surface,
              size: size.height / 24,
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(left: 10, right: 10),
          child: const Divider(
            color: Colors.white,
            thickness: 0.5,
          ),
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            onTap: () {
              if (M4A4done) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: ((context) => const M4A5()),
                  ),
                );
              } else {
                return null;
              }
            },
            leading: const Image(
              image: AssetImage('assets/images/m4a5-mpi.png'),
            ),
            subtitle: const Text(
              'Avanzando paso a paso',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(color: CustomColors.surface, fontSize: 18),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Ejercicio',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            trailing: Icon(
              M4A4done
                  ? M4A5done
                      ? Icons.check
                      : MdiIcons.menuRight
                  : MdiIcons.lock,
              color: M4A5done ? CustomColors.ticketOk : CustomColors.surface,
              size: size.height / 24,
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(left: 10, right: 10),
          child: const Divider(
            color: Colors.white,
            thickness: 0.5,
          ),
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            onTap: () {
              if (M4A5done) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: ((context) => const M4A6()),
                  ),
                );
              } else {
                return null;
              }
            },
            leading: const Image(
              image: AssetImage('assets/images/m4a6-mpi.png'),
            ),
            subtitle: const Text(
              'Tu plan de exposición',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(color: CustomColors.surface, fontSize: 18),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Ejercicio',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            trailing: Icon(
              M4A5done
                  ? M4A6done
                      ? Icons.check
                      : MdiIcons.menuRight
                  : MdiIcons.lock,
              color: M4A6done ? CustomColors.ticketOk : CustomColors.surface,
              size: size.height / 24,
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(left: 10, right: 10),
          child: const Divider(
            color: Colors.white,
            thickness: 0.5,
          ),
        ),
        Container(
          alignment: Alignment.center,
          child: ListTile(
            onTap: () {
              if (M4A6done) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: ((context) => const Termometro(
                          modulo: '4',
                        )),
                  ),
                );
              } else {
                return null;
              }
            },
            leading: const Image(
              image: AssetImage('assets/images/mat-mpi.png'),
            ),
            subtitle: const Text(
              'Termómetro del Ánimo',
              textScaler: TextScaler.linear(1.0),
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(color: CustomColors.surface, fontSize: 18),
            ),
            title: Container(
              margin: const EdgeInsets.only(top: 5),
              child: const Text(
                'Ejercicio',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.orange3, fontSize: 14),
              ),
            ),
            trailing: Icon(
              M4A6done
                  ? M4ATdone
                      ? Icons.check
                      : MdiIcons.menuRight
                  : MdiIcons.lock,
              color: M4ATdone ? CustomColors.ticketOk : CustomColors.surface,
              size: size.height / 24,
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(left: 10, right: 10),
          child: const Divider(
            color: Colors.white,
            thickness: 0.5,
          ),
        ),
        Visibility(
          visible: M4ATdone,
          child: Container(
            alignment: Alignment.topCenter,
            padding: const EdgeInsets.only(top: 20),
            height: size.height / 5,
            child: ListTile(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const EndPage()),
                    ));
              },
              leading: const Image(
                image: AssetImage('assets/images/maf-mpi.png'),
              ),
              subtitle: const Text(
                'Has terminado Tu Programa',
                textScaler: TextScaler.linear(1.0),
                overflow: TextOverflow.ellipsis,
                maxLines: 2,
                style: TextStyle(
                    color: CustomColors.surface,
                    fontWeight: FontWeight.bold,
                    fontSize: 18),
              ),
              title: Container(
                margin: const EdgeInsets.only(top: 5),
                child: const Text(
                  '¡Felicidades!',
                  textScaler: TextScaler.linear(1.0),
                  style: TextStyle(
                      color: CustomColors.orange3,
                      fontSize: 14,
                      fontWeight: FontWeight.bold),
                ),
              ),
              trailing: Icon(
                MdiIcons.menuRight,
                color: CustomColors.ticketOk,
                size: size.height / 24,
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget actividades() {
    Size size = MediaQuery.of(context).size;

    return Column(
      children: [
        Expanded(
          key: const ValueKey('actividades'),
          child: StreamBuilder<QuerySnapshot>(
              stream: chatHomeProvider.getUsersChat(
                FirestoreConstants.pathControlCollection,
              ),
              builder: (BuildContext context,
                  AsyncSnapshot<QuerySnapshot> snapshot) {
                if (snapshot.hasData) {
                  if ((snapshot.data?.docs.length ?? 0) > 0) {
                    return ListView.builder(
                      itemCount: snapshot.data?.docs.length,
                      itemBuilder: (context, index) {
                        if ((snapshot.hasData)) {
                          if (uid == '') {
                            return Container();
                          } else {
                            if (snapshot.data!.docs[index].id == uid) {
                              M1A0done =
                                  snapshot.data!.docs[index]['terminaM1A0'];
                              M1A1done =
                                  snapshot.data!.docs[index]['terminaM1A1'];
                              M1A2done =
                                  snapshot.data!.docs[index]['terminaM1A2'];
                              M1A3done =
                                  snapshot.data!.docs[index]['terminaM1A3'];
                              M1A4done =
                                  snapshot.data!.docs[index]['terminaM1A4'];
                              M1A5done =
                                  snapshot.data!.docs[index]['terminaM1A5'];
                              M1ATdone =
                                  snapshot.data!.docs[index]['terminaM1AT'];

                              M2A1done =
                                  snapshot.data!.docs[index]['terminaM2A1'];
                              M2A2done =
                                  snapshot.data!.docs[index]['terminaM2A2'];
                              M2A3done =
                                  snapshot.data!.docs[index]['terminaM2A3'];
                              M2A4done =
                                  snapshot.data!.docs[index]['terminaM2A4'];
                              M2A5done =
                                  snapshot.data!.docs[index]['terminaM2A5'];
                              M2A6done =
                                  snapshot.data!.docs[index]['terminaM2A6'];
                              M2ATdone =
                                  snapshot.data!.docs[index]['terminaM2AT'];
                              M3A1done =
                                  snapshot.data!.docs[index]['terminaM3A1'];
                              M3A2done =
                                  snapshot.data!.docs[index]['terminaM3A2'];
                              M3A3done =
                                  snapshot.data!.docs[index]['terminaM3A3'];
                              M3A4done =
                                  snapshot.data!.docs[index]['terminaM3A4'];
                              M3A5done =
                                  snapshot.data!.docs[index]['terminaM3A5'];
                              M3A6done =
                                  snapshot.data!.docs[index]['terminaM3A6'];
                              M3A7done =
                                  snapshot.data!.docs[index]['terminaM3A7'];
                              M3A8done =
                                  snapshot.data!.docs[index]['terminaM3A8'];
                              M3ATdone =
                                  snapshot.data!.docs[index]['terminaM3AT'];
                              M4A1done =
                                  snapshot.data!.docs[index]['terminaM4A1'];
                              M4A2done =
                                  snapshot.data!.docs[index]['terminaM4A2'];
                              M4A3done =
                                  snapshot.data!.docs[index]['terminaM4A3'];
                              M4A4done =
                                  snapshot.data!.docs[index]['terminaM4A4'];
                              M4A5done =
                                  snapshot.data!.docs[index]['terminaM4A5'];
                              M4A6done =
                                  snapshot.data!.docs[index]['terminaM4A6'];
                              M4ATdone =
                                  snapshot.data!.docs[index]['terminaM4AT'];
                              return Container(
                                margin: const EdgeInsets.only(bottom: 50),
                                child: ListView(
                                  shrinkWrap: true,
                                  controller: controllerActividades,
                                  children: [
                                    Stack(
                                      alignment: Alignment.topRight,
                                      children: [
                                        Container(
                                          alignment: Alignment.topCenter,
                                          child: const Image(
                                            image: AssetImage(
                                                'assets/images/actividades-banner.png'),
                                          ),
                                        ),
                                        IconButton(
                                          onPressed: () {
                                            Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                builder: (context) =>
                                                    const TuPerfil(
                                                  typeOfUser: 'U',
                                                ),
                                              ),
                                            );
                                          },
                                          icon: Icon(
                                            MdiIcons.accountCircleOutline,
                                            color: Colors.black,
                                          ),
                                        ),
                                      ],
                                    ),
                                    //PRIMERO Vida

                                    Container(
                                      margin: const EdgeInsets.only(
                                          top: 20, left: 15, bottom: 5),
                                      width: size.width,
                                      child: const Text(
                                        'Vida universitaria',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                          color: CustomColors.surface,
                                          fontSize: 22,
                                        ),
                                      ),
                                    ),
                                    ScrollConfiguration(
                                      behavior: ScrollConfiguration.of(context)
                                          .copyWith(dragDevices: {
                                        PointerDeviceKind.touch,
                                        PointerDeviceKind.mouse,
                                      }),
                                      child: SingleChildScrollView(
                                        scrollDirection: Axis.horizontal,
                                        child: Row(
                                          children: [
                                            Container(
                                              margin: const EdgeInsets.all(15),
                                              width: 280,
                                              height: kIsWeb
                                                  ? size.height / 2
                                                  : size.height / 2.5,
                                              decoration: BoxDecoration(
                                                color: CustomColors.surface,
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                              ),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  const ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.only(
                                                            topLeft: Radius
                                                                .circular(20),
                                                            topRight:
                                                                Radius.circular(
                                                                    20)),
                                                    child: Image(
                                                      image: AssetImage(
                                                          'assets/images/balance-vida.png'),
                                                    ),
                                                  ),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    alignment:
                                                        Alignment.centerLeft,
                                                    width: 280,
                                                    child: const Text(
                                                      'Balance vida personal y universidad',
                                                      textScaler:
                                                          TextScaler.linear(
                                                              1.0),
                                                      style: TextStyle(
                                                          fontSize: 16),
                                                    ),
                                                  ),
                                                  const Spacer(),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    child: OutlinedButton.icon(
                                                      style: OutlinedButton
                                                          .styleFrom(
                                                        shape: RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        18.0),
                                                            side: const BorderSide(
                                                                color: Colors
                                                                    .white)),
                                                        side: const BorderSide(
                                                          color: CustomColors
                                                              .primaryDark,
                                                        ),
                                                      ),
                                                      onPressed: () {
                                                        Navigator.push(
                                                          context,
                                                          MaterialPageRoute(
                                                            builder:
                                                                ((context) =>
                                                                    const A1()),
                                                          ),
                                                        );
                                                      },
                                                      icon: Icon(
                                                        MdiIcons.plus,
                                                        color: CustomColors
                                                            .primaryDark,
                                                      ),
                                                      label: const Text(
                                                        'Iniciar',
                                                        textScaler:
                                                            TextScaler.linear(
                                                                1.0),
                                                        style: TextStyle(
                                                          color: CustomColors
                                                              .primaryDark,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Container(
                                              margin: const EdgeInsets.all(15),
                                              width: 280,
                                              height: kIsWeb
                                                  ? size.height / 2
                                                  : size.height / 2.5,
                                              decoration: BoxDecoration(
                                                color: CustomColors.surface,
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                              ),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  const ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.only(
                                                            topLeft: Radius
                                                                .circular(20),
                                                            topRight:
                                                                Radius.circular(
                                                                    20)),
                                                    child: Image(
                                                      image: AssetImage(
                                                          'assets/images/org-tiempo.png'),
                                                    ),
                                                  ),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    alignment:
                                                        Alignment.centerLeft,
                                                    width: 280,
                                                    child: const Text(
                                                      'Organización del tiempo y estudio',
                                                      textScaler:
                                                          TextScaler.linear(
                                                              1.0),
                                                      style: TextStyle(
                                                          fontSize: 16),
                                                    ),
                                                  ),
                                                  const Spacer(),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    child: OutlinedButton.icon(
                                                      style: OutlinedButton
                                                          .styleFrom(
                                                        shape: RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        18.0),
                                                            side: const BorderSide(
                                                                color: Colors
                                                                    .white)),
                                                        side: const BorderSide(
                                                          color: CustomColors
                                                              .primaryDark,
                                                        ),
                                                      ),
                                                      onPressed: () {
                                                        Navigator.push(
                                                          context,
                                                          MaterialPageRoute(
                                                            builder:
                                                                ((context) =>
                                                                    const A2()),
                                                          ),
                                                        );
                                                      },
                                                      icon: Icon(
                                                        MdiIcons.plus,
                                                        color: CustomColors
                                                            .primaryDark,
                                                      ),
                                                      label: const Text(
                                                        'Iniciar',
                                                        textScaler:
                                                            TextScaler.linear(
                                                                1.0),
                                                        style: TextStyle(
                                                          color: CustomColors
                                                              .primaryDark,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Container(
                                              margin: const EdgeInsets.all(15),
                                              width: 280,
                                              height: kIsWeb
                                                  ? size.height / 2
                                                  : size.height / 2.5,
                                              decoration: BoxDecoration(
                                                color: CustomColors.surface,
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                              ),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  const ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.only(
                                                            topLeft: Radius
                                                                .circular(20),
                                                            topRight:
                                                                Radius.circular(
                                                                    20)),
                                                    child: Image(
                                                      image: AssetImage(
                                                          'assets/images/superar-procastinacion.png'),
                                                    ),
                                                  ),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    alignment:
                                                        Alignment.centerLeft,
                                                    width: 280,
                                                    child: const Text(
                                                      'Superando la procastinación',
                                                      textScaler:
                                                          TextScaler.linear(
                                                              1.0),
                                                      style: TextStyle(
                                                          fontSize: 16),
                                                    ),
                                                  ),
                                                  const Spacer(),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    child: OutlinedButton.icon(
                                                      style: OutlinedButton
                                                          .styleFrom(
                                                        shape: RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        18.0),
                                                            side: const BorderSide(
                                                                color: Colors
                                                                    .white)),
                                                        side: const BorderSide(
                                                          color: CustomColors
                                                              .primaryDark,
                                                        ),
                                                      ),
                                                      onPressed: () {
                                                        Navigator.push(
                                                          context,
                                                          MaterialPageRoute(
                                                            builder:
                                                                ((context) =>
                                                                    const A3()),
                                                          ),
                                                        );
                                                      },
                                                      icon: Icon(
                                                        MdiIcons.plus,
                                                        color: CustomColors
                                                            .primaryDark,
                                                      ),
                                                      label: const Text(
                                                        'Iniciar',
                                                        textScaler:
                                                            TextScaler.linear(
                                                                1.0),
                                                        style: TextStyle(
                                                          color: CustomColors
                                                              .primaryDark,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Container(
                                              margin: const EdgeInsets.all(15),
                                              width: 280,
                                              height: kIsWeb
                                                  ? size.height / 2
                                                  : size.height / 2.5,
                                              decoration: BoxDecoration(
                                                color: CustomColors.surface,
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                              ),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  const ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.only(
                                                            topLeft: Radius
                                                                .circular(20),
                                                            topRight:
                                                                Radius.circular(
                                                                    20)),
                                                    child: Image(
                                                      image: AssetImage(
                                                          'assets/images/afrontar-estres.png'),
                                                    ),
                                                  ),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    alignment:
                                                        Alignment.centerLeft,
                                                    width: 280,
                                                    child: const Text(
                                                      'Afrontando el estrés académico',
                                                      textScaler:
                                                          TextScaler.linear(
                                                              1.0),
                                                      style: TextStyle(
                                                          fontSize: 16),
                                                    ),
                                                  ),
                                                  const Spacer(),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    child: OutlinedButton.icon(
                                                      style: OutlinedButton
                                                          .styleFrom(
                                                        shape: RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        18.0),
                                                            side: const BorderSide(
                                                                color: Colors
                                                                    .white)),
                                                        side: const BorderSide(
                                                          color: CustomColors
                                                              .primaryDark,
                                                        ),
                                                      ),
                                                      onPressed: () {
                                                        Navigator.push(
                                                          context,
                                                          MaterialPageRoute(
                                                            builder:
                                                                ((context) =>
                                                                    const A4()),
                                                          ),
                                                        );
                                                      },
                                                      icon: Icon(
                                                        MdiIcons.plus,
                                                        color: CustomColors
                                                            .primaryDark,
                                                      ),
                                                      label: const Text(
                                                        'Iniciar',
                                                        textScaler:
                                                            TextScaler.linear(
                                                                1.0),
                                                        style: TextStyle(
                                                          color: CustomColors
                                                              .primaryDark,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Container(
                                              margin: const EdgeInsets.all(15),
                                              width: 280,
                                              height: kIsWeb
                                                  ? size.height / 2
                                                  : size.height / 2.5,
                                              decoration: BoxDecoration(
                                                color: CustomColors.surface,
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                              ),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  const ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.only(
                                                            topLeft: Radius
                                                                .circular(20),
                                                            topRight:
                                                                Radius.circular(
                                                                    20)),
                                                    child: Image(
                                                      image: AssetImage(
                                                          'assets/images/amistades-social.png'),
                                                    ),
                                                  ),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    alignment:
                                                        Alignment.centerLeft,
                                                    width: 280,
                                                    child: const Text(
                                                      'Amistades y vida social',
                                                      textScaler:
                                                          TextScaler.linear(
                                                              1.0),
                                                      style: TextStyle(
                                                          fontSize: 16),
                                                    ),
                                                  ),
                                                  const Spacer(),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    child: OutlinedButton.icon(
                                                      style: OutlinedButton
                                                          .styleFrom(
                                                        shape: RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        18.0),
                                                            side: const BorderSide(
                                                                color: Colors
                                                                    .white)),
                                                        side: const BorderSide(
                                                          color: CustomColors
                                                              .primaryDark,
                                                        ),
                                                      ),
                                                      onPressed: () {
                                                        Navigator.push(
                                                          context,
                                                          MaterialPageRoute(
                                                            builder:
                                                                ((context) =>
                                                                    const A5()),
                                                          ),
                                                        );
                                                      },
                                                      icon: Icon(
                                                        MdiIcons.plus,
                                                        color: CustomColors
                                                            .primaryDark,
                                                      ),
                                                      label: const Text(
                                                        'Iniciar',
                                                        textScaler:
                                                            TextScaler.linear(
                                                                1.0),
                                                        style: TextStyle(
                                                          color: CustomColors
                                                              .primaryDark,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Container(
                                              margin: const EdgeInsets.all(15),
                                              width: 280,
                                              height: kIsWeb
                                                  ? size.height / 2
                                                  : size.height / 2.5,
                                              decoration: BoxDecoration(
                                                color: CustomColors.surface,
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                              ),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  const ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.only(
                                                            topLeft: Radius
                                                                .circular(20),
                                                            topRight:
                                                                Radius.circular(
                                                                    20)),
                                                    child: Image(
                                                      image: AssetImage(
                                                          'assets/images/resolver-conflictos.png'),
                                                    ),
                                                  ),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    alignment:
                                                        Alignment.centerLeft,
                                                    width: 280,
                                                    child: const Text(
                                                      'Resolviendo conflictos',
                                                      textScaler:
                                                          TextScaler.linear(
                                                              1.0),
                                                      style: TextStyle(
                                                          fontSize: 16),
                                                    ),
                                                  ),
                                                  const Spacer(),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    child: OutlinedButton.icon(
                                                      style: OutlinedButton
                                                          .styleFrom(
                                                        shape: RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        18.0),
                                                            side: const BorderSide(
                                                                color: Colors
                                                                    .white)),
                                                        side: const BorderSide(
                                                          color: CustomColors
                                                              .primaryDark,
                                                        ),
                                                      ),
                                                      onPressed: () {
                                                        Navigator.push(
                                                          context,
                                                          MaterialPageRoute(
                                                            builder:
                                                                ((context) =>
                                                                    const A6()),
                                                          ),
                                                        );
                                                      },
                                                      icon: Icon(
                                                        MdiIcons.plus,
                                                        color: CustomColors
                                                            .primaryDark,
                                                      ),
                                                      label: const Text(
                                                        'Iniciar',
                                                        textScaler:
                                                            TextScaler.linear(
                                                                1.0),
                                                        style: TextStyle(
                                                          color: CustomColors
                                                              .primaryDark,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Container(
                                              margin: const EdgeInsets.all(15),
                                              width: 280,
                                              height: kIsWeb
                                                  ? size.height / 2
                                                  : size.height / 2.5,
                                              decoration: BoxDecoration(
                                                color: CustomColors.surface,
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                              ),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  const ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.only(
                                                            topLeft: Radius
                                                                .circular(20),
                                                            topRight:
                                                                Radius.circular(
                                                                    20)),
                                                    child: Image(
                                                      image: AssetImage(
                                                          'assets/images/trabajos-grupo.png'),
                                                    ),
                                                  ),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    alignment:
                                                        Alignment.centerLeft,
                                                    width: 280,
                                                    child: const Text(
                                                      'Trabajos en grupo: desafíos y oportunidades',
                                                      textScaler:
                                                          TextScaler.linear(
                                                              1.0),
                                                      style: TextStyle(
                                                          fontSize: 16),
                                                    ),
                                                  ),
                                                  const Spacer(),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    child: OutlinedButton.icon(
                                                      style: OutlinedButton
                                                          .styleFrom(
                                                        shape: RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        18.0),
                                                            side: const BorderSide(
                                                                color: Colors
                                                                    .white)),
                                                        side: const BorderSide(
                                                          color: CustomColors
                                                              .primaryDark,
                                                        ),
                                                      ),
                                                      onPressed: () {
                                                        Navigator.push(
                                                          context,
                                                          MaterialPageRoute(
                                                            builder:
                                                                ((context) =>
                                                                    const A7()),
                                                          ),
                                                        );
                                                      },
                                                      icon: Icon(
                                                        MdiIcons.plus,
                                                        color: CustomColors
                                                            .primaryDark,
                                                      ),
                                                      label: const Text(
                                                        'Iniciar',
                                                        textScaler:
                                                            TextScaler.linear(
                                                                1.0),
                                                        style: TextStyle(
                                                          color: CustomColors
                                                              .primaryDark,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    //SEGUNDO Consejos
                                    Container(
                                      margin: const EdgeInsets.only(
                                          top: 20, left: 15, bottom: 5),
                                      width: size.width,
                                      child: const Text(
                                        'Consejos para tu bienestar',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 22,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: const EdgeInsets.only(
                                          top: 5,
                                          left: 15,
                                          bottom: 5,
                                          right: 10),
                                      width: size.width,
                                      child: const Text(
                                        'A medida de que avances en tu programa podrás acceder a diversos consejos que Vamos Juntxs tiene para ti.',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                          color: CustomColors.surface,
                                          fontSize: 16,
                                        ),
                                      ),
                                    ),
                                    ScrollConfiguration(
                                      behavior: ScrollConfiguration.of(context)
                                          .copyWith(dragDevices: {
                                        PointerDeviceKind.touch,
                                        PointerDeviceKind.mouse,
                                      }),
                                      child: SingleChildScrollView(
                                        scrollDirection: Axis.horizontal,
                                        child: Row(
                                          children: [
                                            Container(
                                              margin: const EdgeInsets.all(15),
                                              width: 280,
                                              height: kIsWeb
                                                  ? size.height / 2
                                                  : size.height / 2.5,
                                              decoration: BoxDecoration(
                                                  color: M1A1done
                                                      ? CustomColors.surface
                                                      : CustomColors
                                                          .purplePrograma,
                                                  borderRadius:
                                                      BorderRadius.circular(20),
                                                  border: Border.all(
                                                    color: M1A1done
                                                        ? Colors.transparent
                                                        : CustomColors
                                                            .secondary,
                                                  )),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  ClipRRect(
                                                    borderRadius:
                                                        const BorderRadius.only(
                                                            topLeft:
                                                                Radius.circular(
                                                                    20),
                                                            topRight:
                                                                Radius.circular(
                                                                    20)),
                                                    child: Image(
                                                      image: AssetImage(M1A1done
                                                          ? 'assets/images/buen-dormir.png'
                                                          : 'assets/images/locked-actividades.png'),
                                                    ),
                                                  ),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    alignment:
                                                        Alignment.centerLeft,
                                                    width: 280,
                                                    child: Text(
                                                      M1A1done
                                                          ? 'Buen dormir'
                                                          : '',
                                                      textScaler:
                                                          const TextScaler
                                                              .linear(1.0),
                                                      style: const TextStyle(
                                                          fontSize: 16),
                                                    ),
                                                  ),
                                                  const Spacer(),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    child: OutlinedButton.icon(
                                                      style: OutlinedButton
                                                          .styleFrom(
                                                        shape: RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        18.0),
                                                            side: const BorderSide(
                                                                color: Colors
                                                                    .white)),
                                                        side: BorderSide(
                                                          color: M1A1done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                      ),
                                                      onPressed: () {
                                                        if (M1A1done) {
                                                          Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                              builder:
                                                                  ((context) =>
                                                                      const A8()),
                                                            ),
                                                          );
                                                        } else {
                                                          return null;
                                                        }
                                                      },
                                                      icon: Icon(
                                                        M1A1done
                                                            ? MdiIcons.plus
                                                            : MdiIcons.lock,
                                                        color: M1A1done
                                                            ? CustomColors
                                                                .primaryDark
                                                            : CustomColors
                                                                .secondary,
                                                      ),
                                                      label: Text(
                                                        M1A1done
                                                            ? 'Iniciar'
                                                            : 'No disponible',
                                                        textScaler:
                                                            const TextScaler
                                                                .linear(1.0),
                                                        style: TextStyle(
                                                          color: M1A1done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Container(
                                              margin: const EdgeInsets.all(15),
                                              width: 280,
                                              height: kIsWeb
                                                  ? size.height / 2
                                                  : size.height / 2.5,
                                              decoration: BoxDecoration(
                                                color: M1A5done
                                                    ? CustomColors.surface
                                                    : CustomColors
                                                        .purplePrograma,
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                                border: Border.all(
                                                  color: M1A5done
                                                      ? Colors.transparent
                                                      : CustomColors.secondary,
                                                ),
                                              ),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  ClipRRect(
                                                    borderRadius:
                                                        const BorderRadius.only(
                                                            topLeft:
                                                                Radius.circular(
                                                                    20),
                                                            topRight:
                                                                Radius.circular(
                                                                    20)),
                                                    child: Image(
                                                      image: AssetImage(M1A5done
                                                          ? 'assets/images/habitos-saludables.png'
                                                          : 'assets/images/locked-actividades.png'),
                                                    ),
                                                  ),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    alignment:
                                                        Alignment.centerLeft,
                                                    width: 280,
                                                    child: Text(
                                                      M1A5done
                                                          ? 'Hábitos saludables'
                                                          : '',
                                                      textScaler:
                                                          const TextScaler
                                                              .linear(1.0),
                                                      style: const TextStyle(
                                                          fontSize: 16),
                                                    ),
                                                  ),
                                                  const Spacer(),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    child: OutlinedButton.icon(
                                                      style: OutlinedButton
                                                          .styleFrom(
                                                        shape: RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        18.0),
                                                            side: const BorderSide(
                                                                color: Colors
                                                                    .white)),
                                                        side: BorderSide(
                                                          color: M1A5done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                      ),
                                                      onPressed: () {
                                                        if (M1A5done) {
                                                          Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                              builder:
                                                                  ((context) =>
                                                                      const A9()),
                                                            ),
                                                          );
                                                        } else {
                                                          return null;
                                                        }
                                                      },
                                                      icon: Icon(
                                                        M1A5done
                                                            ? MdiIcons.plus
                                                            : MdiIcons.lock,
                                                        color: M1A5done
                                                            ? CustomColors
                                                                .primaryDark
                                                            : CustomColors
                                                                .secondary,
                                                      ),
                                                      label: Text(
                                                        M1A5done
                                                            ? 'Iniciar'
                                                            : 'No disponible',
                                                        textScaler:
                                                            const TextScaler
                                                                .linear(1.0),
                                                        style: TextStyle(
                                                          color: M1A5done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Container(
                                              margin: const EdgeInsets.all(15),
                                              width: 280,
                                              height: kIsWeb
                                                  ? size.height / 2
                                                  : size.height / 2.5,
                                              decoration: BoxDecoration(
                                                color: M3A4done
                                                    ? CustomColors.surface
                                                    : CustomColors
                                                        .purplePrograma,
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                                border: Border.all(
                                                  color: M3A4done
                                                      ? Colors.transparent
                                                      : CustomColors.secondary,
                                                ),
                                              ),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  ClipRRect(
                                                    borderRadius:
                                                        const BorderRadius.only(
                                                            topLeft:
                                                                Radius.circular(
                                                                    20),
                                                            topRight:
                                                                Radius.circular(
                                                                    20)),
                                                    child: Image(
                                                      image: AssetImage(M3A4done
                                                          ? 'assets/images/resolviendo-problemas.png'
                                                          : 'assets/images/locked-actividades.png'),
                                                    ),
                                                  ),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    alignment:
                                                        Alignment.centerLeft,
                                                    width: 280,
                                                    child: Text(
                                                      M3A4done
                                                          ? '¿Cómo estás resolviendo problemas?'
                                                          : '',
                                                      textScaler:
                                                          const TextScaler
                                                              .linear(1.0),
                                                      style: const TextStyle(
                                                          fontSize: 16),
                                                    ),
                                                  ),
                                                  const Spacer(),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    child: OutlinedButton.icon(
                                                      style: OutlinedButton
                                                          .styleFrom(
                                                        shape: RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        18.0),
                                                            side: const BorderSide(
                                                                color: Colors
                                                                    .white)),
                                                        side: BorderSide(
                                                          color: M3A4done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                      ),
                                                      onPressed: () {
                                                        if (M3A4done) {
                                                          Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                              builder:
                                                                  ((context) =>
                                                                      const A10()),
                                                            ),
                                                          );
                                                        } else {
                                                          return null;
                                                        }
                                                      },
                                                      icon: Icon(
                                                        M3A4done
                                                            ? MdiIcons.plus
                                                            : MdiIcons.lock,
                                                        color: M3A4done
                                                            ? CustomColors
                                                                .primaryDark
                                                            : CustomColors
                                                                .secondary,
                                                      ),
                                                      label: Text(
                                                        M3A4done
                                                            ? 'Iniciar'
                                                            : 'No disponible',
                                                        textScaler:
                                                            const TextScaler
                                                                .linear(1.0),
                                                        style: TextStyle(
                                                          color: M3A4done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Container(
                                              margin: const EdgeInsets.all(15),
                                              width: 280,
                                              height: kIsWeb
                                                  ? size.height / 2
                                                  : size.height / 2.5,
                                              decoration: BoxDecoration(
                                                color: M3A5done
                                                    ? CustomColors.surface
                                                    : CustomColors
                                                        .purplePrograma,
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                                border: Border.all(
                                                  color: M3A5done
                                                      ? Colors.transparent
                                                      : CustomColors.secondary,
                                                ),
                                              ),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  ClipRRect(
                                                    borderRadius:
                                                        const BorderRadius.only(
                                                            topLeft:
                                                                Radius.circular(
                                                                    20),
                                                            topRight:
                                                                Radius.circular(
                                                                    20)),
                                                    child: Image(
                                                      image: AssetImage(M3A5done
                                                          ? 'assets/images/superando-sobrecarga.png'
                                                          : 'assets/images/locked-actividades.png'),
                                                    ),
                                                  ),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    alignment:
                                                        Alignment.centerLeft,
                                                    width: 280,
                                                    child: Text(
                                                      M3A5done
                                                          ? 'Superando la sobrecarga mental'
                                                          : '',
                                                      textScaler:
                                                          const TextScaler
                                                              .linear(1.0),
                                                      style: const TextStyle(
                                                          fontSize: 16),
                                                    ),
                                                  ),
                                                  const Spacer(),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    child: OutlinedButton.icon(
                                                      style: OutlinedButton
                                                          .styleFrom(
                                                        shape: RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        18.0),
                                                            side: const BorderSide(
                                                                color: Colors
                                                                    .white)),
                                                        side: BorderSide(
                                                          color: M3A5done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                      ),
                                                      onPressed: () {
                                                        if (M3A5done) {
                                                          Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                              builder:
                                                                  ((context) =>
                                                                      const A11()),
                                                            ),
                                                          );
                                                        } else {
                                                          return null;
                                                        }
                                                      },
                                                      icon: Icon(
                                                        M3A5done
                                                            ? MdiIcons.plus
                                                            : MdiIcons.lock,
                                                        color: M3A5done
                                                            ? CustomColors
                                                                .primaryDark
                                                            : CustomColors
                                                                .secondary,
                                                      ),
                                                      label: Text(
                                                        M3A5done
                                                            ? 'Iniciar'
                                                            : 'No disponible',
                                                        textScaler:
                                                            const TextScaler
                                                                .linear(1.0),
                                                        style: TextStyle(
                                                          color: M3A5done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Container(
                                              margin: const EdgeInsets.all(15),
                                              width: 280,
                                              height: kIsWeb
                                                  ? size.height / 2
                                                  : size.height / 2.5,
                                              decoration: BoxDecoration(
                                                color: M3A7done
                                                    ? CustomColors.surface
                                                    : CustomColors
                                                        .purplePrograma,
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                                border: Border.all(
                                                  color: M3A7done
                                                      ? Colors.transparent
                                                      : CustomColors.secondary,
                                                ),
                                              ),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  ClipRRect(
                                                    borderRadius:
                                                        const BorderRadius.only(
                                                            topLeft:
                                                                Radius.circular(
                                                                    20),
                                                            topRight:
                                                                Radius.circular(
                                                                    20)),
                                                    child: Image(
                                                      image: AssetImage(M3A7done
                                                          ? 'assets/images/cambiando-forma.png'
                                                          : 'assets/images/locked-actividades.png'),
                                                    ),
                                                  ),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    alignment:
                                                        Alignment.centerLeft,
                                                    width: 280,
                                                    child: Text(
                                                      M3A7done
                                                          ? 'Cambiando la forma en la que nos hablamos'
                                                          : '',
                                                      textScaler:
                                                          const TextScaler
                                                              .linear(1.0),
                                                      style: const TextStyle(
                                                          fontSize: 16),
                                                    ),
                                                  ),
                                                  const Spacer(),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    child: OutlinedButton.icon(
                                                      style: OutlinedButton
                                                          .styleFrom(
                                                        shape: RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        18.0),
                                                            side: const BorderSide(
                                                                color: Colors
                                                                    .white)),
                                                        side: BorderSide(
                                                          color: M3A7done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                      ),
                                                      onPressed: () {
                                                        if (M3A7done) {
                                                          Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                              builder:
                                                                  ((context) =>
                                                                      const A12()),
                                                            ),
                                                          );
                                                        } else {
                                                          return null;
                                                        }
                                                      },
                                                      icon: Icon(
                                                        M3A7done
                                                            ? MdiIcons.plus
                                                            : MdiIcons.lock,
                                                        color: M3A7done
                                                            ? CustomColors
                                                                .primaryDark
                                                            : CustomColors
                                                                .secondary,
                                                      ),
                                                      label: Text(
                                                        M3A7done
                                                            ? 'Iniciar'
                                                            : 'No disponible',
                                                        textScaler:
                                                            const TextScaler
                                                                .linear(1.0),
                                                        style: TextStyle(
                                                          color: M3A7done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    //TERCERO Respira
                                    Container(
                                      margin: const EdgeInsets.only(
                                          top: 20, left: 15, bottom: 5),
                                      width: size.width,
                                      child: const Text(
                                        'Respira y relájate',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 22,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: const EdgeInsets.only(
                                          top: 5,
                                          left: 15,
                                          bottom: 5,
                                          right: 10),
                                      width: size.width,
                                      child: const Text(
                                        'Aquí podrás encontrar diversos audios de relajación y respiración que te ayudarán a relajarte y promover tu bienestar. A medida que avances en tu programa podrás ir desbloqueando nuevos audios.',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                          color: CustomColors.surface,
                                          fontSize: 16,
                                        ),
                                      ),
                                    ),
                                    ScrollConfiguration(
                                      behavior: ScrollConfiguration.of(context)
                                          .copyWith(dragDevices: {
                                        PointerDeviceKind.touch,
                                        PointerDeviceKind.mouse,
                                      }),
                                      child: SingleChildScrollView(
                                        scrollDirection: Axis.horizontal,
                                        child: Row(
                                          children: [
                                            Container(
                                              margin: const EdgeInsets.all(15),
                                              width: 280,
                                              height: kIsWeb
                                                  ? size.height / 2
                                                  : size.height / 2.5,
                                              decoration: BoxDecoration(
                                                color: M1A2done
                                                    ? CustomColors.surface
                                                    : CustomColors
                                                        .purplePrograma,
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                                border: Border.all(
                                                  color: M1A2done
                                                      ? Colors.transparent
                                                      : CustomColors.secondary,
                                                ),
                                              ),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  ClipRRect(
                                                    borderRadius:
                                                        const BorderRadius.only(
                                                            topLeft:
                                                                Radius.circular(
                                                                    20),
                                                            topRight:
                                                                Radius.circular(
                                                                    20)),
                                                    child: Image(
                                                      image: AssetImage(M1A2done
                                                          ? 'assets/images/agua.png'
                                                          : 'assets/images/locked-actividades.png'),
                                                    ),
                                                  ),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    alignment:
                                                        Alignment.centerLeft,
                                                    width: 280,
                                                    child: Text(
                                                      M1A2done
                                                          ? 'Agua de río'
                                                          : '',
                                                      textScaler:
                                                          const TextScaler
                                                              .linear(1.0),
                                                      style: const TextStyle(
                                                          fontSize: 16),
                                                    ),
                                                  ),
                                                  const Spacer(),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    child: OutlinedButton.icon(
                                                      style: OutlinedButton
                                                          .styleFrom(
                                                        shape: RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        18.0),
                                                            side: const BorderSide(
                                                                color: Colors
                                                                    .white)),
                                                        side: BorderSide(
                                                          color: M1A2done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                      ),
                                                      onPressed: () {
                                                        if (M1A2done) {
                                                          Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                              builder:
                                                                  ((context) =>
                                                                      const A13()),
                                                            ),
                                                          );
                                                        } else {
                                                          return null;
                                                        }
                                                      },
                                                      icon: Icon(
                                                        M1A2done
                                                            ? MdiIcons.plus
                                                            : MdiIcons.lock,
                                                        color: M1A2done
                                                            ? CustomColors
                                                                .primaryDark
                                                            : CustomColors
                                                                .secondary,
                                                      ),
                                                      label: Text(
                                                        M1A2done
                                                            ? 'Iniciar'
                                                            : 'No disponible',
                                                        textScaler:
                                                            const TextScaler
                                                                .linear(1.0),
                                                        style: TextStyle(
                                                          color: M1A2done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Container(
                                              margin: const EdgeInsets.all(15),
                                              width: 280,
                                              height: kIsWeb
                                                  ? size.height / 2
                                                  : size.height / 2.5,
                                              decoration: BoxDecoration(
                                                color: M1A2done
                                                    ? CustomColors.surface
                                                    : CustomColors
                                                        .purplePrograma,
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                                border: Border.all(
                                                  color: M1A2done
                                                      ? Colors.transparent
                                                      : CustomColors.secondary,
                                                ),
                                              ),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  ClipRRect(
                                                    borderRadius:
                                                        const BorderRadius.only(
                                                            topLeft:
                                                                Radius.circular(
                                                                    20),
                                                            topRight:
                                                                Radius.circular(
                                                                    20)),
                                                    child: Image(
                                                      image: AssetImage(M1A2done
                                                          ? 'assets/images/bosque.png'
                                                          : 'assets/images/locked-actividades.png'),
                                                    ),
                                                  ),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    alignment:
                                                        Alignment.centerLeft,
                                                    width: 280,
                                                    child: Text(
                                                      M1A2done
                                                          ? 'Bosque lluvioso'
                                                          : '',
                                                      textScaler:
                                                          const TextScaler
                                                              .linear(1.0),
                                                      style: const TextStyle(
                                                          fontSize: 16),
                                                    ),
                                                  ),
                                                  const Spacer(),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    child: OutlinedButton.icon(
                                                      style: OutlinedButton
                                                          .styleFrom(
                                                        shape: RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        18.0),
                                                            side: const BorderSide(
                                                                color: Colors
                                                                    .white)),
                                                        side: BorderSide(
                                                          color: M1A2done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                      ),
                                                      onPressed: () {
                                                        if (M1A2done) {
                                                          Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                              builder:
                                                                  ((context) =>
                                                                      const A14()),
                                                            ),
                                                          );
                                                        } else {
                                                          return null;
                                                        }
                                                      },
                                                      icon: Icon(
                                                        M1A2done
                                                            ? MdiIcons.plus
                                                            : MdiIcons.lock,
                                                        color: M1A2done
                                                            ? CustomColors
                                                                .primaryDark
                                                            : CustomColors
                                                                .secondary,
                                                      ),
                                                      label: Text(
                                                        M1A2done
                                                            ? 'Iniciar'
                                                            : 'No disponible',
                                                        textScaler:
                                                            const TextScaler
                                                                .linear(1.0),
                                                        style: TextStyle(
                                                          color: M1A2done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Container(
                                              margin: const EdgeInsets.all(15),
                                              width: 280,
                                              height: kIsWeb
                                                  ? size.height / 2
                                                  : size.height / 2.5,
                                              decoration: BoxDecoration(
                                                color: M1A3done
                                                    ? CustomColors.surface
                                                    : CustomColors
                                                        .purplePrograma,
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                                border: Border.all(
                                                  color: M1A3done
                                                      ? Colors.transparent
                                                      : CustomColors.secondary,
                                                ),
                                              ),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  ClipRRect(
                                                    borderRadius:
                                                        const BorderRadius.only(
                                                            topLeft:
                                                                Radius.circular(
                                                                    20),
                                                            topRight:
                                                                Radius.circular(
                                                                    20)),
                                                    child: Image(
                                                      image: AssetImage(M1A3done
                                                          ? 'assets/images/respiracion-dormir.png'
                                                          : 'assets/images/locked-actividades.png'),
                                                    ),
                                                  ),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    alignment:
                                                        Alignment.centerLeft,
                                                    width: 280,
                                                    child: Text(
                                                      M1A3done
                                                          ? 'Respiración para antes de dormir'
                                                          : '',
                                                      textScaler:
                                                          const TextScaler
                                                              .linear(1.0),
                                                      style: const TextStyle(
                                                          fontSize: 16),
                                                    ),
                                                  ),
                                                  const Spacer(),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    child: OutlinedButton.icon(
                                                      style: OutlinedButton
                                                          .styleFrom(
                                                        shape: RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        18.0),
                                                            side: const BorderSide(
                                                                color: Colors
                                                                    .white)),
                                                        side: BorderSide(
                                                          color: M1A3done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                      ),
                                                      onPressed: () {
                                                        if (M1A3done) {
                                                          Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                              builder:
                                                                  ((context) =>
                                                                      const A15()),
                                                            ),
                                                          );
                                                        } else {
                                                          return null;
                                                        }
                                                      },
                                                      icon: Icon(
                                                        M1A3done
                                                            ? MdiIcons.plus
                                                            : MdiIcons.lock,
                                                        color: M1A3done
                                                            ? CustomColors
                                                                .primaryDark
                                                            : CustomColors
                                                                .secondary,
                                                      ),
                                                      label: Text(
                                                        M1A3done
                                                            ? 'Iniciar'
                                                            : 'No disponible',
                                                        textScaler:
                                                            const TextScaler
                                                                .linear(1.0),
                                                        style: TextStyle(
                                                          color: M1A3done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Container(
                                              margin: const EdgeInsets.all(15),
                                              width: 280,
                                              height: kIsWeb
                                                  ? size.height / 2
                                                  : size.height / 2.5,
                                              decoration: BoxDecoration(
                                                color: M2A5done
                                                    ? CustomColors.surface
                                                    : CustomColors
                                                        .purplePrograma,
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                                border: Border.all(
                                                  color: M2A5done
                                                      ? Colors.transparent
                                                      : CustomColors.secondary,
                                                ),
                                              ),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  ClipRRect(
                                                    borderRadius:
                                                        const BorderRadius.only(
                                                            topLeft:
                                                                Radius.circular(
                                                                    20),
                                                            topRight:
                                                                Radius.circular(
                                                                    20)),
                                                    child: Image(
                                                      image: AssetImage(M2A5done
                                                          ? 'assets/images/relajacion-respiracion.png'
                                                          : 'assets/images/locked-actividades.png'),
                                                    ),
                                                  ),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    alignment:
                                                        Alignment.centerLeft,
                                                    width: 280,
                                                    child: Text(
                                                      M2A5done
                                                          ? 'Relajación por medio de la respiración'
                                                          : '',
                                                      textScaler:
                                                          const TextScaler
                                                              .linear(1.0),
                                                      style: const TextStyle(
                                                          fontSize: 16),
                                                    ),
                                                  ),
                                                  const Spacer(),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    child: OutlinedButton.icon(
                                                      style: OutlinedButton
                                                          .styleFrom(
                                                        shape: RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        18.0),
                                                            side: const BorderSide(
                                                                color: Colors
                                                                    .white)),
                                                        side: BorderSide(
                                                          color: M2A5done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                      ),
                                                      onPressed: () {
                                                        if (M2A5done) {
                                                          Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                              builder:
                                                                  ((context) =>
                                                                      const A16()),
                                                            ),
                                                          );
                                                        } else {
                                                          return null;
                                                        }
                                                      },
                                                      icon: Icon(
                                                        M2A5done
                                                            ? MdiIcons.plus
                                                            : MdiIcons.lock,
                                                        color: M2A5done
                                                            ? CustomColors
                                                                .primaryDark
                                                            : CustomColors
                                                                .secondary,
                                                      ),
                                                      label: Text(
                                                        M2A5done
                                                            ? 'Iniciar'
                                                            : 'No disponible',
                                                        textScaler:
                                                            const TextScaler
                                                                .linear(1.0),
                                                        style: TextStyle(
                                                          color: M2A5done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Container(
                                              margin: const EdgeInsets.all(15),
                                              width: 280,
                                              height: kIsWeb
                                                  ? size.height / 2
                                                  : size.height / 2.5,
                                              decoration: BoxDecoration(
                                                color: M3A2done
                                                    ? CustomColors.surface
                                                    : CustomColors
                                                        .purplePrograma,
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                                border: Border.all(
                                                  color: M3A2done
                                                      ? Colors.transparent
                                                      : CustomColors.secondary,
                                                ),
                                              ),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  ClipRRect(
                                                    borderRadius:
                                                        const BorderRadius.only(
                                                            topLeft:
                                                                Radius.circular(
                                                                    20),
                                                            topRight:
                                                                Radius.circular(
                                                                    20)),
                                                    child: Image(
                                                      image: AssetImage(M3A2done
                                                          ? 'assets/images/selva.png'
                                                          : 'assets/images/locked-actividades.png'),
                                                    ),
                                                  ),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    alignment:
                                                        Alignment.centerLeft,
                                                    width: 280,
                                                    child: Text(
                                                      M3A2done
                                                          ? 'Selva tropical'
                                                          : '',
                                                      textScaler:
                                                          const TextScaler
                                                              .linear(1.0),
                                                      style: const TextStyle(
                                                          fontSize: 16),
                                                    ),
                                                  ),
                                                  const Spacer(),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    child: OutlinedButton.icon(
                                                      style: OutlinedButton
                                                          .styleFrom(
                                                        shape: RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        18.0),
                                                            side: const BorderSide(
                                                                color: Colors
                                                                    .white)),
                                                        side: BorderSide(
                                                          color: M3A2done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                      ),
                                                      onPressed: () {
                                                        if (M3A2done) {
                                                          Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                              builder:
                                                                  ((context) =>
                                                                      const A17()),
                                                            ),
                                                          );
                                                        } else {
                                                          return null;
                                                        }
                                                      },
                                                      icon: Icon(
                                                        M3A2done
                                                            ? MdiIcons.plus
                                                            : MdiIcons.lock,
                                                        color: M3A2done
                                                            ? CustomColors
                                                                .primaryDark
                                                            : CustomColors
                                                                .secondary,
                                                      ),
                                                      label: Text(
                                                        M3A2done
                                                            ? 'Iniciar'
                                                            : 'No disponible',
                                                        textScaler:
                                                            const TextScaler
                                                                .linear(1.0),
                                                        style: TextStyle(
                                                          color: M3A2done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Container(
                                              margin: const EdgeInsets.all(15),
                                              width: 280,
                                              height: kIsWeb
                                                  ? size.height / 2
                                                  : size.height / 2.5,
                                              decoration: BoxDecoration(
                                                color: M3A2done
                                                    ? CustomColors.surface
                                                    : CustomColors
                                                        .purplePrograma,
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                                border: Border.all(
                                                  color: M3A2done
                                                      ? Colors.transparent
                                                      : CustomColors.secondary,
                                                ),
                                              ),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  ClipRRect(
                                                    borderRadius:
                                                        const BorderRadius.only(
                                                            topLeft:
                                                                Radius.circular(
                                                                    20),
                                                            topRight:
                                                                Radius.circular(
                                                                    20)),
                                                    child: Image(
                                                      image: AssetImage(M3A2done
                                                          ? 'assets/images/viento.png'
                                                          : 'assets/images/locked-actividades.png'),
                                                    ),
                                                  ),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    alignment:
                                                        Alignment.centerLeft,
                                                    width: 280,
                                                    child: Text(
                                                      M3A2done ? 'Viento' : '',
                                                      textScaler:
                                                          const TextScaler
                                                              .linear(1.0),
                                                      style: const TextStyle(
                                                          fontSize: 16),
                                                    ),
                                                  ),
                                                  const Spacer(),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    child: OutlinedButton.icon(
                                                      style: OutlinedButton
                                                          .styleFrom(
                                                        shape: RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        18.0),
                                                            side: const BorderSide(
                                                                color: Colors
                                                                    .white)),
                                                        side: BorderSide(
                                                          color: M3A2done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                      ),
                                                      onPressed: () {
                                                        if (M3A2done) {
                                                          Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                              builder:
                                                                  ((context) =>
                                                                      const A18()),
                                                            ),
                                                          );
                                                        } else {
                                                          return null;
                                                        }
                                                      },
                                                      icon: Icon(
                                                        M3A2done
                                                            ? MdiIcons.plus
                                                            : MdiIcons.lock,
                                                        color: M3A2done
                                                            ? CustomColors
                                                                .primaryDark
                                                            : CustomColors
                                                                .secondary,
                                                      ),
                                                      label: Text(
                                                        M3A2done
                                                            ? 'Iniciar'
                                                            : 'No disponible',
                                                        textScaler:
                                                            const TextScaler
                                                                .linear(1.0),
                                                        style: TextStyle(
                                                          color: M3A2done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Container(
                                              margin: const EdgeInsets.all(15),
                                              width: 280,
                                              height: kIsWeb
                                                  ? size.height / 2
                                                  : size.height / 2.5,
                                              decoration: BoxDecoration(
                                                color: M3A6done
                                                    ? CustomColors.surface
                                                    : CustomColors
                                                        .purplePrograma,
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                                border: Border.all(
                                                  color: M3A6done
                                                      ? Colors.transparent
                                                      : CustomColors.secondary,
                                                ),
                                              ),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  ClipRRect(
                                                    borderRadius:
                                                        const BorderRadius.only(
                                                            topLeft:
                                                                Radius.circular(
                                                                    20),
                                                            topRight:
                                                                Radius.circular(
                                                                    20)),
                                                    child: Image(
                                                      image: AssetImage(M3A6done
                                                          ? 'assets/images/noche-verano.png'
                                                          : 'assets/images/locked-actividades.png'),
                                                    ),
                                                  ),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    alignment:
                                                        Alignment.centerLeft,
                                                    width: 280,
                                                    child: Text(
                                                      M3A6done
                                                          ? 'Noche de verano'
                                                          : '',
                                                      textScaler:
                                                          const TextScaler
                                                              .linear(1.0),
                                                      style: const TextStyle(
                                                          fontSize: 16),
                                                    ),
                                                  ),
                                                  const Spacer(),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    child: OutlinedButton.icon(
                                                      style: OutlinedButton
                                                          .styleFrom(
                                                        shape: RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        18.0),
                                                            side: const BorderSide(
                                                                color: Colors
                                                                    .white)),
                                                        side: BorderSide(
                                                          color: M3A6done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                      ),
                                                      onPressed: () {
                                                        if (M3A6done) {
                                                          Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                              builder:
                                                                  ((context) =>
                                                                      const A19()),
                                                            ),
                                                          );
                                                        } else {
                                                          return null;
                                                        }
                                                      },
                                                      icon: Icon(
                                                        M3A6done
                                                            ? MdiIcons.plus
                                                            : MdiIcons.lock,
                                                        color: M3A6done
                                                            ? CustomColors
                                                                .primaryDark
                                                            : CustomColors
                                                                .secondary,
                                                      ),
                                                      label: Text(
                                                        M3A6done
                                                            ? 'Iniciar'
                                                            : 'No disponible',
                                                        textScaler:
                                                            const TextScaler
                                                                .linear(1.0),
                                                        style: TextStyle(
                                                          color: M3A6done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Container(
                                              margin: const EdgeInsets.all(15),
                                              width: 280,
                                              height: kIsWeb
                                                  ? size.height / 2
                                                  : size.height / 2.5,
                                              decoration: BoxDecoration(
                                                color: M3A8done
                                                    ? CustomColors.surface
                                                    : CustomColors
                                                        .purplePrograma,
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                                border: Border.all(
                                                  color: M3A8done
                                                      ? Colors.transparent
                                                      : CustomColors.secondary,
                                                ),
                                              ),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  ClipRRect(
                                                    borderRadius:
                                                        const BorderRadius.only(
                                                            topLeft:
                                                                Radius.circular(
                                                                    20),
                                                            topRight:
                                                                Radius.circular(
                                                                    20)),
                                                    child: Image(
                                                      image: AssetImage(M3A8done
                                                          ? 'assets/images/olas-suaves.png'
                                                          : 'assets/images/locked-actividades.png'),
                                                    ),
                                                  ),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    alignment:
                                                        Alignment.centerLeft,
                                                    width: 280,
                                                    child: Text(
                                                      M3A8done
                                                          ? 'Olas suaves'
                                                          : '',
                                                      textScaler:
                                                          const TextScaler
                                                              .linear(1.0),
                                                      style: const TextStyle(
                                                          fontSize: 16),
                                                    ),
                                                  ),
                                                  const Spacer(),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    child: OutlinedButton.icon(
                                                      style: OutlinedButton
                                                          .styleFrom(
                                                        shape: RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        18.0),
                                                            side: const BorderSide(
                                                                color: Colors
                                                                    .white)),
                                                        side: BorderSide(
                                                          color: M3A8done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                      ),
                                                      onPressed: () {
                                                        if (M3A8done) {
                                                          Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                              builder:
                                                                  ((context) =>
                                                                      const A20()),
                                                            ),
                                                          );
                                                        } else {
                                                          return null;
                                                        }
                                                      },
                                                      icon: Icon(
                                                        M3A8done
                                                            ? MdiIcons.plus
                                                            : MdiIcons.lock,
                                                        color: M3A8done
                                                            ? CustomColors
                                                                .primaryDark
                                                            : CustomColors
                                                                .secondary,
                                                      ),
                                                      label: Text(
                                                        M3A8done
                                                            ? 'Iniciar'
                                                            : 'No disponible',
                                                        textScaler:
                                                            const TextScaler
                                                                .linear(1.0),
                                                        style: TextStyle(
                                                          color: M3A8done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Container(
                                              margin: const EdgeInsets.all(15),
                                              width: 280,
                                              height: kIsWeb
                                                  ? size.height / 2
                                                  : size.height / 2.5,
                                              decoration: BoxDecoration(
                                                color: M4A4done
                                                    ? CustomColors.surface
                                                    : CustomColors
                                                        .purplePrograma,
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                                border: Border.all(
                                                  color: M4A4done
                                                      ? Colors.transparent
                                                      : CustomColors.secondary,
                                                ),
                                              ),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  ClipRRect(
                                                    borderRadius:
                                                        const BorderRadius.only(
                                                            topLeft:
                                                                Radius.circular(
                                                                    20),
                                                            topRight:
                                                                Radius.circular(
                                                                    20)),
                                                    child: Image(
                                                      image: AssetImage(M4A4done
                                                          ? 'assets/images/lluvia-tranquila.png'
                                                          : 'assets/images/locked-actividades.png'),
                                                    ),
                                                  ),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    alignment:
                                                        Alignment.centerLeft,
                                                    width: 280,
                                                    child: Text(
                                                      M4A4done
                                                          ? 'Lluvia tranquila'
                                                          : '',
                                                      textScaler:
                                                          const TextScaler
                                                              .linear(1.0),
                                                      style: const TextStyle(
                                                          fontSize: 16),
                                                    ),
                                                  ),
                                                  const Spacer(),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    child: OutlinedButton.icon(
                                                      style: OutlinedButton
                                                          .styleFrom(
                                                        shape: RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        18.0),
                                                            side: const BorderSide(
                                                                color: Colors
                                                                    .white)),
                                                        side: BorderSide(
                                                          color: M4A4done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                      ),
                                                      onPressed: () {
                                                        if (M4A4done) {
                                                          Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                              builder:
                                                                  ((context) =>
                                                                      const A21()),
                                                            ),
                                                          );
                                                        } else {
                                                          return null;
                                                        }
                                                      },
                                                      icon: Icon(
                                                        M4A4done
                                                            ? MdiIcons.plus
                                                            : MdiIcons.lock,
                                                        color: M4A4done
                                                            ? CustomColors
                                                                .primaryDark
                                                            : CustomColors
                                                                .secondary,
                                                      ),
                                                      label: Text(
                                                        M4A4done
                                                            ? 'Iniciar'
                                                            : 'No disponible',
                                                        textScaler:
                                                            const TextScaler
                                                                .linear(1.0),
                                                        style: TextStyle(
                                                          color: M4A4done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Container(
                                              margin: const EdgeInsets.all(15),
                                              width: 280,
                                              height: kIsWeb
                                                  ? size.height / 2
                                                  : size.height / 2.5,
                                              decoration: BoxDecoration(
                                                color: M4A5done
                                                    ? CustomColors.surface
                                                    : CustomColors
                                                        .purplePrograma,
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                                border: Border.all(
                                                  color: M4A5done
                                                      ? Colors.transparent
                                                      : CustomColors.secondary,
                                                ),
                                              ),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  ClipRRect(
                                                    borderRadius:
                                                        const BorderRadius.only(
                                                            topLeft:
                                                                Radius.circular(
                                                                    20),
                                                            topRight:
                                                                Radius.circular(
                                                                    20)),
                                                    child: Image(
                                                      image: AssetImage(M4A5done
                                                          ? 'assets/images/respiracion-diafragma.png'
                                                          : 'assets/images/locked-actividades.png'),
                                                    ),
                                                  ),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    alignment:
                                                        Alignment.centerLeft,
                                                    width: 280,
                                                    child: Text(
                                                      M4A5done
                                                          ? 'Respiración Diafragmática'
                                                          : '',
                                                      textScaler:
                                                          const TextScaler
                                                              .linear(1.0),
                                                      style: const TextStyle(
                                                          fontSize: 16),
                                                    ),
                                                  ),
                                                  const Spacer(),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    child: OutlinedButton.icon(
                                                      style: OutlinedButton
                                                          .styleFrom(
                                                        shape: RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        18.0),
                                                            side: const BorderSide(
                                                                color: Colors
                                                                    .white)),
                                                        side: BorderSide(
                                                          color: M4A5done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                      ),
                                                      onPressed: () {
                                                        if (M4A5done) {
                                                          Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                              builder:
                                                                  ((context) =>
                                                                      const A22()),
                                                            ),
                                                          );
                                                        } else {
                                                          return null;
                                                        }
                                                      },
                                                      icon: Icon(
                                                        M4A5done
                                                            ? MdiIcons.plus
                                                            : MdiIcons.lock,
                                                        color: M4A5done
                                                            ? CustomColors
                                                                .primaryDark
                                                            : CustomColors
                                                                .secondary,
                                                      ),
                                                      label: Text(
                                                        M4A5done
                                                            ? 'Iniciar'
                                                            : 'No disponible',
                                                        textScaler:
                                                            const TextScaler
                                                                .linear(1.0),
                                                        style: TextStyle(
                                                          color: M4A5done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    //Cuarto Mindful

                                    Container(
                                      margin: const EdgeInsets.only(
                                          top: 20, left: 15, bottom: 5),
                                      width: size.width,
                                      child: const Text(
                                        'Mindfulness',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                          color: CustomColors.surface,
                                          fontSize: 22,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: const EdgeInsets.only(
                                          top: 5,
                                          left: 15,
                                          bottom: 5,
                                          right: 10),
                                      width: size.width,
                                      child: const Text(
                                        'A medida de que avances en tu programa podrás acceder a prácticas y consejos de mindfulness que Vamos Juntxs tiene para ti.',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                          color: CustomColors.surface,
                                          fontSize: 16,
                                        ),
                                      ),
                                    ),
                                    ScrollConfiguration(
                                      behavior: ScrollConfiguration.of(context)
                                          .copyWith(dragDevices: {
                                        PointerDeviceKind.touch,
                                        PointerDeviceKind.mouse,
                                      }),
                                      child: SingleChildScrollView(
                                        scrollDirection: Axis.horizontal,
                                        child: Row(
                                          children: [
                                            Container(
                                              margin: const EdgeInsets.all(15),
                                              width: 280,
                                              height: kIsWeb
                                                  ? size.height / 2
                                                  : size.height / 2.5,
                                              decoration: BoxDecoration(
                                                color: M2A2done
                                                    ? CustomColors.surface
                                                    : CustomColors
                                                        .purplePrograma,
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                                border: Border.all(
                                                  color: M2A2done
                                                      ? Colors.transparent
                                                      : CustomColors.secondary,
                                                ),
                                              ),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  ClipRRect(
                                                    borderRadius:
                                                        const BorderRadius.only(
                                                            topLeft:
                                                                Radius.circular(
                                                                    20),
                                                            topRight:
                                                                Radius.circular(
                                                                    20)),
                                                    child: Image(
                                                      image: AssetImage(M2A2done
                                                          ? 'assets/images/consejo-mindful.png'
                                                          : 'assets/images/locked-actividades.png'),
                                                    ),
                                                  ),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    alignment:
                                                        Alignment.centerLeft,
                                                    width: 280,
                                                    child: Text(
                                                      M2A2done
                                                          ? 'Consejos de mindfulness'
                                                          : '',
                                                      textScaler:
                                                          const TextScaler
                                                              .linear(1.0),
                                                      style: const TextStyle(
                                                          fontSize: 16),
                                                    ),
                                                  ),
                                                  const Spacer(),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    child: OutlinedButton.icon(
                                                      style: OutlinedButton
                                                          .styleFrom(
                                                        shape: RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        18.0),
                                                            side: const BorderSide(
                                                                color: Colors
                                                                    .white)),
                                                        side: BorderSide(
                                                          color: M2A2done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                      ),
                                                      onPressed: () {
                                                        if (M2A2done) {
                                                          Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                              builder:
                                                                  ((context) =>
                                                                      const A23()),
                                                            ),
                                                          );
                                                        } else {
                                                          return null;
                                                        }
                                                      },
                                                      icon: Icon(
                                                        M2A2done
                                                            ? MdiIcons.plus
                                                            : MdiIcons.lock,
                                                        color: M2A2done
                                                            ? CustomColors
                                                                .primaryDark
                                                            : CustomColors
                                                                .secondary,
                                                      ),
                                                      label: Text(
                                                        M2A2done
                                                            ? 'Iniciar'
                                                            : 'No disponible',
                                                        textScaler:
                                                            const TextScaler
                                                                .linear(1.0),
                                                        style: TextStyle(
                                                          color: M2A2done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Container(
                                              margin: const EdgeInsets.all(15),
                                              width: 280,
                                              height: kIsWeb
                                                  ? size.height / 2
                                                  : size.height / 2.5,
                                              decoration: BoxDecoration(
                                                color: M2A5done
                                                    ? CustomColors.surface
                                                    : CustomColors
                                                        .purplePrograma,
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                                border: Border.all(
                                                  color: M2A5done
                                                      ? Colors.transparent
                                                      : CustomColors.secondary,
                                                ),
                                              ),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  ClipRRect(
                                                    borderRadius:
                                                        const BorderRadius.only(
                                                            topLeft:
                                                                Radius.circular(
                                                                    20),
                                                            topRight:
                                                                Radius.circular(
                                                                    20)),
                                                    child: Image(
                                                      image: AssetImage(M2A5done
                                                          ? 'assets/images/practica-mindful.png'
                                                          : 'assets/images/locked-actividades.png'),
                                                    ),
                                                  ),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    alignment:
                                                        Alignment.centerLeft,
                                                    width: 280,
                                                    child: Text(
                                                      M2A5done
                                                          ? 'Práctica de mindfulness'
                                                          : '',
                                                      textScaler:
                                                          const TextScaler
                                                              .linear(1.0),
                                                      style: const TextStyle(
                                                          fontSize: 16),
                                                    ),
                                                  ),
                                                  const Spacer(),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    child: OutlinedButton.icon(
                                                      style: OutlinedButton
                                                          .styleFrom(
                                                        shape: RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        18.0),
                                                            side: const BorderSide(
                                                                color: Colors
                                                                    .white)),
                                                        side: BorderSide(
                                                          color: M2A5done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                      ),
                                                      onPressed: () {
                                                        if (M2A5done) {
                                                          Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                              builder:
                                                                  ((context) =>
                                                                      const A24()),
                                                            ),
                                                          );
                                                        } else {
                                                          return null;
                                                        }
                                                      },
                                                      icon: Icon(
                                                        M2A5done
                                                            ? MdiIcons.plus
                                                            : MdiIcons.lock,
                                                        color: M2A5done
                                                            ? CustomColors
                                                                .primaryDark
                                                            : CustomColors
                                                                .secondary,
                                                      ),
                                                      label: Text(
                                                        M2A5done
                                                            ? 'Iniciar'
                                                            : 'No disponible',
                                                        textScaler:
                                                            const TextScaler
                                                                .linear(1.0),
                                                        style: TextStyle(
                                                          color: M2A5done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Container(
                                              margin: const EdgeInsets.all(15),
                                              width: 280,
                                              height: kIsWeb
                                                  ? size.height / 2
                                                  : size.height / 2.5,
                                              decoration: BoxDecoration(
                                                color: M4A2done
                                                    ? CustomColors.surface
                                                    : CustomColors
                                                        .purplePrograma,
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                                border: Border.all(
                                                  color: M4A2done
                                                      ? Colors.transparent
                                                      : CustomColors.secondary,
                                                ),
                                              ),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  ClipRRect(
                                                    borderRadius:
                                                        const BorderRadius.only(
                                                            topLeft:
                                                                Radius.circular(
                                                                    20),
                                                            topRight:
                                                                Radius.circular(
                                                                    20)),
                                                    child: Image(
                                                      image: AssetImage(M4A2done
                                                          ? 'assets/images/practica-aware.png'
                                                          : 'assets/images/locked-actividades.png'),
                                                    ),
                                                  ),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    alignment:
                                                        Alignment.centerLeft,
                                                    width: 280,
                                                    child: Text(
                                                      M4A2done
                                                          ? 'Práctica Aware'
                                                          : '',
                                                      textScaler:
                                                          const TextScaler
                                                              .linear(1.0),
                                                      style: const TextStyle(
                                                          fontSize: 16),
                                                    ),
                                                  ),
                                                  const Spacer(),
                                                  Container(
                                                    margin:
                                                        const EdgeInsets.all(
                                                            15),
                                                    child: OutlinedButton.icon(
                                                      style: OutlinedButton
                                                          .styleFrom(
                                                        shape: RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        18.0),
                                                            side: const BorderSide(
                                                                color: Colors
                                                                    .white)),
                                                        side: BorderSide(
                                                          color: M4A2done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                      ),
                                                      onPressed: () {
                                                        if (M4A2done) {
                                                          Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                              builder:
                                                                  ((context) =>
                                                                      const A25()),
                                                            ),
                                                          );
                                                        } else {
                                                          return null;
                                                        }
                                                      },
                                                      icon: Icon(
                                                        M4A2done
                                                            ? MdiIcons.plus
                                                            : MdiIcons.lock,
                                                        color: M4A2done
                                                            ? CustomColors
                                                                .primaryDark
                                                            : CustomColors
                                                                .secondary,
                                                      ),
                                                      label: Text(
                                                        M4A2done
                                                            ? 'Iniciar'
                                                            : 'No disponible',
                                                        textScaler:
                                                            const TextScaler
                                                                .linear(1.0),
                                                        style: TextStyle(
                                                          color: M4A2done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    //Quinto Aprende mas
                                    Container(
                                      margin: const EdgeInsets.only(
                                          top: 20, left: 15, bottom: 5),
                                      width: size.width,
                                      child: const Text(
                                        'Aprende más de salud mental',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 22,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: const EdgeInsets.only(
                                          top: 5,
                                          left: 15,
                                          bottom: 5,
                                          right: 10),
                                      width: size.width,
                                      child: const Text(
                                        'En esta sección podrás encontrar una serie de videos y recursos respecto a diversas temáticas relacionadas a la salud mental. A medida que avances en tu programa podrás ir desbloqueando nuevos contenidos.',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                          color: CustomColors.surface,
                                          fontSize: 16,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: const EdgeInsets.only(bottom: 70),
                                      child: ScrollConfiguration(
                                        behavior:
                                            ScrollConfiguration.of(context)
                                                .copyWith(dragDevices: {
                                          PointerDeviceKind.touch,
                                          PointerDeviceKind.mouse,
                                        }),
                                        child: SingleChildScrollView(
                                          scrollDirection: Axis.horizontal,
                                          child: Row(
                                            children: [
                                              Container(
                                                margin:
                                                    const EdgeInsets.all(15),
                                                width: 280,
                                                height: kIsWeb
                                                    ? size.height / 2
                                                    : size.height / 2.5,
                                                decoration: BoxDecoration(
                                                  color: M1A3done
                                                      ? CustomColors.surface
                                                      : CustomColors
                                                          .purplePrograma,
                                                  borderRadius:
                                                      BorderRadius.circular(20),
                                                  border: Border.all(
                                                    color: M1A3done
                                                        ? Colors.transparent
                                                        : CustomColors
                                                            .secondary,
                                                  ),
                                                ),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: [
                                                    ClipRRect(
                                                      borderRadius:
                                                          const BorderRadius
                                                              .only(
                                                              topLeft: Radius
                                                                  .circular(20),
                                                              topRight: Radius
                                                                  .circular(
                                                                      20)),
                                                      child: Image(
                                                        image: AssetImage(M1A3done
                                                            ? 'assets/images/busqueda-ayuda.png'
                                                            : 'assets/images/locked-actividades.png'),
                                                      ),
                                                    ),
                                                    Container(
                                                      margin:
                                                          const EdgeInsets.all(
                                                              15),
                                                      alignment:
                                                          Alignment.centerLeft,
                                                      width: 280,
                                                      child: Text(
                                                        M1A3done
                                                            ? 'Búsqueda de ayuda: Primera consulta en salud mental'
                                                            : '',
                                                        textScaler:
                                                            const TextScaler
                                                                .linear(1.0),
                                                        style: const TextStyle(
                                                            fontSize: 16),
                                                      ),
                                                    ),
                                                    const Spacer(),
                                                    Container(
                                                      margin:
                                                          const EdgeInsets.all(
                                                              15),
                                                      child:
                                                          OutlinedButton.icon(
                                                        style: OutlinedButton
                                                            .styleFrom(
                                                          shape: RoundedRectangleBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          18.0),
                                                              side: const BorderSide(
                                                                  color: Colors
                                                                      .white)),
                                                          side: BorderSide(
                                                            color: M1A3done
                                                                ? CustomColors
                                                                    .primaryDark
                                                                : CustomColors
                                                                    .secondary,
                                                          ),
                                                        ),
                                                        onPressed: () {
                                                          if (M1A3done) {
                                                            Navigator.push(
                                                              context,
                                                              MaterialPageRoute(
                                                                builder:
                                                                    ((context) =>
                                                                        const A26()),
                                                              ),
                                                            );
                                                          } else {
                                                            return null;
                                                          }
                                                        },
                                                        icon: Icon(
                                                          M1A3done
                                                              ? MdiIcons.plus
                                                              : MdiIcons.lock,
                                                          color: M1A3done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                        label: Text(
                                                          M1A3done
                                                              ? 'Iniciar'
                                                              : 'No disponible',
                                                          textScaler:
                                                              const TextScaler
                                                                  .linear(1.0),
                                                          style: TextStyle(
                                                            color: M1A3done
                                                                ? CustomColors
                                                                    .primaryDark
                                                                : CustomColors
                                                                    .secondary,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Container(
                                                margin:
                                                    const EdgeInsets.all(15),
                                                width: 280,
                                                height: kIsWeb
                                                    ? size.height / 2
                                                    : size.height / 2.5,
                                                decoration: BoxDecoration(
                                                  color: M2A1done
                                                      ? CustomColors.surface
                                                      : CustomColors
                                                          .purplePrograma,
                                                  borderRadius:
                                                      BorderRadius.circular(20),
                                                  border: Border.all(
                                                    color: M2A1done
                                                        ? Colors.transparent
                                                        : CustomColors
                                                            .secondary,
                                                  ),
                                                ),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: [
                                                    ClipRRect(
                                                      borderRadius:
                                                          const BorderRadius
                                                              .only(
                                                              topLeft: Radius
                                                                  .circular(20),
                                                              topRight: Radius
                                                                  .circular(
                                                                      20)),
                                                      child: Image(
                                                        image: AssetImage(M2A1done
                                                            ? 'assets/images/prevencion-suicidio.png'
                                                            : 'assets/images/locked-actividades.png'),
                                                      ),
                                                    ),
                                                    Container(
                                                      margin:
                                                          const EdgeInsets.all(
                                                              15),
                                                      alignment:
                                                          Alignment.centerLeft,
                                                      width: 280,
                                                      child: Text(
                                                        M2A1done
                                                            ? 'Video Hablemos de prevención del suicidio'
                                                            : '',
                                                        textScaler:
                                                            const TextScaler
                                                                .linear(1.0),
                                                        style: const TextStyle(
                                                            fontSize: 16),
                                                      ),
                                                    ),
                                                    const Spacer(),
                                                    Container(
                                                      margin:
                                                          const EdgeInsets.all(
                                                              15),
                                                      child:
                                                          OutlinedButton.icon(
                                                        style: OutlinedButton
                                                            .styleFrom(
                                                          shape: RoundedRectangleBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          18.0),
                                                              side: const BorderSide(
                                                                  color: Colors
                                                                      .white)),
                                                          side: BorderSide(
                                                            color: M2A1done
                                                                ? CustomColors
                                                                    .primaryDark
                                                                : CustomColors
                                                                    .secondary,
                                                          ),
                                                        ),
                                                        onPressed: () {
                                                          if (M2A1done) {
                                                            Navigator.push(
                                                              context,
                                                              MaterialPageRoute(
                                                                builder:
                                                                    ((context) =>
                                                                        const A27()),
                                                              ),
                                                            );
                                                          } else {
                                                            return null;
                                                          }
                                                        },
                                                        icon: Icon(
                                                          M2A1done
                                                              ? MdiIcons.plus
                                                              : MdiIcons.lock,
                                                          color: M2A1done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                        label: Text(
                                                          M2A1done
                                                              ? 'Iniciar'
                                                              : 'No disponible',
                                                          textScaler:
                                                              const TextScaler
                                                                  .linear(1.0),
                                                          style: TextStyle(
                                                            color: M2A1done
                                                                ? CustomColors
                                                                    .primaryDark
                                                                : CustomColors
                                                                    .secondary,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Container(
                                                margin:
                                                    const EdgeInsets.all(15),
                                                width: 280,
                                                height: kIsWeb
                                                    ? size.height / 2
                                                    : size.height / 2.5,
                                                decoration: BoxDecoration(
                                                  color: M2A2done
                                                      ? CustomColors.surface
                                                      : CustomColors
                                                          .purplePrograma,
                                                  borderRadius:
                                                      BorderRadius.circular(20),
                                                  border: Border.all(
                                                    color: M2A2done
                                                        ? Colors.transparent
                                                        : CustomColors
                                                            .secondary,
                                                  ),
                                                ),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: [
                                                    ClipRRect(
                                                      borderRadius:
                                                          const BorderRadius
                                                              .only(
                                                              topLeft: Radius
                                                                  .circular(20),
                                                              topRight: Radius
                                                                  .circular(
                                                                      20)),
                                                      child: Image(
                                                        image: AssetImage(M2A2done
                                                            ? 'assets/images/hablemos-bullying.png'
                                                            : 'assets/images/locked-actividades.png'),
                                                      ),
                                                    ),
                                                    Container(
                                                      margin:
                                                          const EdgeInsets.all(
                                                              15),
                                                      alignment:
                                                          Alignment.centerLeft,
                                                      width: 280,
                                                      child: Text(
                                                        M2A2done
                                                            ? 'Video Hablemos de bullying'
                                                            : '',
                                                        textScaler:
                                                            const TextScaler
                                                                .linear(1.0),
                                                        style: const TextStyle(
                                                            fontSize: 16),
                                                      ),
                                                    ),
                                                    const Spacer(),
                                                    Container(
                                                      margin:
                                                          const EdgeInsets.all(
                                                              15),
                                                      child:
                                                          OutlinedButton.icon(
                                                        style: OutlinedButton
                                                            .styleFrom(
                                                          shape: RoundedRectangleBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          18.0),
                                                              side: const BorderSide(
                                                                  color: Colors
                                                                      .white)),
                                                          side: BorderSide(
                                                            color: M2A2done
                                                                ? CustomColors
                                                                    .primaryDark
                                                                : CustomColors
                                                                    .secondary,
                                                          ),
                                                        ),
                                                        onPressed: () {
                                                          if (M2A2done) {
                                                            Navigator.push(
                                                              context,
                                                              MaterialPageRoute(
                                                                builder:
                                                                    ((context) =>
                                                                        const A28()),
                                                              ),
                                                            );
                                                          } else {
                                                            return null;
                                                          }
                                                        },
                                                        icon: Icon(
                                                          M2A2done
                                                              ? MdiIcons.plus
                                                              : MdiIcons.lock,
                                                          color: M2A2done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                        label: Text(
                                                          M2A2done
                                                              ? 'Iniciar'
                                                              : 'No disponible',
                                                          textScaler:
                                                              const TextScaler
                                                                  .linear(1.0),
                                                          style: TextStyle(
                                                            color: M2A2done
                                                                ? CustomColors
                                                                    .primaryDark
                                                                : CustomColors
                                                                    .secondary,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Container(
                                                margin:
                                                    const EdgeInsets.all(15),
                                                width: 280,
                                                height: kIsWeb
                                                    ? size.height / 2
                                                    : size.height / 2.5,
                                                decoration: BoxDecoration(
                                                  color: M2A3done
                                                      ? CustomColors.surface
                                                      : CustomColors
                                                          .purplePrograma,
                                                  borderRadius:
                                                      BorderRadius.circular(20),
                                                  border: Border.all(
                                                    color: M2A3done
                                                        ? Colors.transparent
                                                        : CustomColors
                                                            .secondary,
                                                  ),
                                                ),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: [
                                                    ClipRRect(
                                                      borderRadius:
                                                          const BorderRadius
                                                              .only(
                                                              topLeft: Radius
                                                                  .circular(20),
                                                              topRight: Radius
                                                                  .circular(
                                                                      20)),
                                                      child: Image(
                                                        image: AssetImage(M2A3done
                                                            ? 'assets/images/hablemos-ciberbullying.png'
                                                            : 'assets/images/locked-actividades.png'),
                                                      ),
                                                    ),
                                                    Container(
                                                      margin:
                                                          const EdgeInsets.all(
                                                              15),
                                                      alignment:
                                                          Alignment.centerLeft,
                                                      width: 280,
                                                      child: Text(
                                                        M2A3done
                                                            ? 'Video Hablemos de cyberbullying'
                                                            : '',
                                                        textScaler:
                                                            const TextScaler
                                                                .linear(1.0),
                                                        style: const TextStyle(
                                                            fontSize: 16),
                                                      ),
                                                    ),
                                                    const Spacer(),
                                                    Container(
                                                      margin:
                                                          const EdgeInsets.all(
                                                              15),
                                                      child:
                                                          OutlinedButton.icon(
                                                        style: OutlinedButton
                                                            .styleFrom(
                                                          shape: RoundedRectangleBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          18.0),
                                                              side: const BorderSide(
                                                                  color: Colors
                                                                      .white)),
                                                          side: BorderSide(
                                                            color: M2A3done
                                                                ? CustomColors
                                                                    .primaryDark
                                                                : CustomColors
                                                                    .secondary,
                                                          ),
                                                        ),
                                                        onPressed: () {
                                                          if (M2A3done) {
                                                            Navigator.push(
                                                              context,
                                                              MaterialPageRoute(
                                                                builder:
                                                                    ((context) =>
                                                                        const A29()),
                                                              ),
                                                            );
                                                          } else {
                                                            return null;
                                                          }
                                                        },
                                                        icon: Icon(
                                                          M2A3done
                                                              ? MdiIcons.plus
                                                              : MdiIcons.lock,
                                                          color: M2A3done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                        label: Text(
                                                          M2A3done
                                                              ? 'Iniciar'
                                                              : 'No disponible',
                                                          textScaler:
                                                              const TextScaler
                                                                  .linear(1.0),
                                                          style: TextStyle(
                                                            color: M2A3done
                                                                ? CustomColors
                                                                    .primaryDark
                                                                : CustomColors
                                                                    .secondary,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Container(
                                                margin:
                                                    const EdgeInsets.all(15),
                                                width: 280,
                                                height: kIsWeb
                                                    ? size.height / 2
                                                    : size.height / 2.5,
                                                decoration: BoxDecoration(
                                                  color: M2A4done
                                                      ? CustomColors.surface
                                                      : CustomColors
                                                          .purplePrograma,
                                                  borderRadius:
                                                      BorderRadius.circular(20),
                                                  border: Border.all(
                                                    color: M2A4done
                                                        ? Colors.transparent
                                                        : CustomColors
                                                            .secondary,
                                                  ),
                                                ),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: [
                                                    ClipRRect(
                                                      borderRadius:
                                                          const BorderRadius
                                                              .only(
                                                              topLeft: Radius
                                                                  .circular(20),
                                                              topRight: Radius
                                                                  .circular(
                                                                      20)),
                                                      child: Image(
                                                        image: AssetImage(M2A4done
                                                            ? 'assets/images/manejo-crisis.png'
                                                            : 'assets/images/locked-actividades.png'),
                                                      ),
                                                    ),
                                                    Container(
                                                      margin:
                                                          const EdgeInsets.all(
                                                              15),
                                                      alignment:
                                                          Alignment.centerLeft,
                                                      width: 280,
                                                      child: Text(
                                                        M2A4done
                                                            ? 'Video Manejo de crisis en salud mental'
                                                            : '',
                                                        textScaler:
                                                            const TextScaler
                                                                .linear(1.0),
                                                        style: const TextStyle(
                                                            fontSize: 16),
                                                      ),
                                                    ),
                                                    const Spacer(),
                                                    Container(
                                                      margin:
                                                          const EdgeInsets.all(
                                                              15),
                                                      child:
                                                          OutlinedButton.icon(
                                                        style: OutlinedButton
                                                            .styleFrom(
                                                          shape: RoundedRectangleBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          18.0),
                                                              side: const BorderSide(
                                                                  color: Colors
                                                                      .white)),
                                                          side: BorderSide(
                                                            color: M2A4done
                                                                ? CustomColors
                                                                    .primaryDark
                                                                : CustomColors
                                                                    .secondary,
                                                          ),
                                                        ),
                                                        onPressed: () {
                                                          if (M2A4done) {
                                                            Navigator.push(
                                                              context,
                                                              MaterialPageRoute(
                                                                builder:
                                                                    ((context) =>
                                                                        const A30()),
                                                              ),
                                                            );
                                                          } else {
                                                            return null;
                                                          }
                                                        },
                                                        icon: Icon(
                                                          M2A4done
                                                              ? MdiIcons.plus
                                                              : MdiIcons.lock,
                                                          color: M2A4done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                        label: Text(
                                                          M2A4done
                                                              ? 'Iniciar'
                                                              : 'No disponible',
                                                          textScaler:
                                                              const TextScaler
                                                                  .linear(1.0),
                                                          style: TextStyle(
                                                            color: M2A4done
                                                                ? CustomColors
                                                                    .primaryDark
                                                                : CustomColors
                                                                    .secondary,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Container(
                                                margin:
                                                    const EdgeInsets.all(15),
                                                width: 280,
                                                height: kIsWeb
                                                    ? size.height / 2
                                                    : size.height / 2.5,
                                                decoration: BoxDecoration(
                                                  color: M2A6done
                                                      ? CustomColors.surface
                                                      : CustomColors
                                                          .purplePrograma,
                                                  borderRadius:
                                                      BorderRadius.circular(20),
                                                  border: Border.all(
                                                    color: M2A6done
                                                        ? Colors.transparent
                                                        : CustomColors
                                                            .secondary,
                                                  ),
                                                ),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.end,
                                                  children: [
                                                    ClipRRect(
                                                      borderRadius:
                                                          const BorderRadius
                                                              .only(
                                                              topLeft: Radius
                                                                  .circular(20),
                                                              topRight: Radius
                                                                  .circular(
                                                                      20)),
                                                      child: Image(
                                                        image: AssetImage(M2A6done
                                                            ? 'assets/images/consumo-universitario.png'
                                                            : 'assets/images/locked-actividades.png'),
                                                      ),
                                                    ),
                                                    Container(
                                                      margin:
                                                          const EdgeInsets.all(
                                                              15),
                                                      alignment:
                                                          Alignment.centerLeft,
                                                      width: 280,
                                                      child: Text(
                                                        M2A6done
                                                            ? 'Consumo de alcohol y drogas en el contexto universitario'
                                                            : '',
                                                        textScaler:
                                                            const TextScaler
                                                                .linear(1.0),
                                                        style: const TextStyle(
                                                            fontSize: 16),
                                                      ),
                                                    ),
                                                    const Spacer(),
                                                    Container(
                                                      margin:
                                                          const EdgeInsets.all(
                                                              15),
                                                      child:
                                                          OutlinedButton.icon(
                                                        style: OutlinedButton
                                                            .styleFrom(
                                                          shape: RoundedRectangleBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          18.0),
                                                              side: const BorderSide(
                                                                  color: Colors
                                                                      .white)),
                                                          side: BorderSide(
                                                            color: M2A6done
                                                                ? CustomColors
                                                                    .primaryDark
                                                                : CustomColors
                                                                    .secondary,
                                                          ),
                                                        ),
                                                        onPressed: () {
                                                          if (M2A6done) {
                                                            Navigator.push(
                                                              context,
                                                              MaterialPageRoute(
                                                                builder:
                                                                    ((context) =>
                                                                        const A31()),
                                                              ),
                                                            );
                                                          } else {
                                                            return null;
                                                          }
                                                        },
                                                        icon: Icon(
                                                          M2A6done
                                                              ? MdiIcons.plus
                                                              : MdiIcons.lock,
                                                          color: M2A6done
                                                              ? CustomColors
                                                                  .primaryDark
                                                              : CustomColors
                                                                  .secondary,
                                                        ),
                                                        label: Text(
                                                          M2A6done
                                                              ? 'Iniciar'
                                                              : 'No disponible',
                                                          textScaler:
                                                              const TextScaler
                                                                  .linear(1.0),
                                                          style: TextStyle(
                                                            color: M2A6done
                                                                ? CustomColors
                                                                    .primaryDark
                                                                : CustomColors
                                                                    .secondary,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            } else {
                              return Container();
                            }
                          }
                        }
                        return null;
                      },
                    );
                  } else {
                    return const Center(
                      child: Text(
                        "ERROR",
                        textScaler: TextScaler.linear(1.0),
                      ),
                    );
                  }
                } else {
                  return const Center(
                    child: CircularProgressIndicator(
                      color: Colors.red,
                    ),
                  );
                }
              }),
        ),
      ],
    );
  }

  Widget termometro() {
    return const Termometro(modulo: 'P');
  }
}

Widget buscaAyuda() {
  return const PideAyuda();
}
