// ignore_for_file: unused_local_variable, prefer_typing_uninitialized_variables

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:seguimiento/colors.dart';
import 'package:seguimiento/notifications_cloud.dart';
import 'package:seguimiento/user_pages/historial-animo.dart';
import 'package:seguimiento/user_pages/perfil.dart';
import '../../firebase.dart';
import 'MainPage.dart';

class Termometro extends StatefulWidget {
  const Termometro({
    Key? key,
    required this.modulo,
  }) : super(key: key);
  final String modulo;
  @override
  State<Termometro> createState() => _TermometroState();
}

class _TermometroState extends State<Termometro> {
  String uid = FirebaseAuth.instance.currentUser!.uid;

  bool favorite = false;
  CollectionReference favoriteData =
      FirebaseFirestore.instance.collection('favoritos');
  void getFavData() {
    favoriteData.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        favorite = fields['M' + widget.modulo + 'AT'];
      });
    }).catchError((err) {
      //print('Error: $err');
    });
  }

  ScrollController scrollController = ScrollController();

  int activeStep = 0;
  var finaltermoValue;
  double _currentSliderValue = 5;
  var timeStart;
  @override
  void initState() {
    super.initState();

    getFavData();

    var tiempo = DateTime.now();
    timeStart = tiempo;
    FirestoreService().setTime(
        tiempo.toString() + ' - START - ' + 'M' + widget.modulo + 'Termometro',
        'Comenzó Termometro M' + widget.modulo,
        '0 minutos (comenzando)');
  }

  @override
  void dispose() {
    super.dispose();
  }

  bool active1 = false;
  bool active2 = false;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        active1 = true;
        active2 = false;
        break;
      case 1:
        active1 = true;
        active2 = true;
        break;

      default:
    }
    switch (widget.modulo) {
      case 'P':
        return Scaffold(
          backgroundColor: CustomColors.primary3,
          body: Center(
            child: Column(
              children: [
                content(),
              ],
            ),
          ),
        );
      default:
        return PopScope(
          canPop: false,
          onPopInvokedWithResult: (bool didPop, result) {
            if (didPop) {
              return;
            }
            showDialog(
              context: context,
              builder: (context) => AlertDialog(
                shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(32.0))),
                title: const Text(
                  '¿Estás seguro que deseas salir de la actividad?',
                  textScaler: TextScaler.linear(1.0),
                  style: TextStyle(color: CustomColors.primaryDark),
                ),
                content: const Text(
                  'Tus avances quedarán guardados',
                  textScaler: TextScaler.linear(1.0),
                  style: TextStyle(color: CustomColors.primaryDark),
                ),
                actions: [
                  TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text(
                      'Continuar con la actividad',
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(color: CustomColors.primary),
                    ),
                  ),
                  TextButton(
                    onPressed: () {
                      /*
                  FirestoreService().updateCurrentPage(
                      'Termometro', (activeStep + 1).toString());*/
                      Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute<void>(
                              builder: (BuildContext context) => const MainPage(
                                    activePage: 0,
                                  )),
                          (route) => false);
                    },
                    child: const Text(
                      'Salir',
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(color: CustomColors.error),
                    ),
                  ),
                ],
              ),
            );
          },
          child: Container(
            color: Colors.black,
            child: SafeArea(
              child: Scaffold(
                backgroundColor:
                    active2 ? CustomColors.greenEnd : CustomColors.primary3,
                body: Center(
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Container(
                            width: size.width / 2,
                            height: 5,
                            color:
                                active1 ? CustomColors.primary3 : Colors.white,
                            child: const Text(''),
                          ),
                          Container(
                            width: size.width / 2,
                            height: 5,
                            color:
                                active2 ? CustomColors.primary3 : Colors.white,
                            child: const Text(''),
                          ),
                        ],
                      ),
                      content(),
                      Container(
                        margin: const EdgeInsets.only(top: 10, bottom: 10),
                        child: bottomStructure(),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
    }
  }

  Widget topStructure() {
    Size size = MediaQuery.of(context).size;
    switch (widget.modulo) {
      case 'P':
        return Stack(
          alignment: Alignment.topRight,
          children: [
            const Image(
              image: AssetImage('assets/images/termometro-banner.png'),
            ),
            IconButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const TuPerfil(
                      typeOfUser: 'U',
                    ),
                  ),
                );
              },
              icon: Icon(
                MdiIcons.accountCircleOutline,
                color: CustomColors.primaryDark,
              ),
            ),
          ],
        );

      default:
        return Container(
          width: size.width,
          height: active2 ? null : size.height / 3,
          color: CustomColors.greenList,
          child: Column(
            children: [
              Row(
                children: [
                  active2
                      ? Container()
                      : Container(
                          alignment: Alignment.topRight,
                          child: IconButton(
                            onPressed: () {
                              showDialog(
                                context: context,
                                builder: (context) => AlertDialog(
                                  shape: const RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(32.0))),
                                  title: const Text(
                                    '¿Estás seguro que deseas salir de la actividad?',
                                    textScaler: TextScaler.linear(1.0),
                                    style: TextStyle(
                                        color: CustomColors.primaryDark),
                                  ),
                                  content: const Text(
                                    'Tus avances quedarán guardados',
                                    textScaler: TextScaler.linear(1.0),
                                    style: TextStyle(
                                        color: CustomColors.primaryDark),
                                  ),
                                  actions: [
                                    TextButton(
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      },
                                      child: const Text(
                                        'Continuar con la actividad',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            color: CustomColors.primary),
                                      ),
                                    ),
                                    TextButton(
                                      onPressed: () {
                                        String tiempo =
                                            DateTime.now().toString();
                                        FirestoreService().setTime(
                                            tiempo +
                                                ' - CLOSED - ' +
                                                'M' +
                                                widget.modulo +
                                                'Termometro',
                                            'Salió de Termometro M' +
                                                widget.modulo +
                                                ' sin terminar',
                                            '0 minutos (sin terminar)');
                                        /*FirestoreService().updateCurrentPage(
                                        'Termometro',
                                        (activeStep + 1).toString());*/
                                        Navigator.pushAndRemoveUntil(
                                            context,
                                            MaterialPageRoute<void>(
                                                builder:
                                                    (BuildContext context) =>
                                                        const MainPage(
                                                          activePage: 0,
                                                        )),
                                            (route) => false);
                                      },
                                      child: const Text(
                                        'Salir',
                                        textScaler: TextScaler.linear(1.0),
                                        style: TextStyle(
                                            color: CustomColors.error),
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            },
                            icon: const Icon(
                              Icons.close,
                              color: CustomColors.primaryDark,
                            ),
                          ),
                        ),
                  active2 ? Container() : favorito(),
                ],
              ),
              Text(
                headerText(),
                textScaler: const TextScaler.linear(1.0),
                textAlign: TextAlign.center,
                style: const TextStyle(
                  color: CustomColors.primaryDark,
                  fontSize: 24,
                ),
              ),
              active2 ? Container() : const Spacer(),
              header(),
            ],
          ),
        );
    }
  }

  Widget favorito() {
    return StreamBuilder<QuerySnapshot>(
        stream: FirestoreService().favoritos(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasData) {
            return SizedBox(
              width: 50,
              height: 50,
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (context, index) {
                    var document = snapshot.data?.docs[index];
                    if (document != null && document.id == uid) {
                      switch (document['M' + widget.modulo + 'AT']) {
                        case true:
                          return IconButton(
                              icon: Icon(MdiIcons.star,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito(
                                        'M' + widget.modulo + 'AT', false)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Eliminado de favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                        case false:
                          return IconButton(
                              icon: Icon(MdiIcons.starOutline,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito(
                                        'M' + widget.modulo + 'AT', true)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Agregado a favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                      }
                    } else {
                      return Container();
                    }
                    return null;
                  }),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(
                color: Colors.red,
              ),
            );
          }
        });
  }

  Widget bottomStructure() {
    Size size = MediaQuery.of(context).size;

    switch (widget.modulo) {
      case 'P':
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SizedBox(
              width: size.width / 2.2,
              child: ElevatedButton(
                style: ButtonStyle(
                  overlayColor:
                      WidgetStateProperty.all<Color>(CustomColors.surface),
                  backgroundColor:
                      WidgetStateProperty.all<Color>(Colors.transparent),
                  shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                        side: const BorderSide(color: Colors.white)),
                  ),
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: ((context) => const HistorialAnimo()),
                    ),
                  );
                },
                child: const Text(
                  'Historial del ánimo',
                  textScaler: TextScaler.linear(1.0),
                  style: TextStyle(
                    color: CustomColors.surface,
                  ),
                ),
              ),
            ),
            SizedBox(
              width: size.width / 2.2,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: WidgetStateProperty.all<Color>(active2
                      ? CustomColors.primary
                      : CustomColors.greenButton),
                  shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ),
                  ),
                ),
                onPressed: () async {
                  FirestoreService().terminaSesion(
                      widget.modulo + 'ágina principal',
                      'T',
                      'Marcó ' +
                          _currentSliderValue.round().toString() +
                          '/10 en el termómetro');
                  var tiempo = DateTime.now();
                  var difTiempo = tiempo.difference(timeStart);
                  FirestoreService().setTime(
                      tiempo.toString() +
                          ' - ENDED - ' +
                          'Página' +
                          widget.modulo +
                          'rincipal' +
                          'Termometro',
                      'Terminó Termometro Página principal',
                      difTiempo.inMinutes.toString() + ' minutos');

                  FirestoreService().setTermometro(
                      widget.modulo, _currentSliderValue.round());
                  FirestoreService().updateLastTermometroValue(
                      uid, _currentSliderValue.round());
                  final response = await NotificationService()
                      .sendAnswersTermometro(uid, _currentSliderValue.round())
                      .catchError((err) {});

                  showDialog(
                    context: context,
                    builder: (context) => AlertDialog(
                      shape: const RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.all(Radius.circular(32.0))),
                      title: const Text(
                        '!Estado guardado!',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(color: CustomColors.primaryDark),
                      ),
                      content: const Text(
                        'Tu estado de ánimo ha sido guardado correctamente.',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(color: CustomColors.primaryDark),
                      ),
                      actions: [
                        TextButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: const Text(
                            'Cerrar',
                            textScaler: TextScaler.linear(1.0),
                            style: TextStyle(color: CustomColors.primary),
                          ),
                        ),
                      ],
                    ),
                  );
                },
                child: const Text(
                  'Guardar tu ánimo',
                  textScaler: TextScaler.linear(1.0),
                  style: TextStyle(
                    color: CustomColors.primaryDark,
                  ),
                ),
              ),
            ),
          ],
        );
      default:
        return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            nextButton(),
          ],
        );
    }
  }

  Widget nextButton() {
    Size size = MediaQuery.of(context).size;
    if (activeStep == 1) {
      return SizedBox(
        width: size.width / 1.2,
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor: WidgetStateProperty.all<Color>(
                active2 ? CustomColors.primary : CustomColors.greenButton),
            shape: WidgetStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              ),
            ),
          ),
          onPressed: () async {
            if (activeStep < 1) {
              setState(() {
                activeStep++;
              });
            } else {
              FirestoreService().terminaSesion(
                  widget.modulo,
                  'T',
                  'Leyó la guía del ejercicio → Marcó ' +
                      _currentSliderValue.round().toString() +
                      '/10 en el termómetro → Terminó la actividad de termómetro');
              var tiempo = DateTime.now();
              var difTiempo = tiempo.difference(timeStart);
              FirestoreService().setTime(
                  tiempo.toString() +
                      ' - ENDED - ' +
                      'M' +
                      widget.modulo +
                      'Termometro',
                  'Terminó Termometro M' + widget.modulo,
                  difTiempo.inMinutes.toString() + ' minutos');

              FirestoreService()
                  .setTermometro(widget.modulo, _currentSliderValue.round());
              FirestoreService()
                  .updateLastTermometroValue(uid, _currentSliderValue.round());
              final response = await NotificationService()
                  .sendAnswersTermometro(uid, _currentSliderValue.round())
                  .catchError((err) {
                setState(() {
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute<void>(
                          builder: (BuildContext context) => const MainPage(
                                activePage: 0,
                              )),
                      (route) => false);
                });
              });

              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute<void>(
                      builder: (BuildContext context) => const MainPage(
                            activePage: 0,
                          )),
                  (route) => false);
            }
          },
          child: Text(
            buttonText(),
            textScaler: const TextScaler.linear(1.0),
            style: TextStyle(
                color:
                    active2 ? CustomColors.surface : CustomColors.primaryDark,
                fontWeight: FontWeight.bold),
          ),
        ),
      );
    } else {
      return SizedBox(
        width: size.width / 1.2,
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor: WidgetStateProperty.all<Color>(
                active2 ? CustomColors.primary : CustomColors.greenButton),
            shape: WidgetStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              ),
            ),
          ),
          onPressed: () async {
            if (activeStep < 1) {
              setState(() {
                activeStep++;
              });
            } else {
              FirestoreService().terminaSesion(
                  widget.modulo,
                  'T',
                  'Leyó la guía del ejercicio → Marcó ' +
                      _currentSliderValue.round().toString() +
                      '/10 en el termómetro → Terminó la actividad 6');
              var tiempo = DateTime.now();
              var difTiempo = tiempo.difference(timeStart);
              FirestoreService().setTime(
                  tiempo.toString() +
                      ' - ENDED - ' +
                      'M' +
                      widget.modulo +
                      'Termometro',
                  'Terminó Termometro M' + widget.modulo,
                  difTiempo.inMinutes.toString() + ' minutos');

              FirestoreService()
                  .setTermometro(widget.modulo, _currentSliderValue.round());
              FirestoreService()
                  .updateLastTermometroValue(uid, _currentSliderValue.round());

              final response = await NotificationService()
                  .sendAnswersTermometro(uid, _currentSliderValue.round())
                  .catchError((err) {
                setState(() {
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute<void>(
                          builder: (BuildContext context) => const MainPage(
                                activePage: 0,
                              )),
                      (route) => false);
                });
              });

              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute<void>(
                      builder: (BuildContext context) => const MainPage(
                            activePage: 0,
                          )),
                  (route) => false);
            }
          },
          child: Text(
            buttonText(),
            textScaler: const TextScaler.linear(1.0),
            style: TextStyle(
              color: active2 ? CustomColors.surface : CustomColors.primaryDark,
            ),
          ),
        ),
      );
    }
  }

  Widget header() {
    Size size = MediaQuery.of(context).size;
    switch (activeStep) {
      case 1:
        return Image(
          width: size.width / 8,
          height: size.width / 8,
          fit: BoxFit.scaleDown,
          image: AssetImage(imageHeader()),
        );
      default:
        return Image(
          width: kIsWeb ? 100 : size.width / 3,
          height: kIsWeb ? 100 : size.width / 3,
          fit: BoxFit.scaleDown,
          image: AssetImage(imageHeader()),
        );
    }
  }

  String imageHeader() {
    switch (activeStep) {
      case 0:
        return 'assets/images/termometro.png';

      case 1:
        return 'assets/images/end.png';

      default:
        return ' ';
    }
  }

  String headerText() {
    switch (activeStep) {
      case 0:
        return 'Termómetro del Ánimo';

      case 1:
        return '';

      default:
        return ' ';
    }
  }

  String buttonText() {
    switch (activeStep) {
      case 0:
        return 'Finalizar';

      case 1:
        return 'Continuar con el programa';

      default:
        return ' ';
    }
  }

  String stateText() {
    switch (finaltermoValue) {
      case 0:
        return 'Estás viviendo un mal momento. Esto no va a durar para siempre. Conversa con alguien cercano.';

      case 1:
        return 'Estás viviendo un mal momento. Esto no va a durar para siempre. Conversa con alguien cercano.';
      case 2:
        return 'Estás viviendo un mal momento. Esto no va a durar para siempre. Conversa con alguien cercano.';
      case 3:
        return 'Estás viviendo un mal momento. Esto no va a durar para siempre. Conversa con alguien cercano.';
      case 4:
        return 'Estás viviendo un mal momento. Esto no va a durar para siempre. Conversa con alguien cercano.';
      case 5:
        return 'Mmmh…tienes uno de esos días no tan buenos. Recuerda a los aliados para una buena salud mental: actividad física regular, alimentación saludable, higiene del sueño, organizar rutinas, pausas para distracción, socializar, etc.';
      case 6:
        return 'Mmmh…tienes uno de esos días no tan buenos. Recuerda a los aliados para una buena salud mental: actividad física regular, alimentación saludable, higiene del sueño, organizar rutinas, pausas para distracción, socializar, etc.';
      case 7:
        return '¡La energía está en ti! Recuerda darte pequeños descansos en medio de tus actividades.';
      case 8:
        return '¡La energía está en ti! Recuerda darte pequeños descansos en medio de tus actividades.';
      case 9:
        return '¡La energía está en ti! Recuerda darte pequeños descansos en medio de tus actividades.';
      case 10:
        return '¡La energía está en ti! Recuerda darte pequeños descansos en medio de tus actividades.';
      default:
        return 'Mmmh…tienes uno de esos días no tan buenos. Recuerda a los aliados para una buena salud mental: actividad física regular, alimentación saludable, higiene del sueño, organizar rutinas, pausas para distracción, socializar, etc.';
    }
  }

  String stateIcon() {
    switch (finaltermoValue) {
      case 0:
        return 'assets/images/termo-high.png';

      case 1:
        return 'assets/images/termo-high.png';
      case 2:
        return 'assets/images/termo-high.png';
      case 3:
        return 'assets/images/termo-high.png';
      case 4:
        return 'assets/images/termo-high.png';
      case 5:
        return 'assets/images/termo-mid.png';
      case 6:
        return 'assets/images/termo-mid.png';
      case 7:
        return 'assets/images/termo-low.png';
      case 8:
        return 'assets/images/termo-low.png';
      case 9:
        return 'assets/images/termo-low.png';
      case 10:
        return 'assets/images/termo-low.png';
      default:
        return 'assets/images/termo-mid.png';
    }
  }

  String stateHint() {
    switch (finaltermoValue) {
      case 0:
        return 'No me siento bien';

      case 1:
        return 'No me siento bien';
      case 2:
        return 'No me siento bien';
      case 3:
        return 'No me siento bien';
      case 4:
        return 'No me siento bien';
      case 5:
        return 'Me siento más o menos';
      case 6:
        return 'Me siento más o menos';
      case 7:
        return 'Me siento bien';
      case 8:
        return 'Me siento bien';
      case 9:
        return 'Me siento bien';
      case 10:
        return 'Me siento bien';
      default:
        return 'Me siento más o menos';
    }
  }

  Widget content() {
    switch (widget.modulo) {
      case 'P':
        switch (activeStep) {
          case 0:
            Future.delayed(const Duration(milliseconds: 50), () {
              scrollController.jumpTo(0.0);
            });

            return Expanded(
              child: RawScrollbar(
                radius: const Radius.circular(20),
                thickness: 5,
                thumbColor: CustomColors.background,
                controller: scrollController,
                thumbVisibility: true,
                child: SingleChildScrollView(
                  controller: scrollController,
                  child: Column(
                    children: [
                      topStructure(),
                      Container(
                        margin: const EdgeInsets.all(15),
                        child: RichText(
                          text: const TextSpan(
                            text:
                                '¿Cómo está tu estado de ánimo el día de hoy?  Completa el termómetro del ánimo y te daremos un consejo.',
                            style: TextStyle(
                                fontSize: 16,
                                color: CustomColors.surface,
                                height: 1.5),
                          ),
                        ),
                      ),
                      Column(
                        children: [
                          Container(
                            margin: const EdgeInsets.only(
                                left: 15, right: 15, top: 10),
                            height: 60,
                            decoration: const BoxDecoration(
                              color: CustomColors.surface,
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(20),
                                topRight: Radius.circular(20),
                              ),
                            ),
                            child: Row(
                              children: [
                                Container(
                                  margin: const EdgeInsets.only(
                                      left: 20, right: 20),
                                  child: Image(
                                    width: 40,
                                    height: 40,
                                    image: AssetImage(stateIcon()),
                                  ),
                                ),
                                Text(
                                  stateHint(),
                                  textScaler: const TextScaler.linear(1.0),
                                  style: const TextStyle(
                                      color: CustomColors.primaryDark,
                                      fontSize: 20),
                                )
                              ],
                            ),
                          ),
                          Container(
                              margin:
                                  const EdgeInsets.only(left: 15, right: 15),
                              height: 100,
                              alignment: Alignment.center,
                              decoration: const BoxDecoration(
                                color: CustomColors.primary70,
                                borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(20),
                                  bottomRight: Radius.circular(20),
                                ),
                              ),
                              child: Slider(
                                activeColor: CustomColors.primary,
                                inactiveColor: CustomColors.surface,
                                value: _currentSliderValue,
                                max: 10,
                                divisions: 10,
                                label: _currentSliderValue.round().toString(),
                                onChanged: (double value) {
                                  setState(() {
                                    _currentSliderValue = value;
                                    finaltermoValue =
                                        _currentSliderValue.round();
                                  });
                                },
                              )),
                        ],
                      ),
                      Container(
                        margin: const EdgeInsets.only(
                          top: 20,
                        ),
                        child: bottomStructure(),
                      ),
                    ],
                  ),
                ),
              ),
            );

          case 1:
            return endModulo();

          default:
            return Container();
        }
      default:
        switch (activeStep) {
          case 0:
            Future.delayed(const Duration(milliseconds: 50), () {
              scrollController.jumpTo(0.0);
            });

            return Expanded(
              child: RawScrollbar(
                radius: const Radius.circular(20),
                thickness: 5,
                thumbColor: CustomColors.background,
                controller: scrollController,
                thumbVisibility: true,
                child: SingleChildScrollView(
                  controller: scrollController,
                  child: Column(
                    children: [
                      topStructure(),
                      Container(
                        margin: const EdgeInsets.all(15),
                        child: RichText(
                          text: const TextSpan(
                            text:
                                '¿Cómo está tu estado de ánimo el día de hoy?  Completa el termómetro del ánimo y te daremos un consejo.',
                            style: TextStyle(
                                fontSize: 16,
                                color: CustomColors.surface,
                                height: 1.5),
                          ),
                        ),
                      ),
                      Column(
                        children: [
                          Container(
                            margin: const EdgeInsets.only(
                                left: 15, right: 15, top: 10),
                            height: 60,
                            decoration: const BoxDecoration(
                              color: CustomColors.surface,
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(20),
                                topRight: Radius.circular(20),
                              ),
                            ),
                            child: Row(
                              children: [
                                Container(
                                  margin: const EdgeInsets.only(
                                      left: 20, right: 20),
                                  child: Image(
                                    width: 40,
                                    height: 40,
                                    image: AssetImage(stateIcon()),
                                  ),
                                ),
                                Text(
                                  stateHint(),
                                  textScaler: const TextScaler.linear(1.0),
                                  style: const TextStyle(
                                      color: CustomColors.primaryDark,
                                      fontSize: 20),
                                )
                              ],
                            ),
                          ),
                          Container(
                              margin:
                                  const EdgeInsets.only(left: 15, right: 15),
                              height: 100,
                              alignment: Alignment.center,
                              decoration: const BoxDecoration(
                                color: CustomColors.primary70,
                                borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(20),
                                  bottomRight: Radius.circular(20),
                                ),
                              ),
                              child: Slider(
                                activeColor: CustomColors.primary,
                                inactiveColor: CustomColors.surface,
                                value: _currentSliderValue,
                                max: 10,
                                divisions: 10,
                                label: _currentSliderValue.round().toString(),
                                onChanged: (double value) {
                                  setState(() {
                                    _currentSliderValue = value;
                                    finaltermoValue =
                                        _currentSliderValue.round();
                                  });
                                },
                              )),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            );

          case 1:
            return endModulo();

          default:
            return Container();
        }
    }
  }

  Widget endModulo() {
    switch (widget.modulo) {
      case '1':
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Container(
                margin: const EdgeInsets.all(15),
                child: Column(
                  children: [
                    topStructure(),
                    Container(
                      margin: const EdgeInsets.only(
                          bottom: 10, top: 20, left: 10, right: 10),
                      child: Text(
                        stateText(),
                        textScaler: const TextScaler.linear(1.0),
                        textAlign: TextAlign.center,
                        style: const TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 10, bottom: 10),
                      child: const Text(
                        'Tenemos recompensas para apoyarte:',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(fontSize: 16),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 20, bottom: 10),
                      decoration: BoxDecoration(
                          color: CustomColors.lightContainer,
                          border:
                              Border.all(width: 2, color: CustomColors.surface),
                          borderRadius: BorderRadius.circular(15)),
                      child: Row(
                        children: [
                          const Image(
                            width: 100,
                            height: 120,
                            fit: BoxFit.scaleDown,
                            image: AssetImage('assets/images/termometro.png'),
                          ),
                          Container(
                            margin: const EdgeInsets.only(left: 10),
                            child: const Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Nueva función en perfil:',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      fontSize: 13,
                                      color: CustomColors.primaryDark),
                                ),
                                Text(
                                  'Tu historial del ánimo',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16,
                                      color: CustomColors.primaryDark),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );

      default:
        return Expanded(
          child: Container(
            margin: const EdgeInsets.all(15),
            child: Column(
              children: [
                const Spacer(),
                topStructure(),
                Container(
                  margin: const EdgeInsets.all(10),
                  child: Text(
                    stateText(),
                    textScaler: const TextScaler.linear(1.0),
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                        fontWeight: FontWeight.bold,
                        // color: CustomColors.primaryDark,
                        fontSize: 18),
                  ),
                ),
                const Spacer(),
              ],
            ),
          ),
        );
    }
  }
}
