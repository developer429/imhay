// ignore_for_file: file_names

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:seguimiento/colors.dart';
import 'package:seguimiento/firebase.dart';

class ConfigurarNotificaciones extends StatefulWidget {
  const ConfigurarNotificaciones({
    Key? key,
  }) : super(key: key);

  @override
  State<ConfigurarNotificaciones> createState() =>
      _ConfigurarNotificacionesState();
}

class _ConfigurarNotificacionesState extends State<ConfigurarNotificaciones> {
  String uid = FirebaseAuth.instance.currentUser!.uid;
  List<String> horasT = [];
  List<int> estadosT = [];
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('usuarios');
  void getNotiData() {
    collectionReference.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        if (fields['receiveNots'] == true) {
          switchIsOn = true;
        } else {
          switchIsOn = false;
        }
        if (fields['time'] != 12) {
          _selection = fields['time'];
        } else {
          _selection == 12;
        }
        if (fields['days'][0] == true) {
          activeLunes = true;
        } else {
          activeLunes = false;
        }
        if (fields['days'][1] == true) {
          activeMartes = true;
        } else {
          activeMartes = false;
        }
        if (fields['days'][2] == true) {
          activeMiercoles = true;
        } else {
          activeMiercoles = false;
        }
        if (fields['days'][3] == true) {
          activeJueves = true;
        } else {
          activeJueves = false;
        }
        if (fields['days'][4] == true) {
          activeViernes = true;
        } else {
          activeViernes = false;
        }
        if (fields['days'][5] == true) {
          activeSabado = true;
        } else {
          activeSabado = false;
        }
        if (fields['days'][6] == true) {
          activeDomingo = true;
        } else {
          activeDomingo = false;
        }
      });
    });
  }

  bool switchIsOn = true;
  final WidgetStateProperty<Icon?> thumbIcon =
      WidgetStateProperty.resolveWith<Icon?>(
    (Set<WidgetState> states) {
      if (states.contains(WidgetState.selected)) {
        return const Icon(Icons.check);
      }
      return const Icon(Icons.close);
    },
  );
  bool activeLunes = true;
  bool activeMartes = true;
  bool activeMiercoles = true;
  bool activeJueves = true;
  bool activeViernes = true;
  bool activeSabado = true;
  bool activeDomingo = true;
  List<int> horaDeseada = <int>[
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20,
    21,
    22
  ].toList();
  int _selection = 12;

  int activeStep = 0;
  @override
  void initState() {
    super.initState();
    getNotiData();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: CustomColors.background,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: CustomColors.background,
          body: Center(
            child: Column(
              children: [
                content(),
                Container(
                  margin: const EdgeInsets.only(top: 10, bottom: 10),
                  child: bottomStructure(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget topStructure() {
    return Stack(
      children: [
        Column(
          children: [
            Row(
              children: [
                Container(
                  alignment: Alignment.topRight,
                  child: IconButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    icon: const Icon(
                      Icons.arrow_back,
                      color: CustomColors.primaryDark,
                    ),
                  ),
                ),
              ],
            ),
            Text(
              headerText(),
              textScaler: const TextScaler.linear(1.0),
              textAlign: TextAlign.center,
              style: const TextStyle(
                color: CustomColors.primaryDark,
                fontSize: 24,
              ),
            ),
            header(),
          ],
        ),
      ],
    );
  }

  Widget bottomStructure() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        nextButton(),
      ],
    );
  }

  Widget nextButton() {
    Size size = MediaQuery.of(context).size;

    return SizedBox(
      width: size.width / 1.2,
      child: ElevatedButton(
        style: ButtonStyle(
          backgroundColor: WidgetStateProperty.all<Color>(CustomColors.primary),
          shape: WidgetStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(18.0),
            ),
          ),
        ),
        onPressed: () async {
          FirestoreService().updateNotificationSettings(
              switchIsOn,
              _selection,
              activeLunes,
              activeMartes,
              activeMiercoles,
              activeJueves,
              activeViernes,
              activeSabado,
              activeDomingo);
          showDialog(
            context: context,
            builder: (context) => AlertDialog(
              shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(32.0))),
              title: const Text(
                '¡Configuración guardada!',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.primaryDark),
              ),
              content: const Text(
                'Recibirás notificaciones de recordatorio los días y a la hora que seleccionaste.',
                textScaler: TextScaler.linear(1.0),
                style: TextStyle(color: CustomColors.primaryDark),
              ),
              actions: [
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: const Text(
                    'Cerrar',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(color: CustomColors.primary),
                  ),
                ),
              ],
            ),
          );
        },
        child: Text(
          buttonText(),
          textScaler: const TextScaler.linear(1.0),
          style: const TextStyle(color: CustomColors.surface),
        ),
      ),
    );
  }

  Widget header() {
    Size size = MediaQuery.of(context).size;
    switch (activeStep) {
      default:
        return Container(
          margin: const EdgeInsets.all(15),
          decoration: const BoxDecoration(),
          child: Image(
            width: size.width / 3,
            height: size.width / 3,
            fit: BoxFit.scaleDown,
            image: AssetImage(imageHeader()),
          ),
        );
    }
  }

  String imageHeader() {
    switch (activeStep) {
      case 0:
        return 'assets/images/nots-icon.png';

      default:
        return ' ';
    }
  }

  String headerText() {
    switch (activeStep) {
      case 0:
        return 'Configura las notificaciones';

      default:
        return ' ';
    }
  }

  String buttonText() {
    switch (activeStep) {
      case 0:
        return 'Guardar';

      default:
        return ' ';
    }
  }

  Widget weekButtons() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        ElevatedButton(
          onPressed: () {
            setState(() {
              if (activeLunes == false) {
                activeLunes = true;
              } else {
                activeLunes = false;
              }
            });
          },
          child: Text(
            'Lu',
            textScaler: const TextScaler.linear(1.0),
            style: TextStyle(
                color:
                    activeLunes ? CustomColors.surface : CustomColors.primary),
          ),
          style: ElevatedButton.styleFrom(
            backgroundColor:
                activeLunes ? CustomColors.primary : Colors.transparent,
            tapTargetSize: MaterialTapTargetSize.shrinkWrap,
            shadowColor: Colors.transparent,
            elevation: 0,
            side: const BorderSide(
              width: 1.0,
              color: CustomColors.primary,
            ),
            padding: EdgeInsets.zero,
            minimumSize: const Size(40, 40),
            shape: const CircleBorder(),
          ),
        ),
        ElevatedButton(
          onPressed: () {
            setState(() {
              if (activeMartes == false) {
                activeMartes = true;
              } else {
                activeMartes = false;
              }
            });
          },
          child: Text(
            'Ma',
            textScaler: const TextScaler.linear(1.0),
            style: TextStyle(
                color:
                    activeMartes ? CustomColors.surface : CustomColors.primary),
          ),
          style: ElevatedButton.styleFrom(
            backgroundColor:
                activeMartes ? CustomColors.primary : Colors.transparent,
            tapTargetSize: MaterialTapTargetSize.shrinkWrap,
            shadowColor: Colors.transparent,
            elevation: 0,
            side: const BorderSide(
              width: 1.0,
              color: CustomColors.primary,
            ),
            padding: EdgeInsets.zero,
            minimumSize: const Size(40, 40),
            shape: const CircleBorder(),
          ),
        ),
        ElevatedButton(
          onPressed: () {
            setState(() {
              if (activeMiercoles == false) {
                activeMiercoles = true;
              } else {
                activeMiercoles = false;
              }
            });
          },
          child: Text(
            'Mi',
            textScaler: const TextScaler.linear(1.0),
            style: TextStyle(
                color: activeMiercoles
                    ? CustomColors.surface
                    : CustomColors.primary),
          ),
          style: ElevatedButton.styleFrom(
            backgroundColor:
                activeMiercoles ? CustomColors.primary : Colors.transparent,
            tapTargetSize: MaterialTapTargetSize.shrinkWrap,
            shadowColor: Colors.transparent,
            elevation: 0,
            side: const BorderSide(
              width: 1.0,
              color: CustomColors.primary,
            ),
            padding: EdgeInsets.zero,
            minimumSize: const Size(40, 40),
            shape: const CircleBorder(),
          ),
        ),
        ElevatedButton(
          onPressed: () {
            setState(() {
              if (activeJueves == false) {
                activeJueves = true;
              } else {
                activeJueves = false;
              }
            });
          },
          child: Text(
            'Ju',
            textScaler: const TextScaler.linear(1.0),
            style: TextStyle(
                color:
                    activeJueves ? CustomColors.surface : CustomColors.primary),
          ),
          style: ElevatedButton.styleFrom(
            backgroundColor:
                activeJueves ? CustomColors.primary : Colors.transparent,
            tapTargetSize: MaterialTapTargetSize.shrinkWrap,
            shadowColor: Colors.transparent,
            elevation: 0,
            side: const BorderSide(
              width: 1.0,
              color: CustomColors.primary,
            ),
            padding: EdgeInsets.zero,
            minimumSize: const Size(40, 40),
            shape: const CircleBorder(),
          ),
        ),
        ElevatedButton(
          onPressed: () {
            setState(() {
              if (activeViernes == false) {
                activeViernes = true;
              } else {
                activeViernes = false;
              }
            });
          },
          child: Text(
            'Vi',
            textScaler: const TextScaler.linear(1.0),
            style: TextStyle(
                color: activeViernes
                    ? CustomColors.surface
                    : CustomColors.primary),
          ),
          style: ElevatedButton.styleFrom(
            backgroundColor:
                activeViernes ? CustomColors.primary : Colors.transparent,
            tapTargetSize: MaterialTapTargetSize.shrinkWrap,
            shadowColor: Colors.transparent,
            elevation: 0,
            side: const BorderSide(
              width: 1.0,
              color: CustomColors.primary,
            ),
            padding: EdgeInsets.zero,
            minimumSize: const Size(40, 40),
            shape: const CircleBorder(),
          ),
        ),
        ElevatedButton(
          onPressed: () {
            setState(() {
              if (activeSabado == false) {
                activeSabado = true;
              } else {
                activeSabado = false;
              }
            });
          },
          child: Text(
            'Sa',
            textScaler: const TextScaler.linear(1.0),
            style: TextStyle(
                color:
                    activeSabado ? CustomColors.surface : CustomColors.primary),
          ),
          style: ElevatedButton.styleFrom(
            backgroundColor:
                activeSabado ? CustomColors.primary : Colors.transparent,
            tapTargetSize: MaterialTapTargetSize.shrinkWrap,
            shadowColor: Colors.transparent,
            elevation: 0,
            side: const BorderSide(
              width: 1.0,
              color: CustomColors.primary,
            ),
            padding: EdgeInsets.zero,
            minimumSize: const Size(40, 40),
            shape: const CircleBorder(),
          ),
        ),
        ElevatedButton(
          onPressed: () {
            setState(() {
              if (activeDomingo == false) {
                activeDomingo = true;
              } else {
                activeDomingo = false;
              }
            });
          },
          child: Text(
            'Do',
            textScaler: const TextScaler.linear(1.0),
            style: TextStyle(
                color: activeDomingo
                    ? CustomColors.surface
                    : CustomColors.primary),
          ),
          style: ElevatedButton.styleFrom(
            backgroundColor:
                activeDomingo ? CustomColors.primary : Colors.transparent,
            tapTargetSize: MaterialTapTargetSize.shrinkWrap,
            shadowColor: Colors.transparent,
            elevation: 0,
            side: const BorderSide(
              width: 1.0,
              color: CustomColors.primary,
            ),
            padding: EdgeInsets.zero,
            minimumSize: const Size(40, 40),
            shape: const CircleBorder(),
          ),
        ),
      ],
    );
  }

  Widget content() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        return Expanded(
          child: SingleChildScrollView(
            child: Column(
              children: [
                topStructure(),
                Container(
                  margin: const EdgeInsets.only(left: 20, right: 20),
                  child: RichText(
                    text: const TextSpan(
                      text:
                          'Acá puedes configurar las notificaciones que podemos enviarte para recordarte el uso de la App Vamos Juntxs.',
                      style: TextStyle(
                          fontSize: 16,
                          color: CustomColors.primary,
                          height: 1.5),
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.all(20),
                  child: ListView(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Text(
                            'Recibir notificaciones',
                            textScaler: TextScaler.linear(1.0),
                            style: TextStyle(
                                color: CustomColors.primary3,
                                fontWeight: FontWeight.bold,
                                fontSize: 16),
                          ),
                          Switch(
                            // This bool value toggles the switch.
                            thumbIcon: thumbIcon,
                            activeTrackColor: CustomColors.primary,
                            value: switchIsOn,
                            onChanged: (bool value) {
                              setState(() {
                                switchIsOn = value;
                              });
                            },
                          ),
                        ],
                      ),
                      const Divider(
                        color: CustomColors.primary,
                      ),
                      Visibility(
                        visible: switchIsOn,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text(
                              'Elige los días de las semanas',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  color: CustomColors.primary3, fontSize: 15),
                            ),
                            Container(
                                margin:
                                    const EdgeInsets.only(top: 10, bottom: 10),
                                child: weekButtons()),
                          ],
                        ),
                      ),
                      Visibility(
                        visible: switchIsOn,
                        child: const Divider(
                          color: CustomColors.primary,
                        ),
                      ),
                      Visibility(
                        visible: switchIsOn,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text(
                              'Elige la hora para recibir la notificación',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  color: CustomColors.primary3, fontSize: 15),
                            ),
                            Container(
                              margin:
                                  const EdgeInsets.only(top: 10, bottom: 10),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    decoration: BoxDecoration(
                                        color: CustomColors.primary2,
                                        borderRadius: BorderRadius.circular(20),
                                        border: Border.all(
                                            color: CustomColors.primary)),
                                    width: size.width / 5,
                                    child: DropdownButton<int>(
                                      underline: Visibility(
                                        visible: false,
                                        child: Container(
                                          height: 0,
                                          color: Colors.transparent,
                                        ),
                                      ),
                                      alignment: Alignment.center,
                                      menuMaxHeight: 200,
                                      itemHeight: 70,
                                      isExpanded: true,
                                      borderRadius: BorderRadius.circular(10),
                                      iconSize: 0.0,
                                      icon: null,
                                      value: _selection,
                                      style: const TextStyle(
                                          color: CustomColors.primary,
                                          fontSize: 35),
                                      onChanged: (newvalue) {
                                        // This is called when the user selects an item.
                                        setState(() {
                                          _selection = newvalue!;
                                        });
                                      },
                                      items: horaDeseada.map((int value) {
                                        return DropdownMenuItem<int>(
                                          alignment: Alignment.center,
                                          value: value,
                                          child: Text(
                                            value.toString(),
                                            textAlign: TextAlign.center,
                                            textScaler:
                                                const TextScaler.linear(1.0),
                                          ),
                                        );
                                      }).toList(),
                                    ),
                                  ),
                                  Container(
                                    height: 70,
                                    alignment: Alignment.center,
                                    child: const Text(
                                      ':',
                                      style: TextStyle(
                                          color: CustomColors.primary,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 28),
                                    ),
                                  ),
                                  Container(
                                    height: 70,
                                    width: size.width / 5,
                                    decoration: BoxDecoration(
                                        color: CustomColors.surface,
                                        borderRadius: BorderRadius.circular(20),
                                        border: Border.all(
                                            color: CustomColors.surface)),
                                    alignment: Alignment.center,
                                    child: const Text(
                                      '00',
                                      style: TextStyle(
                                          color: CustomColors.primary,
                                          fontSize: 28),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Visibility(
                        visible: switchIsOn,
                        child: const Divider(
                          color: CustomColors.primary,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );

      default:
        return Container();
    }
  }
}
