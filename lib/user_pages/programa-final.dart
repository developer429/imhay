// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:seguimiento/colors.dart';

class EndPage extends StatefulWidget {
  const EndPage({
    Key? key,
  }) : super(key: key);

  @override
  State<EndPage> createState() => _EndPageState();
}

class _EndPageState extends State<EndPage> {
  int activeStep = 0;
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  ScrollController scrollController = ScrollController();

  bool active1 = false;
  @override
  Widget build(BuildContext context) {
    switch (activeStep) {
      case 0:
        active1 = true;
        break;

      default:
    }
    return Container(
      color: Colors.black,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: CustomColors.purplePrograma,
          body: Center(
            child: Column(
              children: [
                content(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget topStructure() {
    return Stack(
      children: [
        Column(
          children: [
            Row(
              children: [
                Container(
                  alignment: Alignment.topRight,
                  child: IconButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    icon: const Icon(
                      Icons.close,
                      color: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
            Container(
              margin: const EdgeInsets.only(top: 10),
              child: Text(
                headerText(),
                textScaler: const TextScaler.linear(1.0),
                textAlign: TextAlign.center,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 25,
                ),
              ),
            ),
            Container(
                margin: const EdgeInsets.only(top: 20, bottom: 20),
                child: header()),
          ],
        ),
      ],
    );
  }

  Widget bottomStructure() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        nextButton(),
      ],
    );
  }

  Widget nextButton() {
    Size size = MediaQuery.of(context).size;

    // ignore: sized_box_for_whitespace
    return Container(
      width: size.width / 1.2,
      child: ElevatedButton(
        style: ButtonStyle(
          backgroundColor:
              WidgetStateProperty.all<Color>(CustomColors.greenButton),
          shape: WidgetStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(18.0),
            ),
          ),
        ),
        onPressed: () {
          Navigator.of(context).pop();
        },
        child: Text(
          buttonText(),
          textScaler: const TextScaler.linear(1.0),
          style: const TextStyle(color: CustomColors.primaryDark),
        ),
      ),
    );
  }

  Widget header() {
    Size size = MediaQuery.of(context).size;
    switch (activeStep) {
      default:
        return Image(
          width: size.width / 1.5,
          height: size.width / 1.5,
          fit: BoxFit.scaleDown,
          image: AssetImage(imageHeader()),
        );
    }
  }

  String imageHeader() {
    switch (activeStep) {
      case 0:
        return 'assets/images/end-programa.png';

      default:
        return ' ';
    }
  }

  String headerText() {
    switch (activeStep) {
      case 0:
        return '¡Felicidades!\nHas terminado Tu Programa';

      default:
        return ' ';
    }
  }

  String buttonText() {
    switch (activeStep) {
      case 0:
        return 'Finalizar';

      default:
        return ' ';
    }
  }

  Widget content() {
    switch (activeStep) {
      case 0:
        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.background,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 5, right: 20, left: 20, bottom: 5),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Durante estas semanas has tomado acción y has adquirido conocimientos, habilidades y estrategias para potenciar tu bienestar y salud mental.\n¡Buen trabajo!\n\nEstos aprendizajes te ayudarán a enfrentar los desafíos y cambios que encontrarás durante esta etapa de tu vida.\n¡Sigue así!\n\nRecuerda que “la práctica hace al maestro”, por lo que es importante que sigas practicando tus nuevas habilidades y estrategias. De este modo podrás recordarlas siempre que las necesites.\nRecuerda también que no necesitas solucionar todo por tu cuenta, puedes pedir ayuda cuando la necesites. Hay personas y profesionales que podrán ayudarte.\n\nNos alegra mucho haber podido acompañarte durante este proceso,\n\n¡Sigamos Juntxs!',
                        style: TextStyle(
                            fontSize: 16, color: Colors.white, height: 1.5),
                        children: <TextSpan>[],
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 20, bottom: 20),
                    child: bottomStructure(),
                  ),
                ],
              ),
            ),
          ),
        );

      default:
        return Container();
    }
  }
}
