// ignore_for_file: file_names

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:seguimiento/colors.dart';
import 'package:seguimiento/video-player.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../firebase.dart';

class PlanSeguridad extends StatefulWidget {
  const PlanSeguridad({
    Key? key,
  }) : super(key: key);

  @override
  State<PlanSeguridad> createState() => _PlanSeguridadState();
}

class _PlanSeguridadState extends State<PlanSeguridad> {
  String uid = FirebaseAuth.instance.currentUser!.uid;

  String videoPlanSeguridad =
      'https://firebasestorage.googleapis.com/v0/b/maquetadatos-5dee1.appspot.com/o/videos%2Fplan-seguridad.mp4?alt=media&token=4537b7e8-e639-47bc-be3e-7c0366612194';
  final Uri videoYT = Uri.parse('https://www.youtube.com/watch?v=-Cm9sTYmGK8');

  CollectionReference dataReference =
      FirebaseFirestore.instance.collection('plan-seguridad-respuestas');
  void getData() {
    dataReference.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        apoyoprofesional.text = fields['apoyo_profesional'];
        personascontactar.text = fields['contactos'];
        calmarme.text = fields['que_hacer_calmarme'];
        razonesvivir.text = fields['razones_vivir'];
      });
    }).catchError((err) {
      // print('Error: $err');
    });
  }

  ScrollController scrollController = ScrollController();

  int activeStep = 0;
  @override
  void initState() {
    super.initState();
    setState(() {
      SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);
    });

    getData();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> _launchUrl(url) async {
    if (!await launchUrl(url)) {
      throw Exception('Could not launch $url');
    }
  }

  bool active1 = false;
  bool active2 = false;
  bool active3 = false;
  final razonesvivir = TextEditingController();
  final calmarme = TextEditingController();
  final personascontactar = TextEditingController();
  final apoyoprofesional = TextEditingController();

  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    setState(() {
      SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);
    });

    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        active1 = true;
        active2 = false;
        active3 = false;

        break;
      case 1:
        active1 = true;
        active2 = true;
        active3 = false;

        break;
      case 2:
        active1 = true;
        active2 = true;
        active3 = true;
        break;

      default:
    }
    return Container(
      color: Colors.black,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: CustomColors.background,
          body: Center(
            child: Column(
              children: [
                Row(
                  children: [
                    Container(
                      width: size.width / 3,
                      height: 5,
                      color: active1 ? CustomColors.primary2 : Colors.black,
                      child: const Text(''),
                    ),
                    Container(
                      width: size.width / 3,
                      height: 5,
                      color: active2 ? CustomColors.primary2 : Colors.black,
                      child: const Text(''),
                    ),
                    Container(
                      width: size.width / 3,
                      height: 5,
                      color: active3 ? CustomColors.primary2 : Colors.black,
                      child: const Text(''),
                    ),
                  ],
                ),
                content(),
                Container(
                  margin: const EdgeInsets.only(top: 10, bottom: 10),
                  child: bottomStructure(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget topStructure() {
    return Stack(
      children: [
        Column(
          children: [
            Row(
              children: [
                Container(
                  alignment: Alignment.topRight,
                  child: IconButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    icon: const Icon(
                      Icons.arrow_back,
                      color: CustomColors.primaryDark,
                    ),
                  ),
                ),
              ],
            ),
            Text(
              headerText(),
              textScaler: const TextScaler.linear(1.0),
              textAlign: TextAlign.center,
              style: const TextStyle(
                color: CustomColors.primaryDark,
                fontSize: 24,
              ),
            ),
            header(),
          ],
        ),
      ],
    );
  }

  Widget bottomStructure() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        nextButton(),
      ],
    );
  }

  Widget nextButton() {
    Size size = MediaQuery.of(context).size;
    String empty = '-';
    if (activeStep >= 1 && activeStep < 3) {
      return SizedBox(
        width: size.width / 1,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        WidgetStateProperty.all<Color>(CustomColors.background),
                    shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: const BorderSide(color: CustomColors.primary)),
                    ),
                  ),
                  onPressed: () {
                    setState(() {
                      activeStep = activeStep - 1;
                    });
                  },
                  child: const Text(
                    'Anterior',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(color: CustomColors.primary),
                  )),
            ),
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor:
                      WidgetStateProperty.all<Color>(CustomColors.primary),
                  shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ),
                  ),
                ),
                onPressed: () {
                  if (activeStep < 2) {
                    setState(() {
                      activeStep++;
                    });
                  } else {
                    if (activeStep == 2) {
                      if (_formKey.currentState!.validate()) {
                        FirestoreService()
                            .guardarRespuestasPlanSeguridad(
                              razonesvivir.text.isEmpty
                                  ? empty
                                  : razonesvivir.text.trim(),
                              calmarme.text.isEmpty
                                  ? empty
                                  : calmarme.text.trim(),
                              personascontactar.text.isEmpty
                                  ? empty
                                  : personascontactar.text.trim(),
                              apoyoprofesional.text.isEmpty
                                  ? empty
                                  : apoyoprofesional.text.trim(),
                            )
                            .then((value) => Navigator.of(context).pop());
                      } else {
                        showDialog<String>(
                          context: context,
                          builder: (BuildContext context) => AlertDialog(
                            title: const Text(
                              'Error',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  color: Colors.red,
                                  fontWeight: FontWeight.bold),
                            ),
                            content: const Text(
                              'Complete los campos faltantes',
                              textScaler: TextScaler.linear(1.0),
                            ),
                            actions: <Widget>[
                              TextButton(
                                onPressed: () => Navigator.pop(
                                  context,
                                ),
                                child: const Text(
                                  'OK',
                                  textScaler: TextScaler.linear(1.0),
                                  style: TextStyle(
                                      color: CustomColors.primaryDark,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ],
                          ),
                        );
                      }
                    }
                  }
                },
                child: Text(
                  buttonText(),
                  textScaler: const TextScaler.linear(1.0),
                  style: const TextStyle(
                      color: CustomColors.background,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      return SizedBox(
        width: size.width / 1.2,
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor:
                WidgetStateProperty.all<Color>(CustomColors.primary),
            shape: WidgetStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              ),
            ),
          ),
          onPressed: () {
            if (activeStep < 2) {
              setState(() {
                activeStep++;
              });
            } else {
              if (activeStep == 2) {
                if (_formKey.currentState!.validate()) {
                  FirestoreService()
                      .guardarRespuestasPlanSeguridad(
                        razonesvivir.text.isEmpty
                            ? empty
                            : razonesvivir.text.trim(),
                        calmarme.text.isEmpty ? empty : calmarme.text.trim(),
                        personascontactar.text.isEmpty
                            ? empty
                            : personascontactar.text.trim(),
                        apoyoprofesional.text.isEmpty
                            ? empty
                            : apoyoprofesional.text.trim(),
                      )
                      .then((value) => Navigator.of(context).pop());
                } else {
                  showDialog<String>(
                    context: context,
                    builder: (BuildContext context) => AlertDialog(
                      title: const Text(
                        'Error',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(
                            color: Colors.red, fontWeight: FontWeight.bold),
                      ),
                      content: const Text(
                        'Complete los campos faltantes',
                        textScaler: TextScaler.linear(1.0),
                      ),
                      actions: <Widget>[
                        TextButton(
                          onPressed: () => Navigator.pop(
                            context,
                          ),
                          child: const Text(
                            'OK',
                            textScaler: TextScaler.linear(1.0),
                            style: TextStyle(
                                color: CustomColors.primaryDark,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                  );
                }
              }
            }
            /*
            */
          },
          child: Text(
            buttonText(),
            textScaler: const TextScaler.linear(1.0),
            style: const TextStyle(color: CustomColors.background),
          ),
        ),
      );
    }
  }

  Widget header() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      default:
        return Container(
          margin: const EdgeInsets.all(15),
          decoration: const BoxDecoration(),
          child: Image(
            width: size.width / 3,
            height: size.width / 3,
            fit: BoxFit.scaleDown,
            image: AssetImage(imageHeader()),
          ),
        );
    }
  }

  String imageHeader() {
    switch (activeStep) {
      default:
        return 'assets/images/plan-seguridad.png';
    }
  }

  String headerText() {
    switch (activeStep) {
      default:
        return 'Plan de seguridad';
    }
  }

  String buttonText() {
    switch (activeStep) {
      case 2:
        return 'Finalizar';

      default:
        return 'Siguiente ';
    }
  }

  Widget content() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 20, right: 20, left: 20, bottom: 30),
                    child: RichText(
                      text: const TextSpan(
                        text: '',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            fontWeight: FontWeight.bold,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                'A veces puedes sentir que tu malestar emocional es insoportable, llegando a tener ideas de hacerte daño o de querer morir. Si estás en esta situación debemos contar con un plan de seguridad que te permita identificar ideas, acciones, lugares o personas que pueden ayudarte a superar el momento de crisis.\n\nEn el siguiente video aprenderás cómo crear un plan de seguridad en salud mental.',
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 1:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    width: kIsWeb ? size.width / 2 : size.width / 1.2,
                    height: size.height / 4,
                    margin: const EdgeInsets.only(top: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: CustomColors.surface,
                    ),
                    child: Stack(
                      alignment: Alignment.topCenter,
                      children: [
                        const Center(
                          child: CircularProgressIndicator(
                            color: CustomColors.orange,
                          ),
                        ),
                        VideoPlayerURL(url: videoPlanSeguridad),
                        kIsWeb
                            ? Container(
                                margin: const EdgeInsets.only(top: 10),
                                child: ElevatedButton(
                                  style: ButtonStyle(
                                    backgroundColor:
                                        WidgetStateProperty.all<Color>(
                                      const Color.fromRGBO(255, 0, 0, 1),
                                    ),
                                  ),
                                  onPressed: () => _launchUrl(videoYT),
                                  child: const Text(
                                    'Ver en YouTube',
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                              )
                            : Container(),
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 20, right: 20, left: 20, bottom: 5),
                    child: RichText(
                      text: const TextSpan(
                        text: '',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                'Este video fue creado gracias al financiamiento de la Iniciativa Científica Milenio del programa de la Agencia Nacional de Investigación y Desarrollo (ANID), perteneciente al Ministerio de Ciencia, Tecnología, Conocimiento e Innovación, a través de su programa de Proyección al Medio Externo.',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 2:
        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 5, right: 20, left: 20, bottom: 30),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Ahora que has aprendido qué es y cómo se crea un plan de seguridad queremos acompañarte a construir el tuyo',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[],
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.all(10),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        children: [
                          Container(
                            margin: const EdgeInsets.only(
                                left: 10, right: 10, bottom: 0, top: 10),
                            alignment: Alignment.centerLeft,
                            child: const Text(
                              'Mis razones para vivir',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 16, color: CustomColors.primary),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: razonesvivir,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: 'Escribe tu respuesta...\n\n\n\n\n',
                                  hintMaxLines: 5,
                                  border: OutlineInputBorder()),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(
                                left: 10, right: 10, bottom: 0, top: 10),
                            alignment: Alignment.centerLeft,
                            child: const Text(
                              'Cosas que puedo hacer para calmarme',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 16, color: CustomColors.primary),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: calmarme,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: 'Escribe tu respuesta...\n\n\n\n\n',
                                  hintMaxLines: 5,
                                  border: OutlineInputBorder()),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(
                                left: 10, right: 10, bottom: 0, top: 10),
                            alignment: Alignment.centerLeft,
                            child: const Text(
                              'Personas que puedo contactar',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 16, color: CustomColors.primary),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: personascontactar,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: 'Escribe tu respuesta...\n\n\n\n\n',
                                  hintMaxLines: 5,
                                  border: OutlineInputBorder()),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(
                                left: 10, right: 10, bottom: 0, top: 10),
                            alignment: Alignment.centerLeft,
                            child: const Text(
                              'Apoyo profesional',
                              textScaler: TextScaler.linear(1.0),
                              style: TextStyle(
                                  fontSize: 16, color: CustomColors.primary),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.all(10),
                            child: TextFormField(
                              textAlign: TextAlign.left,
                              autofocus: false,
                              controller: apoyoprofesional,
                              decoration: const InputDecoration(
                                  filled: true,
                                  fillColor: CustomColors.surface,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.always,
                                  hintText: 'Escribe tu respuesta...\n\n\n\n\n',
                                  hintMaxLines: 5,
                                  border: OutlineInputBorder()),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 5, right: 20, left: 20, bottom: 30),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'Recuerda que si te resulta difícil enfrentar situaciones complejas, hay personas que pueden y quieren ayudarte.',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );

      default:
        return Container();
    }
  }
}
