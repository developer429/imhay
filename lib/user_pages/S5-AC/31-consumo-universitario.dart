// ignore_for_file: file_names, avoid_print

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:seguimiento/colors.dart';
import 'package:seguimiento/user_pages/MainPage.dart';
import 'package:seguimiento/video-player.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../firebase.dart';

class A31 extends StatefulWidget {
  const A31({
    Key? key,
  }) : super(key: key);

  @override
  State<A31> createState() => _A31State();
}

class _A31State extends State<A31> {
  final Uri _url = Uri.parse('https://academiasenda.gob.cl/');

  final Uri videoYT1 = Uri.parse('https://www.youtube.com/watch?v=aPLRtX9MIMc');

  String videoPrevencionEdSuperior =
      'https://firebasestorage.googleapis.com/v0/b/maquetadatos-5dee1.appspot.com/o/videos%2Fprevencion-educacion-superior.mp4?alt=media&token=66e163f5-9f36-4649-9e30-e63be5036182';

  final Uri videoYT2 = Uri.parse('https://www.youtube.com/watch?v=QpSXhMP9_kM');
  String videoIntroduccionDrogas =
      'https://firebasestorage.googleapis.com/v0/b/maquetadatos-5dee1.appspot.com/o/videos%2Fintroduccion-drogas.mp4?alt=media&token=1467490f-81c6-42e7-bc96-70929391b871';

  final Uri videoYT3 = Uri.parse('https://www.youtube.com/watch?v=Bxy-MqRtEPY');

  String videoConsumoProblematico =
      'https://firebasestorage.googleapis.com/v0/b/maquetadatos-5dee1.appspot.com/o/videos%2Fconsumo-problematico.mp4?alt=media&token=3a81b36a-fe83-4770-8fb5-ac0a00d65cad';
  String uid = FirebaseAuth.instance.currentUser!.uid;
  bool loadingData = true;
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('page-control');
  String activeStepData = '';
  void getActivePageStored() {
    collectionReference.doc(uid).get().then((value) {
      var fields = value;
      activeStepData = fields['A31'];
      setState(() {
        if (int.parse(activeStepData) < 6) {
          activeStep = int.parse(activeStepData) - 1;
          Future.delayed(const Duration(milliseconds: 500), () {
            setState(() {
              loadingData = false;
            });
          });
        }
      });
    });
  }

  ScrollController scrollController = ScrollController();

  bool favorite = false;
  CollectionReference favoriteData =
      FirebaseFirestore.instance.collection('favoritos');
  void getFavData() {
    favoriteData.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        favorite = fields['A31'];
      });
    }).catchError((err) {
      print('Error: $err');
    });
  }

  int activeStep = 0;
  @override
  void initState() {
    super.initState();

    getActivePageStored();
    getFavData();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> _launchUrl() async {
    if (!await launchUrl(_url)) {
      throw Exception('Could not launch $_url');
    }
  }

  Future<void> _launchUrlYT(url) async {
    if (!await launchUrl(url)) {
      throw Exception('Could not launch $url');
    }
  }

  bool active1 = false;
  bool active2 = false;
  bool active3 = false;
  bool active4 = false;
  bool active5 = false;
  bool active6 = false;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        active1 = true;
        active2 = false;
        active3 = false;
        active4 = false;
        active5 = false;
        active6 = false;

        break;
      case 1:
        active1 = true;
        active2 = true;
        active3 = false;
        active4 = false;
        active5 = false;
        active6 = false;

        break;
      case 2:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = false;
        active5 = false;
        active6 = false;
        break;
      case 3:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = false;
        active6 = false;
        break;
      case 4:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = true;
        active6 = false;
        break;
      case 5:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;
        active5 = true;
        active6 = true;
        break;
      default:
    }
    return PopScope(
      canPop: false,
      onPopInvokedWithResult: (bool didPop, result) {
        if (didPop) {
          FirestoreService()
              .updateCurrentPage('A31', (activeStep + 1).toString());
          return;
        }
      },
      child: loadingData
          ? Container(
              width: size.width,
              height: size.height,
              color: CustomColors.primaryDark,
              child: const Center(
                  child: CircularProgressIndicator(
                color: CustomColors.tertiary70,
              )))
          : Container(
              color: Colors.black,
              child: SafeArea(
                child: Scaffold(
                  backgroundColor: CustomColors.background,
                  body: Center(
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              width: size.width / 6,
                              height: 5,
                              color: active1
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 6,
                              height: 5,
                              color: active2
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 6,
                              height: 5,
                              color: active3
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 6,
                              height: 5,
                              color: active4
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 6,
                              height: 5,
                              color: active5
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 6,
                              height: 5,
                              color: active6
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                          ],
                        ),
                        content(),
                        Container(
                          margin: const EdgeInsets.only(top: 10, bottom: 10),
                          child: bottomStructure(),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
    );
  }

  Widget topStructure() {
    return Column(
      children: [
        Row(
          children: [
            Container(
              alignment: Alignment.topRight,
              child: IconButton(
                onPressed: () {
                  FirestoreService()
                      .updateCurrentPage('A31', (activeStep + 1).toString())
                      .then((value) => Navigator.of(context).pop());
                },
                icon: const Icon(
                  Icons.close,
                  color: CustomColors.primaryDark,
                ),
              ),
            ),
            const Spacer(),
            favorito(),
          ],
        ),
        Container(
          margin: const EdgeInsets.only(left: 10, right: 10),
          child: Text(
            headerText(),
            textScaler: const TextScaler.linear(1.0),
            textAlign: TextAlign.center,
            style: const TextStyle(
              color: CustomColors.primaryDark,
              fontSize: 24,
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(top: 20, bottom: 20),
          child: header(),
        ),
      ],
    );
  }

  Widget favorito() {
    return StreamBuilder<QuerySnapshot>(
        stream: FirestoreService().favoritos(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasData) {
            return SizedBox(
              width: 50,
              height: 50,
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (context, index) {
                    var document = snapshot.data?.docs[index];
                    if (document != null && document.id == uid) {
                      switch (document['A31']) {
                        case true:
                          return IconButton(
                              icon: Icon(MdiIcons.star,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('A31', false)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Eliminado de favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                        case false:
                          return IconButton(
                              icon: Icon(MdiIcons.starOutline,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('A31', true)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Agregado a favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                      }
                    } else {
                      return Container();
                    }
                    return null;
                  }),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(
                color: Colors.red,
              ),
            );
          }
        });
  }

  Widget bottomStructure() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        nextButton(),
      ],
    );
  }

  Widget nextButton() {
    Size size = MediaQuery.of(context).size;
    if (activeStep >= 1 && activeStep < 6) {
      return SizedBox(
        width: size.width / 1,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        WidgetStateProperty.all<Color>(CustomColors.background),
                    shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: const BorderSide(color: CustomColors.primary)),
                    ),
                  ),
                  onPressed: () {
                    setState(() {
                      activeStep = activeStep - 1;
                    });
                  },
                  child: const Text(
                    'Anterior',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(color: CustomColors.primary),
                  )),
            ),
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor:
                      WidgetStateProperty.all<Color>(CustomColors.primary),
                  shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ),
                  ),
                ),
                onPressed: () {
                  if (activeStep < 5) {
                    setState(() {
                      activeStep++;
                    });
                  } else {
                    FirestoreService().updateCurrentPage('A31', '1');

                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute<void>(
                            builder: (BuildContext context) => const MainPage(
                                  activePage: 1,
                                )),
                        (route) => false);
                  }
                },
                child: Text(
                  buttonText(),
                  textScaler: const TextScaler.linear(1.0),
                  style: const TextStyle(
                      color: CustomColors.background,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      return SizedBox(
        width: size.width / 1.2,
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor:
                WidgetStateProperty.all<Color>(CustomColors.primary),
            shape: WidgetStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              ),
            ),
          ),
          onPressed: () {
            if (activeStep < 5) {
              setState(() {
                activeStep++;
              });
            } else {
              FirestoreService().updateCurrentPage('A31', '1');

              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute<void>(
                      builder: (BuildContext context) => const MainPage(
                            activePage: 1,
                          )),
                  (route) => false);
            }
            /*
            */
          },
          child: Text(
            buttonText(),
            textScaler: const TextScaler.linear(1.0),
            style: const TextStyle(color: CustomColors.background),
          ),
        ),
      );
    }
  }

  Widget header() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      default:
        return Image(
          width: size.width / 3,
          height: size.width / 3,
          fit: BoxFit.scaleDown,
          image: AssetImage(imageHeader()),
        );
    }
  }

  String imageHeader() {
    switch (activeStep) {
      case 0:
        return 'assets/images/consumo-universitario-icon.png';

      default:
        return 'assets/images/consumo-universitario-icon.png';
    }
  }

  String headerText() {
    switch (activeStep) {
      case 0:
        return 'Consumo de alcohol y drogas en el contexto universitario';

      default:
        return 'Consumo de alcohol y drogas en el contexto universitario';
    }
  }

  String buttonText() {
    switch (activeStep) {
      case 0:
        return 'Ver el video';

      case 5:
        return 'Finalizar';

      default:
        return 'Siguiente ';
    }
  }

  Widget content() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 20, right: 20, left: 20, bottom: 30),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            '¿Sabías que 2 de cada 3 estudiantes en universidades y/o centros de educación superior refieren haber experimentado al menos un episodio de embriaguez, equivalente a cinco o más tragos por ocasión?\n\n',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            fontWeight: FontWeight.bold,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                'El ingreso a la universidad implica un proceso de adaptación a un nuevo entorno social que puede ser desconocido y exigente, lo cual en ocasiones puede traer consigo experimentar',
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          TextSpan(
                            text:
                                ' estrés y dificultad para conciliar la vida personal con la académica.\n\n',
                          ),
                          TextSpan(
                            text:
                                'En este contexto, se han identificado factores tales como la influencia social y la expectativa de mantener un alto rendimiento académico, que podrían estar relacionados con el consumo de drogas y alcohol como una estrategia poco saludable para regular el malestar subjetivo.\n\n',
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          TextSpan(
                            text:
                                'Por eso, ten presentes las estrategias de regulación emocional que te ayudarán a promover tu bienestar y salud.\n\n',
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          TextSpan(
                            text:
                                'Ahora te invitamos a ver los siguientes video del Servicio Nacional para la Prevención y Rehabilitación del Consumo de Drogas y Alcohol (SENDA) acerca de la prevención del uso de alcohol y drogas en la educación superior...',
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 1:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    width: kIsWeb ? size.width / 2 : size.width / 1.2,
                    height: size.height / 4,
                    margin: const EdgeInsets.only(top: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: CustomColors.surface,
                    ),
                    child: Stack(
                      alignment: Alignment.topCenter,
                      children: [
                        const Center(
                          child: CircularProgressIndicator(
                            color: CustomColors.orange,
                          ),
                        ),
                        VideoPlayerURL(url: videoPrevencionEdSuperior),
                        kIsWeb
                            ? Container(
                                margin: const EdgeInsets.only(top: 10),
                                child: ElevatedButton(
                                  style: ButtonStyle(
                                    backgroundColor:
                                        WidgetStateProperty.all<Color>(
                                      const Color.fromRGBO(255, 0, 0, 1),
                                    ),
                                  ),
                                  onPressed: () => _launchUrlYT(videoYT1),
                                  child: const Text(
                                    'Ver en YouTube',
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                              )
                            : Container(),
                      ],
                    ),
                  ),
                  Container(
                    width: kIsWeb ? size.width / 2 : size.width / 1.2,
                    height: size.height / 4,
                    margin: const EdgeInsets.only(top: 30),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: CustomColors.surface,
                    ),
                    child: Stack(
                      alignment: Alignment.topCenter,
                      children: [
                        const Center(
                          child: CircularProgressIndicator(
                            color: CustomColors.orange,
                          ),
                        ),
                        VideoPlayerURL(url: videoIntroduccionDrogas),
                        kIsWeb
                            ? Container(
                                margin: const EdgeInsets.only(top: 10),
                                child: ElevatedButton(
                                  style: ButtonStyle(
                                    backgroundColor:
                                        WidgetStateProperty.all<Color>(
                                      const Color.fromRGBO(255, 0, 0, 1),
                                    ),
                                  ),
                                  onPressed: () => _launchUrlYT(videoYT2),
                                  child: const Text(
                                    'Ver en YouTube',
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                              )
                            : Container(),
                      ],
                    ),
                  ),
                  Container(
                    width: size.width,
                    margin: const EdgeInsets.only(top: 15, left: 20, bottom: 5),
                    child: const Text(
                      'Dato útil:',
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(
                        fontSize: 22,
                        color: CustomColors.primary,
                      ),
                    ),
                  ),
                  Container(
                    width: size.width,
                    margin:
                        const EdgeInsets.only(left: 20, bottom: 5, right: 20),
                    child: const Text(
                      'Si quieres conocer más acerca de esta temática te invitamos a visitar la página web de Academia Senda, portal con distintos recursos informativos en torno a las drogas y alcohol en el contexto chileno.',
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(
                        fontSize: 16,
                        color: CustomColors.primary,
                      ),
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin:
                        const EdgeInsets.only(top: 20, bottom: 20, left: 20),
                    child: ElevatedButton.icon(
                      style: ButtonStyle(
                        backgroundColor: WidgetStateProperty.all<Color>(
                            CustomColors.primary80pink),
                        shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                          ),
                        ),
                      ),
                      onPressed: () {
                        _launchUrl();
                      },
                      icon: Icon(MdiIcons.openInNew,
                          color: CustomColors.primaryDark),
                      label: const Text(
                        'Web de Academia Senda',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(color: CustomColors.primaryDark),
                      ), // <-- Text
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 2:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 5, right: 20, left: 20, bottom: 30),
                    child: RichText(
                      text: const TextSpan(
                        text: '',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                '¿Conoces los efectos negativos del abuso de sustancias?\n\n',
                            style: TextStyle(fontSize: 22),
                          ),
                          TextSpan(
                            text:
                                'El uso de sustancias se convierte en un abuso cuando conlleva un malestar significativo y duradero en alguna de las siguientes dimensiones:\n\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Incapacidad para cumplir las obligaciones principales.\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Consumo en situaciones peligrosas como la conducción de vehículos.\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text: 'Problemas legales.\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Consumo a pesar de dificultades sociales y personales.\n\n',
                          ),
                          TextSpan(
                            text:
                                'En el siguiente video queremos contarte sobre cómo prevenir un consumo problemático de alcohol y drogas.',
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    width: kIsWeb ? size.width / 2 : size.width / 1.2,
                    height: size.height / 4,
                    margin: const EdgeInsets.only(top: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: CustomColors.surface,
                    ),
                    child: Stack(
                      alignment: Alignment.topCenter,
                      children: [
                        const Center(
                          child: CircularProgressIndicator(
                            color: CustomColors.orange,
                          ),
                        ),
                        VideoPlayerURL(url: videoConsumoProblematico),
                        kIsWeb
                            ? Container(
                                margin: const EdgeInsets.only(top: 10),
                                child: ElevatedButton(
                                  style: ButtonStyle(
                                    backgroundColor:
                                        WidgetStateProperty.all<Color>(
                                      const Color.fromRGBO(255, 0, 0, 1),
                                    ),
                                  ),
                                  onPressed: () => _launchUrlYT(videoYT3),
                                  child: const Text(
                                    'Ver en YouTube',
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                              )
                            : Container(),
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 20, right: 20, left: 20, bottom: 5),
                    child: RichText(
                      text: const TextSpan(
                        text: '',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                'Este video se desarrolló gracias al financiamiento de la Iniciativa Científica Milenio Centros ANID del Ministerio de Ciencia, Tecnología, Conocimiento e Innovación a través de su programa de Proyección al Medio Externo; y cuenta con el patrocinio del Instituto Nacional de la Juventud.',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );

      case 3:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 5, right: 20, left: 20, bottom: 30),
                    child: RichText(
                      text: const TextSpan(
                        text: '',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                '¿Qué se considera uso intensivo de alcohol o “binge drinking”?\n\n',
                            style: TextStyle(fontSize: 22),
                          ),
                          TextSpan(
                            text:
                                'El consumo intensivo de alcohol se define como beber grandes cantidades de alcohol en un periodo corto de tiempo, elevando la concentración de alcohol en la sangre a un 0.08 %. Cuánto tiempo tome en alcanzar este 0.08 depende de muchos factores, como el género, peso, beber con el estómago vacío, y la rapidez con que se bebe. Un consumo intensivo de alcohol típicamente significa cuatro bebidas alcohólicas para una mujer adulta y cuatro o más para un hombre adulto en un periodo de 2 horas (una bebida alcohólica corresponde, por ejemplo a una lata de cerveza o a una copa de vino). La gente joven requiere menos alcohol dependiendo de su estatura y peso.\n\n',
                          ),
                          TextSpan(
                            text:
                                '¿Cuándo preocuparse por el uso intensivo de alcohol?\n\n',
                            style: TextStyle(fontSize: 22),
                          ),
                          TextSpan(
                            text:
                                'Algunas señales de alerta que pueden indicar que tu consumo de alcohol es algo de lo que debes preocuparte corresponden a:\n\n',
                          ),
                          TextSpan(
                            text:
                                '·  Cuando bebes lo haces en grandes cantidades y/o en un corto periodo de tiempo.',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' Ya no se trata de un error de juicio ocasional, sino que sucede de forma mensual o semanal.\n\n',
                          ),
                          TextSpan(
                            text:
                                '·  El uso intensivo de alcohol se ha vuelto sinónimo de socializar.',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' La idea de beber poco o nada no parece divertida.\n\n',
                          ),
                          TextSpan(
                            text:
                                '·  Empiezas a pensar con anticipación sobre cuándo beberás de forma intensiva.',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' Lo planificas, e incluso puedes desearlo con fuerza.\n\n',
                          ),
                          TextSpan(
                            text: '·  Desarrollas tolerancia.',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' Toma más tiempo de lo usual conseguir el estado que deseas con la bebida.\n\n',
                          ),
                          TextSpan(
                            text: '·  Pierdes el conocimiento.',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' Te despiertas sin saber cuánto bebiste, qué hiciste, o cómo llegaste a tu casa. Las personas te cuentan sobre lo que hiciste mientras estabas bajo los efectos del alcohol, y no reconoces lo que hablan.\n\n',
                          ),
                          TextSpan(
                            text: '·  Tomas más de lo que planeabas.',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' Te dices a ti mismo que tendrás una noche tranquila en cuanto al alcohol o juras que no tomarás más de un cierto monto, pero traspasas ese límite.\n\n',
                          ),
                          TextSpan(
                            text:
                                '·  Te sientes físicamente mal cuando no bebes.',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' Eso puede ser un signo de dependencia física y de abstinencia que se produce cuando no tienes alcohol en tu organismo. Dependiendo de la cantidad de alcohol que beba, los síntomas pueden ser leves o potencialmente mortales y pueden incluir estrés, ansiedad, dificultad para dormir, dolores de cabeza, ansiedad, irritabilidad, sudoración y temblores.\n\n',
                          ),
                          TextSpan(
                            text: '·  Terminas en situaciones de riesgo.',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' Despiertas con moretones inexplicables o visitas la sala de urgencias después de beber mucho. Tal vez conduces en estado de ebriedad o tienes relaciones sexuales con personas que apenas conoces. El alcohol perjudica tu juicio. Así, incluso cuando nada terrible haya pasado aún, considera este comportamiento como una señal de riesgo de que podría ocurrir.\n\n',
                          ),
                          TextSpan(
                            text:
                                'La próxima vez que cuestiones tu forma de beber alcohol, toma una pausa e intenta comprender el momento. Los puedes hacer cuando te diriges a beber o cuando te despiertas preguntándote ¿qué fue lo que hice ayer?. Te puedes preguntar:\n\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text: '¿Hay algo que desee cambiar de cómo bebo?\n',
                            style: TextStyle(fontStyle: FontStyle.italic),
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text: '¿Me arrepiento de beber tanto?\n',
                            style: TextStyle(fontStyle: FontStyle.italic),
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                '¿Por qué creo que quiero o necesito estar ebrio?\n\n',
                            style: TextStyle(fontStyle: FontStyle.italic),
                          ),
                          TextSpan(
                            text:
                                'Ser consciente de tus sentimientos y motivaciones puede animarte a realizar algunos cambios.',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 4:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 5, right: 20, left: 20, bottom: 30),
                    child: RichText(
                      text: const TextSpan(
                        text: '',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                '¿Cómo identificar el mal uso de sustancias en tus círculos cercanos?\n\n',
                            style: TextStyle(fontSize: 22),
                          ),
                          TextSpan(
                            text:
                                'A veces, la preocupación no está situada en nosotrxs mismos si no en quienes nos rodean. Si conoces a alguien que puede estar haciendo un uso abusivo de sustancias hay algunas señales que te pueden indicar la necesidad de ayuda para el consumo problemático. Ten presente que estos signos',
                          ),
                          TextSpan(
                            text:
                                ' no son un diagnóstico de mal uso de sustancias o de un comportamiento abusivo de ellas.',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text: ' Estas señales de alerta son:\n\n',
                          ),
                          TextSpan(
                            text: '·  Cambios en su comportamiento:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' por ejemplo, frecuentemente falta a sus clases, muestra desinterés repentino por sus actividades académicas y/o presenta una baja significativa en sus notas o desempeño académico.\n\n',
                          ),
                          TextSpan(
                            text: '·  Cambios en su salud física:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' presenta falta de energía o motivación, experimenta temblores, dificultad para hablar, o problemas de coordinación, muestra cambios en el apetito, pérdida o ganancia de peso y/o presenta cambios en sus patrones de sueño.\n\n',
                          ),
                          TextSpan(
                            text: '·  Cambios en la apariencia:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' Descuida su apariencia, se observa una falta de interés en la ropa o aseo personal lo que se evidencia, por ejemplo, en olores inusuales en su aliento, cuerpo, o vestimenta.\n\n',
                          ),
                          TextSpan(
                            text: '·  Cambios en la conducta:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' cambios súbitos en las personas con las que se relaciona, lugares que frecuenta y/o las actividades que realiza, cambia la forma de relacionarse con sus amistades o familia (ej. mayor secretismo y aislamiento).\n\n',
                          ),
                          TextSpan(
                            text: '·  Cambios en su estado de ánimo:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' presenta cambios repentinos de humor, irritabilidad, depresión, o arrebatos de ira, tiene periodos inusuales de hiperactividad o agitación, seguido de una baja en su motivación y/o muestra temor, ansiedad, o paranoia, sin aparente razón.\n\n',
                          ),
                          TextSpan(
                            text: '·  Cambios en el manejo del dinero:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' presenta problemas financieros inexplicables, o pedir repentinamente dinero prestado sin explicación y/o comienza a robar o tomar dinero u objetos valiosos que pueden ser transados por sustancias.\n\n',
                          ),
                          TextSpan(
                            text:
                                '·  Cambios en su relación con las sustancias:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' usa drogas o alcohol incluso cuando genera problemas en las relaciones, presenta conductas tales como ocultar drogas o alcohol en distintas partes de su casa, etc.',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 5:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 5, right: 20, left: 20, bottom: 30),
                    child: RichText(
                      text: const TextSpan(
                        text: '',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                '¿Cómo se relacionan el uso de sustancias y los problemas de salud mental?\n\n',
                            style: TextStyle(fontSize: 22),
                          ),
                          TextSpan(
                            text:
                                'Muchas personas que lidian con el mal uso de sustancias también enfrentan problemas de salud mental. A veces, el uso de drogas y alcohol se utiliza como una mala forma de afrontar estos problemas; en otras ocasiones, el uso de drogas puede aumentar la probabilidad de sentir ansiedad o síntomas depresivos.\n\n',
                          ),
                          TextSpan(
                            text:
                                'Dado que los problemas de salud mental y el uso de sustancias se encuentran estrechamente relacionados ¿una condición causa la otra? No necesariamente. Los problemas de salud mental y los trastornos por uso de sustancias comparten algunas causas comunes, incluida la genética, el desarrollo del cerebro y las experiencias traumáticas en las primeras etapas de la vida, lo que ayuda a explicar por qué ocurren juntos con tanta frecuencia.\n\n',
                          ),
                          TextSpan(
                            text:
                                '¿Cómo el consumo de alcohol se relaciona con la depresión?\n\n',
                            style: TextStyle(fontSize: 22),
                          ),
                          TextSpan(
                            text:
                                'Beber mucho alcohol a menudo aumenta los síntomas depresivos que una persona puede estar atravesando. Ello puede conducir a un mayor uso de alcohol u otras drogas como una forma de reducir el empeoramiento de estos síntomas, generando un círculo vicioso y que afecta negativamente la calidad de vida.\nAquí te compartimos algunas formas en cómo el alcohol puede empeorar los síntomas depresivos:\n\n',
                          ),
                          TextSpan(
                            text: '·  Interfiere en el sueño:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' los problemas de alcohol están fuertemente ligados a la depresión. El uso de alcohol puede afectar negativamente tus patrones de sueño, lo que puede incrementar los síntomas de depresión como fatiga o la dificultad para concentrarse.\n\n',
                          ),
                          TextSpan(
                            text: '·  Interfiere con la medicación:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' si estás en un tratamiento farmacológico para tratar la depresión, el alcohol reduce la efectividad de los antidepresivos y puede aumentar otros efectos secundarios como somnolencia, lo que puede ser peligroso.\n\n',
                          ),
                          TextSpan(
                            text:
                                '·  Puede aumentar comportamientos de riesgo:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' además de empeorar los síntomas de depresión, beber alcohol puede disminuir tus inhibiciones e incrementar su impulsividad, lo cual puede llevarte a situaciones peligrosas que pueden impactar en tu ánimo.\n\n',
                          ),
                          TextSpan(
                            text: '·  Puede incrementar la conducta suicida:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' el mal uso de alcohol mientras estás deprimido aumenta el riesgo de intentos de suicidio.\n\n',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      default:
        return Container();
    }
  }
}
