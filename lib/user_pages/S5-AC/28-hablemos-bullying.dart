// ignore_for_file: file_names, avoid_print

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:seguimiento/colors.dart';
import 'package:seguimiento/user_pages/MainPage.dart';
import 'package:seguimiento/video-player.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../firebase.dart';

class A28 extends StatefulWidget {
  const A28({
    Key? key,
  }) : super(key: key);

  @override
  State<A28> createState() => _A28State();
}

class _A28State extends State<A28> {
  final Uri videoYT = Uri.parse('https://www.youtube.com/watch?v=Gi9g4bYQXXo');

  String videoBullying =
      'https://firebasestorage.googleapis.com/v0/b/maquetadatos-5dee1.appspot.com/o/videos%2Fhablemos-bullying.mp4?alt=media&token=d328c300-95e3-44e3-8fdf-cb522c62c539';
  String uid = FirebaseAuth.instance.currentUser!.uid;
  bool loadingData = true;
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('page-control');
  String activeStepData = '';
  void getActivePageStored() {
    collectionReference.doc(uid).get().then((value) {
      var fields = value;
      activeStepData = fields['A28'];
      setState(() {
        if (int.parse(activeStepData) < 6) {
          activeStep = int.parse(activeStepData) - 1;
          Future.delayed(const Duration(milliseconds: 500), () {
            setState(() {
              loadingData = false;
            });
          });
        }
      });
    });
  }

  ScrollController scrollController = ScrollController();

  bool favorite = false;
  CollectionReference favoriteData =
      FirebaseFirestore.instance.collection('favoritos');
  void getFavData() {
    favoriteData.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        favorite = fields['A28'];
      });
    }).catchError((err) {
      print('Error: $err');
    });
  }

  int activeStep = 0;
  @override
  void initState() {
    super.initState();

    setState(() {
      SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);
    });

    getActivePageStored();
    getFavData();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> _launchUrl(url) async {
    if (!await launchUrl(url)) {
      throw Exception('Could not launch $url');
    }
  }

  bool active1 = false;
  bool active2 = false;
  final situacion = TextEditingController();
  final diario = TextEditingController();
  @override
  Widget build(BuildContext context) {
    setState(() {
      SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);
    });

    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        active1 = true;
        active2 = false;

        break;
      case 1:
        active1 = true;
        active2 = true;

        break;

      default:
    }
    return PopScope(
      canPop: false,
      onPopInvokedWithResult: (bool didPop, result) {
        if (didPop) {
          FirestoreService()
              .updateCurrentPage('A28', (activeStep + 1).toString());
          return;
        }
      },
      child: loadingData
          ? Container(
              width: size.width,
              height: size.height,
              color: CustomColors.primaryDark,
              child: const Center(
                  child: CircularProgressIndicator(
                color: CustomColors.tertiary70,
              )))
          : Container(
              color: Colors.black,
              child: SafeArea(
                child: Scaffold(
                  backgroundColor: CustomColors.background,
                  body: Center(
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              width: size.width / 2,
                              height: 5,
                              color: active1
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 2,
                              height: 5,
                              color: active2
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                          ],
                        ),
                        content(),
                        Container(
                          margin: const EdgeInsets.only(top: 10, bottom: 10),
                          child: bottomStructure(),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
    );
  }

  Widget topStructure() {
    return Column(
      children: [
        Row(
          children: [
            Container(
              alignment: Alignment.topRight,
              child: IconButton(
                onPressed: () {
                  FirestoreService()
                      .updateCurrentPage('A28', (activeStep + 1).toString())
                      .then((value) => Navigator.of(context).pop());
                },
                icon: const Icon(
                  Icons.close,
                  color: CustomColors.primaryDark,
                ),
              ),
            ),
            const Spacer(),
            favorito(),
          ],
        ),
        Text(
          headerText(),
          textScaler: const TextScaler.linear(1.0),
          textAlign: TextAlign.center,
          style: const TextStyle(
            color: CustomColors.primaryDark,
            fontSize: 24,
          ),
        ),
        Container(
          margin: const EdgeInsets.only(top: 20, bottom: 20),
          child: header(),
        ),
      ],
    );
  }

  Widget favorito() {
    return StreamBuilder<QuerySnapshot>(
        stream: FirestoreService().favoritos(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasData) {
            return SizedBox(
              width: 50,
              height: 50,
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (context, index) {
                    var document = snapshot.data?.docs[index];
                    if (document != null && document.id == uid) {
                      switch (document['A28']) {
                        case true:
                          return IconButton(
                              icon: Icon(MdiIcons.star,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('A28', false)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Eliminado de favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                        case false:
                          return IconButton(
                              icon: Icon(MdiIcons.starOutline,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('A28', true)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Agregado a favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                      }
                    } else {
                      return Container();
                    }
                    return null;
                  }),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(
                color: Colors.red,
              ),
            );
          }
        });
  }

  Widget bottomStructure() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        nextButton(),
      ],
    );
  }

  Widget nextButton() {
    Size size = MediaQuery.of(context).size;
    if (activeStep >= 1 && activeStep < 2) {
      return SizedBox(
        width: size.width / 1,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        WidgetStateProperty.all<Color>(CustomColors.background),
                    shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: const BorderSide(color: CustomColors.primary)),
                    ),
                  ),
                  onPressed: () {
                    setState(() {
                      activeStep = activeStep - 1;
                    });
                  },
                  child: const Text(
                    'Anterior',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(color: CustomColors.primary),
                  )),
            ),
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor:
                      WidgetStateProperty.all<Color>(CustomColors.primary),
                  shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ),
                  ),
                ),
                onPressed: () {
                  if (activeStep < 1) {
                    setState(() {
                      activeStep++;
                    });
                  } else {
                    FirestoreService().updateCurrentPage('A28', '1');

                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute<void>(
                            builder: (BuildContext context) => const MainPage(
                                  activePage: 1,
                                )),
                        (route) => false);
                  }
                },
                child: Text(
                  buttonText(),
                  textScaler: const TextScaler.linear(1.0),
                  style: const TextStyle(
                      color: CustomColors.background,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      return SizedBox(
        width: size.width / 1.2,
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor:
                WidgetStateProperty.all<Color>(CustomColors.primary),
            shape: WidgetStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              ),
            ),
          ),
          onPressed: () {
            if (activeStep < 1) {
              setState(() {
                activeStep++;
              });
            } else {
              FirestoreService().updateCurrentPage('A28', '1');

              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute<void>(
                      builder: (BuildContext context) => const MainPage(
                            activePage: 1,
                          )),
                  (route) => false);
            }
            /*
            */
          },
          child: Text(
            buttonText(),
            textScaler: const TextScaler.linear(1.0),
            style: const TextStyle(color: CustomColors.background),
          ),
        ),
      );
    }
  }

  Widget header() {
    Size size = MediaQuery.of(context).size;
    switch (activeStep) {
      default:
        return Image(
          width: size.width / 3,
          height: size.width / 3,
          fit: BoxFit.scaleDown,
          image: AssetImage(imageHeader()),
        );
    }
  }

  String imageHeader() {
    switch (activeStep) {
      case 0:
        return 'assets/images/hablemos-bullying-icon.png';

      default:
        return 'assets/images/hablemos-bullying-icon.png';
    }
  }

  String headerText() {
    switch (activeStep) {
      case 0:
        return 'Video Hablemos de bullying';

      default:
        return 'Video Hablemos de bullying';
    }
  }

  String buttonText() {
    switch (activeStep) {
      case 0:
        return 'Ver el video';

      case 1:
        return 'Finalizar';

      default:
        return 'Siguiente ';
    }
  }

  Widget content() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 20, right: 20, left: 20, bottom: 30),
                    child: RichText(
                      text: const TextSpan(
                        text: '¿Sabes qué es el bullying?\n',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            fontWeight: FontWeight.bold,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                'El bullying es una acción de agresión con la intención de hacer daño o herir a otros que se repite en el tiempo y en la cual se presenta un desequilibrio de poder entre quien sufre la agresión y quien la ejerce.\n\n',
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          TextSpan(
                            text:
                                'El bullying genera un impacto negativo, a corto y largo plazo, en las personas que lo sufren. Es por esto que en este video queremos mostrarte cómo enfrentar y prevenir el bullying.',
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );

      case 1:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    width: kIsWeb ? size.width / 2 : size.width / 1.2,
                    height: size.height / 4,
                    margin: const EdgeInsets.only(top: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: CustomColors.surface,
                    ),
                    child: Stack(
                      alignment: Alignment.topCenter,
                      children: [
                        const Center(
                          child: CircularProgressIndicator(
                            color: CustomColors.orange,
                          ),
                        ),
                        VideoPlayerURL(url: videoBullying),
                        kIsWeb
                            ? Container(
                                margin: const EdgeInsets.only(top: 10),
                                child: ElevatedButton(
                                  style: ButtonStyle(
                                    backgroundColor:
                                        WidgetStateProperty.all<Color>(
                                      const Color.fromRGBO(255, 0, 0, 1),
                                    ),
                                  ),
                                  onPressed: () => _launchUrl(videoYT),
                                  child: const Text(
                                    'Ver en YouTube',
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                              )
                            : Container(),
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 20, right: 20, left: 20, bottom: 5),
                    child: RichText(
                      text: const TextSpan(
                        text: '',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                'Este video se desarrolló gracias al financiamiento de la Iniciativa Científica Milenio Centros ANID del Ministerio de Ciencia, Tecnología, Conocimiento e Innovación a través de su programa de Proyección al Medio Externo; y cuenta con el patrocinio del Instituto Nacional de la Juventud.',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      default:
        return Container();
    }
  }
}
