// ignore_for_file: file_names, avoid_print

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:seguimiento/colors.dart';
import 'package:seguimiento/user_pages/MainPage.dart';
import 'package:seguimiento/video-player.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../firebase.dart';

class A26 extends StatefulWidget {
  const A26({
    Key? key,
  }) : super(key: key);

  @override
  State<A26> createState() => _A26State();
}

class _A26State extends State<A26> {
  final Uri _url = Uri.parse(
      'https://www.apa.org/topics/psychotherapy/entendiendo-la-psicoterapia#:~:text=La%20psicoterapia%20es%20un%20tratamiento,alguien%20objetivo%2C%20neutral%20e%20imparcial');
  final Uri videoYT = Uri.parse('https://www.youtube.com/watch?v=ke1cxwM10jA');
  String videoPrimeraConsulta =
      'https://firebasestorage.googleapis.com/v0/b/maquetadatos-5dee1.appspot.com/o/videos%2Fprimera-consulta.mp4?alt=media&token=3cd739be-03ee-4896-a16a-fc663b7f5dc5';
  String uid = FirebaseAuth.instance.currentUser!.uid;
  bool loadingData = true;
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('page-control');
  String activeStepData = '';
  void getActivePageStored() {
    collectionReference.doc(uid).get().then((value) {
      var fields = value;
      activeStepData = fields['A26'];
      setState(() {
        if (int.parse(activeStepData) < 6) {
          activeStep = int.parse(activeStepData) - 1;
          Future.delayed(const Duration(milliseconds: 500), () {
            setState(() {
              loadingData = false;
            });
          });
        }
      });
    });
  }

  ScrollController scrollController = ScrollController();
  bool favorite = false;
  CollectionReference favoriteData =
      FirebaseFirestore.instance.collection('favoritos');
  void getFavData() {
    favoriteData.doc(uid).get().then((value) {
      var fields = value;
      setState(() {
        favorite = fields['A26'];
      });
    }).catchError((err) {
      print('Error: $err');
    });
  }

  int activeStep = 0;
  @override
  void initState() {
    super.initState();
    setState(() {
      SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);
    });
    getActivePageStored();
    getFavData();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> _launchUrl() async {
    if (!await launchUrl(_url)) {
      throw Exception('Could not launch $_url');
    }
  }

  Future<void> _launchUrlYT(url) async {
    if (!await launchUrl(url)) {
      throw Exception('Could not launch $url');
    }
  }

  bool active1 = false;
  bool active2 = false;
  bool active3 = false;
  bool active4 = false;

  @override
  Widget build(BuildContext context) {
    setState(() {
      SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: []);
    });
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        active1 = true;
        active2 = false;
        active3 = false;
        active4 = false;
        break;
      case 1:
        active1 = true;
        active2 = true;
        active3 = false;
        active4 = false;

        break;
      case 2:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = false;

        break;
      case 3:
        active1 = true;
        active2 = true;
        active3 = true;
        active4 = true;

        break;
      default:
    }
    return PopScope(
      canPop: false,
      onPopInvokedWithResult: (bool didPop, result) {
        if (didPop) {
          FirestoreService()
              .updateCurrentPage('A26', (activeStep + 1).toString());
          return;
        }
      },
      child: loadingData
          ? Container(
              width: size.width,
              height: size.height,
              color: CustomColors.primaryDark,
              child: const Center(
                  child: CircularProgressIndicator(
                color: CustomColors.tertiary70,
              )))
          : Container(
              color: Colors.black,
              child: SafeArea(
                child: Scaffold(
                  backgroundColor: CustomColors.background,
                  body: Center(
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                              width: size.width / 4,
                              height: 5,
                              color: active1
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 4,
                              height: 5,
                              color: active2
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 4,
                              height: 5,
                              color: active3
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                            Container(
                              width: size.width / 4,
                              height: 5,
                              color: active4
                                  ? CustomColors.primary2
                                  : Colors.black,
                              child: const Text(''),
                            ),
                          ],
                        ),
                        content(),
                        Container(
                          margin: const EdgeInsets.only(top: 10, bottom: 10),
                          child: bottomStructure(),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
    );
  }

  Widget topStructure() {
    return Column(
      children: [
        Row(
          children: [
            Container(
              alignment: Alignment.topRight,
              child: IconButton(
                onPressed: () {
                  FirestoreService()
                      .updateCurrentPage('A26', (activeStep + 1).toString())
                      .then((value) => Navigator.of(context).pop());
                },
                icon: const Icon(
                  Icons.close,
                  color: CustomColors.primaryDark,
                ),
              ),
            ),
            const Spacer(),
            favorito(),
          ],
        ),
        Text(
          headerText(),
          textScaler: const TextScaler.linear(1.0),
          textAlign: TextAlign.center,
          style: const TextStyle(
            color: CustomColors.primaryDark,
            fontSize: 24,
          ),
        ),
        Container(
          margin: const EdgeInsets.only(top: 20, bottom: 20),
          child: header(),
        ),
      ],
    );
  }

  Widget favorito() {
    return StreamBuilder<QuerySnapshot>(
        stream: FirestoreService().favoritos(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasData) {
            return SizedBox(
              width: 50,
              height: 50,
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (context, index) {
                    var document = snapshot.data?.docs[index];
                    if (document != null && document.id == uid) {
                      switch (document['A26']) {
                        case true:
                          return IconButton(
                              icon: Icon(MdiIcons.star,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('A26', false)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Eliminado de favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                        case false:
                          return IconButton(
                              icon: Icon(MdiIcons.starOutline,
                                  color: CustomColors.primaryDark),
                              onPressed: () {
                                FirestoreService()
                                    .setFavorito('A26', true)
                                    .then((value) => Fluttertoast.showToast(
                                        msg: "Agregado a favoritos",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        textColor: Colors.white,
                                        fontSize: 16.0));
                              });
                      }
                    } else {
                      return Container();
                    }
                    return null;
                  }),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(
                color: Colors.red,
              ),
            );
          }
        });
  }

  Widget bottomStructure() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        nextButton(),
      ],
    );
  }

  Widget nextButton() {
    Size size = MediaQuery.of(context).size;
    if (activeStep >= 1 && activeStep < 4) {
      return SizedBox(
        width: size.width / 1,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor:
                        WidgetStateProperty.all<Color>(CustomColors.background),
                    shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: const BorderSide(color: CustomColors.primary)),
                    ),
                  ),
                  onPressed: () {
                    setState(() {
                      activeStep = activeStep - 1;
                    });
                  },
                  child: const Text(
                    'Anterior',
                    textScaler: TextScaler.linear(1.0),
                    style: TextStyle(color: CustomColors.primary),
                  )),
            ),
            SizedBox(
              width: size.width / 2.5,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor:
                      WidgetStateProperty.all<Color>(CustomColors.primary),
                  shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ),
                  ),
                ),
                onPressed: () {
                  if (activeStep < 3) {
                    setState(() {
                      activeStep++;
                    });
                  } else {
                    FirestoreService().updateCurrentPage('A26', '1');

                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute<void>(
                            builder: (BuildContext context) => const MainPage(
                                  activePage: 1,
                                )),
                        (route) => false);
                  }
                },
                child: Text(
                  buttonText(),
                  textScaler: const TextScaler.linear(1.0),
                  style: const TextStyle(
                      color: CustomColors.background,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      return SizedBox(
        width: size.width / 1.2,
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor:
                WidgetStateProperty.all<Color>(CustomColors.primary),
            shape: WidgetStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0),
              ),
            ),
          ),
          onPressed: () {
            if (activeStep < 3) {
              setState(() {
                activeStep++;
              });
            } else {
              FirestoreService().updateCurrentPage('A26', '1');

              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute<void>(
                      builder: (BuildContext context) => const MainPage(
                            activePage: 1,
                          )),
                  (route) => false);
            }
            /*
            */
          },
          child: Text(
            buttonText(),
            textScaler: const TextScaler.linear(1.0),
            style: const TextStyle(color: CustomColors.background),
          ),
        ),
      );
    }
  }

  Widget header() {
    Size size = MediaQuery.of(context).size;
    switch (activeStep) {
      default:
        return Image(
          width: size.width / 3,
          height: size.width / 3,
          fit: BoxFit.scaleDown,
          image: AssetImage(imageHeader()),
        );
    }
  }

  String imageHeader() {
    switch (activeStep) {
      case 0:
        return 'assets/images/busqueda-ayuda-icon.png';

      default:
        return 'assets/images/busqueda-ayuda-icon.png';
    }
  }

  String headerText() {
    switch (activeStep) {
      case 0:
        return 'Búsqueda de ayuda: Primera consulta en salud mental';

      default:
        return 'Búsqueda de ayuda: Primera consulta en salud mental';
    }
  }

  String buttonText() {
    switch (activeStep) {
      case 0:
        return 'Ver el video';

      case 3:
        return 'Finalizar';

      default:
        return 'Siguiente ';
    }
  }

  Widget content() {
    Size size = MediaQuery.of(context).size;

    switch (activeStep) {
      case 0:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 20, right: 20, left: 20, bottom: 30),
                    child: RichText(
                      text: const TextSpan(
                        text: '',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            fontWeight: FontWeight.bold,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                'Muchas personas en el mundo sufren de algún problema de salud mental y hay profesionales que pueden ayudar.\n\n',
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          TextSpan(
                            text:
                                'En el siguiente video queremos mostrarte algunos aspectos a considerar respecto de cómo debiera ser una primera consulta con un o una profesional de salud mental.',
                            style: TextStyle(
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 1:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    width: kIsWeb ? size.width / 2 : size.width / 1.2,
                    height: size.height / 4,
                    margin: const EdgeInsets.only(top: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: CustomColors.surface,
                    ),
                    child: Stack(
                      alignment: Alignment.topCenter,
                      children: [
                        const Center(
                          child: CircularProgressIndicator(
                            color: CustomColors.orange,
                          ),
                        ),
                        VideoPlayerURL(url: videoPrimeraConsulta),
                        kIsWeb
                            ? Container(
                                margin: const EdgeInsets.only(top: 10),
                                child: ElevatedButton(
                                  style: ButtonStyle(
                                    backgroundColor:
                                        WidgetStateProperty.all<Color>(
                                      const Color.fromRGBO(255, 0, 0, 1),
                                    ),
                                  ),
                                  onPressed: () => _launchUrlYT(videoYT),
                                  child: const Text(
                                    'Ver en YouTube',
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                              )
                            : Container(),
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 20, right: 20, left: 20, bottom: 5),
                    child: RichText(
                      text: const TextSpan(
                        text: '',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                'Este video fue creado gracias al financiamiento de la Iniciativa Científica Milenio del programa de la Agencia Nacional de Investigación y Desarrollo (ANID), perteneciente al Ministerio de Ciencia, Tecnología, Conocimiento e Innovación, a través de su programa de Proyección al Medio Externo.',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 2:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 5, right: 20, left: 20, bottom: 30),
                    child: RichText(
                      text: const TextSpan(
                        text:
                            'A continuación, puedes encontrar una serie de preguntas frecuentes en torno a la primera atención en psicoterapia:\n\n',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text: '¿Qué es la psicoterapia?\n\n',
                            style: TextStyle(fontSize: 22),
                          ),
                          TextSpan(
                            text:
                                'En la psicoterapia, los profesionales de salud mental aplican procedimientos científicamente válidos para la creación de hábitos más sanos y efectivos.\nImplica un trabajo colaborativo basado en el diálogo, en un espacio de apoyo objetivo, neutral e imparcial.\n\n',
                          ),
                          TextSpan(
                            text:
                                '¿Cuándo considerar psicoterapia como opción?\n\n',
                            style: TextStyle(fontSize: 22),
                          ),
                          TextSpan(
                            text: 'No hay una única razón',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' para requerir psicoterapia, sin embargo, todas tienen en común el reconocimiento de un malestar subjetivo que interfiere en la vida cotidiana y/o en las relaciones con otrxs.\n\n',
                          ),
                          TextSpan(
                            text:
                                '¿Qué puedo esperar de la primera sesión?\n\n',
                            style: TextStyle(fontSize: 22),
                          ),
                          TextSpan(
                            text: '·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Una sesión típica de psicoterapia dura entre 45 y 50 minutos.\n\n',
                          ),
                          TextSpan(
                            text: '·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'La sesión puede ser en persona, de forma telemática (ej.: reunión virtual), o por teléfono.\n\n',
                          ),
                          TextSpan(
                            text: '·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Es normal sentir ansiedad en las primeras sesiones, pero descuida, los profesionales de salud mental tienen experiencia suficiente para facilitar el diálogo.\n\n',
                          ),
                          TextSpan(
                            text: '·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'En estas primeras sesiones, el objetivo es que por una parte,',
                          ),
                          TextSpan(
                            text: ' el o la terapeuta pueda conocerte;',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text: ' y por otra, que puedas',
                          ),
                          TextSpan(
                            text: ' evaluar tu comodidad',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' con el o la profesional de salud mental.\n\n',
                          ),
                          TextSpan(
                            text: '·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Generalmente, la primera sesión comienza abordando las razones que te llevaron a consultar por psicoterapia, y posteriormente, se te consultará por antecedentes personales, familiares, sociales, entre otros. Esto podría tomar más de una sesión.\n\n',
                          ),
                          TextSpan(
                            text: '·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text: 'Ten presente que',
                          ),
                          TextSpan(
                            text:
                                ' no hay conflicto alguno si prefieres comentarle que no quieres hablar de algún tema.\n\n',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text: '·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Una buena forma de evaluar tu comodidad con el o la terapeuta, es haciéndole preguntas sobre su experiencia u otra información clave que para ti puede ser importante. Algunas preguntas pueden ser:\n',
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                '¿Con qué tipo de persona y/o problemática de salud mental tienes mayor experiencia trabajando?\n',
                            style: TextStyle(fontStyle: FontStyle.italic),
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                '¿Qué tipo de enfoque utilizas en psicoterapia? ¿Hay enfoques o modalidades específicas que utilices?\n',
                            style: TextStyle(fontStyle: FontStyle.italic),
                          ),
                          TextSpan(
                            text: '   ·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                '¿Podrías darme una idea de lo que podría esperar que ocurra en las próximas sesiones?\n\n',
                            style: TextStyle(fontStyle: FontStyle.italic),
                          ),
                          TextSpan(
                            text: '·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Ya teniendo suficientes antecedentes, trabajarán conjuntamente en',
                          ),
                          TextSpan(
                            text: ' acordar objetivos',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' y plan de acompañamiento terapéutico. A medida que el proceso avance, irás sintiendo mayor confianza y podrás marcar tu propio ritmo sobre cómo abordar los temas.\n\n',
                          ),
                          TextSpan(
                            text: '·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'También conforme avance el proceso, tus objetivos pueden ir cambiando según tus necesidades.\n\n',
                          ),
                          TextSpan(
                            text: '·  ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                'Al término de unas cuantas sesiones, tendrás una nueva perspectiva de su problema, un plan de acción y una renovada sensación de esperanza.\n\n',
                          ),
                          TextSpan(
                            text: '¿Cómo encontrar un terapeuta?\n\n',
                            style: TextStyle(fontSize: 22),
                          ),
                          TextSpan(
                            text:
                                'Si tienes claridad sobre desear comenzar un proceso de psicoterapia pero necesitas orientación para encontrar al profesional adecuado para ti, te invitamos a revisar las siguientes recomendaciones:\n\n',
                          ),
                          TextSpan(
                            text: '·  Considera tus razones:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' piensa acerca de los motivos para iniciar psicoterapia. Esto puede ayudarte a identificar un terapeuta que se especialice en lo que quieras abordar.\n\n',
                          ),
                          TextSpan(
                            text: '·  Busca referencias:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' puedes preguntar a personas cercanas acerca de su experiencia asistiendo a psicoterapia. También puedes consultar con las unidades estudiantiles o de salud de tu universidad.\n\n',
                          ),
                          TextSpan(
                            text: '·  Aborda aspectos logísticos:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' elementos como el costo, ubicación y tiempo de traslado pueden ser útiles de considerar al momento de elegir a tu terapeuta.\n\n',
                          ),
                          TextSpan(
                            text: '·  Evalúa tu comodidad:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' es importante encontrar un terapeuta con quien te sientas cómodo. Puedes tener una conversación por teléfono o una sesión introductoria para analizar si te sientes bien con el/la terapeuta.',
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      case 3:
        Future.delayed(const Duration(milliseconds: 50), () {
          scrollController.jumpTo(0.0);
        });

        return Expanded(
          child: RawScrollbar(
            radius: const Radius.circular(20),
            thickness: 5,
            thumbColor: CustomColors.primary2,
            controller: scrollController,
            thumbVisibility: true,
            child: SingleChildScrollView(
              controller: scrollController,
              child: Column(
                children: [
                  topStructure(),
                  Container(
                    margin: const EdgeInsets.only(
                        top: 5, right: 20, left: 20, bottom: 30),
                    child: RichText(
                      text: const TextSpan(
                        text: '',
                        style: TextStyle(
                            fontSize: 16,
                            color: CustomColors.primary,
                            height: 1.5),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                                '¿Cuál es la diferencia entre psicoterapia y hablar con una amistad?\n\n',
                            style: TextStyle(fontSize: 22),
                          ),
                          TextSpan(
                            text:
                                'Puede suceder que te preguntes ¿por qué necesito ir a psicoterapia cuando puedo simplemente hablarlo con mis amistades?. Tener una fuerte red de apoyo para sostenerse al estar atravesando un momento difícil es importante. Sin embargo, un profesional de salud mental está capacitado para entregarte un',
                          ),
                          TextSpan(
                            text: ' apoyo más especializado,',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' especialmente si estás lidiando con un problema de salud mental. De esta forma, hablar con un terapeuta difiere de hablar con amistades o cercanos en:\n\n',
                          ),
                          TextSpan(
                            text: '·  Capacitación:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' un terapeuta cuenta con un entrenamiento especializado para reconocer y tratar el malestar emocional y problemas de salud mental. Una amistad puede ser un gran oyente, pero no poseen la capacitación necesaria para ayudarte a resolver todas las complejidades psicológicas y emocionales que resultan de trauma, estrés prolongado, o patrones de comportamiento no saludables.\n\n',
                          ),
                          TextSpan(
                            text: '·  Confidencialidad:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' esperamos que nuestras amistades respeten nuestra privacidad, pero el único momento en que nuestra confidencialidad está garantizada es cuando conversamos con un profesional de salud mental. Los terapeutas cuentan con estándares legales y profesionales de proteger toda información discutida en sesión, a menos que exista un riesgo de daño a sí mismx u otros.\n\n',
                          ),
                          TextSpan(
                            text: '·  Objetividad:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' cuando le pedimos consejo a nuestras amistades, sus opiniones están influidas por la relación y experiencias compartidas de nuestras vidas. Los terapeutas pueden ofrecer una perspectiva externa y ayudarnos a pensar a través de diferentes puntos de vista.\n\n',
                          ),
                          TextSpan(
                            text: '·  Atención:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' una amistad saludable es una calle de doble sentido: cada uno expresa sus sentimientos y hablan acerca de sus problemas. Es importante dedicar el mismo tiempo y atención a cada persona en una amistad, pero el trabajo de un terapeuta es prestarte completa atención. Sería injusto decirle a una amistad que no hable de sus problemas, mientras que un terapeuta puede ayudarte a enfocarte en los problemas más complicados que necesitan completa atención.\n\n',
                          ),
                          TextSpan(
                            text:
                                '¿Qué cosas afectan el éxito de la psicoterapia?\n\n',
                            style: TextStyle(fontSize: 22),
                          ),
                          TextSpan(
                            text:
                                'Hay algunos ingredientes clave para una experiencia terapéutica exitosa. Algunos están en nuestro control como clientes, y otros no, pero comprenderlos pueden ayudarte a conocer cómo prepararte para terapia y para su éxito, el cual consiste principalmente en:\n\n',
                          ),
                          TextSpan(
                            text: '·  Disposición y compromiso con el cambio:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' la terapia comienza cuando estás listo para ella. Para las personas que no desean ir a terapia puede ser difícil sentirse que se está ganando algo cuando no se quiere estar ahí. Sin embargo, si has elegido comprometerte (ej.: asistes a terapia a tiempo; participas; realizas las “tareas” fuera de las sesiones, etc.), verás los beneficios que la terapia te puede entregar.\n\n',
                          ),
                          TextSpan(
                            text: '·  Recursos financieros y logísticos:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' si no puedes pagar, o conseguir transporte para ir y volver, o gestionar otros aspectos logísticos de las sesiones regulares de terapia, puede ser más difícil alcanzar logros. Algunos de estos elementos pueden estar en tu control, mientras que otros no lo estarán. Esto puede ser una barrera para acceder a tratamiento, pero conocer tus limitaciones puede ayudarte a encontrar una opción de tratamiento que se ajuste a tus posibilidades ¡Pero no te desanimes! existen diversas redes y recursos de ayuda en salud mental a los que podrás acceder si lo requieres.\n\n',
                          ),
                          TextSpan(
                            text: '·  Ajuste cliente- terapeuta:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' la relación cliente-terapeuta es una que afecta la calidad de la terapia y su posible éxito. Si la relación es forzada, o hay incompatibilidades, la terapia se podría sentir más agotadora que útil.\n\n',
                          ),
                          TextSpan(
                            text: '·  Cualificaciones del terapeuta:',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          TextSpan(
                            text:
                                ' parte de la adaptación cliente-terapeuta es si su experticia se ajusta a tus necesidades. Dependiendo de sus cualificaciones, especializaciones, y experiencia, pueden ayudarte a conseguir tus objetivos, o les puede faltar la experiencia para ayudarte. Por ejemplo, si tu objetivo es sanar una experiencia traumática, busca un terapeuta con entrenamiento en tratamiento de experiencias traumáticas.\n\n',
                          ),
                          TextSpan(
                            text:
                                'Y recuerda siempre que ¡hay profesionales que están preparados para ayudarte si lo necesitas!',
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    width: size.width,
                    margin: const EdgeInsets.only(top: 25, left: 20, bottom: 5),
                    child: const Text(
                      '¿Quieres saber más?',
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(
                        fontSize: 22,
                        color: CustomColors.primary,
                      ),
                    ),
                  ),
                  Container(
                    width: size.width,
                    margin:
                        const EdgeInsets.only(left: 20, bottom: 5, right: 20),
                    child: const Text(
                      'Revisa el contenido que la Sociedad Americana de Psicología (APA) ha preparado en torno a este tema.',
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(
                        fontSize: 16,
                        color: CustomColors.primary,
                      ),
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin:
                        const EdgeInsets.only(top: 20, bottom: 20, left: 20),
                    child: ElevatedButton.icon(
                      style: ButtonStyle(
                        backgroundColor: WidgetStateProperty.all<Color>(
                            CustomColors.primary80pink),
                        shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                          ),
                        ),
                      ),
                      onPressed: () {
                        _launchUrl();
                      },
                      icon: Icon(MdiIcons.openInNew,
                          color: CustomColors.primaryDark),
                      label: const Text(
                        'APA',
                        textScaler: TextScaler.linear(1.0),
                        style: TextStyle(color: CustomColors.primaryDark),
                      ), // <-- Text
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      default:
        return Container();
    }
  }
}
