import 'package:cloud_firestore/cloud_firestore.dart';

class ChatPageProvider {
  final FirebaseFirestore firebaseFirestore;

  ChatPageProvider({required this.firebaseFirestore});

  Future<void> updateDataFirestore(
      String collectionPath, String path, Map<String, String> dataNeedUpdate) {
    return firebaseFirestore
        .collection(collectionPath)
        .doc(path)
        .update(dataNeedUpdate);
  }

  Stream<QuerySnapshot> getUsersChat(String pathCollection) {
    return firebaseFirestore.collection(pathCollection).snapshots();
  }
}
