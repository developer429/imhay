// ignore_for_file: deprecated_member_use

import 'dart:async';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';
import 'package:seguimiento/chatlogics/models_and_others/chat_provider.dart';
import 'package:seguimiento/chatlogics/models_and_others/firestore_constants.dart';
import 'package:seguimiento/chatlogics/loading_view.dart';
import 'package:seguimiento/chatlogics/models_and_others/message_chat.dart';
import 'package:seguimiento/colors.dart';
import 'package:seguimiento/firebase.dart';
import 'package:seguimiento/notifications_cloud.dart';
import 'package:seguimiento/psicologo_pages/iniciopsi.dart';
import 'package:seguimiento/user_pages/MainPage.dart';

class ChatPageArguments {
  final String peerId;
  final String peerNickname;

  ChatPageArguments({required this.peerId, required this.peerNickname});
}

class ChatPage extends StatefulWidget {
  const ChatPage(
      {Key? key,
      required this.arguments,
      required this.isPsicologo,
      required this.isVistaDetails})
      : super(key: key);

  final ChatPageArguments arguments;
  final bool isPsicologo;
  final bool isVistaDetails;

  @override
  ChatPageState createState() => ChatPageState();
}

class ChatPageState extends State<ChatPage> {
  late String currentUserId;

  List<QueryDocumentSnapshot> listMessage = [];
  int _limit = 20;
  final int _limitIncrement = 20;
  String groupChatId = "";

  File? imageFile;
  bool isLoading = false;
  bool isShowSticker = false;
  String imageUrl = "";

  final TextEditingController textEditingController = TextEditingController();
  final ScrollController listScrollController = ScrollController();
  final FocusNode focusNode = FocusNode();

  late ChatProvider chatProvider;

  @override
  void initState() {
    super.initState();
    chatProvider = context.read<ChatProvider>();
    currentUserId = FirebaseAuth.instance.currentUser!.uid;

    //focusNode.addListener(onFocusChange);
    listScrollController.addListener(_scrollListener);
    createGroupChatID();
  }

  _scrollListener() {
    if (!listScrollController.hasClients) return;
    if (listScrollController.offset >=
            listScrollController.position.maxScrollExtent &&
        !listScrollController.position.outOfRange &&
        _limit <= listMessage.length) {
      setState(() {
        _limit += _limitIncrement;
      });
    }
  }

  void createGroupChatID() {
    String peerId = widget.arguments.peerId;
    if (currentUserId.compareTo(peerId) > 0) {
      groupChatId = '$currentUserId-$peerId';
    } else {
      groupChatId = '$peerId-$currentUserId';
    }

    chatProvider.updateDataFirestore(
      widget.isPsicologo
          ? FirestoreConstants.pathPsicologosCollection
          : FirestoreConstants.pathUserCollection,
      currentUserId,
      {FirestoreConstants.chattingWith: peerId},
    );
  }

  void onSendMessage(String content, int type) async {
    if (content.trim().isNotEmpty) {
      if (widget.isPsicologo) {
        FirestoreService()
            .updateEstadoAtencionAutomatico(widget.arguments.peerId);
      }
      chatProvider.sendMessage(
          content, type, groupChatId, currentUserId, widget.arguments.peerId);
      final response = await NotificationService().sendNotification(
          widget.arguments.peerId,
          textEditingController.text,
          currentUserId,
          widget.isPsicologo);
      if (response == null) {
        return;
      }

      if (listScrollController.hasClients) {
        listScrollController.animateTo(0,
            duration: const Duration(milliseconds: 300), curve: Curves.easeOut);
      }
    } else {}
  }

  Widget buildChatMessages(int index, DocumentSnapshot? document) {
    if (document != null) {
      MessageChat messageChat = MessageChat.fromDocument(document);
      if (messageChat.idFrom == currentUserId) {
        // Right (my message)
        return Row(
          children: <Widget>[
            messageChat.type == TypeMessage.text
                // Text
                ? Container(
                    child: Text(
                      messageChat.content,
                      textScaler: const TextScaler.linear(1.0),
                      style: const TextStyle(color: CustomColors.primaryDark),
                    ),
                    padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                    width: 200,
                    decoration: BoxDecoration(
                        color: CustomColors.primary80pink,
                        borderRadius: BorderRadius.circular(15)),
                    margin: EdgeInsets.only(
                        bottom: isLastMessageRight(index) ? 20 : 10, right: 10),
                  )
                : messageChat.type == TypeMessage.image
                    // Image
                    ? Container(
                        child: OutlinedButton(
                          child: Material(
                            child: Image.network(
                              messageChat.content,
                              loadingBuilder: (BuildContext context,
                                  Widget child,
                                  ImageChunkEvent? loadingProgress) {
                                if (loadingProgress == null) return child;
                                return Container(
                                  decoration: const BoxDecoration(
                                    color: Colors.grey,
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(8),
                                    ),
                                  ),
                                  width: 200,
                                  height: 200,
                                  child: Center(
                                    child: CircularProgressIndicator(
                                      color: Colors.black,
                                      value:
                                          loadingProgress.expectedTotalBytes !=
                                                  null
                                              ? loadingProgress
                                                      .cumulativeBytesLoaded /
                                                  loadingProgress
                                                      .expectedTotalBytes!
                                              : null,
                                    ),
                                  ),
                                );
                              },
                              errorBuilder: (context, object, stackTrace) {
                                return Material(
                                  child: Image.asset(
                                    'images/img_not_available.jpeg',
                                    width: 200,
                                    height: 200,
                                    fit: BoxFit.cover,
                                  ),
                                  borderRadius: const BorderRadius.all(
                                    Radius.circular(8),
                                  ),
                                  clipBehavior: Clip.hardEdge,
                                );
                              },
                              width: 200,
                              height: 200,
                              fit: BoxFit.cover,
                            ),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8)),
                            clipBehavior: Clip.hardEdge,
                          ),
                          onPressed: () {},
                          style: ButtonStyle(
                              padding: MaterialStateProperty.all<EdgeInsets>(
                                  const EdgeInsets.all(0))),
                        ),
                        margin: EdgeInsets.only(
                            bottom: isLastMessageRight(index) ? 20 : 10,
                            right: 10),
                      )
                    // Sticker
                    : Container(
                        child: Image.asset(
                          'images/${messageChat.content}.gif',
                          width: 100,
                          height: 100,
                          fit: BoxFit.cover,
                        ),
                        margin: EdgeInsets.only(
                            bottom: isLastMessageRight(index) ? 20 : 10,
                            right: 10),
                      ),
          ],
          mainAxisAlignment: MainAxisAlignment.end,
        );
      } else {
        // Left (peer message)
        return Container(
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  isLastMessageLeft(index)
                      ? const Material(
                          child: SizedBox.shrink(),
                        )
                      : Container(),
                  messageChat.type == TypeMessage.text
                      ? Container(
                          child: Text(
                            messageChat.content,
                            textScaler: const TextScaler.linear(1.0),
                            style: const TextStyle(
                                color: CustomColors.primaryDark),
                          ),
                          padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                          width: 200,
                          decoration: BoxDecoration(
                              color: CustomColors.surface,
                              borderRadius: BorderRadius.circular(8)),
                          margin: const EdgeInsets.only(left: 10),
                        )
                      : messageChat.type == TypeMessage.image
                          ? Container(
                              child: TextButton(
                                child: Material(
                                  child: Image.network(
                                    messageChat.content,
                                    loadingBuilder: (BuildContext context,
                                        Widget child,
                                        ImageChunkEvent? loadingProgress) {
                                      if (loadingProgress == null) return child;
                                      return Container(
                                        decoration: const BoxDecoration(
                                          color: Colors.blueGrey,
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(8),
                                          ),
                                        ),
                                        width: 200,
                                        height: 200,
                                        child: Center(
                                          child: CircularProgressIndicator(
                                            color: Colors.red,
                                            value: loadingProgress
                                                        .expectedTotalBytes !=
                                                    null
                                                ? loadingProgress
                                                        .cumulativeBytesLoaded /
                                                    loadingProgress
                                                        .expectedTotalBytes!
                                                : null,
                                          ),
                                        ),
                                      );
                                    },
                                    errorBuilder:
                                        (context, object, stackTrace) =>
                                            Material(
                                      child: Image.asset(
                                        'images/img_not_available.jpeg',
                                        width: 200,
                                        height: 200,
                                        fit: BoxFit.cover,
                                      ),
                                      borderRadius: const BorderRadius.all(
                                        Radius.circular(8),
                                      ),
                                      clipBehavior: Clip.hardEdge,
                                    ),
                                    width: 200,
                                    height: 200,
                                    fit: BoxFit.cover,
                                  ),
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(8)),
                                  clipBehavior: Clip.hardEdge,
                                ),
                                onPressed: () {},
                                style: ButtonStyle(
                                    padding:
                                        MaterialStateProperty.all<EdgeInsets>(
                                            const EdgeInsets.all(0))),
                              ),
                              margin: const EdgeInsets.only(left: 10),
                            )
                          : Container(
                              child: Image.asset(
                                'images/${messageChat.content}.gif',
                                width: 100,
                                height: 100,
                                fit: BoxFit.cover,
                              ),
                              margin: EdgeInsets.only(
                                  bottom: isLastMessageRight(index) ? 20 : 10,
                                  right: 10),
                            ),
                ],
              ),

              // Time
              isLastMessageLeft(index)
                  ? Container(
                      child: Text(
                        DateFormat('dd MMM kk:mm').format(
                            DateTime.fromMillisecondsSinceEpoch(
                                int.parse(messageChat.timestamp))),
                        textScaler: const TextScaler.linear(1.0),
                        style: const TextStyle(
                            color: CustomColors.surface,
                            fontSize: 12,
                            fontStyle: FontStyle.italic),
                      ),
                      margin:
                          const EdgeInsets.only(left: 50, top: 5, bottom: 5),
                    )
                  : const SizedBox.shrink()
            ],
            crossAxisAlignment: CrossAxisAlignment.start,
          ),
          margin: const EdgeInsets.only(bottom: 10),
        );
      }
    } else {
      return const SizedBox.shrink();
    }
  }

  bool isLastMessageLeft(int index) {
    if ((index > 0 &&
            listMessage[index - 1].get(FirestoreConstants.idFrom) ==
                currentUserId) ||
        index == 0) {
      return true;
    } else {
      return false;
    }
  }

  bool isLastMessageRight(int index) {
    if ((index > 0 &&
            listMessage[index - 1].get(FirestoreConstants.idFrom) !=
                currentUserId) ||
        index == 0) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> onBackPress() {
    if (isShowSticker) {
      setState(() {
        isShowSticker = false;
      });
    } else {
      chatProvider.updateDataFirestore(
        FirestoreConstants.pathUserCollection,
        currentUserId,
        {FirestoreConstants.chattingWith: null},
      );
      Navigator.pop(context);
    }

    return Future.value(false);
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Container(
      color: CustomColors.primary80pink,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: CustomColors.purplePrograma,
          appBar: widget.isVistaDetails
              ? null
              : AppBar(
                  backgroundColor: CustomColors.purplePrograma,
                  iconTheme: const IconThemeData(
                    color: Colors.black, //change your color here
                  ),
                  elevation: 0,
                  toolbarHeight: size.height / 12,
                  centerTitle: true,
                  title: Text(
                    widget.arguments.peerNickname,
                    textScaler: const TextScaler.linear(1.0),
                    style: const TextStyle(color: Colors.black),
                  ),
                  actions: <Widget>[
                    widget.isPsicologo
                        ? IconButton(
                            icon: Icon(
                              MdiIcons.bookInformationVariant,
                              color: Colors.black,
                            ),
                            onPressed: () {
                              /*
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: ((context) => InfoPaciente(
                                          docID: widget.arguments.peerId,
                                          nombreCompleto:
                                              widget.arguments.peerNickname,
                                          rut: '',
                                        )),
                                  ));*/
                            },
                          )
                        : Container(),
                    widget.isPsicologo
                        ? IconButton(
                            icon: Icon(
                              MdiIcons.home,
                              color: Colors.black,
                            ),
                            onPressed: () {
                              Navigator.pushAndRemoveUntil(
                                  context,
                                  MaterialPageRoute<void>(
                                      builder: (BuildContext context) =>
                                          const InicioPsicologo()),
                                  (route) => false);
                            },
                          )
                        : IconButton(
                            icon: Icon(
                              MdiIcons.home,
                              color: Colors.black,
                            ),
                            onPressed: () {
                              Navigator.pushAndRemoveUntil(
                                  context,
                                  MaterialPageRoute<void>(
                                      builder: (BuildContext context) =>
                                          const MainPage(
                                            activePage: 0,
                                          )),
                                  (route) => false);
                            },
                          )
                  ],
                ),
          body: Stack(
            children: <Widget>[
              Column(
                children: <Widget>[
                  // List of messages
                  buildListMessage(),

                  // Input content
                  buildInput(),
                ],
              ),

              // Loading
              buildLoading()
            ],
          ),
        ),
      ),
    );
  }

  Widget buildLoading() {
    return Positioned(
      child: isLoading ? const LoadingView() : const SizedBox.shrink(),
    );
  }

  List<String> respuestasRapidas = [
    'Me gustaría ser contactadx',
    'Necesito ayuda',
    'No me siento bien en este momento'
  ];
  Widget buildInput() {
    return Container(
      child: Column(
        children: <Widget>[
          widget.isPsicologo
              ? Container()
              : SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: List.generate(respuestasRapidas.length, (index) {
                      return InkWell(
                        onTap: () {
                          onSendMessage(respuestasRapidas[index].toString(),
                              TypeMessage.text);
                        },
                        child: Container(
                          margin: const EdgeInsets.all(10),
                          padding: const EdgeInsets.symmetric(
                              vertical: 8, horizontal: 15),
                          decoration: const BoxDecoration(
                            color: CustomColors.primary,
                            borderRadius: BorderRadius.all(
                              Radius.circular(12),
                            ),
                          ),
                          child: Text(
                            respuestasRapidas[index],
                            style: const TextStyle(
                                fontSize: 12,
                                color: Colors.white,
                                fontFamily: 'roboto-regular'),
                          ),
                        ),
                      );
                    }),
                  ),
                ),

          // Edit text
          Flexible(
            child: Container(
              margin:
                  const EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 5),
              child: TextField(
                onSubmitted: (value) {
                  onSendMessage(textEditingController.text, TypeMessage.text);
                  textEditingController.clear();
                },
                style: const TextStyle(color: Colors.black, fontSize: 15),
                controller: textEditingController,
                decoration: InputDecoration(
                  enabledBorder: const UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: CustomColors.primary,
                    ),
                  ),
                  focusedBorder: const UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: CustomColors.primary,
                    ),
                  ),
                  filled: true,
                  fillColor: CustomColors.surface,
                  floatingLabelBehavior: FloatingLabelBehavior.always,
                  hintText: 'Envía un mensaje',
                  hintStyle: const TextStyle(color: Colors.black),
                  suffixIcon: IconButton(
                    onPressed: () {
                      onSendMessage(
                          textEditingController.text, TypeMessage.text);
                      textEditingController.clear();
                    },
                    icon: const Icon(
                      Icons.send,
                      color: CustomColors.primary,
                    ),
                  ),
                ),
                focusNode: focusNode,
              ),
            ),
          ),

          // Button send message
        ],
      ),
      constraints: BoxConstraints(
          minWidth: double.infinity, maxHeight: widget.isPsicologo ? 60 : 120),
      //width: double.infinity,
      // height: 130,
      decoration: const BoxDecoration(
          border: Border(top: BorderSide(color: Colors.blueGrey, width: 0.5)),
          color: CustomColors.primary80pink),
    );
  }

  Widget buildListMessage() {
    return Flexible(
      child: groupChatId.isNotEmpty
          ? StreamBuilder<QuerySnapshot>(
              stream: chatProvider.getChatStream(groupChatId, _limit),
              builder: (BuildContext context,
                  AsyncSnapshot<QuerySnapshot> snapshot) {
                if (snapshot.hasData) {
                  listMessage = snapshot.data!.docs;
                  if (listMessage.isNotEmpty) {
                    return ListView.builder(
                      padding: const EdgeInsets.all(10),
                      itemBuilder: (context, index) =>
                          buildChatMessages(index, snapshot.data?.docs[index]),
                      itemCount: snapshot.data?.docs.length,
                      reverse: true,
                      controller: listScrollController,
                    );
                  } else {
                    return const Center(
                        child: Text(
                      "No se ha iniciado conversación aquí...",
                      textScaler: TextScaler.linear(1.0),
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                          color: Colors.white),
                    ));
                  }
                } else {
                  return const Center(
                    child: CircularProgressIndicator(
                      color: CustomColors.orange2,
                    ),
                  );
                }
              },
            )
          : const Center(
              child: CircularProgressIndicator(
                color: Colors.black,
              ),
            ),
    );
  }
}
